��    -     �              �"  ,   �"  .   #  ?   I#     �#     �#  0   �#     �#     �#     $  	   !$     +$  i   :$  *   �$     �$     �$     �$  
   �$  -   %     2%  V   :%     �%  	   �%  	   �%     �%  3   �%  Y   �%     M&     c&  
   p&     {&     �&     �&     �&     �&     �&  	   �&     �&  $   �&  ?   '  /   K'  (   {'  %   �'     �'  	   �'     �'     �'  
   (     (  	   (     (     *(     6(     =(  
   J(     U(     m(     t(     �(     �(     �(  
   �(     �(     �(     �(  <   �(     *)  	   0)  
   :)     E)     N)     V)     s)     {)     �)     �)     �)     �)  +   �)  3   *      B*     c*     v*     �*  
   �*     �*     �*     �*     �*  3   +     6+     T+     g+     �+     �+     �+     �+     �+     �+     �+     �+     ,     ,     ,     ,  0   ",     S,     d,     j,     v,  
   �,     �,     �,     �,     �,     �,     �,  $   �,  ~   -     �-  
   �-     �-  
   �-  	   �-  
   �-     �-     �-     �-     �-     .  5   .     L.     [.     g.  !   n.     �.  #   �.     �.     �.     �.     �.     /     /     //     H/     T/     m/     �/     �/     �/  
   �/     �/     �/  	   �/  6   �/     
0  v   0     �0  *   �0  c   �0     )1     11     81     ?1     W1     q1     u1  
   }1  
   �1  
   �1  
   �1  
   �1  
   �1     �1     �1     �1  	   �1     2     2     2     2     02  
   62     A2     S2     e2     z2     �2     �2     �2     �2     �2     �2     �2     �2     �2     3     %3     23  	   H3     R3     d3     l3     t3     �3     �3     �3     �3     �3     �3     �3     �3  2   �3     4     %4     .4     H4     Q4     l4     �4     �4     �4     �4     �4  	   �4     �4     �4     �4     5     #5     55     J5     W5      \5  *   }5     �5     �5     �5     �5     �5     �5     �5     6     $6     56     N6     e6  	   n6     x6     {6     �6     �6     �6     �6     �6  
   �6     �6  	   7     7     $7     87     F7     Z7     i7     r7     �7     �7  9   �7     �7  I   �7  !   /8  '   Q8  	   y8     �8     �8  0   �8  	   �8     �8     �8  	   �8  '   �8  V   9  3   p9  0   �9     �9     �9     �9     �9     :     :     ':     3:     8:     I:     Q:  
   ]:  &   h:  
   �:     �:     �:     �:     �:     �:     ;  
   ;     ;  
   !;     ,;     ;;     L;     Y;     f;     �;     �;     �;     �;     �;     �;     �;     �;  	   �;     �;     �;     <     #<     )<     <<     C<     X<     `<     l<     z<  �   �<     =      0=     Q=     l=  	   �=     �=     �=     �=     �=     �=     �=     �=     	>  2   >     N>  &   \>     �>     �>     �>     �>     �>  5   �>  &   ?     B?     O?     T?  &   c?     �?     �?     �?     �?     �?  
   �?     �?     �?  	   �?     @     @     @     -@     2@     P@  	   U@     _@  	   h@     r@  
   w@     �@     �@     �@  ?   �@  A   A  S   CA  =   �A  K   �A  @   !B  6   bB  -   �B  x   �B  �   @C  �   	D  �   �D  �   E  }   �E  L   F  �   hF  �   �F  �   �G  }   )H  h   �H  k   I  �   |I  R   PJ  ;   �J  =   �J  v   K    �K  j   �L  n   M  ]   �M     �M  �   `N  7   �N     -O  �   3O  |   �O     LP     PP     WP     ]P     qP     �P  	   �P  '   �P     �P  D   �P     
Q     Q     Q  H   'Q     pQ     �Q     �Q  !   �Q     �Q     �Q  P   �Q     GR  U   WR     �R     �R  	   �R  
   �R     �R  N   �R     /S     ;S     AS  �   OS  
   
T     T     #T     )T  	   .T  ^   8T  f   �T     �T  	   U     U  
    U     +U     7U     =U     EU     KU     RU     dU     kU  	   yU     �U  
   �U     �U     �U     �U     �U  
   �U     �U     �U  	   �U     �U     V     	V     V     V     'V     +V  
   1V     <V     EV  	   KV     UV     eV     mV     yV     �V     �V     �V     �V     �V     �V  
   �V     �V     �V     �V     �V     �V     �V     �V  	   W     W     %W     5W     =W     DW  	   MW     WW     iW     xW     ~W     �W     �W     �W     �W     �W     �W     �W     �W     �W  
   �W     �W     X  
   X      X     ,X     8X     FX     RX     ZX  
   bX     mX  
   zX     �X     �X  	   �X     �X     �X     �X     �X     �X     �X     �X     �X     �X  
   
Y  (  Y  B   >[  C   �[  Q   �[     \  ,   #\  �   P\  �   >]  1   ^  
   6^     A^     N^  �   k^  N    _     O_     e_     w_     �_  5   �_     �_  �   �_     }`     �`     �`  3   �`  M   �`  u   2a     �a     �a     �a     �a     b     #b     =b     Nb     _b     lb  $   �b  1   �b  X   �b  k   6c  ;   �c  G   �c  '   &d     Nd  &   ld     �d     �d     �d     �d     �d     �d     e     e  
   0e      ;e     \e  -   ee     �e     �e     �e  
   �e  '   �e     �e  !   f  �   0f     �f     �f      �f     g  
   g  )   g  
   Gg  $   Rg  &   wg     �g     �g     �g  .   �g  >   h  ;   Yh  5   �h     �h     �h     �h     i  5   &i  -   \i  /   �i  O   �i  /   
j     :j  "   Mj     pj      �j  2   �j  !   �j      k     k     k     .k     Ek     Sk     bk     ik  I   xk     �k     �k     �k     l     $l  
   4l     ?l  
   Vl     al     tl      �l  ,   �l  �   �l     �m     �m  "   �m     �m     �m     n     n     0n     7n  1   Mn  
   n  B   �n     �n     �n  
   �n  ,   o     3o  ,   Mo     zo     �o      �o     �o     �o     �o  9   p     Ap  *   Wp  '   �p     �p  
   �p     �p     �p     �p     q     q  �   (q     �q  �   �q     �r  6   �r  �   �r     {s     �s     �s  :   �s  J   �s     %t     )t     1t     It     Wt     et     st     �t     �t  D   �t  "   �t     u     &u     7u     >u  $   Tu     yu     �u     �u  !   �u  $   �u     v     v     .v     Dv     `v     qv     �v     �v     �v  &   �v     �v     w     w     7w  +   Fw     rw     {w     �w  $   �w     �w  <   �w     x  )   %x     Ox     ex     nx  _   �x     �x     �x  .   y     7y  $   Ky  ,   py  "   �y  D   �y     z  $   $z     Iz  	   az     kz     �z  !   �z  2   �z  +   �z  )   {     7{  
   P{  =   [{  J   �{     �{     �{  "   |     +|     ?|  '   L|  .   t|      �|  ,   �|  *   �|  #   }     @}  
   I}  
   T}  "   _}     �}     �}     �}     �}     �}     	~  '   ~      B~     c~  3   �~     �~      �~     �~               ;     G  �   _  "   �  �   �  <   ��  >   �     #�     ;�     Q�  :   Z�     ��     ��     ��     Ɓ  :   ρ  �   
�  8   ��  5   Ƃ     ��     �     $�     5�  "   >�     a�     u�     ��  )   ��  
   ��     ʃ     ڃ  V   �     @�     X�  !   s�     ��  ;   ��  C   �     5�     >�     P�     `�     r�  $   ��     ��     ͅ  D   �     &�     3�  )   <�  "   f�     ��     ��     ��     ̆     �     �     �  %   &�     L�  )   [�     ��  &   ��     ��     ��     Ї     �  �   ��     Ԉ  4   �  3   (�  6   \�  
   ��     ��     ��     ԉ     �     
�  3   )�  "   ]�      ��  `   ��  &   �  2   )�  -   \�  )   ��  -   ��  '   �  "   
�  U   -�  ?   ��     Ì     ٌ  :   ��  5   �     Q�  )   p�     ��     ��          ��     �  '   ��     &�     >�     G�     ]�     y�  1   ��     ��     ��     Վ  
   �     �     �     �     �     3�  H   R�  @   ��  c   ܏  j   @�  q   ��  a   �  J   �  7   ʑ  �   �  S  ��  �   ��    ��  �   ��  �   ��  s   Q�  �   ŗ  �   ��  �   J�  _   �  �   a�  �   �  �   ��  m   ��  T   ��  `   O�  �   ��  r  e�  �   ؟  �   w�  �   �    ��  �   ��  I   ��     ��    �  �   �     ��  
   ��  
   ǥ  1   ҥ  1   �     6�     S�  G   q�     ��  |   ��  
   =�     H�     X�  ~  j�      �  "   
�      -�  E   N�  "   ��     ��  |   ֩     S�  s   r�     �     ��     	�     �  
   2�  u   =�     ��  
   ̫     ׫  �   �     �     �  
   �     (�  
   /�  �   :�  �   ŭ     u�     ��     ��     ��     ��     ˮ     ޮ     �  	   ��     �     �     $�     6�  	   J�     T�  	   l�     v�     ��     ��     ��  %   Ư     �     �     �     +�     3�     B�     P�  	   e�     o�     }�     ��     ��     ��     ��  	   ˰     հ  +   �  	   �     �     7�     D�     S�  	   a�     k�     ��     ��     ��     ��     ϱ     �     �  
   �  	   �  )   "�     L�  	   a�     k�     y�  4   ��     ��     Ҳ     ۲     ��     �     �     -�     D�     R�     e�     ~�  	   ��     ��     ��  	   ׳  	   �     �      ��      �     :�  
   O�  	   Z�     d�     p�     |�     ��     ��  	   ��     ��  	   ��     ˴     ״     �     
�  
    �  �   +�     ȵ     ܵ   		This plugin provides bar for bookmarks.
		 %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i errors occurred, see log %i file will be deleted %i files will be deleted %i open item %i open items %i warnings occurred, see log <Top> <Unknown> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Add Application Add Bookmark Add Notebook Add column Add new bookmarks to the beginning of the bar Add row Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files All Tasks Allow public access Always use last cursor position when opening a page An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Arithmetic Attach File Attach image first Attachment Browser Attachments Attachments: Author Auto
Wrap Auto indenting Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave version on regular intervals Back to Original Name BackLinks BackLinks Pane Backend Backlinks: Bazaar Bookmarks BookmarksBar Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Capture whole screen Center Change columns Changes Characters Characters excluding spaces Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Clone row Code Block Column 1 Command Command does not modify data Comment Common include footer Common include header Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Created Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Demote Dependencies Description Details Discard note? Distraction Free Editing Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root E_xport... Edit Custom Tool Edit Image Edit Link Edit Table Edit _Source Editing Editing file: %s Enable Version Control? Enabled Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Fossil Get more plugins online Get more templates online Git Gnuplot Grid lines Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide menubar in fullscreen mode Highlight current line Home Page Icon Images Import Page Include subpages Index Index page Inline Calculator Insert Code Block Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Sequence Diagram Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Labels marking tasks Last Modified Leave link to new page Left Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Managing table columns Map document root to URL Match _case Maximum page width Menubar Mercurial Modified Month Move Selected Text... Move Text to Other Page Move column ahead Move column backward Move text to Name Need output file to export MHTML Need output folder to export full notebook New File New Page New S_ub Page... New _Attachment Next No Applications Found No changes since last version No dependencies No such file: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open cell content link Open help Open in New Window Open in New _Window Open new page Open plugins folder Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Name Page Template Page section Paragraph Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please select a row, before you push the button. Please specify a notebook Plugin Plugins Port Position in the window Pr_eferences Preferences Prev Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Remove column Remove row Removing Links Rename page "%s" Replace _All Replace with Restore page to saved version? Rev Right Right Side Pane Right margin position Row down Row up S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Screenshot Command Search Search _Backlinks... Section Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Sequence Diagram Server not started Server started Server stopped Set New Name Set default text editor Set to Current Page Show Line Numbers Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show full Page Name Show full page name Show helper toolbar Show in the toolbar Show right margin Show the cursor also for pages that can not be edited Show the page title heading in the ToC Single _page Size Smart Home key Some error occurred while running "%s" Sort alphabetically Sort pages by tags Source View Spell Checker Start _Web Server Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Tags for non-actionable tasks Task Task List Template Templates Text Text Files Text From _File... Text background color Text foreground color The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The inline calculator plugin was not able
to evaluate the expression at the cursor. The table must consist of at least on row!
 No deletion done. There are no changes in this notebook since the last version that was saved This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a sequence diagram editor for zim based on seqdiag.
It allows easy editing of sequence diagrams.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This plugin turns one section of the notebook into a journal
with a page per day, week or month.
Also adds a calendar widget to access these pages.
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Tray Icon Turn page name into tags for task items Type Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unspecified Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use a page for each Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s With this plugin you can embed a 'Table' into the wiki page. Tables will be shown as GTK TreeView widgets.
Exporting them to various formats (i.e. HTML/LaTeX) completes the feature set.
 Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Zim Desktop Wiki Zoom _Out _About _All Panes _Arithmetic _Back _Browse _Bugs _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Edit _Edit Link _Edit Link or Object... _Edit Properties _Emphasis _FAQ _File _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _Move Here _New Page... _Next _None _Normal Size _Numbered List _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In horizontal lines no grid lines readonly seconds translator-credits vertical lines with lines Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2020-10-20 06:26+0000
Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>
Language-Team: Arabic <https://hosted.weblate.org/projects/zim/master/ar/>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 ? 4 : 5;
X-Generator: Weblate 4.3.1-dev
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
 		هذا الملحق يُنشئ شريطاً للعلامات.
		 %(cmd)s
ظهور رسالة انتهاء غير طبيعي %(code)i %(n_error)i أخطاء %(n_warning)i هناك تحذيرات, راجع السجل %A %d %B %Y %i حدثت أخطاء، راجع السجل %i من الملفات سيتم حذفها ملف واحد %i سيتم حذفه ملفان اثنان %i سيتم حذفهما %i ملفات سيتم حذفها %i ملفاً سيتم حذفها %i من الملفات سيتم حذفها %i بطاقة مفتوحة بطاقة واحدة %i  مفتوحة بطاقتان %i  اثنان مفتوحتان %i بطاقات مفتوحة %i بطاقات مفتوحة %i بطاقة مفتوحة %i  هناك تحذيرات، راجع السجل <أعلى> <مجهول> ويكي سطح المكتب هناك ملف بالاسم <b>"%s"</b> موجود مسبقاً
بمكنك اختيار اسم آخر أو استبدال الملف الموجود الجدول يجب أن يحتوي على عمود واحد على الأقل أضف تطبيقاً أضف علامة أضف دفتراً إضافة عمود أضف علامة جديدة في أول الشريط إضافة صفوف يضيف دعماً للتدقيق اللغوي باستخدام gtkspell.

هذا الملحق جزء من زيم ويأتي مضمناً فيه.
 محاذاة كل الملفات كل المهام السماح للعامة (الكل) بالوصول دائماً استخدم آخر موضع للمؤشر عند فتح صفحة حدث خطأ أثناء إنشاء الصورة.
هل تريد حفظ النص المصدري على أية حال؟ مصدر صفحة مشروح التطبيقات عمليات حسابية إرفق ملفاً أرفق صورة أولاً عارض المرفقات المرفقات المرفقات المؤلف التفاف تلقائي مسافة بادئة تلقائية تم حفظ إصدارة زيم تلقائياً. اختر الكلمة الحالية تلقائياً عند تطبيقك للتنسيق تلقائياً حول الكلمات  "ذات الأحرف الأولى الكبيرة" إلى وصلات حول مسار الملف إلى وصلة تلقائياً حفظ الإصدارة تلقائياً على فترات منتظمة عودة إلى الاسم الأصلي الوصلات الخلفية لوحة الوصلات الخلفية الخلفية وصلات الخلفية: Bazaar العلامات شريط العلامات لوحة إلى الأسفل استعرض قائمة نق_طية إعداد تعذر حفظ الصفحة: %s الغِ التقاط صورة لكامل الشاشة وسط تغيير الأعمدة التغييرات محارف محارف لا تتضمن مسافات تدقيق لغوي قائمة مربع ا_ختيار صينية أيقونات تقليدية،
لا تستخدم الأسلوب الجديد لصينية الحالة الخاصة بأوبونتو امحُ استنساخ صفوف كتلة النص البرمجي العمود 1 الأمر الأمر لا يغير البيانات تعليق تعميم تضمين التذييل تعميم تضمين الترويسة ال_دفتر كاملاً هيء التطبيقات إعداد الملحق هيء تطبيقاً لفتح وصلات "%s" هيء تطبيقاً لفتح الملفات
من نوع "%s" اعتبار كل مربعات الإختيار كمهام. انسخ عنوان البريد الإلكتروني انسخ القالب إنسخ _كـ إنسخ _الوصلة انسخ موقع الوصلة تعذر العثور على ملف تنفيذي "%s" تعذر العثور على الدفتر: %s تعذر العثور على القالب: "%s" تعذر العثور على الملف أو المجلد لهذا الدفتر تعذر تحميل التدقيق اللغوي تعذر فتح %s تعذر تحليل التعبير تعذرت قراءة: %s تعذر حفظ الصفحة: %s إنشاء صفحة جديدة لكل ملاحظة هل تود إنشاء مجلد؟ تم إنشاؤه ق_ص أدوات مخصصة أدوات _مخصصة تخصيص... التاريخ يوم إفتراضي التنسيق الافتراضي لنسخ النص إلى الحافظة الدفتر الافتراضي التأخير احذف الصفحة احذف الصفحة "%s"؟ حذف صفوف تخفيض الاعتماديات الوصف التّفاصيل أأنبذ الملاحظة؟ تحرير بلا مُلهيات هل تريد حذف كل العلامات؟ هل تريد استعادة الصفحة: %(page)s\n
إلى الإصدارة: %(version)s ?\n
\n
كل التغييرات منذ الإصدارة الأخير المحفوظة ستضيع! جذر الصفحة صدّر... حرر الأداة المخصصة حرر الصورة حرر الوصلة حرر الجدول حرر _المصدر حرر حرر الملف: %s تمكين التحكم في الإصدارات؟ مُفعل خطأ في %(file)s في السطر %(line)i قرب "%(snippet)s" تقويم الرياضيات وسع الكل صدِّر صدر كل الصفحات كملف واحد إكتمل التصدير تصدير كل صفحة كملف منفصل تصدير الدفتر فشل فشل بدء العملية: %s فشل بدء التطبيق %s الملف موجود قوال_ب الملف تغيرت محتويات الملف على القرص:%s الملف موجود الملف محمي ضد الكتابة:%s نوع الملف غير مدعوم: %s اسم الملف تصفية إبحث إيجاد ال_تالي إيجاد ال_سابق إبحث واستبدل ابحث عن تعليم المهام بمستحقة الأداء في يوم الإثنين أو الثلاثاء قبل نهاية الأسبوع مجلد المجلد موجود مسبقاً وبه ملفات، التصدير إلى هذا المجلد ربما يؤدي لاستبدال الملفات الموجودة. هل تريد الاستمرار؟ المجلد موجود: %s مجلد به قوالب لمرفقات الملفات للبحث المتقدم يمكنك استخدام عوامل بحث مثل
AND, OR و NOT. أنظر صفحات المساعدة للمزيد من المعلومات تنـ_سيق التنسيق Fossil المزيد من الملحقات على الإنترنت الحصول على المزيد من القوالب من الإنترنت Git Gnuplot خطوط إرشادية رئيسي _1 رئيسي _2 رئيسي _3 رئيسي _4 رئيسي _5 الارتفاع أخفِ شريط القوائم في وضعية ملء الشاشة تمييز السطر الحالي الصفحة الرئيسية الأيقونة صور استورد صفحة ضمن الصفحات الفرعية صفحة الفهرس صفحة الفهرس حاسبة مضمنة إدرج كتلة نص برمجي إدرج التاريخ والوقت إدرج مخططاً "إدخال ديتا" إدرج معادلة إدرج خريطة Gnuplot إدرج Gnuplot إدرج صورة إدرج وصلة إدرج المجموع إدرج لقطة شاشة إدرج مخططاً تسلسلياَ إدرج رمزاً إدرج جدولاً إدرج نصاً من ملف الواجهة كلمة مفتاحية لوصلة ويكي مجلة اذهب إلى اذهب إلى الصفحة بطاقات تمييز المهام آخر تعديل استبقِ الوصلة إلى الصفحة الجديدة يسار لوحة جانبية إلى اليسار فارز السطور سطور ربط بخريطة ذهنية اربط الملفات تحت جذر المستندات بالمسار الكامل للملف اربط بـ الموقع سجّل الأحداث مع  زايتقيست ملف السجلّ يبدو أنك وجدت علة ما اجعله التطبيق الافتراضي إدارة أعمدة الجدول اربط  المستند الجذر الى عنوان رابط (URL) طابق حالة الحروف العرض الأقصى للصفحة شريط القوائم Mercurial تاريخ التعديل شهر أنقل النص المحدد... أنقل النص إلى الصفحة الأخرى تحريك العمود إلى الأمام تحريك العمود إلى الخلف أنقل النص إلى الإسم يجب تحديد الملف الوجهة لتصدير  MHTML بحاجة إلى مجلد الوجهة لتصدير كامل الدفتر ملف جديد صفحة جديدة صفحة ف_رعية جديدة... _مرفق  جديد التالي تعذر العثور على تطبيق لا تغييرات منذ آخر إصدارة لا توجد اعتماديات لا يوجد ملف بهذا الاسم: %s لا وجود لوصلة الويكي:  %s لا توجد قوالب مثبتة دفتر دفاتر حسناً إفتح مجلد المرفقات افتح مجلداً افتح دفتراً افتح باستخدام... فتح جذر _المستند إفتح مجلد _الدفتر فتح _صفحة فتح وصلة محتوى الخلية فتح صفحة المساعدة إفتح نافذة جديدة إفتح المحددة في نافذة _جديدة افتح صفحة جديدة فتح مجلد الملحقات إفتح بواسطة "%s" اختياري خيارات الملحق %s أخرى... ملف المخرجات الملف الذي اخترته للتصدير موجود مسبقاً، حدد "_استبدل" للتصدير مع استبدال الملف. مجلد نواتج التصدير المجلد الذي اخترته للتصدير موجود وبه محتويات.. حدد "_استبدال" للتصدير مع الاستبدال يجب تحديد الوجهة لمخرجات التصدير الملف الناتج سيستبدل الملف المحدد استبدل الملف شريط_المسار صفحة الصفحة "%s" ليس لها مجلد أو مرفقات اسم الصفحة قالب الصفحة قسم فقرة يُرجى كتابة تعليق لهذه الإصدارة يُرجى ملاحظة أن الربط بصفحة غير موجودة
ينشيء أيضاً صفحة جديدة تلقائياً. الرجاء اختيار اسم ومجلد للدفتر يُرجى اختيار صف، قبل ضغط الزر يُرجى تحديد دفتر ملحق الملحقات منفذ الموضع ضمن النافذة الت_فضيلات التفضيلات السابق طباعة إلى مستعرض الويب ترقية الخ_صائص الخصائص يتيح لك أن تتعامل مع أحداثك المهمة بطريقة عصرية ملاحظة سريعة ملاحظة سريعة... التغييرات الأخيرة التغيرات الأخيرة الصفحات التي جرى _تعديلها مؤخراً إعادة تنسيق علامات الويكي لحظة بلحظة إحذف إحذف الكل حذف عمود حذف اعمدة جارٍ حذف الوصلات أعد تسمية الصفحة  "%s" استبدل ال_كل استبدال بـ استعادة الصفحة إلى إصدارتها المحفوظة مراجعة يمين لوحة جانبية إلى اليمين موضع الهامش الأيمن صف إلى أسفل صف إلى أعلى ح_فظ الإصدارة إحفظ ن_سخة... احفظ نسخة حفظ الإصدارة احفظ العلامات إصدارة محفوظة من زيم المجموع أمر التقاط صورة الشاشة ابحث إبحث الوصلات الخلفية قسم اختر ملفاً اختر مجلداً اختر صورة اختر إصدارة لرؤية التغييرات بين تلك الإصدارة والإصدارة الحالية
أو اختر عدة إصدارات لرؤية الاختلافات بين هذه الإصدارات.
 اختر شكل التصدير اختر الملف أو المجلد للتصدير اختر الصفحات المراد تصديرها اختيار نافذة أو  جزء من الشاشة تحديد مخطط تسلسلي لم يتم بدء الخادم تم بدء الخادم تم إيقاف الخادم حدد اسماً جديداً حدد محرراً افتراضياً للنصوص حدد للصفحة الحالية أظهر أرقام السطور عرض جدول المحتويات كويدجت عائمة بدلاً عن لوحة جانبية عرض فروقات الإصدارات أعرض أيقونة منفصلة لكل دفتر أظهر الاسم الكامل للصفحة أظهر اسم الصفحة كاملاً أظهر شريط أدوات المساعدة أظهار في شريط الأدوات أظهر الهامش الأيمن أظهر المؤشر أيضاً للصفحات التي لا يمكن تحريرها عرض عنوان الصفحة في جدول المحتويات صفحة_منفردة حجم مفتاح الانتقال الذكي إلى المنزل حدث خطأ أثناء تشغيل التطبيق %s فرز حسب الأبجدية فرز الصفحات حسب الوسوم عرض المصدر تدقيق لغوي ابدأ _خادم الويب رم_ز بناء الجملة الإعدادات الإفتراضية عرض التبويبة جدول محرر الجدول جدول المحتويات وسوم وسوم للمهام متعذرة التحقيق مهمة قائمة المهام القالب قوالب نص ملفات نصية نص من _ملف لون خلفية النص لون النص الأمامي المجلد
%s
لا وجود له
أتريد أن تنشئه الآن؟ المجلد "%s غير موجود.
هل تريد إنشاءه؟ هذه الحاسبة المضمنة لم تستطع
تقييم التعبير عند المؤشر. هذا الجدول يجب أن يحتوي على صف واحد على الأقل!
لم يتم الحذف. لا توجد تغييرات في هذا الدفتر منذ آخر مرة تم حفظ الإصدارة فيها. ربما يعني هذا أنك لم تقم بعد
بتثبيت القواميس المناسبة هذا الملف موجود مسبقاً
هل تريد استبداله؟ هذه الصفحة ليس لها مجلد مرفقات يتيح هذا الملحق التقاط صورة شاشة وإدرجها مباشرة
في صفحة زيم

هذا الملحق جزء من زيم ومضمن فيه.
 هذا الملحق يضيف مربع حوار يظهر كافة المهام المفتوحة في 
هذا الدفتر. فتح المهام إما أن يفتح مربعات الإختيار
أو العناصر المعلمة بوسوم مثل "TODO" أو "FIXME".

هذا الملحق جزء من زيم ويأتي مضمناً معه.
 هذا الملحق يضيف مربع حوار لإضافة نص سريع أو محتوى
الحافظة إلى صفحة زيم.

هذا الملحق جزء مضمن في زيم.
 يضيف هذا الملحق صينية أيقونات لتسهيل الوصول السريع إليها.

اعتماديات هذا الملحق  هي Gtk+ version 2.10 أو أحدث.

هذا الملحق جزء من زيم ويأتي مضمناً فيه.
 هذا الملحق يضيف ويدجتا اضافية تظهر قائمة الصفحات
التي لها ارتباط بالصفحة الحالية

هذا الملحق جزء من زيم ويأتي مضمناً به.
 هذا الملحق يضيف ويدجت إضافية تعرض جدول المحتويات
للصفحة الحالية.

هذا الملحق جزء من زيم ويأتي مضمناً فيه.
 هذا الملحق يضيف إعدادات تساعد في استخدام زيم
كمحرر بلا مُلهيات
 هذا الملحق يضيف  مربع حوار "إدرج رمز" ويتيح
التنسيق التلقائي للحروف المطبعية.
هذا الملحق جزء مضمن في زيم.
 هذا الملحق يضيف إمكانية التحكم في إصدارة الدفاتر.

هذا الملحق يدعم نظم كل من Bazaar، Git وMercurial  لمتابعة الإصدارات
 هذا الملحق يمكنك من تضمين معادلات حسابية في زيم.
وهو مبني على الوحدة القياسية من
http://pp.com.mx/python/arithmetic.
 هذا الملحق يتيح لك تقويم
 تعابير حسابية بسيطة في زيم
 هذا الملحق يوفر محرر مخططات لزيم وهو مبني على Ditaa. 

هذا الملحق جزء من زيم ويأتي مضمناً فيه.
 هذا الملحق يوفر محرر مخططات لزيم اعتماداً على GraphViz.

هذا ملحق أصيل مضمن مع زيم.
 هذا الملحق يوفر مربع حوار مع عرض مرئي 
لبنية الربط مع 
الدفتر. يمكن استخدامه كنوع من "الخريطة الذهنية"
لبيان كيفية ارتباط الصفحات ببعضها.
 هذا الملحق يتيح صنع فهرس للصفحة مفروزاً حسب الوسوم في سحابة
 هذا الملحق يوفر محرر خرط لزيم وهو مبني على GNU R.
 يوفر هذا الملحق إمكانية إدرج خريطة وهو مبني على Gnuplot.
 يتيح هذا الملحق محرراً للمخططات التسلسلية في زيم وهو مبني على seqdiag.
يسهل تحرير المخططات التسلسلية
.
 يوفر هذا الملحق حلاً لعدم توفر
دعم الطباعة في زيم. ويمكنه تصدير الصفحة الحالية
إلى هيئة html وفتحها في مستعرض الانترنت. بافتراض أن المستعرض 
يدعم الطباعة. وهذا يمكنك من إرسال البيانات إلى الطابعة في خطوتين.
 هذا الملحق يوفر محرر معادلات لزيم. الملحق مبني على latex.

هذا الملحق جزء من زيم ومضمن فيه.
 هذا الملحق يتيح تحرير المجموع لزيم وهو مبني على GNU Lilypond.

هذا الملحق جزء من زيم ومضمن فيه.
 هذا الملحق يعرض مجلد المرفقات للصفحة الحالية 
كأيقونة معروضة في اللوحة السفلية
 يتيح هذا الملحق إمكانية فرز سطور محددة بالترتيب  الهجائي.
إن كانت السطور مرتبة فعلا فإن الأمر يعكس الترتيب.
(من الألف إلى الياء ومن الياء إلى الألف).
 هذا الملحق يحول قسماً واحداً من الدفتر إلى مجلة
صفحة يومياً، أسبوعياً أو شهرياً
أيضاً يضيف ويدجيت إلى التقويم للوصول إلى هذه الصفحات.
 هذا يعني عادة أن الملف يحوي أحرفاً خاطئة العنوان للمتابعة يمكنك حفظ نسخة من هذه الصفحة أو نبذ
أي تغييرات. لو اخترت الحفظ نسخة فإن أي تغييرات سوف
تُفقد أيضاً، ولكن يمكنك استعادة النسخة لاحقاً. لإنشاء دفتر جديد تحتاج لاختيار مجلد فارغ.
بالطبع يمكنك أيضاً اختيار دفتر زيم موجود.
 جدول المحتويات اليوم اليوم تبديل حالة مربع الاختيار 'V' تبديل حالة مربع الاختيار 'X' لوحة إلى الأعلى صينية الأيقونات تحويل اسم الصفحة إلى وسم لعناصر المهام. نوع ألغِ المسافة البادئة بـ  <BackSpace>
(إذا كانت معطلة يمكنك استخدام <Shift><Tab>) مجهول غير محدد غير موسوم تحديث %i وصلة مرتبطة بهذه الصفحة تحديث وصلة واحدة %i مرتبطة بهذه الصفحة تحديث وصلتين %i  اثنتين مرتبطتين بهذه الصفحة تحديث %i وصلات مرتبطة بهذه الصفحة تحديث %i وصلة مرتبطة بهذه الصفحة تحديث %i وصلة مرتبطة بهذه الصفحة حدّث عنوان الصفحة جارِ تحديث الوصلات جاري تحديث الفهرس استخدم %s للانتقال إلى اللوحة الجانبية استخدم خطاً مخصصاً استخدم صفحة لكلِ اضغط مفتاح <Enter> لفتح الوصلات
(لو كان ذلك معطلاً يمكنك استخدام <Alt><Enter>) التحكم في النسخة التحكم في الإصدارات غير ممكن حاليا لهذا الدفتر.
هل تريد تمكينه؟ إصدارات عرض الشروح إعرض ال_سجل خادم الويب أسبوع عند تبليغك عن هذه العلة يُرجى تضمين
المعلومات في مربع النص أدناه كلمة بأ_كملها العرض صفحة الويكي: %s بواسطة هذا الملحق يمكنك تضمين جدول في صفحة الويكي. الجداول ستظهر في شكل ويدجات GTK TreeView.
تصديرها إلى صيغ متعددة مثل html/ laTeX يكمل ميزات الملحق.
 عدد الكلمات عدد الكلمات... كلمات سنة الأمس أنت تحرر ملفاً  بواسطة تطبيق خارجي. يُرجى إغلاق مربع الحوار هذا عند الانتهاء يمكنك تهيئة الأدوات المخصصة التي ستظهر
في قائمة الأدوات وفي شريط الأدوات أو في القائمة المنبثقة Zim Desktop Wiki تص_غير _حول كل اللو_حات معادلات حسابية _إلى الخلف _استعراض _العلل _فرعي _محو التنسيق أغلق اطوِ الكل _المحتويات _انسخ انس_خ إلى هنا _احذف _إحذف الصفحة أنبذ التغييرات _حرر _حرر الوصلة _حرر وصلة أو كائناً... _حرر الخصائص مائل _الأسئلة الشائعة _ملف _إيجاد... _للأمام _ملء الشاشة _إذهب _مساعدة تميي_ز _التاريخ _المنزل _صورة... _استورد صفحة _إدرج _القفز إلى _ارتباطات لوحة المفاتيح _وصلة الربط بتاري_خ _وصلة... مُميَّز _المزيد _أنقل ان_قل إلى هنا _صفحة جديدة... _التالي _لا شيء الحجم ال_عادي _قائمة مرقمة افتح _إفتح دفتراً آخر _آخر... _صفحة التسلسل الهرمي _للصفحة _مستوى أعلى _الصق م_عاينة ال_سابق ال_طباعة إلى مستعرض الإنترنت _دفتر سريع... أنهِ _الصفحات الأخيرة _إعد تعبير _عادي أعد التحميل _إحذف الوصلة استب_دل استب_دال... أعد ضبط الحجم _استعادة النسخة _إحفظ احفظ نسخة التقاط _صورة شاشة _إبحث _إبحث _أرسل إلى... _اللوحات الجانبية _جنباً إلى جنب _فرز السطور مشطوب _عريض _منخفض _مرتفع _قوالب _أدوات _تراجع _حرفي الإ_صدارات... _إعرض تكبي_ر خطوط أفقية بلا خطوط إرشادية للقراءة فقط ثوانِ Launchpad Contributions:
  MaXeR https://launchpad.net/~themaxer
  Sulaiman A. Mustafa https://launchpad.net/~seininn
  Tarig https://launchpad.net/~taritom خطوط رأسية مع السطور 