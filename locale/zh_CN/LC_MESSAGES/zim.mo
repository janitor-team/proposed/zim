��    J     l              �$  ,   �$  .   �$  ?   %     Y%     e%     �%  0   �%     �%     �%     &  	   &     &  i   '&  *   �&     �&     �&     �&     �&  
   '  -   '     :'  V   B'     �'  	   �'  	   �'     �'  3   �'  Y   �'     U(     k(  
   x(     �(     �(     �(     �(     �(     �(     �(  	   �(     �(  $   )  ?   ')  /   g)  (   �)  %   �)     �)  	   �)     *     *  
   *     (*  	   /*     9*     F*     R*     Y*  
   f*     q*     �*     �*     �*     �*     �*  
   �*     �*     �*     �*     +     #+     3+  <   B+     +  	   �+  
   �+     �+     �+     �+     �+     �+     �+     �+     ,     &,  +   7,  3   c,      �,     �,     �,     �,  
   �,     �,     �,     -     :-  3   W-     �-     �-     �-     �-     �-     .     ".     1.     9.     >.     K.     Y.     f.     k.     o.  0   w.     �.     �.     �.     �.  
   �.     �.     �.     �.     /     /     /  $   7/  ~   \/     �/     �/  
   �/     �/  
   	0  	   0  
   0     )0     60     >0     O0     g0     u0  5   }0     �0     �0     �0  !   �0     �0  #   1     ,1     ?1     F1     Y1     w1     �1     �1     �1     �1     �1     �1     �1      2  
   2     2     2  	   02  6   :2     q2  v   x2     �2  *   3  c   ,3     �3     �3     �3     �3     �3     �3     �3  
   �3  
   �3  
   �3  
   4  
   4  
   4     &4     -4     M4  	   d4     n4     4     �4     �4     �4     �4  
   �4     �4     �4     �4     �4     5     5     5     05     ?5     L5     X5     e5     w5     �5     �5     �5  	   �5     �5     �5     �5     �5     �5     6     6     36     86     G6     S6     Y6  2   b6     �6     �6     �6     �6     �6     �6     �6     7     -7     97     L7  	   T7     ^7     g7     m7     �7     �7     �7     �7     �7      �7  *   �7      8     )8     28     C8     S8     X8     n8     �8  2   �8     �8     �8     �8     9  	   9     #9     &9     ?9     K9     Y9     f9     z9  
   �9     �9  	   �9     �9     �9     �9     �9     :     :     :     3:     <:  9   H:     �:  I   �:  !   �:  '   �:  	   $;     .;     7;  0   <;  	   m;     w;     �;  	   �;  '   �;  V   �;  3   <  0   O<     �<     �<  .   �<     �<     �<     �<     �<     =     =     =     #=     +=  
   7=  &   B=  
   i=     t=     �=     �=     �=     �=     �=  
   �=     �=  
   �=     >     >     &>     3>     @>     _>     c>     i>     y>     �>     �>     �>     �>  	   �>     �>     �>     �>     �>     ?     ?     ?     2?     F?     N?     Z?     h?  �   u?     @      @     ?@     Z@  	   r@     |@     �@     �@     �@     �@     �@     �@     �@  2   	A     <A  &   JA     qA     �A     �A     �A     �A  5   �A  &   	B     0B     =B     BB  &   QB     xB     �B     �B     �B     �B     �B  
   �B     �B     �B  	   �B     �B     C     C     !C     &C     DC  	   IC     SC  	   \C     fC  
   kC     vC     �C     �C  ?   �C  A   �C  �  7D  S   �E  =   OF  K   �F  @   �F  6   G  -   QG  I   G  x   �G  �   BH  �   I  �   �I  �   J  }   �J  L   K  �   jK  �   �K  �   �L  }   +M  C   �M  h   �M  k   VN  �   �N  R   �O  ;   �O  =   %P  v   cP    �P  j   �Q  n   YR  ]   �R     &S  �   �S  7   ;T     sT  �   yT  |   U     �U     �U     �U     �U     �U     �U     �U  	   �U  '   �U     V     V  D   1V     vV     ~V     �V     �V     �V  H   �V     �V     W     +W  !   :W     \W     nW  P   �W     �W  U   �W     9X     BX  	   RX  
   \X     gX  N   lX     �X     �X     �X  �   �X  
   �Y     �Y     �Y     �Y  	   �Y  ^   �Y  f   #Z     �Z  	   �Z     �Z     �Z  
   �Z     �Z     �Z     �Z     �Z     �Z  	   �Z     �Z     �Z     [     [  	   [     &[  
   ,[     7[     ?[     L[     ][     m[  
   s[     ~[     �[  	   �[     �[     �[     �[     �[     �[     �[     �[     �[  
   �[     �[     �[  	   \     \     \     &\     ,\     8\     E\     K\     Y\     b\     h\     n\  
   t\     \     �\     �\     �\     �\     �\     �\  	   �\     �\     �\     �\     �\     ]  	   ]     ]     (]     7]     =]     K]     Q]     e]     m]     u]     �]     �]     �]     �]     �]     �]  
   �]     �]     �]  
   �]     �]      ^     ^     ^     &^     .^  
   6^     A^  
   N^     Y^     `^  	   f^     p^     }^     �^     �^     �^     �^     �^     �^     �^     �^  
   �^  �  �^      �`  ,   �`  K   (a     ta     �a  %   �a     �a     �a  $   �a     b     b  
   2b     =b     Ob     ob     �b     �b     �b  	   �b  !   �b  	   �b  ,   �b     c     #c     0c     =c  0   Pc  4   �c     �c     �c     �c     �c     �c     d     #d     3d     :d     Ad     Hd     Vd     cd  '   }d  &   �d     �d  '   �d     e     #e     0e     Ce     Je     Ze     ae  	   he     re     e     �e  
   �e     �e     �e     �e     �e     �e     �e  	   �e     �e     f     *f     >f     Sf     df  H   tf     �f  	   �f  	   �f  	   �f     �f     �f     �f     g     g     2g     Fg     Yg  )   fg  /   �g     �g     �g     �g     h     h     'h      8h     Yh     uh  0   �h     �h     �h     �h     �h     i  $   )i     Ni     ai  
   ni     yi     �i     �i     �i     �i     �i  '   �i     �i     �i     �i     j  	   j     !j     (j     5j     <j     Ij     Yj     oj  �   �j     k     k  
   #k     .k     Dk     Qk     ^k     kk     |k     �k     �k     �k  	   �k  E   �k     l     l     0l  !   7l     Yl  '   fl     �l     �l     �l     �l     �l     �l      �l     m     ,m     Am  	   ^m  	   hm     rm     ym     �m     �m     �m  6   �m  	   �m  l   �m     cn  $   }n  c   �n  
   o     o     o     o     8o     Qo     Uo  	   ]o     go     xo     �o     �o     �o     �o     �o     �o     �o     �o     p     p     p     (p     8p  	   ?p     Ip     Yp     ip     p     �p     �p     �p     �p     �p     �p     �p     �p     q     q     ,q     9q     Oq     Vq     lq  	   sq     }q     �q     �q     �q  	   �q     �q  	   �q     �q  	   �q  6   �q  	   5r     ?r     Fr     dr     kr     �r     �r     �r     �r     �r  	   �r  	   �r     s     s     s     +s     Gs     Ws     gs     ws      ~s  -   �s     �s     �s     �s     �s  	   	t     t     )t     Bt  2   Rt     �t     �t     �t  	   �t  	   �t     �t     �t     �t     u     u     #u     =u     Wu     hu     uu     �u     �u     �u     �u     �u  	   �u     �u  	   	v     v  ?    v     `v  Q   pv  $   �v  !   �v     	w     w     w  3   %w     Yw  	   fw     pw     }w  !   �w  O   �w  '   �w  "   x     Ax     Zx     ax     �x     �x     �x     �x  	   �x  	   �x     �x     �x  
   �x     �x  /   �x      y     -y     =y     Jy     Zy     wy     �y     �y  	   �y  	   �y     �y     �y     �y  	   �y     �y     z  	   !z     +z     8z     Hz     Xz     hz     yz     �z     �z     �z     �z     �z     �z     �z     �z     �z     {     {     {     +{  R   8{     �{     �{     �{     �{     �{  	   �{     	|     |     /|     B|     R|     n|     ~|  '   �|     �|  $   �|     �|     }     }     +}     A}  '   Q}     y}     �}     �}     �}     �}     �}     �}     ~     ~      ~     '~  
   >~     I~     P~  	   ]~     g~     k~     {~     �~     �~     �~     �~     �~     �~     �~     �~     �~     �~       5     ;   N  �  �  ?   �  +   ^�  N   ��  $   ف     ��     �  9   9�  \   s�  �   Ђ  �   ��  �   9�  �   ʄ  j   [�  K   ƅ  �   �  �   ��  v   3�  Y   ��  4   �  1   9�  Y   k�  �   ň  @   ��  7   ˉ  5   �  V   9�  �   ��  n   ��  D   ��  7   C�  y   {�  �   ��  *   ��       T   ɍ  m   �     ��  
   ��     ��     ��     ��     Վ     �     ��  '   �     9�     F�  T   \�     ��     ��     ԏ  	   �     �  %   ��     !�     7�     D�     Q�     i�     �  T   ��     �  [   ��     P�     W�     n�     �     ��  3   ��     ő     ֑     ݑ  �   �     ��     ��     ��     Ȓ     ̒  N   Ӓ  [   "�     ~�  
   ��  
   ��  
   ��     ��  
   ��  
   ̓  
   ד     �  
   �     ��     �     �  
   +�     6�  
   M�  
   X�     c�  
   w�     ��     ��     ��  
   ��     ��     Δ     �  
   ��     �  
   �  
    �     +�  
   9�  
   D�  
   O�  
   Z�     e�  
   v�  
   ��     ��     ��  
   ��  
   ��     ��     ҕ  
   ��     �     ��     �  
   �  
   &�     1�     E�     V�     d�     l�     }�     ��     ��     ��  
   ��     ̖     ݖ  
   �  
   ��     �     �     &�  
   :�     E�  
   V�     a�     u�  
   ��     ��     ��  
   ��     ��     ɗ     ڗ  
   �     ��     �  
   �     &�     4�     E�     S�  	   `�     j�  
   x�  
   ��  
   ��  
   ��  
   ��  
   ��  
   ��     Ř  
   ֘  
   �     �  	   �     �     �     &�  `  *�     ��  	   ��   		This plugin provides bar for bookmarks.
		 %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i Attachment %i Attachments %i errors occurred, see log %i file will be deleted %i files will be deleted %i open item %i open items %i warnings occurred, see log <Top> <Unknown> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Add Application Add Bookmark Add Notebook Add bookmark/Show settings Add column Add new bookmarks to the beginning of the bar Add row Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files All Tasks Allow public access Always use last cursor position when opening a page An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Arithmetic Ascii graph (Ditaa) Attach File Attach image first Attachment Browser Attachments Attachments: Author Auto
Wrap Auto indenting Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave version on regular intervals Back to Original Name BackLinks BackLinks Pane Backend Backlinks: Bazaar Bookmarks BookmarksBar Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Capture whole screen Center Change columns Changes Characters Characters excluding spaces Check Checkbox '>' Check Checkbox 'V' Check Checkbox 'X' Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Clone row Code Block Column 1 Command Command does not modify data Comment Common include footer Common include header Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Created Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Demote Dependencies Description Details Discard note? Distraction Free Editing Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root Due E_xport... Edit Custom Tool Edit Image Edit Link Edit Table Edit _Source Editing Editing file: %s Enable Version Control? Enable plugin Enabled Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Fossil Get more plugins online Get more templates online Git Gnuplot Grid lines Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide menubar in fullscreen mode Highlight current line Home Page Horizontal _Line Icon Images Import Page Include subpages Index Index page Inline Calculator Insert Code Block Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Sequence Diagram Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Labels marking tasks Last Modified Leave link to new page Left Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Managing table columns Map document root to URL Match _case Maximum page width Menubar Mercurial Modified Month Move Selected Text... Move Text to Other Page Move column ahead Move column backward Move text to Name Need output file to export MHTML Need output folder to export full notebook New File New Page New S_ub Page... New _Attachment Next No Applications Found No changes since last version No dependencies No plugin available to display objects of type: %s No such file: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open cell content link Open help Open in New Window Open in New _Window Open new page Open plugins folder Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Name Page Template Page section Paragraph Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please select a row, before you push the button. Please specify a notebook Plugin Plugin "%s" is required to display this object Plugins Port Position in the window Pr_eferences Preferences Prev Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Remove column Remove row Removing Links Rename page "%s" Replace _All Replace with Restore page to saved version? Rev Right Right Side Pane Right margin position Row down Row up S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Screenshot Command Search Search _Backlinks... Search this section Section Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Sequence Diagram Server not started Server started Server stopped Set New Name Set default text editor Set to Current Page Show Line Numbers Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show full Page Name Show full page name Show helper toolbar Show in the toolbar Show right margin Show the cursor also for pages that can not be edited Show the page title heading in the ToC Single _page Size Smart Home key Some error occurred while running "%s" Sort alphabetically Sort pages by tags Source View Spell Checker Start Start _Web Server Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Tags for non-actionable tasks Task Task List Template Templates Text Text Files Text From _File... Text background color Text foreground color The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The following parameters will be substituted
in the command when it is executed:
<tt>
<b>%f</b> the page source as a temporary file
<b>%d</b> the attachment directory of the current page
<b>%s</b> the real page source file (if any)
<b>%p</b> the page name
<b>%n</b> the notebook location (file or folder)
<b>%D</b> the document root (if any)
<b>%t</b> the selected text or word under cursor
<b>%T</b> the selected text including wiki formatting
</tt>
 The inline calculator plugin was not able
to evaluate the expression at the cursor. The table must consist of at least on row!
 No deletion done. There are no changes in this notebook since the last version that was saved This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This page name cannot be used due to technical limitations of the storage This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin also has properties,
see the notebook properties dialog This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a sequence diagram editor for zim based on seqdiag.
It allows easy editing of sequence diagrams.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This plugin turns one section of the notebook into a journal
with a page per day, week or month.
Also adds a calendar widget to access these pages.
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox '>' Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Tray Icon Turn page name into tags for task items Type Un-check Checkbox Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unkown Image type Unkown Object Unspecified Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use a page for each Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s With this plugin you can embed a 'Table' into the wiki page. Tables will be shown as GTK TreeView widgets.
Exporting them to various formats (i.e. HTML/LaTeX) completes the feature set.
 Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Zim Desktop Wiki Zoom _Out _About _Add _All Panes _Arithmetic _Back _Browse _Bugs _Cancel _Checkbox _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Duplicate Line _Edit _Edit Link _Edit Link or Object... _Edit Properties _Emphasis _FAQ _File _Find _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _Move Here _New Page... _Next _None _Normal Size _Numbered List _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove _Remove Line _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In calendar:week_start:0 horizontal lines no grid lines readonly seconds translator-credits vertical lines with lines Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2019-03-29 01:16+0000
Last-Translator: Tian Xie <txie@live.cn>
Language-Team: Simplified Chinese <zh_CN@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2020-01-01 13:40+0000
X-Generator: Launchpad (build bceb5ef013b87ef7aafe0755545ceb689ca7ac60)
 		这个插件显示书签栏
		 %(cmd)s
返回非零的退出状态 %(code)i 出现了 %(n_error)i 个错误和 %(n_warning)i 个警告, 请查看日志 %Y 年 %B 月 %d 日  %A %i 个附件 出现 %i 个错误，请查看日志 %i文件将被删除 %i 个打开的项目 出现 %i 个警告, 请查看日志 <Top> 未知文件名的占位符 桌面wiki %s文件已存在 表格必须至少包含一列. 添加应用程序 添加书签 添加笔记本 添加书签/显示设置 增加列 新书签添加到书签栏最前 添加行 使用 gtkspell 来增加拼写检查功能
 对齐 所有文件 所有任务 允许公开访问 打开页面时光标始终置于上一次位置 生成图像失败。
还是需要保存文件吗？ 附注的页面源代码 应用程序 算术运算 Ascii 图(Ditaa) 附加文件 先将图片添加为附件 附件浏览器 附件 附件 作者 自动
换行 自动缩进 zim 自动保存的版本 当应用格式时自动选择当前词 自动将"CamelCase"词转换为链接 自动将路径转换为链接 以常规时间间隔自动保存版本 改回原名称 反向链接 反向链接窗格 后端 反向链接： Bazaar 书签 书签栏 底部窗格 浏览 项目符号列表 配置(_O) 不能修改页面： %s 取消 截取全屏 居中 更改列名称 改动 字符数 字符数（不计空格） 选中复选框  '>' 选中复选框 'V' 选中复选框  'X' 拼写检查(_S) 可复选列表 经典托盘图标，
不要在 Ubuntu 上使用新风格的状态图标 清除 复制行 代码块 第 1 列 命令 命令不修改数据 备注 Common include footer Common include header 整个笔记本(_N) 配置应用程序 配置插件 配置用于打开“%s”链接的程序 配置用于打开“%s”类型文件的程序 将所有选中项作为任务 复制电子邮件地址 复制模板 拷贝为(_A)... 复制链接(_L) 复制地址(_L) 找不到可执行文件“%s” 无法找到笔记本： %s 无法找到 "%s" 模板 没有找到这个笔记本的文件或文件夹 无法载入拼写检查 不能打开:%s 不能解析表达式 无法读取：%s 无法保存页面： %s 为每个笔记创建一个新页面 创建目录吗？ 创建日期 剪切(_T) 自定义工具 自定义_工具 自定义... 日期 天 默认 复制到剪贴板文本的默认格式 默认记事本 延迟 删除页面 确定删除"%s"? 删除行 降级 依赖关系 说明 详细信息 丢弃笔记？ 免打扰编辑模式 确定删除所有书签? 将要恢复此页面：%(page)s
它来源于此版本：%(version)s
你确定要这样吗？
保存的所有改变都会丢失！ 文档根目录 截止 导出(_X) 编辑自定义工具 编辑图像 编辑链接 编辑表格 编辑源码(_S) 编辑 正在编辑文件:%s 启用版本控制？ 启用插件 已启用 文件 %(file)s 的第%(line)i 行, 在 "%(snippet)s" 附近有错误 执行计算 展开所有(_A)内容 导出 所有页面导出至单个文件 导出成功 每个页面分别导出至一个文件 正在导出笔记本 失败 运行失败：%s 应用程序运行失败: %s 文件已存在 文件模板(_T) 磁盘上的文件已更改：%s 文件已存在 文件不可写：%s 不支持的文件类型: %s 文件名 过滤器 查找 查找下一个(_X) 查找上一个(_V) 查找替换 查找 将任务标记为在周末前的周一或周二到期 文件夹 文件夹已存在且非空，导出到这个文件夹可能导致已存在的文件被覆盖。继续吗？ 文件夹已经存在: %s 包含附件文件模板的文件夹 你可以使用操作符 AND、OR 和 NOT 实现高级搜索功能。详细内容参见帮助页。 格式(_M) 格式 Fossil 联网获取更多插件 联网获取更多模板 Git Gnuplot 网格线 一级标题(_1) 二级标题(_2) 三级标题(_3) 四级标题(_3) 五级标题(_5) 高度 全屏模式中隐藏菜单栏 高亮当前行 首页 水平分割线(_L) 图标 图片 导入页面 包括子页面 目录 索引页 内置计算器 插入代码块 插入日期与时间 插入图形 插入 ditaa 绘图 插入公式 插入 GNU R 绘图 插入Gnuplot绘图 插入图片 插入链接 插入乐谱 插入屏幕截图 插入时序图 插入符号 插入表格 从文件插入文本 界面 内部维基关键词 日志 跳转到 跳转到页面 使用标签标注任务 最后更改 保留到新页面的链接 左对齐 左侧窗格 行排序 行 链接图 用完整文件路径链接文档根目录下的文件 链接到 位置 使用 Zeitgeist 记录事件 日志 看起来你发现了一个Bug 设置默认程序 管理表格的列 把文档根映射为URL 匹配大小写(_C) 最大页面宽度 菜单栏 Mercurial 修改日期 月 移动已选文本... 移动文本到其它页面 向前移动列 向后移动列 移动文本到 名称 需要输出文件以导出MHTML 需要输出文件夹以导出整个笔记本 新建文件 新建页面 新建子页面(_U) 新附件(_A) 下一个 未找到应用程序 没有做出任何修改 无依赖关系 没有插件可用于显示此类型的对象：%s 无此文件：%s 该内部链接 %s 无效 没有安装模板 记事本 记事本 确定 打开附件目录(_F) 打开目录 打开笔记本 打开方式... 打开文档根目录(_D) 打开笔记本目录(_N) 打开页面(_P) 打开链接 打开帮助 在新窗口中打开 在新窗口中打开(_W) 打开新页面 打开插件文件夹 使用 "%s" 打开 可选的 插件 %s 的选项 其它... 输出文件 导出文件已存在, 可以使用"--overwrite"以强制导出 输出文件夹 输出文件夹已存在, 并且非空, 可以使用"--overwrite"以强制导出. 需要指定输出位置才能导出 执行结果会替换当前所选 覆写 路径栏(_A) 页面 页面 "%s" 并没有用于摆放附件的文件夹 页面名称 页模板 页面段落 段落 请为此版本添加一个注释 请注意，链接到一个不存在的页面
将会自动创建一个新页。 请给笔记选择一个名称和目录 点击按钮前请先选择一行. 请指定一个笔记本 插件 显示此对象需要插件"%s" 插件 端口 窗口中的位置 首选项(_E) 首选项 上一个 打印至浏览器 提升 属性(_T) 属性 向 Zeitgeist 守护进程推送事件信息。 快速笔记 快速笔记... 最近更改 最近更改... 最近更改的页面(_C)... 实时格式化 wiki 代码 移除 移除全部 移除列 删除行 正在删除链接 重命名页面 "%s" 替换全部(_A) 替换为 从版本的恢复此页面 版本 右对齐 右侧窗格 右边界位置 向下移动行 向上移动行 保存版本(_A) 另存为(_C) 保存副本 保存版本 保存书签 zim 保存的版本 次数 截图命令 查找 搜索反向链接(_B)... 搜索此部分 章节 选择文件 选择文件夹 选择图片 选择一个或多个版本，用于查看这些版本与当前版本的区别。
 选择导出的格式 选择输出文件或文件夹 选择要导出的页面 选中的窗口或区域 选中区域 时序图 服务器未开启 服务器已启动 服务器已停止 设置新名称 设置默认文本编辑器 设为当前页 显示行号 浮动显示目录（代替侧面板） 显示改动(_C) 为每个笔记显示不同的图标 显示完整页面名称 显示完整页面名称 显示帮助栏 显示在工具栏中 显示右边界 不可编辑的文档仍然显示光标 在目录中显示页面标题 单个页面(_P) 大小 智能Home键 运行"%s"时发生错误 按字母顺序排序 按标签排序页面 Source View 拼写检查器 开始 启动Web服务器(_W) 符号(_m) 语法 系统默认 Tab宽度 表 表格编辑器 目录 标签 尚无法执行的任务标签 任务 任务列表 模板 模板 文本 文本文件 来自文件(_F)... 文本背景颜色 文字的前景色 文件夹
%s
不存在。
您想现在创建它吗？ 文件夹 “%s” 不存在。
您想现在创建它吗？ 在执行命令时，将替换下列参数：
<tt>
<b>%f</b> 作为临时文件的页面地址
<b>%d</b> 当前页的连接目录
<b>%s</b> 真实页面源文件（ 假如有的话 ）
<b>%p</b> 页名
<b>%n</b> 笔记簿的位置（文件或文件夹）
<b>%D</b> 文档根目录 （假如有的话）
<b>%t</b> 光标下选定的文本或单词
<b>%T</b> 包含维基格式的所选定的文本
</tt>
 内置计算器无法计算鼠标指针所处位置的表达式 表格必须至少包含一行.
 未删除. 自从上次保存之后到现在为止，记事本中没有做出任何修改 你可能没有安装合适的词典 这个文件已经存在。 该页面不包含附件目录 由于存储的技术限制，无法使用此页面名称 这个插件可以获取屏幕快照并且插入到页面中。
这是一个核心插件。
 此插件将会用对话框来显示记事本里面所有的开放任务。
开放任务包括所有的复选项和标记有“TODO”和“FIXME”标签的项目。
此插件是 zim 的核心插件。
 此插件会显示一个对话框，可以将文本或剪切板内容快速的粘贴到 zim 页面中。
此插件是随 zim 共同分发的核心插件。
 此插件会增加一个用以快速访问的托盘图标。
这个插件依赖于 GTK+ 2.10以上版本。
此插件是 zim 的核心插件。
 此插件会在窗口中添加一个小部件，
显示链接到当前页面的所有页面列表。

此插件为 zim 自带的核心插件。
 此插件添加一个额外的控件来显示当面页面的目录。
这是 zim 自带的核心插件。
 此插件添加了部分设置以将 zim 作为免打扰编辑器使用。
 该插件将添加“插入符号”对话框并允许
可打印字符进行自动排列。

这是一个由 zim 提供的核心插件。
 此插件为笔记本添加版本控制功能。
此插件支持 Bazaar, Git 及 Mercurial 版本控制系统。
这是 zim 自带的核心插件。
 此插件可在 zim 中嵌入算数计算。
此插件基于
http://pp.com.mx/python/arithmetic
中的算数模块。
 这个插件可以快速帮你确定一个数学表达式。
这是一个核心插件。
 此插件有属性，
查看笔记本属性对话框 此插件为 zim 提供基于 Ditaa 的绘图。
 此插件提供一个基于 GraphViz 的图形编辑器。
这是 zim 的核心插件。
 此插件提供了一个记事本的图形化的链接结构描述对话框。
他能用于类似于“Mind Map”这样的软件来展示页面间的关系。
此插件是 zim 的核心插件。
 该插件依据标签云中的所选标签，生成页面索引
 此插件提供 zim 基于 GNU R 的绘图编辑器。
 该插件提供一个基于Gnuplot的绘图编辑器
 此插件为zim提供一个基于seqdiag的时序图编辑器, 易于编辑时序图.
 此插件给 zim 提供了一个更好打印解决方案。
他可以将当前页导出到 HTML 页面并用浏览器打开。
如果浏览器支持打印，则可以很容易的将页面打印出来。
此插件是随 zim 一同分发的核心插件。
 此插件提供了一个基于 Latex 的公式编辑器。
此插件是随 zim 一同分发的核心插件。
 此插件为 zim 提供一基于 GNU Lilypond 的乐谱编辑器。
 这个插件会在底栏显示附件文件夹的图标
 该插件按字母顺序对选定的行进行排序
如果选定的行已经是有序的，则会逆序
(A-Z 变成 Z-A)
 此插件将笔记本的一个章节设为日志，日志可以每天一页，每周一页， 或者每月一页。可以通过日历控件访问这些页面。
 这通常意味着文件包含无效字符 标题 你可以保存此页或放弃修改。如果保存失败，稍后还可以复原。 你需要选择一个空文件夹来创建新的笔记本.
当然你也可以选择打开已有的笔记本.
 目录 今天(_D) 今天 切换复选框 '>' 切换复选框为“钩” 切换复选框为“叉” 顶部窗格 系统托盘图标 将页面名称作为任务项的标签 文件类型 取消选中复选框 使用“删除键”取消缩进
（如果禁用也可以使用“Shift + Tab”） 未知状态 未知的图片类型 未知的对象 未指定 没有标签 更新从 %i 到本页的页面链接 更新此页的标题 更新链接 更新索引 使用%s切换侧窗格 使用自定义字体 每个一个页面 使用“回车键”跟进链接
（如果禁用也可以使用“Alt + Enter”） 版本控制 当前笔记本没有启用版本控制。
需要在当前笔记本启用版本控制吗？ 版本 查看附加说明(_A) 查看日志(_L) Web 服务器 周 提交错误时请包含以下文本框中的信息 整词查找(_W) 宽度 维基页面：%s 利用此插件可以在页面中插入表格。 表格会显示为GTK TreeView部件。
表格可导出为不同的格式（比如，HTML或LaTeX格式），补完表格功能。
 字数统计 字数统计... 字数 年 昨天 您正在使用外部程序编辑文件，完成后你可以关闭对话框。 你可以设置自定义工具，
它们会出现在菜单、工具栏和右键菜单中。 Zim 桌面维基 缩小(_O) 关于(_A) 添加(_A) 所有窗格(_A) 算术(_A) 后退(_B) 浏览(_B) 报告错误(_B) 取消(_C) 复选框(_C) 子对象(_C) 清除格式(_C) 关闭(_C) 折叠所有(_C)内容 内容(_C) 复制(_C) 复制到此处(_C) 删除(_D) 删除页面(_R) 放弃更改(_D) 复制行(_D) 编辑(_E) 编辑链接(_E) 编辑链接或对象(_E)... _编辑属性 斜体(_E) 常见问题(_F) 文件(_F) 查找(_F) 查找(_F)... 前进(_F) 全屏(_F) 转到(_G) 帮助(_H) 高亮显示(_H) 历史(_H) 主页(_H) 图像(_I)... 导入页面(_I) 插入(_I) 跳转(_J) 跳转到(_J)... 快捷键(_K) 链接(_L) 链接到日期(_L) 链接(_L)... 下划线(_M) 更多(_M) 移动(_M) 移动到此处(_M) 新建页面(_N) 下一个(_N) 无(_N) 正常大小(_N) 顺序(_N)列表 _打开 打开另一个笔记本(_O) 其它(_O)... 页面(_P) 页面(_P)结构 父对象(_P) 粘贴(_P) 预览(_P) 上一个(_P) 打印至浏览器(_P) 快速笔记...(_Q) 退出(_Q) 最近页面(_R) 重做(_R) 正则表达式(_R) 重新载入(_R) 移除(_R) 删除行(_R) 去除链接(_R) 替换(_R) 替换(_R)... 恢复大小(_R) 恢复版本(_R) 保存(_S) 保存副本(_S) 屏幕截图(_S)... 搜索(_S) 搜索(_S)... 发送至...(_S) 侧窗格(_S) 并排比较 排序行 删除线(_S) 粗体(_S) 下标(_S) 上标(_S) 模板(_T) 工具(_T) 撤销(_U) 逐字(_V) 所有版本(_V) 视图(_V) 放大(_Z) calendar:week_start:1 水平线 不使用网格线 只读 秒 Launchpad Contributions:
  DdXPrBgl https://launchpad.net/~cef50854
  Exaos Lee https://launchpad.net/~exaos
  Feng Chao https://launchpad.net/~chaofeng
  H https://launchpad.net/~zheng7fu2
  Harris https://launchpad.net/~huangchengzhi
  Jaap Karssenberg https://launchpad.net/~jaap.karssenberg
  Jianhan Gao https://launchpad.net/~jhgao
  Junnan Wu https://launchpad.net/~mygoobox
  Qianqian Fang https://launchpad.net/~fangq
  Robin Lee https://launchpad.net/~cheeselee
  Saul Thomas https://launchpad.net/~stthomas
  SongL https://launchpad.net/~songl
  Tian Xie https://launchpad.net/~txie
  Tolbkni Kao https://launchpad.net/~tolbkni
  Xhacker Liu https://launchpad.net/~xhacker
  Yang Li https://launchpad.net/~liyang-deepbrain
  Zihao Wang https://launchpad.net/~wzhd
  aosp https://launchpad.net/~aosp
  ben https://launchpad.net/~duyujie
  forget https://launchpad.net/~yangchguo
  hutushen222 https://launchpad.net/~hutushen222
  jo cun https://launchpad.net/~cipher2
  lhquark https://launchpad.net/~lhquark
  piugkni zhang https://launchpad.net/~flamefist-163
  qinghao https://launchpad.net/~qingxianhao
  quake0day https://launchpad.net/~quake0day
  rns https://launchpad.net/~remotenonsense
  sharpevo https://launchpad.net/~sharpevo
  stein https://launchpad.net/~zhoufei715
  太和 https://launchpad.net/~tayhe
  宝刀没开刃 https://launchpad.net/~xkhome 竖线 使用行 