��    �     �              �  .   �     �     �  0   �     $     ?  	   E     O  i   ^     �     �     �     �  V   �  	   P  	   Z     d  3   x  Y   �              
   )      4      @      S      f      r   $   y   ?   �   /   �   (   !  %   7!  	   ]!     g!     v!     ~!  	   �!     �!     �!     �!  
   �!     �!     �!     �!     �!  
   �!     "     "  <    "     ]"     c"     k"     �"     �"     �"     �"     �"     �"  +   �"  3   ##      W#     x#     }#     �#     �#  
   �#     �#     �#  3   �#     $     '$     B$     U$     m$     �$     �$     �$     �$     �$     �$     �$     �$  0   �$     %     %     "%     .%     @%     G%     T%     `%     h%     v%  $   �%  ~   �%     3&  
   A&     L&  
   ]&  	   h&     r&     &     �&     �&     �&     �&     �&     �&     �&     �&     �&     '     %'     1'     D'     ]'     i'     �'     �'     �'     �'  
   �'     �'     �'  	   �'  6   �'     (  v   &(     �(  *   �(  c   �(     >)     F)     M)     Q)  
   Y)  
   d)  
   o)  
   z)  
   �)     �)     �)  	   �)     �)     �)     �)     �)  
   �)     �)     �)     *      *     -*     =*     O*     ^*     k*     w*     �*     �*     �*  	   �*     �*     �*     �*     �*     �*     +     +     -+     <+     H+     N+  2   W+     �+     �+     �+     �+     �+     �+     �+     ,     ,  	   *,     4,     =,     C,     Y,     q,     ~,     �,     �,     �,     �,     �,     �,     �,     �,     -     $-     ;-  	   D-     N-     Q-     j-     v-     �-     �-     �-  
   �-     �-     �-     �-     �-      .     .     .     +.  	   9.     C.     L.  0   Q.  	   �.     �.     �.  	   �.     �.     �.  '   �.  V   �.  3   J/     ~/     �/     �/     �/     �/     �/     �/     �/     �/  
   �/  &   �/  
   0     $0     20     A0     S0     k0     �0  
   �0     �0     �0     �0     �0     �0     �0     �0     
1     1  	   +1     51     B1     Q1     h1     n1     u1     �1     �1     �1  �   �1     A2      Z2     {2     �2  	   �2     �2     �2     �2  2   �2     3  &   *3     Q3  5   e3     �3     �3  &   �3     �3     �3     �3     	4  
   4     &4     54     G4     L4     j4  	   o4     y4  	   �4     �4  
   �4     �4     �4     �4  ?   �4  A   5  S   ]5     �5  K   �5  @   6  6   D6  -   {6  x   �6  �   "7  �   �7  �   p8  �   �8  }   9  L   �9  �   J:  �   �:  �   |;  }   <  h   �<  k   �<  �   ^=  R   2>  ;   �>  =   �>    �>  j   @  n   ~@     �@  7   mA     �A  �   �A     GB     KB     RB     XB     lB     �B  	   �B  '   �B     �B  D   �B     C     C  H   C     _C     C     �C     �C     �C  P   �C     D  U   $D     zD     �D  	   �D  
   �D     �D  N   �D     �D     E     E  
   E     'E     5E     ;E  	   @E  ^   JE  f   �E     F  	   !F     +F  
   2F     =F     IF     XF     ^F     fF     lF     tF     {F     �F     �F  	   �F     �F  
   �F     �F     �F     �F     �F  
   �F     �F     G  	   G     'G     ,G     2G     ;G     DG     PG     TG  
   ZG     eG     nG  	   tG     ~G     �G     �G     �G     �G     �G     �G     �G     �G     �G  
   �G     �G     �G     �G     H     H     H     $H  	   >H     HH     NH     VH     ]H  	   fH     pH     �H     �H     �H     �H     �H     �H     �H     �H     �H     �H     �H     �H     I  
   I     I     .I  
   6I     AI     MI     YI     gI     sI     {I  
   �I     �I  
   �I     �I     �I  	   �I     �I     �I     �I     �I     �I     �I      J     J  �  "J  6    L     7L     CL  .   cL  )   �L     �L     �L     �L  ]   �L     >M     GM     XM     mM  o   }M     �M     �M     N  #   'N  F   KN     �N     �N     �N     �N     �N     �N     �N      O  '   O  C   0O  <   tO  "   �O  3   �O     P     P  	   &P     0P     7P  	   DP     NP     TP  
   dP     oP     �P     �P  	   �P  	   �P  	   �P     �P  N   �P  	   #Q     -Q     5Q  	   NQ     XQ     wQ     �Q     �Q     �Q  $   �Q  *   �Q  /   R     MR     TR     nR     }R     �R     �R     �R  4   �R     S  "   S     3S  !   IS  $   kS     �S     �S     �S     �S  
   �S  	   �S     �S     �S  2   �S     !T     /T     5T     AT     ST     ZT     gT     nT     }T     �T  %   �T  �   �T     _U     kU     sU     �U     �U     �U     �U     �U     �U  	   �U     V     V     -V     1V     EV     RV     gV     �V     �V     �V     �V      �V     W     "W     1W     :W     ?W     NW     ^W  
   mW  L   xW     �W  y   �W     FX     ]X  s   }X  	   �X     �X     Y     Y  
   Y  
   Y  
   #Y  
   .Y  
   9Y     DY  -   LY     zY     �Y     �Y     �Y     �Y     �Y     �Y     �Y     �Y     �Y     Z     Z     4Z     DZ     QZ     `Z     oZ     �Z     �Z     �Z     �Z     �Z     �Z     �Z     [     [     -[     I[     Z[     l[     t[  3   �[     �[     �[  (   �[      \  #   \     1\     K\     e\     z\  	   �\     �\     �\     �\     �\     �\     ]     ]  	   ]     $]     5]     A]  "   O]     r]     �]     �]     �]     �]     �]     �]     �]     ^     ^     ^     0^     B^  
   X^     c^     {^     �^     �^     �^     �^     �^     �^     �^     �^     �^  '   �^  
   #_  	   ._     8_     P_     X_     a_     h_  P   �_      �_     �_  	   �_     `     `  
   `  	   *`     4`     G`     O`     ]`  4   j`     �`     �`     �`     �`     �`  +   �`     (a     /a     =a     Ra     ja     ya  >   �a     �a     �a     �a     �a     b     b     &b     <b     Zb     cb     hb     �b     �b     �b  y   �b     (c      <c     ]c     vc     �c     �c     �c     �c  =   �c     d     +d     Jd  =   cd  
   �d     �d  $   �d     �d  $   �d     e     $e     >e     Je     `e  
   re  /   }e  	   �e     �e     �e  	   �e     �e     �e     �e     f     f  %   (f  0   Nf  U   f     �f  E   �f  ;   !g  .   ]g  $   �g  t   �g  �   &h  �   i  �   �i  �   ?j     �j     Xk  �   �k  �   ul  �   m  �   �m  j   Mn  m   �n  �   &o  a   �o  3   ;p  3   op  �   �p  g   �q  l   �q  �   er  9   �r     #s  �   (s     �s  
   �s  	   �s     �s     t  
   t     't  7   =t     ut  j   |t     �t     �t  Y   u     \u     zu     �u     �u     �u  H   �u     v  P   +v     |v     �v     �v     �v     �v  &   �v  
   �v     �v     �v      w     w     w     $w     *w  h   0w  m   �w     x  
   x     'x     0x     ?x     Nx  	   Zx     dx     kx     qx     yx     �x     �x     �x  
   �x     �x     �x     �x     �x     �x     �x     y     y     4y  	   Gy     Qy  	   fy     py     yy     �y     �y     �y  	   �y  	   �y     �y     �y     �y     �y     �y     �y      z     z  
   #z  	   .z     8z     Az     Jz     [z  	   iz     sz     �z     �z     �z     �z     �z     �z     �z     �z     �z  
   �z     �z     {     {     {     7{     F{     _{     p{     w{     �{  
   �{     �{     �{  	   �{     �{     �{     �{     �{     �{     �{     |     |     *|  	   6|     @|     L|  
   Y|  
   d|     o|     v|     �|     �|  	   �|     �|  	   �|     �|  �   �|     �}   %(cmd)s
returned non-zero exit status %(code)i %A %d %B %Y %i _Backlink %i _Backlinks %i file will be deleted %i files will be deleted %i open item %i open items <Top> <Unknown> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. Action Add Application Add Bookmark Add Notebook Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 All Files All Tasks Allow public access Always use last cursor position when opening a page An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Arithmetic Attach File Attach image first Attachment Browser Attachments Author Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave version on regular intervals BackLinks BackLinks Pane Backend Bazaar Bookmarks Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Capture whole screen Changes Characters Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Command Command does not modify data Comment Common include footer Common include header Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Could not find notebook: %s Could not find the file or folder for this notebook Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Demote Dependencies Description Details Discard note? Distraction Free Editing Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root E_xport... Edit Custom Tool Edit Image Edit Link Edit _Source Editing Editing file: %s Enable Version Control? Enabled Evaluate _Math Expand _All Export Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Git Gnuplot Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide menubar in fullscreen mode Home Page Icon Images Import Page Index Index page Inline Calculator Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Symbol Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Labels marking tasks Last Modified Leave link to new page Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Map document root to URL Match _case Maximum page width Mercurial Modified Month Move Selected Text... Move Text to Other Page Move text to Name New File New Page New S_ub Page... New _Attachment No Applications Found No changes since last version No dependencies No such file: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open in New _Window Open new page Open with "%s" Optional Options for plugin %s Other... Output file Output folder Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Name Page Template Page already exists: %s Paragraph Password Paste Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Plugin Plugins Port Position in the window Pr_eferences Preferences Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Removing Links Rename page "%s" Replace _All Replace with Restore page to saved version? Rev Right Side Pane S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Search Search _Backlinks... Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Server not started Server started Server stopped Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show in the toolbar Show the cursor also for pages that can not be edited Single _page Size Some error occurred while running "%s" Sort alphabetically Sort pages by tags Spell Checker Start _Web Server Sy_mbol... System Default Table of Contents Tags Tags for non-actionable tasks Task Task List Template Templates Text Text Files Text From _File... Text background color Text foreground color The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The inline calculator plugin was not able
to evaluate the expression at the cursor. Theme There are no changes in this notebook since the last version that was saved This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. ToC To_day Today Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Tray Icon Turn page name into tags for task items Type Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use a custom font Use a page for each Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Zim Desktop Wiki Zoom _Out _About _All Panes _Arithmetic _Attachment... _Back _Browse _Bugs _Cancel _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Edit _Edit Link _Edit Link or Object... _Edit Properties _Emphasis _FAQ _File _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _Move Here _New Page... _Next _None _Normal Size _Numbered List _Open _Open Another Notebook... _Other... _Page _Parent _Paste _Preview _Previous _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In calendar:week_start:0 readonly seconds translator-credits vertical lines Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2021-03-28 01:29+0000
Last-Translator: Nikolay Korotkiy <sikmir@gmail.com>
Language-Team: Finnish <https://hosted.weblate.org/projects/zim/master/fi/>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.6-dev
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
 %(cmd)s
keskeytyi ja palautti poistumiskoodin %(code)i %A %d.%m.%Y %i _Paluulinkki %i _Paluulinkit Poistetaan tiedosto %i Poistetaan tiedostot %i %i avoin tehtävä %i avointa tehtävää <Alku> <Tuntematon> Työpöytäwiki Tiedosto <b>"%s"</b> on jo olemassa.
Voit vaihtaa nimeä tai korvata alkuperäisen tiedoston. Toiminto Lisää sovellus Lisää kirjanmerkki Lisää muistio Lisäosa lisää oikoluvun, joka käyttää gtkspell:iä.

Tämä keskeinen lisäosa toimitetaan Zimin ohessa.
 Kaikki tiedostot Kaikki tehtävät Julkaise sivu kaikille Muista kohdistimen sijainti sivulla Tapahtui virhe kuvaa luotaessa.
Haluatko silti tallentaa lähdekoodin? Kommentoitu sivun lähdekoodi Sovellukset Aritmetiikka Liitä tiedosto Liitä kuva ensin Liiteselain Liitteet Tekijä Zimin automaattisesti tallentama versio Valitse kohdistimen alla oleva sana kokonaan muotoilua muutettaessa Muuta "KamelinKyttyrä" -muotoiset yhdyssanat aina linkeiksi Muuta tiedostopolut aina linkeiksi Tallenna automaattisesti säännöllisin väliajoin PaluuLinkit Paluulinkkipalkki Taustaosa Bazaar Kirjanmerkit Alapalkki Selaa Lue_telmamerkit _Asetukset Sivua ei voi muokata: %s Peru Kaappaa koko näyttö Muutokset Merkkejä _Oikoluku _Valintaruksilista Klassinen ilmoitusaluekuvake,
älä käytä uudentyylistä kuvaketta Ubuntussa Tyhjennä Komento Komento ei muuta tietoja Kommentti Yleinen lisäyksen alatunniste Yleinen lisäyksen ylätunniste _Koko muistio Muokkaa sovelluksia Lisäosan asetukset Aseta sovellus avaamaan "%s" -linkit Aseta avaussovellus "%s" -tiedostomuodolle Oleta kaikkien valintaruksien olevan tehtäviä Kopioi Kopioi sähköpostiosoite Kopioi malline Kopioi _muodossa... Kopioi _linkki Kopioi si_jainti Muistiota ei löytynyt: %s Tämän muistion tiedostoa tai kansiota ei löytynyt Ei auennut: %s Lausekkeen jäsennys ei onnistunut Luku epäonnistui: %s Sivun tallennus ei onnistunut: %s Luo uusi sivu kullekin muistilapulle Luo kansio? _Leikkaa Mukautetut työkalut O_mat työkalut Mukauta... Päiväys Päivä Oletus Leikepöydälle kopioitavan tekstin oletusmuotoilu Oletusmuistio Viive Poista sivu Poista sivu "%s"? Alenna Riippuvuudet Kuvaus Yksityiskohdat Hylkää muistilappu? Häiriötön muokkaus Haluatko poistaa kaikki kirjanmerkit? Haluatko palauttaa sivun: %(page)s
tallennettuun versioon: %(version)s ?

Kaikki edellisen tallennuksen jälkeen tehdyt muutokset menetetään! Juurikansio _Vie... Muokkaa omaa työkalua Muokkaa kuvaa Muokkaa linkkiä Muokkaa l_ähdekoodia Muokkaus Muokataan tiedostoa: %s Salli versionhallinta? Päällä _Arvioi yhtälön tuloarvo _Laajenna kaikki Vie Viedään muistiota Epäonnistui Ei käynnistynyt: %s Sovellus ei käynnistynyt: %s Tiedosto on jo olemassa _Tiedostomallineet... Tiedosto on muuttunut: %s Tiedosto on jo olemassa Tiedostoon ei voi kirjoittaa: %s Tiedostomuoto ei tuettu: %s Tiedoston nimi Suodatin Etsi Etsi _seuraava Etsi _edellinen Etsi ja korvaa Etsi mitä Liputa maanantaina tai tiistaina erääntyvät tehtävät ennen viikonloppua Kansio Kansio on jo olemassa ja se sisältää tiedostoja - vienti tähän kansioon saattaa korvata tiedostoja. Haluatko jatkaa? Kansio on olemassa: %s Liitetiedostomallineiden kansio Edistynyt haku; voit käyttää loogisia suhteuttimia kuten
AND, OR, NOT (suom. JA, TAI, EI). Lisätietoa ohjeessa. M_uotoilu Muoto Git Gnuplot Otsikko _1 Otsikko _2 Otsikko _3 Otsikko _4 Otsikko _5 Korkeus Piilota vetovalikkopalkki kokonäyttötilassa Aloitussivu Kuvake Kuvat Tuo sivu Sisällysluettelo Sisällysluettelosivu Välitön laskin Lisää päiväys ja aika Lisää kaavio Lisää Ditaa Lisää yhtälö Lisää GNU R -kuvaaja Lisää Gnuplot Lisää kuva Lisää linkki Lisää nuotit Lisää kuvankaappaus Lisää symboli Lisää tekstiä tiedostosta Käyttöliittymä Wikien välinen avainsana Päiväkirja Hyppää Hyppää sivulle Tehtävien tunnisteet Muokattu viimeksi Jätä linkki uuteen sivuun Vasen sivupalkki Rivien järjestin Rivejä Linkkipuun rakennekartta Sisällytä linkkiin tiedoston polku juurikansiossa Linkin kohde Sijainti Tallenna tapahtumat lokiin Zeitgeist:lla Lokitiedosto Taisit löytää ohjelmasta virheen Aseta oletussovellukseksi Kuvaa juurikansio URL:ksi _Huomioi kirjainkoko Suurin sallittu sivun leveys Mercurial Muokattu Kuukausi Siirrä valittu teksti... Siirrä teksti toiselle sivulle Siirrä teksti kohteeseen Nimi Uusi tiedosto Uusi sivu Uusi A_lisivu... Uusi _liite Ei sovellusta Ei muutosta edellisestä versiosta Ei riippuvuuksia Tiedosto ei ole olemassa: %s Wikiä ei määritelty: %s Mallineita ei ole asennettu Muistio Muistiot Valmis Avaa _liitekansio Avaa kansio Avaa muistio Avaa sovelluksella... Avaa _juurikansio Avaa _muistion kansio Avaa _sivu Avaa uudessa _ikkunassa Avaa uusi sivu Avaa ohjelmalla "%s" Valinnainen Lisäosan %s valinnat Muu... Kohdetiedosto Kohdekansio Korvaa _Polkupalkki Sivu Sivulla "%s" ei ole kansiota liitteille Sivun nimi Sivupohja Sivu on jo olemassa: %s Kappale Salasana Liitä Kommentoi tämä versio Huomaa, että olemattoman sivun linkittäminen
luo uuden sivun linkin kohteeksi. Valitse muistion nimi ja kansio. Lisäosa Lisäosat Portti Sijainti ikkunassa As_etukset Asetukset Tulosta selaimelle Ylennä Ominai_suudet Ominaisuudet Työntää tapahtumat Zeitgeist-palvelin-daemonille. Muistilappu Muistilappu... Uusimmat muutokset Uusimmat muutokset... Viimeksi _muokatut sivut Päivitä wikin muotoilumerkinnät lennossa Poista Poista kaikki Poistetaan linkkejä Muuta sivun "%s" nimeä Korvaa k_aikki Korvaava teksti Palautetaanko tallennettu versio käyttöön nykyisen tilalle? Versio Oikea sivupalkki _Tallenna versio... Tallenna _kopio… Tallenna kopio Tallenna versio Tallenna kirjanmerkit Versiotallenne Zim-muistiosta Arvosana Etsi Etsi _paluulinkeistä... Valitse tiedosto Valitse kansio Valitse kuva Valitse yksi versio nähdäksesi muutokset sen ja nykyisen välillä 
tai monta nähdäksesi muutokset niiden välillä.
 Valitse vientimuoto Valitse kohdetiedosto tai kansio Valitse vietävät sivut Valitse ikkuna tai alue Valinta Palvelin ei käynnissä Palvelin käynnistetty Palvelin pysäytetty Näytä sisällysluettelo omassa ikkunassaan, ei sivupalkissa Näytä _muutokset Oma kuvake kullekin muistiolle Näytä työkalupalkissa Näytä kohdistin myös niillä sivuilla joita ei voi muokata _Yksi sivu Koko Tapahtui jokin virhe ajettaessa "%s" Järjestä aakkosjärjestykseen Järjestä sivut tunnisteiden mukaan Oikoluku Käynnistä _WWW-palvelin Sy_mboli... Järjestelmän oletus Sisällysluettelo Tunnisteet Tunnisteet tehtäville, joita ei voi tehdä nyt Tehtävä Tehtävälista Malline Mallineet Teksti Tekstitiedostot _Teksti tiedostosta... Tekstin taustaväri Tekstin väri Kansiota
%s
ei ole.
Luodaanko se nyt? Kansiota "%s" ei ole olemassa.
Luodaanko se nyt? Välitön laskin -lisäosa ei pystynyt arvioimaan kohdistimen alla olevaa yhtälöä. Teema Ei uusia muutoksia muistiossa edellisen version tallennuksen jälkeen Syynä saattaa olla, että kielen sanastoa ei ole asennettu Tiedosto on jo olemassa.
Haluatko korvata sen? Tällä sivulla ei ole liitekansiota Lisäosa sallii kuvankaappauksen lisäämisen suoraan sivulle.

Tämä keskeinen lisäosa toimitetaan Zimin ohessa.
 Lisäosa näyttää muistion kaikki avoimet tehtävät. 
Avoimet tehtävät voivat olla joko avoimia valintarukseja tai työvaiheita, jotka on merkitty tunnisteilla kuten "TEE" tai "KORJAA".

Tämä keskeinen lisäosa toimitetaan Zimin ohessa.
 Lisäosa antaa ikkunan tekstin tuomiseen Zimiin raahaten tai leikepöydältä liittäen.

Tämä keskeinen lisäosa toimitetaan Zimin ohessa.
 Lisäosa lisää kuvakkeen ilmoitusalueelle.

Riippuu paketista: Gtk+ versio 2.10 tai uudempi.

Tämä keskeinen lisäosa toimitetaan Zimin ohessa.
 Lisäosa lisää ikkunaelementin, joka listaa ne sivut, joilta on linkki auki olevaan sivuun.

Tämä keskeinen lisäosa toimitetaan aina Zimin ohessa.
 Lisäosa näyttää auki olevan sivun sisällysluettelon ikkunaoliossaan.

Tämä keskeinen lisäosa toimitetaan Zimin ohessa.
 Lisäosa antaa asetuksia, joiden avulla Zimistä saa piilotettua
keskittymistä haittaavia, huomiota herättäviä piirteitä.
 Lisäosa lisää "Lisää symboli" -valinnan ja sallii typografisten merkkien automaattisen muotoilun.

Tämä keskeinen lisäosa toimitetaan Zimin ohessa.
 Lisäosa tuo muistioihin versionhallinnan.

Lisäosa tukee Bazaar, Git ja Mercurial -versionhallintajärjestelmiä.

Tämä keskeinen lisäosa toimitetaan Zimin ohessa.
 Lisäosa sallii aritmeettisten laskujen sisällyttämisen Zimiin.
Se perustuu aritmeettiseen moduliin lähteestä
http://pp.com.mx/python/arithmetic .
 Lisäosa sallii yksinkertaisten matemaattisten yhtälöiden arvioinnin välittömästi sivulla.

Tämä keskeinen lisäosa toimitetaan Zimin ohessa.
 Lisäosa on Ditaa:han perustuva kaaviomuokkain.

Tämä keskeinen lisäosa toimitetaan aina Zimin ohessa.
 Lisäosa on GraphViz:iin perustuva kaaviomuokkain.

Tämä keskeinen lisäosa toimitetaan aina Zimin ohessa.
 Lisäosa esittää muistion linkkipuun rakenteen graafisesti. Havainnollista kuviota voi käyttää vaikkapa miellekarttana.

Tämä keskeinen lisäosa toimitetaan Zimin ohessa.
 Lisäosa sallii sisällysluettelon suodattamisen pilvestä valittavien tunnisteiden perusteella.
 Lisäosa on GNU R:ään perustuva kuvaajamuokkain.
 Lisäosa on Gnuplot:iin perustuva kuvaajamuokkain.
 Lisäosa kiertää tulostintuen puutteen Zimissä. Se vie sivun html-muodossa ja avaa sen selaimella. Jos selaimessa on tulostintuki, lisäosa mahdollistaa tulostamisen sitä kautta.

Tämä keskeinen lisäosa toimitetaan Zimin ohessa.
 Lisäosa on LaTeX:iin perustuva yhtälömuokkain.

Tämä keskeinen lisäosa toimitetaan Zimin ohessa.
 Lisäosa on GNU Lilypond:iin perustuva nuottimuokkain.

Tämä keskeinen lisäosa toimitetaan Zimin ohessa.
 Lisäosa järjestää valitut rivit aakkosjärjestykseen.
Jos lista on jo järjestetty, järjestys käännetään.
(A-Ö -> Ö-A).
 Yleensä tämän syynä on kelvottomat merkit tiedostossa Nimi Jatkaaksesi voit tallentaa kopion tästä sivusta ja hylätä 
muutokset, tai vain hylätä muutokset. 
Jos tallennat kopion, voit palauttaa sen myöhemmin. Sisällysluettelo _Tänään Tänään Vaihda valintaruksi 'V' Vaihda valintaruksi 'X' Yläpalkki Ilmoitusalueen kuvake Luo sivun nimestä tunnisteita tehtävien työvaiheille Tyyppi Vähennä sisennystä näppäimellä <BackSpace>
(Jos ei käytössä, paina <Shift><Tab> vähentääksesi) Ei tiedossa Ei tunnisteita Päivitä sivu %i , jolla on linkki tähän Päivitä sivut %i , joilla on linkki tähän Päivitä otsikko vastaavaksi Päivitetään linkkejä Luetteloa päivitetään Käytä omaa kirjasinta Sivu kullekin Paina <Enter> avataksesi linkin
(Jos ei toiminnassa, paina <Alt><Enter>) Versionhallinta Versionhallinta ei ole päällä tässä muistiossa.
Kytketäänkö se päälle? Versiot Näytä _kommentit Näytä _loki WWW-palvelin Viikko Liitä alla oleva tieto bugiraporttiin K_oko sana Leveys Wiki-sivu: %s Sanamäärä Sanamäärä... Sanoja Vuosi Eilen Käytät ulkoista sovellusta tiedoston muokkaamiseen. Voit sulkea tämän valintaikkunan kun olet valmis Voit määrittää omia työkaluja, jotka näkyvät
työkaluvalikossa ja työkalurivissä tai pikavalikoissa. Zim työpöytä-wiki _Pienennä _Tietoja _Kaikki palkit _Aritmeettinen Liitteet… _Takaisin _Selaa _Viat Peruuta _Alisivu _Tyhjennä muotoilu _Sulje _Supista kaikki _Sisältö _Kopioi _Kopioi tähän P_oista _Poista sivu _Hylkää muutokset _Muokkaa _Muokkaa linkkiä _Muokkaa linkkiä tai oliota... _Muokkaa asetuksia K_ursiivi _FAQ (usein kysytyt) _Tiedosto _Etsi… _Eteenpäin _Koko näyttö _Siirry _Ohje _Korostus _Historia _Aloitus _Kuva… Tuo _sivu… _Lisää _Mene kohtaan... _Pikanäppäimet _Linkki _Linkki päivämäärään _Linkki… _Korostus _Lisää _Siirrä _Siirrä tähän _Uusi sivu... _Seuraava _Ei mitään P_alauta koko _Numeroitu lista _Avaa _Avaa toinen muistio... _Muu... _Sivu _Ylisivu L_iitä _Esikatsele _Edellinen _Tulosta selaimelle _Muistilappu... _Lopeta _Viimeksi käydyt sivut _Tee uudelleen Säännöllinen _lauseke La_taa uudelleen Poista _Poista linkki _Korvaa Ko_rvaa… _Palauta koko _Palauta versio _Tallenna _Tallenna kopio _Kuvankaappaus... _Etsi _Etsi... _Lähetä... _Sivupalkit _Vierekkäin _Järjestä rivit _Yliviivaus _Voimakas _Alaindeksi _Yläindeksi _Mallineet T_yökalut K_umoa _Tasalevyinen _Versiot... _Näytä _Suurenna calendar:week_start:1 vain luku sekuntia Launchpad Contributions:
  Harri K. Hiltunen https://launchpad.net/~harri-k-hiltunen
  Juhana Uuttu https://launchpad.net/~rexroom
  Matti Pulkkinen https://launchpad.net/~matti-pulkkinen3
  Torsti Schulz https://launchpad.net/~torsti-schulz pystysuuntaiset viivat 