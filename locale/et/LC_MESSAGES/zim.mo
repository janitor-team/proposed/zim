��    6     �	              |     }     �     �     �  V   �  	     Y   !     {     �     �     �     �  $   �  ?   �  /   /  (   _  %   �  
   �     �     �     �  
   �     �  <   	     F     N     k     s     �      �     �  
   �     �  3   �     &     9     T     l     �     �     �     �     �     �     �     �     �     �     �     
       ~        �  
   �     �  
   �  	   �     �     �     �               )     0     C     J     V     ]  
   b     m     |  	   �     �  v   �  c        y     �  
   �  
   �  
   �  
   �  
   �     �  	   �     �     �     �  
   �     �               )     9     K     X     d     v     �     �     �     �     �     �  2   �                          ;     T     `     e     n          �     �     �  	   �     �     �     �     �               (     >     R     a     w     �     �     �     �  0   �  	   �  	   �  '   �  V     3   l     �     �     �     �     �     �     �  
   �  
   �                .     =     N     [     h     �     �     �  	   �     �     �     �     �     �     �        �         �       �      �      �   	   !     !     0!     ?!     N!  &   \!     �!  5   �!     �!  &   �!     "     "  
   !"     ,"     1"  	   6"     @"     I"  
   N"     Y"  S   l"  K   �"  6   #  -   C#  x   q#  �   �#  �   �$  �   8%  �   �%  }   P&  k   �&  �   :'  ;   (    J(  j   ^)     �)  �   �)     k*     r*     �*  	   �*  D   �*  H   �*     2+     R+     a+     p+  P   �+     �+  U   �+     9,     B,  	   R,     \,     h,  
   n,     y,     �,  ^   �,  f   �,     S-     d-     k-     q-     w-     ~-     �-  	   �-     �-     �-     �-     �-     �-  
   �-     �-  	   �-      .     .     .     .     .     ).     -.  
   3.     >.     G.  	   M.     W.     g.     o.     {.     �.     �.     �.     �.     �.     �.     �.     �.     �.     �.  	   �.     �.     �.     /     	/  	   /     /     ./     =/     C/     Q/     W/     k/     s/     �/     �/     �/     �/     �/  
   �/     �/     �/  
   �/     �/     �/     �/     0  
   0     0     '0     .0  	   40     >0     K0     Q0     Z0     b0  �  u0     `2      l2     �2     �2  V   �2     3  Y   3  "   l3     �3     �3     �3     �3  (   �3  9   �3  /   '4  /   W4  /   �4  	   �4  $   �4     �4  
   �4     5     5  <   "5     _5     e5  
   5     �5     �5      �5     �5     �5     �5  +   6     =6     N6  '   i6  "   �6     �6     �6     �6     �6     �6  	   7     7     !7     *7     <7     N7  	   Z7     d7  ~   m7     �7      8     8     *8     68     B8     T8      e8     �8     �8     �8     �8     �8     �8     �8     �8     �8     �8     �8  	   9     9  �   9  b   �9     :     :     $:     0:     <:     H:     T:     `:     h:     v:     |:     �:     �:     �:     �:     �:  
   �:     �:  	   
;     ;     #;     7;     D;     W;     `;     s;     �;  
   �;  9   �;     �;     �;     �;  #   �;  #   <     4<     G<     L<     Z<  2   p<     �<     �<     �<  
   �<     �<     �<  	   �<     �<     =     =     5=     J=     Z=     i=     �=     �=     �=     �=  	   �=  0   �=     �=     �=  "   >  V   (>  ,   >     �>     �>     �>     �>  
   �>     �>      ?  
   ?     ?     $?  )   4?     ^?     o?     �?     �?  =   �?     �?     �?     �?     @     @     (@     D@     L@     S@  	   j@  
   t@  �   @     A     )A  (   GA     pA     �A     �A     �A     �A     �A  &   �A     B  -   B     LB  &   ^B     �B     �B     �B     �B  	   �B     �B     �B     �B     �B     �B  S   C  L   XC  6   �C  &   �C  x   D  �   |D  �   EE  �   �E  �   XF  }   �F  k   `G  �   �G  ;   �H    �H  j   �I     [J  �   dJ      K     K     5K  	   MK  D   WK  n   �K     L     $L     8L     NL  P   fL     �L  U   �L  
   M     (M  
   9M     DM     QM     WM     eM     vM  j   }M  f   �M     ON     bN     iN  
   qN     |N     �N     �N     �N     �N     �N     �N     �N  
   �N     �N  !   �N     O     *O     /O     5O     >O     EO     RO     XO     ]O     kO     tO     zO     �O  	   �O     �O     �O     �O     �O  	   �O     �O     �O     �O  
   P     P     #P     (P     DP  
   LP     WP     gP  	   oP     yP     �P     �P     �P     �P     �P     �P     �P     �P     Q  
   
Q     Q     ,Q  	   =Q     GQ     XQ     hQ  
   pQ  	   {Q     �Q     �Q     �Q  
   �Q     �Q     �Q     �Q     �Q     �Q     �Q      R     R  �   R   %A %d %B %Y %i open item %i open items A desktop wiki Add Notebook Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 All Files An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Attach File Attach image first Attachment Browser Author Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave version on regular intervals C_onfigure Can not modify page: %s Capture whole screen Changes Characters Check _spelling Classic trayicon,
do not use new style status icon on Ubuntu Command Command does not modify data Comment Complete _notebook Configure Plugin Consider all checkboxes as tasks Copy Email Address Copy _Link Could not find notebook: %s Could not find the file or folder for this notebook Could not open: %s Could not parse expression Could not save page: %s Create a new page for each note Create folder? Cu_t Custom Tools Custom _Tools Date Default Default notebook Delay Delete Page Delete page "%s"? Dependencies Description Details Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root E_xport... Edit Custom Tool Edit Image Edit Link Edit _Source Editing file: %s Enable Version Control? Enabled Evaluate _Math Export Exporting notebook Failed File exists Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Home Page Icon Images Import Page Index page Inline Calculator Insert Date and Time Insert Diagram Insert Equation Insert GNU R Plot Insert Image Insert Link Insert Screenshot Insert Symbol Insert Text From File Jump to Jump to Page Labels marking tasks Lines Link Map Link files under document root with full file path Link to Location Log file Looks like you found a bug Map document root to URL Match _case Name New Page New S_ub Page... No changes since last version No dependencies No such file: %s Notebook Notebooks OK Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open in New _Window Open with "%s" Options for plugin %s Other... Output file Output folder P_athbar Page Page "%s" does not have a folder for attachments Page Name Paragraph Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Plugin Plugins Port Pr_eferences Preferences Print to Browser Proper_ties Properties Quick Note Quick Note... Reformat wiki markup on the fly Removing Links Rename page "%s" Replace _All Replace with Restore page to saved version? Rev S_ave Version... Save A _Copy... Save Copy Save Version Saved version from zim Score Search Search _Backlinks... Select File Select Folder Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Server not started Server started Server stopped Show _Changes Show a separate icon for each notebook Show in the toolbar Show the cursor also for pages that can not be edited Single _page Some error occurred while running "%s" Spell Checker Start _Web Server Sy_mbol... Tags Task Task List Template Text Text Files Text From _File... The inline calculator plugin was not able
to evaluate the expression at the cursor. There are no changes in this notebook since the last version that was saved This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To_day Toggle Checkbox 'V' Toggle Checkbox 'X' Tray Icon Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use a custom font Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Whole _word Width Word Count Word Count... Words You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Zim Desktop Wiki _About _Back _Bugs _Child _Clear Formatting _Close _Contents _Copy _Delete _Delete Page _Discard Changes _Edit _Edit Link _Edit Link or Object... _Emphasis _FAQ _File _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _New Page... _Next _None _Open _Open Another Notebook... _Other... _Page _Parent _Paste _Preview _Previous _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side by Side _Strike _Strong _Subscript _Superscript _Tools _Undo _Verbatim _Versions... _View readonly seconds translator-credits Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2012-10-06 21:09+0000
Last-Translator: Jaap Karssenberg <jaap.karssenberg@gmail.com>
Language-Team: Estonian <et@li.org>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
X-Generator: Launchpad (build bceb5ef013b87ef7aafe0755545ceb689ca7ac60)
 %A %d %B %Y %i avatud kirje %i avatud kirjet Töölaua wiki Lisa uus märkmik Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Kõik failid An error occurred while generating the image.
Do you want to save the source text anyway? Märgistatud lehekülje lähtekood Manusta fail Manusta esmalt pilt Manusesirvija Autor Automaatselt salvestatud Zimi versioonid Vormingu lisamisel märgista automaatselt käesolev sõna Automatically turn "CamelCase" words into links Teisenda faili asukohatee automaatselt viidaks. Salvesta versioon regulaarse ajavahemiku järel _Seadista Lehekülje %s muutmine ebaõnnestus. Kaasa terve ekraan. Muudatused Tähed Kontrolli _õigekirja Classic trayicon,
do not use new style status icon on Ubuntu Käsk Käsklus ei muuda andmeid Kommentaar Terve _märkmik Seadista lisandmoodulit Consider all checkboxes as tasks Kopeeri e-posti aadress Kopeeri _link Ei leitud märkmiku: %s Selle märkmiku faili või kausta ei leitud Ei saa avada: %s Avaldise sõelumine nurjus Lehekülje %s salvestamine ebaõnnestus Lisa iga märkme jaoks eraldi leht Loo uus kaust? _Lõika Kohandatud tööriistad Kohandatud _Tööriistad Kuupäev Vaikimisi Aktiivne märkmik Viivitus Kustuta lehekülg Delete page "%s"? Sõltuvused Kirjeldus Detailid Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Dokumendi juurkaust _Ekspordi... Muuda kohandatud tööriista Muuda pilti Muuda linki Muuda lähtekoodi Muudan faili: %s Lülitan versioonihalduse sisse? Lubatud Hinda _Avaldist Ekspordi Märkmiku eksportimine Nurjus Fail on olemas Filter Otsi Otsi _järgmine Otsi _eelmine Otsi ja asenda Otsi mida Kaust Kataloog on juba olemas ja sisaldab faile. Eksportimine antud kataloogi võib olemasolevad failid üle kirjutada. Oled kindel, et soovid jätkata? For advanced search you can use operators like
AND, OR and NOT. Vaata lähemalt abilehekülgedelt. Vor_ming Vorming Pealkiri _1 Pealkiri _2 Pealkiri _3 Pealkiri _4 Pealkiri _5 Kõrgus Kodulehekülg Ikoon Pildid Impordi lehekülg Indekslehekülg Tekstisisene kalkulaator Kuupäeva ja kellaaja lisamine Lisa Diagramm Lisa valem Lisa GNU R diagramm Lisa pilt Viite lisamine Lisa ekraanitõmmis Lisa sümbol Lisa tekst failist Liigu... Liigu leheküljele Labels marking tasks Read Viidaloend Lingi dokumendi juurkaustas asuvad failid täisfailiteena Viita... Asukoht Logifail Tundub, et Sa leidsid programmivea. Vastenda dokumendi juurkaust URLiks Erista _Suurtähti Nimi Uus Lehekülg Uus A_lamlehekülg... Eelmise versiooniga võrreldes muudatused puuduvad Sõltuvusi pole Sellist faili pole: %s Märkmik Märkmikud OK Ava _Manuste kaust Ava kaust Ava märkmik Ava kasutades... Ava _Dokumendi juurkaust Ava _Märkmiku kaust Ava uues _Aknas Open with "%s" Lisandmooduli %s suvandid Muu... Väljundfail Väljundkaust F_ailitee riba Lehekülg Page "%s" does not have a folder for attachments Lehekülje nimi Lõik Sisesta kommentaar versiooni kohta Please note that linking to a non-existing page
also creates a new page automatically. Vali palun märkmikule pealkiri ja kataloog. Lisandmoodul Lisandmoodulid Port _Eelistused Eelistused Trüki veebilehitseja kaudu A_tribuudid Atribuudid Kiire märge Kiire märge... Vorminda wiki märgistus jooksvalt ümber Eemaldan viiteid Rename page "%s" Asenda _kõik Asenda millega Kas soovid lehekülje salvestatud versiooni põhjal taastada? Rev Sa_lvesta versioon Salvesta _koopia... Salvesta koopia Salvesta versioon Salvestatud Zimi versioonid Hinnang Otsing Otsi _tagasiviiteid... Vali fail Vali kaust Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Vali eksportimise formaat Vali väljundfail või -kaust Vali leheküljed, mida soovid eksportida Vali aken või piirkond Valik Server pole käivitunud. Server on käivitunud Server on peatatud Kuva _muutused Show a separate icon for each notebook Näita tööriistaribal Kuva kursor ka kirjutuskaitsud lehekülgedel. Üksik _lehekülg Some error occurred while running "%s" Õigekirjakontroll Käivita _veebiserver Sü_mbol... Sildid Ülesanne Ülesandeloend Mall Tekst Tekstifailid Tekst _failist... The inline calculator plugin was not able
to evaluate the expression at the cursor. Märkmikusse ei ole viimase versiooni salvestamisest saadik tehtud muudatusi This file already exists.
Do you want to overwrite it? Sellel leheküljel pole manuste kausta This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 Pealkiri To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. Tänase _päeva leheküljele Märgista valikkast 'V' Märgista valikkast 'X' Tray Icon Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Värskenda %i lehekülg, viidates sellele leheküljele Värskenda %i lehekülge, viidates sellele leheküljele Uuenda lehekülje päist Värskendan viiteid Registrite uuendamine Kasuta kohandatud fonti Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Versioonihaldus Version control is currently not enabled for this notebook.
Do you want to enable it? Versioonid Vaata märgitu_d Kuva _logi Terve s_õna Laius Sõnaarvestus Sõnaarvestus... Sõnad Hetkel kasutatakse faili välise programmi poolt. Sa võid selle akna pärast tegevuse lõpetamist sulgeda You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Zim Töölaua Viki _Teave _Tagasi _Veateated Alamlehekülg _Puhasta vorming _Sulge _Sisu _Kopeeri _Kustuta _Kustuta lehekülg _Unusta muudatused _Redigeeri _Redigeeri linki _Viite või objekti redigeerimine _Rõhutatud _KKK _Fail _Otsi... _Edasi _Täisekraan _Mine _Abi _Tõsta esile _Ajalugu _Kodu _Pilt... _Impordi lehekülg... _Lisamine Liigu l_eheküljele... Klahviseosed _Viit Vii_ta kuupäevale _Viide... _Märgistatud V_eel _Uus Lehekülg... _Järgmine _Puudub _Ava A_va olemasolev märkmik... _Muu... _Lehekülg _Ülemlehekülg _Kleebi _Eelvaade _Eelmine _Trüki veebilehitseja abil _Quick Note... _Lõpeta _Hiljutised leheküljed _Taasta _Regulaaravaldis La_e uuesti _Eemalda viide _Asenda _Asenda... Suuruse l_ähtestamine _Taasta versioon _Salvesta Salvesta _koopia _Ekraanitõmmis _Otsing _Otsing... _Saada... _Rida-realt _Läbikriipsutatud _Tugev Allindek_s _ülaindeks _Tööriistad Võta _tagasi _Sõnasõnaline _Versioonid _Kuva Ainult loetav sekundid Launchpad Contributions:
  *nix https://launchpad.net/~xinu7
  Jaap Karssenberg https://launchpad.net/~jaap.karssenberg
  Timo Sulg https://launchpad.net/~timgluz
  mahfiaz https://launchpad.net/~mahfiaz
  milosh https://launchpad.net/~raoul-hot 