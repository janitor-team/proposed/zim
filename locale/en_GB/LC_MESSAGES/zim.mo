��    s     �              L'  ,   M'  .   z'  ?   �'     �'     �'     (  0   .(     _(     z(     �(  	   �(     �(  i   �(  *   !)     L)     \)     i)     v)  
   �)  -   �)     �)  V   �)     )*  	   /*  	   9*     C*  3   W*  Y   �*     �*     �*  
   +     +     '+     3+     F+     Y+     e+     r+  	   y+     �+  $   �+  ?   �+  /   �+  (   ',     P,  %   m,  ,   �,     �,  	   �,     �,     �,  
   �,     -  	   	-     -      -     --     9-     @-  
   M-     X-     p-     w-     �-     �-     �-  
   �-     �-     �-     �-     �-     
.     .  <   ).     f.  	   l.  
   v.     �.     �.     �.     �.     �.     �.     �.     �.     /  +   /  3   J/      ~/     �/     �/     �/     �/  
   �/     �/     �/     
0     &0  3   C0     w0     �0     �0     �0     �0     �0     1     1     %1     *1     71     E1     R1     W1     [1  0   c1     �1     �1     �1     �1  
   �1     �1     �1     �1     �1     �1     2     2  $   +2  ~   P2     �2     �2  
   �2     �2     �2  
   3  	   3  
   3     %3     23     :3     K3     c3     q3     y3  5   �3     �3     �3     �3  !   �3     �3  #   4     14     D4     K4     ^4     |4     �4     �4     �4     �4     �4     �4     �4     5  
   
5     5     $5  	   55  6   ?5     v5  v   }5     �5  *   6  c   16     �6     �6     �6  
   �6     �6     �6     �6     �6  
   �6  
   �6  
   
7  
   7  
    7  
   +7     67     =7     X7     x7  	   �7     �7     �7     �7     �7     �7     �7  
   �7     �7     �7     8     8     ,8     98     I8     [8     j8     w8     �8     �8     �8     �8     �8     �8  	   �8     �8     9     9     9     $9     99     G9     ^9     c9     r9     ~9     �9  2   �9     �9     �9     �9     �9     �9     :     (:     ?:     X:     d:     �:     �:  	   �:     �:     �:     �:     �:     �:     �:     	;     ;      ;  *   <;     g;     p;     y;     �;     �;     �;     �;     �;     �;  *   �;  2   <     P<     a<     r<     �<     �<  	   �<     �<     �<     �<     �<     �<     =     =     #=  
   9=     D=  	   [=     e=     x=     �=     �=     �=     �=     �=     �=     �=  9   �=     +>  I   9>  !   �>  '   �>  	   �>     �>     �>  0   �>  
   ?  	   !?     +?     9?     Q?     f?  	   s?     }?     �?  '   �?  V   �?  3   @  0   ?@  (   p@     �@     �@  .   �@     �@     �@     �@     A     A     &A     +A     <A     DA  
   PA  &   [A  
   �A     �A     �A     �A     �A     �A     �A  
   �A     B     B  
   'B     2B     AB  ?   RB     �B     �B     �B     �B     �B     �B     �B     �B     C     C     C  	   ,C     6C     CC     RC     iC     oC     �C     �C     �C     �C     �C     �C     �C     �C     �C  �   
D     �D      �D     �D     �D  	   E     E     "E     5E     DE     SE     `E     xE     �E     �E  2   �E     �E  &   �E     F     2F     FF     ZF     nF     �F  5   �F  &   �F     �F     G     	G  &   G     ?G     SG     fG     rG     �G     �G  
   �G     �G     �G  	   �G     �G     �G     �G     �G     �G     H  	   H     H      H  	   )H     3H  
   8H     CH     VH     lH  ?   �H  A   �H  �  I  S   �J  =   K  K   ZK  @   �K  6   �K  -   L  I   LL  x   �L  �   M  �   �M  �   N  �   O  �   �O  }   P  L   �P  �   �P  9   hQ  �   �Q  �   JR  �   �R  }   fS  C   �S  h   (T  k   �T  �   �T  R   �U  ;   $V  =   `V  v   �V    W  j   )X  n   �X  ]   Y     aY  �   �Y  7   vZ     �Z  �   �Z  |   P[     �[     �[     �[     �[     �[     \     \  	   #\  '   -\     U\     Z\  D   l\     �\     �\     �\     �\     �\  H   �\     7]     W]     f]  !   u]     �]     �]     �]  P   �]     *^  U   :^     �^     �^  	   �^  
   �^     �^  N   �^     _     _     $_  �   2_  
   �_     �_     `     `  	   `  ^   `  f   z`     �`  	   �`     �`     a  
   a     a     a     %a     -a     3a  	   ;a     Ea     La     ^a     ea  	   sa     }a  
   �a     �a     �a     �a     �a     �a  
   �a     �a     �a     �a  	   b     b     b     b     "b     +b     4b     @b     Db  
   Jb     Ub     ^b  	   db     nb     ~b     �b     �b     �b     �b     �b     �b     �b     �b     �b  
   �b     �b     �b     �b     c     c     "c     (c     5c     Dc     Hc     Nc  	   hc     rc     xc     �c     �c     �c  	   �c     �c     �c     �c     �c     �c     �c     �c      d     d     d     d     *d     3d     ?d     Kd     \d  
   bd     md     |d  
   �d     �d     �d     �d     �d     �d     �d  
   �d     �d  
   �d     �d     �d  	   e     e     e     e     'e     =e     Ue  
   ke     ve     �e     �e     �e     �e     �e  
   �e     �e  �  �e  ,   �g  .   h  ?   1h     qh     }h     �h  0   �h     �h     i      i  	   &i     0i  i   ?i  *   �i     �i     �i     �i     �i  
   j  -   $j     Rj  U   Zj     �j  	   �j  	   �j     �j  3   �j  Y   k     lk     �k  
   �k     �k     �k     �k     �k     �k     �k     �k  	    l     
l  $   l  >   >l  /   }l  (   �l     �l  %   �l  ,   m     Fm  	   \m     fm     um  
   }m     �m  	   �m     �m     �m     �m     �m     �m  
   �m     �m     �m     �m     n     n     (n  
   0n     ;n     Wn     jn     }n     �n     �n  <   �n     �n  	   �n  
   �n     o     o     o     5o     =o     So     io     |o     �o  +   �o  3   �o      p     %p     *p     >p     Lp  
   Xp     cp     rp     �p     �p  3   �p     �p     q     /q     Jq     ]q     uq     �q     �q     �q     �q     �q     �q     �q     �q     �q  0   �q     r     ,r     2r     >r  
   Pr     [r     br     or     {r     �r     �r     �r  $   �r  ~   �r     Vs     ds  
   hs     ss     {s  
   �s  	   �s  
   �s     �s     �s     �s     �s     �s     �s      t  5   	t     ?t     Nt     Zt  !   at     �t  #   �t     �t     �t     �t     �t     u     u     "u     ;u     Gu     `u     |u     �u     �u  
   �u     �u     �u  
   �u  6   �u     �u  w   v     }v  *   �v  c   �v     w     &w     -w  
   4w     ?w     Ww     qw     uw  
   }w  
   �w  
   �w  
   �w  
   �w  
   �w     �w     �w     �w     x  	   x     "x     3x     8x     ?x     Kx     \x  
   bx     mx     �x     �x     �x     �x     �x     �x     �x     �x     y     y     y     ,y     Dy     Ry     _y  	   uy     y     �y     �y     �y     �y     �y     �y     �y     �y     �y     z     z  2   z     Jz     Rz     [z     uz     ~z     �z     �z     �z     �z     �z     
{     {  	   %{     /{     8{     >{     T{     n{     �{     �{     �{      �{  *   �{     �{     �{     |     |     %|     5|     :|     P|     n|  *   ~|  2   �|     �|     �|     �|     }     .}  	   7}     A}     D}     [}     t}     �}     �}     �}     �}  
   �}     �}  	   �}     �}     ~     ~     &~     :~     I~     R~     h~     q~  9   }~     �~  I   �~  !     '   1  	   Y     c     l  0   q  
   �  	   �     �     �     �     �  	   �     	�     �  '   �  U   @�  3   ��  0   ʀ  (   ��     $�     >�  .   E�     t�     |�     ��     ��     ��     ��     ��     ǁ     ρ  
   ہ  &   �  
   �     �     &�     5�     G�     ^�     ~�  
   ��     ��     ��  
   ��     ��     ˂  ?   ܂     �     )�     6�     U�     Y�     _�     o�     ��     ��     ��     ��  	   ��     ��     ̓     ܃     �     ��     �     �     (�     <�     D�     Y�     m�     y�     ��  �   ��     $�      =�     ^�     y�  	   ��     ��     ��     ��     ΅     ݅     �     �     �     (�  6   @�     w�  &   ��     ��     ��     Ԇ     �     ��     �  4   (�  &   ]�     ��     ��     ��  #   ��     ɇ     ݇     ��     ��     
�     �  
   "�     -�     4�  	   C�     M�     S�     `�     r�     w�     ��  	   ��     ��     ��  	   ��     ��  
        ͈     ��     ��  ?   �  A   N�  �  ��  U   T�  =   ��  K   �  @   4�  6   u�  -   ��  I   ڌ  x   $�  �   ��  �   D�  �   �  �   ��  �    �  }   ��  L   �  �   l�  9   ��  �   4�  �   ߒ  �   l�  �   ��  E   |�  h     l   +�  �   ��  R   n�  ;   ��  =   ��  v   ;�    ��  k   Ƙ  n   2�  ]   ��  �   ��  �   ��  7   �     P�  �   V�  |   ��     q�     u�     |�     ��     ��     ��     ��  	   ǜ  '   ќ     ��     ��  D   �     U�     ]�     o�     }�     ��  H   ��     ۝     ��     
�  !   �     ;�     M�     a�  P   }�     Ξ  U   ޞ     4�     =�  	   M�  
   W�     b�  N   g�     ��          ȟ  �   ֟  
   ��     ��     ��     ��  	   ��  `   ��  f    �     ��  	   ��     ��     ��  
   ��     ��     š     ˡ     ӡ     ١  	   �     �     �     �     �  	   �     #�  
   )�     4�     <�     I�     Z�     j�  
   p�     {�     ��     ��  	   ��     ��     ��     ¢     Ȣ     Ѣ     ڢ     �     �  
   �     ��     �  	   
�     �     $�     ,�     2�     >�     K�     Q�     _�     h�     n�     t�  
   z�     ��     ��     ��     ��     £     ȣ     Σ     ۣ     �     �     ��  	   �     �     �     .�     6�     =�  	   F�     P�     W�     i�     x�     ~�     ��     ��     ��     ��     ��     ä     Ф     ٤     �     �     �  
   �     �     "�  
   *�     5�     A�     M�     [�     g�     o�  
   w�     ��  
   ��     ��     ��  	   ��     ��     ��     ĥ     ͥ     �     ��  
   �     �     -�     ;�     D�  �  L�     ��  
   �     �   		This plugin provides bar for bookmarks.
		 %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i Attachment %i Attachments %i errors occurred, see log %i file will be deleted %i files will be deleted %i open item %i open items %i warnings occurred, see log <Top> <Unknown> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Add Application Add Bookmark Add Notebook Add bookmark/Show settings Add column Add new bookmarks to the beginning of the bar Add row Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files All Tasks Allow public access Always use last cursor position when opening a page An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Arithmetic Ascii graph (Ditaa) Attach File Attach image first Attachment Browser Attachments Attachments: Author Auto
Wrap Auto indenting Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave interval in minutes Autosave version on regular intervals Autosave version when the notebook is closed Back to Original Name BackLinks BackLinks Pane Backend Backlinks: Bazaar Bookmarks BookmarksBar Border width Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Capture whole screen Center Change columns Changes Characters Characters excluding spaces Check Checkbox '>' Check Checkbox 'V' Check Checkbox 'X' Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Clone row Code Block Column 1 Command Command does not modify data Comment Common include footer Common include header Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Created Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Demote Dependencies Description Details Diagram Discard note? Distraction Free Editing Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root Due E_xport... Edit %s Edit Custom Tool Edit Image Edit Link Edit Table Edit _Source Editing Editing file: %s Enable Version Control? Enable plugin Enabled Equation Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Fossil GNU R Plot Get more plugins online Get more templates online Git Gnuplot Grid lines Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide Journal pane if empty Hide menubar in fullscreen mode Highlight current line Home Page Horizontal _Line Icon Images Import Page Include subpages Index Index page Inline Calculator Insert Code Block Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Sequence Diagram Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Labels marking tasks Last Modified Leave link to new page Left Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Managing table columns Map document root to URL Match _case Maximum number of bookmarks Maximum page width Menubar Mercurial Modified Month Move Selected Text... Move Text to Other Page Move column ahead Move column backward Move text to Name Need output file to export MHTML Need output folder to export full notebook New File New Page New Page in %s New S_ub Page... New _Attachment Next No Applications Found No changes since last version No dependencies No document root defined for this notebook No plugin available to display objects of type: %s No such file: %s No such page: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Only Show Active Tasks Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open cell content link Open help Open in New Window Open in New _Window Open new page Open plugins folder Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Index Page Name Page Template Page already exists: %s Page not allowed: %s Page section Paragraph Paste Path Bar Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please select a row, before you push the button. Please select more than one line of text Please specify a notebook Plugin Plugin "%s" is required to display this object Plugins Port Position in the window Pr_eferences Preferences Prev Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Remove column Remove links to %s Remove row Removing Links Rename page "%s" Repeated clicking a checkbox cycles through the checkbox states Replace _All Replace with Restore page to saved version? Rev Right Right Side Pane Right margin position Row down Row up S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Screenshot Command Search Search _Backlinks... Search this section Section Section(s) to ignore Section(s) to index Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Sequence Diagram Server not started Server started Server stopped Set New Name Set default text editor Set to Current Page Show Line Numbers Show Tasks as Flat List Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show full Page Name Show full page name Show helper toolbar Show in the toolbar Show right margin Show tasklist in sidepane Show the cursor also for pages that can not be edited Show the page title heading in the ToC Single _page Size Smart Home key Some error occurred while running "%s" Sort alphabetically Sort pages by tags Source View Spell Checker Start Start _Web Server Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Tags for non-actionable tasks Task Task List Tasks Template Templates Text Text Files Text From _File... Text background color Text foreground color The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The following parameters will be substituted
in the command when it is executed:
<tt>
<b>%f</b> the page source as a temporary file
<b>%d</b> the attachment directory of the current page
<b>%s</b> the real page source file (if any)
<b>%p</b> the page name
<b>%n</b> the notebook location (file or folder)
<b>%D</b> the document root (if any)
<b>%t</b> the selected text or word under cursor
<b>%T</b> the selected text including wiki formatting
</tt>
 The inline calculator plugin was not able
to evaluate the expression at the cursor. The table must consist of at least on row!
 No deletion done. There are no changes in this notebook since the last version that was saved This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This page name cannot be used due to technical limitations of the storage This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a "path bar" to the top of the window.
This "path bar" can show the notebook path for the current page,
recent visited pages or recent edited pages.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds the page index pane to the main window.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows inserting 'Code Blocks' in the page. These will be
shown as embedded widgets with syntax highlighting, line numbers etc.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin also has properties,
see the notebook properties dialog This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a sequence diagram editor for zim based on seqdiag.
It allows easy editing of sequence diagrams.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This plugin turns one section of the notebook into a journal
with a page per day, week or month.
Also adds a calendar widget to access these pages.
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox '>' Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Tray Icon Turn page name into tags for task items Type Un-check Checkbox Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unkown Image type Unkown Object Unspecified Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use a page for each Use date from journal pages Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s With this plugin you can embed a 'Table' into the wiki page. Tables will be shown as GTK TreeView widgets.
Exporting them to various formats (i.e. HTML/LaTeX) completes the feature set.
 Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Zim Desktop Wiki Zoom _Out _About _Add _All Panes _Arithmetic _Back _Browse _Bugs _Cancel _Checkbox _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Duplicate Line _Edit _Edit Link _Edit Link or Object... _Edit Properties _Edit... _Emphasis _FAQ _File _Find _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _Move Here _Move Line Down _Move Line Up _New Page Here... _New Page... _Next _None _Normal Size _Numbered List _OK _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Print _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove _Remove Line _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In as due date for tasks as start date for tasks calendar:week_start:0 do not use horizontal lines no grid lines readonly seconds translator-credits vertical lines with lines {count} of {total} Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2020-09-17 05:43+0000
Last-Translator: J. Lavoie <j.lavoie@net-c.ca>
Language-Team: English (United Kingdom) <https://hosted.weblate.org/projects/zim/master/en_GB/>
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.3-dev
X-Launchpad-Export-Date: 2020-01-01 13:40+0000
 		This plugin provides bar for bookmarks.
		 %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i Attachment %i Attachments %i errors occurred, see log %i file will be deleted %i files will be deleted %i open item %i open items %i warnings occurred, see log <Top> <Unknown> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Add Application Add Bookmark Add Notebook Add bookmark/Show settings Add column Add new bookmarks to the beginning of the bar Add row Adds spellchecking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files All Tasks Allow public access Always use last cursor position when opening a page An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Arithmetic ASCII graph (Ditaa) Attach File Attach image first Attachment Browser Attachments Attachments: Author Auto
Wrap Auto indenting Automatically saved version from zim Automatically select the current word when applying formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave interval in minutes Autosave version at regular intervals Autosave version when the notebook is closed Back to Original Name BackLinks BackLinks Pane Backend Backlinks: Bazaar Bookmarks BookmarksBar Border width Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Capture whole screen Centre Change columns Changes Characters Characters excluding spaces Check Checkbox '>' Check Checkbox 'V' Check Checkbox 'X' Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Clone row Code Block Column 1 Command Command does not modify data Comment Common include footer Common include header Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Copy E-mail Address Copy Template Copy _As... Copy _Link Copy _Location Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Created Cu_t Custom Tools Custom _Tools Customise... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Demote Dependencies Description Details Diagram Discard note? Distraction Free Editing Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root Due E_xport... Edit %s Edit Custom Tool Edit Image Edit Link Edit Table _Edit Source Editing Editing file: %s Enable Version Control? Enable plugin Enabled Equation Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Search for Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and is not empty, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Fossil GNU R Plot Get more plugins online Get more templates online Git Gnuplot Grid lines Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide Journal pane if empty Hide menubar in fullscreen mode Highlight current line Home Page Horizontal _Line Icon Images Import Page Include subpages Index Index page In-line Calculator Insert Code Block Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Sequence Diagram Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Labels marking tasks Last Modified Leave link to new page Left Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Managing table columns Map document root to URL Match _case Maximum number of bookmarks Maximum page width Menubar Mercurial Modified Month Move Selected Text... Move Text to Another Page Move column ahead Move column backward Move text to Name Need output file to export MHTML Need output folder to export full notebook New File New Page New Page in %s New S_ub Page... New _Attachment Next No Applications Found No changes since last version No dependencies No document root defined for this notebook No plugin available to display objects of type: %s No such file: %s No such page: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Only Show Active Tasks Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open cell content link Open help Open in New Window Open in New _Window Open new page Open plugins folder Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Index Page Name Page Template Page already exists: %s Page not allowed: %s Page section Paragraph Paste Path Bar Please enter a comment for this version Please note that linking to a nonexistant page
also creates a new page automatically. Please select a name and a folder for the notebook. Please select a row, before you push the button. Please select more than one line of text Please specify a notebook Plugin Plugin "%s" is required to display this object Plugins Port Position in the window Pr_eferences Preferences Prev Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently Changed Pages Reformat wiki markup on the fly Remove Remove All Remove column Remove Links to %s Remove row Removing Links Rename page "%s" Repeated clicking a checkbox cycles through the checkbox states Replace _All Replace with Restore page to saved version? Rev Right Right Side Pane Right margin position Row down Row up S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Screenshot Command Search Search _Backlinks... Search this section Section Section(s) to ignore Section(s) to index Select File Select Folder Select Image Select a version to see changes between that version and the current
state, or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Sequence Diagram Server not started Server started Server stopped Set New Name Set default text editor Set to Current Page Show Line Numbers Show Tasks as Flat List Show ToC as floating widget instead of in the sidepane Show _Changes Show a separate icon for each notebook Show full Page Name Show full page name Show helper toolbar Show in the toolbar Show right margin Show tasklist in sidepane Show the cursor even for pages that cannot be edited Show the page title heading in the ToC Single _page Size Smart Home key An error occured while running "%s" Sort alphabetically Sort pages by tags Source View Spell Checker Start Start _Web Server Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Tags for non-actionable tasks Task Task List Tasks Template Templates Text Text Files Text From _File... Text background colour Text foreground colour The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The following parameters will be substituted
in the command when it is executed:
<tt>
<b>%f</b> the page source as a temporary file
<b>%d</b> the attachment directory of the current page
<b>%s</b> the real page source file (if any)
<b>%p</b> the page name
<b>%n</b> the notebook location (file or folder)
<b>%D</b> the document root (if any)
<b>%t</b> the selected text or word under cursor
<b>%T</b> the selected text including wiki formatting
</tt>
 The in-line calculator plug-in was not able
to evaluate the expression at the cursor. The table must consist of at least on row!
 No deletion done. There are no changes in this notebook since the last version that was saved This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This page name cannot be used due to technical limitations of the storage This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a "path bar" to the top of the window.
This "path bar" can show the notebook path for the current page,
recent visited pages or recent edited pages.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plug-in adds the 'Insert Symbol' dialogue and allows
auto-formatting typographic characters.

This is a core plug-in shipping with Zim.
 This plugin adds the page index pane to the main window.
 This plug-in adds version control for notebooks.

This plug-in supports the Bazaar, Git and Mercurial version control systems.

This is a core plug-in shipping with zim.
 This plugin allows inserting 'Code Blocks' in the page. These will be
shown as embedded widgets with syntax highlighting, line numbers etc.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plug-in allows you to quickly evaluate simple
mathematical expressions in Zim.

This is a core plug-in and ships with Zim.
 This plugin also has properties,
see the notebook properties dialogue This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim, based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialogue with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plug-in provides a plot editor for Zim based on GNU R
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a sequence diagram editor for zim based on seqdiag.
It allows easy editing of sequence diagrams.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim, based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This plug-in sorts selected lines into alphabetical order.
If the list is already sorted, the order will be reversed
(A-Z to Z-A).
 This plugin turns one section of the notebook into a journal
with a page per day, week or month.
Also adds a calendar widget to access these pages.
 This usually means the file contains invalid characters Title To continue, you can save a copy of this page or discard
any changes. If you save a copy, changes will also be
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox '>' Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Tray Icon Turn page name into tags for task items Type Un-check Checkbox Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unkown Image type Unkown Object Unspecified Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use a page for each Use date from journal pages Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s With this plugin you can embed a 'Table' into the wiki page. Tables will be shown as GTK TreeView widgets.
Exporting them to various formats (i.e. HTML/LaTeX) completes the feature set.
 Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialogue when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Zim Desktop Wiki Zoom _Out _About _Add _All Panes _Arithmetic _Back _Browse _Bugs _Cancel _Checkbox _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Duplicate Line _Edit _Edit Link _Edit Link or Object... _Edit Properties _Edit... _Emphasis _FAQ _File _Find _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _Move Here _Move Line Down _Move Line Up _New Page Here... _New Page... _Next _None _Normal Size _Numbered List _OK _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Print _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove _Remove Line _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort Lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In as due date for tasks as start date for tasks calendar:week_start:0 do not use horizontal lines no grid lines readonly seconds Launchpad Contributions:
  Andi Chandler https://launchpad.net/~bing
  Biffaboy https://launchpad.net/~curtisbull
  Domhnall Walsh https://launchpad.net/~domhnall-walsh
  Jaap Karssenberg https://launchpad.net/~jaap.karssenberg
  Matthew Gadd https://launchpad.net/~darkotter
  Michael Mulqueen https://launchpad.net/~michael.mulqueen
  MoLE https://launchpad.net/~moleonthehill
  Nicholas Wastell https://launchpad.net/~nickwastell
  Otto Robba https://launchpad.net/~otto-ottorobba
  Paul Thomas Stiles https://launchpad.net/~paulstiles91
  Rui Moreira https://launchpad.net/~rui-f-moreira
  Vibhav Pant https://launchpad.net/~vibhavp
  dotancohen https://launchpad.net/~dotancohen vertical lines with lines {count} of {total} 