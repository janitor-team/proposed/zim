��    r     �              <'  ,   ='  .   j'  ?   �'     �'     �'     (  0   (     O(     j(     �(  	   �(     �(  i   �(  *   )     <)     L)     Y)     f)  
   �)  -   �)     �)  V   �)     *  	   *  	   )*     3*  3   G*  Y   {*     �*     �*  
   �*     +     +     #+     6+     I+     U+     b+  	   i+     s+  $   �+  ?   �+  /   �+  (   ,     @,  %   ],  ,   �,     �,  	   �,     �,     �,  
   �,     �,  	   �,     -     -     -     )-     0-  
   =-     H-     `-     g-     |-     �-     �-  
   �-     �-     �-     �-     �-     �-     
.  <   .     V.  	   \.  
   f.     q.     z.     �.     �.     �.     �.     �.     �.     �.  +   /  3   :/      n/     �/     �/     �/     �/  
   �/     �/     �/     �/     0  3   30     g0     �0     �0     �0     �0     �0     �0     1     1     1     '1     51     B1     G1     K1  0   S1     �1     �1     �1     �1  
   �1     �1     �1     �1     �1     �1     �1     2  $   2  ~   @2     �2     �2  
   �2     �2     �2  
   �2  	    3  
   
3     3     "3     *3     ;3     S3     a3     i3  5   r3     �3     �3     �3  !   �3     �3  #   �3     !4     44     ;4     N4     l4     x4     �4     �4     �4     �4     �4     �4     �4  
   �4     5     5  	   %5  6   /5     f5  v   m5     �5  *   �5  c   !6     �6     �6     �6  
   �6     �6     �6     �6     �6  
   �6  
   �6  
   �6  
   7  
   7  
   7     &7     -7     H7     h7  	   7     �7     �7     �7     �7     �7     �7  
   �7     �7     �7     �7     8     8     )8     98     K8     Z8     g8     s8     �8     �8     �8     �8     �8  	   �8     �8     �8     �8     9     9     )9     79     N9     S9     b9     n9     t9  2   }9     �9     �9     �9     �9     �9     �9     :     /:     H:     T:     p:     �:  	   �:     �:     �:     �:     �:     �:     �:     �:     ;      ;  *   ,;     W;     `;     i;     x;     �;     �;     �;     �;     �;  *   �;  2   <     @<     Q<     b<     {<     �<  	   �<     �<     �<     �<     �<     �<     �<     �<     =  
   )=     4=  	   K=     U=     h=     |=     �=     �=     �=     �=     �=     �=  9   �=     >  I   )>  !   s>  '   �>  	   �>     �>     �>  0   �>  
   ?  	   ?     ?     )?     A?     V?  	   c?     m?     s?  '   |?  V   �?  3   �?  0   /@  (   `@     �@     �@  .   �@     �@     �@     �@     �@     
A     A     A     ,A     4A  
   @A  &   KA  
   rA     }A     �A     �A     �A     �A     �A  
   �A     �A  
   B     B     B  ?   /B     oB     |B     �B     �B     �B     �B     �B     �B     �B     �B     �B  	   	C     C      C     /C     FC     LC     _C     fC     {C     �C     �C     �C     �C     �C     �C  �   �C     wD      �D     �D     �D  	   �D     �D     �D     E     !E     0E     =E     UE     iE     {E  2   �E     �E  &   �E     �E     F     #F     7F     KF     ]F  5   wF  &   �F     �F     �F     �F  &   �F     G     0G     CG     OG     ]G     cG  
   uG     �G     �G  	   �G     �G     �G     �G     �G     �G     �G  	   �G     �G     �G  	   H     H  
   H      H     3H     IH  ?   _H  A   �H  �  �H  S   �J  =   �J  K   7K  @   �K  6   �K  -   �K  I   )L  x   sL  �   �L  �   �M  �   \N  �   �N  �   oO  }   �O  L   nP  �   �P  9   EQ  �   Q  �   'R  �   �R  }   CS  C   �S  h   T  k   nT  �   �T  R   �U  ;   V  =   =V  v   {V    �V  j   X  n   qX  ]   �X     >Y  �   �Y  7   SZ     �Z  �   �Z  |   -[     �[     �[     �[     �[     �[     �[     �[  	    \  '   
\     2\     7\  D   I\     �\     �\     �\     �\     �\  H   �\     ]     4]     C]  !   R]     t]     �]     �]  P   �]     ^  U   ^     m^     v^  	   �^  
   �^     �^  N   �^     �^     �^     _  �   _  
   �_     �_     �_     �_  	   �_  ^   �_  f   W`     �`  	   �`     �`     �`  
   �`     �`     �`     a     
a     a  	   a     "a     )a     ;a     Ba  	   Pa     Za  
   `a     ka     sa     �a     �a     �a  
   �a     �a     �a     �a  	   �a     �a     �a     �a     �a     b     b     b     !b  
   'b     2b     ;b  	   Ab     Kb     [b     cb     ib     ub     �b     �b     �b     �b     �b     �b  
   �b     �b     �b     �b     �b     �b     �b     c     c     !c     %c     +c  	   Ec     Oc     Uc     ec     mc     tc  	   }c     �c     �c     �c     �c     �c     �c     �c     �c     �c     �c     �c     d     d     d     (d     9d  
   ?d     Jd     Yd  
   ad     ld     xd     �d     �d     �d     �d  
   �d     �d  
   �d     �d     �d  	   �d     �d     �d     �d     e     e     2e  
   He     Se     de     re     {e     �e     �e  
   �e     �e  �  �e  2   �g  /   �g  M   �g     Jh  #   Wh  )   {h  F   �h  &   �h  .   i     Bi  	   Ii     Si  w   ji  *   �i     j     %j     <j  )   Qj     {j  1   �j     �j  q   �j  
   Ek     Pk     bk     uk  Q   �k  s   �k     Ul     pl     }l     �l     �l  !   �l     �l     �l     m     m     m     6m  /   Nm  3   ~m  9   �m  M   �m  9   :n  B   tn  D   �n     �n     o      o     7o     ?o     Ko     Ro     _o     uo     �o  	   �o     �o     �o      �o     �o     �o     p     p     %p     1p     =p     Zp     rp     �p     �p     �p  V   �p     "q     *q     :q  	   Gq     Qq  '   Zq     �q     �q     �q     �q     �q     �q  9   r  G   Jr  1   �r     �r     �r     �r     �r     s     !s  -   7s  (   es  %   �s  ?   �s  2   �s     't  !   ?t     at  '   yt  )   �t     �t  
   �t     �t     �t     u     #u     4u     9u     >u  8   Ju     �u     �u     �u     �u     �u  	   �u     �u     �u     v  	   v     v     *v  -   Cv  �   qv     �v     w     w  
   w     #w     Bw     Sw     dw     xw     �w     �w     �w     �w     �w  	   �w  @   �w  !   9x     [x     jx  0   sx     �x  -   �x     �x     �x     �x  )   y     Gy     Yy  .   qy     �y  *   �y  !   �y     �y     z  
   z     !z     8z     Pz  
   fz  A   qz     �z  �   �z     K{  /   b{  z   �{     |     |     |  
   #|  !   .|  !   P|     r|     v|     ~|     �|     �|     �|     �|     �|     �|  &   �|  .   �|  (   }     9}     H}     [}     b}     i}     z}     �}     �}     �}     �}     �}     �}     ~     ~     +~     D~     _~     r~     �~     �~  "   �~     �~     �~  #     	   &     0     C     K     T     e     �  %   �     �     �     �     �     �  W   �  	   Y�     c�  +   o�     ��  (   ��     Ӏ     �  "   
�     -�     @�     Z�     s�  	   ��     ��     ��  #   ��  %   ��  #   �  #   �     ,�     G�  =   K�  J   ��     Ԃ     �     �     �     �     7�     ?�  0   [�     ��  3   ��  >   ԃ     �     +�     ;�     X�  
   s�  
   ~�     ��  &   ��  )   ��     ݄     �     �     �      .�     O�     _�     |�  !   ��  "   ��     х     �     �  
   �     *�     @�     I�  G   [�     ��  Y   ��  -   �  )   B�     l�     u�     ��  >   ��     ʇ     ڇ     �     ��     �     .�  
   A�     L�     S�  +   c�  f   ��  9   ��  6   0�  0   g�  $   ��     ��  7   ŉ     ��     �     �     %�     4�     B�     I�  
   e�     p�     ~�  +   ��     ��     Ê     Ҋ     �     ��  (   �  	   E�     O�     ^�     s�     ��     ��  S   ��     �     �  $   &�     K�     O�     V�     m�     ��     ��     ��     ̌     �     ��     �     0�  	   P�     Z�  
   w�     ��     ��     ��          ؍     �     �      �  �   8�  %   ܎  5   �  #   8�  &   \�  
   ��     ��     ��     ��     Ώ     ߏ  (   �     �     1�  -   P�  Y   ~�     ؐ  *   �  #   �  *   A�  !   l�     ��     ��  6   ʑ  $   �  8   &�     _�     l�     s�  4   ��     ��     ϒ     �     �     �     $�     >�     J�     R�     q�     ��     ��     ��     ��  .   Ɠ     ��     ��     �     �     �     '�     -�      <�     ]�     v�  3   ��  5   ��    �  E    �  B   F�  V   ��  F   ��  2   '�  2   Z�  S   ��  �   �  �   n�  
  =�  �   H�  �   �  �   ��  �   &�  D   ��  �   ��  M   ��  �   ��  �   ��  �   s�  �   �  Z   ��  s   �  l   d�  �   Ѣ  a   ��  D   ��  E   9�  �   �  $   �  h   %�  x   ��  `   �  �   h�  �   �  N   ��     ��  �   �  �   ݩ     q�     y�     ��     ��     ��     ��     ת     �  =   �     ?�     D�  j   V�     ��     ɫ     ޫ     �     ��  ^   �  '   k�     ��     ��  2   ¬  "   ��     �  '   6�  n   ^�     ͭ  X   �     9�     B�     X�     n�     z�  ]   ��     �     �     ��  �   �     ٯ     �     ��     ��     �  �   	�  �   ��     ,�  
   C�  
   N�     Y�     b�     u�     ��  
   ��     ��     ��     ��     ��     ñ     ױ     ߱  	   ��     ��     ��  
   �     �     )�     D�  	   X�     b�     t�     ��     ��  	   ��     ��     ò     ̲     ز     �     �  	   ��     �  
   �     �     %�  	   .�     8�  	   M�     W�     i�     v�     ��     ��     ��     ��     ��  
   ��     ų     Գ     �     ��     �     !�     *�     1�     A�     T�     X�     `�  	   |�     ��     ��     ��     ��     ��     ��  	   ʴ     Դ     �     �     
�  
   �     &�     >�  
   J�     U�     i�  
   z�     ��     ��     ��     ��     ˵     �     ��     �     �      �     4�     D�     V�     ^�     d�  	   l�  	   v�     ��     ��  	   ��     ��  
   ��     ��  '   ��     �     ��     �     �     2�     @�     N�  �  W�     '�     9�     I�   		This plugin provides bar for bookmarks.
		 %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i Attachment %i Attachments %i errors occurred, see log %i file will be deleted %i files will be deleted %i open item %i open items %i warnings occurred, see log <Top> <Unknown> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Add Application Add Bookmark Add Notebook Add bookmark/Show settings Add column Add new bookmarks to the beginning of the bar Add row Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files All Tasks Allow public access Always use last cursor position when opening a page An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Arithmetic Ascii graph (Ditaa) Attach File Attach image first Attachment Browser Attachments Attachments: Author Auto
Wrap Auto indenting Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave interval in minutes Autosave version on regular intervals Autosave version when the notebook is closed Back to Original Name BackLinks BackLinks Pane Backend Backlinks: Bazaar Bookmarks BookmarksBar Border width Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Capture whole screen Center Change columns Changes Characters Characters excluding spaces Check Checkbox '>' Check Checkbox 'V' Check Checkbox 'X' Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Clone row Code Block Column 1 Command Command does not modify data Comment Common include footer Common include header Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Created Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Demote Dependencies Description Details Diagram Discard note? Distraction Free Editing Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root Due E_xport... Edit %s Edit Custom Tool Edit Image Edit Link Edit Table Edit _Source Editing Editing file: %s Enable Version Control? Enable plugin Enabled Equation Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Fossil GNU R Plot Get more plugins online Get more templates online Git Gnuplot Grid lines Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide Journal pane if empty Hide menubar in fullscreen mode Highlight current line Home Page Horizontal _Line Icon Images Import Page Include subpages Index Index page Inline Calculator Insert Code Block Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Sequence Diagram Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Labels marking tasks Last Modified Leave link to new page Left Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Managing table columns Map document root to URL Match _case Maximum number of bookmarks Maximum page width Menubar Mercurial Modified Month Move Selected Text... Move Text to Other Page Move column ahead Move column backward Move text to Name Need output file to export MHTML Need output folder to export full notebook New File New Page New Page in %s New S_ub Page... New _Attachment Next No Applications Found No changes since last version No dependencies No document root defined for this notebook No plugin available to display objects of type: %s No such file: %s No such page: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Only Show Active Tasks Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open cell content link Open help Open in New Window Open in New _Window Open new page Open plugins folder Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Index Page Name Page Template Page already exists: %s Page not allowed: %s Page section Paragraph Paste Path Bar Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please select a row, before you push the button. Please select more than one line of text Please specify a notebook Plugin Plugin "%s" is required to display this object Plugins Port Position in the window Pr_eferences Preferences Prev Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Remove column Remove row Removing Links Rename page "%s" Repeated clicking a checkbox cycles through the checkbox states Replace _All Replace with Restore page to saved version? Rev Right Right Side Pane Right margin position Row down Row up S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Screenshot Command Search Search _Backlinks... Search this section Section Section(s) to ignore Section(s) to index Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Sequence Diagram Server not started Server started Server stopped Set New Name Set default text editor Set to Current Page Show Line Numbers Show Tasks as Flat List Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show full Page Name Show full page name Show helper toolbar Show in the toolbar Show right margin Show tasklist in sidepane Show the cursor also for pages that can not be edited Show the page title heading in the ToC Single _page Size Smart Home key Some error occurred while running "%s" Sort alphabetically Sort pages by tags Source View Spell Checker Start Start _Web Server Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Tags for non-actionable tasks Task Task List Tasks Template Templates Text Text Files Text From _File... Text background color Text foreground color The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The following parameters will be substituted
in the command when it is executed:
<tt>
<b>%f</b> the page source as a temporary file
<b>%d</b> the attachment directory of the current page
<b>%s</b> the real page source file (if any)
<b>%p</b> the page name
<b>%n</b> the notebook location (file or folder)
<b>%D</b> the document root (if any)
<b>%t</b> the selected text or word under cursor
<b>%T</b> the selected text including wiki formatting
</tt>
 The inline calculator plugin was not able
to evaluate the expression at the cursor. The table must consist of at least on row!
 No deletion done. There are no changes in this notebook since the last version that was saved This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This page name cannot be used due to technical limitations of the storage This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a "path bar" to the top of the window.
This "path bar" can show the notebook path for the current page,
recent visited pages or recent edited pages.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds the page index pane to the main window.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows inserting 'Code Blocks' in the page. These will be
shown as embedded widgets with syntax highlighting, line numbers etc.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin also has properties,
see the notebook properties dialog This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a sequence diagram editor for zim based on seqdiag.
It allows easy editing of sequence diagrams.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This plugin turns one section of the notebook into a journal
with a page per day, week or month.
Also adds a calendar widget to access these pages.
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox '>' Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Tray Icon Turn page name into tags for task items Type Un-check Checkbox Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unkown Image type Unkown Object Unspecified Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use a page for each Use date from journal pages Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s With this plugin you can embed a 'Table' into the wiki page. Tables will be shown as GTK TreeView widgets.
Exporting them to various formats (i.e. HTML/LaTeX) completes the feature set.
 Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Zim Desktop Wiki Zoom _Out _About _Add _All Panes _Arithmetic _Back _Browse _Bugs _Cancel _Checkbox _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Duplicate Line _Edit _Edit Link _Edit Link or Object... _Edit Properties _Edit... _Emphasis _FAQ _File _Find _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _Move Here _Move Line Down _Move Line Up _New Page Here... _New Page... _Next _None _Normal Size _Numbered List _OK _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Print _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove _Remove Line _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In as due date for tasks as start date for tasks calendar:week_start:0 do not use horizontal lines no grid lines readonly seconds translator-credits vertical lines with lines {count} of {total} Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2020-09-21 06:39+0000
Last-Translator: J. Lavoie <j.lavoie@net-c.ca>
Language-Team: French <https://hosted.weblate.org/projects/zim/master/fr/>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Weblate 4.3-dev
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
 		Ce greffon fournit une barre de marque-pages.
		 %(cmd)s
a retourné un code d'erreur : %(code)i Il y a %(n_error)i erreurs et %(n_warning)i messages, regardez le fichier log %A, %d %B %Y %i pièce jointe %i pièces jointes %i erreurs se sont produites, voir le log le fichier %i va être supprimé les fichiers %i vont être supprimés %i tâche en cours %i tâches en cours %i avertissements ont été émis, voir le log <Haut> <Inconnu> Un wiki pour le bureau Un fichier portant le nom <b>"%s"</b> existe déjà.
Vous pouvez utiliser un autre nom ou écraser le fichier existant. Un tableau doit avoir au moins une colonne Ajouter une application Ajouter un marque-page Ajouter un bloc-note Ajouter un signet/Montrer les paramètres Ajouter une colonne Ajouter un nouveau marque-page en début de barre Ajouter une ligne Ajoute le soutien de vérification d'orthographe basée sur gtkspell.

C'est un greffon de base livré avec zim.
 Alignement Tous les fichiers Toutes les tâches Autoriser l'accès public Toujours utiliser la dernière position du curseur lors de l'ouverture de la page Une erreur est survenue lors de la génération de l'image.
Voulez-vous tout de même sauvegarder le texte source ? Source de la page annotée Applications Arithmétique Graphe Ascii (Ditaa) Joindre un fichier Copier l'image dans le bloc-notes Navigateur de fichiers liés Fichiers liés Fichiers liés Auteur Retour à la ligne
Automatique Indentation automatique Version enregistrée automatiquement depuis Zim Sélectionner le mot courant en appliquant un style Transformer automatiquement les mots "CamelCase" en liens Transformer automatiquement les chemins vers le système de fichiers en liens Intervalle entre deux sauvegardes automatiques en minutes Enregistrer automatiquement des versions à intervalles réguliers Sauvegarder automatiquement la version à la fermeture du bloc-notes Revenir au nom original Rétroliens Panneau de rétroliens Backend Rétroliens Bazaar Marque-pages Barre de marque-pages Épaisseur de la bordure Panneau inférieur Parcourir Lis_te à puces C_onfigurer Ne peut pas modifier la page: %s Annuler Capturer tout l'écran Centré Modifier les colonnes Changements Caractères Caractères sans les espaces (Dé)Cocher la case '>' (Dé)Cocher la case 'V' (Dé)Cocher la case 'X' Vérifier l'_orthographe Ca_se à cocher Icône de notification classique,
ne pas utiliser le nouveau type d'icône sous Ubuntu Effacer Cloner la ligne Bloc de code Colonne 1 Commande La commande ne modifie pas les données Commentaire Pied de page d'inclusion commun En-tête d'inclusion commun _Bloc-notes complet Configurer les applications Configurer le greffon Configurez une application pour ouvrir les liens « %s » Configurer une application pour ouvrir des fichiers
 de type « %s » Considérer les cases à cocher comme des tâches Copier Copier l'adresse électronique Copier le modèle Copier _au format... Copier le _lien Copier _l'emplacement Impossible de trouver l'exécutable | %s » Impossible de trouver le bloc-note : %s Impossible de trouver le modèle "%s" Impossible de trouver le fichier ou le dossier de ce bloc-notes Impossible de charger le correcteur orthographique Ne peut pas ouvrir : %s Ne peut pas analyser l'expression Impossible de lire : %s Impossible de sauvegarder la page : %s Créer une nouvelle page pour chaque note Créer le répertoire ? Créée le Co_uper Outils personnalisés Outils _personnalisés Personnaliser... Date Jour Par défaut Format par défaut du texte copié dans le presse-papier Carnet par défaut Délai Supprimer la page Supprimer la page « %s » ? Supprimer la ligne Minimiser Dépendances Description Détails Diagramme Abandonner la note ? Edition sans distraction Voulez-vous supprimer tous les marque-pages ? Voulez-vous revenir à la version %(version)s
de la page %(page)s ?
Toutes les modifications effectuées depuis seront perdues ! Racine du document Échu E_xporter... Éditer %s Éditer un outil personnalisé Modifier l'image Modifier le lien Modifier le tableau Modifier les sources Modification Modification de fichier : %s Activer la gestion de version ? Greffon activé Actif Équation Erreur dans %(file)s à la ligne %(line)i près de "%(snippet)s" Évalue expression _mathématique Tout _déplier Exporter Exporter toutes les pages dans un unique fichier Export terminé Exporter chaque page dans un fichier séparé Export du bloc-notes Échec Échec dans l'exécution de %s Échec du lancement de l'application : %s Le fichier existe _Modèles de fichier... Le fichier a été modifié sur le disque : %s Le fichier existe Le fichier est protégé en écriture : %s Type de fichier non supporté: %s Nom du fichier Filtrer Rechercher Rechercher le _suivant Rechercher _précédent Chercher et remplacer Rechercher Marquer avant le fin de semaine les tâches dû le lundi ou mardi Dossier Le dossier existe déjà et possède des fichiers ; exporter dans ce dossier pourrait remplacer des fichiers existants. Voulez-vous continuer ? Le dossier existe : %s Dossier avec templates pour les pièces jointes Pour une recherche avancée, vous pouvez utiliser des opérateurs
comme AND, OR et NOT. Voir l'aide pour plus de détails. For_mat Format Fossil GNU R Plot Obtenir plus de greffons en ligne Obtenir plus de modèles en ligne Git Gnuplot Grille Titre _1 Titre _2 Titre _3 Titre _4 Titre _5 Hauteur Masquer l'onglet journal s'il est vide Masquer la barre de menus en mode plein écran Mettre en surbrillance la ligne actuelle Page d'accueil _Ligne Horizontale Icône Images Importer la page Inclure les sous-pages Index Page d'index Calculatrice en ligne Insérer un bloc de code Insérer la date et l'heure Insérer un diagramme Insérer Ditaa Insérer une équation Insérer un graphe GNU R Insérer un graphe Gnuplot Insérer une image Insérer un lien Insérer une partition Insérer une capture d'écran Insérer un diagramme de séquence Insérer un symbole Insère un tableau Insérer du texte depuis un fichier Interface Mot clé interwiki Journal Aller à Aller à la page Labels marquant les tâches Dernière modification Ajouter un lien vers la nouvelle page Gauche Panneau latéral gauche Tri de ligne Lignes Carte des liens Lier les fichiers présents dans l'arborescence des documents avec des chemins complets Lien vers Emplacement Journaliser les évènements avec Zeitgeist Fichier de log Il semble que vous ayez trouvé un bogue Application par défaut Gérer les colonnes du tableau Lien du document parent vers l'URL Respecter la casse Nombre maximal de signets Largeur maximale de page Barre de menus Mercurial Modifié Mois Déplacer le texte sélectionné... Déplacer le texte sur une autre page Déplacer la colonne vers la droite Déplacer la colonne vers la gauche Déplacer le texte vers... Nom Un fichier de destination est nécessaire pour l'export MHTML Un dossier de destination est nécessaire pour exporter tout le bloc-notes Nouveau fichier Nouvelle page Nouvelle page dans %s Nouvelle so_us-page... Nouvelle _pièce jointe Suivant Aucune application trouvée Pas de modifications depuis la dernière version Pas de dépendances Aucune racine pour les documents dans ce bloc-notes Aucun greffon disponible pour afficher des objets du type : %s Fichier inexistant : %s Pas de page: %s Pas de lien wiki défini: %s Pas de modèles installés Bloc-notes Bloc-notes OK Montrer uniquement les tâches actives Ouvrir le _dossier des pièces attachées Ouvrir le dossier Ouvrir le bloc-notes Ouvrir avec... Ouvrir le _document racine Ouvrir le _dossier du bloc-notes Ouvrir la _Page Ouvrir le lien de la cellule Afficher l'aide Ouvrir dans une nouvelle fenêtre Ouvrir dans une nouvelle _fenêtre Ouvrir une nouvelle page Ouvrir le dossier des plugins Ouvrir avec « %s » Facultatif Options du greffon %s Autre... Fichier de sortie Le fichier d'export existe, utiliser "--overwrite" pour forcer l'export Dossier de destination Le dossier d'export existe et n'est pas vide, utiliser "--overwrite" pour forcer l'export Une destination est nécessaire pour l'export La sortie remplace la sélection actuelle Écraser _Barre d'adresse Page La page « %s » n'a pas de dossier pour les pièces jointes Index des pages Nom de la page Modèle de page Cette page existe déjà: %s Page non autorisée: %s Section de la page Paragraphe Coller Barre de chemin Saisissez un commentaire pour cette version Remarquez que lier une page non-existante
aura pour effet de créer une nouvelle page automatiquement. Veuillez choisir un nom et un dossier pour le bloc-notes. Sélectionner une ligne avant d'appuyer sur le bouton. Veuillez sélectionner plus d'une ligne de texte Merci de sélectionner un bloc-notes Greffon Le greffon "%s" est nécessaire pour afficher cet objet Greffons Port Position dans la fenêtre _Préférences Préférences Préc. Imprimer vers le navigateur Promouvoir Proprié_tés Propriétés Passer les évènements au démon Zeitgeist Note rapide Note rapide... Changements récents Changements récents... Pages _récemment modifiées Reformater les balises wiki à la volée Supprimer Tout supprimer Supprimer la colonne Supprimer la ligne Supprimer les liens Renommer la page « %s » Des clics répétées sur la case à cocher permettent de naviguer entre les états Remplacer _tout Remplacer par Revenir à la version enregistrée ? Rev Droite Panneau latéral droit Position de la marge de droite Ligne vers le bas Ligne vers le haut Enregi_strer la version... Enregistrer une _copie... Enregistrer une copie Enregistrer la version Enregitsrer les marque-pages Version enregistrée depuis zim Résultat Commande de capture d'écran Rechercher Rechercher les _rétroliens Chercher dans cette section Section Section(s) à ignorer Section(s) à indexer Sélectionner un fichier Sélectionner un dossier Sélectionner une image Sélectionner une version pour voir les différences entre celle-ci et l'état actuel.
Ou sélectionner plusieurs versions pour voir les différences entre elles.
 Sélectionner le format d'exportation Sélectionner le fichier ou le dossier de destination Sélectionner les pages à exporter Sélectionner une fenêtre ou une zone Sélection Diagramme de séquences Serveur non-démarré Serveur démarré Serveur arrêté Nouveau nom Définir l'éditeur de texte par défaut Associer à la page actuelle Afficher les numéros de ligne Montrer la liste des tâches sans hiérarchie Afficher la table des matières sous forme d'objet flottant au lieu d'un panneau latéral Afficher les _Changements Afficher une icône pour chaque bloc-notes Montrer le nom de la page en entier Afficher l'intégralité du nom de la page Afficher la barre d'outils d'aide Afficher dans la barre d'outils Afficher la marge de droite Montrer la liste des tâches dans une panneau latéral Afficher le curseur en lecture seule Afficher le titre de la page dans la table des matières _Page unique Taille Touche Accueil Une erreur est survenue lors de l'exécution de "%s" Trier alphabétiquement Trier les pages par étiquette Visualiseur de source Correcteur d'orthographe Début Démarrer le serveur _web Sy_mbole... Syntaxe Valeur par défaut du système Largeur des tabulations Tableau Éditeur de tableaux Table des Matières Étiquettes Etiquettes pour les tâches non-déclenchables Tâche Liste des tâches Tâches Modèle Modèles Texte Fichiers texte Texte à partir d'un _fichier... Couleur de fond du texte Couleur du texte Le dossier
%s
n'existe pas.
Voulez-vous le créer ? Le dossier "%s" n'existe pas.
Voulez-vous le créer ? Les paramètres suivant vont être remplacés
dans la commande quand elle sera exécutée :
<tt>
<b>%f</b> le fichier temporaire de la page source
<b>%d</b> le dossier de fichiers liés de la page actuelle
<b>%s</b> le vrai fichier source de la page (s'il y en a un)
<b>%p</b> le nom de la page
<b>%n</b> l'emplacement du bloc-notes (fichier ou dossier)
<b>%D</b> la racine du document (s'il y en a)
<b>%t</b> le texte sélectionné ou le mot sous le curseur
<b>%T</b> le texte sélectionné incluant le formattage wiki
</tt>
 La calculatrice en ligne n'a pas pu
évaluer l'expression au curseur. Le tableau doit contenir au moins une ligne.
Suppression annulée. Il n'y a aucune modification de ce bloc-notes depuis la dernière version sauvegardée Cela peut signifier que vous n'avez pas le bon
dictionnaire installé. Le fichier existe déjà.
Voulez-vous l'écraser ? Cette page n'a pas de dossier pour pièces jointes Ce nom de page ne peut être utilisé en raison de limitation technique du stockage Ce greffon permet de prendre une capture d'écran et de
l'insérer directement dans une page zim.

C'est un greffon de base livré avec zim
 Ce plugin ajoute une "barre de chemin" en haut de la fenêtre.
Cette barre peut afficher le chemin de la page courante dans le bloc-notes,
les pages récemment visitées ou les pages récemment modifiées.
 Ce greffon ajoute une boite de dialogue listant toutes 
les taĉhes en cours dans le bloc-notes. Ces tâches peuvent
être soit des cases à cocher, soit des lignes marquées par un
mot-clé comme « TODO » ou « FIXME ».

C'est un greffon de base livré avec zim
 Ce greffon fournit une boîte de dialogue pour ajouter rapidement
du texte ou le contenu du presse-papier dans une page zim.

C'est un greffon de base livré avec zim.
 Ce greffon ajoute une icône de notification pour un accès rapide.

Il dépend de Gtk+, version 2.10 ou supérieure.

C'est un greffon de base livré avec zim.
 Ce greffon ajoute une icône supplémentaire affichant une liste
des pages liées à la page courante.

C'est un greffon de base livré avec zim.
 Ce greffon ajoute un objet supplémentaire affichant une
table des matières de la page courante.

C'est un greffon de base livré avec zim.
 Ce plugin permet de transformer Zim en
"éditeur sans distraction".
 Ce greffon ajoute la boîte de dialogue « Insérer un symbole »
et permet une mise en forme automatique des caractères
typographiques.

C'est un greffon de base livré avec zim.
 Ce greffon ajoute le panneau de l'index des pages à la fenêtre principale.
 Ce greffon permet de gérer des versions de notebooks.

Ce greffon soutien les systèmes de gestion de versions Bazaar, Git et Mercurial. 

C'est un greffon de base livré avec zim.
 Cette extension permet d'insérer des blocs de code dans la page. Ils
seront affichés sous la forme d'un composant graphique intégré avec
coloration syntaxique, numéro de lignes, etc.
 Ce greffon permet d'intégrer des calculs arithmétiques dans Zim.
Il est basé sur le module arithmétique suivant:
http://pp.com.mx/python/arithmetic.
 Ce greffon vous permet d'évaluer rapidement les
expressions mathématiques simple dans zim.
C'est un greffon de base livré avec zim.
 Ce greffon a aussi des propriétés,
consultez la fenêtre des propriétés du bloc-notes. Ce greffon fournit un éditeur de diagrammes pour zim, basé sur Ditaa.

C'est un greffon de base livré avec zim.
 Ce greffon fournit un éditeur de diagrammes basé sur GraphViz.

C'est un greffon de base livré avec Zim.
 Ce greffon fournit une représentation graphique
des liens entre les pages du bloc-notes.
C'est une sorte de « carte des idées » (ou "mind map").

C'est un greffon de base livré avec Zim.
 Ce greffon fournit un index de pages filtré par les étiquettes sélectionnées
dans une liste.
 Ce greffon fournit un éditeur de graphes pour Zim basé sur GNU R.
 Ce greffon fournit un éditeur de graphe pour zim basé sur Gnuplot.
 Ce greffon fournit un éditeur de diagramme de séquence basé sur seqdiag.
Il facilite l'édition de diagrammes de séquences.
 Ce greffon permet de contourner l'absence de fonctionnalités
d'impression dans zim. Il exporte la page courante en HTML
et ouvre un navigateur web.  Si celui-ci a la possibilité
d'imprimer, il enverra vos données à l'imprimante en
deux étapes.

C'est un greffon de base livré avec zim.
 Ce greffon fournit un éditeur d'équations basé sur latex.

C'est un greffon de base livré avec Zim.
 Ce greffon fournit un éditeur de partition musicale basé sur GNU Lilypond.

C'est un greffon de base livré avec zim.
 Ce greffon affiche le dossier des pièces attachées sous
forme d'une icône au bas du panneau.
 Ce greffon trie les lignes sélectionnées par ordre alphabétique.
Si la liste est déjà triée, elle le sera de nouveau en sens inverse
(A-Z vers Z-A).
 Ce greffon transforme une section d'un bloc-note en journal
avec une page par jour, semaine ou mois.
Il ajoute aussi un calendrier permettant d'accéder à ces pages.
 Ceci signifie généralement que le fichier contient des caractères invalides Titre Pour continuer, vous pouvez enregistrer une copie de la page ou abandonner 
les modifications. Si vous enregistrez une copie, les modifications seront aussi
abandonnées mais vous pourrez récupérer la copie plus tard. Pour créer un nouveau bloc-notes, vous devez sélectionner un dossier.
Vous pouvez évidemment sélectionner le dossier d'un bloc-notes existant.
 Contenu Aujour_d'hui Aujourd'hui Alterner la case '>' (Dé)Cocher la case 'V' (Dé)Cocher la case 'X' Panneau supérieur Icône de notification Transformer le nom de la page en tags pour les items de tache Type Décocher la case Désindenter avec la touche <Retour Arrière>
(si désactiver, vous pouvez toujours utiliser <Shift><Tab>) Inconnu Type d'image inconnu Objet inconnu Non spécifiée Non étiquetté Mettre à jour %i page pointant sur cette page Mettre à jour %i pages pointant sur cette page Mettre à jour l'en-tête de cette page Mise à jour des liens Mise à jour de l'index Utiliser %s pour basculer vers le panneau latéral Utiliser une police personnalisée Utiliser une page pour chacun Utiliser les dates des pages du journal Utiliser la touche <Entrée> pour suivre les liens
(si désactivé vous pouvez encore employer <Alt><Entrée>) Gestion de version La gestion de versions n'est pas activée pour ce bloc-notes.
Souhaitez-vous l'activer ? Versions Voir les _annotations Consulter le _journal Serveur web Semaine Lors du rapport sur ce bogue, veuillez inclure
l'information de la boîte de texte ci-dessous Mot _entier Largeur Page Wiki : %s Avec ce greffon, vous pouvez insérer un tableau dans une page wiki. Les tableaux seront représentés par 
un composant GTK TreeView. Ils pourront être exportés en différents formats (HTML/LaTex par exemple).
 Statistiques Nombre de mots... Mots Année Hier Vous êtes en train de modifier un fichier avec un éditeur externe. Vous pouvez fermer cette fenêtre de dialogue lorsque vous aurez terminé Vous pouvez configurer des outils personnalisés qui 
apparaîtront dans le menu « Outils » et dans la barre d'outils
ou les menus contextuels. Zim, le Wiki de bureau _Dézoomer _À propos _Ajouter _Tous les panneaux _Arithmétique _Précédent _Parcourir _Bugs Ann_uler _Case à cocher _Enfant _Effacer les styles _Fermer Tout _replier _Contenus _Copier _Copier ici _Supprimer _Supprimer la page _Annuler les modifications _Dupliquer la ligne _Édition Mo_difier le lien É_diter lien ou objet... _Editer les propriétés _Modifier… _Italique _FAQ _Fichier Re_chercher _Rechercher... _Suivant _Plein écran A_ller à _Aide S_urligner _Historique _Accueil _Image... _Importer la page... _Insérer Aller à la ligne A_ller à... _Raccourcis clavier _Lien _Lier à une date _Lien... _Marquer _Plus _Déplacer _Déplacer ici _Descendre la ligne _Monter la ligne _Nouvelle page ici... _Nouvelle page... _Suivant _Aucun Taille _normale Liste _numérotée _OK _Ouvrir _Ouvrir un autre bloc-notes _Autre... _Page Hiérarchie de la _page _Parent C_oller _Aperçu _Précédent _Imprimer _Imprimer vers le navigateur _Note rapide... _Quitter _Pages récentes _Rétablir Expression _régulière _Actualiser _Supprimer _Supprimer la ligne _Retirer le lien Re_mplacer _Remplacer... _Supprimer la taille _Restaurer la version _Enregistrer _Enregistrer la copie _Capture d'écran... _Rechercher _Rechercher... _Envoyer à... Panneaux _latéraux _Côte à côte _Trier les lignes _Barré _Gras _Indice E_xposant _Modèles _Outils Ann_uler _Verbatim _Versions... _Affichage _Zoomer comme date d'échéance pour les tâche comme date de début calendar:week_start:1 ne pas utiliser Lignes horizontales Pas de grille Lecture seule secondes Jean Demartini
Launchpad Contributions:
  BobMauchin https://launchpad.net/~zebob.m
  Calinou https://launchpad.net/~calinou
  Daniel Stoyanov https://launchpad.net/~dankh
  Etienne LB https://launchpad.net/~etiennelb-deactivatedaccount
  François Boulogne https://launchpad.net/~sciunto.org
  Herve Robin https://launchpad.net/~robin-herve
  Jaap Karssenberg https://launchpad.net/~jaap.karssenberg
  Jean DEMARTINI https://launchpad.net/~jean-demartini-dem-tech
  Jean-Baptiste Holcroft https://launchpad.net/~jibecfed
  Jean-Marc https://launchpad.net/~m-balthazar
  Jean-Philippe Rutault https://launchpad.net/~rutault-jp
  Jigho https://launchpad.net/~jigho
  Joseph Martinot-Lagarde https://launchpad.net/~contrebasse
  Jérôme Guelfucci https://launchpad.net/~jerome-guelfucci-deactivatedaccount
  Kernel https://launchpad.net/~larrieuandy
  LEROY Jean-Christophe https://launchpad.net/~celtic2-deactivatedaccount
  Le Bouquetin https://launchpad.net/~damien-accorsi
  Loïc Meunier https://launchpad.net/~lomr
  Makidoko https://launchpad.net/~makidoko
  Pascollin https://launchpad.net/~pascollin
  Quentin THEURET @Amaris https://launchpad.net/~qtheuret
  Raphaël Hertzog https://launchpad.net/~hertzog
  Rui Nibau https://launchpad.net/~ruinibau
  Steve Grosbois https://launchpad.net/~steve.grosbois
  Stéphane Aulery https://launchpad.net/~lkppo
  TROUVERIE Joachim https://launchpad.net/~joachim-trouverie
  Thomas LAROCHE https://launchpad.net/~mikaye
  Xavier Jacquelin https://launchpad.net/~ygster
  andré https://launchpad.net/~andr55
  marius DAVID https://launchpad.net/~marius851000
  oswald_volant https://launchpad.net/~obordas
  pitchum https://launchpad.net/~pitchum
  samuel poette https://launchpad.net/~fracte Lignes verticales Avec des lignes {count} sur {total} 