��    �                      .      ?   <      |      �   0   �      �      �      !  	   !     !  i   -!  *   �!     �!     �!  
   �!     �!  V   �!     I"  	   O"  	   Y"     c"  3   w"  Y   �"     #     #  
   (#     3#     ?#     R#     e#     q#  $   x#  ?   �#  /   �#  (   $  %   6$  	   \$     f$     n$     u$     �$     �$  
   �$     �$     �$     �$     �$     �$     �$  
   �$     �$     %     )%  <   8%     u%  	   {%  
   �%     �%     �%     �%     �%     �%     �%     �%  +   &  3   -&      a&     �&     �&     �&  
   �&     �&     �&     �&     '  3   !'     U'     s'     �'     �'     �'     �'     �'     �'      (     (     (     ((     -(     1(  0   9(     j(     {(     �(     �(  
   �(     �(     �(     �(     �(     �(  ~   �(     _)  
   m)     x)  
   �)  	   �)  
   �)     �)     �)     �)     �)     �)  5   �)     %*     4*     @*  !   G*     i*  #   z*     �*     �*     �*     �*     �*     �*     +     !+     -+     F+     b+     k+     r+  
   w+     �+     �+  	   �+     �+  v   �+     *,  *   <,  c   g,     �,     �,     �,     �,     -     -  
   -  
   #-  
   .-  
   9-  
   D-  
   O-     Z-     a-     �-  	   �-     �-     �-     �-     �-     �-  
   �-     �-     �-      .     .     $.     1.     A.     S.     b.     o.     {.     �.     �.     �.     �.  	   �.     �.     �.     �.     /     /     $/     2/     I/     N/     ]/     i/     o/  2   x/     �/     �/     �/     �/     �/     �/     0     )0     50  	   H0     R0     [0     a0     w0     �0     �0      �0  *   �0     �0     �0     �0     1     1     /1     E1     c1     s1     �1     �1     �1  	   �1     �1     �1     �1     �1     �1     
2     2  
   42  	   ?2     I2     ]2     k2     z2     �2     �2     �2  9   �2     �2  I   �2  !   @3  '   b3  	   �3     �3     �3  0   �3  	   �3     �3     �3  	   �3  '   4  V   *4  3   �4  0   �4     �4      5     5     5     5     +5     85     D5     U5     ]5  
   i5  
   t5     5     �5     �5     �5     �5     �5  
   �5     �5     6     6     ,6     96     X6     \6     b6     r6     �6     �6  	   �6     �6     �6     �6     �6     �6     �6     7     7      7     .7  �   ;7     �7      �7     8      8  	   88     B8     S8     f8     u8     �8     �8  2   �8     �8  &   �8     9     *9     >9  5   P9     �9     �9  &   �9     �9     �9     �9     �9      :  
   :     :     $:  	   3:     =:     C:     P:     b:     g:  	   l:     v:  	   :     �:  
   �:     �:     �:     �:  ?   �:  A   ;  S   Z;  =   �;  K   �;  @   8<  6   y<  -   �<  x   �<  �   W=  �    >  �   �>  �   3?  }   �?  �   2@  �   �@  �   dA  }   �A  h   qB  k   �B  �   FC  R   D  ;   mD  =   �D  v   �D    ^E  j   rF     �F  7   ]G     �G  �   �G     7H     ;H     BH     HH     \H     pH  	   yH  '   �H     �H  D   �H     �H     �H     	I  H   I     [I     {I     �I     �I     �I  P   �I     J  U    J     vJ     J  	   �J  
   �J     �J  N   �J     �J     K     
K  �   K  
   �K     �K     �K     �K  	   �K  ^   L  f   `L     �L  	   �L     �L  
   �L     �L      M     M     M     M     M     -M     4M  	   BM     LM  
   RM     ]M     eM     rM     �M  
   �M     �M     �M  	   �M     �M     �M     �M     �M     �M     �M     �M  
   �M     N     N  	   N     N     .N     6N     BN     ON     UN     cN     lN     rN     xN  
   ~N     �N     �N     �N     �N     �N     �N     �N  	   �N     �N     �N     �N     �N  	   O     O     "O     1O     7O     EO     KO     _O     gO     tO     }O     �O     �O     �O  
   �O     �O     �O  
   �O     �O     �O     �O     �O     P     P  
   P     &P  
   3P     >P     EP  	   KP     UP     bP     hP     qP     �P     �P     �P     �P     �P     �P  
   �P  �  �P  [   �R  �   S     �S  L   �S  Q   �S  Q   QT  Z   �T     �T     U  =   U  �   \U  ^   .V  #   �V  -   �V     �V     �V  �   W     X     %X     >X  9   ]X  �   �X  �   'Y  5   �Y      Z     3Z  !   HZ  ,   jZ  +   �Z     �Z     �Z  F   �Z  �   2[  s   �[  `   +\  j   �\  	   �\     ]     ]     &]     >]  !   O]     q]  B   �]  
   �]  5   �]     ^     ^     ,^     ;^  K   P^  &   �^      �^  �   �^     �_  %   �_     �_     �_     �_  <   �_     :`  1   G`  !   y`  !   �`  T   �`  [   a  A   na  -   �a  #   �a     b      b  (   ?b  ;   hb  F   �b  5   �b  u   !c  I   �c  '   �c  8   	d  %   Bd  @   hd  M   �d  $   �d     e  +   ,e  ,   Xe     �e     �e     �e     �e  {   �e  5   Pf     �f     �f  -   �f     �f     g     g     3g     Fg  0   _g    �g  2   �h     �h  F   �h  %   3i  #   Yi  #   }i  $   �i     �i  )   �i  ?   j     Gj  Y   Tj  ?   �j     �j     	k  F   k  '   _k  K   �k  +   �k     �k  '   l  G   6l  !   ~l     �l  B   �l  '   m  :   *m  A   em     �m     �m     �m     �m  &    n  .   'n     Vn     pn  �   n  )   {o  Q   �o    �o     �p  
   q  J    q  H   kq     �q     �q  !   �q     �q     �q     r     0r     Jr     dr  ^   mr  6   �r     s     s     0s     ?s  +   _s     �s  !   �s  @   �s  (   t  7   *t  )   bt     �t  %   �t     �t     �t     �t     u  6   <u  >   su  !   �u     �u  5   �u     (v     7v     Wv     fv  $   |v  D   �v  )   �v  E   w     Vw  ,   gw  !   �w     �w     �w  a   �w     Ax     Ux  !   hx  j   �x  ?   �x  .   5y  b   dy  1   �y  (   �y  	   "z     ,z  
   Cz  (   Nz  @   wz  *   �z  
   �z  J   �z  r   9{     �{     �{     �{     �{     |  (   +|  ^   T|     �|  #   �|  2   �|  A   *}     l}     �}     �}  5   �}     �}  +   ~     1~  B   H~  ;   �~     �~     �~  ,     &   2     Y     r  0   �     �     �  �   �     a�  �   }�  J   "�  e   m�     Ӂ      �     �  H   �     e�     �     ��     ��  G   ΂  �   �  t   ߃  \   T�  >   ��     ��     �     �  )   �     E�     ]�  ,   t�     ��     ��     ƅ     م  "   ��  -   �  0   J�  =   {�  P   ��     
�     (�  ,   H�  1   u�  $   ��     ̇  Q   �     >�  
   G�  $   R�  4   w�  $   ��  6   ш  )   �  #   2�  5   V�     ��  =   ��     ߉  '   �     �     '�     E�     c�  5  ��  /   ��  C   �  3   +�  5   _�     ��  '   ��  7   ̌  0   �  4   5�  S   j�  .   ��  v   �      d�  e   ��  ?   �  2   +�  2   ^�  �   ��     (�     =�  a   L�  )   ��  9   ؐ     �  %   ,�  3   R�     ��     ��  )   ��     ґ     �  %   ��  '   %�     M�     ^�     m�     ��     ��     ��     ��  &   Ԓ  (   ��  0   $�  o   U�  q   œ  �   7�  �   �  �   ��  �   E�  _   Ӗ  L   3�     ��  +  ��  h  ͚  �  6�  o  ͝  $  =�  *  b�  _  ��  �   ��  �   �  �   Ƥ    ��  �  ��  �   ��  �   j�  �   ��  %  ��  �  ��  �   I�    F�  �   J�     ۰  �  �     x�     }�     ��     ��     ��     Ӳ  8   �  i   $�  
   ��  �   ��     H�     W�     s�  �   ��  M   [�  .   ��  '   ص  D    �  0   E�  �   v�     �  �   <�     �  )   ��  &   #�  #   J�     n�  �   ��     J�     a�     n�  ~  ��     �     "�     >�     K�     T�  �   ]�  �   �  .   �     A�     T�     i�     ��  	   ��     ��     ��  	   ʽ  .   Խ     �     �     0�     H�     \�     w�      ��      ��     ˾  "   �  C   �  *   J�     u�      ��     ��     ��     ÿ     ӿ     �     ��     �     $�     6�     D�  #   U�     y�     ��  4   ��     ��  +   ��     �     $�     :�     R�     h�     ��     ��     ��      ��      ��     ��  8   �     F�     S�  	   a�     k�     ��     ��  -   ��  #   ��     �  "   �  "   9�      \�     }�     ��     ��     ��  $   ��  "   �     9�  *   O�  $   z�     ��     ��     ��     ��     �  #   $�     H�     b�     p�     ��     ��     ��     ��     ��     ��     ��     ��  2   
�  #   =�  *   a�      ��     ��  �  ��     ��     ��   %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i errors occurred, see log %i file will be deleted %i files will be deleted %i open item %i open items %i warnings occurred, see log <Top> <Unknown> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Add Application Add Notebook Add column Add row Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files All Tasks Allow public access Always use last cursor position when opening a page An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Arithmetic Attach File Attach image first Attachment Browser Attachments Author Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave version on regular intervals BackLinks Backend Bazaar Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Capture whole screen Center Change columns Changes Characters Characters excluding spaces Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Clone row Code Block Column 1 Command Command does not modify data Comment Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Demote Dependencies Description Details Discard note? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root E_xport... Edit Custom Tool Edit Image Edit Link Edit Table Edit _Source Editing Editing file: %s Enable Version Control? Enabled Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Get more plugins online Get more templates online Git Gnuplot Grid lines Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide menubar in fullscreen mode Highlight current line Home Page Icon Images Import Page Include subpages Index Index page Inline Calculator Insert Code Block Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Screenshot Insert Sequence Diagram Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Labels marking tasks Last Modified Leave link to new page Left Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log file Looks like you found a bug Make default application Managing table columns Map document root to URL Match _case Maximum page width Mercurial Modified Month Move Selected Text... Move Text to Other Page Move text to Name Need output file to export MHTML Need output folder to export full notebook New File New Page New Page in %s New S_ub Page... New _Attachment No Applications Found No changes since last version No dependencies No such file: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open help Open in New _Window Open new page Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Name Page Template Page section Paragraph Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please select a row, before you push the button. Please specify a notebook Plugin Plugins Port Position in the window Pr_eferences Preferences Print to Browser Promote Proper_ties Properties Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove column Remove row Removing Links Rename page "%s" Replace _All Replace with Restore page to saved version? Rev Right Right Side Pane Right margin position S_ave Version... Save A _Copy... Save Copy Save Version Saved version from zim Score Screenshot Command Search Search _Backlinks... Section Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Sequence Diagram Server not started Server started Server stopped Set default text editor Show Line Numbers Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show full page name Show in the toolbar Show right margin Show the cursor also for pages that can not be edited Single _page Size Some error occurred while running "%s" Sort alphabetically Sort pages by tags Source View Spell Checker Start _Web Server Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Task Task List Template Templates Text Text Files Text From _File... Text background color Text foreground color The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The inline calculator plugin was not able
to evaluate the expression at the cursor. The table must consist of at least on row!
 No deletion done. There are no changes in this notebook since the last version that was saved This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a sequence diagram editor for zim based on seqdiag.
It allows easy editing of sequence diagrams.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. ToC To_day Today Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Tray Icon Turn page name into tags for task items Type Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unspecified Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use a custom font Use a page for each Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s With this plugin you can embed a 'Table' into the wiki page. Tables will be shown as GTK TreeView widgets.
Exporting them to various formats (i.e. HTML/LaTeX) completes the feature set.
 Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Zim Desktop Wiki Zoom _Out _About _All Panes _Arithmetic _Back _Browse _Bugs _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Edit _Edit Link _Edit Link or Object... _Edit Properties _Emphasis _FAQ _File _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _Move Here _New Page... _Next _None _Normal Size _Numbered List _Open _Open Another Notebook... _Other... _Page _Parent _Paste _Preview _Previous _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In calendar:week_start:0 horizontal lines no grid lines readonly seconds translator-credits vertical lines with lines Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2021-02-09 19:50+0000
Last-Translator: Michalis <michalisntovas@yahoo.gr>
Language-Team: Greek <https://hosted.weblate.org/projects/zim/master/el/>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.5-dev
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
 %(cmd)s
Επεστράφει μη μηδενική κατάσταση εξόδου %(code)i %(n_error)i σφάλματα και %(n_warning)i προειδοποιήσεις συνέβησαν, δείτε την καταγραφή %A, %d %B %Y %i σφάλματα συνέβησαν, δείτε την καταγραφή %i αρχείο θα διαγραφεί %i αρχεία θα διαγραφούν %i ανοικτό αντικείμενο %i ανοικτά αντικείμενα %i προειδοποιήσεις συνέβησαν, δείτε την καταγραφή <Κορυφή> <Άγνωστο> Ένα Wiki για την Επιφάνεια Εργασίας Υπάρχει ήδη ένα αρχείο με το όνομα <b>"%s"</b>.
Μπορείτε να δώσετε άλλο όνομα ή να αντικαταστήσετε το υφιστάμενο αρχείο. Ο πίνακας απαιτείται να έχει τουλάχιστον μια στήλη. Προσθήκη Εφαρμογής Προσθήκη σημειωματάριου Προσθήκη στήλης Προσθήκη γραμμής Το πρόσθετο αυτό προσθέτει υποστήριξη ορθογραφικού ελέγχου
με χρήση του gtkspell.

Είναι ένα από τα πρόσθετα του zim και παρέχεται μαζί του.
 Στοίχιση Όλα τα αρχεία Όλες οι εργασίες Επιτρέπεται η δημόσια πρόσβαση Να χρησιμοποιείται πάντα η τελευταία θέση του κέρσορα όταν ανοίγει μια σελίδα Παρουσιάστηκε σφάλμα κατά τη δημιουργία της εικόνας.
Θέλετε παρόλα αυτά να αποθηκεύσετε το πηγαίο αρχείο; Κώδικας Σελίδας με Ενδείξεις Εφαρμογές Αριθμητική Επισύναψη αρχείου Επισύναψη εικόνων πρώτα Περιηγητής επισυνάψεων Συνημμένα Δημιουργός Αυτόματα αποθηκευμένη έκδοση από το zim Αυτόματη επιλογή της τρέχουσας λέξης όταν γίνεται εφαρμογή διαμόρφωσης Αυτόματη μετατροπή λέξεων γραμμένων σε μορφή "CamelCase" σε δεσμούς Αυτόματη μετατροπή των διαδρομών αρχείων σε δεσμούς Αυτόματη αποθήκευσης έκδοσης σε τακτά χρονικά διαστήματα BackLinks Σύστημα ελέγχου Bazaar Κάτω Πλαίσιο Πλοήγηση Λίσ_τα με κουκίδες Ρύ_θμιση Αδύνατη η τροποποίηση της σελίδας: %s Άκυρο Σύλληψη ολόκληρης της οθόνης Κέντρο Αλλαγή στηλών Αλλαγές Χαρακτήρες Χαρακτήρες εξαιρουμένων των διαστημάτων Έλεγχος ορ_θογραφίας Λίστα με κουτάκια Κλασικό εικονίδιο συστήματος.
Να μη χρησιμοποιηθεί το νέου στυλ εικονίδιο κατάστασης στο Ubuntu Εκκαθάριση Κλωνοποίηση γραμμής Μπλοκ κώδικα Στήλη 1 Εντολή Η εντολή δεν τροποποιεί δεδομένα Σχόλιο Ο_λόκληρο το σημειωματάριο Ρύθμιση Εφαρμογών Ρύθμιση προσθέτου Ρύθμιση εφαρμογής για το άνοιγμα "%s" συνδέσμων Ρύθμιση εφαρμογής για το άνοιγμα
αρχείων τύπου "%s" Θεώρησε όλα τα κουτάκια ως εργασίες Αντιγραφή διεύθυνσης email Αντιγραφή Προτύπου _Αντιγραφή Ως... Αντιγραφή _δεσμού Αντιγραφή _Τοποθεσίας Αδυναμία εύρεσης εκτελέσιμου "%s" Αδύνατη η εύρεση του σημειωματάριου: %s Αδυναμία εύρεσης πρότυπου "%s" Αδύνατη η εύρεση του αρχείου ή φακέλου αυτού του σημειωματάριου Αδυναμία φόρτωσης ορθογραφικού ελέγχου Αδύνατο ανοίγματος: %s Αδύνατη η ανάλυση της έκφρασης Ανέφικτη ανάγνωση: %s Αδύνατη η αποθήκευση της σελίδας: %s Δημιουργία νέας σελίδας για κάθε σημείωση Δημιουργία φακέλου; Απ_οκοπή Προσαρμοσμένα εργαλεία Προσαρμοσμένα _εργαλεία Προσαρμογή... Ημερομηνία Ημέρα Προεπιλογή Προεπιλεγμένη μορφοποίηση για την αντιγραφή κειμένου στο πρόχειρο Προεπιλεγμένο σημειωματάριο Καθυστέρηση Διαγραφή σελίδας Να διαγραφεί η σελίδα "%s"; Διαγραφή γραμμής Υποχώρηση Εξαρτήσεις Περιγραφή Λεπτομέρειες Να απορριφθεί το σημείωμα; Θέλετε να επαναφέρετε τη σελίδα: %(page)s
στην αποθηκευμένη έκδοση: %(version)s;

Όλες οι αλλαγές που έγιναν μετά την τελευταία
αποθήκευση της σελίδας, θα χαθούν! Ριζικός κατάλογος εγγράφου Εξαγ_ωγή... Επεξεργασία προσαρμοσμένου εργαλείου Επεξεργασία εικόνας Επεξεργασία δεσμού Επεξεργασία πίνακα Επεξεργασία _κώδικα Επεξεργασία Επεξεργασία αρχείου: %s Ενεργοποίηση του ελέγχου εκδόσεων Ενεργό Σφάλμα στο %(file)s στη γραμμή %(line)i κοντά στο "%(snippet)s" Υπολογισμός _μαθηματικής έκφρασης Επέκτ_αση όλων Εξαγωγή Εξαγωγή όλων των σελίδων σε ένα αρχείο Εξαγωγή ολοκληρώθηκε Εξαγωγή κάθε σελίδας σε ξεχωριστό αρχείο Εξαγωγή σημειωματάριου Απέτυχε Αποτυχία εκτέλεσης: %s Αποτυχημένη εκτέλεσης της εφαρμογής: %s Υφιστάμενο αρχείο Πρότυπα _Αρχείων Το αρχείο τροποποιήθηκε στο δίσκο: %s Το αρχείο υπάρχει ήδη Το αρχείο δεν είναι εγγράψιμο: %s Μη υποστηριζόμενος τύπος αρχείου: %s Όνομα αρχείου Φίλτρο Αναζήτηση Εύρεση ε_πόμενου Εύρεση προη_γούμενου Εύρεση και Αντικατάσταση Αναζήτηση του Φάκελος Ο φάκελος υπάρχει ήδη και δεν είναι κενός. Αν κάνετε εξαγωγή σε αυτόν, είναι πιθανό να επικαλύψετε υπάρχοντα αρχεία. Θέλετε να συνεχίσετε; Υφιστάμενος φάκελος: %s Ο Φάκελος με πρότυπα για τα συννημένα αρχεία Στην προχωρημένη αναζήτηση μπορείτε να χρησιμοποιήσετε
τους τελεστές AND, OR και NOT. Ανατρέξτε στη σελίδα βοήθειας
για περισσότερες πληροφορίες. Μορ_φοποίηση Μορφή Απέκτησε πιο πολλά πρόσθετα διαδικτυακά Απέκτησε πιο πολλά πρότυπα διαδικτυακά Git Gnuplot Γραμμές πλέγματος Επικεφαλίδα _1 Επικεφαλίδα _2 Επικεφαλίδα _3 Επικεφαλίδα _4 Επικεφαλίδα _5 Ύψος Απόκρυψη μπάρας μενού σε λειτουργία πλήρους οθόνης Επισήμανση τρέχουσας γραμμής Αρχική σελίδα Εικονίδιο Εικόνες Εισαγωγή σελίδας Συμπερίλιψη υποσελίδων Ευρετήριο Σελίδα ευρετηρίου Υπολογιστής μαθηματικών εκφράσεων Εισαγωγή μπλοκ κώδικα Εισαγωγή ημερομηνίας και ώρας Εισαγωγή διαγράμματος Εισαγωγή Ditaa Εισαγωγή συνάρτησης Εισαγωγή GNU R Plot Εισαγωγή Gnuplot Εισαγωγή εικόνας Εισαγωγή δεσμού Εισαγωγή στιγμιότυπου οθόνης Εισαγωγή διαγράμματος ακολουθίας Εισαγωγή συμβόλου Εισαγωγή πίνακα Εισαγωγή κειμένου από αρχείο Διεπαφή Interwiki Λέξη-κλειδί Χρονικό Μετάβαση σε Μετάβαση στη σελίδα Ετικέτες που καταδεικνύουν εργασίες: Τελευταία Τροποποίηση Παραμονή συνδέσμου προς τη νέα σελίδα Αριστερά Αριστερό Πλαϊνό Πλαίσιο Ταξινόμος γραμμών Γραμμές Χάρτης δεσμών Χρήση πλήρους διαδρομής σε δεσμούς για τοπικά αρχεία Σύνδεση με Τοποθεσία Αρχείο καταγραφής Απ' ότι φαίνεται μόλις βρήκατε ένα σφάλμα προγραμματισμού Ορισμός ως προεπιλεγμένη εφαρμογή Διαχείριση στηλών πίνακα Αντιστοίχηση του ριζικού καταλόγου του εγγράφου σε URL Ταίριασμα πεζών/_κεφαλαίων Μέγιστο μήκος σελίδας Mercurial Τροποποίηση Μήνας Μετακίνηση Επιλογής... Μετακίνηση Κειμένου σε Άλλη Σελίδα Μετακίνηση κειμένου σε Όνομα Απαιτείται αρχείο εξόδου για εξαγωγή MHTML Απαιτείται φάκελος εξόδου για εξαγωγή πλήρους σημειωματαρίου Νέο Αρχείο Νέα σελίδα Νέα σελίδα στο %s Νέα _υποσελίδα... Νέο _Συννημένο Δε βρέθηκαν Εφαρμογές Καμία αλλαγή από την τελευταία αποθηκευμένη έκδοση Χωρίς εξαρτήσεις Ανύπαρκτο αρχείο: %s Δεν έχει οριστεί τέτοι wiki: %s Δεν υπάρχουν εγκατεστημένα πρότυπα Σημειωματάριο Σημειωματάρια Εντάξει Άνοιγμα _φακέλου επισυνάψεων Άνοιγμα φακέλου Άνοιγμα σημειωματάριου Άνοιγμα με... Άνοιγμα αρχι_κού καταλόγου εγγράφου Ά_νοιγμα φακέλου σημειωματάριων Άνοιγμα Σελίδας Άνοιγμα βοήθειας Άνοιγμα σε νέο παρά_θυρο Άνοιγμα νέας σελίδας Άνοιγμα με "%s" Προαιρετική Επιλογές για το πρόσθετο %s Άλλο... Αρχείο εξόδου Ο φάκελος εξόδου υπάρχει, καθορίστε "--overwrite" για εξαναγκασμένη εξαγωγή Φάκελος εξόδου Ο φάκελος εξόδου υπάρχει και δεν είναι κενός, καθορίστε "--overwrite" για εξαναγκασμένη εξαγωγή Τοποθεσία εξόδου χρειάζεται για εξαγωγή Η έξοδος πρέπει να αντικαταστήσει την τρέχουσα επιλογή Αντικατάσταση Γρ_αμμή διαδρομής Σελίδα Η σελίδα "%s" δεν έχει φάκελο επισυνάψεων Όνομα σελίδας Πρότυπο Σελίδας Τομέας σελίδας Παράγραφοι Εισαγάγετε σχόλιο για αυτήν την έκδοση Παρακαλώ σημειώστε ότι όταν δημιουργήσετε ένα δεσμό σε μία
μη υπάρχουσα σελίδα, αυτή δημιουργείται αυτόματα. Παρακαλώ επιλέξτε το όνομα και ένα φάκελο για το σημειοματάριο. Παρακαλώ επιλέξτε γραμμή, πριν πατήσετε το κουμπί. Παρακαλώ καθορίστε σημειωματάριο Πρόσθετο Πρόσθετα Θύρα Θέση μέσα στο παράθυρο Π_ροτιμήσεις Προτιμήσεις Εκτύπωση στον περιηγητή Προώθηση Ιδιότ_ητες Ιδιότητες Γρήγορη σημείωση Γρήγορη σημείωση... Πρόσφατες Τροποποιήσεις Πρόσφατες Τροποποιήσεις... Πρόσφατα Τ_ροποποιημένες σελίδες Επαναμορφοποίηση κώδικα markup στο παρασκήνιο Αφαίρεση στήλης Αφαίρεση γραμμής Γίνεται αφαίρεση δεσμών Μετονομασία της σελίδας "%s" Αντικατάσταση ό_λων Αντικατάσταση με Επαναφορά σελίδας στην αποθηκευμένη έκδοση; Αναθ Δεξιά Δεξί Πλαϊνό Πλαίσιο Τοποθεσία δεξιού περιθωρίου Απο_θήκευση έκδοσης Αποθήκευση ενός αν_τιγράφου... Αποθήκευση αντιγράφου Αποθήκευση έκδοσης Αποθηκευμένη έκδοση από το zim Βαθμολογία Εντολή λήψης στιγμιότυπου οθόνης Αναζήτηση Αναζήτηση για _Backlinks... Τομέας Επιλογή αρχείου Επιλογή φακέλου Επιλογή Εικόνας Επιλέξτε μία έκδοση για να δείτε τις αλλαγές μεταξύ εκείνης και της
τρέχουσας κατάστασης. Ή επιλέξτε πολλαπλές εκδόσεις για να δείτε
τις διαφορές που έχουν μεταξύ τους.
 Επιλογή της μορφής εξόδου Επιλογή του αρχείου ή φακέλου εξόδου Επιλογή σελίδων για εξαγωγή Επιλογή παραθύρου ή περιοχής Επιλογή Διάγραμμα ακολουθίας Ο εξυπηρετητής δεν εκκινήθηκε Ο εξυπηρετητής εκκινήθηκε Ο εξυπηρετητής τερματίστηκε Ορισμός προεπιλεγμένου επεξεργαστή κειμένου Εμφάνιση αριθμών γραμμής Εμφάνιση του Πίνακα ως πλεούμενου χειριστήριου αντί για πλαϊνού Εμ_φάνιση αλλαγών Εμφάνιση ξεχωριστού εικονιδίου για κάθε σημειωματάριο Εμφάνιση ονόματος πλήρους σελίδας Εμφάνιση στην εργαλειοθήκη Εμφάνιση δεξιού περιθωρίου Εμφάνιση του δρομέα ακόμα και στις σελίδες οι οποίες δεν μπορούν να τροποποιηθούν Μία _σελίδα Μέγεθος Παρουσιάστηκε κάποιο σφάλμα κατά την εκτέλεση του "%s" Αλφαβητική ταξινόμηση Ταξινόμηση σελίδων ανά ετικέτα Προβολή πηγής Έλεγχος ορθογραφίας Εκκίνηση εξυπηρετητή _Ιστού Σύ_μβολο... Σύνταξη Προεπιλογή Συστήματος Πλάτος καρτέλας Πίνακας Επεξεργαστής πίνακα Πίνακας Περιεχομένων Ετικέτες Εργασία Λίστα εργασιών Πρότυπο Πρότυπα Κείμενο Αρχεία κειμένου Κείμενο από αρ_χείο... Χρώμα κειμένου φόντου Χρώμα κειμένου προσκηνίου Ο φάκελος
%s
δεν υπάρχει.
Επιθυμείτε να το δημιουργήσετε τώρα; Ο φάκελος "%s" δεν υπάρχει.
Επιθυμείτε να το δημιουργήσετε τώρα; Το πρόσθετο υπολογισμού μαθηματικών εκφράσεων απέτυχε
να υπολογίσει την έκφραση στη θέση του δρομέα. Ο πίνακας πρέπει να αποτελείται από τουλάχιστον μια γραμμή!
 Καμία διαγραφή δεν πραγματοποιήθηκε. Δεν έχει γίνει καμία αλλαγή από την τελευταία αποθηκευμένη έκδοση  του σημειωματάριου Αυτό μπορεί να είναι ένδειξη ότι δεν έχουν
εγκατασταθεί τα απαραίτητα λεξικά Το αρχείο υπάρχει ήδη.
Θέλετε να το αντικαταστήσετε; Αυτή η σελίδα δεν έχει φάκελο επισυνάψεων Το πρόσθετο αυτό επιτρέπει τη λήψη ενός στιγμιοτύπου οθόνης
και την εισαγωγή του σε μία σελίδα του zim.

Είναι ένα από τα πρόσθετα του zim και παρέχεται μαζί του.
 Αυτό το ένθετο προσθέτει ένα διάλογο που εμφανίζει όλες τις
εκκρεμείς ενέργειες σε ένα σημειωματάριο. Εκκρεμείς ενέργειες
αποτελούν  είτε κουτάκια ελέγχου που δεν έχουν τσεκαρισιτεί ή
στοιχεία που έχουν σημειωθεί με ετικέτες όπως "TODO" ή "FIXME".
Είναι ένα κεντρικό ένθετο που παρέχεται με τη διανομή του zim.
 Το πρόσθετο αυτό παρέχει ένα διάλογο μέσω του οποίου μπορείτε
εύκολα να εισάγετε κείμενο ή τα περιεχόμενα του προχείρου σε
μία σελίδα του zim.

Είναι ένα από τα πρόσθετα του zim και παρέχεται μαζί του.
 Το πρόσθετο αυτό προσθέτει το Εικονίδιο πλαισίου συστήματος
του zim για γρήγορη πρόσβαση στα σημειωματάρια.

Το πρόσθετο εξαρτάται από την έκδοση 2.10 της Gtk+ (ή νεότερη).

Είναι ένα από τα πρόσθετα του zim και παρέχεται μαζί του.
 Το πρόσθετο αυτό προσθέτει ένα επιπλέον πλαίσιο που προβάλει τη λίστα με τις σελίδες
που περιέχουν συνδέσμους που οδηγούν στην τρέχουσα σελίδα.

Το πρόσθετο αυτό είναι βασικό και έρχεται μαζί με το zim.
 Αυτό το ένθετο προσθέτει ένα νέο χειριστήριο που
εμφανίζει τον πίνακα περιεχομένων της τρέχουσας σελίδας.

Πρόκειται για ένα βασικό ένθετο της διανομής του zim.
 Το πρόσθετο αυτό εισάγει το διάλογο 'Εισαγωγή συμβόλου' και
κάνει δυνατή τη χρήση τυπογραφικών χαρακτήρων.

Είναι ένα από τα πρόσθετα του zim και παρέχεται μαζί του.
 Αυτό το ένθετο παρέχει έλεχγο εκδόσεων για τα σημειωματάρια.
Το ένθετο υποστηρίζει τα συστήματα ελέγχου εκδόσεων Bazzar,
Git και Mercurial.

Είναι ένα κεντρικό ένθετο που παρέχεται με τη διανομή του zim.
 Το πρόσθετο αυτό επιτρέπει την ενσωμάτωση αριθμητικών υπολογισμών στο zim.
Είναι βασισμένο στο αριθμητικό άρθρωμα από
http://pp.com.mx/python/arithmetic.
 Το πρόσθετο αυτό υπολογίζει απλές
μαθηματικές εκφράσεις στο zim.

Είναι ένα από τα πρόσθετα του zim και παρέχεται μαζί του.
 Αυτό το ένθετο παρέχει έναν συντάκτη διαγραμμάτων για το zim,
βασισμένο στο Dita..

Είναι ένα κεντρικό ένθετο που διανείμεται με το zim.
 Το πρόσθετο αυτό παρέχει έναν επεξεργαστή διαγραμμάτων
για το zim βασισμένο στο GraphViz.

Είναι ένα από τα πρόσθετα του zim και παρέχεται μαζί του.
 Το πρόσθετο αυτό παρέχει μία γραφική αναπαράσταση της διάρθρωσης
των δεσμών του σημειωματάριου. μπορεί να χρησιμοποιηθεί σαν ένα είδος
"χάρτη" που εμφανίζει τον τρόπο με τον οποίο συνδέονται οι σελίδες
μεταξύ τους.

Είναι ένα από τα πρόσθετα του zim και παρέχεται μαζί του.
 Αυτό το ένθετο παρέχει ένα ευρετήριο σελίδων που φιλτράρεται με βάση
την επιλογή ετικετών από ένα σύνεφο.
 Το πρόσθετο αυτό παρέχει έναν επεξεργαστή σχεδιαγραμμάτων βασισμένο στο GNU R.
 Αυτό το ένθετο παρέχει ένα συντάκτη γραφημάτων για το zim,
βασισμένο στο Gnuplot.
 Αυτό το πρόσθετο παρέχει έναν επεξεργαστή διαγραμμάτων ακολουθίας για τον zim βασισμένο στο seqdiag.
Επιτρέπει την εύκολη επεξεργασία των διαγραμμάτων ακολουθίας.
 Το πρόσθετο αυτό αποτελεί μια προσωρινή λύση για την έλλειψη
υποστήριξης εκτύπωσης στο zim. Μετατρέπει και εξάγει την τρέχουσα
σελίδα σε html και την ανοίγει τον περιηγητή ιστοσελίδων σας. Εφόσον
ο περιηγητής ιστοσελίδων σας έχει υποστήριξη εκτύπωσης, θα μπορέσετε
να εκτυπώσετε τα δεδομένα σας, έστω και έμμεσα.

Είναι ένα από τα πρόσθετα του zim και παρέχεται μαζί του.
 Το πρόσθετο αυτό παρέχει έναν επεξεργαστή συναρτήσεων
για το zim βασισμένο στο latex.

Είναι ένα από τα πρόσθετα του zim και παρέχεται μαζί του.
 Αυτό το ένθετο ταξινομεί τις επιλεγμένες γραμμές με
αλφαβητική σειρά. Αν είναι ήδη ταξινομημένες, τότε
αντιστρέφεται η ταξινόμηση (A-Ω σε Ω-Α).
 Συνήθως πρόκειται για ένδειξη ότι το αρχείο περιέχει μη αποδεκτούς χαρακτήρες Τίτλος Για να συνεχίσετε θα πρέπει  να αποθηκεύσετε ένα αντίγραφο της σελίδας, αλλιώς οι
αλλαγές σας θα χαθούν. Αν κάνετε αποθήκευση, οι αλλαγές θα χαθούν και πάλι, αλλά
θα μπορείτε να τις επαναφέρετε αργότερα από το αντίγραφο. ΠΠ Σήμε_ρα Σήμερα Σημείωση ως OK (V) Σημείωση ως NOK (X) Πάνω Πλαίσιο Εικονίδιο πλαισίου συστήματος Μετατροπή ονομάτων σελίδων σε ετικέτες για τις ενέργειες Τύπος Αφαίρεση εσοχής με το <BackSpace>
(Αν είναι απενεργοποιημένο, μπορείτε να χρησιμοποιήσετε το <Shift><Tab>) Άγνωστη Μη καθορισμένο Χωρίς ετικέτα Ενημέρωση %i σελίδας που έχει δεσμούς σε αυτήν τη σελίδα Ενημέρωση %i σελίδων που έχουν δεσμούς σε αυτήν τη σελίδα Ενημέρωση της κεφαλίδας αυτής της σελίδας Γίνεται ενημέρωση δεσμών Ενημέρωση ευρετηρίου Χρήση προσαρμοσμένης γραμματοσειράς Χρήση μίας σελίδα για κάθε Χρήση του <Enter> για ακολούθηση δεσμών
(Αν είναι απενεργοποιημένο, χρησιμοποιήστε το <Alt><Enter>) Έλεγχος εκδόσεων Ο έλεγχος εκδόσεων δεν είναι ενεργός για αυτό το σημειωματάριο.
Θέλετε να τον ενεργοποιήσετε; Εκδόσεις Εμφ_άνιση με Ενδείξεις Εμφάνιση _καταγραφής Εξυπηρετητής Ιστού Εβδομάδα Όταν καταγράφετε ένα σφάλμα παρακαλούμε να περιλαμβάνετε
την πληροφορία από το κουτί κειμένου που ακολουθεί Πλήρης _λέξη Πλάτος Βικισελίδα: %s Με αυτό το πρόσθετο μπορείς να ενσωματωθεί ένα 'πίνακας' σε σελίδα wiki. Οι πίνακες εμφανίζονται σαν γραφικά στοιχεία GTK TreeView.
Η εξαγωγή αυτών σε διάφορες μορφές (π.χ. HTML/LaTeX) ολοκληρώνει το σύνολο χαρακτηριστικών.
 Μέτρηση λέξεων Μέτρηση λέξεων Λέξεις Έτος Χθες Το αρχείο είναι ανοικτό σε μία εξωτερική εφαρμογή. Μπορείτε να κλείσετε αυτό το παράθυρο όταν τελειώσετε Μπορείτε να καθορίσετε ποια προσαρμοσμένα εργαλεία θα εμφανίζονται
στο μενού εργαλείων και στην εργαλειοθήκη ή τα αναδυόμενα μενού Zim Wiki Επιφάνειας Εργασίας Απεστίαση _Σχετικά με _Όλα τα Πλαίσια _Αριθμητική _Πίσω Περιήγηση Σ_φάλματα _Κάτω _Εκκαθάριση μορφοποίησης _Κλείσιμο _Σύμπτυξη όλων Περιε_χόμενα Αντι_γραφή Αντι_γραφή Εδώ _Διαγραφή _Διαγραφή σελίδας Από_ρριψη αλλαγών _Επεξεργασία Ε_πεξεργασία δεμού _Επεξεργασία δεσμού ή αντικειμένου... _Επεξεργασία Ιδιοτήτων Έμ_φαση _Συχνές ερωτήσεις _Αρχείο _Εύρεση... _Μπροστά _Πλήρης οθόνη _Μετάβαση _Βοήθεια Επι_σήμανση _Ιστορικό _Αρχική _Εικόνα... Ε_ισαγωγή σελίδας... Ε_ισαγωγή Μετάβαση σε... Συντομεύσεις πλη_κτρολογίου _Δεσμός _Δεσμός στην ημερομηνία _Δεσμός... _Μαρκάρισμα _Περισσότερα _Μετακίνηση _Μετακίνηση Εδώ _Νέα σελίδα... _Επόμενο _Τίποτα Κα_νονικό Μέγεθος Αριθμημέ_νη Λίστα Άν_οιγμα Άν_οιγμα νέου σημειωματάριου... Ά_λλα... _Σελίδα Πάν_ω Ε_πικόλληση _Προεπισκόπηση _Προηγούμενο Εμφάνι_ση στον περιηγητή _Γρήγορη σημείωση... 'Ε_ξοδος Π_ρόσφατες σελίδες Α_κύρωση αναίρεσης _Κανονική έκφραση _Επαναφόρτωση Α_φαίρεση δεσμού _Αντικατάσταση _Αντικατάσταση... Επαναφο_ρά μεγέθους Επαναφο_ρά έκδοσης _Αποθήκευση Αποθήκευ_ση αντιγράφου Σ_τιγμιότυπο οθόνης Ανα_ζήτηση Ανα_ζήτηση... Αποστολή π_ρος... Π_λαϊνά Πλαίσια _Σε αντιπαραβολή Ταξινόμηση γραμμών Μεσο_γράμμιση Έ_ντονα _Δείκτης Εκ_θέτης _Πρότυπα Ερ_γαλεία _Αναίρεση Α_υτολεξί Ε_κδόσεις... _Προβολή Εστίαση ημερολόγιο:αρχή_εβδομάδας:0 οριζόντιες γραμμές καμία γραμμή πλέγματος Μόνο για ανάγνωση δευτερόλεπτα Launchpad Contributions:
  Aggelos Arnaoutis https://launchpad.net/~angelosarn
  Constantinos Koniaris https://launchpad.net/~kostkon
  Jaap Karssenberg https://launchpad.net/~jaap.karssenberg
  Melsi Habipi https://launchpad.net/~mhabipi
  Spiros Georgaras https://launchpad.net/~sngeorgaras
  paxatouridis https://launchpad.net/~paxatouridis-deactivatedaccount
  tkout https://launchpad.net/~tkout
  tzem https://launchpad.net/~athmakrigiannis κάθετες γραμμές με γραμμές 