��    �     T              �  .   �  ?   �     �          %  0   A     r     �  	   �     �  i   �     .      >   V   K   	   �   	   �      �   3   �   Y   �      X!     n!  
   {!     �!     �!     �!     �!     �!  $   �!  ?   �!  /   0"  (   `"  %   �"  	   �"     �"     �"     �"     �"     �"     �"     �"  
   #     #     '#     .#     C#  
   K#     V#     f#  <   u#     �#     �#     �#     �#     �#     �#     $     $$     ;$  +   L$  3   x$      �$     �$     �$     �$  
   �$     %     %     3%  3   O%     �%     �%     �%     �%     �%     �%     &     &     &     +&     8&     =&     A&  0   I&     z&     �&     �&     �&     �&     �&     �&     �&  ~   �&     V'  
   d'     o'     w'  
   �'  	   �'     �'     �'     �'     �'     �'     �'     �'      (     (  !   (     5(  #   F(     j(     }(     �(     �(     �(     �(     �(     �(     �(     )     .)     7)     >)  
   C)     N)     ])  	   n)  6   x)     �)  v   �)     -*  *   ?*  c   j*     �*     �*     �*     �*     +     +  
   +  
   &+  
   1+  
   <+  
   G+     R+     Y+  	   y+     �+     �+     �+     �+     �+     �+  
   �+     �+     �+     �+     ,     ,     !,     3,     B,     O,     [,     h,     z,     �,  	   �,     �,     �,     �,     �,     �,     �,     �,     -     -      -  2   )-     \-     d-     m-     �-     �-     �-     �-     �-     �-  	   �-     �-     .     
.      .     8.     E.     J.     S.     \.     m.     }.     �.     �.     �.     �.     �.     �.     /  	   /     &/     )/     B/     N/     \/     i/     }/  
   �/     �/     �/     �/     �/     �/     �/     �/     0     0     *0  	   80     B0     K0  0   P0  	   �0     �0     �0  	   �0  '   �0  V   �0  3   :1     n1     �1  .   �1     �1     �1     �1     �1     �1     �1     2     2  
    2  &   +2  
   R2     ]2     k2     z2     �2     �2  
   �2     �2     �2  ?   �2     3     +3     83     W3     [3     k3     |3  	   �3     �3     �3     �3     �3     �3     �3     �3     �3     4     4     !4  �   .4     �4      �4     �4     5  	   +5     55     H5     W5     f5     ~5  2   �5     �5  &   �5     �5  5   6     D6     Q6  &   V6     }6     �6     �6     �6  
   �6     �6     �6     �6     �6  	   �6     7  	   7     7  
   7     '7  ?   :7  A   z7  �  �7  S   �9  K   �9  @    :  6   a:  -   �:  x   �:  �   ?;  �   <  �   �<  �   =  }   �=  �   >  �   �>  }   L?  h   �?  k   3@  �   �@  R   sA  ;   �A  =   B    @B  j   TC  n   �C     .D  7   �D     �D  �   �D  |   �E     F     	F     F     F     *F     >F  	   GF  '   QF     yF  D   ~F     �F     �F     �F     �F  H   �F     =G     ]G     lG  !   {G     �G     �G  P   �G     H  U   $H     zH     �H  	   �H  
   �H     �H  N   �H     �H     I     I  
   I     'I     5I     ;I  	   @I  ^   JI  f   �I     J  	   !J     +J     2J  
   7J     BJ     NJ     TJ     \J     bJ  	   jJ     tJ     {J     �J     �J  	   �J     �J  
   �J     �J     �J     �J     �J  
   �J     �J     K     K  	   &K     0K     5K     ;K     AK     JK     SK     _K     cK  
   iK     tK     }K  	   �K     �K     �K     �K     �K     �K     �K     �K     �K     �K     �K     �K  
   �K     �K     L     L     L     $L     3L     7L     =L  	   WL     aL     gL     oL     vL  	   L     �L     �L     �L     �L     �L     �L     �L     �L     �L     �L     �L     
M     M     'M  
   -M     8M     GM  
   OM     ZM     fM     rM     �M     �M     �M  
   �M     �M  
   �M     �M     �M  	   �M     �M     �M     �M     �M     N     N     N    ,N  4   ?P  L   tP     �P  1   �P  $    Q  T   %Q  F   zQ  &   �Q  
   �Q     �Q  c   R     oR     }R  z   �R     S     S     -S  A   JS  g   �S     �S  	   T  
   T     "T     0T     HT     eT     rT  -   xT  A   �T  0   �T  7   U  <   QU     �U     �U     �U     �U  	   �U     �U     �U     V     V  +   )V     UV     \V     wV     ~V     �V     �V  P   �V  	   �V  	   W     W  	   +W  #   5W  '   YW     �W     �W     �W  -   �W  &   �W  ,   X     :X     OX     _X     oX     �X     �X     �X  '   �X     �X  "   Y     )Y  #   BY  )   fY     �Y     �Y     �Y     �Y     �Y     �Y     �Y  	   �Y  /   �Y     ,Z     <Z     IZ     WZ     nZ     |Z     �Z     �Z  �   �Z     6[     S[  	   c[     m[     �[     �[     �[     �[     �[     �[     �[  
   \     \     !\     4\  +   <\     h\  +   |\     �\     �\     �\  (   �\     ]     #]     8]     S]  (   a]  !   �]     �]     �]     �]     �]     �]     �]     �]  N   ^     V^  �   ^^     �^  ,   _  �   1_     �_     �_     �_     �_     	`     `     `     #`     1`     ?`     M`  
   [`  $   f`     �`     �`     �`     �`     �`     �`     �`     �`     �`     a     a     (a     4a     Ra     ea     sa     a     �a     �a     �a     �a  	   �a     �a     �a     �a     b     &b  "   ;b     ^b     ub     �b     �b  8   �b     �b     �b  (   �b     c  &   ,c      Sc     tc     �c  
   �c  	   �c     �c  	   �c     �c      �c     d     &d  	   ,d     6d     Bd     Td     fd     d     �d     �d     �d     �d     �d     e     e     %e  !   (e     Je     Ze     he     ~e     �e     �e     �e     �e     �e     f     f     1f     =f     Rf     Zf     jf     zf     �f     �f  1   �f     �f     �f     �f     �f     g  ^   #g  &   �g     �g     �g  3   �g     �g     �g     h     h  
    h     +h     Gh     Uh     eh  *   th     �h     �h     �h     �h  :   �h     i     !i     0i     Ai  ?   [i     �i  
   �i  )   �i     �i     �i     j     j     &j     4j     Cj     Uj  
   uj     �j     �j     �j     �j     �j     �j     �j  �   �j     dk  "   |k     �k     �k     �k     �k     �k     l     3l     Sl  J   ll     �l  -   �l     �l  N   m     am     tm  8   |m  "   �m  $   �m     �m     n  
   %n     0n     On  	   \n     fn     nn     {n     �n     �n     �n     �n  .   �n  A   �n  �  $o  W   �p  (   Pq  I   yq  0   �q  /   �q  �   $r  ;  �r  �   t  �   �t  �   cu  �   v  �   �v  �   2w  �   �w  �   |x  �   y    �y  \   �z  F   �z  H   @{    �{  �   �|  �   (}  �   �}  F   <~     �~  �   �~  y   ^     �     �     �     �     �     )�     9�  2   U�     ��  \   ��     �     ��     	�     �  �   &�  !   ˁ     �     �  *   �     H�  !   `�  o   ��     �  O   �     R�     Y�     i�  
   ��     ��  H   ��     �     ��     �     �     !�     2�     9�     >�  R   F�  �   ��     !�  
   5�     @�     M�     T�     f�     r�     z�     ��     ��     ��     ��     ��     Ʌ     ҅     �     ��     ��     �     �     �     .�     6�     H�     h�  
   {�  
   ��     ��     ��     ��     ��  	   ��     ��     Ɇ     φ     ֆ  	   �     ��  	   ��      �     �     �     "�     2�  
   F�     Q�     d�     r�     {�  
   ��     ��     ��  
   ��     ��     ��     ҇     �     �     ��     
�     �     �     '�  	   .�  
   8�     C�     `�  	   o�     y�     ��     ��  
   ��     ��     ��     ˈ     Ԉ     ��      �     �     �     +�     <�  	   D�     N�     ]�     l�     y�     ��     ��     ��     ��  	   ��     ʉ     ։     މ  	   �     ��  
   ��     �     �     /�  �  6�   %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i Attachment %i Attachments %i errors occurred, see log %i file will be deleted %i files will be deleted %i open item %i open items %i warnings occurred, see log <Unknown> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. Add Application Add Notebook Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 All Files All Tasks Allow public access Always use last cursor position when opening a page An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Arithmetic Attach File Attach image first Attachment Browser Attachments Author Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave version on regular intervals BackLinks BackLinks Pane Backend Bazaar BookmarksBar Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Capture whole screen Changes Characters Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Command Command does not modify data Comment Common include footer Common include header Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Could not find executable "%s" Could not find notebook: %s Could not find the file or folder for this notebook Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Demote Dependencies Description Details Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root E_xport... Edit %s Edit Custom Tool Edit Image Edit Link Edit _Source Editing Editing file: %s Enable Version Control? Enable plugin Enabled Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Get more plugins online Get more templates online Git Gnuplot Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide menubar in fullscreen mode Home Page Horizontal _Line Icon Images Import Page Include subpages Index Index page Inline Calculator Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Symbol Insert Text From File Interface Journal Jump to Jump to Page Labels marking tasks Last Modified Leave link to new page Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Map document root to URL Match _case Menubar Mercurial Modified Month Move Selected Text... Move Text to Other Page Move text to Name New File New Page New S_ub Page... New _Attachment No Applications Found No changes since last version No dependencies No such file: %s No such page: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open in New Window Open in New _Window Open new page Open plugins folder Open with "%s" Optional Options for plugin %s Other... Output file Output folder Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Name Page Template Page already exists: %s Paragraph Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please specify a notebook Plugin Plugin "%s" is required to display this object Plugins Port Position in the window Pr_eferences Preferences Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Reformat wiki markup on the fly Remove Remove All Removing Links Rename page "%s" Repeated clicking a checkbox cycles through the checkbox states Replace _All Replace with Restore page to saved version? Rev Right Side Pane S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Search Search _Backlinks... Search this section Section Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Server not started Server started Server stopped Set default text editor Set to Current Page Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show in the toolbar Show the cursor also for pages that can not be edited Single _page Size Some error occurred while running "%s" Sort alphabetically Sort pages by tags Spell Checker Start _Web Server Sy_mbol... System Default Table of Contents Tags Task Task List Template Templates Text Text Files Text From _File... The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The following parameters will be substituted
in the command when it is executed:
<tt>
<b>%f</b> the page source as a temporary file
<b>%d</b> the attachment directory of the current page
<b>%s</b> the real page source file (if any)
<b>%p</b> the page name
<b>%n</b> the notebook location (file or folder)
<b>%D</b> the document root (if any)
<b>%t</b> the selected text or word under cursor
<b>%T</b> the selected text including wiki formatting
</tt>
 The inline calculator plugin was not able
to evaluate the expression at the cursor. There are no changes in this notebook since the last version that was saved This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Tray Icon Turn page name into tags for task items Type Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unkown Image type Unkown Object Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use a page for each Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Zim Desktop Wiki Zoom _Out _About _Add _All Panes _Arithmetic _Back _Browse _Bugs _Cancel _Checkbox _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Edit _Edit Link _Edit Link or Object... _Edit Properties _Edit... _Emphasis _FAQ _File _Find _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _Move Here _New Page... _Next _None _Normal Size _Numbered List _OK _Open _Open Another Notebook... _Other... _Page _Parent _Paste _Preview _Previous _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In calendar:week_start:0 readonly seconds translator-credits Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2019-10-15 16:52+0000
Last-Translator: Wojtek Kazimierczak <Unknown>
Language-Team: Polish <pl@li.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
X-Generator: Launchpad (build bceb5ef013b87ef7aafe0755545ceb689ca7ac60)
 Komenda %(cmd)s zwróciła niezerowy status %(code)i Wystąpiło %(n_error)i błędów i %(n_warning)i ostrzeżeń, sprawdź logi %A, %d %B %Y %i załacznik %i załączników %i załączników %i napotkanych błędów, zobacz log %i plik nie będzie usunięty %i plik będzie usunięty %i plików będzie usunięte %i zadań do wykonania %i zadanie do wykonania %i zadania do wykonania %i napotkanych ostrzeżeń, zobacz log <Nieznany> Twój osobisty notatnik Plik o nazwie <b>"%s"</b> już istnieje.
Możesz użyć innej nazwy lub nadpisać istniejący plik. Dodaj program Dodaj notes Dodaje obsługę sprawdzania pisowni przy użyciu gtkspell.

Wtyczka ta jest domyślnie dostarczona wraz z programem Zim.
 Wszystkie typy plików Wszystkie zadania Zezwól na publiczny dostęp Zapamiętaj ostatnią pozycję kursora w czasie otwierania strony Podczas generowania pliku graficznego wystąpił błąd. Czy mimo to chcesz zapisać tekst źródłowy? Opisane źródło strony Aplikacje Arytmetyka Załącz plik Najpierw dołącz obraz Przeglądarka załączników Załączniki Autor Automatycznie zapisana wersja z programu Zim. Automatycznie zaznacz aktualne słowo przy nadawaniu formatowania Automatycznie twórz odnośniki z "TakichSłów" Automatycznie twórz odnośniki ze ścieżek do plików Automatyczny zapis wersji w określonych przedziałach czasu Odnośniki do strony Panel z Odnośnikami do strony System kontroli wersji Bazaar Zakładki Panel na dole Przeglądaj Wypunk_towanie Sk_onfiguruj wtyczkę Brak możliwości zmodyfikowania strony: %s Anuluj Zrób zrzut całego ekranu Zmiany Znaki Sprawdź _pisownię Lista checkbo_x Klasyczna ikona obszaru powiadamiania,
nie używaj nowych ikon statusu na Ubuntu Wyczyść Polecenie Komenda nie modyfikuje danych Komentarz Stopka dołączana za każdym razem Nagłówek dołączany za każdym razem Cały _notes Konfiguruj aplikacje Skonfiguruj wtyczkę Aplikacja uruchamiająca odnośniki typu "%s" Aplikacja otwierająca pliku typu "%s" Rozpatruj wszystkie pola wyboru jako zadania Skopiuj adres e-mail Skopiuj szablon Kopiuj _Jako... Skopiuj _odnośnik S_kopiuj położenie NIe znaleziono programu "%s" Brak notesu: %s Brak pliku lub katalogu dla tego notesu Nie mogłem otowrzyć: %s Błąd analizy składni wyrażenia Nie można odczytać: %s Nie udało się zapisać strony: %s Stwórz nową stronę dla każdej notatki Utworzyć katalog? _Wytnij Narzędzia użytkownika Narzędzia uży_tkownika Dostosuj... Data dnia Domyślna Domyślny format tekstu skopiowanego do schowka Domyślny notes Opóźnienie Usuń stronę Usunąć stronę "%s"? Poziom niżej Zależności Opis Szczegóły Czy chcesz przywrócić stronę: %(page)s
do zapisanej wersji: %(version)s ?

Wszystkie zmiany wprowadzone po zapisaniu ostatniej wersji zostaną utracone! Katalog główny dokumentów Wye_ksportuj... Edytuj %s Edytuj narzędzie użytkownika Edytuj obraz Edytuj odnośnik Edytuj _źródło Edycja Edytowany plik: %s Włączyć kontrolę wersji? Włącz wtyczkę Włączona Oblicz wyrażenie _Rozwiń wszystkie Eksport Eksportuj wszystkie strony do jednego pliku Eksport zakończony Eksportuj każdą stronę do osobnego pliku Eksportowanie notesu Nie udało się Nie udało się uruchomić: %s Nie udało się uruchomić aplikacji: %s Plik istnieje Szab_lony plików... Plik został zmieniony: %s Plik istnieje Plik nie ma atrybutu zapisywalności: %s Plik o nieobsługiwanym typie: %s Nazwa pliku Filtr Znajdź Zn_ajdź następne Znajdź p_oprzednie Znajdź i zamień Znajdź Oznaczaj zadania do wykonania na poniedziałek lub wtorek jako przedweekendowe Katalog Katalog już istnieje i nie jest pusty. Eksport do tego katalogu może nadpisać znajdujące się w nim pliki. Czy na pewno chcesz kontynuować? Katalog istnieje: %s Katalog zawierający szablony załączników Dla zaawansowanego wyszukiwania możesz używać operatorów takich jak
AND, OR i NOT. Aby uzyskać więcej informacji, zobacz stronę pomocy. For_mat Format Uzyskaj więcej wtyczek online Pobierz więcej szablonów Git Gnuplot Nagłówek _1 Nagłówek _2 Nagłówek _3 Nagłówek _4 Nagłówek _5 Wysokość Ukryj menu w zmaksymalizowanym oknie Strona główna Linia_pozioma Ikona Obrazy Importuj stronę Uwzględnij podstrony Indeks Indeks Kalkulator wewnętrzny Wstawienie daty i czasu Wstaw diagram Wstaw Ditaa Wstaw wyrażenie matematyczne Wstaw wykres GNU R Wstaw Gnuplot Wstaw obraz Wstaw odnośnik Wstaw partyturę Wstaw zrzut ekranu Wstaw symbol Wstaw tekst z pliku Interfejs Dziennik Przejdź do Przejdź do strony Etykiety wskazujące na zadania Ostatnia modyfikacja Pozostaw odnośnik do nowej strony Panel po lewej stronie Sortowanie wierszy Wiersze Mapa odnośników Użyj pełnych ścieżek do plików w katalogu głównym Odnośnik do Położenie Zapisuj wydarzenia przy pomocy Zeitgeist Plik dziennika Wygląda na to, że znalazłeś błąd Ustaw jako aplikację domyślną Mapuj katalog główny na URL Wielkość _Liter Pasek menu Mercurial Data modyfikacji miesiąca Przenieś Zaznaczony Tekst... Przenieś tekst na inną stronę Przenieś tekst na Nazwa Nowy plik Nowa strona Nowa podstrona... Nowy _Załącznik Nie znaleziono aplikacji Brak zmian od ostatniej wersji Brak zależności Brak takiego pliku: %s Nie znaleziono strony: %s Nie znaleziono strony: %s Nie zainstalowano szablonów Notes Notesy OK Otwórz katalog z _załącznikami Otwórz katalog Otwórz notes Otwórz za pomocą... Otwórz katalog główny Otwórz katalog notesu Otwórz _stronę Otwiera w nowym oknie Ot_wórz w nowym oknie Otwórz nową stronę Otwórz folder wtyczek Otwórz za pomocą "%s" Opcjonalnie Opcje dla wtyczki %s Inny... Plik wyjściowy Folder docelowy Nadpisz Pasek ścieżki Strona Strona "%s" nie posiada katalogu z załącznikami Nazwa strony Szablon strony Strona juz isntieje %s Akapit Wpisz komentarz do tej wersji Pamiętaj że utworzenie linku do nieistniejącej strony powoduje jej automatyczne utworzenie. Wybierz nazwę i folder dla notatnika. Wybierz notatnik Wtyczka Wymagana jest wtyczka "%s"  aby wyświetlić objekt Wtyczki Port Pozycja w oknie _Preferencje Ustawienia Drukuj do przeglądarki www Poziom wyżej _Właściwości Właściwości Przesyła wydarzenia do usługi Zeitgeist. Szybka notatka Szybka notatka... Ostatnie zmiany Ostatnie zmiany Zamieniaj znaczniki wiki na formatowanie w trakcie pisania Usuń Usuń wszystko Usuwanie linków Zmień nazwę strony "%s" Wielokrotne kliknięcie w pole wyboru przełącza kolejne stany Zastąp _wszystkie Zamień na Przywrócić stronę do zapisanej wersji? Rewizja Panel po prawej stronie Zapisz wersje... Zapisz _kopię... Zapisz Kopię Zapisz wersję Zapisz zakładkę Zapisana wersja z programu Zim. Trafność Znajdź Szukaj linków zwrotnych Szukaj w sekcji Sekcja Wybierz plik Wybierz katalog Wybierz obraz Wybierz wersję, którą chcesz porównać z obecnym stanem.
Możesz też wybrać wiele wersji, by porównać je między sobą.
 Wybierz format eksportu Wybierz plik wyjściowy lub folder Wybierz stronę do eksportu Wybierz okno lub obszar Zaznaczenie Serwer nie został uruchomiony Serwer został uruchomiony Serwer został zatrzymany Wybierz domyślny edytor tekstu Ustaw bieżącą stronę Wyświetlaj spis treści jako pływający widżet zamiast w panelu bocznym Pokaż _Zmiany Pokaż oddzielne ikony dla każdego notatnika Pokaż na pasku narzędziowym Pokaż kursor również na stronach nie posiadających możliwości edytowania Pojedyncza _strona Rozmiar W trakcie wykonywania polecenia "%s" wystąpiły błędy Sortuj w kolejności alfabetycznej Sortuj strony zgodnie ze znacznikami Sprawdzanie pisowni Uruchom _serwer www Sy_mbol... Domyślne ustawienia systemowe Spis treści Znaczniki Zadanie Lista zadań Szablon Szablony Tekst Pliki tekstowe Tekst z _pliku... Katalog%s
jeszcze nie istnieje.
 Utworzyć go? Folder  "%s"  jeszcze nie istnieje.
Czy chcesz go teraz utworzyć Poniższe parametry będą zastąpione
w poleceniu podczas uruchomienia:
<tt>
<b>%f</b> źródło strony jako plik tymczasowy
<b>%d</b> folder załączników dla bieżącej strony
<b>%s</b> plik właściwej strony źródłowej (jeśli jest)
<b>%p</b> nazwa strony
<b>%n</b> lokalizacja notatnika (plik lub folder)
<b>%D</b> lokalizacja bazowa (jeśli jest)
<b>%t</b> zaznaczony tekst lub słowo pod kursorem
<b>%T</b> zaznaczony tekst wraz z formatowaniem wiki
</tt>
 Wtyczka kalkulatora wewnętrznego nie była w stanie
obliczyć wyrażenia pod kursorem. Brak zmian od ostatniej zapisanej wersji To może oznaczać, że nie masz zainstalowanych
odpowiednich słowników Ten plik już istnieje.
Czy chcesz go nadpisać? Ta strona nie posiada katalogu z załącznikami Wtyczka umożliwia wykonywanie zrzutów ekranu i bezpośrednie wstawianie ich
na aktualną stronę.
Wtyczka ta jest domyślnie dostarczona wraz z programem Zim.
 Ta wtyczka wyświetla okno dialogowe zawierające wszystkie zadania do wykonania
znajdujące się w otwartym notesie. Zadania te powinny być umieszczone w notesie
w postaci niezaznaczonych pól "[ ]" lub elementów oznakowanych słowami "TODO" lub "FIXME".

Jest to wtyczka domyślnie dostarczana z programem Zim.
 Wtyczka udostępnia okno do szybkiego umieszczania tekstu
ze schowka w treści aktualnej strony.

Wtyczka ta jest domyślnie dostarczona wraz z programem Zim.
 Wtyczka to dodaje ikonę obszaru powiadamiania, by ułatwić szybki dostęp do notesu Zim.

Zależy od Gtk+ w wersji 2.10 lub wyższej.

Jest to wtyczka domślnie dostarczana z programem Zim.
 Wtyczka dodaje widżet pokazujący listę stron zawierających
odnośniki do aktualnej strony.

Wtyczka ta jest domyślnie dostarczona wraz z programem Zim.
 Wtyczka dodaje specjalny widżet wyświetlający spis treści aktualnej strony.

Wtyczka ta jest domyślnie dostarczona wraz z programem Zim.
 Wtyczka dodaje okno 'Wstaw symbol' i umożliwia
automatyczne formatowanie znaków typograficznych.

Wtyczka ta jest domyślnie dostarczona wraz z programem Zim.
 Wtyczka dodaje kontrolę wersji dla notesów.

Wtyczka obsługuje systemy kontroli wersji Bazaar, Git i Mercurial.

Wtyczka ta jest domyślnie dostarczona wraz z programem Zim.
 Wtyczka umożliwia szybkie obliczanie prostych wyrażeń matematycznych
w programie Zim.

Wtyczka ta jest domyślnie dostarczona wraz z programem Zim.
 Wtyczka udostępnia programowi Zim edytor diagramów wykorzystujący Ditaa.

Wtyczka ta jest domyślnie dostarczona wraz z programem Zim.
 Wtyczka udostępnia programowi Zim edytor diagramów wykorzystujący GraphViz.

Wtyczka ta jest domyślnie dostarczona wraz z programem Zim.
 Wtyczka udostępnia okno zawierające graficzną
reprezentację struktury odnośników w notesie.
Może być użyta jako coś na wzór „mapy myślowej”
przedstawiającej relacje między stronami.

Wtyczka ta jest domyślnie dostarczona wraz z programem Zim.
 Wtyczka udostępnia indeks stron filtrowanych według znaczników wyświetlanych w chmurce.
 Wtyczka dostarcza programowi Zim edytor wykresów na podstawie GNU R.
 Wtyczka dostarcza programowi Zim edytor wykresów na podstawie Gnuplot.
 Wtyczka ta służy jako substytut drukowania w programie Zim.
Eksportuje aktualną stronę do pliku html i otwiera ten plik
w przeglądarce www. Następnie możesz użyć przeglądarki www
do wydrukowania swojej strony.

Jest to wtyczka domyślnie dostarczana z programem Zim.
 Wtyczka udostępnia programowi Zim edytor równań wykorzystujący LaTeX.

Wtyczka ta jest domyślnie dostarczona wraz z programem Zim.
 Wtyczka dostarcza programowi Zim edytor partytur na podstawie Lilypond.

Wtyczka ta jest domyślnie dostarczona wraz z programem Zim.
 Wtyczka sortuje wybrane wiersze w kolejności alfabetycznej.
Jeżeli wiersze są już posortowane, kolejność jest odwracana
(A-Z na Z-A).
 Ten komunikat przeważnie oznacza, że plik zawiera niepoprawne znaki. Nazwa Aby kontynuować możesz zapisać kopię tej strony lub odrzucić
wprowadzone zmiany. Jeśli zapiszesz kopię tej strony zmiany również zostaną
odrzucone, jednak później będziesz mógł przywrócić kopię. Aby utworzyć nowy notatnik, musisz wybrać pusty folder.
Oczywiście możesz również wybrać istniejący folder zima.
 Spis treści Dziś Dziś Zmień zaznaczenie pola 'V' Zmień zaznaczenie pola 'X' Panel na górze Ikona obszaru powiadamiania Przekształć nazwę strony w znaczniki dla zadań Rodzaj Usuń wcięcie klawiszem <BackSpace>
(jeśli wyłączysz, nadal możesz użyć <Shift><Tab>) Nieznany Nieznany typ obrazu Nieznany obiekt Nieoznaczone Zaktualizuj %i stron odwołujących się do tej strony Zaktualizuj %i stronę odwołującą się do tej strony Zaktualizuj %i strony odwołujące się do tej strony Zaktualizuj nagłówek tej strony Aktualizacja odnośników Odświeżanie indeksu Użyj %s do przełączenia na panel boczny Użyj wybranej czcionki Użyj osobnej strony dla każdego Użyj klawisza <Enter>, by podążać za odnośnikami
(Jeśli wyłączysz, nadal możesz używać <Alt><Enter>) Kontrola wersji Kontrola wersji nie jest włączona dla tego notesu.
Czy chcesz ją włączyć? Wersje Pokaż _opisane Wyświetl _dziennik zdarzeń Web Serwer tygodnia W trakcie zgłaszania błędu, proszę podać
informacje z pola poniżej Tylko całe słowa Szerokość Strona wiki: %s Ilość słów Liczba słów... Słowa roku Wczoraj Edytujesz plik za pomocą zewnętrznej aplikacji. Zamknij to okno kiedy skończysz Możesz konfigurować narzędzia użytkownika, które zostaną wyświetlone
w menu narzędzi, w pasku narzędzi lub menu kontekstowych. Zim - Osobista Wiki Po_mniejsz _O programie _Dodaj Wszystkie p_anele _Arytmetyka _Wstecz _Przeglądaj _Błędy _Anuluj _Pole wyboru _Podrzędny _Wyczyść formatowanie _Zamknij Zw_iń wszystkie _Zawartość _Skopiuj _Skopiuj tutaj _Usuń _Usuń stronę _Odrzuć zmiany _Edycja _Edytuj odnośnik _Edytuj odnośnik lub obiekt... _Zmień ustawienia _Modyfikuj Pochylenie _FAQ _Plik _Znajdź _Znajdź… _Naprzód _Pełny ekran _Idź _Pomoc _Podświetlenie _Historia Start _Obraz... _Importuj stronę... _Wstaw _Idź _Przejdź do... _Skróty klawiszowe _Odnośnik _Odnośnik do daty _Odnośnik... _Zaznacz _Więcej _Przenieś _Przenieś tutaj _Nowa strona... _Następny _Brak _Zwykły rozmiar Lista numeryczna _OK _Otwórz _Otwórz kolejny notes... _Inne... _Strona _Nadrzędny _Wklej _Podgląd _Poprzedni _Drukuj do przeglądarki www Szybka notatka _Zakończ _Ostatnie strony _Ponów _Wyrażenie regularne _Odśwież _Usuń _Usuń odnośnik _Zastąp Za_stąp... _Przywróć rozmiar początkowy _Przywróć wersję Zapi_sz _Zapisz Kopię _Zrzut ekranu... _Szukaj Szukaj... _Wyślij do... Panele _boczne _Obok siebie Sortuj wiersze _Przekreślenie _Pogrubienie Indeks dolny Indeks górny _Szablony _Narzędzia _Cofnij _Wyjustowanie Wersje... _Widok Po_większ calendar:week_start:1 Tylko do odczytu sekund Launchpad Contributions:
  BPS https://launchpad.net/~bps
  DB79 https://launchpad.net/~mr-bogus
  Dariusz Dwornikowski https://launchpad.net/~tdi-kill-9-deactivatedaccount
  Filip Stepien https://launchpad.net/~filstep
  Jaap Karssenberg https://launchpad.net/~jaap.karssenberg
  Kabturek https://launchpad.net/~marcindomanski
  Lucif https://launchpad.net/~lucif-onet
  Marcin Swierczynski https://launchpad.net/~orneo1212
  Mirosław Zalewski https://launchpad.net/~miroslaw-zalewski
  Piotr Palucki https://launchpad.net/~palucki
  Piotr Strębski https://launchpad.net/~strebski
  Tomasz (Tomek) Muras https://launchpad.net/~zabuch
  Wojtek Kazimierczak https://launchpad.net/~w-kazimierczak
  ZZYZX https://launchpad.net/~zzyzx-eu
  ciastek https://launchpad.net/~ciastek
  spirytdaghost https://launchpad.net/~spirytdaghost
  yaras https://launchpad.net/~jaroslaw-zajaczkowski
  Ędward Ącki https://launchpad.net/~edwardacki 