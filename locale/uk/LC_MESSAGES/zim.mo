��    D     <              \$  ,   ]$  .   �$  ?   �$     �$     %     !%     <%  	   Z%     d%  i   s%  *   �%     &     &     %&     2&  
   M&  -   X&     �&  V   �&     �&  	   �&  	   �&     �&  3   '  Y   G'     �'     �'  
   �'     �'     �'     �'     (     (  	   (     &(  -   5(  $   c(  ?   �(  /   �(  (   �(     !)  %   >)     d)     z)  	   �)     �)     �)     �)     �)  
   �)     �)     �)  �   �)  �   e*     �*     +     +     (+  
   0+     ;+     W+     g+  <   v+     �+  	   �+  
   �+     �+     �+     �+     �+     ,     ,     .,  +   ?,  3   k,      �,     �,     �,     �,     �,  
   �,     �,     -     +-     G-  3   d-     �-     �-     �-     �-     �-     .     /.     >.     F.     K.     X.     f.     s.     x.     |.  0   �.     �.     �.     �.     �.  
   �.     �.     /     /     /     /     ,/  $   A/  ~   f/     �/  
   �/     �/     0  
   0  	   "0  
   ,0     70     D0     L0     ]0     u0     �0     �0  5   �0     �0     �0     �0  !   �0     1  #   1     C1     V1     ]1     p1     �1     �1     �1     �1     �1     �1     2     2     52     >2     E2  
   J2     U2     d2  	   u2     2  v   �2     �2  *   3  c   :3     �3     �3     �3     �3  
   �3  
   �3  
   �3  
    4  
   4  
   4     !4     (4     C4     c4  	   z4     �4     �4     �4     �4  #   �4     �4     �4  
   �4     �4     5     5     ,5     ;5     K5     ]5     j5     v5     �5     �5     �5     �5  	   �5     �5     �5     �5     �5  �   �5     �6     �6     �6     7     7     7     &7     ,7  2   57     h7     p7     y7     �7     �7     �7     �7     �7     �7     
8     8     8     !8     78     O8     a8     v8     �8      �8  *   �8     �8     �8     �8     �8     9     9     !9     ?9  *   O9  2   z9     �9     �9     �9     �9     �9  	   :     :     :     ,:     E:     Q:     _:     l:     �:  
   �:  	   �:     �:     �:     �:     �:     �:     ;     ;     ";     +;  9   7;     q;  I   ;  !   �;  '   �;  	   <     <     &<  0   +<  
   \<  	   g<     q<     <     �<  	   �<     �<     �<     �<  '   �<  V   �<  3   M=  0   �=  (   �=     �=     �=  .   �=     +>     3>     8>     O>     \>     h>     m>     ~>  
   �>  
   �>     �>     �>     �>     �>     �>     ?  
   ?     ?  
   '?     2?     A?  ?   R?     �?     �?     �?     �?     �?     �?     �?     �?     @     @     "@     3@  	   C@     M@     Z@     i@     �@     �@     �@     �@     �@     �@     �@     �@     �@      A  �   A     �A      �A     �A     �A  	   
B     B     'B     6B     EB     RB     cB     {B     �B  2   �B     �B  &   �B     	C     C     1C     EC     WC  5   qC     �C     �C  &   �C     �C     �C     D     D     D  
   -D     8D     ?D  	   ND     XD     ^D     kD     }D     �D  	   �D     �D     �D  	   �D     �D  
   �D     �D     �D     �D     �D  ?   E  A   HE  �  �E  S   NG  =   �G     �G  K   �G  6   2H  -   iH  I   �H  x   �H  �   ZI  �   #J  �   �J  �   6K  �   �K  9   AL  �   {L  �   M  }   �M  h   N  k   ~N  �   �N  ;   �O  =   �O    8P  j   LQ  ]   �Q     R  �   �R  7   *S     bS  �   hS  |   T     �T     �T     �T     �T     �T  	   �T     �T  D   �T     U     U     -U     ;U  H   GU     �U     �U     �U  !   �U     �U  P   V     SV     bV     kV  U   {V     �V     �V  	   �V  
   �V     �V     W     W  �   W  
   �W     �W     �W     �W  	   �W  ^   �W  f   ^X  �   �X     hY  	   yY     �Y     �Y  
   �Y     �Y     �Y     �Y     �Y     �Y  	   �Y     �Y     �Y     �Y     �Y  	   �Y     Z  
   
Z     Z     Z     *Z     ;Z     KZ  
   QZ     \Z     tZ     �Z  	   �Z     �Z     �Z     �Z     �Z     �Z     �Z     �Z     �Z  
   �Z     �Z     �Z  	   �Z     �Z     [     [     [     [     ,[     2[     @[     I[     O[     U[  
   [[     f[     v[     �[     �[     �[     �[     �[     �[     �[     �[     �[  	   �[     �[     �[     \     \     \  	   '\     1\     8\     J\     Y\     _\     m\     s\     �\     �\     �\     �\     �\     �\     �\     �\     �\  
   �\     �\     ]  
   ]     ]     "]     .]     <]     H]     P]  
   X]     c]  
   p]     {]     �]  	   �]     �]     �]     �]     �]  
   �]     �]     �]     �]     �]     ^     ^      ^  
   /^     :^  �  M^  Y   8`  R   �`  j   �`     Pa  0   \a  p   �a  :   �a     9b  +   Lb  �   xb  S   %c     yc     �c     �c  I   �c     d  K   5d     �d  �   �d     :e     Qe     ce  B   {e  �   �e  �   Mf  <   g     >g     Og     dg     g  :   �g     �g  
   �g  )   �g  '    h  ]   Hh  =   �h  z   �h  b   _i  a   �i  T   $j  <   yj  ?   �j  
   �j     k     k     0k     Hk     _k     ~k  6   �k     �k  �   �k  *  �l  5   �m  
   +n     6n  
   Rn     ]n  &   ln  (   �n      �n     �n     ]o     no     �o     �o     �o  )   �o     �o     �o  '   p  #   :p  Z   ^p  Z   �p  I   q     ^q  A   qq     �q     �q  &   �q  4   r  E   Lr  )   �r  2   �r  R   �r  P   Bs  (   �s  ;   �s  )   �s  8   "t  Q   [t     �t     �t     �t  '   �t  (   u     <u     Wu     `u     iu  p   xu     �u     v  !   v  '   6v     ^v     zv     �v     �v     �v  $   �v  0   �v  )   w  �   6w  ,   x     8x     Ux  8   nx  )   �x  '   �x  #   �x  $   y     By  '   Yy  1   �y  '   �y     �y     �y  H   �y  ,   Hz     uz     �z  E   �z  !   �z  O   {     c{     {  !   �{  2   �{     �{     �{  *   |     :|  ,   N|  (   {|  -   �|  5   �|     }     }     +}     8}  "   O}  "   r}     �}     �}  �   �}     ~~  E   �~  �   �~     �     �  H   �  F   �     A�     W�     m�     ��     ��     ��     ŀ  N   Ҁ  U   !�  ,   w�     ��  &   ā     �      �  '   �  =   =�  '   {�     ��  #   ��  +   Ԃ  "    �  #   #�     G�     g�     ��  %   ��  #   ��     ߃  *   ��     (�     F�  +   f�     ��  #   ��     Ʉ     ք  $   �  y  �  0   ��     ��  @   Ԇ     �      $�  %   E�     k�     x�  �   ��     *�     B�     O�  .   \�  ?   ��  S   ˈ  &   �  :   F�  4   ��     ��     Ɖ     �  8   �  @   '�  4   h�  2   ��  &   Њ  
   ��  H   �  e   K�     ��  !   ŋ     �  ,   �     4�  &   G�  6   n�  !   ��  Z   ǌ  r   "�  &   ��  *   ��  <   �  *   $�  
   O�     Z�     g�  =   t�  1   ��     �     ��  '   �  5   B�  '   x�  !   ��  !     +   �  ,   �  *   =�  ,   h�  -   ��     Ð  "   ߐ     �     �  q   (�     ��  �   ��  O   >�  S   ��     �     ��     �  H   %�     n�     ��     ��  &   Ɠ  ,   �  
   �     %�     2�     C�  <   [�  �   ��  E   7�  M   }�  B   ˕  2   �     A�  e   N�     ��     ��     ʖ     �     ��     �  5   #�     Y�     q�     ��     ��     ×     ݗ  3   ��  F   /�     v�     ��  !   ��     ��  #   ݘ  0   �  t   2�     ��     ��  -   ֙  G   �     L�     Y�  "   j�  &   ��     ��     Ț  !   ޚ      �      �     <�  !   Z�  &   |�     ��  (   ��     ߛ  4   �  )   !�     K�  .   X�     ��     ��  #   ��  �   ۜ  ,   ĝ  8   �  7   *�  <   b�     ��  "   ��     ՞     �  (   �  ,   :�  X   g�  9   ��  *   ��  _   %�     ��  S   ��  7   ��  7   -�  ?   e�  $   ��  I   ʡ  \   �     q�     ��  H   ��  ;   �  5   �  '   T�  
   |�  '   ��     ��     ��  .   ӣ     �     �     -�  
   M�     X�     i�     z�     ��     ��     ��  
   ä     Τ     �     	�     (�  .   @�  U   o�  W   ť  �  �  �   ��  �   ��     �  i   �  R   ��  H   ժ  �   �  �   ��  e  ��  �   ��  �   �    ѯ    ڰ  p   ߱    P�  �   V�  �   3�  �   �  �   ��  p  b�  n   ӷ  o   B�  T  ��  �   �  �   ɻ  �   h�    G�  b   [�  
   ��  2  ɾ  �   ��     ��     ��  '   ��     !�     ?�     Y�     t�  �   {�     �  .   (�     W�     v�    ��  ;   ��  #   ��  !    �  S   "�  8   v�  �   ��  +   E�     q�     ��  z   ��     )�  ,   6�  $   c�     ��     ��  )   ��     ��  Q  ��     4�     I�     h�     q�  
   x�  �   ��  �   �  S  ��     2�     C�     U�     n�     |�     ��     ��     ��     ��     ��     ��  	   �  (   �     6�     F�     c�     o�     ��     ��  "   ��     ��     ��     �  (   !�  @   J�  ,   ��     ��     ��     ��  	   ��     �     �     #�     1�     K�     [�     k�     �     ��     ��  +   ��     ��     ��     �  "   �     A�  !   U�     w�     ��     ��     ��      ��  +   ��  -   �  %   I�  %   o�     ��     ��  !   ��  $   ��      �     �  *    �     K�     X�  "   j�     ��     ��  &   ��     ��     ��  6   ��     1�     Q�      ]�     ~�      ��     ��     ��     ��  $   �     '�     9�     N�      k�     ��     ��     ��     ��     ��     ��     �     -�     9�     X�     p�     ~�     ��     ��     ��     ��     ��     �     �     %�     9�  %   O�  %   u�     ��  &   ��  "   ��     ��  �   �  !   ��     ��     ��   		This plugin provides bar for bookmarks.
		 %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i errors occurred, see log %i open item %i open items %i warnings occurred, see log <Unknown> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Add Application Add Bookmark Add Notebook Add bookmark/Show settings Add column Add new bookmarks to the beginning of the bar Add row Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files All Tasks Allow public access Always use last cursor position when opening a page An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Arithmetic Ascii graph (Ditaa) Attach File Attach image first Attachment Browser Author Auto
Wrap Auto indenting Automatically collapse sections on close page Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave interval in minutes Autosave version on regular intervals Back to Original Name Bazaar Bookmarks BookmarksBar Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Cannot write this file. Probably this is due to the lenght
of the file name, please try using a name with less
than 255 characters Cannot write this file. Probably this is due to the lenght
of the file path, please try using a folder structure resulting in less
than 4096 characters Capture whole screen Center Change columns Changes Characters Characters excluding spaces Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Clone row Code Block Column 1 Command Command does not modify data Comment Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Created Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Dependencies Description Details Diagram Discard note? Display line numbers Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root E_xport... Edit %s Edit Custom Tool Edit Image Edit Link Edit Table Edit _Source Editing Editing file: %s Enable Version Control? Enable plugin Enabled Equation Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File name too long: %s File path too long: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Get more plugins online Get more templates online Grid lines Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide Journal pane if empty Hide menubar in fullscreen mode Highlight current line Home Page Horizontal _Line Icon Images Import Page Include horizontal lines in the ToC Include subpages Index Index page Inline Calculator Insert Code Block Insert Date and Time Insert Diagram Insert Equation Insert GNU R Plot Insert Image Insert Link Insert Score Insert Screenshot Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Key bindings can be changed by clicking on a field with a key combination
in the list and then press the new key binding.
To disable a keybinding, select it in the list and use <tt>&lt;Backspace&gt;</tt>. Labels marking tasks Last Modified Leave link to new page Left Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log file Looks like you found a bug Make default application Map document root to URL Match _case Maximum number of bookmarks Maximum page width Menubar Modified Month Move Selected Text... Move Text to Other Page Move column ahead Move column backward Move text to Name Need output file to export MHTML Need output folder to export full notebook New File New Page New Page in %s New S_ub Page... Next No Applications Found No changes since last version No dependencies No document root defined for this notebook No plugin available to display objects of type: %s No such file: %s No such page: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Only Show Active Tasks Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open help Open in New Window Open in New _Window Open new page Open plugins folder Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Index Page Name Page Template Page already exists: %s Page not allowed: %s Paragraph Password Paste Path Bar Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please select a row, before you push the button. Please select more than one line of text Please specify a notebook Plugin Plugin "%s" is required to display this object Plugins Port Position in the window Pr_eferences Preferences Prev Print to Browser Proper_ties Properties Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Remove column Remove row Removing Links Rename page "%s" Repeated clicking a checkbox cycles through the checkbox states Replace _All Replace with Require authentication Restore page to saved version? Rev Right Right Side Pane Right margin position Row down Row up S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Screenshot Command Search Search _Backlinks... Search this section Section Section(s) to ignore Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Server not started Server started Server stopped Set New Name Set ToC fontsize Set default text editor Set to Current Page Show Line Numbers Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show full Page Name Show full page name Show in the toolbar Show right margin Show tasklist in sidepane Show the cursor also for pages that can not be edited Single _page Size Some error occurred while running "%s" Sort alphabetically Sort pages by tags Spell Checker Start Start _Web Server Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Task Task List Tasks Template Templates Text Text Files Text From _File... Text background color Text foreground color Text wrap mode The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The following parameters will be substituted
in the command when it is executed:
<tt>
<b>%f</b> the page source as a temporary file
<b>%d</b> the attachment directory of the current page
<b>%s</b> the real page source file (if any)
<b>%p</b> the page name
<b>%n</b> the notebook location (file or folder)
<b>%D</b> the document root (if any)
<b>%t</b> the selected text or word under cursor
<b>%T</b> the selected text including wiki formatting
</tt>
 The inline calculator plugin was not able
to evaluate the expression at the cursor. The table must consist of at least on row!
 No deletion done. Theme There are no changes in this notebook since the last version that was saved This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This page name cannot be used due to technical limitations of the storage This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds the page index pane to the main window.
 This plugin allows inserting 'Code Blocks' in the page. These will be
shown as embedded widgets with syntax highlighting, line numbers etc.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This plugin turns one section of the notebook into a journal
with a page per day, week or month.
Also adds a calendar widget to access these pages.
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 To_day Today Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Tray Icon Type Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unkown Image type Unkown Object Unspecified Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Use thumbnails Username Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Web Server Week Whole _word Width With this plugin you can embed a 'Table' into the wiki page. Tables will be shown as GTK TreeView widgets.
Exporting them to various formats (i.e. HTML/LaTeX) completes the feature set.
 Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Your system encoding is set to %s, if you want support for special characters
or see errors due to encoding, please ensure to configure your system to use "UTF-8" Zim Desktop Wiki Zoom _Out _About _Add _All Panes _Arithmetic _Back _Browse _Bugs _Cancel _Checkbox _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Duplicate Line _Edit _Edit Link _Edit Link or Object... _Edit Properties _Edit... _Emphasis _FAQ _File _Find _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _Move Here _Move Line Down _Move Line Up _New Page Here... _New Page... _Next _None _Normal Size _Numbered List _OK _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Print _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove _Remove Line _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In calendar:week_start:0 do not use horizontal lines macOS Menubar no grid lines readonly seconds translator-credits vertical lines with lines {count} of {total} Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2021-07-09 20:00+0300
Last-Translator: Luxo <wmd@ukr.net>
Language-Team: Ukrainian <uk@li.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2020-01-01 13:40+0000
X-Generator: Poedit 3.0
 		Це розширення забезпечує панель для закладок.
		 %(cmd)s
повернуто ненульовий статус виходу %(code)i Помилки %(n_error)i та попередження %(n_warning)i, дивитіся в журналі %A %d %B %Y Помилки %i, дивіться журнал %i відкрите завдання %i відкритих завдання %i відкритих завдань Попередження %i, дивіться журнал <Невідомо> Вікі для робочого стола Файл із назвою <b>"%s"</b> уже існує.
Ви можете використовувати інше ім'я або замінити наявний файл. Таблиця повинна мати принаймні один стовпчик Додати програму Додати закладку Додати зошит Додати закладку / Показати налаштування Додати стовпчик Додавати нові закладки на початок панелі Додати рядок Додає перевірку орфографії через gtkspell.

Це базовий модуль, що поставляється в складі zim.
 Вирівнювати Всі файли Всі завдання Дозволити загальнодоступний доступ Завжди використовувати останнє положення курсору під час відкриття сторінки Під час генерування зображення виникла помилка.
Зберегти джерельний текст незважаючи на помилку? Прокоментоване джерело сторінки Програми Арифметика Графік ASCII (Ditaa) Прикріпити файл Спочатку прикріпити зображення Attachment Browser Автор Автоматичний
Упаковка Автоматичний відступ Автоматично згортати розділи на закритій сторінці Автоматично збережена в zim версія Автоматично виділяти поточне слово при застосуванні форматування Автоматично перетворювати „CamelCase“ слова в посилання Автоматично перетворювати адреси файлів в посилання Інтервал автоматичного збереження в хвилинах Регулярне автозбереження версій Повернутися до оригінальної назви Базар Закладка Панель закладок Нижня панель Переглянути Список маркері_в _Налаштувати Неможливо змінити сторінку: %s Скасувати Не можливо записати цей файл. Напевно, це пов'язано з довжиною
імені файлу, спробуйте використовувати ім'я з менш
ніж 255 символів Не вдається записати цей файл. Можливо, це пов’язано з довжиною
шляху до файлу, спробуйте використати структуру папок, що призведе до зменшення
більше 4096 символів Зробити знімок всього екрана Центр Зміна стовпців Зміни Символи Символи без пробілів Перевірка _орфографії Список прапорці_в Класичний значок в лотку,
не використовувати новий значок стану в Ubuntu Очистити Клонувати рядок Блок коду Стовпчик 1 Команда Команда не змінює дані Коментар _Увесь зошит Налаштування програм Налаштувати модуль Налаштуйте програму, щоб відкривати посилання "%s" Налаштуйте програму для відкриття файлів
типу "%s" Вважати всі поля з відміткою завданнями Копіювати Копіювати адресу електронної пошти Копіювати шаблон Копіювати _як ... _Копіювати посилання Копіювати _місцезнаходження Не вдалося знайти виконуваний файл "%s" Не можу знайти зошит: %s Не вдалося знайти шаблон "%s" Не можу знайти файл або теку для цього зошита Не вдалося завантажити перевірку правопису Не вдалось відкрити: %s Не вдалось проаналізувати вираз Не вдалося прочитати:%s Неможливо зберегти сторінку: %s Створювати нову сторінку для кожної нотатки Створити теку? Створити _Вирізати Зовнішні інструменти Зовнішні інс_трументи Налаштувати ... Дата День Типовий Формат за замовчуванням для копіювання тексту в буфер обміну Типовий зошит Затримка Вилучити сторінку Вилучити сторінку "%s"? Видалити рядок Залежності Опис Деталі Діаграма Відхилити примітку? Відображати номери рядків Видалити всі закладки? Хочете відновити сторінку: %(page)s
в збережену версію: %(version)s ?

Всі зміни в останній збереженій версії будуть втрачені! Коренева тека документа _Експортувати... Редагувати: %s Зміна зовнішнього інструмента Редагувати зображення Редагувати посилання Редагувати таблицю Редагувати д_жерело Редагування Редагувальний файл: %s Увімкнути контроль версій? Увімкнути розширення Увімкнено Рівняння Помилка в %(file)s у рядку %(line)i біля "%(snippet)s" Обчислення _мат. виразів Розгорнути _все Експортувати Експортувати всі сторінки в один файл Експорт завершено Експортувати кожну сторінку в окремий файл Експорт зошита Збій Помилка запуску: %s Помилка запуску програми: %s Файл існує Файл _Шаблонів... Файл змінено на диску:%s Файл існує Файл не можна записати:%s Назва файлу задовга: %s Шлях до файлу задовгий: %s Тип файлу не підтримується: %s Назва файлу Фільтр Знайти Зна_йти далі Знайти _попередній Знайти та замінити Знайти що Тека Тека вже існує і містить файли, експортування до цієї теки може перезаписати наявні файли. Ви хочете продовжити? Папка існує:%s Тека із шаблонами для файлів вкладень Для розширеного пошуку використовуйте знаки операції
AND, OR або NOT. Детальніше на довідковій сторінці. Фор_мат Формат Отримайте більше розширень в Інтернеті Отримайте більше шаблонів в Інтернеті Лінії сітки Заголовок _1 Заголовок _2 Заголовок _3 Заголовок _4 Заголовок _5 Висота Сховати область журналу, якщо вона порожня Приховати панель меню в повноекранному режимі Виділити поточний рядок Головна сторінка Горизонтальна _лінія Піктограма Зображення Імпортувати сторінку Включити горизонтальні лінії в ToC Включіть підсторінки Індекс Початкова сторінка Вбудований калькулятор Вставити блок коду Вставити дату й час Вставка діаграми Вставка формул Insert GNU R Plot Вставити зображення Вставити посилання Вставити оцінку Вставка знімків екрана Вставка символу Вставити таблицю Вставити текст із файлу Інтерфейс Ключове слово Interwiki Журнал Перейти до Перейти до сторінки Прив’язки клавіш можна змінити, клацнувши поле з комбінацією клавіш
у списку, а потім натисніть нову прив'язку клавіш.
Щоб вимкнути прив'язку клавіш, виберіть її у списку та скористайтеся <tt> & lt; Backspace & gt; </tt>. Ярлики маркування завдань Остання зміна Залиште посилання на нову сторінку Ліворуч Ліва бічна панель Сортувальник рядків Рядків Схема посилань Створювати посилання на файли в кореневому документі використовуючи повний шлях Посилання на Адреса Журнал Схоже, ви знайшли помилку Зробити програму за замовчуванням Перетворити шлях до кореневого документа в URL В_раховувати регістр Максимальна кількість закладок Максимальна ширина сторінки Меню бар Модифікований Місяць Перемістити виділений текст ... Перемістити текст на іншу сторінку Перемістити стовпчик вперед Перемістити стовпчик назад Перемістити текст до Назва Потрібен вихідний файл для експорту MHTML Потрібна вихідна тека, щоб експортувати повний блокнот Новий файл Створити сторінку Нова сторінка у %s Створити _під-сторінку... Наступний Програми не знайдено Немає змін з останньої версії Немає залежностей Для цього блокнота не визначено корінь документа Немає доступного розширення для відображення об’єктів типу:%s Немає такого файла: %s Немає такої сторінки: %s Жодної такої вікі не визначено: %s Шаблони не встановлені Зошит Зошити Гаразд Показувати лише активні завдання Відкрити теку п_рикріплень Відкрити теку Відкрити зошит Відкрити у програмі... Відкрити кореневий _документ Відкрити теку _зошита Відкрити сторінку Відкрити допомогу Відкрити в новому вікні В_ідкрити в новому вікні Відкрити нову сторінку Відкрити теку розширень Відкрити за допомогою "%s" Необов’язково Параметри модуля %s Інше… Вихідний файл Вихідний файл існує, вкажіть "--overwrite" для примусового експорту Вихідна тека Вихідна тека існує і не порожня, вкажіть "--overwrite", щоб примусити експортувати Місце розташування, необхідне для експорту Вихідні дані повинні замінити поточний вибір Перезаписати Р_ядок адреси Сторінка Сторінка "%s" не має теки для прикріплень Індекс сторінки Назва сторінки Шаблон сторінки Сторінка вже існує: %s Сторінка не дозволено: %s Абзац Пароль Вставити панель шляху Додайте коментар для цієї версії Зауважте, що посилання на неіснуючу сторінку
також автоматично створює нову сторінку. Вкажіть назву і теку для цього зошита. Виберіть рядок, перш ніж натиснути кнопку. Виберіть більше одного рядка тексту Будь ласка, вкажіть блокнот Модуль Для відображення цього об’єкта потрібне розширення "%s" Модулі Порт Позиція у вікні _Параметри Параметри Попередній Друкувати до веб-переглядача В_ластивості Властивості Швидка нотатка Швидка нотатка... Останні зміни Останні зміни ... Нещодавно _Змінені сторінки Форматування вікі-розмітки «на льоту» Вилучити Вилучити все Вилучити стовпчик Вилучити рядок Вилучення посилань Перейменувати сторінку "%s" Повторне клацання прапорця перемикається між станами прапорця Замінити _все Замінити на Вимагати автентифікації Відновити сторінку в збережену версію? Версія Праворуч Права бічна панель Позиція правого поля Рядок вниз Рядок вгору Зберегти вер_сію... Зберегти _копію... Зберегти копію Зберегти версію Зберегти закладки Збережена в Zim версія Кількість Команда знімка екрана Знайти Знайти з_воротні посилання... Шукати в цьому розділі Розділ Розділ(и) для ігнорування Вибрати файл Вибрати теку Вибрати Зображення Виберіть версію, щоб побачити зміни між цією версією та чинним
станом, або виберіть кілька версій, щоб побачити зміни між ними.
 Вибрати формат експорту Вибрати вихідний файл або теку Вибрати сторінки для експорту Вибрати вікно або область екрана Виділення Сервер не запущено Сервер запущено Сервер зупинено Встановити нову назву Змінити ToC розмір шрифта Встановити текстовий редактор за замовчуванням Встановити на поточну сторінку Показати номери рядків Показати ToC як плаваючий віджет, а не в бічній панелі Показати_зміни Показувати окремий значок для кожного зошита Показати повну назву сторінки Показати повну назву сторінки Показувати на панелі інструментів Показати праве поле Показати список завдань у бічній панелі Показувати курсор на сторінках тільки для читання Одна _сторінка Розмір Виникли деякі помилки під час роботи "%s" Сортувати в алфавітному порядку Сортувати сторінки за тегами Перевірка орфографії Старт Запустити _веб сервер Си_мвол... Синтаксис Система за замовчуванням Ширина вкладки Таблиця Редактор таблиць Зміст Позначки Завдання Список завдань Завдання Шаблон Шаблони Текст Текстові файли Текст із _файла... Колір тла тексту Колір тексту Режим перенесення тексту Тека
%s
ще не існує.
Ви хочете створити її зараз? Тека "%s" ще не існує.
Ви хочете створити її зараз? Наступні параметри будуть замінені
У команді, коли вона виконується:
<tt>
<b>%f</ b> джерело сторінки як тимчасовий файл
<b>%d</ b> каталог вкладення поточної сторінки
<b>%s</ b> Реальний файл джерела сторінки (якщо є)
<b>%p</ b> Назва сторінки
<b>%n</ b> розташування ноутбуків (файл або папку)
<b>%D</ b> Корінь документа (якщо є)
<b>%t</ b> вибраний текст або слово під курсором
<b>%T</ b> вибраний текст, включаючи форматування вікі
</tt>
 Модуль вбудованого калькулятора не
в змозі обчислити вираз під курсором. Таблиця повинна складатися принаймні на ряд!
  Не виконано жодного видалення. Тема Від часу збереження останньої версії зошита змін не було. Цей файл вже існує.
Хочете перезаписати його? Ця сторінка не має теки для прикріплень Назву цієї сторінки не можна використовувати через технічні обмеження сховища Цей модуль дає змогу робити знімок екрана і вставляти
його прямо на сторінку zim.

Цей модуль є базовим і поставляється із zim.
 Цей модуль додає діалог зі списком всіх відкритих
завдань в окремому зошиті. Відкриті завдання позначені
або галками або позначками «TODO» чи «FIXME».

Це базовий модуль, що поставляється в складі zim.
 Модуль додає діалог для вставки тексту або вмісту буферу 
обміну на сторінку zim.

Цей модуль основний, і поставляється разом з zim.
 Цей модуль додає значок в системний лоток.

Залежить від Gtk+ версії 2.10 або новішої.

Це базовий модуль, що поставляється в складі zim.
 Цей модуль додає додатковий віджет, що відображає список сторінок
посилання на поточну сторінку.

Це основний розширення, що постачається із zim.
 Цей модуль додає діалог «Вставка символу» і дає змогу
авто-форматувати типографські символи.

Це базовий модуль, що поставляється в складі zim.
 Цей плагін додає панель індексу сторінок до головного вікна.
 Цей модуль дозволяє вставляти «блок коду» на сторінці. Це буде
показані як вбудовані віджети з підсвічуванням синтаксису, номери рядків тощо.
 Цей модуль дозволяє вставляти арифметичні розрахунки в ZIM.
Вона заснована на арифметичному модулі з
http://pp.com.mx/python/arithmetic.
 Цей модуль дає змогу швидко обчислити прості
математичні вирази в zim.

Цей модуль є базовим і поставляється із zim.
 Цей модуль надає редактор діаграми для ZIM на основі Ditaa.

Це ядро модуля доставка з ZIM.
 Цей модуль надає редактор діаграм для zim базований на GraphViz.

Це базовий модуль, що поставляється в складі zim.
 Цей модуль показує діалог з графічним
поданням структури посилань зошита.
Його можна використовувати як «розумну»
схему, яка показує зв’язок посилань.

Це базовий модуль, що поставляється в складі zim.
 Цей модуль надає графічний редактор для ZIM, базований на GNU R.
 Цей модуль забезпечує редактор ділянки для ZIM на основі Gnuplot.
 Цей модуль реалізує обхідний спосіб друку, зважаючи
на відсутність підтримки друку в zim, шляхом експорту
поточної сторінки до HTML файлу і відкриває його у
веб-переглядачі. Припускаючи, що в веб-переглядачі
реалізована функція друку, ви подаєте ваші данні
до принтера в два кроки.

Це базовий модуль, що поставляється в складі zim.
 Цей модуль надає редактор рівнянь для zim базований на latex.

Це базовий модуль, що поставляється в складі zim.
 Цей модуль показує папку вкладення поточної сторінки як
Значок виду на нижній панелі.
 Цей плагін сортує вибрані лінії в алфавітному порядку.
Якщо список вже відсортований, замовлення буде змінено
(з A-z до z-a).
 Цей модуль перетворює один розділ ноутбука в журнал
з сторінкою на добу, тиждень або місяць.
Також додає віджет календаря для доступу до цих сторінок.
 Зазвичай це означає, що файл містить недійсні символи Назва Щоб продовжити, ви можете зберегти копію цієї сторінки
або відкинути зміни. Якщо ви збережете копію, зміни також
не будуть внесені, але ви зможете відновити їх пізніше. Для створення нового блокнота потрібно вибрати порожню папку.
Звичайно, ви також можете вибрати існуючу папку zim notebook.
 С_ьогодні Сьогодні Встановити прапорець Зняти прапорець Верхня панель Значок в лотку Тип Клавіша <BackSpace> зменшує відступи за табуляцією
(якщо вимкнено, використовуйте <Shift><Tab>) Невідомо Невідомий тип зображення Невідомий об'єкт Невизначений Оновити %i сторінку, що посилається на цю сторінку Оновити %i сторінки, що посилаються на цю сторінку Оновити %i сторінок, що посилаються на цю сторінку Оновити заголовок цієї сторінки Оновлення посилань Оновлення індексу Використовуйте %s, щоб перейти на бічну панель Використовувати власний шрифт Натискайте <Enter> для переходу за посиланнями
(якщо зайнято, використовуйте <Alt><Enter>) Використовувати ескізи Ім'я користувача Контроль версій Контроль версій для цього зошита вимкнений.
Хочете його увімкнути? Версії Переглянути _зауваження Переглянути _журнал Веб-сервер Тиждень Збігається _ціле слово Ширина За допомогою цього плагіна ви можете вставити "Таблицю" на вікі-сторінку. Таблиці відображатимуться як віджети GTK TreeView.
Експорт їх у різні формати (тобто HTML / LaTeX) завершує набір функцій.
 Статистика Кількість слів... Слів Рік Вчора Ви редагуєте файл в зовнішній програмі. Коли зробите, можете закрити цей діалог Ви можете налаштувати зовнішні інструменти показані
в меню та на панелі інструментів або в контекстних меню. Для системного кодування встановлено %s, якщо вам потрібна підтримка спеціальних символів
або побачите помилки через кодування, будь ласка, налаштуйте свою систему на використання "UTF-8" Zim Desktop Wiki _Зменшити _Про програму _Додати _Всі панелі _Арифметика На_зад _Переглянути П_омилки _Скасувати _Прапорець В_низ Скинути _форматування _Закрити _Зруйнувати все _Зміст _Копіювати _Копіювати сюди В_илучити В_илучити сторінку _Відкинути зміни _Дублювати рядок _Правка _Редагувати посилання Редагувати посилання або _об’єкт... _Редагувати властивості _Редагувати... _Курсив _Часті питання _Файл _Знайти З_найти... В_перед На _весь екран Пере_йти _Довідка _Виділення _Історія _Головна _Зображення... _Імпортувати сторінку... Вст_авити _Стрибок Перейти _до... _Комбінації клавіш _Посилання Посилання _на дату _Посилання... _Підкреслений _Більше _Перемістити _Перемістити сюди _Перемістити рядок вниз _Перемістити рядок вгору _Нова сторінка тут ... _Створити сторінку... Нас_тупний _Сховати Нормальний розмір _Номерований список _Гаразд _Відкрити _Відкрити інший зошит... _Інше... _Сторінка _Ієрархія сторінок _Вгору Вст_авити _Попередній перегляд П_опередій _Друкувати _Друкувати до веб-переглядача _Швидка нотатка... Ви_йти _Недавні сторінки Пов_торити _Регулярний вираз _Перезавантажити _Вилучити _Вилучити рядок В_илучити посилання За_мінити _Замінити... С_кинути розмір _Відновити версію З_берегти Зберегти _копію _Знімок екрана... З_найти З_найти... Надіслати _до... _Бічні панелі _Поруч _Сортувати рядки _Викреслений _Жирний _Нижній індекс _Верхній індекс _Шаблони С_ервіс Пов_ернути _Стенографічний _Версії... _Вигляд _Збільшити calendar:week_start:1 не використовується горизонтальні рядки Меню бар macOS Немає сіткових ліній тільки для читання секунд(и) Launchpad Contributions:
  Luxo https://launchpad.net/~wmd-o
  Sergiy Gavrylov https://launchpad.net/~gavro
  atany https://launchpad.net/~ye-gorshkov вертикальні рядки з лініями {count} з {total} 