��    �     D              l*  ,   m*  .   �*  ?   �*     	+     +     2+  0   N+     +     �+     �+  	   �+     �+  %   �+  i   �+  *   g,     �,     �,     �,     �,     �,  
   �,  -   �,     -  V   -     v-  	   |-  	   �-     �-  3   �-     �-     �-  Y   .     j.     �.  .   �.  
   �.     �.     �.     �.     �.     /     /     &/  	   -/     7/  -   F/  *   t/  $   �/  ?   �/  /   0  (   40     ]0  %   z0  ,   �0     �0  	   �0     �0     �0  
   1     1  	   1      1     -1     :1     F1     M1  
   Z1     e1     }1  �   �1  �   2     �2     �2     �2     �2  
   �2     �2     �2     3     3     23     B3  <   Q3     �3  	   �3  
   �3     �3     �3     �3     �3     �3     �3     4     4     54  +   F4  3   r4      �4     �4     �4     �4     �4  
   �4     5     5     *5     I5     e5  3   �5     �5     �5     �5     6     6     -6     M6     \6     d6     i6     v6     �6     �6     �6     �6  0   �6     �6     �6     �6     �6  
   7     7     7     '7     37     ;7     C7     Q7     f7  $   7  ~   �7     #8     18  
   58     @8     H8  
   Y8  	   d8  
   n8     y8     �8     �8     �8     �8     �8     �8  5   �8     9     9     '9  !   .9     P9  #   a9     �9     �9     �9     �9     �9     �9     �9     :     :     -:  .   L:     {:     �:     �:     �:     �:     �:  
   �:     �:     �:  	   ;  6   ;     F;  v   M;     �;  *   �;  c   <     e<     m<     t<     {<     �<     �<     �<     �<     �<  
   �<  
   �<  
   �<  
   �<  
   �<  
   =     =     =     5=     U=  	   l=     v=     �=     �=     �=  %   �=     �=     �=  #   �=     >     >  
   >     &>     8>     J>     _>     n>     {>     �>     �>     �>     �>     �>     �>     �>     �>     
?     ?  	   -?     7?     I?     Q?     Y?     f?     r?     ?     �?     �?     �?     �?     �?     �?     �?     �?  2   �?     !@     )@     2@     L@     U@     p@     �@     �@     �@     �@     �@     �@  	   �@     A     A     A     +A     CA     UA     jA     wA      |A  *   �A     �A     �A     �A     �A     �A     B     B      B     6B     TB  *   dB  2   �B     �B     �B     �B     �B     C     %C  	   .C     8C     ;C     RC     kC     wC     �C     �C     �C  
   �C     �C  	   �C     �C     �C     D     D     1D     @D     ID     _D     hD  9   tD     �D  I   �D  !   E  '   (E  	   PE     ZE     cE  0   hE  
   �E  	   �E     �E     �E  &   �E     �E     F  	   F     'F     0F     6F  '   ?F  V   gF  3   �F  0   �F  (   #G     LG     fG  .   mG     �G     �G     �G     �G  !   �G  !   �G     H     H     "H     3H     ;H  
   GH  &   RH  
   yH     �H     �H     �H     �H     �H     �H  
   �H     �H     I  
   I     )I     8I     DI     XI  ?   iI     �I     �I     �I     �I     �I     �I     J     J     )J     2J     9J     JJ  	   ZJ     dJ     qJ     �J     �J     �J     �J     �J     �J     �J     �J     �J     K     K     +K  �   8K     �K      �K     L     L  	   5L     ?L     PL     cL     rL     �L     �L     �L     �L     �L     �L  2   �L     (M  &   6M  $   ]M     �M     �M     �M     �M     �M     �M  5   �M  &   4N     [N     hN     mN     sN  &   �N     �N     �N     �N     �N     �N     �N     O  
   O     (O     /O  	   >O     HO     NO     [O     mO     rO     �O  	   �O     �O     �O  	   �O     �O  
   �O     �O     �O     �O     P  ?   P  A   VP  �  �P  S   \R  =   �R     �R  K   �R  )   @S  @   jS  6   �S  -   �S  I   T  x   ZT  �   �T  �   zU  �   CV  �   �V  �   VW  }   �W  L   UX  �   �X  9   ,Y  �   fY  �   Z  �   �Z  }   *[  C   �[  s   �[  �   `\  h   6]  k   �]  �   ^  -   �^  R   _  ;   `_  =   �_  v   �_    Q`  j   ea  n   �a  ]   ?b     �b  �   c  7   �c     �c  �   �c  |   �d     	e     e     e     e     .e     Be     Ve  
   _e  9   je  	   �e  '   �e     �e     �e  D   �e     2f     :f     Lf     Zf     ff  H   of     �f     �f     �f  !   �f     g     *g     >g  +   Zg  P   �g     �g     �g     �g  U   �g     Uh     ^h  	   nh     xh  
   �h     �h  N   �h     �h     �h     �h  �   i  
   �i     �i     �i     �i  	   �i  ^   �i  f   Nj  �   �j     Xk  	   ik     sk     zk  
   k     �k     �k     �k     �k     �k  	   �k     �k     �k     �k     �k  	   �k     �k  
   �k     l     l     l     +l     ;l  
   Al     Ll     dl     ul  	   ~l     �l     �l     �l     �l     �l     �l     �l     �l  
   �l     �l     �l  	   �l     �l     �l     �l     m     m     m     "m     0m     9m     ?m     Em  
   Km     Vm     fm     tm     �m     �m     �m     �m     �m     �m     �m     �m  	   �m     �m     �m     �m     n     n  	   n     !n     (n     :n     In     On     ]n     cn     wn     n     �n     �n     �n     �n     �n     �n     �n  
   �n     �n     �n  
   �n     o     o     o     ,o     8o     @o  
   Ho     So  
   `o     ko     ro  	   xo     �o     �o     �o     �o     �o     �o  
   �o     �o     �o     p     p     #p     +p     >p  
   Mp     Xp  �  kp  6   Qr  >   �r  Q   �r     s     %s  +   <s  3   hs  +   �s  0   �s     �s     t     t  &   &t  l   Mt  *   �t     �t     �t     u     u  (   #u     Lu  /   \u     �u  k   �u     v     v      v     1v  K   Jv     �v  '   �v  a   �v     >w     [w  1   hw     �w     �w     �w     �w     �w     �w     x     
x     x     x  <   3x  =   px  ,   �x  D   �x  :    y  6   [y  $   �y  0   �y  :   �y     #z  	   ?z     Iz     Yz     iz     wz  
   }z     �z     �z     �z     �z     �z     �z  $   �z     {  �   {  �   �{     ^|     y|     �|     �|     �|     �|     �|     �|     �|     	}     }  S   <}     �}     �}     �}  	   �}     �}     �}  
   �}     �}     ~     &~     B~     Z~  6   p~  ?   �~  1   �~                >     O     _     n      |  (   �  $   �  &   �  <   �  +   O�     {�  !   ��     ��  !   ǀ  &   �     �     !�     (�     0�     L�     i�     y�     �     ��  6   ��     ʁ     �     �  $   ��      �     .�     7�     D�     Q�     Z�     c�     {�     ��  "   ��  �   ւ     h�     x�     ��  	   ��      ��     ��     ̃     ڃ     �     ��      �     �     8�  
   I�  	   T�  >   ^�     ��     ��     ��  /   ʄ     ��  +   �     <�     Y�     `�  &   w�     ��     ��  /   Ʌ     ��  #   �  +   2�  =   ^�  %   ��  #         �     �     �     "�     )�     ;�     L�     c�  G   q�     ��  �   ��     C�  -   Y�  {   ��     �     �     �  $   �  #   D�     h�     l�     t�     {�     ��     ��     ��     ��     Ή     ܉     �  *   �  6   �     S�     m�     ~�     ��     ��     ��  <   ��  	   �     ��  *   �     8�     L�     S�     g�     }�     ��     ��          ԋ     �     �     �     #�     5�     E�     b�     ��     ��     ��     ��     Ȍ     ڌ     �     �     ��     �     �     :�     A�     W�  	   w�     ��     ��     ��     ��  K   ƍ     �  
   �  #   )�     M�     a�  1   �     ��  $   Ў  #   ��     �     7�     P�  	   `�  
   j�     u�     y�     ��     ��     ͏     �     �  /   ��  D   )�     n�     ��     ��     ��     ��     Ȑ  	   א     �  %    �     &�  J   7�  ?   ��          ۑ     ��  "   �     5�     U�     ^�     q�     y�     ��     ��     ��     ͒     ڒ  '   ��     �  )   ,�     V�     b�     }�     ��     ��     ͓     ܓ     �     �     �  U   �     t�  e   ��  '   �  .   �     F�     T�     j�  ;   r�     ��     ��     ֕     �  1   �     6�     O�     c�     l�     x�     ~�  1   ��  a   Ŗ  C   '�  :   k�  0   ��  !   ח  
   ��  8   �     =�     I�     P�     h�  /   v�  0   ��     ט     �     �     �     
�     �  &   #�     J�     W�     g�     y�      ��  $   ��     љ     ڙ     �     ��     �     �     .�     H�      g�  _   ��     �     ��     	�  .   �     N�     S�     [�     q�  
   ��     ��     ��     ��     ʛ     ܛ     �      �     �     (�     D�     K�     c�  	   {�     ��     ��     ��     ̜     �  �   ��  %   ��  )   ��  &   ݝ     �  
   "�     -�     C�     _�     r�     ��  &   ��  &   ��     �     �     !�  F   A�     ��  -   ��  (   ǟ  "   �  "   �     6�  #   M�     q�  .   ��  *   ��  +   �     �     �     "�     +�  $   G�     l�     ��  
   ��     ��     ơ     Ρ     �      �     �     �     1�     F�     L�     ]�  	   e�  $   o�     ��     ��     ��  	   ��  
   ��     Ƣ     ̢     ޢ     ��     �     �  8   6�  ;   o�    ��  S   ��  F   �     K�  J   P�  3   ��  F   Ϧ  0   �  8   G�  b   ��  �   �  �   ��  �   `�  �   I�  �   
�  �   ��  �   A�  `   լ  �   6�  N   �  �   8�  �   �  �   ��  �   �  \   ��  �   �  �   ��  M   ��     в  	  P�  B   Z�  R   ��  ?   �  F   0�  �   w�  C  ��  w   A�  b   ��  p   �  �   ��  �   �  J   ɹ     �  �   �  �   ͺ     v�     ~�     ��     ��     ��     ��     ͻ     ܻ  C   ��      =�  =   ^�     ��     ��  E   ��     ��     	�     $�     7�     G�  Q   T�  -   ��     Խ     �  $   ��  $   #�     H�  '   b�  I   ��  ^   Ծ     3�     C�     U�  T   j�  	   ��     ɿ     ޿     �     �     �  Z   �     w�     ��     ��  �   ��     ]�     m�     ��     ��     ��  a   ��  �   ��  �   ��     G�     ^�  
   f�  	   q�     {�     ��     ��  	   ��     ��  	   ��     ��     ��     ��     ��     ��     ��     �     �     �     "�     2�     E�     V�     ^�     m�     ��  
   ��  	   ��     ��     ��     ��  
   ��  	   ��     ��      �     �  	   �  
   �      �  
   (�     3�  	   H�     R�     Z�     g�     z�     ��     ��     ��     ��     ��     ��     ��     ��     ��     �  
   �     '�     0�     @�     P�     Y�     `�     ~�     ��     ��     ��     ��     ��     ��  	   ��     ��     ��     ��     �     �     !�  	   5�  	   ?�     I�     Y�     j�     v�     ��     ��     ��     ��     ��     ��  
   ��     ��      �     �     �     0�  	   8�     B�     N�     \�     h�  	   v�     ��     ��     ��     ��     ��     ��     ��     ��     �     �     3�     H�     X�  �  a�     K�  
   ^�     i�   		This plugin provides bar for bookmarks.
		 %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i Attachment %i Attachments %i errors occurred, see log %i file will be deleted %i files will be deleted %i open item %i open items %i warnings occurred, see log <Top> <Unknown> A desktop wiki A file with that name already exists. A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Action Add Application Add Bookmark Add Notebook Add bookmark/Show settings Add column Add new bookmarks to the beginning of the bar Add row Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files All Tasks Allow public access Always use last cursor position when opening a page Always wrap at character Always wrap at word boundaries An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Are you sure you want to delete the file '%s'? Arithmetic Ascii graph (Ditaa) Attach File Attach image first Attachment Browser Attachments Attachments: Author Auto
Wrap Auto indenting Automatically collapse sections on close page Automatically expand sections on open page Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave interval in minutes Autosave version on regular intervals Autosave version when the notebook is closed Back to Original Name BackLinks BackLinks Pane Backend Backlinks: Bazaar Bookmarks BookmarksBar Border width Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Cannot write this file. Probably this is due to the lenght
of the file name, please try using a name with less
than 255 characters Cannot write this file. Probably this is due to the lenght
of the file path, please try using a folder structure resulting in less
than 4096 characters Capture whole screen Center Change columns Changes Characters Characters excluding spaces Check Checkbox '>' Check Checkbox 'V' Check Checkbox 'X' Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Clone row Code Block Column 1 Command Command does not modify data Comment Common include footer Common include header Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Copy link to clipboard Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Created Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Demote Dependencies Description Details Diagram Discard note? Display line numbers Distraction Free Editing Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root Due E_xport... Edit %s Edit Custom Tool Edit Image Edit Link Edit Table Edit _Source Editing Editing file: %s Enable Version Control? Enable plugin Enabled Equation Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File name should not be blank. File name should not contain path declaration. File name too long: %s File path too long: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Fossil Get more plugins online Get more templates online Git Gnuplot Go back Go to home page Grid lines Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide Journal pane if empty Hide menubar in fullscreen mode Highlight current line Home Page Horizontal _Line Icon Icons & Text horizontal Id Id "%s" not found on the current page Images Import Page Include horizontal lines in the ToC Include subpages Index Index page Inline Calculator Insert Code Block Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Sequence Diagram Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Key Binding Key bindings Labels marking tasks Large Last Modified Leave link to new page Left Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Managing table columns Map document root to URL Match _case Maximum number of bookmarks Maximum page width Menubar Mercurial Modified Month Move Selected Text... Move Text to Other Page Move column ahead Move column backward Move text to Name Need output file to export MHTML Need output folder to export full notebook Never wrap lines New File New Page New Page in %s New S_ub Page... New _Attachment Next No Applications Found No changes since last version No dependencies No document root defined for this notebook No plugin available to display objects of type: %s No such file: %s No such page: %s No such wiki defined: %s No templates installed No text selected Notebook Notebooks OK Only Show Active Tasks Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open cell content link Open help Open in New Window Open in New _Window Open new page Open plugins folder Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Index Page Name Page Template Page already exists: %s Page is read-only and cannot be edited Page not allowed: %s Page section Paragraph Password Paste Path Bar Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please select a row, before you push the button. Please select more than one line of text Please specify a notebook Plugin Plugin "%s" is required to display this object Plugins Port Position in the window Pr_eferences Prefer short link names for pages Prefer short names for page links Preferences Prev Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Remove column Remove links to %s Remove row Removing Links Rename file Rename or Move Page Rename page "%s" Repeated clicking a checkbox cycles through the checkbox states Replace _All Replace with Require authentication Restore page to saved version? Rev Right Right Side Pane Right margin position Row down Row up S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Screenshot Command Search Search _Backlinks... Search this section Section Section(s) to ignore Section(s) to index Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Sequence Diagram Server not started Server started Server stopped Set New Name Set ToC fontsize Set default text editor Set to Current Page Show Line Numbers Show Tasks as Flat List Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show edit bar along bottom of editor Show full Page Name Show full page name Show helper toolbar Show in the toolbar Show right margin Show tasklist in sidepane Show the cursor also for pages that can not be edited Show the page title heading in the ToC Single _page Size Small Smart Home key Some error occurred while running "%s" Sort alphabetically Sort pages by tags Source View Spell Checker Start Start _Web Server Support thumbnails for SVG Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Tags for non-actionable tasks Task Task List Tasks Template Templates Text Text Files Text From _File... Text background color Text foreground color Text wrap mode The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The following parameters will be substituted
in the command when it is executed:
<tt>
<b>%f</b> the page source as a temporary file
<b>%d</b> the attachment directory of the current page
<b>%s</b> the real page source file (if any)
<b>%p</b> the page name
<b>%n</b> the notebook location (file or folder)
<b>%D</b> the document root (if any)
<b>%t</b> the selected text or word under cursor
<b>%T</b> the selected text including wiki formatting
</tt>
 The inline calculator plugin was not able
to evaluate the expression at the cursor. The table must consist of at least on row!
 No deletion done. Theme There are no changes in this notebook since the last version that was saved There was a problem loading this plugin

 This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This page name cannot be used due to technical limitations of the storage This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a "path bar" to the top of the window.
This "path bar" can show the notebook path for the current page,
recent visited pages or recent edited pages.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds the page index pane to the main window.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows inserting 'Code Blocks' in the page. These will be
shown as embedded widgets with syntax highlighting, line numbers etc.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin also has properties,
see the notebook properties dialog This plugin cannot be enabled due to missing dependencies.
Please see the dependencies section below for details.

 This plugin opens a search dialog to allow quickly executing menu entries. The search dialog can be opened by pressing the keyboard shortcut Ctrl+Shift+P which can be customized via Zim's key bindings preferences. This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a macOS menubar for zim. This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a sequence diagram editor for zim based on seqdiag.
It allows easy editing of sequence diagrams.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This plugin turns one section of the notebook into a journal
with a page per day, week or month.
Also adds a calendar widget to access these pages.
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox '>' Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Trash Page Trash failed, do you want to permanently delete instead ? Tray Icon Turn page name into tags for task items Type Un-check Checkbox Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unkown Image type Unkown Object Unspecified Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use a page for each Use date from journal pages Use horizontal scrollbar (may need restart) Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Use thumbnails Username Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log View debug log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s With this plugin you can embed a 'Table' into the wiki page. Tables will be shown as GTK TreeView widgets.
Exporting them to various formats (i.e. HTML/LaTeX) completes the feature set.
 Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Your system encoding is set to %s, if you want support for special characters
or see errors due to encoding, please ensure to configure your system to use "UTF-8" Zim Desktop Wiki Zoom _Out _About _Add _All Panes _Arithmetic _Back _Browse _Bugs _Cancel _Checkbox _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Duplicate Line _Edit _Edit Link _Edit Link or Object... _Edit Properties _Edit... _Emphasis _FAQ _File _Find _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _Move Here _Move Line Down _Move Line Up _New Page Here... _New Page... _Next _None _Normal Size _Numbered List _OK _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Print _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove _Remove Line _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In as due date for tasks as start date for tasks calendar:week_start:0 do not use horizontal lines macOS Menubar no grid lines readonly seconds translator-credits vertical lines with lines {count} of {total} Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2021-09-30 17:37+0000
Last-Translator: Adolfo Jayme Barrientos <fitojb@ubuntu.com>
Language-Team: Spanish <https://hosted.weblate.org/projects/zim/master/es/>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.9-dev
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
 		Este complemento provee una barra para favoritos.
		 %(cmd)s
retornó un status de salida diferente a cero %(code)i Han ocurrido %(n_error)i errores y %(n_warning)i advertencias, revise el registro %A %d %B %Y %i Adjunto %i Adjuntos Han ocurrido %i errores, revise el registro Se eliminará %i archivo Se eliminarán %i archivos %i artículo abierto %i artículos abiertos Han ocurrido %i advertencias, revise el registro <Inicio> <Desconocido> Un wiki de escritorio Ya existe otro archivo con ese nombre. Un archivo con el nombre <b>"%s"</b> ya existe.
Puede usar otro nombre o sobreescribir el archivo existente. Una tabla debe tener al menos una columna. Acción Añadir aplicación Añadir marcador Añadir cuaderno Agregar marcador/Mostrar configuraciones Añadir columna Añadir nuevos marcadores al inicio de la barra Añadir fila Adiciona corrección ortográfica usando gtkspell.

Esta es una extensión principal suministrada con zim.
 Alinear Todos los Archivos Todas las tareas Permitir acceso público Utilizar siempre la última posición del cursor cuando se abre una página Ajustar siempre por carácter Ajustar siempre por límites de palabra Ocurrió un error mientras se generaba la imagen.
¿Desea guardar el texto fuente de todos modos? Fuente anotada de la página Aplicaciones ¿Confirma que quiere eliminar el archivo «%s»? Aritmética Gráficos Ascii (Ditaa) Adjuntar Archivo Adjuntar imagen primero Adjuntar Navegador Anexos Adjuntos Autor Ajuste
autom. Sangría automática Colapsar automáticamente las secciones al cerrar la página Expandir automáticamente las secciones en la página abierta Versión guardada automáticamente desde zim Seleccionar automáticamente la palabra actual al aplicar un formato Convertir automáticamente palabras "CamelCase" en enlaces Convertir automáticamente rutas de archivo en enlaces Intervalo de autoguardado en minutos Auto-guardar una versión a intervalos regulares Autoguardar versión cuando el cuaderno de notas se cierre Regresar al nombre original BackLinks Panel Backlinks Administración Enlazado por: Bazar Marcadores Barra de Marcadores Grosor del borde Panel inferior Examinar Lista viñe_tas _Configurar No se puede modificar la página: %s Cancelar No se puede escribir este archivo. Probablemente esto se deba a la longitud
del nombre del archivo, intente usar un nombre con menos
de 255 caracteres No se puede escribir este archivo. Probablemente esto se deba a la longitud
de la ruta del archivo, intente utilizar una estructura de carpetas que resulte en menos
de 4096 caracteres Capturar pantalla completa Centrado Cambiar columnas Cambios Letras Caracteres excluyendo espacios Seleccionar Casilla '>' Seleccionar Casilla 'V' Seleccionar Casilla 'X' Revisar _ortografía Lista _casillas de selección Bandeja del sistema clásica,
no usar el nuevo estilo de ícono de estado en Ubuntu Vaciar Clonar fila Bloque de código Columna 1 Comando El comando no modifica datos Comentario Incluir pié de página común Incluir cabecera común Cuaderno de _notas completo Configurar aplicaciones Configurar extensión Configurar una aplicación para abrir los enlaces "%s" Configurar una aplicación para abrir los archivos de tipo "%s" Considerar todas las marcas de cotejo como tareas Copiar Copiar dirección de correo-e Copiar Plantilla Copiar _como… Copiar en_lace Copiar _lugar Copiar enlace en el portapapeles No se puede encontrar el ejecutable "%s" No se pudo encontrar el cuaderno: %s No se pudo encontrar la plantilla "%s" No se pudo encontrar el archivo o carpeta para este cuaderno No se pudo cargar el corrector ortográfico No se puede abrir: %s No se pudo analizar la expresión No se pudo leer: %s No se pudo guardar la página: %s Crear una página nueva para cada nota ¿Crear carpeta? Creado Cor_tar Herramientas personalizadas Herramien_tas personalizadas Personalizar… Fecha Día Predeterminado Formato predeterminado para copiar texto a la papelera Cuaderno predeterminado Retardo Eliminar página ¿Quiere eliminar la página «%s»? Eliminar fila Inferior Dependencias Descripción Detalles Diagrama ¿Desea borrar la nota? Mostrar números de renglón Edición sin distracciones Desea borrar todos los marcadores? ¿Desea restaurar la página: %(page)s
a su versión guardada: %(version)s ?

¡Todos los cambios hechos desde la versión guardada se perderán! Documento raíz Fecha límite E_xportar... Editar %s Editar herramienta personalizada Editar imagen Editar enlace Editar tabla Editar _Código Editando Editando el archivo. %s ¿Activar control de versiones? Habilitar plugin Habilitado Ecuación Error en %(file)s en la línea %(line)i cerca de "%(snippet)s" Evaluar _matemática Expandir _todo Exportar Exportar todas las páginas a un único archivo Exportación completa Exportar cada página a un archivo separado Exportando cuaderno de notas Falló No se pudo ejecutar %s No se pudo ejecutar la aplicación: %s El archivo ya existe Plantilla de _archivo %s de los archivos del disco han sido cambiados El archivo ya existe El Archivo no se puede escribir: %s El nombre de archivo no debe quedar vacío. El nombre de archivo no debe incluir la declaración de ruta. Nombre de archivo demasiado largo: %s Ruta de archivo demasiado larga: %s Tipo de archivo no soportado: %s Nombre del archivo Filtrar Buscar Buscar _siguiente Buscar _anterior Encontrar y reemplazar Econtrar qué Las tareas marcadas vencen el lunes o el martes antes del fin de semana Carpeta La carpeta existe y no está vacía, si exporta a esta carpeta puede sobreescribirse el contenido de la misma. ¿Desea continuar? La carpeta existe: %s Carpeta con plantillas para archivos adjuntos Para búsquedas avanzadas puede user operadores
como «AND», «OR» y «NOT». Vea la página de ayuda
para más detalles. _Formato Dar formato Fósil Obtener más extensiones en Internet Obtener más plantillas de Internet Git Gnuplot Volver Ir a la página inicial Lineas de grilla Encabezado _1 Encabezado _2 Encabezado _3 Encabezado _4 Encabezado _5 Altura Ocultar página del Diario si está vacía Esconder la barra de menu en el modo pantalla completa Resaltar la línea actual Página personal Línea _horizontal Icono Iconos y texto horizontal Id. El identificador «%s» no se encuentra en la página actual Imágenes Importar página Incluir líneas horizontales en el sumario Incluir subpáginas Indice Índice de páginas Calculadora en línea Insertar bloque de código Introduzca fecha y hora Insertar diagrama Insertar Diagrama Insertar ecuación Insertar gráfico de GNU R Insertar Gnuplot Insertar imagen Inserta un enlace Agregar puntaje Insertar captura de pantalla Insertar diagrama de secuencia Insertar símbolo Insertar tabla Insertar Texto desde Archivo Interfaz Teclado Interwiki Diario Saltar a Saltar a página Atajo de teclado Atajos de teclado Etiquetas que marcan tareas Grande Última Modificación Dejar enlace a la página nueva Izquierda Panel lateral izquierdo Ordenador de líneas Líneas Mapa de Enlaces Enlace los ficheros del directorio raíz de documentos con su ruta completa Enlazar con Ubicación Registrar los eventos con Zeitgeist Archivo de registro Parece que encontró un error Hacer esta aplicación la aplicación por defecto Gestionar columnas de la tabla Mapear la raíz del documento al URL Coincidir mayúsculas y minúsculas Máximo número de marcadores Ancho de página máximo Barra de menús Mercurial Modificado Mes Mover texto seleccionado… Mover texto a otra página Mover columna hacia adelante Mover columna adelante Mover texto a Nombre Necesitas archivo de salida para exportar MHTML Necesitas carpeta de salida para exportar completo el block de notas Nunca ajustar renglones Archivo nuevo Página nueva Página nueva en %s Nueva s_ubpágina Nuevo _Adjunto Siguiente No se encontraron aplicaciones Sin cambios desde la última versión Sin dependencias No se ha definido un directorio raíz para los documentos de este cuaderno Ninguna extensión disponible para mostrar objetos del tipo: %s No existe el archivo: %s No existe la página: %s El wiki no está definido: %s No hay ninguna plantilla instalada No se seleccionó ningún texto Cuaderno Cuadernos de notas Aceptar Mostrar solo tareas activas Abrir _Carpeta de Adjuntos Abrir carpeta Abrir cuaderno Abrir Con... Abrir Raíz de _Documentos Abrir un directorio de libreta de notas Abrir _página Abrir el enlace del contenido de la celda Abrir ayuda Abrir en una nueva ventana Abrir en una _ventana nueva Abrir una página nueva Abrir la carpeta de plugins Abrir con "%s" Opcional Opciones para la extensión %s Otro... Archivo de salida Existe el archivo de salida, especifique "--overwrite" para obligar a la exportación Directorio de salida La carpeta de salida existe y no está vacía, especifique "--overwrite" para forzar la exportación. Se requiere la ubicación para exportar La salida debe reemplazar la selección actual Sobreescribir B_arra de direcciones Página La página "%s" no tiene una carpeta para archivos adjuntos Índice de Página Nombre de la página Plantilla de página La página ya existe: %s La página es de solo lectura y no puede editarse Página no permitida: %s Sección de página Párrafo Contraseña Pegar Barra de direcciones Por favor, entre un comentario para esta versión Observe que si enlaza a una página inexistente
también crea una página nueva automáticamente. Por favor, seleccione un nombre y carpeta para el cuaderno de notas Por favor, seleccione una fila, antes de pulsar el botón. Por favor seleccione más de una línea de texto Por favor especifique un cuaderno Extensión La extensión "%s" es necesaria para mostrar este objeto Extensiones Puerto Posición en la ventana Pr_eferencias Preferir nombres de enlace breves para páginas Preferir nombres breves para enlaces de páginas Preferencias Previo Imprimir al Navegador Superior P_ropiedades Propiedades Envía los eventos al daemon Zeitgeist Nota rápida Nota rápida... Cambios recientes Cambios Recientes Páginas Cambiadas Recientemente Reformatear marcado de wiki al vuelo Eliminar Eliminar Todo Eliminar columna Quitar enlaces a %s Eliminar fila Eliminando enlaces Cambiar nombre de archivo Cambiar nombre o mover página Cambiar nombre de página «%s» Al hacer clic repetidamente en una casilla de verificación, su valor cambia de forma cíclica. Reemplazar _Todos Reemplazar con Exigir autenticación ¿Restaurar la página a su versión guardada? Rev. Derecha Panel lateral derecho Posición del margen derecho Fila abajo Fila arriba Gu_ardar versión Guardad una c_opia Guardar una copia Guardar versión Guardar marcadores Versión guardada desde zim Puntuación Comando captura de pantalla Buscar Lo que _enlaza aquí... Buscar en esta sección Sección Sección(es) para ignorar Sección(es) a índice Seleccione el archivo Seleccione la carpeta Seleccionar Imagen Seleccione una versión para ver los cambios entre esa versión y el estado actual.
O seleccione multiples versiones para ver los cambios entre ellas.
 Seleccione el formato de exportación Seleccione el archivo o carpeta de salida Seleccione la(s) página(s) a exportar Seleccionar ventana o región Selección Diagrama de secuencia El servidor no ha arrancado Servidor arrancado Servidor detenido Definir nuevo nombre Establecer tamaño de letra de sumario Establecer editor de texto por defecto Establecer a la página actual Mostrar números de línea Mostrar tareas como lista plana Mostrar sumario como elemento flotante en lugar de en el panel lateral Mostrar _cambios Mostrar un ícono separado para cada cuaderno Mostrar barra de edición bajo el editor Mostrar nombre de página completo Mostrar nombre de página completo Mostrar barra de ayuda Mostrar en la barra de herramientas Mostrar margen derecho Mostrar la lista de tareas en el panel lateral Mostrar el cursor en páginas no editables Mostrar título de la página en el sumario Una página Tamaño Pequeño Tecla de inicio inteligente Ocurrió un error al ejecutar «%s» Ordenar alfabéticamente Ordenar páginas por etiquetas Ver fuente Corrector ortográfico Iniciar Arrancar _Servidor Web Permitir miniaturas en SVG Sí_mbolo... Sintaxis Predeterminado del sistema Anchura de tabulador Tabla Editor de tablas Sumario Etiquetas Etiquetas para tareas no accionables Tarea Lista de tareas Tareas Plantilla Plantillas Texto Archivos de texto Texto desde _archivo... Color del texto de fondo color de texto Modo de ajuste de texto El directorio
%s
no existe aún.
¿Quiere crearlo ahora? El directorio "%s" no existe ¿Desea crear uno nuevo ahora? Los siguientes parámetros serán
reemplazados al ejecutar el comando:
<tt>
<b>%f</b> el origen de la página como un archivo temporal
<b>%d</b> el directorio de datos adjuntos de la página actual
<b>%s</b> el origen real del archivo (si existe)
<b>%p</b> el nombre de la página
<b>%n</b> la ubicación del cuaderno de notas (archivo o carpeta)
<b>%D</b> el documento raíz (si existe)
<b>%t</b> el texto seleccionado o la palabra bajo el cursor
<b>%T</b> el texto seleccionado incluyendo el formateado wiki
</tt>
 El complemento de calculadora en línea
no pudo evaluar la expresión en el cursor. ¡La table debe contener al menos una columna!
 No se ha borrado nada. Tema No hay cambios en esta libreta de notas desde la última vez que se guardo Se produjo un problema al cargar este complemento

 Esto puede significar que no tiene el diccionario
apropiado instalado. Este archivo ya existe.
¿Quiere sobrescribirlo? Esta página no tiene una carpeta para archivos adjuntos Este nombre de página no puede ser utilizado debido a una limitación técnica del almacenamiento Este complemento permite tomar una captura de pantalla
e insertarla directamente en una página de Zim.

Este es un complemento incluido en la base de Zim.
 Este plugin añade una "barra de ruta" en la parte superior de la ventana.
Esta "barra de ruta" puede mostrar la ruta del cuaderno para la página actual,
páginas visitadas recientemente o páginas editadas recientemente.
 Este complemento añade un diálogo que muestra todas las
tareas pendientes de este cuaderno, sean cajas sin marcar 
o listas marcadas con las etiquetas "TODO" o "FIXME".

Este es un complemento principal que se suministra con zim.
 Esta extensión adiciona una caja de diálogo para lanzar unas
rápidamente un texto o el contenido del portapapeles dentro de
página.

Esta es una extensión principal suministrada con zim.
 Este complemento añade un icono en la bandeja del sistema
para acceso rápido.

Depende de: gtk versión 2.10 o mayor.

Este es un complemento principal que se suministra con zim
 Este plugin agrega un widget extra que muestra una lista de páginas
enlazadas a la página actual.

Es un plugin propio de zim.
 Este complemento añade un elemento gráfico adicional que
muestra el contenido de la página actual.

Es un complemento básico incluido con zim.
 Este plugin agrega configuraciones que ayudan a usar zim
como un editor libre de distracciones.
 Esta extensión adiciona el diálogo "Insertar símbolo"
y permite el formateado automático de caracteres
tipográficos.

Esta es una extensión principal suministrada con zim.
 Esta extensión añade el panel de índice de página a la ventana principal.
 Esta extensión añade control de versiones para las notas.
Soporta los siguientes sistemas: Bazaar, Git y Mercurial.
Es una extensión integrada e incluida por defecto en Zim.
 Esta extensión permite insertar 'Bloques de Código' en la página. Estos
se mostrarán como widgets incrustados con resaltado de sintaxis, números de línea, etc.
 Este plugin le permite insertar cálculos aritméticos en Zim. Está basado en el módulo aritmético  de http://pp.com.mx/python/arithmetic
 Este complemento le permite evaluar rápidamente
expresiones matemáticas simples en Zim.

Este es un complemento incluido en la base de Zim.
 Esta extensión también tiene propiedades,
consulte el diálogo de propiedades del cuaderno No se puede activar este complemento porque faltan dependencias.
Para más detalles, vea la sección de dependencias, más abajo.

 Este complemento abre un diálogo de búsqueda para ejecutar entradas de los menús de forma rápida. El diálogo se puede abrir al presionar Ctrl+Mayús+P, y este atajo se puede personalizar mediante las preferencias correspondientes de Zim. Esta extensión proporciona un editor de diagramas para Zim basado en Ditaa.
 Esta extensión provee un editor de diagramas basado en GraphViz.

Esta es una extensión principal que se suministra con zim.
 Esta extensión provee un diálogo con una
representación gráfica de la estructura de enlaces
del cuaderno. Puede ser usada como especie de
mapa mental que muestra como las páginas se
relacionan entre sí.

Esta es una extensión principal suministrada con zim.
 Este complemento proporciona una barra de menú de macOS para zim. Este complemento ofrece un índice filtrado al seleccionar etiquetas en una nube.
 Esta extensión provee un editor de gráficos basado en GNU R.
 Este plugin proporciona un editor gráfico de zim  basado en Gnuplot.
 Este plugin provee zim con un editor de diagrama de sequencia basado en seqdiag.
Permite editar diagramas de sequencias fácilmente.
 Esta extensión provee una solución alternativa para la
falta de soporte de impresión en zim. La página actual
se exporta a HTML y se abre en un navegador.
Asumiendo que el navegador tienen soporte para
impresión esto lleva sus datos a la impresora en dos
pasos.

Esta es una extensión principal suministrada con zim.
 Esta extensión provee un editor de ecuaciones basdo en LaTeX.

Esta es una extensión principal suministrada con zim.
 Este plugin provee a zim un editor de puntaje basado en GNU Lilypond.

Este plugin viene con zim.
 Este complemento muestra la carpeta de adjuntos de la
página actual como vista de ícono en el panel inferior.
 Este plugin ordena alfabéticamente las líneas seleccionadas . 
Si la lista ya está ordenada invierte el orden
 (AZ para ZA).
 Este complemento convierte una sección del cuaderno de notas en un diario
con una página por día, semana o mes.
Además agrega un widget de calendario. para acceder a estas páginas.
 Esto generalmente significa que el archivo contiene caracteres no válidos Título Para continuar puede guardar una copia de esta página o
descartar los cambios. Si guarda una copia también se
descartarán los cambios, pero podrá restaurarla más adelante. Para crear un nuevo cuaderno de notas  tiene que seleccionar una carpeta vacía.
Por supuesto, también puede seleccionar la carpeta de un cuaderno de notas existente.
 Sumario _Hoy Hoy Alternar estado de Casilla '>' Alternar marca de cotejo Alternar cruz Panel superior Enviar página a la papelera No se pudo enviar a la papelera; ¿quiere eliminar permanentemente? Icono del área de notificación Convertir el nombre de la página a etiquetas para las tareas Tipo Deseleccionar Casilla Desindentar con <Retroceso>
(Si no se activa, puede usar <Mays><Tab>) Desconocida Tipo de imagen desconocido Objeto desconocido Sin especificar Sin etiqueta Actualizar %i página que enlaza a esta Actualizar %i páginas que enlazan a esta Actualizar el encabezamiento de ésta página Actualizando enlaces Actualizando índice Use %s para cambiar al panel lateral Utilizar tipo de letra personalizado Usar una página por cada Usar la fecha de las páginas de diario Utilizar barra de desplazamiento horizontal (puede necesitar un reinicio) Use la tecla <Intro> para seguir enlaces
(si está deshabilitado aún puede usar <Alt><Intro>) Usar miniaturas Nombre de usuario Control de versiones El control de versiones no se ha habilitado para este cuaderno.
¿Desea habilitarlo? Versiones Ver con _Anotaciones Ver _registro Ver registro de depuración Servidor web Semana Cuando reporte este error por favor incluya
la información en el recuadro de texto debajo Toda la _palabra Ancho Página Wiki: %s Con este plugin, puedes insertar una 'Tabla' en la página wiki. Las tablas serán mostradas como widgets GTK TreeView. 
También pueden ser exportadas en varios formatos (i.e.HTML/LaTeX).
 Contar palabras Conteo de palabras... Palabras Año Ayer Está editando el archivo en una aplicación externa. Puede cerrar este dialogo cuando este listo Puede configurar herramientas personalizadas que aparecerán
en el menú de herramientas y en la barra de herramientas o los menús contextuales. La codificación actual de su sistema es %s, si desea compatibilidad con caracteres especiales
o ve errores debido a la codificación, asegúrese de configurar su sistema para usar "UTF-8" Zim Wiki de escritorio Al_ejar Acerca _de _Añadir _Todos los paneles _Aritmético A_trás _Examinar _Errores _Cancelar _Casilla _Hija _Limpiar formatos _Cerrar _Contraer todo _Contenidos _Copiar _Copiar aquí _Borrar _Borrar Página _Descartar Cambios _Duplicar Línea _Editar _Editar enlace _Editar Enlace u Objeto... _Editar Propiedades _Editar… _Énfasis Preguntas _frecuentes _Archivo _Buscar _Buscar… A_delante _Pantalla completa I_r Ay_uda Resaltado _Historial _Inicio _Imagen… _Importar página... _Insertar _Saltar Sa_ltar a... _Atajos de teclado En_lace En_lazar a la fecha _Enlace _Marcar _Más _Mover _Mover aquí _Mueva Línea hacia abajo _Mueva Línea hacia arriba _Nueva Página Aquí... _Nueva página _Siguiente _Ninguno Tamaño _normal Lista _numerada _Aceptar _Abrir _Abrir otro cuaderno de notas _Otro… _Página _Jerarquía de Página _Padre _Pegar _Vista previa _Previo _Imprimir Imprimir al na_vegador Nota _rápida... _Salir Páginas _recientes _Rehacer Expresión _regular _Recargar _Eliminar _Remover Línea _Eliminar enlace _Reemplazar _Reemplazar… _Restaurar Tamaño _Restaurar versión _Guardar Guardar una _copia _Captura de pantalla… _Buscar _Buscar... _Enviar a… _paneles laterales Lado a lado _Ordenar líneas _Tachar Negrita_s Su_bíndice Su_períndice _Plantillas _Herramientas _Deshacer _Literal _Versiones... _Ver _Ampliar como fecha de fin de las tareas como fecha de inicio de tareas calendar:week_start:1 no utilizar líneas horizontales barra de menú de macOS sin lineas de grilla de solo lectura segundos Launchpad Contributions:
  Adolfo Jayme https://launchpad.net/~fitojb
  Alberto Guerra Linares https://launchpad.net/~bebetoguerra
  Antonio Maldonado https://launchpad.net/~a.m.
  Carlos Albornoz https://launchpad.net/~caralbornozc
  Charles Edward Bedón Cortázar https://launchpad.net/~charles-bedon
  David https://launchpad.net/~davpaez
  DiegoJ https://launchpad.net/~diegojromerolopez
  Eduardo Alberto Calvo https://launchpad.net/~edu5800
  Federico Vera https://launchpad.net/~fedevera
  Gonzalo Testa https://launchpad.net/~gonzalogtesta
  Hector A. Mantellini https://launchpad.net/~xombra
  Jaap Karssenberg https://launchpad.net/~jaap.karssenberg
  Jaime Ernesto Mora https://launchpad.net/~jemora70
  Janzun https://launchpad.net/~janzun-w
  Javier Rovegno Campos https://launchpad.net/~jrovegno
  Jonay https://launchpad.net/~jonay-santana
  Jorge Pérez https://launchpad.net/~ktl-xv
  Juan Ignacio Pucheu https://launchpad.net/~jpucheu
  Lex https://launchpad.net/~lex-exe
  Lisandro https://launchpad.net/~correo-lisandrodemarchi
  Mariano Esteban https://launchpad.net/~mesteban
  Miguel Cervantas https://launchpad.net/~azulado-deactivatedaccount
  Monkey https://launchpad.net/~monkey-libre
  Pablo Angulo https://launchpad.net/~pablo-angulo
  Paco Molinero https://launchpad.net/~franciscomol
  Reynaldo Cordero https://launchpad.net/~reynaldo-cordero
  Roberto Michán Sánchez https://launchpad.net/~roboron
  Servilio Afre Puentes https://launchpad.net/~servilio
  Ubuntu https://launchpad.net/~invrom-deactivatedaccount1-deactivatedaccount-deactivatedaccount
  korilu https://launchpad.net/~korilu
  runspect https://launchpad.net/~runspect
  sanzoperez https://launchpad.net/~sanzoperez
  victor tejada yau https://launchpad.net/~victormtyau líneas verticales con lineas {count} de {total} 