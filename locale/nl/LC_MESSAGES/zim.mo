��    �     �              �-  ,   �-  .   
.  ?   9.     y.     �.     �.     �.     �.  0   �.  0   #/     T/     o/  5   �/     �/  	   �/     �/  %   �/  i   0  *   r0     �0     �0     �0     �0     �0  
   �0  -   �0     "1  V   *1     �1  	   �1  	   �1     �1  3   �1     �1     �1  Y   2     u2     �2  .   �2  
   �2     �2     �2     �2     3     3     $3     13  	   83     B3  -   Q3  *   3  $   �3  ?   �3  /   4  (   ?4     h4  %   �4  ,   �4     �4  	   �4     �4     5  
   5     5  	   !5     +5     85     E5     L5     X5     _5  
   l5     w5     �5  �   �5  �   6     �6     �6     �6     �6  
   �6     �6     7     7     17     D7     W7     g7     ~7  .   �7  <   �7     �7  	   �7  
   	8     8     8     %8     58     R8     Z8     p8     �8     �8     �8  +   �8  3   �8      !9     B9     G9  	   Z9     d9     r9  
   ~9     �9     �9     �9     �9     �9     :  3   #:     W:     u:     �:     �:     �:     �:     �:     �:     ;     
;     ;     %;     2;     7;     H;     L;  0   T;     �;     �;     �;     �;  
   �;     �;     �;     �;     �;     �;     �;     <     <  )   1<  $   [<  ~   �<     �<     =  
   =     =     $=  
   5=  	   @=  
   J=     U=     b=     j=     {=     �=     �=     �=  5   �=     �=     �=     >  !   
>     ,>  #   =>     a>     t>     {>     �>     �>     �>     �>     �>  #   �>     ?     -?  .   L?     {?     �?     �?     �?     �?     �?  
   �?     �?     �?  	   @  6   @     F@  v   M@     �@  *   �@  c   A     eA     mA     tA  
   {A     �A     �A     �A     �A     �A  
   �A     �A  
   �A  
   �A  
   �A  
   B  
   B  
   B     )B     0B     KB     kB  	   �B     �B     �B     �B     �B     �B     �B  %   �B     �B     �B  #   	C     -C     >C  
   DC     OC     aC     sC     �C     �C     �C     �C     �C     �C     �C     �C     �C     D     %D     3D     @D  	   VD     `D     rD     zD     �D     �D     �D  �   �D     uE     �E     �E     �E     �E     �E     �E     �E     �E     �E     �E  2   �E     -F     5F     >F     XF     aF     |F     �F     �F     �F     �F     �F      G  	   G     G     G     !G     7G     OG     aG     vG     �G     �G      �G  *   �G     �G     �G     H     H     H     0H     @H     EH     [H     yH  *   �H  2   �H     �H     �H     	I     "I     9I     JI  	   SI     ]I     `I     wI     �I     �I     �I     �I     �I  
   �I     �I  	   J     J      J     4J     BJ     VJ     eJ     nJ     �J     �J  9   �J     �J  I   �J  !   +K  '   MK  	   uK     K     �K  w   �K  �   L  0   �L  
   �L  	   �L     �L     �L  &   �L      M     5M     LM  	   YM     cM     lM     rM     �M  '   �M  V   �M  3   N  0   AN  (   rN     �N     �N  .   �N     �N     �N     �N     O  !   O  !   >O     `O     lO     qO     �O     �O  
   �O  &   �O  
   �O     �O     �O     �O     P     P     :P  
   AP     LP     ZP  
   mP     xP     �P     �P     �P  ?   �P     �P     Q     Q     )Q     HQ     LQ     RQ     bQ     xQ     �Q     �Q     �Q  	   �Q     �Q     �Q     �Q     �Q     �Q     �Q     R     R     /R     7R     LR     `R     lR     zR  �   �R     S      0S     QS     lS  	   �S     �S     �S     �S     �S     �S     �S     �S     T     T     .T     KT     ]T  2   uT     �T  &   �T     �T  $   �T     U     0U     EU     YU     mU      �U     �U  !   �U     �U  5   �U  &   &V     MV     ZV     _V     eV  &   tV     �V     �V     �V     �V     �V     �V  
   �V     �V     W  
   'W     2W     9W  	   HW     RW     XW     eW     wW     |W     �W  	   �W     �W     �W  	   �W     �W  
   �W     �W     �W     �W     X  F    X  ?   gX  A   �X  �  �X  S   �Z  =   [     ?[  K   E[  )   �[  @   �[  6   �[  -   3\  F   a\  I   �\  x   �\  �   k]  �   ^  �   �^  �   `_  �   �_  }   o`  L   �`  �   :a  9   �a  �   �a  �   �b  �   3c  }   �c  C   @d  s   �d  �   �d  h   �e  k   7f  �   �f  -   wg  R   �g  ;   �g  =   4h  v   rh    �h  j   �i  n   hj  ]   �j     5k  �   �k  7   Jl     �l  �   �l  |   $m     �m     �m     �m     �m     �m     �m     �m     n     n     #n     ,n     9n     Gn     Kn  
   Tn  9   _n  	   �n  (   �n  '   �n     �n     �n  D   o     Po     Xo     jo     xo     �o  H   �o     �o     �o     p  !   p     6p     Hp     \p     xp  +   �p  P   �p     q     "q     +q  U   ;q     �q     �q  	   �q     �q  
   �q     �q  N   �q     "r     .r     4r  �   Br  
   �r     s     s     s  	   !s  ^   +s  f   �s  �   �s     �t  	   �t     �t     �t  
   �t     �t     �t     �t     �t     �t     �t  	   �t     u     u      u     'u  	   5u     ?u  
   Eu     Pu     Xu  
   eu     pu     �u     �u  
   �u     �u     �u     �u  	   �u     �u     �u     �u     �u     �u     v     v     v  
   v     "v     +v  	   1v     ;v     Kv     Sv     Yv     ev  /   rv     �v     �v     �v     �v     �v     �v  
   �v     �v     �v     �v     w     w     w     .w     4w     Aw     Pw     Tw     Zw  	   tw     ~w     �w     �w     �w     �w  	   �w     �w     �w     �w     �w     �w     �w     x     x     x     +x     3x     ;x     Kx     Xx     ex     rx  
   �x     �x     �x     �x     �x     �x  
   �x     �x     �x  
   �x     �x     y     y      y     ,y     4y  
   <y     Gy  
   Ty     _y     fy  	   ly     vy     �y     �y     �y     �y     �y  
   �y     �y     �y      z     z     z     z     2z  
   Az     Lz  �  _z  3   E|  3   y|  V   �|     }     }     )}     G}  "   g}  F   �}  h   �}     :~  (   U~  F   ~~     �~  
   �~     �~  $   �~  y     +   �     �     �     �     �  #   �     �  6   +�     b�  k   o�  	   ۀ     �  
   �     ��  E   �  #   _�  !   ��  `   ��     �  
   �     *�  
   I�     T�     h�     z�     ��     ��  	   ��     ��     ��     Ղ  >   �  1   +�  %   ]�  E   ��  6   Ƀ  /    �  !   0�  *   R�  E   }�     Ä     ބ     �     ��     �     �     �     %�     5�     B�     K�     [�     d�     r�     ��  	   ��  �   ��  �   6�     �  	   "�     ,�  	   @�     J�     Q�     j�     }�     ��     ��     ��     ˇ     �  =   ��  L   8�     ��     ��     ��     ��     ��     ��  %   ��     �     �     �     �     *�     C�  1   ^�  >   ��  "   ω  	   �     ��     �     "�     5�     G�     W�  !   j�     ��     ��      Ǌ     �  1   �      9�     Z�  +   v�     ��  %   ��  ,   �     �     �     &�     /�     A�     T�     a�     g�     x�  	   |�  !   ��     ��  	   ��     Ȍ     ی     �  
   �     �      �     -�     5�     =�     P�     b�  7   x�  $   ��  �   Ս     l�     z�     ��     ��     ��     ��     ̎     ڎ     �     ��     �     �     /�  
   B�     M�  4   U�     ��     ��  
   ��  -   ��     �  ,   ��      (�     I�     W�  &   m�     ��     ��     ��     ڐ  .   �     �      ;�  )   \�     ��  &   ��  ,   Ƒ     �      �     �     �     �     ,�     <�  J   E�     ��  s   ��     �     �  {   8�     ��     ��     Ó  
   ʓ  "   Փ  $   ��     �     !�     )�  
   2�     =�     R�     ^�     e�     l�     s�     z�     ��  #   ��  +   ��     ؔ     �     ��  	   �     �     %�      :�     [�  *   ^�     ��     ��     ��     Ǖ     ݕ     �     �     ��     �     &�     7�     N�     d�     x�     ��     ��     ��     Ɩ     ܖ     ��     �     �  	   2�     <�     N�     W�     _�     n�     ��  �   ��     ��     ��     ��       -   ٘     �     �     �     1�     8�     =�  I   L�  	   ��     ��     ��     ��  '   ș     �     	�  "   !�     D�     X�     q�     ��  	   ��  	   ��     ��  "   ��  $   Ϛ     ��     �  &   +�     R�     c�  ,   h�  5   ��     ˛     �     �     ��     �     '�     7�     @�  '   Z�     ��  7   ��  0   М     �     �     3�     L�     j�     ��     ��     ��     ��     ��  
   ԝ     ߝ     �      �     �     *�     7�     I�     U�     m�     ��     ��     ��  	   Þ     ͞  	   �     ��  B   �     I�  N   M�  &   ��  /   ß     �     ��     �  �   �  �   ��  +   J�     v�     ��     ��     ��  %   ��     ١     ��     �     �  
   %�     0�     8�     R�  ,   _�  o   ��  .   ��     +�      >�     _�     w�  +   ��     ��     ǣ     ͣ     �     �     �  
   -�     8�     ?�  
   W�     b�     q�  '   �     ��     ��     Ǥ     ۤ     �     �     -�     9�     K�     ]�     w�     ��     ��     ��     ѥ  G   �     /�     ?�     L�  )   d�     ��     ��     ��     ��  
   Ħ  
   Ϧ     ڦ     ��     ��     �     �     0�     M�     S�     i�     p�     ��     ��     ��     ��     ϧ     �     �  �   �     ��     ��     Ϩ  (   �     �     �     (�     D�     W�     j�     z�     ��     ��     ��      ֩     ��  '   �  -   6�     d�  5   �     ��  /   Ѫ     �     !�     A�      a�     ��  '   ��     ë  /   ٫  "   	�  D   ,�  $   q�     ��     ��     ��     ��  &   Ŭ     �      �     �     '�     9�     ?�  
   Q�     \�  %   i�     ��     ��     ��     ��     ��     ŭ     ԭ     �  #   �     �  
   �     �     #�  	   ,�     6�     <�     L�     b�     z�  "   ��  I   ��  1   ��  5   /�  �  e�  I   -�  ?   w�     ��  P   ��  3   �  F   B�  1   ��  '   ��  ^   �  X   B�  �   ��  �   5�  �   ��  �   ȵ  �   L�  �   �  {   ��  C   !�  y   e�  Q   ߸  �   1�  �   ��  �   J�  �   ֺ  [   _�  x   ��  �   4�  d   �  �   ��  �   �  .   ��  s   *�  Q   ��  P   �  ~   A�  �   ��  �   ��  �   j�  S   �  �   d�  �   �  C   ��     �  �   �  s   ��     [�     b�     k�     s�     ��     ��     ��     ��     ��     ��     ��     �     �     �     $�  L   @�     ��  '   ��  +   ��     ��     ��  w   �     z�     ��     ��  	   ��     ��  n   ��     7�     W�     q�  (   ��     ��     ��  "   ��      �  3   $�  �   X�     ��     ��     �  W   �     f�     n�     ��     ��  	   ��     ��  G   ��     ��     �     �  *   "�     M�     \�     n�     v�     {�  �   ��  f   �  �   r�     @�  
   Q�     \�  	   b�     l�     z�     ��     ��  	   ��     ��  	   ��  	   ��     ��     ��     ��     ��     ��  
   ��     �     �     '�     ;�     K�     b�  	   t�     ~�     ��     ��     ��     ��     ��     ��     ��  
   ��     ��     ��     �     �  	   �     '�     5�     <�     K�  	   a�     k�  	   o�     y�  9   ��     ��     ��     ��  	   ��     ��     ��     ��     
�     !�     8�     O�  	   a�     k�     ��     ��     ��     ��     ��     ��     ��     ��     ��     ��      �     	�     �     �     ,�     5�     N�     ]�  
   o�     z�     ��     ��  	   ��  
   ��     ��     ��     ��     ��  #   �     >�     L�     U�     c�     w�     ��     ��     ��     ��  
   ��     ��     ��     ��     ��  
   ��     	�  
   �     �  
   &�     1�     8�     H�     T�     `�  	   g�     q�     ��     ��     ��     ��     ��     ��     ��     �  5  �     K�     \�     m�   		This plugin provides bar for bookmarks.
		 %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i Attachment %i Attachments %i BackLink %i BackLinks %i _Backlink %i _Backlinks %i errors occurred, see log %i file will be deleted %i files will be deleted %i file will be trashed %i files will be trashed %i open item %i open items %i warnings occurred, see log (Un-)indenting a list item also changes any sub-items <Top> <Unknown> A desktop wiki A file with that name already exists. A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Action Add Application Add Bookmark Add Notebook Add bookmark/Show settings Add column Add new bookmarks to the beginning of the bar Add row Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files All Tasks Allow public access Always use last cursor position when opening a page Always wrap at character Always wrap at word boundaries An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Are you sure you want to delete the file '%s'? Arithmetic Ascii graph (Ditaa) Attach File Attach image first Attachment Browser Attachments Attachments: Author Auto
Wrap Auto indenting Automatically collapse sections on close page Automatically expand sections on open page Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave interval in minutes Autosave version on regular intervals Autosave version when the notebook is closed Back to Original Name BackLinks BackLinks Pane Backend Backlinks: Bazaar Bookmarks BookmarksBar Border width Bottom Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Cannot write this file. Probably this is due to the lenght
of the file name, please try using a name with less
than 255 characters Cannot write this file. Probably this is due to the lenght
of the file path, please try using a folder structure resulting in less
than 4096 characters Capture whole screen Center Change columns Changes Characters Characters excluding spaces Check Checkbox '<' Check Checkbox '>' Check Checkbox 'V' Check Checkbox 'X' Check _spelling Check and Update Index Checkbo_x List Checking a checkbox also changes any sub-items Classic trayicon,
do not use new style status icon on Ubuntu Clear Clone row Code Block Column 1 Command Command Palette Command does not modify data Comment Common include footer Common include header Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Copy Email Address Copy Line Copy Template Copy _As... Copy _Link Copy _Location Copy _link to this location Copy link to clipboard Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Created Cu_t Custom Tools Custom _Tools Customize... Date Date and Time... Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Demote Dependencies Description Details Diagram Discard note? Display line numbers Distraction Free Editing Do not use system trash for this notebook Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root Due E_xport... Edit %s Edit Custom Tool Edit Image Edit Link Edit Table Edit _Source Editing Editing file: %s Enable Version Control? Enable plugin Enabled Equation Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File exists, do you want to import? File is not writable: %s File name should not be blank. File name should not contain path declaration. File name too long: %s File path too long: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Fossil GNU R Plot Get more plugins online Get more templates online Git Gnuplot Go back Go forward Go to home page Grid lines Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide Journal pane if empty Hide menubar in fullscreen mode Highlight current line Home Page Horizontal _Line Icon Icons Icons & Text Icons & Text horizontal Id Id "%s" not found on the current page Images Import Page Include horizontal lines in the ToC Include subpages Index Index page Inline Calculator Insert Code Block Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Sequence Diagram Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Key Binding Key bindings Key bindings can be changed by clicking on a field with a key combination
in the list and then press the new key binding.
To disable a keybinding, select it in the list and use <tt>&lt;Backspace&gt;</tt>. Labels marking tasks Large Last Modified Leave Fullscreen Leave link to new page Left Left Side Pane Line Sorter Lines Link Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Managing table columns Map document root to URL Match _case Maximum number of bookmarks Maximum page width Menubar Mercurial Modified Month Move Selected Text... Move Text to Other Page Move column ahead Move column backward Move page "%s" to trash? Move text to Name Need output file to export MHTML Need output folder to export full notebook Never wrap lines New File New Page New Page in %s New S_ub Page... New _Attachment Next No Applications Found No changes since last version No dependencies No document root defined for this notebook No plugin available to display objects of type: %s No such file: %s No such page: %s No such wiki defined: %s No templates installed No text selected Notebook Notebooks OK Only Show Active Tasks Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open cell content link Open help Open in New Window Open in New _Window Open new page Open plugins folder Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" and all of it's sub-pages and
attachments will be deleted.

This deletion is permanent and cannot be un-done. Page "%s" and all of it's sub-pages and
attachments will be moved to your system's trash.

To undo later, go to your system's trashcan. Page "%s" does not have a folder for attachments Page Index Page Name Page Template Page already exists: %s Page is read-only and cannot be edited Page not allowed: %s Page not available: %s Page section Paragraph Password Paste Paste As _Verbatim Path Bar Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please select a row, before you push the button. Please select more than one line of text Please specify a notebook Plugin Plugin "%s" is required to display this object Plugins Port Position in the window Pr_eferences Prefer short link names for pages Prefer short names for page links Preferences Prev Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Remove column Remove links to %s Remove row Removing Links Rename file Rename or Move Page Rename page "%s" Repeated clicking a checkbox cycles through the checkbox states Replace _All Replace with Require authentication Restore page to saved version? Rev Right Right Side Pane Right margin position Row down Row up S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Screenshot Command Search Search _Backlinks... Search this section Section Section(s) to ignore Section(s) to index Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Sequence Diagram Server not started Server started Server stopped Set New Name Set ToC fontsize Set default browser Set default text editor Set to Current Page Show BackLink count in title Show Line Numbers Show Tasks as Flat List Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show due date in sidepane Show edit bar along bottom of editor Show full Page Name Show full Page Names Show full page name Show helper toolbar Show in the toolbar Show linkmap button in headerbar Show right margin Show tasklist button in headerbar Show tasklist in sidepane Show the cursor also for pages that can not be edited Show the page title heading in the ToC Single _page Size Small Smart Home key Some error occurred while running "%s" Sort alphabetically Sort pages by tags Source View Spell Checker Start Start _Web Server Su_bscript Su_perscript Support thumbnails for SVG Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Tags for non-actionable tasks Task Task List Tasks Template Templates Text Text Files Text From _File... Text background color Text foreground color Text wrap mode The file "%s" exists but is not a wiki page.
Do you want to import it? The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The following parameters will be substituted
in the command when it is executed:
<tt>
<b>%f</b> the page source as a temporary file
<b>%d</b> the attachment directory of the current page
<b>%s</b> the real page source file (if any)
<b>%p</b> the page name
<b>%n</b> the notebook location (file or folder)
<b>%D</b> the document root (if any)
<b>%t</b> the selected text or word under cursor
<b>%T</b> the selected text including wiki formatting
</tt>
 The inline calculator plugin was not able
to evaluate the expression at the cursor. The table must consist of at least on row!
 No deletion done. Theme There are no changes in this notebook since the last version that was saved There was a problem loading this plugin

 This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This page name cannot be used due to a conflicting file in the storage This page name cannot be used due to technical limitations of the storage This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a "path bar" to the top of the window.
This "path bar" can show the notebook path for the current page,
recent visited pages or recent edited pages.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds the page index pane to the main window.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows inserting 'Code Blocks' in the page. These will be
shown as embedded widgets with syntax highlighting, line numbers etc.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin also has properties,
see the notebook properties dialog This plugin cannot be enabled due to missing dependencies.
Please see the dependencies section below for details.

 This plugin opens a search dialog to allow quickly executing menu entries. The search dialog can be opened by pressing the keyboard shortcut Ctrl+Shift+P which can be customized via Zim's key bindings preferences. This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a macOS menubar for zim. This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a sequence diagram editor for zim based on seqdiag.
It allows easy editing of sequence diagrams.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This plugin turns one section of the notebook into a journal
with a page per day, week or month.
Also adds a calendar widget to access these pages.
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox '<' Toggle Checkbox '>' Toggle Checkbox 'V' Toggle Checkbox 'X' Toggle _Editable Toggle editable Tool Bar Toolbar size Toolbar style Top Top Pane Trash Page Trash failed, do you want to permanently delete instead ? Tray Icon Try wrap at word boundaries or character Turn page name into tags for task items Type Un-check Checkbox Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unkown Image type Unkown Object Unspecified Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use a page for each Use date from journal pages Use font color for dark theme Use horizontal scrollbar (may need restart) Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Use thumbnails Username Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log View debug log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s With this plugin you can embed a 'Table' into the wiki page. Tables will be shown as GTK TreeView widgets.
Exporting them to various formats (i.e. HTML/LaTeX) completes the feature set.
 Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Your system encoding is set to %s, if you want support for special characters
or see errors due to encoding, please ensure to configure your system to use "UTF-8" Zim Desktop Wiki Zoom _Out _About _Add _All Panes _Arithmetic _Attachment... _Back _Browse _Bugs _Cancel _Checkbox _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Delete... _Discard Changes _Duplicate Line _Edit _Edit Link _Edit Link or Object... _Edit Properties _Edit... _Emphasis _FAQ _File _Find _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump _Jump To... _Keybindings _Limit search to the current page and sub-pages _Link _Link to date _Link... _Mark _More _Move _Move Here _Move Line Down _Move Line Up _New Page Here... _New Page... _Next _Next in Index _None _Normal Size _Numbered List _OK _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Previous in Index _Print _Print to Browser _Properties _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove _Remove Heading _Remove Line _Remove Link _Remove List _Rename or Move Page... _Rename... _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In as due date for tasks as start date for tasks calendar:week_start:0 do not use horizontal lines macOS Menubar no grid lines readonly seconds translator-credits vertical lines with lines {count} of {total} Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2021-09-30 18:57+0000
Last-Translator: Jaap Karssenberg <jaap.karssenberg@gmail.com>
Language-Team: Dutch <https://hosted.weblate.org/projects/zim/master/nl/>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.9-dev
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
 		Deze plugin voegt een balk met bladwijzers toe
		 %(cmd)s
gaf een exit code die niet nul is: %(code)i Er zijn %(n_error)i fouten en %(n_warning)i waarschuwingen opgetreden, zie het logboek %A, %d %B %Y %i bestand %i bestanden %i Verwijzing %i Verwijzingen %i _Verwijzing %i _Verwijzingen %i fouten zijn opgetreden, zie log %i bestand zal verwijderd worden %i bestanden zullen worden verwijderd %i bestand zal naar de afvalbak verplaatst worden %i bestanden zullen naar de afvalbak verplaatst worden %i open taak %i open taken %i waarschuwing zijn opgetreden, zie log Het laten inspringen van een bullet lijst item neemt ook sub-items mee <Top> <Onbekend> Een desktop wiki Een bestand met die naam bestaan al. Een bestand met de naam <b>"%s"</b> bestaat reeds.
U kunt een andere naam opgeven of het bestaande bestand
overschrijven. Een tabel moet minstens één kolom hebben. Actie Programma toevoegen Bladwijzer toevoegen Notitieboek Toevoegen Bookmark toevoegen/Instelling tonen Kolom toevoegen Nieuwe bladwijzers aan het begin van de balk toevoegen Voeg rij toe Deze plugin voegt een spellingscontrole toe 
gebaseerd op gtkspell.

Dit is een standaard plugin voor zim.
 Uitlijnen Alle bestanden Alle taken Publieke toegang toelaten Altijd laatste cursor positie gebruiken bij het openen van een pagina Regels altijd afbreken op karakters Regels altijd afbreken of woorden Er is een fout opgetreden bij het genereren van de 
afbeelding. Wilt u de brontext toch opslaan? Geannoteerde pagina bron Programmas Wilt u bestand %s verwijderen? Arithmetic Text figuur (Ditaa) Bijlage toevoegen Bestand eerst aanhechten Bijlagen-browser Bijlagen Bijlagen: Auteur Automatische
Terugloop Automatisch inspringen Automatisch secties invouwen wanneer een pagina gesloten wordt Automatisch secties uitvouwen voor de open pagina Automatisch opgeslagen versie van zim Selecteer automatisch het huidige woord voor het toepassen van opmaak Link woorden in CamelCase automatisch tijden het typen Link bestandsnamen automatisch tijden het typen Interval voor automatisch opslaan Autosave versie op regelmatige tijdstippen Automatisch een versie opslaan wanneer het notitieboek gesloten wordt Orginele naam terug zetten Referenties Referenties paneel Systeem Referenties: Bazaar Bladwijzers Bladwijzer balk Kaderbreedte Onderaan Onderste paneel Bladeren Bulle_t Lijst C_onfigureren Kan pagina "%s" niet wijzigen Annuleren Dit bestand kan niet geschreven worden. Dit komt mogelijk 
door de lengte van de bestandsnaam. Probeer een naam met 
minder dan 255 karakters Dit bestand kan niet geschreven worden. Waarschijnlijk komt dit
door de lengt van de bestandsnaam en folder namen. Probeer een folder
structuur te gebruiken met een totale bestandsnaam van minder
dan 4096 karaters Hele scherm afdrukken Centreren Kolommen veranderen Wijzingen Tekens Tekens exclusief spaties Check Checkbox '<' Check Checkbox '>' Check Checkbox 'V' Check Checkbox 'X' Controleer _spelling Index controleren en bijwerken Checkbo_x Lijst Bij het afvinken van een checkbox ook alle sub-items afvinken Klassiek status icoon,
gebruik de nieuwe style status icoon voor Ubuntu niet Wissen Rij copieren Codeblok Kolom 1 Commando Functie palet Dit commando verandert geen bestanden Beschrijving Standaard footer Standaard header Gehele _notitieboek Programma's configureren Invoegtoepassing instellen Configureer een programma om "%s" links to openen Configureer een programma om bestanden
van type "%s" te openen Beschouw alle checkboxen als taken Kopiëren Email Adres Kopiëren Regel kopiëren Sjabloon Kopiëren Kopiëren _Als... _Link Kopiëren _Locatie kopiëren Link naar deze _locatie kopiëren Kopieer link naar clipboard Kan programma "%s" niet vinden Kan notitieboek "%s" niet vinden Kan sjabloon niet vinden: "%s" Map of bestand voor dit notitieboek niet gevonden Kan spellingscontrole niet laden Kan bestand niet openen: %s Kan de vergelijking niet goed interpreteren Kan bestand niet lezen: %s Pagina kon niet worden opgeslagen: %s Maak voor elke notitie een nieuwe pagina aan Map creëren? Gemaakt _Knippen Extra Applicaties E_xtra Applicaties Aanpassen... Datum Datum en Tijd... Dag Standaard Standaard formaat om te kopiëren Standaard Notitieboek Wachttijd Pagina Verwijderen Pagina "%s" verwijderen? Rij verwijderen Degraderen Afhankelijkheden Omschrijving Details Diagram Notitie weggooien? Toon regelnummers Afleiding vrij werken Gebruik de standaard afvalbak niet voor dit notitieboek Wilt u alle bladwijzers verwijderen? Wil je pagina: %(page)s terugzetten
naar de opgeslagen versie: %(version)s?

Alle wijzigingen sinds de laatst opgeslagen versie zullen verloren gaan ! Bestanden map Deadline E_xporteren… %s bewerken Extra applicatie wijzigen Afbeelding Bewerken Link Bewerken Tabel bewerken Brontext Bewerken Bewerken Bestand open: %s Versiebeheer inschakelen? Plugin inschakelen In Gebruik Formule Fout in %(file)s op regel %(line)i bij "%(snippet)s" Evalueer _Vergelijking _Alles uitvouwen Exporteren Exporteer alle paginas naar hetzelfde bestand Export compleet Exporteer elke pagina naar een apart bestand Bezig met notitieboek exporteren Niet gevonden Programma gefaald: %s Fout opgetreden bij de excutie van: %s Bestand Bestaat Reeds Bestands _Sjablonen... Bestand is veranderd: %s Bestand bestaat al Dit bestand bestaat al, wilt u het importeren? Kan bestand niet schrijven: %s Bestandsnaam mag niet leeg zijn. Bestandsnaam mag geen separator bevatten. Bestandsnaam te lang: %s Bestandsnaam en foldernaam te lang: %s Dit bestands type wordt niet ondersteund: %s Bestandsnaam Filter Zoeken _Volgende zoeken Zoek vo_rige Zoek en vervang Zoek dit Markeer taken met een eind datum op Maandag of Dinsdag al voor het weekend Map Map bestaat al en bevat bestanden. Exporteren naar deze map kan bestaande bestanden overschrijven. Wilt u doorgaan? Map bestaat: %s Map met sjablonen voor bijlagen Voor geavanceerde zoek opdrachten kunnen termen
zoals AND, OR en NOT worden gebruikt. Zie de hulp
pagina voor meer details. _Opmaak Opmaak Fossil GNU R Plot Download meer plugins van internet Download meer templates van internet Git Gnuplot Ga terug Ga vooruit Ga naar start pagina Grid lijnen Kop _1 Kop _2 Kop _3 Kop _4 Kop _5 Hoogte Journaal verbergen als deze leeg is Verberg de menubalk in schermvullende modus Huidige regel oplichten Start Pagina Horizontale balk Pictogram Pictogrammen Pictogrammen & tekst Pictogrammen & tekst horizontaal Id Id "%s" niet gevonden in de huidige pagina Afbeeldingen Pagina Importeren Toon horizontale lijnen in ToC Inclusief sub-paginas Index Index pagina Rekenmachine Codeblok invoegen Datum en Tijd Invoegen Diagram Invoegen Ditaa diagram invoegen Vergelijking Invoegen GNU R plot invoegen Gnuplot diagram invoegen Afbeelding Invoegen Link Invoegen Notenbalk Invoegen Schermafdruk Invoegen Volgorder diagram invoegen Symbool invoegen Tabel invoegen Tekst Uit Bestand Invoegen Bediening Interwiki Keyword Journaal Ga naar Ga naar pagina Toetsen combinatie Toetsen combinaties Toetsen combinaties kunnen worden veranderd door op een veld met een toetsencombinatie te klikken
en een nieuwe toetsen combinatie in te drukken. 
Om een toetsen combinatie te verwijderen, selecteer de combinatie en gebruik <tt>&lt;Backspace&gt;</tt>. Labels die taken markeren Groot Laatst gewijzigd Schermvullend verlaten Laat een referentie achter naar nieuwe pagina Links Linker zijpaneel Sorteren per regel Regels Link Link Overzicht Bestanden in de document root folder met de volledige bestandsnaam linken Link Naar Lokatie Log events in Zeitgeist Logboek U heeft waarschijnlijk een bug gevonden Maak standaard programma Tabel kolommen wijzigen Map de documentenroot naar een URL Match _hoofdletters Maximum aantal bookmarks Maximale pagina breedte Menubalk Mercurial Gewijzigd Maand Geselecteerde tekst verplaatsen... Tekst naar andere pagina verplaatsen Kolom naar voren halen Kolom naar achteren verplaatsen Pagina "%s" naar afvalbak verplaatsen? Verplaatsen naar Naam Uitvoer bestand nodig om MHTML te exporteren Uitvoer map nodig om gehele notitieboek te exporteren Regels nooit afbreken Nieuw bestand Nieuwe Pagina Nieuwe Pagina in %s Nieuwe S_ub Pagina... Nieuwe _Bijlage Volgende Geen programma's gevonden Geen veranderingen sinds laatste versie Geen afhankelijkheden Geen bestanden folder gedefinieerd voor dit notitieboek Er is geen plugin beschikbaar voor "%s" objecten Bestand niet gevonden: %s Pagina bestaat niet: %s De wiki bestaat niet: %s Geen Sjablonen geïnstalleerd Geen tekst geselecteerd Notitieboek Notitieboeken OK Laat alleen actieve taken zien Open Map Met Bijlagen Map openen Notitieboek Openen Openen met... Open Documentenroot Open _Notitieboek Map Open _Pagina Open link in cell Hulp openen Openen in nieuw venster Openen in _Nieuw Venster Nieuwe pagina openen Open folder met plugins Openen met "%s" Optioneel Opties voor invoegtoepassing %s Andere... Uitvoerbestand Uitvoer bestand bestaat, gebruik "--overwrite" om te overschrijven Map Uitvoer map bestaat en is niet leeg, gebruik "--overwrite" om te overschrijven Uitvoer lokatie nodig om te exporteren Vervang geselecteerde text met programma output Overschrijf _Lokatiebalk Pagina De pagina "%s" en alle sub-paginas en aangehechte 
bestanden zullen worden weggegooid.

Dit is permanente en kan niet worden terug gedraaid. De pagina "%s" wordt met alle sub-paginas en aangehechte 
bestanden naar de afvalbak verplaatst.

Om dit later terug te draaien moet u naar de afvalbak op
uw bureaublad. Pagina "%s" heeft geen map voor de bijlagen Pagina index Pagina naam Pagina Opmaak Pagina bestaat al: %s Deze pagina kan niet worden gewijzigd Pagina niet toegestaan: %s Pagina niet beschikbaar: %s Pagina sectie Alinea Wachtwoord Plakken Tekst _letterlijk plakken Locatie balk Geef een korte omschrijving voor deze versie Tip: Als u een link maakt naar een niet bestaande pagina
wordt er ook automatisch een nieuwe pagina aangemaakt. Kies een naam en een map voor dit notitieboek. Selecteer een rij. Selecteer meer dan 1 regel tekst Geef een notitieboek op Invoegtoepassing Plugin "%s" is nodig om dit object te tonen Invoegtoepassingen Poort Positie in het scherm _Voorkeuren Gebruik korte namen voor links Gebruik korte namen voor links Voorkeuren Vorige Printen naar webbrowser Promoveren Ei_genschappen Eigenschappen Stuur events naar de Zeitgeist service. Korte Notitie _Korte Notitie... Recente wijzigingen Recente wijzigingen... Recente gewijzigde paginas Interpreteer wiki syntax direct Verwijderen Alles verwijderen Kolom verwijderen Links verwijderen naar %s Rij verwijderen Links worden verwijderd Bestand hernoemen Pagina hernoemen of verplaatsen Pagina "%s" hernoemen Herhaald klikken op een checkbox gaat door verschillende statussen heen _Alle Vervangen Vervang door Gebruik authentificatie Pagina herstellen naar opgeslagen versie? Rev Rechts Rechter zijpaneel Rechter kantlijn positie Rij omlaag Rij omhoog V_ersie Opslaan... _Kopie Opslaan... Kopie Opslaan Versie Opslaan Bladwijzers opslaan Versie opgeslagen vanuit zim Score Screenshot applicatie Zoeken Zoek _Verwijzingen... Zoeken in deze sectie Sectie Sectie(s) om te negeren Sectie(s) met taken Bestand Selecteren Map Selecteren Kies een afbeelding Selecteer een versie om de wijzigingen met die versie en de huidige staat 
te bekijken. Of selecteer meerdere versies om de verschillen ertussen te bekijken.
 Selecteer het formaat Selecteer de locatie Selecteer de pagina's Window of deel van het scherm selecteren Selectie Volgorde diagram Web server nog niet gestart Web server gestart Web server gestopt Naam veranderen Zet ToC lettergrote Standaard web browser Standaard text editor Op huidige pagina instellen Toon aantal referenties in titel Regelnummers weergeven Laat pagina lijst als platte lijst zien Laat Inhoudsopgave als "floating" widget zien Geef wijzigingen weer (_C) Laat een apart status icoon zien voor elk notitieboek Toon einddatum in zijpaneel Toon schrijf-balk langs onderkant van de pagina Laat volledige pagina naam zien Laat volledige pagina naam zien Laat volledige pagina naam zien Laat balk met hulp functies zien In de werkbalk toevoegen Toon linkmap knop in de applicatie balk Toon rechter kantlijn Toon knop voor takenlijst in de applicatie-balk Laat taken lijst in zijpaneel zien Laat de cursor ook zien voor pagina's die niet kunnen worden bewerkt Toon pagina title in de inhoudstabel Eén _pagina Grootte Klein Slimme "home" toets Er is een fout opgetreden tijdens "%s" Op alfabet sorteren Sorteer pagina's op labels Codeblokken Spellingscontrole Begin Start _Web Server Su_bscript Su_perscript Gebruik thumbnails voor SVG bestanden Sy_mbool... Syntax Systeemstandaard Tab-breedte Tabel Tabelleneditor Inhoudsopgave Labels Label voor (nog) niet actieve taken Taak Takenlijst Taken Template Sjablonen Tekst Tekst Bestanden Tekst Uit _Bestand... Tekst achtergrond kleur Tekst voorgrond kleur Modus voor het afbreken van regels Het bestand "%s" bestaat maar is geen wiki pagina.
Wilt u het importeren? De map
%s
bestaat niet.
Wilt u deze map aanmaken? De folder "%s" bestaat niet.
Wilt u deze nu aanmaken? Deze parameters worden vervangen in het commando:
<tt>
<b>%f</b> de brontekst van de pagina als tijdelijk bestand
<b>%d</b> de folder met aangehechte bestanden voor de huidige pagina
<b>%s</b> het bestand met brontekst van de pagina
<b>%p</b> de pagina naam
<b>%n</b> de locatie van het notitieboek
<b>%D</b> de locatie van de root folder
<b>%t</b> geselecteerde tekst of het woord onder de cursor
<b>%T</b> geselecteerde tekst inclusief wiki codes
</tt>
 De rekenmachine plugin kan de vergelijking
voor de cursor niet evalueren. Rij niet verwijderd, de tabel moet minimaal één rij bevatten. Thema Er zijn geen veranderingen in dit notitieboek sinds de laatste opgeslagen versie Er was een probleem met het laden van deze plugin

 Dit kan betekenen dat de juiste
woordenboeken niet zijn geïnstalleerd Dit bestand bestaat al.
Wilt u het overschrijven? Deze pagina heeft geen map met bijlagen Deze paginanaam kan niet worden gebruik vanwege een bestand dat al bestaat in de opslag folder Deze paginanaam kan niet worden gebruik vanwege technische beperkingen in de data opslag Deze invoegtoepassing geeft de mogelijkheid een screenshot
te nemen en deze direct in zim in te voegen.

Dit is een standaard invoegtoepassing voor zim.
 Deze plugin voegt een "locatie balk" toe aan de bovenkant 
van het window. Deze balk kan de locatie van de huidige pagina 
laten zien, maar ook recent bezochte paginas of recent gewijzigde 
paginas.
 Deze plugin voegt een dialoog toe die alle open taken in 
dit notitieboek laat zien. Open taken kunnen checkboxen
zijn of codes zoals "TODO" of "FIXME" in de text.

Dit is een standaardplugin voor zim.
 Deze plugin voegt een dialoog toe om snel tekst of plakbord
inhoud in een zim pagina te plakken.

Dit is een hoofd plugin van zim.
 Deze plugin geeft snelle toegang tot zim 
door een icoon toe te voegen aan de status balk.

Deze plugin vereist Gtk+ versie 2.10 of nieuwer.

Dit is een standaard plugin voor zim.
 Deze invoegtoepassing voegt een extra paneel toe met een
lijst van pagina's die naar de huidige pagina refereren.

Dit is een standaard invoegtoepassing voor zim.
 Deze plugin voegt een extra widget toe met
een inhoudsopgave van de huidige pagina.

Dit is een standaard plugin voor zim.
 Deze plugin voegt settings toe om
afleiding vrij te werken in zim.
 Deze plugin voegt een hulpvenster toe om symbolen en
speciale tekens in te voegen.

Dit is een standaardplugin voor zim.
 Deze plugin voegt een pagina index toe aan de zijkant 
van het applicatie scherm
 Deze plugin voegt versie controle toe voor notitieboeken.

Deze plugin support de Bazaar, Git, en Mercurial versie controle systemen.
 Deze plugin maakt het mogelijk om "Code blokken" toe te voegen
aan een pagina. Deze worden getoond met syntax highlighting,
regel nummering etc.
 Deze plugin helpt met kleine berekeningen in
zim paginas. Hij is gebaseerd op de "arithmic" module
van http://pp.com.mx/python/arithmetic.
 Deze invoegtoepassing kan eenvoudige wiskundige vergelijkingen 
in de tekst evalueren.

Dit is een standaard invoegtoepassing voor zim.
 Deze plugin heeft ook notitieboek eigenschappen.
Zie de notitie boek eigenschappen dialoog. De plugin kan niet worden geactiveerd vanwege missende afhankelijkheden.
Zie de sectie met afhankelijkheden hieronder.

 Deze plugin opent een zoek dialoog voor het snel vinden en uitvoeren van menu items. Deze zoek dialog kan worden geopend door de toetsencombinatie Ctrl+Shift+P welke kan worden gepersonaliseerd via de toetsencombinatie configuratie. Deze plugin voegt een diagram editor toe op basis van Ditaa.

Dit is een standaard plugin voor zim.
 Deze plugin geeft de mogelijkheid om diagrammen in de
tekst op te nemen en te wijzigen gemaakt met GraphViz.

Dit is een standaardplugin voor zim.
 Deze plugin voegt een nieuw venster toe met een diagram
van alle links en referenties rond de huidige pagina. Dit is
bruikbaar als een overzicht van relaties tussen verschillende 
pagina's.

Dit is een standaardplugin voor zim.
 Deze plugin voegt een menubalk voor macOS toe. Deze plugin voegt een index toe die gesorteerd is op label
en gefilterd kan worden met behulp van een "tag cloud".
 Deze plugin voegt de mogelijkheid toe om plots te 
gebruiken gebaseerd op GNU R.
 Deze plugin voegt een editor toe om grafieken in te voegen gebaseerd op GNUplot
 Deze plugin voegt een volgorde diagram toe gebasseerd op "seqdiag".
Dit vergemakkelijkt het bewerken van volgorde diagrammen.
 Deze plugin stuurt de huidige pagina naar de 
webbrowser. Bij gebrek aan printer support
in zim geeft dit de mogelijkheid om de pagina 
vanuit de browser te printen.

Dit is een standaard plugin voor zim.
 Deze plugin geeft de mogelijkheid om vergelijkingen in de
tekst op te nemen en deze te bewerken. Vergelijkingen
worden geschreven in latex en zijn in de editor als plaatje te zien.

Dit is een standaardplugin voor zim.
 Deze invoegtoepassing voegt de mogelijkheid toe om
notenbalken in te voegen door gebruik te maken 
van GNU Lilypond.

Dit is een standaard invoegtoepassing voor zim
 Deze plugin laat een folder met aangehechte bestanden
zien voor de huidige pagina.
 Deze plugin voegt een functie toe om geselecteerde
regels te sorteren op alfabetische volgorde.
Als de lijst al gesorteerd is wordt de volgorde omgedraaid
(A-Z naar Z-A).
 Deze plugin veranderd een sectie van een notitieboek in een logboek
met een pagina per dag, per week of per maand.
Het voegt ook een kalender toe om deze paginas te openen.
 Dit treedt meestal op wanneer het bestand ongeldige karakters bevat Titel Om verder te gaan kunt u of een kopie van deze pagina
opslaan, of alle wijzigingen weggooien. Als u een kopie opslaat
zullen wijzigingen ook worden weggegooid, maar kunt u die
later herstellen door de pagina te importeren. Om een nieuw notitieboek te maken moet u een lege folder
selecteren of de folder van een bestaand zim notitieboek.
 Inhoud Van_daag Vandaag Markeer checkbox '<' Check Checkbox '>' Markeer checkbox 'V' Markeer checkbox 'X' Wijzigen toestaan Wijzigen toestaan Werkbalk Werkbalk grote Werkbalk stijl Top Bovenste paneel Verplaats pagina naar afval Naar afval verplaatsen is mislukt, wilt u dit bestand permanent verwijderen? Status Icoon Regels afbreken op woorden of karakters Zet pagina namen om in labels voor de taken Type Reset Checkbox Gebruik <BackSpace> om inspringen ongedaan te maken.
(Als deze optie uitstaat kunt u nog steeds <Shift><Tab> gebruiken) Niet bekend Onbekent type plaatje Onbekent object type Onbepaald Zonder label Update ook de %i pagina die naar deze pagina verwijst Update ook de %i pagina's die naar deze pagina verwijzen Wijzig de titel van deze pagina Snelkoppelingen bijwerken Index aan het herladen Gebruik %s om naar het zijpaneel te gaan Wijzig lettertype Gebruik een pagina voor elke Gebruik datum van journaal paginas Gebruik een donker kleuren thema Gebruik horizontaal scrollen (heeft herstart nodig) Gebruik de <Enter> toets om links te volgen.
(Wanneer deze optie uitstaat kunt nog steeds 
<Alt><Enter> gebruiken om links te volgen.) Gebruik thumbnails Gebruikersnaam Versiebeheer Versiebeheer is momenteel niet in gebruik voor dit notitieboek.
Wilt u dit inschakelen? Versies Bekijk Ge_annoteerde Bekijk _Log Bekijk debug log Webserver Week Als u dit probleem rapporteert, voeg
dan de onderstaande informatie toe Alleen hele woorden Breedte Zim pagina: %s Deze plugin voegt tabellen in paginas toe
 Woorden Tellen Woorden Tellen... Woorden Jaar Gisteren U heeft een bestand in een andere applicatie geopend om te bewerken. Deze dialoog kan gesloten worden als u klaar bent met dit bestand Hier kunt u extra applicaties configureren die in
het "extra" menu en in de toolbar worden toegevoegd. Het systeem is geconfigureerd met %s als karakter set. Als u speciale karakters wilt gebruiken,
of foutmeldingen ziet vanwege de karakter set, zorg er dan voor dat het systeem is geconfigureerd met "UTF-8" Zim Desktop Wiki Uitz_oomen In_fo _Voeg toe _Alle panelen _Arithmetic _Bijlagen... _Terug _Bladeren _Bugs Af_breken _Checkbox Om_laag Opmaak _Verwijderen Sl_uiten Alles _inklappen I_nhoud K_opiëren _Hiernaar kopieren _Verwijderen Pagina Ver_wijderen _Verwijderen... _Wijzigingen wegwerpen Regel _dupliceren Be_werken Link be_werken Link of Object _Wijzigen... _Eigenschapen Wijzigen B_ewerken… _Nadruk _FAQ _Bestand _Zoeken _Vinden... _Vooruit _Schermvullend _Ga naar _Hulp _Markeren _Geschiedenis _Start _Afbeelding... Pagina _Importeren... _Invoegen _Ga Pagina... _Toetsen _Beperk de zoekactie tot de huidige pagina en sub-paginas _Link _Link datum _Link... _Markeren _Meer _Verplaatsen _Hiernaar verplaatsen Regel om_laag schuiven Regel om_hoog schuiven _Nieuwe Pagina Hier... _Nieuwe Pagina... Vo_lgende Vo_lgende in de index _Geen _Normale grootte Ge_nummerde lijst _Ok _Openen _Open ander notitieboek... _Overige... _Pagina _Pagina Hierarchie _Omhoog _Plakken _Tonen Vo_rige Vor_ige in de index _Printen _Printen naar Webbrowser _Eigenschappen _Korte Notitie... _Afsluiten _Recente pagina's Op_nieuw _Reguliere expressie _Herladen Ver_wijder Titel stijl _verwijderen Regel ver_wijderen Link _Verwijderen Lijst markering _verwijderen Pagina he_rnoemen of verplaatsen... _Hernoemen... _Vervang _Vervangen... Grootte He_rstellen He_rstel Versie _Opslaan Kopie _Opslaan _Schermafdruk... _Zoeken _Zoeken... Versturen... _Zijpanelen Zij bij Zij (_S) Regels _Sorteren _Doorhalen _Vet _Subscript _Superscript _Sjablonen E_xtra _Ongedaan maken _Letterlijk _Versies... Beel_d In_zoomen als eind datum voor taken als begin datum voor taken calendar:week_start:1 niet gebruiken horizontale lijnen macOS Menubalk geen rasterlijnen alleen lezen seconden Launchpad Contributions:
  Bart https://launchpad.net/~bartvanherck
  Bernard Decock https://launchpad.net/~decockbernard
  Christopher https://launchpad.net/~cstandaert
  Dasrakel https://launchpad.net/~e-meil-n
  Frits Salomons https://launchpad.net/~salomons
  Jaap Karssenberg https://launchpad.net/~jaap.karssenberg
  Jaap-Andre de Hoop https://launchpad.net/~j-dehoop
  Luc Roobrouck https://launchpad.net/~luc-roobrouck
  Vincent Gerritsen https://launchpad.net/~vgerrits
  Yentl https://launchpad.net/~yentlvt
  user109519 https://launchpad.net/~cumulus-007 verticale lijnen met rasterlijnen {count} van {total} 