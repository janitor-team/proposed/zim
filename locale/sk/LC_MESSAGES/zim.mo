��    Q     �
              ,     -     9     H     X     e  -   r  V   �  	   �  3        5  
   K     V     b     u     �     �  $   �  /   �  %   �  	          
   '     2     G  
   O     Z     j     y     �     �     �     �     �     �  
   �     �       3        S     n     �     �     �     �     �     �     �     �     �     �     �               #     0     <  $   D  
   i     t  
   �  	   �     �     �     �     �     �     �     �     �               +     7     S     \     c  
   h     s     �     �  v   �          #     +     2     L     P  
   X  
   c  
   n  
   y  
   �     �     �  	   �     �     �     �     �  
   �     �     �          !     1     C     R     _     k     }     �  	   �     �     �     �     �     �     �     �     �     �          "     .     4     9     B     S     c     �     �     �  	   �     �     �     �     �     �     �          "     1     G     P     \  	   j     t     }  	   �     �  	   �  '   �  3   �                          !     -     >  
   J  
   U     `     n     }     �     �     �     �     �     �              	   "      ,      9      P      W      l      x      �   �   �      #!      <!     ]!     x!  	   �!     �!     �!     �!     �!     �!     �!     �!     "     "     %"     3"  
   E"     P"     _"     d"  	   i"     s"     |"  
   �"     �"  A   �"  K   �"  6   -#  x   d#  �   �#  �   �$  �   +%  �   �%  k   C&  ;   �&  =   �&  j   )'     �'     (     (     !(     5(     I(  	   ](     g(     �(     �(  !   �(     �(     �(     �(  U   �(     S)     \)  	   l)     v)     {)     �)  
   �)     �)     �)     �)     �)  	   �)     �)     �)     �)     �)     �)  	   �)     �)     *     *  	   *     '*     -*     5*     B*     S*     c*  
   i*     t*     �*  	   �*     �*     �*     �*     �*     �*     �*     �*  
   �*     �*     �*  	   �*     �*     +     +     "+     /+     5+     C+     L+     R+     X+     h+     v+     �+     �+     �+     �+     �+     �+  	   �+     �+     �+     �+     �+  	   �+     �+     ,     ,     $,     2,     8,     L,     T,     a,     n,     w,     �,     �,     �,  
   �,     �,     �,  
   �,     �,     �,     �,     �,     �,  
   -     -  
   -     *-     1-  	   7-     A-     N-     T-     ]-     f-     n-    �-     �/     �/     �/     �/     �/  %   �/  o   0     q0  C   �0     �0  	   �0     �0     �0     1  	   1     "1  #   (1  0   L1  9   }1  	   �1     �1  
   �1     �1     �1     2     2     2     62     >2  	   U2     _2     q2     �2     �2     �2     �2  &   �2  A   3     V3  "   q3  .   �3     �3  
   �3     �3     �3     4     4      4     %4     14     H4     T4     c4     x4  	   �4     �4  *   �4     �4     �4     �4     �4     5  
   5     &5  	   A5     K5     W5     n5     �5  %   �5     �5     �5  #   �5     �5     6     6     6     (6     B6  
   V6  k   a6     �6     �6     �6     �6     7     7  	   7  	   )7  	   37  	   =7  	   G7     Q7     Y7     v7     �7     �7     �7     �7     �7     �7     �7     �7     8     8     38     D8     V8     e8     �8     �8  	   �8     �8     �8     �8     �8      9     9     9     9     %9  #   59     Y9     x9     9     �9     �9     �9  !   �9     �9     �9  
   :     :     :  !   :     ?:     S:     g:      {:     �:     �:     �:     �:     �:     ;  
   ;     ";     5;     >;     L;     ];  *   c;  A   �;     �;     �;     �;  
   �;  
   <     <     %<  
   1<     <<     N<     c<     u<     �<  $   �<     �<     �<     �<     =     =     /=     B=     R=     b=     z=     �=     �=     �=     �=  �   �=     ^>  *   v>     �>     �>     �>     �>     �>     ?     ?     4?     E?     d?  	   t?     ~?     �?     �?     �?     �?     �?     �?     �?  	   @     @     @     !@  :   5@  R   p@  /   �@  �   �@  �   yA  �   oB  �   C  �   �C  l   oD  A   �D  8   E  g   WE  `   �E      F     'F  &   -F  &   TF  &   {F     �F  $   �F     �F     �F  (   G     7G     PG     mG  Q   ~G     �G     �G     �G  	   �G     	H     H     H     *H     9H     >H     BH  
   SH     ^H  
   jH     uH     }H     �H     �H     �H     �H  
   �H     �H     �H     �H     I     I     #I  	   4I     >I     NI     nI  	   �I     �I     �I     �I     �I     �I     �I  
   �I     �I  
   �I     �I     �I     J  	   J     &J     6J     KJ     RJ     cJ     jJ     zJ     �J     �J     �J  	   �J     �J     �J     �J  	    K      
K     +K  	   8K     BK  
   WK  	   bK     lK     xK     �K     �K     �K     �K     �K  	   �K     �K     �K  
   L     L     $L     9L  	   JL     TL     eL  	   {L  	   �L     �L     �L     �L     �L     �L     �L     �L  
   �L  
   �L     	M  
   M  
   M  
   'M     2M     @M     QM  �   YM   %A %d %B %Y A desktop wiki Add Application Add Bookmark Add Notebook Add new bookmarks to the beginning of the bar Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 All Files Always use last cursor position when opening a page Annotated Page Source Arithmetic Attach File Attachment Browser Attachments Attachments: Author Automatically saved version from zim Automatically turn "CamelCase" words into links Autosave version on regular intervals Bookmarks Bulle_t List C_onfigure Capture whole screen Changes Characters Check _spelling Checkbo_x List Command Command does not modify data Comment Complete _notebook Configure Plugin Copy Email Address Copy _As... Copy _Link Copy _Location Could not find notebook: %s Could not find the file or folder for this notebook Could not parse expression Could not save page: %s Create a new page for each note Create folder? Created Cu_t Custom Tools Custom _Tools Date Day Default Default notebook Delay Delete Page Delete page "%s"? Dependencies Description Details Do you want to delete all bookmarks? E_xport... Edit Custom Tool Edit Image Edit Link Edit _Source Editing Enable Version Control? Enabled Export Export completed Exporting notebook Failed Failed to run application: %s File Exists File exists File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s For_mat Format Get more templates online Git Gnuplot Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Highlight current line Home Page Horizontal _Line Icon Images Import Page Index page Inline Calculator Insert Date and Time Insert Diagram Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Screenshot Insert Symbol Insert Text From File Interface Interwiki Keyword Jump to Jump to Page Line Sorter Lines Link Map Link to Location Log file Looks like you found a bug Match _case Month Name New Page New S_ub Page... New _Attachment No changes since last version No dependencies No such file: %s Notebook Notebooks OK Open Attachments _Folder Open Folder Open Notebook Open With... Open _Notebook Folder Open in New _Window Open with "%s" Options for plugin %s Other... Output file Output folder Overwrite P_athbar Page Page Name Page Template Paragraph Please enter a comment for this version Please select a name and a folder for the notebook. Plugin Plugins Port Pr_eferences Preferences Print to Browser Proper_ties Properties Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Removing Links Rename page "%s" Replace _All Replace with S_ave Version... Save A _Copy... Save Copy Save Version Saved version from zim Search Search _Backlinks... Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Server not started Server started Server stopped Show Line Numbers Show _Changes Show in the toolbar Single _page Size Sort alphabetically Spell Checker Start _Web Server Sy_mbol... System Default Tags Task Task List Template Text Text Files Text From _File... The folder "%s" does not yet exist.
Do you want to create it now? There are no changes in this notebook since the last version that was saved This file already exists.
Do you want to overwrite it? This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 Title To_day Toggle Checkbox '>' Toggle Checkbox 'V' Toggle Checkbox 'X' Tray Icon Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use a page for each Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Week Whole _word Width Word Count Word Count... Words Year Zim Desktop Wiki Zoom _Out _About _Arithmetic _Back _Browse _Bugs _Checkbox _Child _Clear Formatting _Close _Contents _Copy _Delete _Delete Page _Discard Changes _Duplicate Line _Edit _Edit Link _Edit Link or Object... _Edit Properties _Emphasis _FAQ _File _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move Line Down _Move Line Up _New Page... _Next _None _Normal Size _Numbered List _Open _Open Another Notebook... _Other... _Page _Parent _Paste _Preview _Previous _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove Line _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In readonly seconds translator-credits Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2018-01-29 15:01+0000
Last-Translator: Robert Hartl <robert@chami.sk>
Language-Team: Slovak <sk-i18n@lists.linux.sk>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 1 : (n>=2 && n<=4) ? 2 : 0;
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
X-Generator: Launchpad (build bceb5ef013b87ef7aafe0755545ceb689ca7ac60)
 %A %d %B %Y Desktopová wiki Pridať aplikáciu Pridať záložku Pridať zápistník Pridať záložky na začiatok panela Pridá podporu pre kontrolu pravopisu pomocou gtkspell.

Toto je základný zásuvný modul dodávaný so zim.
 Všetky súbory Vždy použiť posledné umiestnenie kurzora pri otvorení stránky Okomentovaný zdroj strany Výpočty Pripojiť súbor Prehliadač príloh Prílohy Prílohy: Autor Automaticky uložená verzia zo zim Automaticky vytvárať odkazy zo ZloženýchSlov Automatické uloženie verzie v pravidelných intervaloch Záložky Od_rážkový zoznam _Nastaviť Zachytiť celú obrazovku Zmeny Znaky Skontrolovať _pravopis Z_aškrtávací zoznam Príkaz Príkaz nezmení dáta Komentár Celý _zápisník Nastavenie zásuvného modulu Kopírovať e-mailovú adresu Kopírovať _ako... Kopírovať _odkaz Kopírovať _umiestnenie Nie je  možné nájsť zápisník: %s Nie je možné nájsť súbor alebo priečinok tohoto zápisníka Výraz je nezrozumiteľný Nie je možné uložiť stranu: %s Vytvoriť novú stránku pre každú poznámku Vytvoriť priečinok? Vytvorené _Vystrihnúť Vlastné nástroje Vlastné _nástroje Dátum Deň Predvolený Predvolený zápisník Oneskorenie Zmazať stranu Zmazať stranu "%s"? Závislosti Poznámka Detaily Želáte si odstrániť všetky záložky? _Exportovať Upraviť vlastné nástroje Upraviť obrázok Upraviť odkaz Upraviť _zdroj Editovanie Povoliť kontrolu verzií? Povolené Exportovať Export bol dokončený Export zápisníka Zlyhalo Nepodarilo sa spustiť aplikáciu: %s Súbor už existuje Súbor existuje Typ súboru nie je podporovaný: %s Názov súboru Filter Hľadať Nájsť ď_alšie Nájsť _predchádzajúce Nájsť a nahradiť Priečinok Priečinok už existuje a obsahuje súbory, hrozí, že pri exporte budú prepísané. Chcete pokračovať? Priečinok %s existuje _Formát Formát Získať viac šablón online Git Gnuplot Nadpis _1 Nadpis _2 Nadpis _3 Nadpis _4 Nadpis _5 Výška Zvýrazniť aktuálny riadok Domovská stránka _Vodorovná čiara Ikona Obrázky Importovať stranu Stránka s indexom Kalkulačka v riadku Vložiť dátum a čas Vložiť diagram Vložiť rovnicu Vložiť graf GNU R Vložiť Gnuplot Vložiť obrázok Vložiť odkaz Vložiť snímok obrazovky Vložiť znak Vložiť text zo súboru Rozhranie kľúčové slovo interwiki Skočiť na Skočiť na stranu Zoraďovač riadkov Riadkov Mapa odkazov Odkaz na Miesto Súbor záznamu Pravdepodobne ste narazili na chybu _Rozlišovať veľkosť písma Mesiac Názov Nová stránka Nová _podstránka... Nová _príloha Žiadne zmeny od poslednej verzie Bez závislostí Súbor %s neexistuje Zápisník Zápisníky OK Otvoriť priečinok s _prílohami Otvoriť priečinok Otvoriť zápisník Otvoriť pomocou... Otvoriť priečinok _zápisníka Otvoriť v novom _okne Otvoriť pomocou "%s" Voľby pre zásuvný modul %s Iné... Výstupný súbor Výstupný priečinok Prapísať Panel _umiestnenia Stránka Názov strany Šablóna strany Odsek Zadajte prosím komentár pre túto verziu Zadajte, prosím, názov a umiestnenie priečinka pre zápisník. Zásuvný modul Zásuvné moduly Port _Možnosti Nastavenia Tlačiť to prehliadača _Vlastnosti Vlastnosti Rýchla poznámka Rýchla poznámka... Posledné úpravy Posledné úpravy... Naposledy _zmenené stránky Preformátovať wiki značky za behu Odstraňovanie odkazov Premenovať stranu "%s" Nahradiť _všetky Nahradiť za U_ložiť verziu... Uložiť kópiu... Uložiť kópiu Uložiť verziu Uložená verzia zo zim Hľadať Hľadať _spätné odkazy Vybrať súbor Vybrať priečinok Vybrať obrázok Vyberte verziu pre zobrazenie zmien medzi ňou a súčasným stavom.
Alebo vyberte viac verzií pre zobrazenie zmien medzi týmito verziami.
 Vyberte formát exportu Vyberte výstupný súbor alebo priečinok Vyberte stránky pre export Vybrať okno alebo oblasť Výber Server nie je spustený Server spustený Server zastavený Zobraziť čísla riadkov Zobraziť _zmeny Zobraziť na paneli nástrojov Jednu _stránku Veľkosť Zoradiť abecedne Kontrola pravopisu Spustiť _webový server _Znak... Predvolené nastavenie systému Značky Úloha Zoznam úloh Šablóna Text Textové súbory Text zo _súboru... Priečinok "%s" ešte neexistuje.
Prajete si ho vytvoriť? V tomto zápisníku neboli vykonané žiadne zmeny od posledného uloženia verzie Tento súbor už existuje
Chcete ho prepísať? Tento zásuvný modul zhotoví snímku obrazovky a priamo ju
vloží na stránku zim.

Toto je základný modul dodávaný so Zimom.
 Tento modul pridáva dialógové okno zobrazujúce všetky
otvorené úlohy v zápisníku. Otvorené úlohy sú buď
nezaškrtnuté prepínače alebo položky označené
slovami ako TODO alebo FIXME.

Je to základní modul dodávaný so Zimom.
 Tento modul pridáva dialóg, pomocou ktorého môžete do stránky
rýchlo pridať nejaký text alebo obsah schránky.

Je to základný modul dodávaný so Zimom.
 Tento zásuvný modul pridáva ikonu v oznamovacej oblasti pre rýchly prístup.

Tento zásuvný modul závisí na Gtk+ verzie 2.10 alebo novšej.

Je to základný modul dodávaný so Zimom.
 Tento modul pridáva dialóg 'Vložiť symbol' a umožňuje
automaticky formátovať typografické znaky.

Je to základní modul dodávaný so Zimom.
 Tento modul poskytuje editor diagramov založený na GraphViz.

Je to základný modul dodávaný so Zimom.
 Tento modul poskytuje editor diagramov, schém a grafov v GNU R.
 Modul poskytuje editor pre grafy založené na Gnuplote
 Tento modul poskytuje editor rovníc založený na LaTeX.

Je to základný modul dodávaný so Zimom.
 Modul zoradí označené riadky podľa abecedy.
Ak je zoznam už zoradený, poradie sa obráti.
 Názov _Dnes Prepnúť zaškrtávacie políčko '>' Prepnúť zaškrtávacie políčko 'V' Prepnúť zaškrtávacie políčko 'X' Ikona v oznamovacej oblasti Aktualizovať hlavičku tejto strany Aktualizácia odkazov Aktualizácia indexu Použiť %s k prepnutiu na bočný panel Použiť vlastné písmo Použiť stránku pre každy Kontrola verzií Kontrola verzii nie je práve povolená pre tento zápisník.
Chcete ju povoliť? Verzie Zobraziť _komentáre Pozrieť _záznam Týždeň Celé _slovo Šírka Počet slov Počet slov... Slov Rok Zim Desktop Wiki _Vzdialiť _O programe _Výpočty _Späť _Prehliadať _Chyby _Zaškrtávacie políčko O úroveň _nižšie _Vyčistiť formátovanie _Zatvoriť _Obsah _Koprírovať _Zmazať _Zmazať stranu _Zrušiť zmeny _Zdvojiť riadok _Upraviť _Upraviť odkaz _Upraviť odkaz alebo objekt... _Upraviť vlastnosti _Kurzíva _FAQ _Súbor _Nájsť... _Dopredu Na _celú obrazovku _Prejsť _Pomocník _Zvýrazniť _História _Domov _Obrázok... _Importovať stránku... _Vložiť _Skočiť na... _Klávesové skratky _Odkaz _Odkaz na dátum _Odkaz _Podčiarknuté _Viac _Presunúť riadok dole _Presunúť riadok hore _Nová stránka... Ď_alšie Ž_iadny Normálna _veľkosť Čí_slovaný zoznam _Otvoriť _Otvoriť ďalší zápisník... _Ostatné... _Stránka O úroveň _vyššie _Prilepiť _Náhľad _Predošlé _Tlačiť to prehliadača _Rýchla poznámka... _Koniec _Posledné stránky _Znova _Regulárny výraz _Obnoviť _Zmazať riadok _Odstrániť odkaz Na_hradiť Na_hradiť... _Pôvodná veľkosť _Obnoviť verziu _Uložiť _Uložiť kópiu _Snímok obrazovky... _Hľadať _Hľadať _Poslať na... _Bočné tabule Zoradiť _riadky P_reškrtnuté _Tučné _Dolný index _Horný index Š_ablóny _Nástroje _Späť _Strojopis _Verzie... _Zobraziť _Priblížiť len na čítanie sekúnd Launchpad Contributions:
  DAG Software https://launchpad.net/~dagsoftware
  Robert Hartl https://launchpad.net/~lesnyskriatok
  mirek https://launchpad.net/~bletvaska 