��    �     �              \  .   ]     �  0   �     �     �     �  i   �     c     s  V   �  	   �  	   �  3   �  Y        y  
   �     �     �     �     �     �  $   �  ?     /   D  (   t  %   �     �     �     �  
   �     �            
   #     .     >  <   M     �     �     �     �     �     �     �  +   �  3   $     X     k     y  
   �     �     �  3   �     �               0     H     h     w     |     �     �     �     �     �  0   �     �     �     �     	            (      4   ~   <      �   
   �      �   
   �   	   �      �      !     !      !     8!     @!     O!     V!     i!     p!     �!     �!     �!     �!     �!     �!     "     "     "  
   "     '"     6"  	   G"     Q"  v   X"     �"  *   �"  c   #     p#     x#     #  
   �#  
   �#  
   �#  
   �#  
   �#     �#  	   �#     �#     �#     �#     �#  
   �#     �#     
$     $     .$     >$     P$     _$     l$     x$     �$     �$  	   �$     �$     �$     �$     �$     �$     %     %     %     %%  2   .%     a%     i%     r%     {%     �%     �%     �%     �%     �%     �%     �%     &     &     #&     ,&     5&     F&     V&     l&     �&     �&     �&     �&     �&  	   �&     �&     �&     
'     '     $'     1'     E'  
   ['     f'     z'     �'     �'     �'     �'     �'     �'  	   �'     �'     �'  0   �'  	   "(     ,(  	   :(  '   D(  V   l(  3   �(     �(     �(     )     )     ")     /)     ;)     L)  
   X)  
   c)     n)     |)     �)     �)     �)     �)     �)     �)     *     *     .*     2*     B*     S*  	   c*     m*     z*     �*     �*     �*     �*     �*     �*  �   �*     j+      �+     �+     �+  	   �+     �+     �+     ,     ,  &    ,     G,  5   [,     �,     �,  &   �,     �,     �,     �,  
   �,     -     -     -  	   !-     +-  	   4-     >-  
   C-     N-  ?   a-  A   �-  S   �-  K   7.  @   �.  6   �.  -   �.  x   )/  �   �/  �   '0  �   �0  }   ?1  k   �1  �   )2  R   �2  ;   P3  =   �3    �3  j   �4     I5  7   �5     6  �   6     �6     �6     �6  	   �6  '   �6  D   �6  H   07     y7     �7     �7     �7     �7  P   �7     .8  U   >8     �8     �8  	   �8  
   �8     �8  N   �8     9     "9  
   (9     39     A9     G9  	   L9  ^   V9  f   �9     :  	   -:     7:  
   >:     I:     U:     [:     c:     i:     p:     �:     �:  	   �:     �:     �:     �:     �:     �:  
   �:     �:     �:  	   ;     ;     ;     ;     %;     .;     :;     >;  
   D;     O;     X;  	   ^;     h;     x;     �;     �;     �;     �;     �;     �;     �;     �;     �;     �;     �;     �;     �;     �;     <  	   <     '<     -<     5<     <<  	   E<     O<     a<     p<     v<     �<     �<     �<     �<     �<     �<     �<     �<     �<  
   �<     �<     =  
   =     =     $=     0=     >=     J=     R=  
   Z=     e=  
   r=     }=     �=  	   �=     �=     �=     �=     �=     �=     �=     �=  �  �=  K   �?     !@  :   4@  '   o@     �@     �@  i   �@     A     2A  v   BA     �A     �A  @   �A  T   B  (   sB     �B     �B     �B     �B     �B     �B  +   �B  I   %C  ?   oC  <   �C  <   �C     )D     8D     AD     RD  #   ^D     �D     �D  
   �D     �D  !   �D  D   �D     0E     7E     <E  
   \E     gE     yE     �E  5   �E  ?   �E      F     >F     LF     \F     oF     �F  6   �F  (   �F  %   G     'G  $   ?G  &   dG     �G     �G     �G     �G     �G     �G     �G     �G  7   H     ?H     _H     gH     zH     �H     �H     �H  �   �H     BI     PI  !   ]I     I     �I     �I     �I     �I  ,   �I     �I     J     J      J     3J     FJ  +   \J     �J     �J     �J  !   �J  "   �J     K     K     &K     -K     >K     OK     dK     vK  �   K     L  +   L  l   FL     �L     �L     �L     �L     �L     �L     �L      M     M     M     &M     ,M     3M     DM     LM     _M     uM     �M     �M     �M     �M     �M     �M     N     .N      @N  	   aN     kN     �N     �N     �N     �N     �N     �N     �N     O  ?   O  	   VO  
   `O     kO     �O     �O  &   �O  #   �O  
   �O     
P     P     'P     EP     UP     ZP     hP     uP     �P     �P  (   �P     �P     �P      Q       Q     AQ     RQ     dQ     lQ     �Q     �Q     �Q     �Q     �Q     �Q     �Q     R     -R     <R     ER     dR     mR     �R     �R     �R     �R  /   �R     �R     �R  
   
S  5   S  ]   KS  8   �S     �S  	   �S     �S     �S     T      T     -T     CT     PT     \T     iT     vT     �T      �T  .   �T     �T      U     U     (U  *   8U  	   cU     mU     �U     �U     �U     �U     �U     �U     �U     �U     V     )V     >V  �   UV  $   �V  +   W  !   9W     [W  
   yW     �W     �W     �W     �W  ,   �W     X  >   ,X     kX     }X  #   �X     �X     �X     �X  	   �X     �X  	   Y     !Y     (Y     9Y     @Y     HY     NY     aY  4   wY  0   �Y  ^   �Y  F   <Z  V   �Z  6   �Z  ,   [  �   >[  �   �[  �   �\  �   O]  �   �]     v^    �^  `   �_  O   \`  Q   �`  .  �`  �   -b  �   �b  I   5c     c  �   �c     >d     Dd     Id     Xd  D   td  ]   �d  a   e  (   ye     �e     �e  #   �e     �e  a   f     sf  Z   �f  	   �f     �f     �f     �f     g  Z   g     ng     �g     �g     �g     �g     �g     �g  [   �g  q   h     �h     �h  
   �h     �h     �h     �h  	   �h     �h  	   �h     �h     i     i  	   i     &i  	   .i     8i     Hi     di     li     }i     �i     �i     �i  	   �i  
   �i     �i     �i     j     j  	   j  
   j     #j     +j     2j     Dj     Mj     [j  	   oj     yj     �j     �j     �j     �j     �j  	   �j     �j     �j     �j     �j     �j  	   k     k     k      k     'k  	   5k     ?k     Vk     dk     kk     ~k     �k     �k     �k     �k     �k     �k     �k      l     l     l     3l     ;l     Cl     Pl     `l     ml     |l     �l     �l     �l     �l     �l  	   �l     �l  	   �l     �l     �l     �l     m     m  K  m   %(cmd)s
returned non-zero exit status %(code)i %A %d %B %Y %i file will be deleted %i files will be deleted %i open item %i open items <Top> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. Add Application Add Notebook Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 All Files All Tasks Always use last cursor position when opening a page An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Arithmetic Attach File Attach image first Attachment Browser Attachments Author Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave version on regular intervals Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Capture whole screen Changes Characters Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Command Command does not modify data Comment Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Could not find notebook: %s Could not find the file or folder for this notebook Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Dependencies Description Details Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root E_xport... Edit Custom Tool Edit Image Edit Link Edit _Source Editing Editing file: %s Enable Version Control? Enabled Evaluate _Math Export Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File changed on disk: %s File exists File is not writable: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Gnuplot Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Home Page Icon Images Import Page Index Index page Inline Calculator Insert Date and Time Insert Diagram Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Screenshot Insert Symbol Insert Text From File Interface Interwiki Keyword Jump to Jump to Page Last Modified Leave link to new page Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log file Looks like you found a bug Make default application Map document root to URL Match _case Modified Month Move Selected Text... Move Text to Other Page Move text to Name New File New Page New S_ub Page... New _Attachment No Applications Found No changes since last version No dependencies No such file: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open in New _Window Open new page Open with "%s" Optional Options for plugin %s Other... Output file Output folder Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Name Page Template Paragraph Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Plugin Plugins Port Position in the window Pr_eferences Preferences Print to Browser Proper_ties Properties Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Removing Links Rename page "%s" Replace _All Replace with Restore page to saved version? Rev Right Side Pane S_ave Version... Save A _Copy... Save Copy Save Version Saved version from zim Score Search Search _Backlinks... Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Server not started Server started Server stopped Show _Changes Show a separate icon for each notebook Show in the toolbar Show the cursor also for pages that can not be edited Single _page Size Some error occurred while running "%s" Sort pages by tags Spell Checker Start _Web Server Sy_mbol... System Default Tags Task Task List Template Templates Text Text Files Text From _File... The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The inline calculator plugin was not able
to evaluate the expression at the cursor. There are no changes in this notebook since the last version that was saved This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To_day Today Top Pane Tray Icon Turn page name into tags for task items Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use a custom font Use a page for each Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Zim Desktop Wiki Zoom _Out _About _All Panes _Arithmetic _Back _Browse _Bugs _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Delete _Delete Page _Discard Changes _Edit _Edit Link _Edit Link or Object... _Edit Properties _Emphasis _FAQ _File _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _New Page... _Next _None _Normal Size _Numbered List _Open _Open Another Notebook... _Other... _Page _Parent _Paste _Preview _Previous _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In calendar:week_start:0 readonly seconds translator-credits Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2014-10-01 07:43+0000
Last-Translator: Jaap Karssenberg <jaap.karssenberg@gmail.com>
Language-Team: Galician <gl@li.org>
Language: gl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
X-Generator: Launchpad (build bceb5ef013b87ef7aafe0755545ceb689ca7ac60)
 O comando:
%(cmd)s
devolveu un código de saída distinto de cero: %(code)i %A, %d de %B de %Y %i ficheiro será eliminado %i ficheiros serán eliminados %i elemento aberto %i elementos abertos <Tope> Un wiki de escritorio Xa existe un un ficheiro co nome <b>"%s"</b>.
Pode usar outro nome ou sobreescribir o ficheiro existente. Engadir aplicación Engadir caderno Engade soporte de comprobación de ortografía usando gtkspell.

Este é un complemento básico distribuído con zim.
 Todos os ficheiros Todas as tarefas Lembrar a última posición do cursor cando se abra unha páxina Produciuse un erro ó xerar a imaxe.
¿Quere gardar o fonte do texto de todos modos? Páxina de código fonte con anotacións Aritmética Adxuntar ficheiro Adxuntar a imaxe primeiro Navegador de Adxuntos Adxuntos Autor Versión gardada automáticamente desde zim Seleccionar automáticamente a palabra actual cando se aplique un formato Convertir automáticamente as palabras "CamelCase" en ligazóns Convertir automáticamente as rutas a ficheiros en ligazóns Gardar automáticamente unha versión a intervalos regulares Panel inferior Examinar Lista de _puntos C_onfigurar Non se pode modificar a páxina: %s Capturar a pantalla completa Cambios Caracteres Comprobar _ortografía Lista de _caixas de verificación Icona clásica,
non usar o novo estilo de iconas de estado de Ubuntu Limpar Orde O comando non modifica os datos Comentario Caderno _completo Configurar aplicacións Configurar o complemento Seleccione un aplicativo para abrir as ligazóns "%s" Seleccione unha aplicación para abrir os arquivos
do tipo "%s" Copiar o enderezo de correo-e Copiar modelo Copiar _Como... Copiar a _ligazón Copiar _localización Non se atopou o caderno: %s Non se atopou o ficheiro ou cartafol para este caderno Non foi posible lanzar a aplicación: %s Non foi posible analizar a expresión Non foi posible ler: %s Non foi posible gardar a páxina: %s Crear unha nova páxina para cada nota ¿Crear o cartafol? Cor_tar Ferramentas personalizadas Ferramentas _personalizadas Personalizar… Data Día Predeterminado Formato por defecto para o texto copiado ó portapapeis Caderno de notas predeterminado Retraso Eliminar a páxina ¿Eliminar a páxina "%s"? Dependencias Descripción Detalles ¿Desexa restaurar a páxina: %(page)s
á versión gardada: %(version)s?

¡Perderanse tódolos cambios desde a última versión gardada! Documento pai E_xportar… Editar a ferramenta personalizada Editar imaxe Editar a ligazón Editar f_onte Editando Editando ficheiro: %s ¿Activar o sistema de control de versións? Activado Evaluar _matemáticas Exportar Exportando caderno Produciuse un erro Erro executandose: %s Houbo un erro ó executar a aplicación: %s O ficheiro xa existe %s arquivos cambiados O ficheiro xa existe No se pode escribir o arquivo: %s Tipo de ficheiro non soportado: %s Nome de ficheiro Filtrar Buscar Buscar _seguinte Buscar _anterior Buscar e substituír Palabras a atopar Cartafol O cartafol xa existe e non está baleiro: exportar a este cartafol pode sobreescribir os ficheiros existentes. ¿Quere continuar? O cartafol xa existe: %s Carpeta con modelos para ficheiros adxuntos Para a búsqueda avanzada pode usar operadores como
AND, OR e NOT. Mire a páxina de axuda para saber máis. For_mato Formato Gnuplot Cabeceira _1 Cabeceira _2 Cabeceira _3 Cabeceira _4 Cabeceira _5 Altura Páxina de inicio Icona Imaxes Importar páxina Índice Páxina de índice Calculadora integrada _Insertar data e hora Insertar diagrama Insertar ecuación Insertar un gráfico GNU R Insertar gráfico de Gnuplot Inserir unha imaxe Inserir unha ligazón Insertar captura de pantalla Insertar símbolo Insertar texto desde un ficheiro Interface Palabra clave de Interwiki Ir a Ir a Páxina Modificado por última vez Deixar ligazón á nova páxina Panel lateral esquerdo Ordeador de liñas Liñas Mapa de ligazóns Enlazar ficheiros baixo o raíz de documentos coa ruta completa Enlazar a Ubicación Ficheiro de rexistro Seica atopou un fallo Facer aplicación por defecto Mapear o raíz de documentos cunha URL _Distinguir maiúsculas/minúsculas Modificado Mes Mover Texto Seleccionado Mover o Texto a Outra Páxina Mover o texto a Nome Ficheiro novo Páxina nova Nova _subpáxina Novo _Adxunto Non se atoparon aplicacións Non hai cambios desde a última versión Sen dependencias Non tal ficheiro: %s Non está definido tal wiki: %s Non hai ningún modelo instalado Caderno de Notas Cadernos de notas Aceptar Abrir o cartafol de adx_untos Abrir cartafol Abrir caderno Abrir con... Abrir o raí_z de documentos Abrir o cartafol do ca_derno Abrir _páxina Abrir nunha _xanela nova Abir unha nova páxina Abrir con "%s" Opcional Opcións para o complemento %s Outro... Ficheiro de saída Cartafol de saída Sobrescribir Barra de _rutas Páxina A páxina "%s" non ten unha carpeta de adxuntos Nome da páxina Modelo da páxina Parágrafo Por favor, introduza un comentario para esta versión Por favor, lembre que ligar a unha páxina que non existe
creará a páxina automáticamente. Por favor, escolla un nome e un cartafol para o caderno. Complemento Engadidos Porto Posición da ventá Pr_eferencias Preferencias Imprimir ó navegador Propie_dades Propiedades Nota rápida Nota rápida Cambios recentes Cambios recentes... Páxinas _cambiadas recentemente Aplicar o formato wiki a medida que se escribe Eliminando ligazóns Renomear páxina "%s" Substituír _todo Substituír por ¿Restaurar a páxina á versión gardada? Revisión Panel lateral dereito _Gardar versión Gardar unha _copia Gardar unha copia Gardar versión Versión gardada desde zim Puntuación Buscar Buscar nos enlaces _inversos Escoller un ficheiro Escoller un cartafol Seleccionar unha imaxe Seleccione unha versión para ver os cambios entre esa versión e o estado
actual. Ou seleccione varias versións para ver os cambios entre elas.
 Seleccione o formato de exportación Seleccione o ficheiro ou cartafol de saída Seleccione as páxinas a exportar Seleccionar xanela ou rexión Selección O servidor non está activo O servidor está activo O servidor está parado _Amosar cambios Amosar unha icona distinta para cada caderno Amosar na barra de ferramentas Amosar o cursor tamén nas páxinas que non poden ser editadas Só unha _páxina Tamaño Produciuse un erro ó executar "%s" Ordear páxinas por etiquetas Corrector ortográfico Lanzar _servidor web Sí_mbolo Predeterminado do sistema Etiquetas Tarefa Lista de tarefas Modelo Modelos Texto Ficheiros de texto Texto desde _ficheiro A carpeta
%s
non existe aínda.
Desexa creala agora? O cartafol "%s" aínda non existe.
Quere creala? O complemento de calculadora integrada de zim non
puido evaluar a expresión ó pé do cursor. Non hai cambios neste caderno desde que a última versión foi gardada Isto podería significar que non ten os diccionarios
apropiados instalados no sistema. Este ficheiro xa existe.
¿Desexa escribir por enriba? Esta páxina non ten un cartafol de adxuntos Este complemento permite sacar unha captura de pantalla e insertala
nunha páxina de zim.

Este é un complemento básico distribuído con zim.
 Este complemento engade unha xanela para escribir rápidamente
unha nota ou soltar o contido do portapapeis nunha páxina de zim.

Este é un complemento básico distribuído con zim.
 Este elemento engade unha icona na bandexa do sistema para acceder rápidamente.

Este complemento require a versión de Gtk+ 2.10 ou superior.

Este é un complemento básico distribuído con zim.
 Este complemento engade á xanela de 'Insertar símbolo', e
permite dar formato ós caracteres tipográficos.

Este é un complemento básico distribuído con zim.
 Este complemento permite evaluar expresións
matemáticas sinxelas en zim.

Este é un complemento básico distribuído con zim.
 Este complemento proporciona un editor de diagramas baseado en GraphViz.

Este é un complemento básico distribuído con zim.
 Este complemento proporciona unha xanela cunha representación
gráfica da estructura de ligazóns do caderno. Pode ser usada de xeito
parecido a un "mapa mental", amosando cómo se relacionan as páxinas.

Este é un complemento básico distribuído con zim.
 Este complemento proporciona un índice filtrado mediante a selección de etiquetas nunha nube.
 Este complemento proporciona un editor de gráficos para zim baseado en GNU R.
 Este complemento proporciona un editor de gráficos para zim baseado en Gnuplot.
 Este complemento proporciona un amaño para a falta
de soporte de impresión en zim. Exporta a páxina actual
a html é lanza un navegador. Asumindo que o navegador
ten soporte de impresión, isto enviará os seus datos á
impresora en dous pasos.

Este é un complemento básico distribuído con zim.
 Este complemento proporciona un editor de ecuaciones para zim basado en LaTeX.

É un complemento básico distribuído con zim.
 Este plugin ordea as liñas seleccionadas en orden alfabético.
Se a lista xa está ordeada, a orde será revertida (A-Z pasa a Z-A).
 Esto habitualmente significa que o ficheiro contén caracteres inválidos Título Para continuar pode gardar unha copia desta páxina ou
rexeitar as modificacións. Se garda unha copia, 
as modificacións tamén serán rexeitadas, pero pode restaurala máis tarde. _Hoxe Hoxe PAnel superior Icona na bandexa do sistema Trocar os nomes de páxina en etiquetas para os elementos de tarefas Eliminar indentación con <Backspace>
(se está deshabilitado, sempre pode usar <Shift><Tab>) Actualizar %i páxina que enlaza a esta páxina Actualizar %i páxinas que enlazan a esta páxina Actualizar o título do texto da páxina Actualizando ligazóns Actualizando índice Usar un tipo de letra personalizado Usar unha páxina para cada un Use a tecla <Enter> para seguir ligazóns
(se está deshabilitado, sempre pode usar <Alt><Enter>) Control de versións O sistema de control de versións non está activado para este caderno.
¿Desexa activalo? Versións Ver Ver _rexistro Servidor web Semana Cando informe dun fallo por favor inclúa
a información da caixa de texto a continuación Palabras _enteiras Anchura Contador de palabras Contar palabras Palabras Ano Onte Está editando un ficheiro cunha aplicación externa. Pode pechar esta xanela cando remate. Pode engadir ferramentas personalizadas que aparecerán
no menú e barra de ferramentas, ou no menú de contexto. Wiki persoal Zim Reduc_ir _Acerca de Todos os p_aneis _Aritmética _Atrás _Explorar Fallos _Inferior _Limpar formatos _Pechar _Contraer todo _Contidos _Copiar _Eliminar _Borrar páxina _Rexeitar as modificacións _Editar _Editar ligazón _Editar ligazón ou obxecto _Editar propiedades _Énfase _Preguntas máis frecuentes _Ficheiro _Buscar… _Adiante _Pantalla Completa _Ir _Axuda Resaltado _Historial _Inicio _Imaxe _Importar páxina _Inserir Sal_tar a ... Atallos de _teclado _Ligazón _Ligar á data _Ligazón… _Marcar _Máis _Mover _Nova páxina _Seguinte _Ningún Tamaño _normal Lista _Numerada _Abrir Abrir outro _caderno _Outro... _Páxina _Pai _Pegar Vista _previa _Anterior _Imprimir ó navegador Nota _rápida _Saír Páxinas _recentes _Refacer Expresión _regular A_ctualizar _Eliminar a Ligazón _Substituír _Substituír… _Restaurar tamaño _Restaurar versión _Gardar _Gardar unha copia _Captura de pantalla... _Buscar _Buscar _Enviar a… Paneis laterais _Lado a lado _Ordear liñas _Tachar _Negrita _Subíndice Su_períndice _Modelos _Ferramentas _Desfacer Texto _sen formato Versións _Ver _Ampliar calendar:week_start:1 Só lectura segundos Launchpad Contributions:
  Jaap Karssenberg https://launchpad.net/~jaap.karssenberg
  Manuel Xosé Lemos https://launchpad.net/~mxlemos
  Miguel Anxo Bouzada https://launchpad.net/~mbouzada
  Roberto Suarez https://launchpad.net/~robe-allenta
  Xurxo Fresco https://launchpad.net/~xurxof
  marisma https://launchpad.net/~mariamarcp 