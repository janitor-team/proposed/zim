��    �     �              �-  ,   �-  .   �-  ?   �-     ).     5.     R.     k.     �.  0   �.  0   �.     /     /  5   =/     s/  	   y/     �/  %   �/  i   �/  *   "0     M0     T0     d0     q0     ~0  
   �0  -   �0     �0  V   �0     11  	   71  	   A1     K1  3   _1     �1     �1  Y   �1     %2     ;2  .   H2  
   w2     �2     �2     �2     �2     �2     �2     �2  	   �2     �2  -   3  *   /3  $   Z3  ?   3  /   �3  (   �3     4  %   54  ,   [4     �4  	   �4     �4     �4  
   �4     �4  	   �4     �4     �4     �4     �4     5     5  
   5     '5     ?5  �   F5  �   �5     a6     v6     }6     �6  
   �6     �6     �6     �6     �6     �6     7     7     .7  .   =7  <   l7     �7  	   �7  
   �7     �7     �7     �7     �7     8     
8      8     68     I8     `8  +   q8  3   �8      �8     �8     �8  	   
9     9     "9  
   .9     99     H9     d9     {9     �9     �9  3   �9     :     %:     8:     S:     f:     ~:     �:     �:     �:     �:     �:     �:     �:     �:     �:     �:  0   ;     5;     F;     L;     X;  
   j;     u;     |;     �;     �;     �;     �;     �;     �;  )   �;  $   <  ~   0<     �<     �<  
   �<     �<     �<  
   �<  	   �<  
   �<     =     =     =     +=     C=     Q=     Y=  5   b=     �=     �=     �=  !   �=     �=  #   �=     >     $>     +>     >>     \>     h>     {>     �>  #   �>     �>     �>  .   �>     +?     B?     Y?     u?     ~?     �?  
   �?     �?     �?  	   �?  6   �?     �?  v   �?     t@  *   �@  c   �@     A     A     $A  
   +A     6A     NA     hA     lA     tA  
   |A     �A  
   �A  
   �A  
   �A  
   �A  
   �A  
   �A     �A     �A     �A     B  	   2B     <B     MB     RB     XB     eB     }B  %   �B     �B     �B  #   �B     �B     �B  
   �B     �B     C     #C     8C     GC     TC     dC     vC     �C     �C     �C     �C     �C     �C     �C     �C  	   D     D     "D     *D     2D     ?D     KD  �   XD     %E     :E     @E     NE     _E     vE     {E     �E     �E     �E     �E  2   �E     �E     �E     �E     F     F     ,F     EF     \F     uF     �F     �F     �F  	   �F     �F     �F     �F     �F     �F     G     &G     ?G     LG      QG  *   rG     �G     �G     �G     �G     �G     �G     �G     �G     H     )H  *   9H  2   dH     �H     �H     �H     �H     �H     �H  	   I     I     I     'I     @I     LI     ZI     gI     {I  
   �I     �I  	   �I     �I     �I     �I     �I     J     J     J     4J     =J  9   IJ     �J  I   �J  !   �J  '   �J  	   %K     /K     8K  w   =K  �   �K  0   =L  
   nL  	   yL     �L     �L  &   �L     �L     �L     �L  	   	M     M     M     "M     5M  '   >M  V   fM  3   �M  0   �M  (   "N     KN     eN  .   lN     �N     �N     �N     �N  !   �N  !   �N     O     O     !O     2O     :O  
   FO  
   QO     \O     jO     yO     �O     �O     �O  
   �O     �O     �O  
   �O     P     P     P     0P  ?   AP     �P     �P     �P     �P     �P     �P     �P     �P     Q     
Q     Q     "Q  	   2Q     <Q     IQ     XQ     oQ     uQ     �Q     �Q     �Q     �Q     �Q     �Q     �Q     �Q     R  �   R     �R      �R     �R     �R  	   S     S     (S     ;S     JS     YS     fS     wS     �S     �S     �S     �S     �S     �S  &   T     3T     MT     aT     vT     �T     �T      �T     �T     �T  5   �T  &   5U     \U     iU     nU     tU  &   �U     �U     �U     �U     �U     �U     �U  
   V     V     V  
   6V     AV     HV  	   WV     aV     gV     tV     �V     �V     �V  	   �V     �V     �V  	   �V     �V  
   �V     �V     �V     
W      W  F   /W  ?   vW  A   �W  �  �W  S   �Y  =   Z     NZ  K   TZ  )   �Z  6   �Z  -   [  F   /[  I   v[  x   �[  �   9\  �   �\  �   �]  �   .^  �   �^  }   =_  L   �_  �   `  9   �`  �   �`  �   ta  �   b  }   �b  C   c  s   Rc  �   �c  h   �d  k   e  �   qe  -   Ef  R   sf  ;   �f  =   g  v   @g    �g  j   �h  n   6i  ]   �i     j  �   �j  7   k     Pk  �   Vk  |   �k     ol     sl     zl     �l     �l     �l     �l     �l     �l     �l     �l     m     m     m  
   "m  9   -m  	   gm  (   qm  '   �m     �m     �m  D   �m     n     &n     8n     Fn     Rn  H   [n     �n     �n     �n  !   �n     o     o     *o     Fo  +   do  P   �o     �o     �o     �o  U   	p     _p     hp  	   xp     �p  
   �p     �p  N   �p     �p     �p     q  �   q  
   �q     �q     �q     �q  	   �q  ^   �q  f   Xr  �   �r     bs  	   ss     }s     �s  
   �s     �s     �s     �s     �s     �s     �s  	   �s     �s     �s     �s     �s  	   t     t  
   t     t     &t  
   3t     >t     Ot     _t  
   et     pt     �t     �t  	   �t     �t     �t     �t     �t     �t     �t     �t     �t  
   �t     �t     �t  	   �t     	u     u     !u     'u     3u  /   @u     pu     vu     �u     �u     �u     �u  
   �u     �u     �u     �u     �u     �u     �u     �u     v     v     v     "v     (v  	   Bv     Lv     Rv     bv     jv     qv  	   zv     �v     �v     �v     �v     �v     �v     �v     �v     �v     �v     w     	w     w     &w     3w     @w  
   Xw     cw     lw     xw     �w     �w  
   �w     �w     �w  
   �w     �w     �w     �w     �w     �w     x  
   
x     x  
   "x     -x     4x  	   :x     Dx     Qx     Wx     `x     vx     �x  
   �x     �x     �x     �x     �x     �x     �x      y  
   y     y  �  -y  4   {  :   A{  K   |{     �{     �{  ,   �{  ,   |  1   I|  8   {|  R   �|  &   }  1   .}  a   `}     �}     �}     �}  *   �}  q   ~  *   �~     �~     �~     �~     �~  &   �~       %   &     L  5   Z  	   �     �     �     �  <   �     �     '�  P   A�     ��     ��  /   ��     �     ��     �      �     8�  
   J�     U�     a�     h�     }�  )   ��  +   ��  '   �  5   �  .   C�  3   r�  (   ��  6   ς  ,   �     3�     L�     ]�     u�     }�     ��     ��     ��     ��     ʃ     у     ߃     �     ��     �      �  �   %�  �   ��     M�     e�     l�  	   |�     ��     ��     ��     ƅ     �     ��     �  "   1�     T�  :   o�  E   ��     ��     ��     �  
   �     �     &�      6�     W�     `�     p�     ��     ��     ��  0   ��  <   �  *   )�     T�     \�     y�     ��     ��     ��     ��     Έ     �  '   ��  !   #�     E�  9   e�  -   ��     ͉     ߉     ��     �  &   2�     Y�     h�     t�     z�     ��     ��     ��          Ԋ  
   ڊ  1   �     �  
   +�     6�     D�     X�     j�     w�     ��     ��     ��     ��     ��     ֋  6   �  )   )�  �   S�     �     �     ��  
   �     �     1�     @�     O�     ]�     p�     x�     ��     ��     ��       4   ˍ      �     �  	   #�  -   -�     [�  (   q�     ��     ��     ��  (   Ԏ     ��     �      1�     R�  '   k�     ��  /   ��  7   �     �      6�     W�     w�  	   ��     ��     ��     ��     ��  
   ̐  ;   א     �  {   �     ��  6   ��  �   �  	   i�     s�     |�  
   ��  $   ��  !   ��     Ւ     ْ     �     �     �     �     �     $�     2�     @�     N�     \�  %   d�  )   ��     ��     ̓     ܓ     �     ��     ��      �     2�  #   5�     Y�     a�  $   q�     ��  
   ��     ��     Ô     ٔ     �     �     �     $�     6�     J�     [�     k�     {�     ��     ��     ȕ     ڕ     �  
   �     �  	   *�     4�     D�     ]�     k�  �   y�     7�     U�     \�     k�     ~�  	   ��     ��     ��     ϗ     ח     ޗ  4   �      �  	   .�  +   8�     d�      x�     ��     ��     Ϙ     �      �     #�  
   @�  	   K�     U�     ]�     b�     �     ��     ��      ̙     �      �  0   �  5   7�     m�     ��     ��     ��     ��     ǚ  	   ך     �  "   ��     �  1   3�  >   e�     ��     ě     ڛ      ��     �  	   ,�  
   6�     A�  "   F�     i�     ��     ��     ��     ��     Μ     �      ��     �     &�     :�     O�     a�     x�  	   ��     ��     ��     ��  J   ͝     �  Z   *�  )   ��  "   ��  
   Ҟ     ݞ     �  q   ��  �   j�  /   �     #�     5�     D�     T�  /   r�     ��     ��     ڠ  
   �  	   ��      �     �     �     +�  Y   >�  2   ��  -   ˡ  +   ��     %�     :�  .   B�     q�     z�     ��     ��  *   ��  )   ɢ  
   �     ��      �  	   $�     .�     <�     I�     V�     f�     v�     ��  "   ��     ƣ     ̣     أ     �     ��     �     �     1�     O�  U   g�     ��     Τ     ޤ  %   ��     �  	   #�     -�     B�     `�  
   n�     y�     ��     ��     ��     ��     ҥ  
   �     ��     �     �     8�     N�     V�     t�     ��     ��     ��  �   ¦     V�  '   t�  "   ��     ��  	   ק     �     ��     �     #�     8�  "   K�     n�     ��     ��  $   Ũ     �  '   �     +�  /   >�      n�     ��     ��     ǩ      �     �  ,   �     H�  +   c�  4   ��  '   Ī     �     ��     �     	�  *   �     I�      _�     ��     ��     ��     ��     ǫ     ԫ     �     ��     �     �     *�     A�     G�     V�     i�  /   r�     ��     ��  	   ��  
   Ȭ     Ӭ     ߬     �     ��  "   �  #   2�     V�  F   l�  ,   ��  6   �  �  �  ^   �  G   P�     ��  G   ��  )   �  9   �  +   I�  F   u�  N   ��  a   �  �   m�  �   9�  }   �  �   q�  w    �  r   x�  Y   �  �   E�  @   ݶ  �   �  �   ��  �   N�  o   �  X   T�  t   ��  �   "�  o   �  T   u�  �   ʻ  5   ��  a   ��  L   �  O   f�  �   ��  �   B�  n   �  X   ~�  _   ׿  �   7�  �   ��  G   f�     ��  �   ��  u   b�     ��     ��     ��     ��     �     4�     T�     t�     ��     ��     ��     ��     ��     ��     ��  H   ��     ;�  -   M�     {�     ��     ��  J   ��  	   �     �     '�     9�     G�  \   W�     ��     ��     ��  $   ��  "   #�  !   F�     h�  %   ��  :   ��  j   ��     R�     i�     |�  Q   ��  	   ��     ��     ��     �     "�     2�  E   8�  
   ~�     ��     ��  �   ��     q�     ��     ��     ��     ��  b   ��  r   �  �   z�     ;�  
   R�     ]�     i�     q�     ��     ��     ��     ��     ��     ��     ��     ��     ��     ��     ��     ��     �     �     �     "�     1�     >�     R�     c�     l�     |�     ��     ��     ��     ��     ��     ��  	   ��     ��     ��     �  	   �     �  	   *�     4�  
   =�     H�  	   \�     f�     r�     ��  0   ��     ��     ��  
   ��     ��     ��     ��     ��     
�      �     4�     I�  
   Y�     d�     {�     ��     ��     ��     ��     ��     ��     ��     ��     ��     �     �  	   �     "�  
   8�     C�     \�     j�     z�     ��  	   ��     ��     ��     ��     ��     ��     ��     ��  #   	�     -�     =�  
   F�     Q�     d�     x�     �     ��     ��  
   ��     ��     ��     ��     ��     ��     ��     �     �     �     +�     4�     =�     F�     T�  
   [�      f�      ��     ��  
   ��     ��     ��     ��     �     �  �   !�     �  	   $�     .�   		This plugin provides bar for bookmarks.
		 %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i Attachment %i Attachments %i BackLink %i BackLinks %i _Backlink %i _Backlinks %i errors occurred, see log %i file will be deleted %i files will be deleted %i file will be trashed %i files will be trashed %i open item %i open items %i warnings occurred, see log (Un-)indenting a list item also changes any sub-items <Top> <Unknown> A desktop wiki A file with that name already exists. A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Action Add Application Add Bookmark Add Notebook Add bookmark/Show settings Add column Add new bookmarks to the beginning of the bar Add row Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files All Tasks Allow public access Always use last cursor position when opening a page Always wrap at character Always wrap at word boundaries An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Are you sure you want to delete the file '%s'? Arithmetic Ascii graph (Ditaa) Attach File Attach image first Attachment Browser Attachments Attachments: Author Auto
Wrap Auto indenting Automatically collapse sections on close page Automatically expand sections on open page Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave interval in minutes Autosave version on regular intervals Autosave version when the notebook is closed Back to Original Name BackLinks BackLinks Pane Backend Backlinks: Bazaar Bookmarks BookmarksBar Border width Bottom Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Cannot write this file. Probably this is due to the lenght
of the file name, please try using a name with less
than 255 characters Cannot write this file. Probably this is due to the lenght
of the file path, please try using a folder structure resulting in less
than 4096 characters Capture whole screen Center Change columns Changes Characters Characters excluding spaces Check Checkbox '<' Check Checkbox '>' Check Checkbox 'V' Check Checkbox 'X' Check _spelling Check and Update Index Checkbo_x List Checking a checkbox also changes any sub-items Classic trayicon,
do not use new style status icon on Ubuntu Clear Clone row Code Block Column 1 Command Command Palette Command does not modify data Comment Common include footer Common include header Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Copy Email Address Copy Line Copy Template Copy _As... Copy _Link Copy _Location Copy _link to this location Copy link to clipboard Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Created Cu_t Custom Tools Custom _Tools Customize... Date Date and Time... Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Demote Dependencies Description Details Diagram Discard note? Display line numbers Distraction Free Editing Do not use system trash for this notebook Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root Due E_xport... Edit %s Edit Custom Tool Edit Image Edit Link Edit Table Edit _Source Editing Editing file: %s Enable Version Control? Enable plugin Enabled Equation Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File exists, do you want to import? File is not writable: %s File name should not be blank. File name should not contain path declaration. File name too long: %s File path too long: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Fossil GNU R Plot Get more plugins online Get more templates online Git Gnuplot Go back Go forward Go to home page Grid lines Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide Journal pane if empty Hide menubar in fullscreen mode Highlight current line Home Page Horizontal _Line Icon Icons Icons & Text Icons & Text horizontal Id Id "%s" not found on the current page Images Import Page Include horizontal lines in the ToC Include subpages Index Index page Inline Calculator Insert Code Block Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Sequence Diagram Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Key Binding Key bindings Key bindings can be changed by clicking on a field with a key combination
in the list and then press the new key binding.
To disable a keybinding, select it in the list and use <tt>&lt;Backspace&gt;</tt>. Labels marking tasks Large Last Modified Leave Fullscreen Leave link to new page Left Left Side Pane Line Sorter Lines Link Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Managing table columns Map document root to URL Match _case Maximum number of bookmarks Maximum page width Menubar Mercurial Modified Month Move Selected Text... Move Text to Other Page Move column ahead Move column backward Move page "%s" to trash? Move text to Name Need output file to export MHTML Need output folder to export full notebook Never wrap lines New File New Page New Page in %s New S_ub Page... New _Attachment Next No Applications Found No changes since last version No dependencies No document root defined for this notebook No plugin available to display objects of type: %s No such file: %s No such page: %s No such wiki defined: %s No templates installed No text selected Notebook Notebooks OK Only Show Active Tasks Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open cell content link Open help Open in New Window Open in New _Window Open new page Open plugins folder Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" and all of it's sub-pages and
attachments will be deleted.

This deletion is permanent and cannot be un-done. Page "%s" and all of it's sub-pages and
attachments will be moved to your system's trash.

To undo later, go to your system's trashcan. Page "%s" does not have a folder for attachments Page Index Page Name Page Template Page already exists: %s Page is read-only and cannot be edited Page not allowed: %s Page not available: %s Page section Paragraph Password Paste Paste As _Verbatim Path Bar Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please select a row, before you push the button. Please select more than one line of text Please specify a notebook Plugin Plugin "%s" is required to display this object Plugins Port Position in the window Pr_eferences Prefer short link names for pages Prefer short names for page links Preferences Prev Print to Browser Promote Proper_ties Properties Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Remove column Remove links to %s Remove row Removing Links Rename file Rename or Move Page Rename page "%s" Repeated clicking a checkbox cycles through the checkbox states Replace _All Replace with Require authentication Restore page to saved version? Rev Right Right Side Pane Right margin position Row down Row up S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Screenshot Command Search Search _Backlinks... Search this section Section Section(s) to ignore Section(s) to index Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Sequence Diagram Server not started Server started Server stopped Set New Name Set ToC fontsize Set default browser Set default text editor Set to Current Page Show BackLink count in title Show Line Numbers Show Tasks as Flat List Show _Changes Show a separate icon for each notebook Show due date in sidepane Show full Page Name Show full Page Names Show full page name Show helper toolbar Show in the toolbar Show linkmap button in headerbar Show right margin Show tasklist in sidepane Show the cursor also for pages that can not be edited Show the page title heading in the ToC Single _page Size Small Smart Home key Some error occurred while running "%s" Sort alphabetically Sort pages by tags Source View Spell Checker Start Start _Web Server Su_bscript Su_perscript Support thumbnails for SVG Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Tags for non-actionable tasks Task Task List Tasks Template Templates Text Text Files Text From _File... Text background color Text foreground color Text wrap mode The file "%s" exists but is not a wiki page.
Do you want to import it? The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The following parameters will be substituted
in the command when it is executed:
<tt>
<b>%f</b> the page source as a temporary file
<b>%d</b> the attachment directory of the current page
<b>%s</b> the real page source file (if any)
<b>%p</b> the page name
<b>%n</b> the notebook location (file or folder)
<b>%D</b> the document root (if any)
<b>%t</b> the selected text or word under cursor
<b>%T</b> the selected text including wiki formatting
</tt>
 The inline calculator plugin was not able
to evaluate the expression at the cursor. The table must consist of at least on row!
 No deletion done. Theme There are no changes in this notebook since the last version that was saved There was a problem loading this plugin

 This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This page name cannot be used due to a conflicting file in the storage This page name cannot be used due to technical limitations of the storage This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a "path bar" to the top of the window.
This "path bar" can show the notebook path for the current page,
recent visited pages or recent edited pages.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds the page index pane to the main window.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows inserting 'Code Blocks' in the page. These will be
shown as embedded widgets with syntax highlighting, line numbers etc.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin also has properties,
see the notebook properties dialog This plugin cannot be enabled due to missing dependencies.
Please see the dependencies section below for details.

 This plugin opens a search dialog to allow quickly executing menu entries. The search dialog can be opened by pressing the keyboard shortcut Ctrl+Shift+P which can be customized via Zim's key bindings preferences. This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a macOS menubar for zim. This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a sequence diagram editor for zim based on seqdiag.
It allows easy editing of sequence diagrams.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This plugin turns one section of the notebook into a journal
with a page per day, week or month.
Also adds a calendar widget to access these pages.
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox '<' Toggle Checkbox '>' Toggle Checkbox 'V' Toggle Checkbox 'X' Toggle _Editable Toggle editable Tool Bar Toolbar size Toolbar style Top Top Pane Trash Page Trash failed, do you want to permanently delete instead ? Tray Icon Try wrap at word boundaries or character Turn page name into tags for task items Type Un-check Checkbox Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unkown Image type Unkown Object Unspecified Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use a page for each Use date from journal pages Use font color for dark theme Use horizontal scrollbar (may need restart) Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Use thumbnails Username Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log View debug log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s With this plugin you can embed a 'Table' into the wiki page. Tables will be shown as GTK TreeView widgets.
Exporting them to various formats (i.e. HTML/LaTeX) completes the feature set.
 Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Your system encoding is set to %s, if you want support for special characters
or see errors due to encoding, please ensure to configure your system to use "UTF-8" Zim Desktop Wiki Zoom _Out _About _Add _All Panes _Arithmetic _Attachment... _Back _Browse _Bugs _Cancel _Checkbox _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Delete... _Discard Changes _Duplicate Line _Edit _Edit Link _Edit Link or Object... _Edit Properties _Edit... _Emphasis _FAQ _File _Find _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump _Jump To... _Keybindings _Limit search to the current page and sub-pages _Link _Link to date _Link... _Mark _More _Move _Move Here _Move Line Down _Move Line Up _New Page Here... _New Page... _Next _Next in Index _None _Normal Size _Numbered List _OK _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Previous in Index _Print _Print to Browser _Properties _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove _Remove Heading _Remove Line _Remove Link _Remove List _Rename or Move Page... _Rename... _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In as due date for tasks as start date for tasks calendar:week_start:0 do not use horizontal lines macOS Menubar no grid lines readonly seconds translator-credits vertical lines with lines {count} of {total} Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2021-09-21 20:36+0000
Last-Translator: Alex Gabilondo <alexgabi@disroot.org>
Language-Team: Basque <https://hosted.weblate.org/projects/zim/master/eu/>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.9-dev
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
 		Plugin honek laster marken barra eskaintzen du.
		 %(cmd)s-k
zeroa ez den irteera-egoera bueltatu du %(code)i %(n_error)i errore eta %(n_warning)i alerta gertatu dira, ikusi erregistroa %A %d %B %Y %i eranskina %i eranskinak %i Bueltako esteka... %i Bueltako estekak... %i_Bueltako esteka... %i_Bueltako estekak... %i errore gertatu dira, ikusi egunkari fitxategia %i fitxategia ezabatuko da %i fitxategiak ezabatuko dira %i fitxategia zakarrontzira eramango da %i fitxategiak zakarrontzira eramango dira %i elementu irekia %i elementu irekiak %i alarma gertatu dira, ikusi egunkari fitxategia Zerrenda baten elementu bati koska jarriz (edo kenduz) gero azpiko elementuei ere aplikatuko zaie <Goia> <Ezezaguna> Mahaigaineko wikia Badago izen berdineko beste fitxategi bat. <b>"%s"</b> izeneko fitxategia badago lehendik.
Beste izen bat erabili dezakezu edo dagoen fitxategia gainidatzi. Taulak gutxienez zutabe bat izan behar du. Ekintza Gehitu aplikazioa Gehitu laster-marka Gehitu kodadernoa Gehitu laster-marka/Erakutsi ezarpenak Gehitu zutabea Gehitu laster-marka barraren hasieran Gehitu lerroa Gtkspell bidezko zuzenketa ortografikoa gehitzen du.
 Lerrokatu Fitxategi guztiak Zeregin guztiak Onartu sarbide publikoa Erabili beti kurtsorearen azken kokapena orri bat irekitzean Bildu beti karakterea Bildu beti hitzen mugetan Errore bat gertatu da irudia sortzean.
Nahi duzu hala ere iturburu-testua ikusi? Oharraren orriaren jatorria Aplikazioak Ziur zaude '%s' fitxategia ezabatu nahi duzula? Aritmetikoa Ascii grafikoa (Ditaa) Erantsi fitxategia Aurretik irudia erantsi Eranskina arakatu Eranskinak Eranskinak: Egilea Automatikoki
egokitu Koska automatikoa Tolestu atalak automatikoki orria ixtean. Zabaldu atala automatikoki orria irekitzean Zim-entzat bertsioa automatikoki gordea Hautatu uneko hitza automatikoki formatua aplikatzean Bihurtu "CamelCase" hitzak esteka automatikoki Bihurtu fitxategien bide-izenak esteka automatikoki Gordetze automatikoaren tartea minututan Gorde automatikoki bertsioa maiztasun erregularrarekin Gorde bertsioa automatikoki koadernoa ixtean Atzera jatorriko izenera Bueltako estekak Bueltako esteken panela Motorra Bueltako estekak: Bazaar Laster-markak Laster marken barra Ertzaren zabalera Behean Beheko panela Arakatu Zerrenda buletaduna K_onfiguratu Ezin da %s orria aldatu Utzi Ezin da idatzi fitxategi hau. Seguru aski arrazoia fitxategiaren
izenaren luzera da, saiatu 255 baino karaktere gutxiagoko
izen bat erabiltzen. Ezin da fitxategi hau idatzi. Seguru aski fitxategiaren bidearen
luzera da arrazoia, saiatu 4096 baino karaktere gutxiagoko
karpeta-bide bat erabiltzen Kapturatu pantaila osoa Erdian Aldatu zutabeak Aldaketak Karaktereak Karaktere tarteak salbu Markatu Kontrol-laukian '<' Kontrol-laukian markatu '>' Kontrol-laukian markatu 'V' Kontrol-laukian markatu 'X' Zuzenketa ortografikoa Egiaztatu eta eguneratu aurkibidea Zerrenda kontrol-laukiduna Kontrol-laukia markatuta azpi-elementuak ere aldatzen dira Erretiluko ikono klasikoa,
ez erabili estilo berriko ikonorik Ubuntun Garbitu Kopiatu lerroa Kode blokea 1. zutabea Agindua Command Palette Aginduak ez ditu datuak aldatzen Iruzkina Orri-oina barne Izenburua barne Koaderno _osoa Konfiguratu aplikazioak Konfiguratu plugina Konfiguratu aplikazio bat "%s" lotura irekitzeko Konfiguratu aplikazio bat "%s" motako
fitxategiak irekitzeko Hartu kontrol-lauki guztiak zeregin moduan Kopiatu Kopiatu helbide elektronikoa Kopiatu lerroa Kopiatu txantiloia Gorde honela... Kopiatu _esteka Kopiatu _kokalekua Kopiatu _esteka hona Kopiatu esteka arbelera Ezin izan da "%s" exekutagarria aurkitu Ezin izan da koadernoa topatu: %s Ezin da "%s" txantiloia aurkitu Ezin izan da topatu koaderno honen fitxategia edo karpeta Ezin izan da kargatu zuzentzaile ortografikoa Ezin da ireki: %s Ezin izan da espresioa ebatzi Ezin izan da irakurri: %s Ezin da orri hau gorde: %s Sortu orri berria ohar bakoitzarentzat Karpeta sortu? Sortze-data Ebaki Tresna pertsonalizatuak _Tresna pertsonalizatuak Pertsonalizatu... Data Data eta ordua... Eguna Lehenetsia Testua arbelera kopiatzeko lehenetsitako formatua Koaderno lehenetsia Atzerapena Ezabatu orria Ezabatu "%s" orria? Errenkada Ezabatu Jaitsi maila Mendekotasunak Deskribapena Zehaztasunak Diagrama Baztertu oharra? Bistaratu lerroen zenbakiak Distraziorik gabeko edizioa Ez erabili sistemaren zakarrontzia koaderno honetarako Nahi duzu laster-marka guztiak ezabatzea? Leheneratu nahi duzu orria: %(page)s
gordetako bertsiora: %(version)s ?

Gordetako azken bertsioaren ondorengo aldaketa guztiak galduko dira! Dokumentu erroa Epemuga E_sportatu... Editatu %s Editatu tresna pertsonalizatua Editatu irudia Editatu esteka Editatu taula Editatu _iturburua Edizioa Fitxategia editatzen: %s Gaitu bertsioen kontrola? Gaitu plugin-a Gaituta Ekuazioa Errorea %(file)s at line %(line)i near "%(snippet)s" Ebaluatu matematika Zabaldu guztia Esportatu Esportatu orri guztiak fitxategi bakar batean Esportazioa burutu da Esportatu orri bakoitza fitxategi batean Koadernoa esportatzen Huts egin du Errorea %s exekutatzen Huts egin du aplikazioa exekutatzeak: %s Fitxategia badago lehendik Fitxategi-txantiloiak... Fitxategia diskoan aldatu da: %s Fitxategia existitzen da Fitxategia badago.
Inportatu nahi duzu? Fitxategia ezin da idatzi: %s Fitxategiaren izenak ez luke hutsik egon behar. Fitxategiaren izenak ez du bide-adierazpena izan behar. Fitxategi izena luzeegia: %s Fitxategiaren bidea luzeegia: %s Fitxategi mota ez baliozkoa: %s Fitxategi-izena Iragazkia Bilatu Bilatu hurrengoa Bilatu aurrekoa Bilatu eta ordeztu Zer bilatu Markatu egitekoak asteburu aurreko astelehen eta ostegunean Karpeta Karpeta hori badago eta edukia du, horra esportatzen baduzu dauden fitxategiak gainidatz ditzakezu. Nahi duzu aurrera egin? Karpeta existitzen da: %s Erantsitako fitxategientzat txantiloiak dituen karpeta Bilaketa aurreratuak egiteko AND, OR eta NOT bezalako eragileak
erabili ditzakezu. Ikusi laguntzako orria xehetasun gehiagorako. For_matua Formatua Fosila GNU R Plot Eskuratu plugin gehiago Internetetik Eskuratu sareko txantiloi gehiago Git Gnuplot Atzera Aurrera Joan hasierako orrira Saretako marrak _1. izenburua _2. izenburua _3. izenburua _4. izenburua _5. izenburua Altuera Ezkutatu egunkari-panela hutsa badago Ezkutatu menu-barra pantaila osoko moduan Nabarmendu uneko lerroa Hasierako orria _Lerro horizontala Ikonoa Ikonoak Ikonoak eta testua Ikonoak eta testua horizontalean Id "%s" IDa ez da aurkitu uneko orrian Irudiak Inportatu orria Sartu lerro horizontalak aurkibidean Azpi-orriak barne Aurkibidea Aurkibide-orria Lineako kalkulagailua Txertatu kode blokea Sartu data eta ordua Txertatu grafikoa Txertatu Ditaa Txertatu ekuazioa Txertatu GNU R Plot Txertatu Gnuplot Txertatu irudia Txertatu esteka Txertatu puntuazioa Txertatu pantaila argazkia Txertatu sekuentzien diagrama Txertatu sinboloa Txertatu taula Txertatu testua fitxategitik Interfazea Interwikiaren gako-hitza Egunkaria Jausi egin hona Jausi egin orri honetara Laster-teklak Laster-teklak Laster-teklak alda daitezke, horretarako tekla konbinazioa dagoen eremuan
sakatu laster-tekla berria.
Laster-tekla desgaitzeko, hautatu zerrendan eta erabili  <tt>&lt;Atzera-tekla&gt;</tt>. Zereginak markatzeko etiketak Handia Azken aldaketa Utzi pantaila osoa Utzi esteka bat orri berrira Ezkerrean Ezker aldeko panela Lerroen ordenatzailea Lerroak Esteka Esteken mapa Estekatu fitxategiak fitxategien bide-izen osoarekin Estekatu hona Kokalekua Egin aldaketen egunkaria Zeitgeist-en bidez Egunkari-fitxategia Errore bat aurkitu duzula dirudi Bihurtu aplikazio lehenetsi Taularen zutabeak kudeatu Esleitu dokumentu-erroa URLari Maiuskula/minuskula Laster-marken gehienezko kopurua Orriaren gehienezko zabalera Menu-barra Mercurial Aldatua Hila Mugitu hautatutako testua... Mugitu testua beste orri batera Mugitu zutabea aurrera Mugitu zutabea atzera "%s" orria zakarrontzira eraman? Mugitu testua hona Izena Helburuko fitxategia behar da MHTML esportatzeko Helburuko karpeta behar da koaderno osoa esportatzeko Ez bildu lerroak inoiz Fitxategi berria Orri berria Orri berrian %s-n _Azpiorri berria... Eranskin berria Hurrengoa Ez da aplikaziorik aurkitu Aldaketarik ez azkeneko bertsiotik Menpekotasunik gabe Koaderno honek ez dauka sustraia den dokumenturik Ez dago %s motako objektuen bistaratzeko plugin erabilgarririk Ez da fitxategia existitzen: %s Ez dago horri hau: %s Definitu gabeko wikia: %s Ez dago txantiloirik instalatuta Ez da testurik hautatu Koadernoa Koadernoak Ados Erakutsi bakarrik zeregin aktiboak Ireki _eranskinen karpeta Ireki karpeta Irekin koadernoa Ireki honekin... Ireki dokumentuaren e_rroa Ireki _koadernoaren karpeta Ireki orria Ireki gelazkako edukiaren esteka Ireki laguntza Ireki leiho berrian Ireki _leiho berrian Ireki orri berria Ireki pluginen karpeta Ireki honekin: %s Aukerakoa %s pluginaren aukerak Bestelakoa... Irteerako fitxategia Helburuko fitxategia badago, zehaztu "--gainidatzi" esportazioa behartzeko Irteerako karpeta Helburuko karpeta badago eta ez dago hutsik, zehaztu "--gainidatzi" esportazioa behartzeko Helburuko kokalekua behar da esportatzeko Irteerak ordeztuko du hautatutakoa Gainidatzi _Bide-izenaren barra Orria "%s" orria eta bere azpi-orri
eta eranskin guztiak ezabatuko dira.

Ezabatze hau betirako da eta ezin da desegin. "%s" orria eta bere azpi-orri eta eranskin
guztiak zakarrontzira eramango dira.

Geroago desegiteko, joan zure sistemaren zakarrontzira. "%s" orriak ez dauka eranskinentzako karpetarik Orrien aurkibidea Orriaren izena Orri-txantiloia Orria lehendik ere badago: %s Orria irakurtzeko soilik da eta ezin da editatu Baimenik gabeko orria: %s Orria ez dago erabilgarri: %s Orriaren sekzioa Paragrafoa Pasahitza Itsatsi Itsatsi hitzez hitz Bidearen barra Iruzkindu bertsioa Existitzen ez den orri batera eskeka bat sortuz gero
automatikoki orri berria sortuko da. Hautatu izen bat eta karpeta bat koadernoarentzat. Hautatu errenkada bat botoia sakatu aurretik. Hauta ezazu testuko lerro bat baino gehiago Zehaztu koaderno bat Plugina "%s" plugina behar da objektu hau bistaratzeko Pluginak Ataka Kokalekua leihoan _Hobespenak Orrietarako esteka laburren izenak hobetsi Orrietarako esteken izen laburrak hobetsi Hobespenak Aur. Inprimatu nabigatzailearen bidez Igo maila Propie_tateak Propietateak Ohar azkarra Ohar azkarra... Azken aldaketak Azken aldaketak... Azkena a_ldatutako orriak Birformateatu wiki-markak zuzenean Kendu Kendu denak Kendu zutabea Kendu estekak %s-ra Kendu lerroa Estekak ezabatzen Berrizendatu fitxategia Berrizendatu edo mugitu orria Berrizendatu "%s" orria Kontrol laukian behin eta berriz klik egiteak dituen egoera desberdinak aldatzen ditu Ordeztu _guztiak Ordeztu honekin Autentifikazioa eskatu Leheneratu orria gordetako bertsiora? Berrik. Eskuinean Eskuin aldeko panela Eskuineko marginaren posizioa Jaitsi lerroa Igo lerroa Gorde bertsioa... Gorde _kopia bat... Gorde kopia bat Gorde bertsioa Gorde laster-markak Bertsioa gordeta zim-entzat Puntuazioa Pantaila argazkiaren agindua Bilatu Bilatu bueltako estekak... Bilatu sekzio honetan Sekzioa Baztertu beharreko sekzioa(k) Indexatu beharreko atala(k) Hautatu fitxategia Hautatu karpeta Hautatu irudia Aukeratu bertsio bat ikusteko bertsio horren eta uneko bertsioaren arteko aldaketak.
Edo hautatu hainbat bertsio horien arteko aldaketak ikusteko.
 Hautatu esportatzeko formatua Hautatu irteerako fitxategi edo karpeta Hautatu esportatu beharreko orriak Hautatu leihoa edo area Hautapena Sekuentzien diagrama Zerbitzaria ez da abiatu Zerbitzaria abiatua Zerbitzaria geldirik Ezarri izen berria Ezarri aurkibidearen letra tamaina Ezarri nabigatzaile lehenetsia Ezarri testu editore lehenetsia Ezarri uneko orrialdera Erakutsi BackLink kopurua izenburuan Erakutsi lerro zenbakiak Erakutsi zereginak zerrenda laua moduan Erakutsi aldaketak Erakutsi aparteko ikono bat koaderno bakoitzeko Erakutsi epemuga alboko panelean Erakutsi orriaren izen osoa Erakutsi orriaren izen osoa Erakutsi orriaren izen osoa Erakutsi laguntzako trenen barra Erakutsi tresna-barran Erakutsi esteka-mapa botoia goiburuko barran Erakutsi eskuineko margina Erakutsi zereginen zerrenda alboko panelean Erakutsi kurtsorea baita editatu ezin diren orrietan Erakutsi orriaren izenburua aurkibidean Orri _bakarra Tamaina Txikia Abio-tekla adimendua Erroreren bat gertatu da "%s" exekutatzean Ordenatu alfabetikoki Ordenatu orriak etiketen arabera Iturburua ikusi Ortografia zuzentzailea Hasi Abiarazi _web-zerbitzaria Azpi-indizea Goi-indizea SVGrako koadro txikiak onartu Si_nboloa... Sintaxia Sistemaren lehenetsia Tabulazioaren zabalera Taula Taula-editorea Edukien aurkibidea Etiketak Exekutagarriak ez diren egitekoentzako etiketak Zeregina Zereginen zerrenda Zereginak Txantiloia Txantiloiak Testua Testu-fitxategiak Testua fitxategitik... Testuaren atzeko planoaren kolorea Testuaren aurreko planoaren kolorea Testua biltzeko modua "%s" fitxategia badago baina ez da wiki-orri bat.
Inportatu nahi duzu? %s karpeta
ez dago.
Nahi duzu
orain sortzea? "%s" karpeta honezkero ez dago.
Nahi duzu orain sortu? Komandoa exekutatzen denean bertan
hurrengo parametroak aldatuko dira:
<tt>
<b>%f</b> orriaren jatorria behin behineko fitxategi gisa
<b>%d</b>uneko orriaren eranskinen direktorioa
<b>%s</b> orriaren jatorrizko benetako fitxategia (balego)
<b>%p</b> orriaren izena
<b>%n</b> koadernoaren kokalekua (fitxategia edo karpeta)
<b>%D</b> dokumentu-erroa (balego)
<b>%t</b> kurtsore azpian hautatuta dagoen testua edo hitza
<b>%T</b> hautatutako testua wiki formatua barne
</tt>
 Lineako kalkulagailua plugina ez da gai izan
kurtsoreak markatzen duen espresioa kalkulatzeko. Taulak gutxienez errenkada bat izan behar du!
 Ezabaketa ez da gauzatu. Gaia Azkeneko bertsioa gorde zenetik ez da aldaketarik egon koaderno honetan Arazo bat egon da plugin hau kargatzean

 Fitxategia dagoeneko existitzen da.
Gainidatzi nahi duzu? Orri honek ez du eranskinentzako karpetarik Orri hau ezin da erabili biltegian dagoen fitxategi gatazkatsua delako Orriaren izen hau ezin da erabili biltegiratzearen muga teknikoak direla medio Plugin honek pantaila argazkia egin eta
zim orrian txertatzen du.

Hau zim-en plugin propioa da.
 Plugin honek "bidea barra" gehitzen du leihoaren goiko aldean.
"Bidea barra"-k erakutsi dezake uneko orriak koadernoan duen kokalekua,
duela gutxi bisitatutako orriak edo duela gutxi editatutako orriak.
 Plugin honek koadernoan irekitako zereginen zerrenda gehitzen du.
Zereginak ireki daitezke kontrol laukien bidez edo "TODO" zein "FIXME" 
etiketen bidez.

Hau zim-en plugin propioa da.
 Plugin honek testua edo arbelaren edukia zim orri batera
azkar jaregiteko aukera gehitzen du.

Hau zim-en plugin propioa da.
 Plugin honek sistemaren erretiluan ikono bat gehitzen du aplikazioa azkar irekitzeko.
Plugin honek Gtk+ 2.10 bertsioa edo berriagoa behar du.
 Plugin honek uneko orriari lotutako orrien zerrenda
erakusten duen trepeta gehitzen du.

Hau zim-en plugin propioa da.
 Plugin honek edukien aurkibidea erakusten duen
trepeta gehitzen dio uneko orriari.

Hau zim-en plugin propioa da.
 Plugin honek zim distraziorik gabeko editore
moduan erabiltzeko ezarpenak gehitzen ditu.
 Plugin honek 'Txertatu sinboloa' eginbidea gehitzen du
eta karaktere tipografikoak auto-formateatzeko aukera ematen du.

Hau zim-en plugin propioa da.
 Plugin honek leiho nagusiari orrien atalen panela gehitzen dio.
 Plugin honek koadernorako bertsio kontrola gehitzen du.

Plugin honek Bazaar, Git eta Mercurial bertsio kontrolak onartzen ditu.
 Plugin honek aukera ematen du orrian "Kode blokeak" txertatzeko. Bloke
hauek bistaratuko dira kapsulatutako trepetak bezala sintaxia nabarmendua, lerro zenbakiak, eta abar.
 Plugin honek zim-en kalkulu aritmetikoak egiteko aukera ematen dizu.
http://pp.com.mx/python/arithmetic orriaren
aritmetika moduluan oinarritzen da.
 Plugin honen bidez azkar ebatsi daitezke 
zim-eko espresio matematiko sinpleak.

Hau zim-en plugin propioa da.
 Plugin honek ere propietateak ditu,
ikusi koadernoaren propietateen elkarrizketa-koadroa Plugin hau ezin da gaitu falta diren mendekotasunengatik.
Xehetasunak ikusteko, ikusi beheko mendekotasunen atala.

 Plugin honek bilaketa-koadro bat irekitzen du menu-sarrerak azkar exekutatzeko. Bilaketaren elkarrizketa-koadroa ireki daiteke Ctrl + Maius + P laster-teklak sakatuta eta Zim-en laster-teklen hobespenetan pertsonaliza daiteke. Plugin honen zim-entzako Ditaa-n oinarritutako diagrama editorea eskaintzen du.

Hau zim-en plugin propioa da.
 Plugin honen zim-entzako GraphViz-en oinarritutako diagrama editorea eskaintzen du.
 Plugin honek sortzen du koadernoaren esteken
egitura irudikatzen duen koadro bat.
Orrien harremana erakusten duen
"ideia-mapa" moduan erabili daiteke.

Hau zim-en plugin propioa da.
 Plugin honek zim-erako macOS menu-barra erakusten du. Plugin honek hautatutako etiketen arabera iragazitako orrien indizea hodei batean eskaintzen du.
 Plugin honen zim-entzat GNU R-en oinarritutako traza editorea eskaintzen du
 Plugin honek zim-entzat Gnuplot-en oinarritutako traza editorea eskaintzen du.
 Plugin honek zim-entzat seqdiag-en oinarritutako diagrama editorea eskaintzen du.
Sekuentzien diagramak erraz editatzeko aukera ematen du.
 Plugin honek Zim-en inprimatzeko gaitasun faltarako
soluzio bat ematen du. Uneko orria html formatura
esportatu eta nabigatzailean irekitzen du. Suposatzen
da nabigatzaileak inprimatzeko gaitasuna duela.
 Plugin honek zim-entzat latex-en oinarritutako ekuazio editorea eskaintzen du.

Hau zim-en plugin propioa da.
 Plugin honek zim-entzat GNU Lilypond-en oinarritutako puntuazio editorea eskaintzen du.
 Plugin honen bidez uneko orriaren eranskinen karpeta
goiko paneleko ikono bezala ikusiko duzu.
 Plugin honek hautatutako lerroak alfabetikoki ordenatzen ditu.
Zerrenda lehendik ordenatuta badago ordena alderantzikatzen du.
(A-Z > Z-A).
 Plugin honek koadernoaren sekzio bat egunkaria bihurtzen du
eguneko, asteko edo hilabeteko orri banarekin.
Gainera orri hauek atzitzeko trepeta bat gehitzen du.
 Honek normalean esan nahi du fitxategiak karaktere baliogabeak dauzkala Titulua Aurrera egiteko orri honen kopia bat gorde dezakezu edo
aldaketa guztiak baztertu. Kopia bat gordeta aldaketak galduko 
dira baina beranduago kopia leheneratzen ahal duzu. Koaderno bat sortzeko karpeta huts bat hautatu behar duzu.
Edo dagoen Zim koaderno baten karpeta hauta dezakezu ere.
 Edukien aurkibidea Gaur Gaur Jarri/kendu '<' kontrol laukian Aldadatu kontrol-laukia '>'-ra Jarri/kendu 'V' kontrol laukian Jarri/kendu 'X' kontrol laukian Editagarria Editagarria Tresna-barra Tresna-barraren tamaina Tresna-barraren estiloa Goian Goiko panela Zakarrontziaren orria Zaborrontziak huts egin du. Horren ordez behin betiko ezabatu nahi duzu? Erretiluko ikonoa Saiatu hitzen mugetan edo karakterean biltzen Bihurtu orriaren izena etiketa Mota Kendu kontrol-laukiaren marka Kendu koska atzera-teklaz
(desgaituta badago <Maius><Tab> erabili dezakezu Ezezaguna Irudi mota ezezaguna Objektu ezezaguna Zehaztu gabea Etiketa kenduta Eguneratu orri honi linkatuta dagoen %i orria Eguneratu orri honi linkatuta dauden %i orriak Eguneratu orri honen izenburua Estekak eguneratzen Aurkibidea eguneratzen Erabili %s aldatzeko alboko panelera Erabili letra mota pertsonalizatua Erabili orri bana bakoitzarentzat Erabili egunkari-orrien datak Erabili letra kolorea gai ilunetarako Erabili korritze-barra horizontala (berrabiarazi behar da) Erabili <Sartu> tekla esteka segitzeko
(Erabilgarri ez badago saia zaitezke <Alt><Sartu> konbinazioarekin) Koadro txikiak erabili Erabiltzaile izena Bertsio-kontrola Une honetan bertsioen kontrola ez dago gaituta koaderno honetan.
Gaitu nahi duzu? Bertsioak Ikusi oharra Ikusi _egunkaria Ikusi arazketa erregistroa Web zerbitzaria Astea Errore honen berri ematean txertatu
azpiko testu-kuadroko informazioa Hitz osoak Zabalera Wiki orria: %s Plugin honen bidez taula bat txerta dezakezu wiki orrian. Taulak GTK TreeView trepeta moduan erakutsiko da.
Taula hainbat formatutan (e.b. HTML/LaTeX) esportatzeko aukerak osatzen du plugin honen gaitasuna.
 Hitz zenbaketa Kontatu hitzak... Hitzak Urtea Atzo Fitxategia kanpoko aplikazio batez editatzen ari zara. Elkarrizketa hau ixten ahal prest dagoenean Tresnen menuan eta tresna-barran edo testuinguruko menuetan
agertuko diren tresna pertsonalizatuak ezar ditzakezu. Zure sistemaren kodeketa %s da, karaktere berezientzako laguntza nahi baduzu
edo kodeketaren ondoriozko akatsak ikusten badituzu, ziurtatu zure sistema "UTF-8" erabiltzeko konfiguratzen duzula Zim Mahaigaineko Wikia Txikiagotu Honi _buruz _Gehitu Panel _guztiak _Aritmetikoa _Eranskinak... At_zera _Arakatu Erroreak _Utzi _Kontrol-laukia _Umea Garbitu formatua It_xi _Tolestu guztiak _Edukiak Kopiatu _Kopiatu hemen Ezabatu _Ezabatu orria _Ezabatu ... _Baztertu aldaketak _Bikoiztu lerroa _Editatu _Editatu esteka _Editatu esteka edo objektua... _Editatu propietateak _Editatu... _Etzana _Ohiko galderak _Fitxategia _Bilatu Bilatu... _Aurrera _Pantaila osoan _Joan _Laguntza _Nabarmendua _Historia _Hasiera _Irudia... _Inportatu orria... _Txertatu _Jauzi egin _Saltatu hona... _Laster-teklak _Mugatu bilaketa uneko orrira eta azpi-orrietara _Esteka Data estekatu _Esteka... Nabarmendua Info _Mugitu _Eraman hona _Mugitu lerroa behera _Mugitu lerroa gora Orri berria hemen... Orri _berria... _Hurrengoa _Hurrengoa aurkibidean _Bat ere ez Tamaina _normala Zerrenda numeratua _Ados _Ireki _Ireki beste koaderno bat... _Besteak... _Orria Orriaren hierarkia _Gurasoa Itsatsi _Aurrebista _Aurrekoa _Aurrekoa aurkibidean _Inprimatu Inprimatu nabigatzailera _Propietateak Ohar azkarra... _Irten _Azken orriak _Berregin _Adierazpen erregularra _Birkargatu _Kendu _Kendu goiburu-estiloa _Ezabatu lerroa _Kendu esteka _Kendu zerrenda _ Izena aldatu edo mugitu orria ... Aldatu izena... _Ordeztu Ordeztu... Berrezarri tamaina Leheneratu bertsioa _Gorde _Gorde kopia bat _Pantaila-argazkia... _Bilatu _Bilatu... _Bidali honi... _Alboko panelak _Alboz albo Ordenatu lerroak Marratua Lodia Azpi-indizea Goi-indizea _Txantiloiak _Tresnak _Desegin Aipamena _Bertsioak... _Ikusi Handiagotu zereginaren epemuga eguna moduan zereginaren hasierak data moduan egutegia:aste_hasiera:0 ez erabili lerro horizontalak macOS menu-barra Saretako marrarik gabe irakurtzeko soilik segundu Launchpad Contributions:
  3ARRANO.com https://launchpad.net/~3arrano-3arrano
  Alexander Gabilondo https://launchpad.net/~alexgabi
  Ibai Oihanguren Sala https://launchpad.net/~ibai-oihanguren
  gorkaazk https://launchpad.net/~gorkaazkarate marra bertikalak lerroekin {total}-tik {count} 