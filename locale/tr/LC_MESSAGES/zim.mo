��    �     �              �  .   �     �  0   �     �            i   )     �     �  V   �  	     	          3   /  Y   c     �  
   �     �     �     �               )  $   0  ?   U  /   �  (   �  %   �               #     /     6  
   C     N     f     m     �  
   �     �     �  <   �     �     �     �           $      7      N   +   _   3   �       �      �      �      !  
   !     !     '!     F!  3   b!     �!     �!     �!     �!     �!     "     "     #"     0"     >"     K"     P"     T"  0   \"     �"     �"     �"     �"     �"     �"     �"     �"  ~   �"     p#  
   ~#     �#  
   �#  	   �#     �#     �#     �#     �#     �#     �#     $     $     $     *$     1$     D$     b$     n$     �$     �$     �$     �$     �$     �$     �$  
   �$     �$     
%  	   %     %%  v   ,%     �%  c   �%     &     !&     (&     B&     F&  
   N&  
   Y&  
   d&  
   o&  
   z&     �&     �&  	   �&     �&     �&     �&     �&  
   �&     �&     �&     '     '     "'     2'     D'     S'     `'     l'     ~'     �'  	   �'     �'     �'     �'     �'     �'     �'     �'     (     $(     0(     6(  2   ?(     r(     z(     �(     �(     �(     �(     �(     �(     �(     )     )     )     5)     B)     G)     P)     Y)     j)     �)     �)     �)     �)     �)     �)  	   �)     *     *     *     **     8*     E*     Y*  
   o*     z*     �*     �*     �*     �*     �*     �*     �*  '   �*  	   +     +     (+  0   -+  	   ^+     h+  	   v+  '   �+  V   �+  3   �+     3,     :,     B,     G,     ^,     k,     w,     �,     �,  
   �,  
   �,     �,     �,     �,     �,     �,     -     (-     9-     F-     S-     r-     v-     �-     �-  	   �-     �-     �-     �-     �-     �-     �-     .     .  �   .     �.      �.     �.     /  	   /     %/     8/     G/  2   V/     �/  &   �/     �/  5   �/     0     0  &   0     A0     U0     h0     v0  
   �0     �0     �0     �0     �0  	   �0     �0  	   �0     �0  
   �0     �0     �0     1  ?   *1  A   j1  �  �1  S   p3  K   �3  @   4  6   Q4  -   �4  x   �4  �   /5  �   �5  �   }6  }   7  �   �7  �   8  �   �8  }   J9  h   �9  k   1:  �   �:  ;   q;  =   �;    �;  j   �<     j=  7   �=     ">  �   (>     �>     �>     �>     �>     �>  	   ?  '   ?     4?  D   9?     ~?     �?  H   �?     �?     �?     @     @     (@  P   <@     �@  U   �@     �@     �@  	   A  
   A     !A  N   &A     uA     �A     �A  
   �A     �A     �A     �A  	   �A  ^   �A  f   "B     �B  	   �B     �B  
   �B     �B     �B     �B     �B     �B     �B     �B     �B  	   C     C  
   C     C     'C     4C     EC  
   KC     VC     nC  	   C     �C     �C     �C     �C     �C     �C     �C  
   �C     �C     �C  	   �C     �C     �C     �C     D     D     D     %D     .D     4D     :D  
   @D     KD     XD     ^D     dD     qD     �D     �D  	   �D     �D     �D     �D     �D  	   �D     �D     �D     �D     �D     E     E     !E     )E     6E     ?E     KE     WE     hE  
   nE     yE     �E  
   �E     �E     �E     �E     �E     �E     �E  
   �E     �E  
   �E      F     F  	   F     F     $F     *F     3F     IF     RF     ZF  �  mF  ?   NH     �H  +   �H  !   �H  	   �H     �H  w   I     |I     �I  p   �I     J     J     $J  ?   =J  ]   }J     �J  	   �J  
   K     K     K     5K     ;K     BK  +   HK  L   tK  M   �K  5   L  8   EL     ~L     �L  	   �L     �L     �L     �L     �L     �L  !   �L     M     M     M     .M     CM     XM     `M     fM  
   �M     �M     �M     �M  5   �M  5   �M  0   2N     cN     |N     �N     �N     �N  &   �N     �N  .   O     3O     DO     ]O     lO  %   �O  "   �O     �O     �O     �O     �O     P     P     P  1   P     JP     cP     kP     xP     �P  
   �P     �P     �P  �   �P     �Q     �Q     �Q     �Q     �Q     �Q  
   R     R  '   $R     LR     RR     `R     rR     R     �R  $   �R  $   �R     �R     S  #   S     8S  !   ES     gS  
   �S     �S     �S     �S     �S     �S     �S     �S  �   �S     aT  }   uT     �T     �T  &   U     )U     -U     5U     AU     MU     YU     eU  
   qU     |U     �U     �U     �U     �U     �U     �U     �U     �U     V  
   !V     ,V     9V     KV     XV     eV     uV     �V     �V     �V  *   �V     �V     �V     �V     W     !W  $   5W     ZW     hW     �W     �W  O   �W     �W      X     X  '   X     >X  (   WX     �X     �X     �X     �X     �X     �X     �X     	Y  
   Y  
   Y     "Y     2Y     FY     ZY     mY     �Y     �Y     �Y     �Y     �Y     �Y     �Y  
   Z     Z     Z     3Z  
   LZ     WZ     kZ     zZ     �Z     �Z  	   �Z     �Z     �Z  -   �Z     [     ![     /[  /   5[  
   e[     p[     [  %   �[  n   �[  -   \     K\  
   S\     ^\     c\  
   x\     �\     �\     �\     �\     �\     �\     �\     �\     �\     ]  7   0]     h]  "   �]     �]     �]  0   �]     �]     �]     ^     #^     >^     N^  "   _^     �^     �^     �^  
   �^     �^     �^  �   �^     �_  +   �_  *   �_     �_     `     `     .`     C`  2   U`     �`  )   �`     �`  2   �`     a     &a  +   ,a     Xa  #   qa     �a     �a     �a     �a     �a  	   �a     �a     �a     b  
   b     !b     3b     Db     ]b     tb  F   �b  8   �b  �   c  ^   �d  H   e  F   ce  4   �e  %   �e  �   f    �f  �   �g  �   Th  �   i  �   �i  �   1j  �   �j  �   xk  D    l  }   el  �   �l  ;   �m  L   n  F  Pn  >   �o  �   �o  I   rp     �p  �   �p     �q     �q     �q     �q  
   �q     �q  7   r     Mr  [   Rr  
   �r     �r  m   �r  %   7s     ]s     ts     �s     �s  x   �s     6t  <   Ht  
   �t     �t     �t     �t     �t  W   �t     'u  	   :u     Du     Vu     hu     }u     �u     �u  o   �u  v    v     wv     �v  
   �v     �v  
   �v     �v     �v     �v     �v     �v     �v     �v  	   w     w      w     0w     5w     Cw  	   _w     iw  $   �w     �w     �w     �w     �w     �w     �w  
   �w     �w     �w     �w  	   x  	   x  	   x      x     3x     9x     Bx     Vx     bx     qx  	   �x     �x     �x     �x     �x     �x  	   �x     �x     �x     �x     �x  
   y     y     y     'y  
   4y     ?y     Hy     ^y  
   ny     yy     �y     �y     �y     �y     �y     �y     �y     �y     z     z     &z     >z     Ez     Mz  
   Yz     dz     mz     �z     �z  
   �z     �z     �z  	   �z     �z     �z     �z     �z     �z     {  
   {      {    '{   %(cmd)s
returned non-zero exit status %(code)i %A %d %B %Y %i file will be deleted %i files will be deleted %i open item %i open items <Top> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. Add Application Add Notebook Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 All Files All Tasks Allow public access Always use last cursor position when opening a page An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Arithmetic Attach File Attach image first Attachment Browser Attachments Attachments: Author Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave version on regular intervals Backend Bazaar Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Capture whole screen Changes Characters Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Command Command does not modify data Comment Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Could not find executable "%s" Could not find notebook: %s Could not find the file or folder for this notebook Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Dependencies Description Details Discard note? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root E_xport... Edit Custom Tool Edit Image Edit Link Edit _Source Editing Editing file: %s Enable Version Control? Enabled Evaluate _Math Expand _All Export Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Get more templates online Git Gnuplot Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide menubar in fullscreen mode Home Page Icon Images Import Page Index Index page Inline Calculator Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Screenshot Insert Symbol Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Labels marking tasks Last Modified Leave link to new page Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log file Looks like you found a bug Make default application Map document root to URL Match _case Maximum page width Modified Month Move Selected Text... Move Text to Other Page Move text to Name New File New Page New S_ub Page... No Applications Found No changes since last version No dependencies No such file: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open in New _Window Open new page Open with "%s" Optional Options for plugin %s Other... Output file Output folder Output should replace current selection Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Name Page Template Paragraph Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Plugin Plugins Port Position in the window Pr_eferences Preferences Print to Browser Promote Proper_ties Properties Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Removing Links Rename page "%s" Replace _All Replace with Restore page to saved version? Rev Right Side Pane S_ave Version... Save A _Copy... Save Copy Save Version Saved version from zim Score Search Search _Backlinks... Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Server not started Server started Server stopped Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show in the toolbar Show the cursor also for pages that can not be edited Single _page Size Some error occurred while running "%s" Sort alphabetically Sort pages by tags Spell Checker Start _Web Server Sy_mbol... System Default Table of Contents Tags Task Task List Template Templates Text Text Files Text From _File... Text background color Text foreground color The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The following parameters will be substituted
in the command when it is executed:
<tt>
<b>%f</b> the page source as a temporary file
<b>%d</b> the attachment directory of the current page
<b>%s</b> the real page source file (if any)
<b>%p</b> the page name
<b>%n</b> the notebook location (file or folder)
<b>%D</b> the document root (if any)
<b>%t</b> the selected text or word under cursor
<b>%T</b> the selected text including wiki formatting
</tt>
 The inline calculator plugin was not able
to evaluate the expression at the cursor. There are no changes in this notebook since the last version that was saved This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To_day Today Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Tray Icon Turn page name into tags for task items Type Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use a custom font Use a page for each Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Zim Desktop Wiki Zoom _Out _About _All Panes _Arithmetic _Back _Browse _Bugs _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Edit _Edit Link _Edit Link or Object... _Edit Properties _Emphasis _FAQ _File _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _Move Here _New Page... _Next _None _Normal Size _Numbered List _Open _Open Another Notebook... _Other... _Page _Parent _Paste _Preview _Previous _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In calendar:week_start:0 readonly seconds translator-credits Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2021-02-01 19:41+0000
Last-Translator: Gunduzhan Gunduz <gunduzhan@gmail.com>
Language-Team: Turkish <https://hosted.weblate.org/projects/zim/master/tr/>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.5-dev
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
 %(cmd)s
sıfır-olmayan %(code)i çıkış durumunu döndürdü %A %d %B %Y %i dosyası silinecek %i dosyası silinecek %i açık görev %i açık görev <En Üst> Masaüstü Wikisi <b>"%s"</b> adında bir dosya zaten mevcut.
Başka bir isim kullanabilir veya mevcut dosyanın üzerine yazabilirsiniz. Uygulama Ekle Defter Ekle Gtkspell kullanan yazım denetimi desteğini ekler.

Bu eklenti Zim ile beraber sağlanan temel bir eklentidir.
 Tüm Dosyalar Tüm Görevler Genel ulaşıma izin ver Sayfa açılışında her zaman imlecin son pozisyonunu kullan. Resim oluşturulurken bir hata meydana geldi.
Kaynak metnini yine de kaydetmek ister misiniz? Açıklamalı Sayfa Kaynağı Aritmetik Dosya Ekle Önce resim ekle Eklenti Tarayıcısı Ekler Ekler: Yazar Zim tarafından otomatik kaydedilen sürüm Biçimlendirme uygularken halihazırdaki sözcüğü de otomatik olarak seç "DeveHörgücü" (CamelCase) sözcükleri otomatik olarak bağlantıya çevir Dosya yollarını otomatik olarak bağlantıya çevir Düzenli aralıklarla otomatik olarak sürüm kaydı yap Arka uç Pazar Alt Panel Gözat Bulle_t Listesi Ya_pılandır Sayfa değiştirilemiyor: %s İptal Tüm ekranın görüntüsünü al Değişiklikler Karakterler _Yazım denetle O_nay Kutusu Listesi Klasik tepsi simgesi Temizle Komut Komut veriyi değiştirmiyor Açıklama Tüm _defter Uygulama yapilandir Eklentiyi Yapılandır %s bağlatılarını açmak için bir uygulama ayarla %s türü dosyaları açmak için bir uygulama ayarla İşaret kutularını görevler gibi düşünün E-posta Adresini Kopyala Kopya Şablon Farklı_Kopyala... Bağlantıyı _Kopyala Kopyalama _Konumu Çalıştırılabilir "%s" bulunamadı Defter bulunamadı: %s Bu defter için klasör veya dosya bulunamadı Açılamadı: %s İfade çözümlenemedi. Okunamadı: %s Sayfa kaydedilemedi: %s Her not için yeni bir sayfa oluştur Klasör oluşturmak ister misiniz? K_es Özel araçlar Özel _Araçlar Özelleştir... Tarih Gün Varsayılan Panoya kopyalanan metin için öntanımlı biçim Öntanımlı not defteri Gecikme Sayfayı Sil "%s" sayfası silinsin mi? Bağımlılıklar Açıklama Ayrıntılar Not silinsin mi? %(page)s sayfasını kaydedilmiş sürümlerinden
%(version)s sürümüne geri döndürmek ister misiniz?

Son kaydedilen sürümünden itibaren yapılmış olan tüm değişiklikler silinecektir ! Belge Kök Dizini _Dışa Aktar Özel Aracı Düzenle Görsel Düzenle Bağlantı Düzenle _Kaynağı Düzenle Düzenleme Düzenlenen dosya: %s Sürüm Kontrolü etkinleştirilsin mi? Etkin Math _Hesapla Hepsini_Genişlet Dışa Aktar Defter dışa aktarılıyor Başarısız Çalıştırma başarısız oldu: %s Uygulama çalıştırırken hata: %s Dosya zaten var Dosya _Şablon... Kayıtlı dosya değiştirildir: %s Dosya mevcut Dosya yazdırılabilir değil: %s Dosya türü desteklenmiyor: %s Dosya adı Süzgeç Bul So_nrakini Bul Önc_ekini Bul Bul ve Değiştir Bul... Klasör Klasör zaten mevcut ve içi boş değil; bu klasöre aktarmak var olan dosyaların üzerine yazabilir. Devam etmek ister misiniz? %s klasörü mevcut İleri seviye aramalar için AND, OR ve NOT işleçlerini
kullanabilirsiniz. Ayrıntılar için yardım sayfasına bakınız. Biçi_m Biçim Daha fazla çevirimiçi şablon edinin Git Gnuplot Başlık _1 Başlık _2 Başlık _3 Başlık _4 Başlık _5 Yükseklik Tam ekranda menüleri gizle Başlangıç Sayfası Simge Resimler Sayfa İçe Aktar Dizin Index sayfası Satır İçi Hesaplayıcı Tarih ve Zaman Ekle Diyagram Ekle Ditaa Ekle Denklem Ekle GNU R Plot'u Ekle Gnuplot Ekle Görsel Ekle Bağlantı Ekle Ekran Görüntüsü Ekle Sembol Ekle Dosyadan Metin Aktar Arayüz Wiki Kaynak Bağlantısı Anahtar Kelimesi Günlük Sayfaya git Sayfaya Git Görev işaretleme etiketleri Son değişiklikler Yeni sayfa için bağlantı oluştur Sol Yan Panel Satır Sıralayıcısı Satır Bağlantı Haritası Belge kökü altındaki dosyaların bağlantılarında tam dosya yolunu göster Bağlantı oluştur Konum Kayıt dosyası Öyle görünüyor ki bir hata buldunuz Varsayılan uyhulama yap Belge kökünü şu URL'de haritalandır Büyük/küçük _harf uyumlu Maksimum sayfa genişliği Değiştirildi Ay Seçili Metni Taşı Metni diğer sayfaya taşı Metni taşı Ad Yeni Dosya Yeni Sayfa Yeni A_lt Sayfa Uygulama bulunamadi Son sürümle aynı Bağımlılık yok Böyle bir dosya yok: %s Böyle bir wiki bulunamadı: %s Kurulu şablon yok Not defteri Not Defterleri Tamam Eklentiler _Klasörünü Aç Klasör Aç Defter Aç Birlikte Aç... _Belge Kökünü Aç _Defter Klasörünü Aç Sayfa _Aç Yeni _Pencerede Aç Yeni sayfa aç "%s" ile aç İsteğe bağlı %s eklentisi için Seçenekler Diğer... Çıktı dosyası Çıktı klasörü Çıktı şimdiki seçimle yer değiştirmeli Üzerine yaz Yol _Çubuğu Sayfa "%s" sayfasının ekler için bir klasörü yok Sayfa Adı Sayfa Şablonu Paragraf Bu sürüm için bir açıklama girin Henüz var olmayan bir sayfaya bağlantı yapmanın
otomatik olarak bu sayfayı oluşturacağını unutmayın. Lütfen defter için bir ad ve klasör seçin Eklenti Eklentiler Port Penceredeki pozisyon Te_rcihler Yeğlenenler Tarayıcıya Yazdır Terfi Ettir _Özellikler Özellikler Hızlı Not... Hızlı Not... Son Değişiklikler Son değişiklikler... Son _Değiştirilen sayfalar Eş zamanlı viki işaretleme (markup) biçimlendirmesi Bağlar kaldırılıyor "%s" sayfasını yeniden adlandır _Tümünü Değiştir Yerine bunu koy: Sayfayı kaydedilmiş sürüme döndürsün mü? Rev Sağ Yan Panel Sürümü Kay_det... Bir _Kopyasını Kaydet... Kopyayı Kaydet Sürümü Kaydet Zim tarafından kaydedilen sürüm Sayı Ara _Geri Bağlantıları Ara... Dosya Seç Dizini Seç Görüntü Seç Bir sürüm seçerek şu andaki durum ile seçilen sürüm arasındaki farkları görebilirsiniz.
Veya birden fazla sürüm seçerek bunlar arasındaki farkları görebilirsiniz.
 Aktarım biçimini seçiniz Çıktı dosyasını veya dizinini seçiniz Dışarı aktarılacak sayfaları seçiniz Pencere veya bölge seç Seçim Sunucu başlatılmadı Sunucu başlatıldı Sunucu durduruldu Yançerçeve yerine, ToCas floating widget göster _Değişiklikleri Göster Her not defteri için ayrı simge göster Araç çubuğunda göster İmleci düzenlemeye kapalı sayfalarda da göster Tekil _sayfa Boyut "%s" çalıştırılırken bir hata oluştu Alfabetik olarak sırala Sayfaları etiketlere göre sırala Yazım Denetleyici _Web Sunucusunu Başlat Se_mbol Sistem Varsayılanı İçerik Tablosu Etiketler Görev Görev Listesi Şablon Şablonlar Görünecek Metin Metin Dosyaları _Dosyadan Metin Aktar... Yazı arkaplanı rengi Yazı rengi Klasör
%s
Henüz oluşturulmamış.
Şimdi oluşturmak ister misiniz? %s isimli klasör şu an yok.
Oluşturmak ister misiniz? Çalıştırıldığında aşağıdaki parametreler komuttan çıkarılacaktır:	
<tt>
<b>%f</b> geçici sayfa kaynak dosyası
<b>%d</b> mevcut sayfanın eklenti dosyası
<b>%s</b> asıl sayfa kaynak dosyası (eğer varsa)
<b>%p</b> sayfa ismi
<b>%n</b> Note Defteri yeri (dosya veya klasör)
<b>%D</b> doküman kök dizini (eğer varsa)
<b>%t</b> seçili yazı ve yaimleç altındaki kelime
<b>%T</b> viki formatı dahil seçili yazı
</tt>
 Satır içi hesaplayıcı eklentisi, imleç üzerindeki ifadeyi
değerlendirmeye uygun değil. Son kaydedilen sürümünden beri bu defterde değişiklik yapılmamış Bu hata, sözlüklerinizin doğru yüklenmediği 
anlamına gelebilir. Bu dosya zaten var.
Üzerine yazmak istiyor musunuz? Bu sayfanın eklentiler klasörü yok Bu eklenti ile alınan ekran görüntüsü doğrudan zim sayfasına
eklenebilir. 
Bu eklenti zim ile gelen çekirdek eklentilerdendir.
 Bu eklenti bu defterde buluna tüm açık görevleri gösteren bir iletişim
kutusu ekler. Açık görevler işaretlenmemiş işaret kutuları ya da
"TODO" veya "FIX ME" etiketleri ile işaretlenmiş maddeler olabilir.

Bu eklenti Zim ile beraber sağlanan temel bir eklentidir.
 Bu eklenti, yazıları veya pano içeriğini kolayca 
bir Zim sayfasına taşımayı sağlayan bir diyalog ekler.

Bu eklenti Zim ile beraber sağlanan temel bir eklentidir.
 Bu eklenti çabuk erişim için bir sistem çubuğu simgesi ekler.

Bu eklenti Gtk+ 2.10 veya daha yeni sürümünü gerektirmektedir.

Bu eklenti Zim ile beraber sağlanan temel bir eklentidir.
 Bu eklenti mevcut sayfa içeriğini tablolamaya yarayan bir
widget sunar. 
Bu eklenti zim ile gelen çekirdek eklentilerdendir.
 Bu eklenti 'Sembol Ekle' diyalogunu ekler ve 
oto-biçimlendirilmiş karakterleri sağlar.

Bu eklenti Zim ile beraber sağlanan temel bir eklentidir.
 Bu eklenti not defteri için versiyon kontrol yapar.
Bu eklenti Bazaar, Gir ve Mercurial versiyon kontrol sistemleri tarafından desteklenmektedir.
Bu zim ile gelen çekirdek bir eklentidir
 Bu eklenti zim içine aritmetik hesaplamala ekler.
Aşağıdaki aritmetik modeli taban almıştır
http://pp.com.mx/python/arithmetic.
 Bu eklenti ile zim içindeki basit matematiksel ifadelerin
hızlı bir şekilde değerlendirilmesi sağlanır. 
Bu eklenti zim ile beraber gelen ana eklentilerdendir.
 Bu eklenti zim için, Ditaa tabanlı bir diagram editörü sağlar.
 Bu eklenti, GrapViz'li Zim için bir diyagram editörü sağlar.

Bu eklenti Zim ile beraber sağlanan temel bir eklentidir.
 Bu eklenti, not defteri bağ yapısını haritalayan bir diyalog
penceresi sağlar. Sayfaların ilgisini görüntüleyen bir
"zihin haritası" olarak kullanılabilir.

Bu eklenti Zim ile beraber sağlanan temel bir eklentidir.
 Bu eklenti, GNU R'li Zim için bir plot editörü sağlar.
 Bu eklenti zim için, Gnuplot tabanlı bir grafik çizim editörü sağlar.
 Bu eklenti Zim'in yazdırma eksikliğine bir çözüm getirmeyi
amaçlar. Kullanımdaki sayfayı html haline getirerek tarayıcıda
açar. Tarayıcının yazdırma desteği olduğu varsayılırsa, bu yöntemle
verilerinizi iki adımda yazıcıya gönderebilirsiniz.

Bu eklenti Zim ile beraber sağlanan temel bir eklentidir.
 Bu eklenti, Latex'li Zim için bir denklem editörü sağlar.
 Bu eklenti ile seçili satırlar alfabetik şekilde sıralanır.
Eğer liste daha önceden sıralanmış ise sıralama tersine
çevrilir. (A-Z'den Z-A'ya)
 Bu genellikle dosyanın geçersiz karakterler içerdiği manasına gelir. Başlık Devam etmek için bu sayfanın bir kopyasını kaydedebilir ya da
değişiklikleri yoksayabilirsiniz. Eğer bir kopya kaydederseniz, değişiklikler
yine yoksayılacaktır, ancak daha sonra kopyaya geri döndürebilirsiniz. Bu_gün Bugün 'V' İşaret Kutusunu aç/kapa 'X' İşaret Kutusunu aç/kapa Üst Panel Sistem Çubuğu Simgesi Sayfa ismini görev parçaları için etiketlere çevir Tür <BackSpace> ile girintiyi kaldır
(Eğer devredışı ise, <Shift><Tab> aynı işi görür) Bilinmeyen Etiketlenmemiş Bu saykaya bağlantısı olan %i sayfayı da güncelle Bu saykaya bağlantısı olan %i sayfayı da güncelle Bu sayfanın başlığını güncelle Bağlar Güncelleniyor İndeks güncelleniyor Özel font kullan Her biri için bir sayfa kullan Bağlantıları takip etmek için <Enter> tuşunu kullanın.
(Eğer devre dışı ise <Alt><Enter> da kullanabilirsiniz) Sürüm Kontrolü Bu defter için sürüm kontrolü halihazırda etkin değil. Sürümler _Açıklamalı Göster _Kayıtları Göster Web Sunucusu Hafta Bu hatayı bildirirken lütfen aşağıdaki metin
kutusunda verilen bilgiyi de ekleyin. Kelimenin _tamamı Genişlik Wiki sayfası: %s Sözcük Sayımı Sözcük Sayımı... Sözcük Yıl Dün Dosyayı harici bir uygulama ile düzenliyorsunuz. Tamamladığınızda bu iletişim kutusunu kapatabilirsiniz. Araç menüsünde ve araç çubuğunda veya içerik 
menüsünde gözükecek özel araçları yapılandırabilirsiniz. Zim Desktop Wiki _Uzaklaştır _Hakkında _Tüm Paneller _Aritmetik _Geri _Gözat _Hatalar _Çocuk Biçimlendirmeyi _Temizle _Kapat Tümünü _Daralt _İçerik _Kopyala _Buraya Kopyala _Sil Sayfayı _Sil _Değişikliklerden Vazgeç _Düzenle Bağlantıyı _Düzenle Bağlantı veya Nesneyi _Düzenle... Özellikleri _Düzenle _Eğik _SSS _Dosya _Bul... _İleri _Tam Ekran _Git _Yardım _Vurgula _Geçmiş _Anasayfa _Resim... Sayfa _İçe Aktar _Ekle _Atla... _Tuş kısayolları _Bağlantı Tari_he bağla _Bağlantı... _İşaret _Daha Fazla _Taşı _Buraya Taşı _Yeni Sayfa So_nraki _Hiçbiri _Normal Boyut _Numaralanmış Liste _Aç Başka bir Defter _Aç _Diğer... _Sayfa _Ebeveyn _Yapıştır Ö_nizleme _Önceki Tarayıcıya _Yazdır _Hızlı Not... _Çıkış _Son kullanılan sayfalar _Tekrar Yap _Kurallı ifade _Yenile Bağlantıyı _Kaldır _Değiştir _Değiştir... Boyutu Sıfırla Sürüme _Döndür _Kaydet _Kopyayı Kaydet _Ekran Görüntüsü... _Arama _Ara... _Gönder... _Yan Panel _Yanyana _Satırlari Sırala _Üstü çizili _Kalın _Alt simge _Üst simge _Şablonlar _Araçlar _Geri Al _Aynen _Sürümler... _Görünüm _Yakınlaştır takvim:hafta_basi:1 saltokunur saniye Launchpad Contributions:
  Bekir DURAK https://launchpad.net/~bekir-durak
  Bekir SOYLU https://launchpad.net/~cometus
  Caner GÜRAL https://launchpad.net/~canergural
  Emre Ayca https://launchpad.net/~anatolica
  Emre Cıklatekerlio https://launchpad.net/~emrecikla
  Engin BAHADIR https://launchpad.net/~enginbah
  Gökdeniz Karadağ https://launchpad.net/~gokdeniz
  Jaap Karssenberg https://launchpad.net/~jaap.karssenberg
  M. Emin Akşehirli https://launchpad.net/~memedemin
  ytasan https://launchpad.net/~yasintasan 