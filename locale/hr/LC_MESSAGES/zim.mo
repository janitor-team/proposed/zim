��    �     �              L)  ,   M)  .   z)  ?   �)     �)     �)     *     -*  0   I*     z*     �*     �*  	   �*     �*  i   �*  *   <+     g+     n+     ~+     �+     �+  
   �+  -   �+     �+  V   �+     K,  	   Q,  	   [,     e,  3   y,     �,     �,  Y   �,     ?-     U-  
   b-     m-     �-     �-     �-     �-     �-     �-  	   �-     �-  -   �-  *   .  $   E.  ?   j.  /   �.  (   �.     /  %    /  ,   F/     s/  	   �/     �/     �/  
   �/     �/  	   �/     �/     �/     �/     �/     �/  
    0     0     #0  �   *0  �   �0     E1     Z1     a1     p1  
   x1     �1     �1     �1     �1     �1     �1  <   �1     42  	   :2  
   D2     O2     X2     `2     }2     �2     �2     �2     �2     �2  +   �2  3   3      L3     m3     r3     �3     �3  
   �3     �3     �3     �3     �3  3   4     E4     c4     v4     �4     �4     �4     �4     �4     �4     �4     5     5      5     %5     )5  0   15     b5     s5     y5     �5  
   �5     �5     �5     �5     �5     �5     �5     �5     �5  $   6  ~   36     �6     �6  
   �6     �6     �6  
   �6  	   �6  
   �6     7     7     7     .7     F7     T7     \7  5   e7     �7     �7     �7  !   �7     �7  #   �7     8     '8     .8     A8     _8     k8     ~8     �8     �8     �8     �8     �8     9     9     9  
   9     &9     59  	   F9  6   P9     �9  v   �9     :  *   :  c   B:     �:     �:     �:  
   �:     �:     �:     �:     �:  
   ;  
   ;  
   ;  
   &;  
   1;  
   <;     G;     N;     i;     �;  	   �;     �;     �;     �;     �;  #   �;     �;     <  
   <     <     +<     =<     R<     a<     n<     ~<     �<     �<     �<     �<     �<     �<     �<     �<     
=  	    =     *=     <=     D=     L=     Y=     e=  �   r=     ?>     T>     b>     y>     ~>     �>     �>     �>  2   �>     �>     �>     �>     ?     ?     *?     C?     Z?     s?     ?     �?     �?  	   �?     �?     �?     �?     �?     �?     @     $@     1@      6@  *   W@     �@     �@     �@     �@     �@     �@     �@     �@     �@     A  *   A  2   IA     |A     �A     �A     �A     �A  	   �A     �A     �A     �A     B      B     .B     ;B     OB  
   eB     pB  	   �B     �B     �B     �B     �B     �B     �B     �B     C     C  9   C     WC  I   eC  !   �C  '   �C  	   �C     D     D  0   D  
   BD  	   MD     WD     eD     }D     �D  	   �D     �D     �D     �D     �D  '   �D  V   �D  3   SE  0   �E  (   �E     �E     �E  .   F     1F     9F     >F     UF     bF     nF     sF     �F     �F  
   �F  &   �F  
   �F     �F     �F     �F     G     G     <G  
   CG     NG  
   \G     gG     vG     �G  ?   �G     �G     �G     �G     H     +H     /H     5H     EH     [H     dH     kH     |H  	   �H     �H     �H     �H     �H     �H     �H     �H     �H     I     I     /I     CI     OI     ]I  �   jI     �I      J     4J     OJ  	   gJ     qJ     �J     �J     �J     �J     �J     �J     �J     �J     K  2   'K     ZK  &   hK     �K     �K     �K     �K     �K     �K     L  5   %L  &   [L     �L     �L     �L  &   �L     �L     �L     �L     �L     M     M     #M  
   >M     IM     PM  	   _M     iM     oM     |M     �M     �M     �M  	   �M     �M     �M  	   �M     �M  
   �M     �M     �M     N     (N  ?   7N  A   wN  �  �N  S   }P  =   �P     Q  K   Q  @   aQ  6   �Q  -   �Q  I   R  x   QR  �   �R  �   qS  �   :T  �   �T  �   MU  }   �U  L   LV  �   �V  9   #W  �   ]W  �   X  �   �X  }   !Y  C   �Y  h   �Y  k   LZ  �   �Z  -   �[  R   �[  ;   \  =   I\  v   �\    �\  j   ^  n   }^  ]   �^     J_  �   �_  7   _`     �`  �   �`  |   9a     �a     �a     �a     �a     �a     �a     b  	   b  (   b  '   ?b     gb     lb  D   ~b     �b     �b     �b     �b     �b  H    c     Ic     ic     xc  !   �c     �c     �c     �c  P   �c     <d     Kd     Td  U   dd     �d     �d  	   �d     �d  
   �d     �d  N   �d     Ke     We     ]e  �   ke  
   &f     1f     ?f     Ef  	   Jf  ^   Tf  f   �f  �   g     �g  	   �g     �g     �g  
   �g     �g     �g     
h     h     h     h  	   &h     0h     7h     Ih     Ph  	   ^h     hh  
   nh     yh     �h     �h     �h     �h  
   �h     �h     �h     �h  	   �h     �h     i     i     i     i     i     +i     /i  
   5i     @i     Ii  	   Oi     Yi     ii     qi     wi     �i     �i     �i     �i     �i     �i     �i  
   �i     �i     �i     �i     �i     j     j     j      j     /j     3j     9j  	   Sj     ]j     cj     sj     {j     �j  	   �j     �j     �j     �j     �j     �j     �j     �j     �j     �j     �j     k     k     k     *k     6k     Gk  
   Mk     Xk     gk  
   ok     zk     �k     �k     �k     �k     �k  
   �k     �k  
   �k     �k     �k  	   �k     �k     l     	l     l     (l     @l  
   Vl     al     rl     �l     �l     �l     �l     �l  
   �l     �l  �  �l  .   �n  ,   o  P   :o     �o     �o  E   �o  ,    p  S   -p  E   �p  /   �p     �p     �p     	q  i   q  ,   �q     �q     �q     �q     �q  #   �q     
r  (   r     @r  p   Lr     �r     �r  
   �r     �r  B   �r     7s     Js  P   _s     �s     �s  
   �s     �s     �s     	t     !t     4t     <t     Et     Kt     ^t  7   tt  8   �t  %   �t  D   u  B   Pu  .   �u  )   �u  1   �u  4   v     Sv     iv     |v     �v     �v     �v     �v     �v     �v     �v  	   �v     	w     w  %   %w     Kw  �   Tw  �   �w     �x  
   �x     �x     �x     �x     �x     �x     �x     y     -y     Ay  U   Qy     �y     �y     �y     �y     �y     �y  
   �y     z      z     =z     Qz     bz  /   qz  .   �z  -   �z     �z     {     {     *{     ;{     N{  *   ^{  %   �{  "   �{  :   �{  (   |     6|     N|     j|      �|  '   �|     �|     �|     �|     �|     �|     }      }     &}  
   *}  3   5}     i}     �}     �}     �}     �}     �}  
   �}     �}     �}     �}     �}     �}     ~  $   3~  �   X~     �~     �~     �~               &     2     B     P     ]     i  +   �     �  
   �  
   �  @   �     �     .�     <�     C�     _�      n�     ��  	   ��     ��  !   Ā     �     ��  %   �     6�     G�     c�     ��  !   ��     ��     ΁     Ձ     ށ     �     �     �  >   (�     g�  s   l�     ��  '   �  s   �     ��     ��     ��     ��  !   ��  $   ԃ     ��     ��     �  	   �  	   �  	   (�  	   2�  	   <�     F�  $   M�  /   r�     ��     ��     ΄     ބ     �     �  #   ��     �     2�     9�     J�     _�     s�     ��     ��     ��     ��     υ     ޅ     �     ��     �     #�     ;�     G�     V�     o�     x�     ��  	   ��     ��     ��     ̆  �   �     ��     ��  !   ͇     �     ��     �      �     &�  J   8�  
   ��     ��  $   ��     ��     ͈     �     �  )   )�     S�     o�     ��     ��  	   ��          ω     ։  '   �     �     ;�     Y�     l�  2   p�  :   ��     ފ     ��     ��     �     �     5�  	   B�     L�     e�     ��  5   ��  .   ʋ     ��     �  %   /�     U�     s�     �     ��     ��     ��     Ō     ь     �     ��     �     -�  "   >�     a�     o�     ��     ��     ��     ȍ  
   ��     �  	    �     
�  E   �     c�  O   r�  )     '   �     �     �     *�  &   3�     Z�     j�     w�     ��     ��          ԏ     ܏     �     �     ��     �  P   '�  "   x�  ,   ��     Ȑ     �     ��  3   �     5�     <�     H�  	   [�     e�  	   n�     x�     ��  	   ��     ��  0   ��     ��     �     �     �     (�  1   G�     y�  
   ��     ��     ��     ��      ��     ܒ  @   ��     9�  
   F�     Q�  -   l�     ��     ��     ��     ��     ԓ     �     �     �     �     "�     1�     D�     _�      h�     ��     ��     ��     Ĕ     ͔     ߔ     �     �     �  �   �     ��  #   ϕ     �     �     *�     1�     B�     ]�     s�     ��  !   ��  $   ��     �     �  "   �  E   ?�     ��  +   ��     ė     �     �     "�     @�     W�  (   n�  7   ��  .   Ϙ     ��  	   �     �  9   +�     e�     �     ��     ��     ��     ��     ڙ  	   ��     ��     �     �     +�     3�     F�     O�  ,   V�     ��     ��     ��  
   ��  
   ��     ��     ��     њ     �     ��     �  5   �  ;   R�  �  ��  K   T�  A   ��     �  ?   �  =   '�  3   e�     ��  N   ��  �   �  �   ��  �   l�  �   g�  �   �  �   ��  �   R�  f   ܣ  �   C�  =   �  �   !�  �   �  �   ��  �   �  J   ��  z   �  v   d�  �   ۨ  1   ��  M   �  D   4�  E   y�  {   ��    ;�  |   J�  ~   Ǭ  ^   F�  y   ��  �   �  8   ծ     �  �   �  w   ��     0�     9�     @�     F�     c�     }�     ��     ��  &   ��  )   �     �     �  c   0�  	   ��     ��     ��     ı  
   б  �   ۱  #   }�     ��     ��  +   Ѳ     ��     �  "   7�  j   Z�     ų     س     �  X   ��     W�     _�     r�  &   ��     ��     ��  =   Ĵ     �     �     �  �   +�     �     �     �     	�     �  b   �  h   z�  �   �     u�     ��     ��     ��     ��     ��     ��     ͷ  
   շ     �  	   �     �     �     �     &�     /�  	   ;�     E�     N�  	   ]�     g�     z�     ��     ��     ��     ��     ո  
   �  
   �     ��  	   �  	   �     �     #�     *�     @�     E�     M�     V�  	   c�  
   m�     x�     ��     ��     ��     ��  
   ¹     ͹     �  
   ��     ��  
   ��     	�     �     1�     G�     `�  
   s�     ~�     ��     ��     ��     ��  #   ��  
   �  	   �     ��     �     �     �  
   (�     3�     <�     [�     o�     ��     ��     ��     ��     ��     ��     ϻ  	   �     �     ��     �     $�     ,�     ;�     M�     U�     a�     r�  
   ��     ��  
   ��     ��     ��  
   ��     Ǽ     Ӽ  	   ڼ  	   �     �     ��     �     �     &�     >�     T�     d�     s�     ��     ��     ��  #   ��     Խ  	   �     �   		This plugin provides bar for bookmarks.
		 %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i Attachment %i Attachments %i _Backlink %i _Backlinks %i errors occurred, see log %i file will be deleted %i files will be deleted %i open item %i open items %i warnings occurred, see log <Top> <Unknown> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Action Add Application Add Bookmark Add Notebook Add bookmark/Show settings Add column Add new bookmarks to the beginning of the bar Add row Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files All Tasks Allow public access Always use last cursor position when opening a page Always wrap at character Always wrap at word boundaries An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Arithmetic Ascii graph (Ditaa) Attach File Attach image first Attachment Browser Attachments Attachments: Author Auto
Wrap Auto indenting Automatically collapse sections on close page Automatically expand sections on open page Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave interval in minutes Autosave version on regular intervals Autosave version when the notebook is closed Back to Original Name BackLinks BackLinks Pane Backend Backlinks: Bazaar Bookmarks BookmarksBar Border width Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Cannot write this file. Probably this is due to the lenght
of the file name, please try using a name with less
than 255 characters Cannot write this file. Probably this is due to the lenght
of the file path, please try using a folder structure resulting in less
than 4096 characters Capture whole screen Center Change columns Changes Characters Characters excluding spaces Check Checkbox '>' Check Checkbox 'V' Check Checkbox 'X' Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Clone row Code Block Column 1 Command Command does not modify data Comment Common include footer Common include header Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Created Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Demote Dependencies Description Details Diagram Discard note? Display line numbers Distraction Free Editing Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root Due E_xport... Edit %s Edit Custom Tool Edit Image Edit Link Edit Table Edit _Source Editing Editing file: %s Enable Version Control? Enable plugin Enabled Equation Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File name too long: %s File path too long: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Fossil GNU R Plot Get more plugins online Get more templates online Git Gnuplot Grid lines Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide Journal pane if empty Hide menubar in fullscreen mode Highlight current line Home Page Horizontal _Line Icon Images Import Page Include horizontal lines in the ToC Include subpages Index Index page Inline Calculator Insert Code Block Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Sequence Diagram Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Key Binding Key bindings Key bindings can be changed by clicking on a field with a key combination
in the list and then press the new key binding.
To disable a keybinding, select it in the list and use <tt>&lt;Backspace&gt;</tt>. Labels marking tasks Last Modified Leave link to new page Left Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Managing table columns Map document root to URL Match _case Maximum number of bookmarks Maximum page width Menubar Mercurial Modified Month Move Selected Text... Move Text to Other Page Move column ahead Move column backward Move text to Name Need output file to export MHTML Need output folder to export full notebook Never wrap lines New File New Page New Page in %s New S_ub Page... New _Attachment Next No Applications Found No changes since last version No dependencies No document root defined for this notebook No plugin available to display objects of type: %s No such file: %s No such page: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Only Show Active Tasks Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open cell content link Open help Open in New Window Open in New _Window Open new page Open plugins folder Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Index Page Name Page Template Page already exists: %s Page not allowed: %s Page section Paragraph Password Paste Paste As _Verbatim Path Bar Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please select a row, before you push the button. Please select more than one line of text Please specify a notebook Plugin Plugin "%s" is required to display this object Plugins Port Position in the window Pr_eferences Preferences Prev Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Remove column Remove row Removing Links Rename or Move Page Rename page "%s" Repeated clicking a checkbox cycles through the checkbox states Replace _All Replace with Require authentication Restore page to saved version? Rev Right Right Side Pane Right margin position Row down Row up S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Screenshot Command Search Search _Backlinks... Search this section Section Section(s) to ignore Section(s) to index Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Sequence Diagram Server not started Server started Server stopped Set New Name Set ToC fontsize Set default text editor Set to Current Page Show Line Numbers Show Tasks as Flat List Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show due date in sidepane Show full Page Name Show full page name Show helper toolbar Show in the toolbar Show right margin Show tasklist in sidepane Show the cursor also for pages that can not be edited Show the page title heading in the ToC Single _page Size Smart Home key Some error occurred while running "%s" Sort alphabetically Sort pages by tags Source View Spell Checker Start Start _Web Server Support thumbnails for SVG Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Tags for non-actionable tasks Task Task List Tasks Template Templates Text Text Files Text From _File... Text background color Text foreground color Text wrap mode The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The following parameters will be substituted
in the command when it is executed:
<tt>
<b>%f</b> the page source as a temporary file
<b>%d</b> the attachment directory of the current page
<b>%s</b> the real page source file (if any)
<b>%p</b> the page name
<b>%n</b> the notebook location (file or folder)
<b>%D</b> the document root (if any)
<b>%t</b> the selected text or word under cursor
<b>%T</b> the selected text including wiki formatting
</tt>
 The inline calculator plugin was not able
to evaluate the expression at the cursor. The table must consist of at least on row!
 No deletion done. Theme There are no changes in this notebook since the last version that was saved This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This page name cannot be used due to technical limitations of the storage This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a "path bar" to the top of the window.
This "path bar" can show the notebook path for the current page,
recent visited pages or recent edited pages.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds the page index pane to the main window.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows inserting 'Code Blocks' in the page. These will be
shown as embedded widgets with syntax highlighting, line numbers etc.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin also has properties,
see the notebook properties dialog This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a macOS menubar for zim. This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a sequence diagram editor for zim based on seqdiag.
It allows easy editing of sequence diagrams.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This plugin turns one section of the notebook into a journal
with a page per day, week or month.
Also adds a calendar widget to access these pages.
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox '>' Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Tray Icon Try wrap at word boundaries or character Turn page name into tags for task items Type Un-check Checkbox Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unkown Image type Unkown Object Unspecified Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use a page for each Use date from journal pages Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Use thumbnails Username Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log View debug log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s With this plugin you can embed a 'Table' into the wiki page. Tables will be shown as GTK TreeView widgets.
Exporting them to various formats (i.e. HTML/LaTeX) completes the feature set.
 Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Your system encoding is set to %s, if you want support for special characters
or see errors due to encoding, please ensure to configure your system to use "UTF-8" Zim Desktop Wiki Zoom _Out _About _Add _All Panes _Arithmetic _Attachment... _Back _Browse _Bugs _Cancel _Checkbox _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Duplicate Line _Edit _Edit Link _Edit Link or Object... _Edit Properties _Edit... _Emphasis _FAQ _File _Find _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _Move Here _Move Line Down _Move Line Up _New Page Here... _New Page... _Next _None _Normal Size _Numbered List _OK _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Print _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove _Remove Line _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In as due date for tasks as start date for tasks calendar:week_start:0 do not use horizontal lines macOS Menubar no grid lines readonly seconds translator-credits vertical lines with lines {count} of {total} Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2021-01-27 20:32+0000
Last-Translator: Milo Ivir <mail@milotype.de>
Language-Team: Croatian <https://hosted.weblate.org/projects/zim/master/hr/>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Weblate 4.5-dev
 		Ovaj dodatak dodaje traku za zabilješke.
		 Neuspjela naredba: %(cmd)s
Greška: %(code)i Pojavile su se %(n_error)i greške i %(n_warning)i upozorenja. Pogledaj zapisnik %A, %d. %B %Y. %i prilog %i priloga %i priloga %i po_vratna poveznica %i po_vratne poveznice %i po_vratnih poveznica Pojavile su se %i greške. Pogledaj zapisnik Izbrisat će se %i datoteka Izbrisat će se %i datoteke Izbrisat će se %i datoteka %i neobavljen zadatak %i neobavljena zadatka %i neobavljenih zadataka Pojavila su se %i upozorenja. Pogledaj zapisnik <Vrh> <Nepoznato> Desktop wiki Datoteka s imenom <b>„%s”</b> već postoji.
Koristi jedno drugo ime ili prepiši postojeću datoteku. Tablica mora sadržavati barem jedan stupac. Radnja Dodaj program Dodaj zabilješku Dodaj bilježnicu Dodaj zabilješku/Prikaži postavke Dodaj stupac Dodaj nove zabilješke na početak trake Dodaj redak Dodaje podršku za provjeru pravopisa pomoću gtkspell-a.

Ovo je osnovni dodatak koji se isporučuje sa zimom.
 Poravnaj Sve datoteke Svi zadaci Dozvoli javni pristup Uvijek koristi zadnji položaj kursora prilikom otvaranja stranice Prelomi po slovima Prelomi po riječima Došlo je do greške tijekom generiranja slike.
Svejedno spremiti izvorni tekst? Izvor stranice s napomenama Programi Aritmetika Ascii dijagram (Ditaa) Priloži datoteku Najprije priloži sliku Preglednik priloga Prilozi Prilozi: Autor Automatski
prelomi Automatsko uvlačenje Automatski sklopi odjeljak prilikom zatvaranja stranice Automatski rasklopi odjeljak prilikom otvaranja stranice Automatski spremljena verzija od zima Automatski odaberi trenutačnu riječ prilikom primjene formatiranja Automatski pretvori „složene” riječi (CamelCase) u poveznice Automatski pretvori staze datoteka u poveznice Interval automatskog spremanja u minutama Automatski spremi verziju u redovitim intervalima Automatski spremi verziju kad se bilježnica zatvori Natrag na izvorno ime Povratne poveznice Ploča povratnih poveznica Pozadinski sustav Povratne poveznice: Bazaar Zabilješke Traka zabilježaka Širina rubova Donja ploča Pretraži _Popis s predznakom P_odesi Nije moguće promijeniti stranicu: %s Odustani Nije moguće zapisati ovu datoteku. Uzrok tome je vjerojatno
broj korištenih znakova za ime datoteke. Koristi manje od
255 znakova Nije moguće zapisati ovu datoteku. Uzrok tome je vjerojatno
broj korištenih znakova za stazu datoteke. Koristi strukturu mapa,
tako da je ukupan broj znakova manji od 4096 Snimi cijeli ekran Centrirano Promijeni stupce Promjene Znakovi Znakovi bez razmaka Označi kao premješteno '>' Označi kao obavljeno 'V' Označi kao neobavljeno 'X' Provjera pravopi_sa _Označiv popis Klasična ikona u programskoj traci,
Nemoj koristiti novi stil ikone stanja na Ubuntu Poništi Kloniraj redak Blok s kodom Stupac 1 Naredba Naredba ne mijenja podatke Komentiraj Općenito uključi podnožje Općenito uključi zaglavlje Cijela _bilježnica Postavi programe Podesi dodatak Postavi program za otvaranje „%s” poveznica Postavi program za otvaranje
„%s” datoteka Interpretiraj sve potvrdne okvire kao zadatke Kopiraj Kopiraj e-adresu Kopiraj predložak _Kopiraj kao … Kopiraj po_veznicu Kopiraj _mjesto Izvršna datoteka „%s” nije pronađena Nije moguće pronaći bilježnicu: %s Predložak „%s” nije pronađen Nije moguće pronaći datoteku ili mapu za ovu bilježnicu Neuspjelo učitavanje provjere pravopisa Neuspjelo otvaranje: %s Nije moguće obraditi izraz Neuspjelo čitanje: %s Neuspjelo spremanje stranice: %s Stvori novu stranicu za svaku bilješku Stvoriti mapu? Stvoreno _Izreži Prilagođeni alati Prilagođeni ala_ti Prilagodi … Datum Dan Standardni Standardni format za kopiranje teksta međuspremnik Standardna bilježnica Zadrška Ukloni stranicu Ukloni stranicu „%s”? Izbriši redak Spusti Zavisnosti Opis Detalji Dijagram Odbaciti bilješku? Prikaži brojeve redaka Pojednostavljeno uređivanje Želiš li ukloniti sve zabilješke? Želiš li obnoviti stranicu: %(page)s
pomoću spremljene verzije: %(version)s ?

Izgubit će se sve u međuvremenu urađene promjene! Direktorij dokumenta Rok _Izvezi … Uredi %s Uredi prilagođene alate Uredi sliku Uredi poveznicu Uredi tablicu Uredi i_zvor Uređivanje Uređivanja datoteke: %s Aktivirati sustav za upravljanje verzijama? Aktiviraj dodatak Aktivirano Jednadžba Greška u %(file)s u %(line)i. retku u blizini „%(snippet)s” Izračunaj _matematiku _Rasklopi sve Izvezi Izvezi sve stranice zajedno Izvoz završen Izvezi sve stranice pojedinačno Izvoz bilježnice Neuspjelo Neuspjelo pokretanje: %s Neuspjelo pokretanje programa: %s Datoteka postoji Pre_dlošci datoteka … Datoteka je promijenjena na disku: %s Datoteka postoji Datoteka je zaštićena: %s Predugačko ime datoteke: %s Predugačka staza datoteke: %s Vrsta datoteke nije podržana: %s Ime datoteke Filtar Pronađi Pronađi _sljedeće Pronađi _prethodno Pronađi i zamijeni Što tražiš Označi zadatke s rokom u ponedjeljak ili utorak prije vikenda Mapa Mapa već postoji i sadržava datoteke. Izvoz u ovu mapu može prepisati postojeće datoteke. Želiš li nastaviti? Mapa postoji: %s Mapa s predlošcima za datoteke priloga Za napredno pretraživanje mogu se koristiti operatori poput
I, ILI i NE. Pogledaj stranicu pomoći za pojedinosti. For_mat Format Fossil GNU R grafikon Preuzmi još dodataka s interneta Preuzmi još predložaka s interneta Git Gnuplot Rasterske crte Naslov _1 Naslov _2 Naslov _3 Naslov _4 Naslov _5 Visina Sakrij ploču dnevnika ako je prazna Sakrij traku izbornika u cjeloekranskom prikazu Istakni trenutačni redak Početna stranica Vodoravna _crta Ikona Slike Uvezi stranicu Uključi vodoravne crte u sadržaju Uključi podstranice Indeks Stranica indeksa Umetnutni kalkulator Umetni blok s kodom Umetni datum i vrijeme Umetni dijagram Umetni Ditaa Umetni jednadžbu Umetni GNU R grafikon Umetni Gnuplot Umetni sliku Umetni poveznicu Umetni notni zapis Umetni sliku ekrana Umetni dijagram slijeda Umetni znak Umetni tablicu Umetni tekst iz datoteke Sučelje Interwiki ključna riječ Dnevnik Skoči na Skoči na stranicu Tipkovnički prečac Tipkovnički prečaci Tipkovnički prečaci mogu se promijeniti odabirom radnje u popisu i pritiskom na nov prečac.
Za deaktiviranje prečaca, odaberi ga u popisu i pritisni tipku <tt>&lt;Backspace&gt;</tt>. Oznake za označivanje zadataka Zadnja promjena Ostavi poveznicu na novu stranicu Lijevo Lijeva bočna ploča Razvrstavanje redaka Redci Karta povezivanja Poveži datoteke pod direktorijem dokumenta s potpunom stazom do dokumenta Poveži na Mjesto Zapisuj događaje pomoću Zeitgeista Datoteka zapisnika Čini se da se dogodila greška Postavi kao standardni program Upravljanje stupcima tablice Mapiraj direktorij dokumenta u URL adresu _Usporedi velika/mala slova Maksimalan broj zabilježaka Maksimalna širina stranice Traka izbornika Mercurial Promijenjeno Mjesec Premjesti odabrani tekst … Premjesti tekst na jednu drugu stranicu Premjesti stupac prema naprijed Premjesti stupac prema natrag Premjesti tekst na Ime Za izvoz MHTML-a mora se zadati datoteka rezultata Za izvoz potpune bilježnice mora se zadati mapa rezultata Ne prelamaj retke Nova datoteka Nova stranica Nova stranica u %s Nova po_dstranica … Novi p_rilog Sljedeći Programi nisu pronađeni Nema promjena od zadnje verzije Bez zavisnosti Za ovu bilježnicu nije određen direktorij dokumenta Nedostaje dodatak za prikaz objekata vrste: %s Takva datoteka ne postoji: %s Stranica ne postoji: %s Ova wiki poveznica nije određena: %s Nema instaliranih predložaka Bilježnica Bilježnice U redu Prikaži samo aktivne zadatke Otvori _mapu priloga Otvori mapu Otvori bilježnicu Otvori pomoću … Otvori _direktorij dokumenta Otvori mapu _bilježnice Otvori _stranicu Otvori poveznicu sadržaja ćelije Otvori pomoć Otvori u novom prozoru Otvori u novom _prozoru Otvori novu stranicu Otvori mapu dodataka Otvori pomoću „%s” Opcionalno Opcije za dodatak %s Druga … Datoteka rezultata Datoteka rezultata postoji. Zadaj „--overwrite” za prisilni izvoz Mapa rezultata Mapa rezultata postoji i nije prazna. Zadaj „--overwrite” za prisilni izvoz Za izvoz se mora navesti mjesto rezultata Rezultat zamijenjuje trenutačni odabir Prepiši Tr_aka staza Stranica Stranica „%s” nema mapu za priloge Indeks stranice Ime stranice Predložak stranice Stranica već postoji: %s Stranica nije dozvoljena: %s Odjeljak stranice Odlomak Lozinka Umetni Umetni _doslovno Traka staza Upiši komentar za ovu verziju Napomena: povezivanje s nepostojećom stranicom
automatski stvara novu stranicu. Odaberi ime i mapu za bilježnicu. Prije pritiskanja gumba odaberi jedan redak. Odaberi više od jednog retka Odredi bilježnicu Dodatak Za prikaz ovog objekta potreban je dodatak „%s” Dodaci Priključak Položaj u prozoru Pos_tavke Postavke Prethodni Ispiši putem web-preglednika Digni _Svojstva Svojstva Šalje događaje pozadinskom programu Zeitgeist. Brza bilješka Brza bilješka … Nedavne promjene Nedavne promjene … Nedavno pro_mijenjene stranice Preformatiraj označivanja wiki stranice trenutno Ukloni Ukloni sve Ukloni stupac Ukloni redak Uklanjanje poveznica Preimenuj ili premjesti stranicu Preimenuj stranicu „%s” Ponovnim pritiskanjem potvrdnog okvira mijenja se njegovo stanje Zamjeni _sve Zamijeni s Zahtijevaj autentifikaciju Obnoviti stranicu pomoću spremljene verzije? Revizija Desno Desna bočna ploča Položaj desne margine Spusti redak Digni redak _Spremi verziju … Spremi _kopiju … Spremi kopiju Spremi verziju Spremi zabilješke Spremljena verzija iz zima Rezultat Naredba za snimanje slike ekrana Traži Traži po_vratne poveznice … Traži ovaj odjeljak Odjeljak Zanemari odjeljke Indeksiraj odjeljke Odaberi datoteku Odaberi mapu Odaberi sliku Odaberi verziju za prikaz promjena između te verzije i trenutačnog stanja
ili odaberi više verzija odjednom za prikaz promjene između tih verzija.
 Odaberi format za izvoz Odaberi datoteku ili mapu rezultata Odaberi stranice za izvoz Odaberi prozor ili područje Odabir Dijagram slijeda Poslužitelj nije pokrenut Poslužitelj pokrenut Poslužitelj zaustavljen Postavi novo ime Postavi veličinu fonta sadržaja Postavi standardni uređivač teksta Postavi na trenutačnu stranicu Prikaži brojeve redaka Prikaži zadatke kao običan popis Prikaži sadržaj kao plutajući programčić umjesto u bočnoj traci _Prikaži promjene Prikaži zasebnu ikonu za svaku bilježnicu Prikaži rok u bočnoj ploči Prikaži kompletno ime stranice Prikaži kompletno ime stranice Prikaži alatnu traku pomoći Prikaži u traci alata Prikaži desnu marginu Prikaži popis zadataka u bočnoj ploči Prikaži kusor i za stranice koje se ne mogu uređivati Prikaži naslov zaglavlja stranice u sadržaju Jedna _stranica Veličina Pametna Home tipka Došlo je do greške tijekom pokretanja programa „%s” Razvrstaj abecednim redom Razvrstaj po oznakama Prikaz izvora Provjera pravopisa Početak Pokreni _web poslužitelja Podrži minijature za SVG _Znak … Sintaksa Standard sustava Širina tabulatora Tablica Uređivač tablica Sadržaj Oznake Oznake za zadatke koji ne zahtijevaju radnju Zadatak Popis zadataka Zadaci Predložak Predlošci Tekst Tekstualne datoteke Tekst iz _datoteke … Stražnja boja Prednja boja Modus prelamanja teksta Mapa
%s
još ne postoji.
Želiš li je sada stvoriti? Mapa „%s” još ne postoji.
Želiš li je sada stvoriti? Sljedeći parametri će se zamijeniti
u naredbi kad se izvršava:
<tt>
<b>%f</b> izvor stranice kao privremena datoteka
<b>%d</b> mapa priloga trenutačne stranice
<b>%s</b> stvarna izvorna datoteka stranice (ako postoji)
<b>%p</b> ime stranice
<b>%n</b> mjesto bilježnice (datoteka ili mapa)
<b>%D</b> direktorij dokumenta (ako postoji)
<b>%t</b> odabrani tekst ili riječ ispod kursora
<b>%T</b> odabrani tekst, uključujući wiki formatiranje
</tt>
 Dodatak ugrađenog kalkulatora nije uspio
izračunati izraz na pokazivaču. Tablica mora sadržavati barem jedan redak.
 Ništa se ne briše. Tema U ovoj bilježnici nema promjena otkad je zadnji put spremljena To može značiti da odgovarajući rječnici
nisu instalirani Ova datoteka već postoji.
Želiš li je prepisati? Ova stranica nema mapu priloga Ovo se ime stranice ne može koristiti zbog tehničkih ograničenja spremišta Ovaj dodatak omogućuje snimanje slike ekrana i izravno
umetanje slike na zim stranici.

Ovo je osnovni dodatak koji se isporučuje sa zimom.
 Ovaj dodatak dodaje „traku staza” u gornjem dijelu prozora.
Ova „traka staza” može prikazati stazu bilježnice trenutačne stranice,
stazu nedavno posjećene stranice ili stazu nedavno uređene stranice.
 Ovaj dodatak dodaje dijalog za prikaz svih neobavljenih
zadataka u ovoj bilježnici. Neobavljeni zadaci su neoznačeni ili
su obilježeni s „TODO” (za obaviti) ili „FIXME” (za ispraviti).

Ovo je osnovni dodatak koji se isporučuje sa zimom.
 Ovaj dodatak dodaje dijalog za brzo ispuštanje teksta ili
sadržaja međuspremnika u zim stranicu.

Ovo je osnovni dodatak koji se isporučuje sa zimom.
 Ovaj dodatak dodaje ikonu u programskoj traci za brz pristup.

Ovaj dodatak zahtijeva Gtk+ verziju 2.10 ili noviju.

Ovo je osnovni dodatak koji se isporučuje sa zimom.
 Ovaj dodatak dodaje dodatni programčić za prikaz popisa
stranica koje su povezane s trenutačnom stranicom.

Ovo je osnovni dodatak koji se isporučuje sa zimom.
 Ovaj dodatak dodaje dodatni programčić za prikaz
sadržaja trenutačne stranice.

Ovo je osnovni dodatak koji se isporučuje sa zimom.
 Ovaj dodatak dodaje postavke koje pomažu koristiti zim
kao uređivač s pojednostavljenim sučeljem.
 Ovaj dodatak dodaje dijalog „Umetni znak” i omogućuje
automatsko formatiranje tipografskih znakova.

Ovo je osnovni dodatak koji se isporučuje sa zimom.
 Ovaj dodatak dodaje ploču indeksa stranice u glavni prozor.
 Ovaj dodatak dodaje mogućnost upravljanja verzijama za bilježnice.

Ovaj dodatak podržava sustave za upravljanje verzijama Bazaar, Git i Mercurial.

Ovo je osnovni dodatak koji se isporučuje sa zimom.
 Ovaj dodatak omogućuje umetanje „blokova koda” u stranicu. Prikazuju
se kao ugrađeni programčići s isticanjem sintakse, brojevima redaka itd.
 Ovaj dodatak omogućuje ugrađivanje računskih operacija u zim.
Temelji se na aritmetičkom modulu iz
http://pp.com.mx/python/arithmetic.
 Ovaj dodatak omogućuje brzo izračunavanje jednostavnih
matematički izraza u zimu.

Ovo je osnovni dodatak koji se isporučuje sa zimom.
 Ovaj dodatak također ima svojstva,
pogledaj dijalog svojstava bilježnice Ovaj dodatak pruža uređivač dijagrama za zim, zasnovan na Ditaa.

Ovo je osnovni dodatak koji se isporučuje sa zimom.
 Ovaj dodatak pruža uređivač dijagrama na temelju GraphViz-a.

Ovo je osnovni dodatak koji se isporučuje sa zimom.
 Ovaj dodatak pruža dijalog s grafičkim
prikazom strukture povezivanja bilježnice.
Može se koristiti kao vrsta „mentalne karte”
za prikaz odnosa stranica.

Ovo je osnovni dodatak koji se isporučuje sa zimom.
 Ovaj dodatak pruža macOS traku izbornika za zim. Ovaj dodatak pruža indeks stranice filtriran putem biranja oznaka u oblaku.
 Ovaj dodatak pruža uređivač grafikona za zim na temelju GNU R-a.
 Ovaj dodatak pruža uređivač grafikona za zim na temelju Gnuplota.
 Ovaj dodatak pruža uređivač dijagrama slijeda na temelju seqdiag.
Omogućuje jednostavno uređivanje dijagrama slijeda.
 Ovaj dodatak pruža rješenje za nedostajuću podršku za
ispis u zimu. Trenutačnu stranicu izvozi u html i otvara
preglednik. Pod pretpostavkom da preglednika podržava
ispis, podaci se mogu ispisati u dva koraka.

Ovo je osnovni dodatak koji se isporučuje sa zimom.
 Ovaj dodatak pruža uređivač jednadžbi za zim zasnovan na lateksu.

Ovo je osnovni dodatak koji se isporučuje sa zimom.
 Ovaj dodatak pruža uređivač notnih zapisa zasnovan na GNU Lilypondu.

Ovo je osnovni dodatak koji se isporučuje sa zimom.
 Ovaj dodatak prikazuje mapu s prilozima trenutnače stranice u
prikazu ikona u donjoj ploči.
 Ovaj dodatak ravrstava odabrane retke abecednim redom.
Ako je popis već ravrstan, redoslijed se preokreće
(A-Z u Z-A).
 Ovaj dodatak pretvara jedan odjeljak bilježnice u dnevnik
s pojedinačnim stranicama po danu, tjednu ili mjesecu.
Također dodaje programčić kalendara za pristup tim stranicama.
 To obično znači da datoteka sadrži neispravne znakove Naslov Za nastavak rada, spremi kopiju ove stranice ili odbaci
sve promjene. Ako spremiš kopiju, promjene će se
također odbaciti, ali kopiju možeš kasnije obnoviti. Za stvaranje nove bilježnice moraš odabrati praznu mapu.
Naravno možeš odabrati i postojeću mapu zim bilježnice.
 Sadržaj _Danas Danas Označi kao premješteno '>' Označi kao obavljeno 'V' Označi kao neobavljeno 'X' Gornja ploča Ikona u programskoj traci Prelomi retke po riječima ili slovima Pretvori ime stranice u oznake za zadatke Vrsta Postavi kao neoznačeno Ukloni uvlaku pomoću <BackSpace>
(Ako je deaktivirano, još uvijek možeš koristiti <Shift><Tab>) Nepoznato Nepoznata vrsta slike Nepoznat objekt Neodređeno Bez oznaka Aktualiziraj %i stranicu koja ukazuje na ovu stranicu Aktualiziraj %i stranice koje ukazuje na ovu stranicu Aktualiziraj %i stranica koje ukazuje na ovu stranicu Aktualiziraj zaglavlje ove stranice Aktualiziranje poveznica Aktualiziranje indeksa Za prebacivanje na bočnu ploču koristi %s Koristi prilagođeni font Koristi jednu stranicu za svaki Koristi datum iz stranica dnevnika Koristi tipku <Enter> za praćenje poveznica
(Ako je deaktivirano, i dalje možeš koristiti <Alt><Enter>) Koristi minijature Korisničko ime Upravljanje verzijama Upravljanje verzijama nije aktivirano za ovu bilježnicu.
Želiš li to sada aktivirati? Verzije Prikaži _napomene Prikaži _zapisnik Prikaži zapisnik otklanjanja grešaka Web poslužitelj Tjedan Prilikom prijave ove greške,
uključi dolje navedene podatke _Cijela riječ Širina Wiki stranica: %s Ovaj dodatak omogućuje ugrađivanje tablice u wiki stranicu. Tablice se prikazuju kao GTK TreeView programčići.
Izvoz tablica u različite formate (HTML/LaTeX) je također moguć.
 Broj riječi Broj riječi … Riječi Godina Jučer Uređuješ jednu datoteku u jednom vanjskom programu. Ovaj dijalog možeš zatvoriti kad završiš Postavi prilagođene alate koji će se pojaviti
u izborniku i traci alata ili u kontekstnim izbornicima. Kodiranje tvog sustava postavljeno je na %s. Ako želiš podršku za posebne znakove
ili vidjeti greške kodiranja, postavi sustav na „UTF-8” Zim Desktop Wiki S_manji prikaz _Informacije _Dodaj _Sve ploče _Aritmetika _Prilog … Na_trag _Pretraži _Greške O_dustani Pot_vrdni okvir P_određeni P_oništi formatiranje _Zatvori _Sklopi sve _Sadržaj _Kopiraj _Kopiraj ovamo Iz_briši Iz_briši stranicu _Odbaci promjene _Dupliciraj redak _Uredi Ur_edi poveznicu Ur_edi poveznicu ili objekt … Uredi _svojstva Ur_edi … _Istaknuto Č_PP _Datoteka _Pronađi Pro_nađi … _Dalje _Cjeloekranski prikaz _Idi _Pomoć _Istakni _Kronologija _Početna _Slika … _Uvezi stranicu … U_metni _Skoči _Skoči na … Tip_kovnički prečaci _Poveznica _Poveži s datumom Po_veži … Pod_crtano _Još Pre_mjesti _Premjesti ovamo _Premjesti redak dolje _Premjesti redak gore _Nova stranica ovdje … _Nova stranica … _Sljedeće _Ništa _Normalna veličina _Numerirani popis _U redu _Otvori _Otvori jednu drugu bilježnicu … _Drugo … _Stranica _Hijerarhija stranica _Nadređeni _Umetni _Pregled Pret_hodno _Ispiši _Ispiši putem web-preglednika _Brza bilješka … _Zatvori program Nedavne st_ranice P_onovi _Regularan izraz _Aktualiziraj _Ukloni _Ukloni redak _Ukloni poveznicu _Zamijeni _Zamijeni … _Postavi izvornu veličinu _Obnovi verziju _Spremi Spremi _kopiju _Slika ekrana … _Traži _Traži … _Pošalji na … _Bočne ploče _Usporedno _Razvrstaj retke _Precrtano _Podebljano _Indeks _Eksponent _Predlošci _Alati _Poništi _Doslovno _Verzije … _Prikaz U_većaj prikaz kao rok za zadatke kao početak za zadatke calendar:week_start:1 nemoj koristiti vodoravne crte macOS traka izbornika bez rasterskih crta samo-za-čitanje s Milo Ivir <mail@milotype.de>, 2020. okomite crte sa crtama {count} od {total} 