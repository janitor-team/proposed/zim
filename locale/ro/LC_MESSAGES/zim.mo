��         �              l!  ,   m!  .   �!     �!     �!  0   �!     ""     ="     ["  	   a"     k"  i   z"  *   �"     #     #     ,#  
   9#  -   D#     r#  V   z#     �#  	   �#  	   �#     �#  3   �#  Y   3$     �$     �$  
   �$     �$     �$     �$     �$     �$      %  $   %  ?   4%  /   t%  (   �%  %   �%  	   �%     �%     &     &  	   &     %&     1&     8&  
   E&     P&     h&     o&     �&     �&     �&  
   �&     �&     �&     �&  <   �&     %'  	   +'  
   5'     @'     H'     e'     m'     �'     �'     �'     �'  +   �'  3    (      4(     U(     h(     v(  
   �(     �(     �(     �(     �(  3   �(     ()     F)     Y)     t)     �)     �)     �)     �)     �)     �)     �)     �)     *     *     *  0   *     E*     V*     \*     h*  
   z*     �*     �*     �*     �*     �*     �*  ~   �*     S+  
   a+     l+  
   }+  	   �+  
   �+     �+     �+     �+     �+     �+  5   �+     ,     (,     4,  !   ;,     ],  #   n,     �,     �,     �,     �,     �,     �,     �,     -     !-     :-     V-     _-     f-  
   k-     v-     �-  	   �-  6   �-     �-  v   �-     U.  *   g.  c   �.     �.     �.     /     /     7/     ;/  
   C/  
   N/  
   Y/  
   d/  
   o/     z/     �/     �/  	   �/     �/     �/     �/     �/     �/  
   �/     �/     0      0     50     D0     Q0     a0     s0     �0     �0     �0     �0     �0     �0     �0     �0  	   1     1     1     '1     /1     <1     Q1     _1     v1     {1     �1     �1     �1  2   �1     �1     �1     �1     2     2     '2     @2     Y2     e2     x2  	   �2     �2     �2     �2     �2     �2     �2     �2     �2       3  *   !3     L3     U3     ^3     o3     3     �3     �3     �3     �3     �3     4  	   4     4     4     34     ?4     M4     Z4     n4  
   �4     �4     �4     �4     �4     �4     �4     �4     �4  9   5     A5  I   O5  !   �5  '   �5  	   �5     �5     �5  0   �5  	   ,6     66     D6  	   Q6  '   [6  V   �6  3   �6     7     (7     /7     77     <7     S7     `7     l7     q7     �7     �7  
   �7  &   �7  
   �7     �7     �7     �7     8     8     :8  
   A8     L8  
   Z8     e8     t8     �8     �8     �8     �8     �8     �8     �8     �8     �8  	   9     9     &9     59     L9     R9     e9     l9     �9     �9     �9     �9  �   �9     @:      Y:     z:     �:  	   �:     �:     �:     �:     �:     �:     ;  2   #;     V;  &   d;     �;     �;     �;     �;  5   �;  &   <     6<     C<     H<  &   W<     ~<     �<     �<     �<     �<  
   �<     �<     �<  	   �<     �<     =     =     !=     &=     D=  	   I=     S=  	   \=     f=  
   k=     v=     �=     �=  ?   �=  A   �=  S   7>  K   �>  @   �>  6   ?  -   O?  x   }?  �   �?  �   �@  �   DA  �   �A  }   SB  L   �B  �   C  �   �C  �   PD  }   �D  h   ]E  k   �E  �   2F  R   G  ;   YG  =   �G  v   �G    JH  j   ^I  n   �I  ]   8J     �J  �   K  7   �K     �K  �   �K  |   �L     M     M     M     M     'M     ;M  	   DM  '   NM     vM  D   {M     �M     �M     �M  H   �M     &N     FN     UN     dN     vN  P   �N     �N  U   �N     AO     JO  	   ZO  
   dO     oO  N   tO     �O     �O     �O  
   �O     �O     �O     P  	   P  ^   P  f   pP     �P  	   �P     �P  
   �P     Q     Q     Q     Q     $Q     +Q     =Q     DQ  	   RQ     \Q  
   bQ     mQ     uQ     �Q     �Q  
   �Q     �Q     �Q  	   �Q     �Q     �Q     �Q     �Q     �Q      R     R  
   
R     R     R  	   $R     .R     >R     FR     RR     _R     eR     sR     |R     �R     �R  
   �R     �R     �R     �R     �R     �R     �R     �R  	   �R     �R     �R     S     S     S  	   &S     0S     BS     QS     WS     eS     kS     S     �S     �S     �S     �S     �S     �S  
   �S     �S     �S  
   �S     �S     T     T     T     +T     3T  
   ;T     FT  
   ST     ^T     eT  	   kT     uT     �T     �T     �T     �T     �T     �T     �T     �T  
   �T    �T  A   W  5   MW     �W  /   �W  M   �W  <   X  6   JX     �X     �X     �X  i   �X  1   Y     GY     ZY     nY     |Y  ;   �Y     �Y  p   �Y  	   QZ     [Z     lZ     |Z  I   �Z  S   �Z     /[  
   I[  	   T[     ^[     q[     �[     �[     �[     �[  !   �[  <   �[  5   %\  1   [\  2   �\     �\     �\     �\     �\     �\     ]  	   ]     "]     7]     D]  	   d]     n]     �]     �]     �]  	   �]     �]     �]     �]  g   �]     g^     p^     �^     �^     �^  
   �^     �^     �^     �^     �^     _  B   %_  I   h_  0   �_     �_     �_     `     `     1`  &   D`     k`  $   �`  :   �`  +   �`     a     0a     Pa     fa  /   �a     �a  	   �a     �a     �a     �a     b     b     b     b  ?   'b     gb     xb     �b     �b     �b     �b     �b  	   �b     �b     �b  $   c  �   -c     �c     �c     �c     d     d     %d     5d     Fd     Nd     gd     �d  F   �d     �d     �d  	   e  /   e     Ae  1   Re     �e     �e     �e  !   �e     �e     �e  "   f     (f     :f     Zf     xf     �f  	   �f     �f     �f     �f     �f  A   �f     +g  �   1g     �g  -   �g  ~   �g  
   yh  	   �h  %   �h  (   �h     �h     �h     �h     �h     �h     i     i     i  -   #i     Qi     ni  	   �i     �i     �i     �i     �i     �i     �i     �i     �i     j     $j     3j     Ej     Yj     jj     {j     �j     �j  #   �j     �j     �j     k     %k     1k     Ik     Pk     Xk      gk     �k      �k  
   �k     �k     �k     �k     �k  K   l     Xl     fl  %   ol     �l  !   �l  !   �l  *   �l     m     &m     Bm  	   Qm     [m     gm     mm     �m     �m     �m     �m     �m  =   �m  =   &n     dn     pn     ~n     �n  #   �n  &   �n     �n     �n  $   o     <o     [o     co     jo      mo     �o     �o     �o      �o     �o     �o     p  "   -p     Pp     gp  	   |p     �p  	   �p     �p  V   �p     q  d   ,q  1   �q  9   �q  
   �q     r     r  .   r     Mr     \r     mr     �r  ;   �r  b   �r  4   *s     _s  	   ts  
   ~s     �s     �s     �s     �s  	   �s     �s     �s     �s      t  ,   t     ;t     Lt     `t     st     �t  %   �t     �t     �t     �t     �t     
u      u     9u     Lu  )   du     �u  
   �u     �u     �u     �u     �u      v     v     $v     Cv     ]v  #   bv     �v     �v  	   �v     �v     �v     �v  �   �v  !   �w  -   �w  $   �w      x  	   4x     >x     Vx     nx     �x  &   �x     �x  :   �x     y  2   #y      Vy      wy     �y     �y  4   �y  ,   z     1z     Dz     Mz  &   fz     �z  !   �z     �z     �z     �z  
   �z     {     {     #{     6{     <{     L{     T{  +   ]{     �{     �{     �{  	   �{     �{     �{     �{     �{     �{  9   |  ?   H|  O   �|  Q   �|  D   *}  /   o}  3   �}  �   �}    \~  �   b  �   �  �   ��  �   Y�  i   ݁  �   G�  �   ��  �   ��  �   C�  t   ʄ  �   ?�  �   Å  l   ��  H   ,�  ?   u�  �   ��  X  F�  �   ��  �    �  i   ��  �   �  �   ��  G   D�     ��  �   ��  �   ]�     �     "�     +�  "   3�  "   V�     y�  #   ��  D   ��     ��  R   �     G�     V�     c�  �   p�  '   +�     S�     p�     ��  $   ��  j   ΐ     9�  J   M�     ��     ��     ��  
   đ     ϑ  ^   ݑ     <�  	   M�     W�     j�     |�     ��     ��     ��  [   ��  }   �     ��     ��     ��     ��  
   Ó     Γ     ד  
   �     �     ��  	   �     �     '�  	   5�     ?�     N�     W�     g�  
   ��     ��  $   ��     Ɣ  
   �     �     �  
   �     �     %�  	   5�     ?�     G�     V�     _�     g�     s�  	   ��     ��     ��     ��     ��     ѕ  
   ��  
   �     ��     ��     	�  	   �     "�     )�     ;�  	   L�     V�  
   j�     u�     ~�  	   ��  	   ��     ��  
   ��     Ɩ     �     ��     �     �     &�     :�     G�     Z�     i�     x�     ��     ��     ��     ��  	   З  
   ڗ     �     ��     
�     �     -�     5�     A�  	   I�  
   S�     ^�     f�     n�     w�     ��  
   ��     ��     ��     Ø     ��     �    ��     �   		This plugin provides bar for bookmarks.
		 %(cmd)s
returned non-zero exit status %(code)i %A %d %B %Y %i errors occurred, see log %i file will be deleted %i files will be deleted %i open item %i open items %i warnings occurred, see log <Top> <Unknown> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Add Application Add Bookmark Add Notebook Add column Add new bookmarks to the beginning of the bar Add row Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files All Tasks Allow public access Always use last cursor position when opening a page An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Arithmetic Attach File Attach image first Attachment Browser Attachments Author Auto indenting Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave version on regular intervals BackLinks BackLinks Pane Backend Bazaar Bookmarks Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Capture whole screen Center Change columns Changes Characters Characters excluding spaces Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Clone row Code Block Command Command does not modify data Comment Common include footer Common include header Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Created Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Demote Dependencies Description Details Discard note? Distraction Free Editing Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root E_xport... Edit Custom Tool Edit Image Edit Link Edit Table Edit _Source Editing Editing file: %s Enable Version Control? Enabled Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Get more plugins online Get more templates online Git Gnuplot Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide menubar in fullscreen mode Highlight current line Home Page Icon Images Import Page Include subpages Index Index page Inline Calculator Insert Code Block Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Sequence Diagram Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Labels marking tasks Last Modified Leave link to new page Left Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Map document root to URL Match _case Maximum page width Menubar Mercurial Modified Month Move Selected Text... Move Text to Other Page Move column ahead Move column backward Move text to Name Need output file to export MHTML Need output folder to export full notebook New File New Page New S_ub Page... New _Attachment No Applications Found No changes since last version No dependencies No such file: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open in New Window Open in New _Window Open new page Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Name Page Template Page section Paragraph Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please specify a notebook Plugin Plugins Port Position in the window Pr_eferences Preferences Prev Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Remove column Remove row Removing Links Rename page "%s" Replace _All Replace with Restore page to saved version? Rev Right Right Side Pane Right margin position S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Screenshot Command Search Search _Backlinks... Section Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Sequence Diagram Server not started Server started Server stopped Set default text editor Show Line Numbers Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show full Page Name Show full page name Show in the toolbar Show right margin Show the cursor also for pages that can not be edited Show the page title heading in the ToC Single _page Size Smart Home key Some error occurred while running "%s" Sort alphabetically Sort pages by tags Source View Spell Checker Start _Web Server Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Tags for non-actionable tasks Task Task List Template Templates Text Text Files Text From _File... Text background color Text foreground color The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The inline calculator plugin was not able
to evaluate the expression at the cursor. There are no changes in this notebook since the last version that was saved This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a sequence diagram editor for zim based on seqdiag.
It allows easy editing of sequence diagrams.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This plugin turns one section of the notebook into a journal
with a page per day, week or month.
Also adds a calendar widget to access these pages.
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Tray Icon Turn page name into tags for task items Type Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unspecified Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use a custom font Use a page for each Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Zim Desktop Wiki Zoom _Out _About _All Panes _Arithmetic _Back _Browse _Bugs _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Edit _Edit Link _Edit Link or Object... _Edit Properties _Emphasis _FAQ _File _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _Move Here _New Page... _Next _None _Normal Size _Numbered List _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In calendar:week_start:0 horizontal lines no grid lines readonly seconds translator-credits with lines Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2017-04-28 22:02+0000
Last-Translator: Marrin <pimla@adius.ro>
Language-Team: Romanian <ro@li.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n == 1 ? 0: (((n % 100 > 19) || ((n % 100 == 0) && (n != 0))) ? 2: 1));
X-Launchpad-Export-Date: 2020-01-01 13:40+0000
X-Generator: Launchpad (build bceb5ef013b87ef7aafe0755545ceb689ca7ac60)
 		Acest modul afișează un meniu pentru documentele favorite.
		 %(cmd)s
a returnat statusul de ieșire nenul %(code)i %A %d %B %Y au apărut %i erori, verificați înregistrarea %i fișier va fi șters %i fișiere vor fi șterse %i fișiere vor fi șterse %i element deschis %i elemente deschise %i elemente deschise au apărut %i avertismente, verificați înregistrarea <Top> <Necunoscut> Un wiki pentru desktop Fișierul cu numele <b>"%s"</b> există deja.
Puteți utiliza alt nume sau suprascrie fișierul existent. Un tabel trebuie să aibă cel puțin o coloană. Adaugă aplicație Adaugă la Favorite Adaugă notes Adaugă coloană Adaugă noi documente favorite la începutul barei de meniu Adaugă linie de tabel Adaugă suport pentru corectare ortografică folosind gtkspell.

Acesta este un modul de bază furnizat cu zim.
 Aliniază Toate fișierele Toate sarcinile Permite acces public Utilizează mereu ultima poziție a cursorului la deschiderea unei pagini A ocurs o eroare la generarea imaginii.
Doriți totuși să salvați textul-sursă? Sursa adnotată a paginii Aplicații Aritmetic Atașează fișier Atașați imaginea întâi Navigator de atașamente Atașamente Autor Auto indentare Versiune salvată automat din zim Selectează automat cuvântul curent la aplicarea formatului Transformă automat cuvinte "CamelCase" în legături Transformă automat căi de fișier în legături Salvează automat o versiune la intervale regulate Legături inverse Panoul cu legături inverse Suport Bazaar Favorite Panoul de la bază Răsfoire Lis_tă de marcatori C_onfigurare Nu se poate modifica pagina: %s Anulează Capturează tot ecranul Centrat Modifică coloanele Modificări Caractere Caractere excluzînd spațiile Corectare _ortografie Listă de ca_sete Iconiță clasică în zona de notificare,
nu folosi noul stil pentru iconița de notificare pe Ubuntu. Golește Duplică linie de tabel Bloc de cod Comanda Comanda nu modifică date Comentariu Subsol comun inclus Antet comun inclus Notesul complet Configurare aplicații Configurare supliment Configurează o aplicație care să deschidă legăturile „%s” Configurează o aplicație pentru deschiderea fișierelor
de tip „%s” Consideră toate căsuțele de bifare ca sarcini Copiază adresa de e-mail Copiază șablonul Copi_ază ca Copiază le_gătura Copiază _locația N-am putut găsi executabilul „%s” Nu poate fi găsit notesul: %s Nu am putut găsi șablonul „%s” N-a fost găsit vreun fișier sau dosar pentru acest notes Nu am putut încărca corectorul ortografic Nu s-a putut deschide: %s Expresia nu poate fi analizată Nu s-a putut citi: %s Nu poate fi salvată pagina: %s Crează o pagină nouă pentru fiecare notiță Creați dosarul? Creat(ă) De_cupează Unelte personalizate Unel_te personalizate Modifică... Dată Zi Implicit Formatul implicit de copiere a textului din sertarul de memorie Notesul implicit Întîrziere Ștergeți pagina Ștergeți pagina "%s" Șterge linie (rând) Retrogradează Dependențe Descriere Detalii Renunță la notiță? Editare fără distragerea atenției Doriți să restaurați pagina: %(page)s
la versiunea salvată: %(version)s ?

Toate modificările de la ultima versiune salvată vor fi pierdute! Document-rădăcină E_xportă... Editaţi unealta personalizată Editare imagine Editare legătură Editează tabel Editează _sursa Editare Editarea fișierului: %s Activează controlul versiunii? Activat Eroare în %(file)s la linia %(line)i în apropierea „%(snippet)s” Evaluează expresii _matematice Exp_andează tot Exportare Exportă toate paginile într-un singur fișier Export finalizat Exportă fiecare pagină într-un fișier separat Se exportă notesul... Eșuat Eșec la rularea: %s Eșec în rularea aplicației: %s Fișierul există Ș_abloane de fișiere... FIșierul s-a schimbat pe disc: %s Fișierul există Fișierul nu poate fi scris: %s Tip de fișier nesuportat: %s Nume  fişier Filtru Găsește Gă_sește următorul Găsește precedentul Caută și înlocuiește Ce să caute Marchează sarcinile scadente luni sau marți înainte de weekend Dosar Dosarul există deja și are conținut, iar exportarea în acest dosar ar putea suprascrie fișierele existente. Continuați, totuși? Dosarul există: %s Dosar cu șabloane pentru fișiere atașament Pentru căutări avansate puteți utiliza operatori precum
AND, OR și NOT. Citiți pagina de ajutor pentru mai multe detalii. For_matare Formatare Obține mai multe module de pe rețea Obține mai multe șabloane de pe rețea Git Gnuplot Antet _1 Antet _2 Antet _3 Antet _4 Antet _5 Înălțimea Ascunde bara de meniu în modul ecran complet Evidențiază linia curentă Pagină de pornire Iconiţă Imagini Pagina de import Include subpagini Index Pagina de index Calculator în linie Introducere bloc de cod Inserare dată și oră Inserare diagramă Inserare Ditaa Inserare ecuație Inserare GNU R Plot Inserare Gnuplot Inserare imagine Inserează legătură Introducere partitură Introducere captură de ecran Introducere diagramă de secvență Inserare simbol Inserează tabel Inserare text din fișier Interfață Cuvânt-cheie interwiki Jurnal Salt la Salt la pagina Etichete care marchează sarcini Ultima modificare Lasă legătură la noua pagină La stânga Panoul din partea stângă Sortator de linii Linii Harta cu legături Leagă fișierele de documentul-rădăcină cu întreaga cale a fișierului Legătură la Locația Înregistrare evenimente cu Zeitgeist Fișier-jurnal Se pare că ați găsit un defect Setează ca aplicație implicită Harta dosarului rădăcină în legătură Potrivire exa_ctă Lățimea maximă a paginii Bară de meniu Mercurial Modificată Lună Mută textul selectat... Mută textul în altă pagină Mută coloana înainte Mută coloana înapoi Mută textul în Nume Am nevoie de fișierul destinație pentru a exporta în MHTML Am nevoie de dosarul destinație pentru a exporta tot caietul Fișier nou Pagină nouă S_ubpagină nouă _Atașament nou N-a fost găsită nici o aplicație Nici o schimbare de la ultima versiune Fără dependențe Nu există fișierul: %s Nu există vreun wiki definit ca: %s Nu există șabloane instalate Notesul Caiete OK Deschide d_osarul atașamentelor Deschide dosar Deschide notesul Deschide cu... Deschide Documentul-_rădăcină Deschide dosarul _notesurilor Deschidere _pagină Deschide în fereastră noua Deschide într-o _fereastră nouă Deschide pagină nouă Deschide cu „%s” Opțional Opțiuni pentru suplimentul %s Altele... Fișier de ieșire FIșierul destinație există; specificați „--overwrite” pentru a forța exportul Dosarul de ieșire Dosarul destinație există și nu este gol; specificați „--overwrite” pentru a forța exportul Este nevoie de locația destinație pentru export Rezultatul ar trebui să înlocuiască selecția curentă Suprascrie _Bara de căi Pagină Pagina "%s" nu are un dosar pentru atașamente Numele paginii Model de pagină Secțiune de pagină Paragraf Vă rog introduceți un comentariu pentru această versiune Notați faptul că legarea la o pagină inexistentă
creează de asemenea o nouă pagină automat. Selectați vă rog un nume și un dosar pentru notes Specificați caietul Supliment Suplimente Portul Poziția în fereastră Pref_erințe Preferințe Precedent Tipărește în navigator Promovează Proprie_tăți Proprietăți Împinge evenimente spre daemonul Zeitgeist. Notiță rapidă Notiță rapidă... Schimbări recente Schimbări recente... Pagini _schimbate recent Reformatează din mers marcajele wiki Elimină Eliminați toate Șterge coloană Șterge linie de tabel Se șterg legăturile Redenumirea paginii "%s" Înlo_cuiește tot Cu ce să înlocuiască Restaurare pagină la versiunea salvată? Ver La dreapta Panoul din partea dreaptă Poziția marginii din dreapta S_alvează versiune... Salvează o _copie Salvați o copie Salvează versiune Salvează documentele favorite Versiune salvată din zim Scor Comanda pentru capturarea ecranului Caută Caut_ă antelegături Secțiune Selectați fișierul Selectarea dosarului Selectarea imaginii Selectați o versiune pentru a vedea schimbările dintre acea versiune și starea
prezentă. Sau selectați mai multe versiuni pentru a vedea schimbările dintre acele versiuni.
 Selectați formatul pentru export Alegerea fișierului sau dosarului de ieșire Selectați paginile pentru exportare Alege o fereastră sau o regiune Selecţia Diagramă de secvență Serverul nu este pornit Serverul este pornit Serverul este oprit Stabilește editorul de texte implicit Numerotează liniile Arată cuprinsul ca widget flotant și nu ca panou lateral Arată s_chimbările Arată o iconiță separată pentru fiecare caiet. Arată numele întreg al paginii Arată numele întreg al paginii Afișat în bara de unelte Arată marginea din dreapta Arată cursorul și pe paginile ce nu pot fi editate Arată antetul cu titlul paginii în cuprins O singură pagină Mărime: Tastă Home inteligentă A apărut o eroare la rularea „%s” Sortează alfabetic Sortează paginile după etichete Vedere sursă Corector ortografic Pornește serverul _web Si_mbol... Sintaxă Setare implicită Lățimea tab-ului Tabel Editor de Tabel Cuprins Etichete Etichetele pentru sarcinile fără acțiune Sarcină Listă de sarcini Șablon Șabloane Text Fișiere text Text din _fișier... Culoarea de fundal a textului Culoarea textului Dosarul
%s
nu există încă.
Doriți să fie creat acum? Dosarul „%s” nu există încă.
Doriți să fie creat acum? Suplimentul de calculator în linie nu poate
evalua expresia de lângă cursor. Nu există modificări în acest caiet de la ultima versiune cînd a fost salvat. Asta ar putea însemna că nu aveți dicționarele
corect instalate. Fișierul există.
Doriți să-l suprascrieți? Această pagină nu are un dosar pentru atașamente Acest modul permite capturarea ecranului și introducerea imaginii
într-o pagină zim.

Acesta este un modul de bază furnizat cu zim.
 Acest modul adaugă un dialog care afișează toate sarcinile deschise în
acest caiet. Sarcinile deschise pot fi căsuțe de bifare deschise
sau elemente marcate cu etichete cum sînt „TODO” sau „FIXME”.

Acesta este un modul de bază furnizat cu zim.
 Acest modul adaugă un dialog pentru a introduce rapid un text sau
conținutul clipboardului într-o pagină zim.

Acesta este un modul de bază furnizat cu zim.
 Acest modul adaugă o iconiță la zona de notificare
pentru acces rapid.

Acest modul are nevoie de Gtk+ versiunea 2.10 sau mai nouă.

Acesta este un modul de bază furnizat cu zim.
 Acest modul adaugă un extra widget care afișează o listă de pagini
care se leagă la pagina curentă.

Acesta este un modul de bază furnizat cu zim.
 Acest modul adaugă un widget extra care arată un
cuprins pentru pagina curentă.

Acesta este un modul de bază furnizat cu zim.
 Acest modul adaugă configurări care permit folosirea lui zim
ca un editor fără distragerea atenției
 Acest supliment adaugă dialogul 'Inserare simbol' și permite
caractere tipografice auto-formatate.

Acesta este un supliment de bază asociat cu zim.
 Acest modul adaugă suport pentru controlul versiunii în caiete.

Acest modul suportă următoarele sisteme pentru controlul versiunii:
Bazaar, Git și Mercurial.

Acesta este un modul de bază furnizat cu zim.
 Acest modul vă permite să integrați calcule aritmetice în zim.
Este bazat pe modulul aritmetic de la
http://pp.com.mx/python/arithmetic.
 Acest supliment vă permite să evaluați rapid cu zim
expresii matematice simple.

Acesta este un supliment de bază asociat cu zim.
 Acest supliment furnizează un editor de diagrame bazat pe Ditaa

Acesta este un supliment de bază asociat cu zim.
 Acest supliment furnizează un editor de diagrame pentru zim bazat pe GraphViz.

Acesta este un supliment de bază asociat cu zim.
 Acest supliment furnizează un dialog cu reprezentarea
grafică a structurii cu legături a unui notes.
El poate fi utilizat în genul unei "hărți mentale"
ce arată cum se relaționează paginile.

Acesta este un supliment de bază asociat cu zim.
 Acest modul furnizează un index de pagină filtrat după modalitatea
selectării etichetelor dintr-un nor.
 Acest supliment furnizează un editor grafic pentru zim bazat pe GNU R.
 Acest supliment furnizează un editor grafic bazat pe Gnuplot.
 Acest modul furnizează un editor de diagrame de secvență pentru zim bazat pe seqdiag.
Permite editarea ușoară a diagramelor de secvență.
 Acest modul furnizează o soluție alternativă pentru lipsa
suportului de printare în zim. Exportă pagina curentă
în html și deschide un navigator. Presupunînd că navigatorul
permite posibilitatea de printare, asta vă permite să vă trimiteți
datele către împrimantă în doi pași.

Acesta este un modul de bază furnizat cu zim.
 Acest supliment furnizează un editor de ecuații pentru zim bazat pe latex.

Acesta este un supliment de bază asociat cu zim.
 Acest plugin furnizează un editor de partituri pentru zim bazat pe GNU Lilypond.

Acesta este un modul de bază furnizat cu zim.
 Acest modul afișează directorul cu fișiere atașate paginii curente, 
ca un simbol în panoul de jos.
 Acest supliment sortează liniile selectate în ordine alfabetică.
Dacă lista este deja sortată, ordonarea va fi inversată
(A-Z to Z-A).
 Acest modul transformă o secțiune din caiet într-un jurnal
cu o pagină pe zi, săptămînă sau lună.
Adaugă de asemenea un widget pentru a accesa aceste pagini.
 Aceasta înseamnă de regulă că fișierul conține caractere invalide Titlu Pentru a continua, puteți salva o copie a acestei pagini sau neglijați
toate modificările. Dacă salvați o copie, modificările vor fi de asemenea
neglijate, dar veți putea restaura copia ulterior. Pentru a începe un caiet de notițe nou, trebuie să selectați un director nou (gol).
Bineînțeles, puteți selecta și un director preexistent, care conține deja un caiet de notițe.
 Cuprins La _ziua Astăzi Comută căsuța de verificare 'V' Comută căsuța de verificare 'X' Panoul de sus Iconiță pentru zona de notificare Transformă numele de pagini în etichete pentru elemente de sarcini Tip De-indentare cu tasta <Backspace>
(Dacă debifați, de-indentați cu <Shift><Tab>) Necunoscut(ă) Nespecificat Neetichetate Se actualizează %i legătură de pagină la această pagină Se actualizează %i legături de pagină la această pagină Se actualizează %i de legături de pagină la această pagină Se actualizează antetul acestei pagini Se actualizează legăturile Se actualizează indexarea Utilizează font personalizat Utilizează o pagină pentru fiecare Utilizați tasta <Enter> pentru a urma legătura
(Dacă debifați, utilizați <Alt><Enter> pentru aceasta) Controlul versiunii Controlul versiunii nu este activat pentru acest caiet.
Doriți activarea? Versiuni Vedere _adnotări Arată _jurnalul Server Web Săptămână Când raportați această avarie includeți vă rog
informația din caseta de text de dedesubt Cu_vânt întreg Lățimea Pagina de wiki: %s Contor de cuvinte Numărător de cuvinte... Cuvinte An Ieri Editați fișierul cu o aplicație externă. Puteți închide acest dialog când terminați Puteţi configura uneltele personalizate care apar
în meniul de unelte şi în bara de unelte sau în meniurile contextuale. Zim Desktop Wiki Micș_orează _Despre aplicație To_ate panourile _Aritmetic Îna_poi _Răsfoiește O_bstacole _Copil _Curăță formatarea În_chide Res_trânge tot _Conținuturi _Copiază _Copiază aici Șt_erge _Șterge pagina Neglijați mo_dificările _Editează Editează _legătura _Editează legătura sau obiectul... _Editează proprietățile _Accentuat Întrebări _frecvente _Fișier _Găsește _Înaintează Pe tot _ecranul Navi_gare A_jutor _Evidențiază _Istoric _Acasă _Imagine... _Importă pagina _Inserare Sari _la... Sc_urtături de taste _Legătură Legătură la _dată _Legătură... S_ubliniat _Mai multe _Mută _Mută aici Pagină _nouă _Următor _Nimic Mărime _normală Listă numerală _Deschide Deschide alt n_otes _Altele... _Pagină Ierarhizarea _paginilor _Părinte Li_pește _Previzualizați _Precedent Ti_părește în navigator _Notiță rapidă... P_ărăsește aplicația Pagini _recente _Refă Expresie _regulată _Reîncarcă Ște_rge legătura În_locuiește Înlocui_re... _Resetați mărimea _Restaurare versiune _Salvare _Salvați copia _Captură de ecran... _Căutare _Caută... Trimite _spre... _Panourile laterale _Una lîngă alta _Sortează linii _Tăiat În_groșat _Indice E_xponent Ș_abloane Unel_te _Desfă _Literal _Versiuni... _Vizualizare _Mărește calendar:week_start:1 linii orizontale fără linii despărțitoare doar în citire secunde Launchpad Contributions:
  Adrian Fita https://launchpad.net/~afita
  Arthur Țițeică https://launchpad.net/~arthur-titeica
  Jaap Karssenberg https://launchpad.net/~jaap.karssenberg
  Marrin https://launchpad.net/~pimla
  abelcavasi https://launchpad.net/~abel-cavasi cu linii despărțitoare 