��    e     D              l&  .   m&  ?   �&     �&     �&     '  0   !'     R'     m'  	   �'     �'  i   �'  *   (     9(     I(     V(     c(  
   ~(  -   �(     �(  V   �(     )  	   )  	   &)     0)  3   D)  Y   x)     �)     �)  
   �)      *     *      *     3*     F*     R*     _*     f*  $   u*  ?   �*  /   �*  (   
+     3+  %   P+     v+  	   �+     �+     �+  
   �+     �+  	   �+     �+     �+     �+     �+  
   �+     ,     ,      ,     5,     <,     K,  
   S,     ^,     z,     �,     �,     �,     �,  <   �,     -  	   -  
   -     *-     3-     ;-     X-     `-     s-     �-  +   �-  3   �-      �-     .     !.     4.     B.  
   N.     Y.     h.     �.     �.  3   �.     �.     /     %/     @/     S/     k/     �/     �/     �/     �/     �/     �/     �/     �/  0   �/     	0     0      0     ,0  
   >0     I0     P0     ]0     i0     q0     y0     �0  $   �0  ~   �0     D1     R1  
   V1     a1     i1  
   z1  	   �1  
   �1     �1     �1     �1     �1     �1     �1     �1  5   �1     -2     <2     H2  !   O2     q2  #   �2     �2     �2     �2     �2     �2     �2     3     )3     53     N3     j3     s3     z3  
   3     �3     �3  	   �3  6   �3     �3  v   �3     i4  *   {4  c   �4     
5     5     5      5     85     R5     V5  
   ^5  
   i5  
   t5  
   5  
   �5  
   �5     �5     �5     �5     �5  	   �5     6     6     6      6     ,6     =6  
   C6     N6     `6     r6     �6     �6     �6     �6     �6     �6     �6     �6     �6     7     $7     27     ?7  	   U7     _7     q7     y7     �7     �7     �7     �7     �7     �7     �7     �7     �7  2   �7     *8     28     ;8     U8     ^8     y8     �8     �8     �8     �8     �8     �8  	   9     9     9     9     49     L9     ^9     s9     �9      �9  *   �9     �9     �9     �9     �9     :     :     :     .:     L:  *   \:  2   �:     �:     �:     �:     �:     ;  	   ;     ;     ";     9;     R;     ^;     l;     y;     �;  
   �;     �;  	   �;     �;     �;     �;     <     <     '<     0<     F<     O<  9   [<     �<  I   �<  !   �<  '   =  	   7=     A=     J=  0   O=  
   �=  	   �=     �=     �=     �=     �=  	   �=     �=  '   �=  V   >  3   l>  0   �>  (   �>     �>     ?  .   ?     J?     R?     W?     n?     {?     �?     �?     �?  
   �?  &   �?  
   �?     �?     �?     @     @     0@     P@  
   W@     b@  
   p@     {@     �@  ?   �@     �@     �@     �@     A     A     A     .A     DA     MA     TA     eA  	   uA     A     �A     �A     �A     �A     �A     �A     �A     �A     B     B     ,B     8B     FB  �   SB     �B      �B     C     8C  	   PC     ZC     kC     ~C     �C     �C     �C     �C     �C     �C  2   �C     2D  &   @D     gD     {D     �D     �D     �D     �D  5   �D  &   E     @E     ME     RE  &   aE     �E     �E     �E     �E     �E     �E  
   �E     �E     �E  	   F     F     F     F     1F     6F     TF  	   YF     cF     iF  	   rF     |F  
   �F     �F     �F     �F  ?   �F  A   G  �  MG  S   I  =   eI  K   �I  @   �I  6   0J  -   gJ  I   �J  x   �J  �   XK  �   �K  �   �L  �   MM  �   �M  }   \N  L   �N  �   'O  9   �O  �   �O  �   �P  }   "Q  C   �Q  h   �Q  k   MR  �   �R  R   �S  ;   �S  =   T  v   ZT    �T  j   �U  n   PV  ]   �V     W  �   �W  7   2X     jX  �   pX  |   Y     �Y     �Y     �Y     �Y     �Y     �Y     �Y  	   �Y  '   �Y     Z     Z  D   (Z     mZ     uZ     �Z     �Z     �Z  H   �Z     �Z     [     "[  !   1[     S[     e[     y[  P   �[     �[  U   �[     L\     U\  	   e\  
   o\     z\  N   \     �\     �\     �\  
   �\     �\     ]     ]  	   ]  ^   ]  f   {]     �]  	   �]     �]     ^  
   	^     ^      ^     &^     .^     4^  	   <^     F^     M^     _^     f^  	   t^     ~^  
   �^     �^     �^     �^     �^     �^  
   �^     �^     �^     �^  	   _     _     _     _     #_     ,_     5_     A_     E_  
   K_     V_     __  	   e_     o_     _     �_     �_     �_     �_     �_     �_     �_     �_     �_  
   �_     �_     �_     �_     `     `     #`     )`     6`     E`     I`     O`  	   i`     s`     y`     �`     �`     �`  	   �`     �`     �`     �`     �`     �`     �`     �`     a     	a     a     a     +a     4a     @a     La     ]a  
   ca     na     }a  
   �a     �a     �a     �a     �a     �a     �a  
   �a     �a  
   �a     �a     �a  	   b     b     b     b     (b     >b     Vb  
   lb     wb     �b     �b     �b     �b     �b  
   �b     �b  �  �b     �d  B   �d     e     +e     @e  7   Ye  !   �e     �e     �e     �e  r   �e  $   Yf     ~f     �f     �f  (   �f     �f  .   �f     *g  Z   9g     �g  
   �g     �g     �g  C   �g  G   h     _h     h  	   �h     �h  
   �h     �h     �h     �h     �h     �h     �h  !   	i  4   +i  2   `i  +   �i  %   �i  ,   �i     j     /j     =j     Vj     gj     xj  
   j  	   �j     �j     �j     �j     �j     �j     �j     �j     �j     k  
   k     k     %k     @k     Vk     lk     �k     �k  Z   �k     l  	   l  	   l     l     %l     .l  	   Jl     Tl     kl     �l  -   �l  /   �l  "   �l     m     m     2m     ?m     Lm     [m  (   jm  #   �m     �m  :   �m  "   n     2n     Hn     cn     xn  )   �n     �n  	   �n     �n     �n     �n     �n     o     o  5   o     Ho     _o     mo     zo     �o     �o  	   �o     �o     �o     �o     �o     �o      �o  �   p     �p     �p     �p     �p      �p     q     q     $q     4q  
   Gq     Rq     fq     �q  	   �q     �q  /   �q     �q  
   �q  	   �q  $   r     ,r  '   >r     fr     �r     �r     �r     �r  
   �r     �r     �r     s     s     )s     1s     8s     =s     Js     ]s  
   ns  I   ys     �s  u   �s     At     Tt  w   qt     �t     �t     �t  '   �t     'u     Fu     Ju     Ru  	   Zu  	   du  	   nu  	   xu  	   �u     �u  !   �u  "   �u     �u     �u     �u     
v     v     v     %v     :v     @v  
   Mv     Xv     iv     ~v     �v     �v     �v     �v     �v     �v     �v     �v     w     w     1w     ?w     Uw     aw     zw  
   �w     �w  "   �w     �w     �w     �w     �w      x     x  
   x  3    x     Tx     `x     fx     �x  &   �x      �x     �x      �x     y      y     ;y     Ly  	   Ty     ^y     fy     my     �y     �y     �y     �y     �y  4   �y  G   z     fz     mz     uz     �z  
   �z     �z     �z  '   �z     �z  ;   �z  C   -{     q{     �{  !   �{     �{     �{     �{     �{     �{  #   |     =|     L|     b|     p|  !   �|     �|  "   �|     �|     �|     �|     }     "}     >}     N}  "   U}     x}  	   �}  <   �}     �}  R   �}  #   )~  #   M~     q~     }~     �~  .   �~     �~     �~     �~     �~     �~            
      )   +  T   U  0   �  4   �  "   �     3�     J�  6   X�     ��     ��     ��     ��     ʀ     ـ     �     ��  
   �  8   �     L�     ]�     q�     ��     ��  %   ��     ف     �     �     ��     	�     �  @   6�     w�     ��  %   ��     ��     ��          ς     �     �     ��     �  	   �     $�     2�     C�     Y�     b�     {�     ��     ��     ��     ��     ̃  	   ߃     �  
   ��  �   �     ��      ��  $   ˄     ��  	   �     �     %�     7�     F�     U�  !   d�     ��     ��     ��  K   ҅     �  &   .�     U�     k�  !   ��     ��     ��     Ά  4   �  2   "�     U�     a�     i�  *   y�     ��     ��     Ӈ     ߇     �     ��     	�     �  !   "�     D�     M�     T�     e�  	   |�  7   ��     ��     ƈ  	   Ԉ     ވ     �     �  	   �     ��     �  	   �  +   %�  -   Q�  �  �  O   <�  G   ��  G   ԋ  =   �  0   Z�  4   ��  P   ��  �   �  �   ��    b�  �   h�  �   �  �   ̐  �   X�  K   �  �   4�  ?   �  �   0�  �   ԓ  �   d�  ]   �  p   `�  �   ѕ  �   Z�  l   E�  P   ��  C   �  �   G�    ޘ  �   ��  �   ��  V   �  �   j�  �   �  <   ��     ��  �   �  �   ɝ  	   T�     ^�     d�     j�     �     ��     ��     ��  +   Ξ     ��     �  Y   �     o�     v�     ��     ��  
   ��  d   ��  %   �     7�     J�  +   h�     ��     ��      ɠ  _   �     J�  X   \�  	   ��     ��  
   ѡ  
   ܡ     �  a   ��     O�     e�     k�  
   x�     ��     ��     ��     ��  `   ��  Y   �     \�  	   m�     w�     {�     ��  
   ��     ��  	   ��     ��     ��  
   ƣ     ѣ     ݣ     �     ��  
   �     �     �     %�     .�     <�     R�  	   a�     k�     {�     ��     ��     ��     Ť     פ     ޤ     �     �  
   ��     �     �     �  	   �     "�     '�     0�     @�     H�     O�     [�     k�     r�  	   ��     ��     ��     ��     ��     ��     å     ե     �     �     ��     ��     �     �     !�     )�  	   C�     M�     S�     `�     l�     x�     ��  	   ��     ��     ��     Φ     צ     �     �     �     �     �     !�     0�     9�     E�     Z�     o�     v�     }�     ��     ��     ��     ��     ��     ǧ     ֧     �  
   �  	   ��     �     �     �  	   �     '�     5�  	   ;�  !   E�     g�     ��     ��     ��     ��     Ȩ     Ԩ  �  ݨ     ̪  	   ܪ     �   %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i Attachment %i Attachments %i errors occurred, see log %i file will be deleted %i files will be deleted %i open item %i open items %i warnings occurred, see log <Unknown> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Add Application Add Bookmark Add Notebook Add bookmark/Show settings Add column Add new bookmarks to the beginning of the bar Add row Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files All Tasks Allow public access Always use last cursor position when opening a page An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Arithmetic Ascii graph (Ditaa) Attach File Attach image first Attachment Browser Attachments Attachments: Author Auto indenting Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave interval in minutes Autosave version on regular intervals Back to Original Name BackLinks BackLinks Pane Backend Backlinks: Bazaar Bookmarks Border width Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Capture whole screen Center Change columns Changes Characters Characters excluding spaces Check Checkbox '>' Check Checkbox 'V' Check Checkbox 'X' Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Clone row Code Block Column 1 Command Command does not modify data Comment Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Demote Dependencies Description Details Diagram Discard note? Distraction Free Editing Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root Due E_xport... Edit %s Edit Custom Tool Edit Image Edit Link Edit Table Edit _Source Editing Editing file: %s Enable Version Control? Enable plugin Enabled Equation Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Fossil Get more plugins online Get more templates online Git Gnuplot Grid lines Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide Journal pane if empty Hide menubar in fullscreen mode Highlight current line Home Page Horizontal _Line Icon Images Import Page Include subpages Index Index page Inline Calculator Insert Code Block Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Sequence Diagram Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Labels marking tasks Last Modified Leave link to new page Left Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Managing table columns Map document root to URL Match _case Maximum number of bookmarks Maximum page width Menubar Mercurial Modified Month Move Selected Text... Move Text to Other Page Move column ahead Move column backward Move text to Name Need output file to export MHTML Need output folder to export full notebook New File New Page New Page in %s New S_ub Page... New _Attachment Next No Applications Found No changes since last version No dependencies No document root defined for this notebook No plugin available to display objects of type: %s No such file: %s No such page: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Only Show Active Tasks Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open cell content link Open help Open in New Window Open in New _Window Open new page Open plugins folder Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Index Page Name Page Template Page already exists: %s Page not allowed: %s Page section Paragraph Paste Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please select a row, before you push the button. Please select more than one line of text Please specify a notebook Plugin Plugin "%s" is required to display this object Plugins Port Position in the window Pr_eferences Preferences Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Remove column Remove row Removing Links Rename page "%s" Repeated clicking a checkbox cycles through the checkbox states Replace _All Replace with Restore page to saved version? Rev Right Right Side Pane Right margin position Row down Row up S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Screenshot Command Search Search _Backlinks... Search this section Section Section(s) to ignore Section(s) to index Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Sequence Diagram Server not started Server started Server stopped Set New Name Set default text editor Set to Current Page Show Line Numbers Show Tasks as Flat List Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show full Page Name Show full page name Show helper toolbar Show in the toolbar Show right margin Show tasklist in sidepane Show the cursor also for pages that can not be edited Show the page title heading in the ToC Single _page Size Smart Home key Some error occurred while running "%s" Sort alphabetically Sort pages by tags Source View Spell Checker Start Start _Web Server Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Tags for non-actionable tasks Task Task List Tasks Template Templates Text Text Files Text From _File... Text background color Text foreground color The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The following parameters will be substituted
in the command when it is executed:
<tt>
<b>%f</b> the page source as a temporary file
<b>%d</b> the attachment directory of the current page
<b>%s</b> the real page source file (if any)
<b>%p</b> the page name
<b>%n</b> the notebook location (file or folder)
<b>%D</b> the document root (if any)
<b>%t</b> the selected text or word under cursor
<b>%T</b> the selected text including wiki formatting
</tt>
 The inline calculator plugin was not able
to evaluate the expression at the cursor. The table must consist of at least on row!
 No deletion done. There are no changes in this notebook since the last version that was saved This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This page name cannot be used due to technical limitations of the storage This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a "path bar" to the top of the window.
This "path bar" can show the notebook path for the current page,
recent visited pages or recent edited pages.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds the page index pane to the main window.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin also has properties,
see the notebook properties dialog This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a sequence diagram editor for zim based on seqdiag.
It allows easy editing of sequence diagrams.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This plugin turns one section of the notebook into a journal
with a page per day, week or month.
Also adds a calendar widget to access these pages.
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox '>' Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Tray Icon Turn page name into tags for task items Type Un-check Checkbox Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unkown Image type Unkown Object Unspecified Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use a page for each Use date from journal pages Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Zim Desktop Wiki Zoom _Out _About _Add _All Panes _Arithmetic _Back _Browse _Bugs _Cancel _Checkbox _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Duplicate Line _Edit _Edit Link _Edit Link or Object... _Edit Properties _Edit... _Emphasis _FAQ _File _Find _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _Move Here _Move Line Down _Move Line Up _New Page Here... _New Page... _Next _None _Normal Size _Numbered List _OK _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Print _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove _Remove Line _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In as due date for tasks as start date for tasks calendar:week_start:0 do not use horizontal lines no grid lines readonly seconds translator-credits vertical lines with lines {count} of {total} Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2019-11-28 04:34+0000
Last-Translator: Jonatan Nyberg <Unknown>
Language-Team: Swedish <sv@li.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
X-Generator: Launchpad (build bceb5ef013b87ef7aafe0755545ceb689ca7ac60)
 %(cmd)s
gav felkod %(code)i %(n_error)i fel och %(n_warning)i varningar inträffade, se loggen %A den %d %B %Y %i bilaga %i bilagor %i fel hittades, se logg %i fil kommer att tas bort %i filer kommer att tas bort %i öppet objekt %i öppna objekt %i varning(ar), se logg <Okänd> En skrivbordswiki En fil med namnet <b>"%s"</b> finns redan.
Du kan ange ett annat filnamn eller skriva över den existerande filen. En tabell måste ha minst en kolumn. Lägg till program Lägg till bokmärke Lägg till anteckningsbok Lägg till bokmärke/Visa inställningar Lägg till kolumn Lägg till nya bokmärken i början av fältet Lägg till rad Aktiverar stavningskotroll med hjälp av gtkspell.
Detta tilläggsprogram medföljer zim.
 Justera Alla filer Alla aktiviteter Tillåt åtkomst för alla Återvänd alltid till senaste markörposition när en sida öppnas Ett fel uppstod när bilden skapades.
Vill du spara källtexten ändå? Sidans källkod med kommentarer Program Aritmetik Ascii-diagram (Ditaa) Bifoga fil Bifoga bild först Bilage-bläddrare Bilagor Bilagor: Användarnamn Automatiskt indrag Automatiskt sparad version av zim Välj det aktuella ordet automatiskt vid formatering Gör automatiskt om ord i "CamelCase" till länkar Gör automatiskt om sökvägar till länkar Spara automatiskt intervall i minuter Spara automatiskt med jämna tidsintervaller Tillbaka till originalnamnet Bakåtlänkar Panel för bakåtlänkar Bakgrundsprogram Tillbakalänkar: Bazaar Bokmärken Kantbredd Bottenpanel Bläddra Punk_tlista K_onfigurera Kan inte redigera sidan: %s Avbryt Fånga hela skärmen Centrera Ändra kolumner Ändringar Tecken Tecken exklusive mellanrum Markera kryssruta '>' Markera kryssruta 'V' Markera kryssruta 'X' Kontrollera _stavning kryssrutelista Klassisk ikon för aktivititetsfältet.
Använd inte den nya typen av statusikon i Ubuntu. Rensa Klona rad Kodstycke Kolumn 1 Kommando Kommandot ändrar inte data Kommentar Hela _anteckningsboken Konfigurera programmet Konfigurera modul Konfigurera progammet att öppna "%s" länkar Konfigurera programmet att öppna
filtypen "%s" Se alla kryssrutor som aktiviteter Kopiera Kopiera e-postadress Kopiera mall Kopiera _som Kopiera _länk Kopiera _plats Kunde inte hitta den körbara filen "%s" Kunde inte hitta anteckningsbok: %s Kunde inte hitta mallen "%s" Kunde inte hitta filen eller platsen för anteckningsboken Kunde inte ladda stavningskontroll Kunde inte öppna: %s Kunde inte tolka uttrycket Kunde inte läsa: %s Kunde inte spara sida: %s Skapa en ny sida för varje ny anteckning Skapa katalog? Klipp _ut Egna verktyg Egna _verktyg Anpassar... Datum Dag Standardval Standardformat för att kopiera text till klippbordet Standardanteckningsbok Fördröjning Ta bort sida Ta bort sidan ”%s” Ta bort rad Lägre nivå Beroenden Beskrivning Detaljer Diagram Kasta anteckning? Störningsfri redigering Vill du ta bort alla bokmärken? Vill du återskapa sidan: %(page)s
till den sparade versionen: %(version)s ?
Alla ändringar efter att du senast sparade kommer att förloras! Dokumentets root Förfallodatum E_xportera... Redigera %s Inställningar för eget verktyg Redigera bild Redigera länk Redigera tabell R_edigera källkod Redigering Redigerar filen: %s Aktivera versionshantering? Aktivera insticksmodul Aktiverad Ekvation Fel i %(file)s i rad %(line)i vid "%(snippet)s" Utvärdera _Matematik _Visa alla Exportera Exportera alla sidor som en enda fil Exporten är klar Exportera varje sida som en separat fil Exporterar anteckningsbok Misslyckades Kunde inte starta %s Kunde inte starta %s Filen finns redan _Filmallar Filen ändrades på disken: %s Filen finns redan Skrivfel: %s Filtypen stöds ej: %s Filnamn Filter Sök Sök _nästa Sök _föregående Sök och ersätt Sök efter Markera aktiviteter som inträffar på måndag eller tisdag, innan helgen Katalog Katalogen finns redan och innehåller filer, att exportera hit kan skriva över befintliga filer. Vill du fortsätta? Mapp existerar: %s Mapp med mallar för bilagor För avancerad sökning kan du använda operatorer som
AND, OR och NOT. Se hjälpsidorna för utförligare beskrivning. _Format Format Fossil Hitta fler insticksmoduler på internet Hitta fler mallar på internet Git Gnuplot Rutnät Rubrik _1 Rubrik _2 Rubrik _3 Rubrik _4 Rubrik _5 Höjd Dölj journalpanel om den är tom Dölj menyraden i fullskärmsläge Färgmarkera aktuell rad Webbsida Horisontell _rad Ikon Bilder Importera sida Inkludera undersidor Index Registersida Kalkylator Infoga kodstycke Infoga datum och tid Infoga diagram Infoga Ditaa Infoga ekvation Infoga GNU R Plot Infoga Gnuplot Infoga bild Infoga länk Infoga partitur Infoga skärmbild Infoga sekvensdiagram Infoga specialtecken Infoga tabell Infoga text från fil Gränssnitt Nyckelord för interwiki Dagbok Hoppa till Hoppa till sida Etiketter som markerar aktiviteter Senast ändrad Lämna länk till ny sida Vänster Vänster panel Sortera linjer Rader Länkkarta Länka till filer i dokumentroten med full sökväg Länka till Plats Logga händelser med Zeitgeist Loggfil Det verkar som om du har hittat en bug Sätt programmet som standardval Hantera tabellkolumner Tilldela dokumentets root en URL Matcha _små/stora Högsta antalet bokmärken Maximal sidbredd Menyrad Mercurial Ändrad Månad Flytta markerad text... Flytta text till en annan sida Flytta kolumnen framåt Flytta kolumnen bakåt Flytta text till Namn Du måste ange en fil för att kunna exportera MHTML Du måste ange en folder för att kunna exportera hela anteckningsboken Ny fil Ny sida Ny sida i %s Ny _undersida Ny _bilaga Nästa Hittade inte programmet Inga ändringar sedan senaste versionen Inga beroenden Ingen dokumentrot definierad för den här anteckningsboken Inget insticksmoduler tillgängligt för att visa objekt av typ: %s Filen saknas: %s Ingen sådan sida: %s Ingen sådan wiki har angetts: %s Inga mallar är installerade Anteckningsbok Anteckningsböcker OK Visa endast aktiva uppgifter Öppna katalog för _bifogade filer Öppna katalog Öppna anteckningsbok Öppna med... Öppna dokument_rot Öppna _anteckningsbokens katalog Öppna _sida Öppna cellens innehållande länk Öppna hjälp Öppna i nytt fönster Öppna i _nytt fönster Öppna ny sida Öppna insticksmodulsmappen Öppna med "%s" Valfri Alternativ för insticksmodulen %s Annan... Exportfil Filen finns redan. Ange "--overwrite" för att tvinga export Utdatakatalog Mappen finns redan och den är inte tom. Ange "--overwrite" för att tvinga export Ange plats för att kunna exportera Utskrift skall ersätta markeringen Skriv över S_ökvägsfält Sida Sidan "%s" har ingen mapp för bifogade filer. Sidindex Sidnamn Sidmall Sidan finns redan: %s Sidan är inte tillåten: %s Sidaavsnitt Stycke Klistra in Ange en kommentar för den här versionen Observera att länkning till en icke existerande sida
automatiskt skapar en ny sida. Välj ett namn och en mapp för anteckningsboken Vänligen välj en rad innan du trycker på knappen. Vänligen välj mer än en textrad Ange en anteckningsbok Insticksmodul Insticksmodulen "%s" krävs för att visa detta objekt Insticksmoduler Port Placering i fönstret _Inställningar Inställningar Skriv till webläsare Högre nivå _Egenskaper Egenskaper Bestämmer vilka händelser som ska loggas av Zeitgeist. Snabb anteckning Snabb anteckning... Senaste ändringar Senaste ändringarna... Senast _ändrade sidor Omformatera wiki markup kontinuerligt Ta bort Ta bort alla Ta bort kolumn Ta bort rad Tar bort länkar Byt namn på sidan ”%s” Upprepade klick på en kryssruta cykler genom kryssrutans lägen Ersätt _alla Ersätt med Återställ sida till sparad version? Ver Höger Höger panel Högermarginalens position Rad ner Rad upp Sp_ara version... Spara so_m... Spara som Spara version Spara bokmärken Sparad version av zim Resultat Kommando för skärmdump Hitta Sök _bakåtlänkar Sök i det här avsnittet Sektion Avsnitt att ignorera Avsnitt till index Välj fil Välj katalog Välj bild Välj en version för att se ändringar mellan den versionen och den aktuella.
Du kan också välja flera versioner och se ändringar mellan dessa.
 Välj exportformat Exportera till fil eller katalog Välj vilka sidor som ska exporteras Välj fönster eller region Markering Sekvensdiagram Server ej startad Server startad Server stoppad Ange nytt namn Välj textredigerare som standard Ställ in till nuvarande sida Visa radnummer Visa uppgifter som platt lista Visa innehållsförteckning som en flyttbar widget istället för sidopanel Visa ändringar Visa en ikon för varje anteckningsbok Visa hela sidans namn Visa hela sidans namn Visa verktygsfält för hjälpare Visa på verktygsraden Visa högermarginal Visa uppgiftslista i sidopanel Visa pekaren också på sidor som inte kan redigeras Visa sidans titelrubrik i innehållsförteckningen Enkel _sida Storlek Smart hemnyckel Ett fel inträffade vid körningen av "%s" Sortera alfabetiskt Sortera sidor efter taggar Visa källa Stavningskontroll Start Starta_webbserver Spe_cialtecken... Syntax Operativsystemets standardprogram Tabbredd Tabell Tabellredigerare Innehållsförteckning Etiketter Etiketter för aktiviteter som inte behöver åtgärdas Uppgift Uppgiftslista Uppgifter Mall Mallar Text Textfiler Text från _fil... Teckenbakgrund Textfärg Mappen
%s
finns inte.
Vill du skapa den nu? Mappen "%s" finns inte.
Vill du skapa den nu? Följande parametrar kommer att ersättas
i kommandot när det körs:
<tt>
<b>%f</b> sidkällan som en tillfällig fil
<b>%d</b> bifogarkatalogen på den aktuella sidan
<b>%s</b> den verkliga sidan källfilen (om någon)
<b>%p</b> sidnamnet
<b>%n</b> plats för bärbara datorer (fil eller mapp)
<b>%D</b> dokumentroten (om någon)
<b>%t</b> markerad text eller ord under markör
<b>%T</b > den markerade texten inklusive wikiformatering
</tt>
 Kalkylator-tilläggsprogrammet kunde
inte utvärdera uttrycket under markören. Tabellen måste bestå av åtminstone på rad!
 Ingen borttagning klar. Inga ändringar har gjorts i anteckningsboken sedan den senast sparades Detta kan bero på att du inte har rätt
ordlista installerad Denna fil finns redan.
Vill du skriva över den? Den här sidan har ingen katalog för bifogade filer Detta sidnamn kan inte användas på grund av tekniska begränsningar av lagring Detta tilläggsprogram låter dig ta en skärmdump och infoga den
på en sida i Zim.

Detta är ett kärnprogram som följer med Zim.
 Denna insticksmodul lägger till ett "sökfält" längst upp i fönstret.
Detta "sökfält" kan visa anteckningssökväg för den aktuella sidan,
nyligen besökta sidor eller nyligen redigerade sidor.
 Detta tilläggsprogram visar alla oavslutade aktiviteter i den här
anteckningsboken. En oavslutad aktivitet kan vara antingen omarkerade kryssrutor
eller objekt markerade med etiketter som "TODO" eller "FIXME".

Detta är ett kärnprogram som följer med Zim.
 Detta tilläggsprogram tillhandahåller ett sätt att snabbt lägga in text
eller ett urklipp på en sida i Zim.

Detta är ett kärnprogram som följer med Zim.
 Detta tilläggsprogram lägger till en ikon i aktivitetsfältet för lätt tillgänglighet.

Programmet kräver Gtk+ version 2.10 eller senare.

Detta är ett kärnpogram som följer med Zim.
 Insticksmodul för widget, som visar en lista på sidor
som länkar till den aktuella sidan.

Det här är en basmodul som kommer med Zim.
 Insticksmodul som infogar en extra widget med
en innehållsförteckning för den aktuella sidan.

Det här är en basmodul som kommer med Zim.
 Insticksmodul som låter dig ställa in Zim
för störningsfri redigering.
 Detta tilläggsprogram tillhandahåller en dialogruta för att infoga specialtecken och tillåter
autoformatering av typografiska tecken.

Detta är ett kärnprogram som följer med Zim.
 Denna insticksmodul lägger sidindexrutan till huvudfönstret.
 Insticksmodul för versionshantering av anteckningsböcker.

Insticksmodulen hör stöd för Bazaar, Git och Mercurial.

Detta är en basmodul som kommer med Zim.
 Insticksmodul för att infoga aritmetiska beräkningar i Zim.
Den baseras på den aritmetiska modulen från
http://pp.com.mx/python/aritmetic.
 Det här tilläggsprogrammet tillåter dig att snabbt
utvärdera enkla matematiska uttryck i zim.

Det här är ett bas-tilläggsprogram som kommer med zim.
 Den här insticksmodulen har också egenskaper,
se dialogrutan egenskaper för anteckningsbok Insticksmodul för att redigera diagram i Zim, baserat på Ditaa.

Det här är en basmodul som kommer med Zim.
 Detta tilläggsprogram tillhandahåller en diagramredigerare för zim som baseras på GraphViz

Detta tilläggsprogram följer med zim.
 Detta tilläggsprogram öppnar en grafisk
representation av länkstrukturen i din
anteckningsbol. Det kan användas som en sorts "mind map"
som visar hur sidor relaterar till varandra.

Detta är ett kärnprogram som följer med Zim.
 Denna insticksmodul tillhandahåller ett sidindex som filtreras med hjälp av att välja taggar i ett moln.
 Detta tilläggsprogram tillhandahåller en diagramredigerare baserad på GNU R.
 Insticksmodul för att redigera grafer i Zim, baserat på Gnuplot.
 Den här insticksmodulen ger möjlighet att redigera sekvensdiagram i Zim, baserat på seqdiag.
Det är ett enkelt sätt att redigera sekvensdiagram.
 Detta tilläggsprogram ersätter bristen på
utskriftsstöd i Zim. Det exporterar den aktuella sidan
till html och öppnar en webläsare. Om webläsaren 
har stöd för utskrifter  kommer detta ge möjligheten
att skriva ut i två steg.

Detta är ett kärnprogram som följer med Zim.
 Detta tilläggsprogram tillhandahåller en ekvationsredigerare baserad på latex.

Detta är ett kärnprogram som följer med Zim.
 Den här insticksmodulen ger dig en partiturredigerare för Zim, baserad på GNU Lilypond.

Det här är en basinsticksmodul som kommer med Zim.
 Denna plugin visar mappen bilagor på den aktuella sidan som en
ikonvy i nedre rutan.
 Insticksmodul för att sortera markerade rader i alfabetisk ordning.
Om listan redan är sorterade kommer ordningen att bli omvänd
(från A-Ö till Ö-A).
 Insticksmodul som förvandlar en sektion i anteckningsboken till en dagbok
med en sida per dag, vecka eller månad.
Den lägger också till en kalenderwidget för att nå dessa sidor.
 Det är vanligen för att filen innehåller felaktiga tecken Titel För att fortsätta måste du spara en kopia av den här sidan eller förkasta
alla ändringar. Om du sparar en kopia kommer ändringarna också
att förkastas, men du kan återskapa kopian senare. För att skapa en ny anteckningsbok måste du välja en tom mapp.
Naturligtvis kan du också välja en befintlig zim anteckningsboksmapp.
 Innehåll I_dag I dag Växla kryssruta '>' Växla kryssruta 'V' Växla kryssruta 'X' Toppanel Ikon för aktivitetsfältet Omvandla sidnamn till taggar för uppgifter Filtyp Avmarkera kryssruta Ta bort indrag med <Backsteg>
(om du avaktiverar kan <Skift><Tabb> fortfarande användas) Okänd Okänd bildtyp Okänt objekt Ospecificerat Ej taggade Uppdatera %i sida som länkar till den här sidan Uppdatera %i sidor som länkar till den här sidan Uppdatera rubriken på den här sidan Uppdaterar länkar Uppdaterar biblioteksstruktur Använd %s för att växla till sidopanelen Använd anpassat typsnitt Använd en sida för varje Använd datum från journalsidor Använd <Retur> för att följa länkar
(om avstängd kan du fortfarande använda <Alt><Retur>) Versionshantering Versionskontroll är inte aktivierat för den här anteckningsboken
Vill du aktivera nu? Versioner Visa _kommentarer Visa _logg Webbserver Vecka Var vänlig och inkludera informationen från textrutan nedan när du rapporterar den här buggen Sök endast _hela ord Bredd Wikisida: %s Räkna ord Räkna _ord... Ord År I går Du redigerar en fil i ett externt program. Du kan stänga den här dialogrutan när du är klar. Du kan konfigurera egna verktyg som kommer att synas
i verktygsmenyn eller i snabbmenyer. Zim Desktop Wiki Zooma _ut _Om _Lägg till _Alla paneler _Aritmetik _Bakåt _Bläddra Kända _fel _Avbryt _Kryssruta Underordnad _Rensa formatering St_äng _Dölj alla _Innehåll _Kopiera _Kopiera hit _Ta bort _Ta bort sida _Förkasta ändringar _Duplicera rad _Redigera _Redigera länk _Redigera länk eller objekt... _Redigera egenskaper _Redigera... _Kursiv _Frågor och svar _Arkiv _Hitta _Sök... _Framåt _Helskärm _Gå _Hjälp _Markera _Historik _Hem _Bild... _Importera sida _Infoga _Hoppa Ho_ppa till _Snabbkommandon _Länk _Länka till datum _Länk... _Markera _Mer _Flytta _Flytta hit _Flytta raden ner _Flytta raden upp Ny sida här... _Ny sida _Nästa _Ingen _Normal storlek _Numrerad lista _OK _Öppna _Öppna anteckningsbok... _Annan... _Sida Sid_hierarki Överordnad K_listra in _Förhandsgranska _Föregående _Skriv ut _Skriv till webläsare _Snabb anteckning... _Avsluta Senaste sidor _Gör om _Reguljärt uttryck _Läs om _Ta bort _Ta bort rad _Ta bort länk _Ersätt _Ersätt... _Återställ storlek _Återställ version _Spara _Spara _Skärmbild... _Sök _Sök... _Skicka till... _Sidopaneler _Sida vid sida _Sortera rader _Genomstrykning _Fetstil _Nedsänkt _Upphöjd _Mallar Ver_ktyg _Ångra _Spärrad _Versioner... _Visa Zooma _in som förfallodatum för uppgifter som startdatum för uppgifter calendar:week_start:1 använd inte horisontella rader inga rutnät Ej skrivbar sekunder Launchpad Contributions:
  Jaap Karssenberg https://launchpad.net/~jaap.karssenberg
  Jesper J https://launchpad.net/~jesperj
  Jonatan Nyberg https://launchpad.net/~jony0008
  Kess Vargavind https://launchpad.net/~kess
  Leopold Augustsson https://launchpad.net/~leopold-augustsson
  Mikael Mildén https://launchpad.net/~mikael-milden
  Patrik Nilsson https://launchpad.net/~nipatriknilsson
  Rustan Håkansson https://launchpad.net/~rustanhakansson
  rylleman https://launchpad.net/~rylleman vertikala rader med rader {count} av {total} 