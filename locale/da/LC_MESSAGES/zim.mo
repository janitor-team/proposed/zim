��    z     �              �'  ,   �'  .   �'  ?   (     Y(     e(     �(     �(  0   �(     �(     )     #)  	   ))     3)  i   B)  *   �)     �)     �)     �)     �)     *  
   #*  -   .*     \*  V   d*     �*  	   �*  	   �*     �*  3   �*  Y   +     w+     �+  
   �+     �+     �+     �+     �+     �+     �+     ,  	   ,     ,  $   $,  ?   I,  /   �,  (   �,     �,  %   �,  ,   %-     R-  	   h-     r-     �-  
   �-     �-  	   �-     �-     �-     �-     �-     �-  
   �-     �-     .     	.     .     %.     4.  
   <.     G.     c.     v.     �.     �.     �.  <   �.     �.  	   �.  
   /     /     /     $/     A/     I/     _/     u/     �/     �/  +   �/  3   �/      0     10     60     I0     W0  
   c0     n0     }0     �0     �0  3   �0     	1     '1     :1     U1     h1     �1     �1     �1     �1     �1     �1     �1     �1     �1     �1  0   �1     &2     72     =2     I2  
   [2     f2     m2     z2     �2     �2     �2     �2  $   �2  ~   �2     a3     o3  
   s3     ~3     �3  
   �3  	   �3  
   �3     �3     �3     �3     �3     �3     4     4  5   4     J4     Y4     e4  !   l4     �4  #   �4     �4     �4     �4     �4     5     5     -5     F5     R5     k5     �5     �5     �5  
   �5     �5     �5  	   �5  6   �5     6  v   6     �6  *   �6  c   �6     '7     /7     67  
   =7     H7     `7     z7     ~7  
   �7  
   �7  
   �7  
   �7  
   �7  
   �7     �7     �7     �7     
8  	   !8     +8     <8     A8     H8     T8     e8  
   k8     v8     �8     �8     �8     �8     �8     �8     �8     �8     	9     9     "9     49     L9     Z9     g9  	   }9     �9     �9     �9     �9     �9     �9     �9     �9     �9     	:     :     :     ):     /:  2   8:     k:     s:     |:     �:     �:     �:     �:     �:     ;     ;     +;     >;  	   F;     P;     Y;     _;     u;     �;     �;     �;     �;      �;  *   �;     <     <     $<     3<     D<     T<     Y<     o<     �<  *   �<  2   �<     �<     =     =     6=     M=  	   V=     `=     c=     z=     �=     �=     �=     �=     �=  
   �=     �=  	   >     >     #>     7>     E>     Y>     h>     q>     �>     �>  9   �>     �>  I   �>  !   .?  '   P?  	   x?     �?     �?  0   �?  
   �?  	   �?     �?     �?     �?     @  	   @     (@     1@     7@  '   @@  V   h@  3   �@  0   �@  (   $A     MA     gA  .   nA     �A     �A     �A     �A     �A     �A     �A     �A     �A  
   B  &   B  
   6B     AB     OB     ^B     pB     �B     �B  
   �B     �B  
   �B     �B     �B  ?   �B     3C     @C     MC     dC     �C     �C     �C     �C     �C     �C     �C     �C  	   �C     �C     �C     
D     !D     'D     :D     AD     VD     jD     rD     �D     �D     �D     �D  �   �D     RE      kE     �E     �E  	   �E     �E     �E     �E     �E     F     F     0F     DF     VF  2   nF     �F  &   �F     �F     �F     �F     G     &G     8G  5   RG  &   �G     �G     �G     �G  &   �G     �G     H     H     *H     8H     >H  
   PH     [H     bH  	   qH     {H     �H     �H     �H     �H     �H  	   �H     �H     �H  	   �H     �H  
   �H     �H     I     $I  ?   :I  A   zI  �  �I  S   �K  =   �K  K   L  @   ^L  6   �L  -   �L  I   M  x   NM  �   �M  �   nN  �   7O  �   �O  �   JP  }   �P  L   IQ  �   �Q  9    R  �   ZR  �   S  �   �S  }   T  C   �T  h   �T  k   IU  �   �U  R   �V  ;   �V  =   W  v   VW    �W  j   �X  n   LY  ]   �Y     Z  �   �Z  7   .[     f[  �   l[  |   \     �\     �\     �\     �\     �\     �\     �\  	   �\  '   �\     ]     ]  D   $]     i]     q]     �]     �]     �]  H   �]     �]     ^     ^  !   -^     O^     a^     u^  P   �^     �^     �^  U   �^     Q_     Z_  	   j_  
   t_     _  N   �_     �_     �_     �_  �   �_  
   �`     �`     �`     �`  	   �`  ^   �`  f   ;a     �a  	   �a     �a     �a  
   �a     �a     �a     �a     �a     �a     b  	   b     b     b     .b     5b  	   Cb     Mb  
   Sb     ^b     fb     sb     �b     �b  
   �b     �b     �b     �b  	   �b     �b     �b     �b     �b     �b     c     c     c  
   c     %c     .c  	   4c     >c     Nc     Vc     \c     hc     uc     {c     �c     �c     �c     �c  
   �c     �c     �c     �c     �c     �c     �c     �c     d     d     d     d  	   8d     Bd     Hd     Xd     `d     gd  	   pd     zd     �d     �d     �d     �d     �d     �d     �d     �d     �d     �d     �d     e     e     e     ,e  
   2e     =e     Le  
   Te     _e     ke     we     �e     �e     �e  
   �e     �e  
   �e     �e     �e  	   �e     �e     �e     �e     �e     f     %f  
   ;f     Ff     Wf     ef     nf     vf     �f  
   �f     �f  �  �f  0   �h  =   �h  =   i     Di  !   Qi  -   si     �i  3   �i  '   �i     j     6j     <j     Ej  p   Uj  %   �j     �j     �j     k     k  #   (k     Lk  3   \k     �k  W   �k     �k  
   �k     	l     l  7   .l  X   fl     �l     �l  	   �l     �l     m     m     )m     Gm     Ym  	   lm     vm     �m     �m  >   �m  2   �m  '   %n  '   Mn  -   un  1   �n  $   �n  	   �n     o     o  
    o     +o  
   2o     =o  
   Lo     Wo     fo     oo     {o     �o  	   �o     �o  	   �o     �o  
   �o     �o     �o     	p     'p     Ep     cp     sp  ;   �p     �p     �p     �p  	   �p     �p     �p  	   q     !q     7q     Mq     ^q     pq  )   �q  2   �q  *   �q     
r     r     (r     9r     Ir     Wr  %   jr     �r      �r  3   �r      s     %s     9s     Ws     ns     �s     �s     �s     �s     �s     �s  	   �s     �s     �s     �s  7   �s     7t     Ht  	   Tt     ^t     ot     {t     �t     �t     �t     �t  
   �t     �t  %   �t  �   �t     �u     �u  
   �u  
   �u     �u     �u     �u     �u     �u  
   v     v     +v     Ev  	   Uv     _v  5   gv     �v     �v     �v  &   �v     �v  #   �v     "w     7w     >w     Sw     rw     �w     �w     �w     �w     �w     �w     x     x     x     x     1x  
   @x  A   Kx     �x  �   �x     y  *   .y  l   Yy     �y     �y     �y  
   �y     �y     z     z     "z     *z     Gz     Uz     cz     qz     z     �z  #   �z  $   �z     �z     �z     �z     
{     {     {     &{     :{  
   A{     L{     a{     r{     �{     �{     �{     �{     �{     �{     �{     �{     	|     |     6|     E|     S|     i|     v|     �|  
   �|     �|     �|     �|     �|     �|     �|     }     }     -}     <}  	   C}  0   M}     ~}  	   �}     �}     �}  '   �}     �}     �}     ~     %~     A~     [~  	   o~  	   y~     �~     �~     �~     �~     �~     �~     �~     �~  +   �~  1   '     Y     `     h     u     �     �     �  %   �     �  4   �  G   -�     u�     ��  (   ��     ̀     �     �     ��     �  !   �  
   :�     E�     S�     _�     t�  
   ��     ��     ��     ��     ʁ     ܁     �     ��     	�     �     0�  	   9�  A   C�     ��  S   ��  -   �  $   �  	   =�  	   G�     Q�  0   V�  
   ��     ��     ��     ��     ǃ  
   �     �     �     ��     �  &   �  N   7�  !   ��  /   ��  '   ؄      �     �  *   �     D�     L�     Q�     e�     t�     ��     ��     ��     ��  
   ��  &   ��     �     �     ��     �     $�  +   ;�     g�  
   m�     x�     ��     ��     ��  P   ��     �  
   �     �     0�     N�     R�     Y�     j�  
   ��  	   ��     ��     ��     ��     ��     ˇ     ڇ     �     ��     	�     �     ,�     @�     G�     a�  	   |�     ��     ��  �   ��     9�     R�  !   o�     ��  	   ��     ��     ŉ     ى     �     ��     �     �     .�     =�  I   X�     ��  $   ��     ׊     ��     �     �     7�     H�  4   d�  1   ��     ˋ  
   ؋  
   �  (   �     �     )�     ?�     L�     Y�     _�  
   p�     {�     ��  
   ��     ��     ��     ��     Ì  +   Ȍ     �     ��     �     �  
   �     #�  
   )�     4�     F�     _�  4   x�  @   ��  �  �  ?   ��  <   �  H   '�  @   p�  8   ��  /   �  A   �  �   \�  �   �  �   ��  �   }�  �    �  �   Ȕ  �   Y�  h   �  �   S�  6   �  �   (�  �   ԗ  �   b�  �   ��  ?   }�  q   ��  u   /�  �   ��  T   |�  ?   ћ  ?   �  �   Q�  +  ڜ  o   �  r   v�  Z   �  �   D�  �   �  >   ��     ͠  �   Ӡ  �   ��     �     �     �     %�     @�     [�     v�  	   ��  (   ��     ��     ��  T   ע     ,�     3�     E�     S�     `�  Z   m�     ȣ     �     �  #   �     '�     8�     N�  U   i�  
   ��     ʤ  P   ڤ  	   +�     5�     D�  	   M�     W�  ?   [�  	   ��     ��     ��  �   ��     �     ��     ��     ��     ��  h   ��  n   �     ��     ��     ��     ��     ��  
   ��     ��     ק  	   �     �  	   �     ��     �     �     +�     0�     A�     J�     S�     c�  
   i�     t�     ��     ��     ��     ��     ͨ     �     �     ��     ��     �     �     �     �     $�     )�  	   1�  	   ;�     E�     N�     Z�     l�     u�     z�     ��     ��     ��     ��     ��     ©     ȩ     Ω     ۩     �     ��     �     �     �     &�     9�     K�     O�     U�  	   m�     w�     }�     ��     ��     ��     ��     ��     Ȫ     ݪ     �     �     �     	�     �     '�     .�  
   ;�     F�  
   N�     Y�     m�     }�  	   ��     ��     ��     ��     ��     ��     ǫ     ޫ  
   ��     ��     ��     	�     �     "�     /�     8�     F�     T�  	   Y�     c�     ��     ��  	   ��     ��     ˬ     ج     �  V  �     H�  
   X�     c�   		This plugin provides bar for bookmarks.
		 %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i Attachment %i Attachments %i _Backlink %i _Backlinks %i errors occurred, see log %i file will be deleted %i files will be deleted %i open item %i open items %i warnings occurred, see log <Top> <Unknown> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Action Add Application Add Bookmark Add Notebook Add bookmark/Show settings Add column Add new bookmarks to the beginning of the bar Add row Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files All Tasks Allow public access Always use last cursor position when opening a page An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Arithmetic Ascii graph (Ditaa) Attach File Attach image first Attachment Browser Attachments Attachments: Author Auto
Wrap Auto indenting Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave interval in minutes Autosave version on regular intervals Autosave version when the notebook is closed Back to Original Name BackLinks BackLinks Pane Backend Backlinks: Bazaar Bookmarks BookmarksBar Border width Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Capture whole screen Center Change columns Changes Characters Characters excluding spaces Check Checkbox '>' Check Checkbox 'V' Check Checkbox 'X' Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Clone row Code Block Column 1 Command Command does not modify data Comment Common include footer Common include header Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Created Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Demote Dependencies Description Details Diagram Discard note? Distraction Free Editing Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root Due E_xport... Edit %s Edit Custom Tool Edit Image Edit Link Edit Table Edit _Source Editing Editing file: %s Enable Version Control? Enable plugin Enabled Equation Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Fossil GNU R Plot Get more plugins online Get more templates online Git Gnuplot Grid lines Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide Journal pane if empty Hide menubar in fullscreen mode Highlight current line Home Page Horizontal _Line Icon Images Import Page Include subpages Index Index page Inline Calculator Insert Code Block Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Sequence Diagram Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Key Binding Key bindings Labels marking tasks Last Modified Leave link to new page Left Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Managing table columns Map document root to URL Match _case Maximum number of bookmarks Maximum page width Menubar Mercurial Modified Month Move Selected Text... Move Text to Other Page Move column ahead Move column backward Move text to Name Need output file to export MHTML Need output folder to export full notebook New File New Page New Page in %s New S_ub Page... New _Attachment Next No Applications Found No changes since last version No dependencies No document root defined for this notebook No plugin available to display objects of type: %s No such file: %s No such page: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Only Show Active Tasks Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open cell content link Open help Open in New Window Open in New _Window Open new page Open plugins folder Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Index Page Name Page Template Page already exists: %s Page not allowed: %s Page section Paragraph Password Paste Path Bar Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please select a row, before you push the button. Please select more than one line of text Please specify a notebook Plugin Plugin "%s" is required to display this object Plugins Port Position in the window Pr_eferences Preferences Prev Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Remove column Remove row Removing Links Rename page "%s" Repeated clicking a checkbox cycles through the checkbox states Replace _All Replace with Require authentication Restore page to saved version? Rev Right Right Side Pane Right margin position Row down Row up S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Screenshot Command Search Search _Backlinks... Search this section Section Section(s) to ignore Section(s) to index Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Sequence Diagram Server not started Server started Server stopped Set New Name Set default text editor Set to Current Page Show Line Numbers Show Tasks as Flat List Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show full Page Name Show full page name Show helper toolbar Show in the toolbar Show right margin Show tasklist in sidepane Show the cursor also for pages that can not be edited Show the page title heading in the ToC Single _page Size Smart Home key Some error occurred while running "%s" Sort alphabetically Sort pages by tags Source View Spell Checker Start Start _Web Server Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Tags for non-actionable tasks Task Task List Tasks Template Templates Text Text Files Text From _File... Text background color Text foreground color The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The following parameters will be substituted
in the command when it is executed:
<tt>
<b>%f</b> the page source as a temporary file
<b>%d</b> the attachment directory of the current page
<b>%s</b> the real page source file (if any)
<b>%p</b> the page name
<b>%n</b> the notebook location (file or folder)
<b>%D</b> the document root (if any)
<b>%t</b> the selected text or word under cursor
<b>%T</b> the selected text including wiki formatting
</tt>
 The inline calculator plugin was not able
to evaluate the expression at the cursor. The table must consist of at least on row!
 No deletion done. There are no changes in this notebook since the last version that was saved This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This page name cannot be used due to technical limitations of the storage This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a "path bar" to the top of the window.
This "path bar" can show the notebook path for the current page,
recent visited pages or recent edited pages.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds the page index pane to the main window.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows inserting 'Code Blocks' in the page. These will be
shown as embedded widgets with syntax highlighting, line numbers etc.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin also has properties,
see the notebook properties dialog This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a sequence diagram editor for zim based on seqdiag.
It allows easy editing of sequence diagrams.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This plugin turns one section of the notebook into a journal
with a page per day, week or month.
Also adds a calendar widget to access these pages.
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox '>' Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Tray Icon Turn page name into tags for task items Type Un-check Checkbox Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unkown Image type Unkown Object Unspecified Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use a page for each Use date from journal pages Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Username Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s With this plugin you can embed a 'Table' into the wiki page. Tables will be shown as GTK TreeView widgets.
Exporting them to various formats (i.e. HTML/LaTeX) completes the feature set.
 Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Zim Desktop Wiki Zoom _Out _About _Add _All Panes _Arithmetic _Attachment... _Back _Browse _Bugs _Cancel _Checkbox _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Duplicate Line _Edit _Edit Link _Edit Link or Object... _Edit Properties _Edit... _Emphasis _FAQ _File _Find _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _Move Here _Move Line Down _Move Line Up _New Page Here... _New Page... _Next _None _Normal Size _Numbered List _OK _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Print _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove _Remove Line _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In as due date for tasks as start date for tasks calendar:week_start:0 do not use horizontal lines no grid lines readonly seconds translator-credits vertical lines with lines {count} of {total} Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2020-12-21 13:29+0000
Last-Translator: scootergrisen <scootergrisen@gmail.com>
Language-Team: Danish <https://hosted.weblate.org/projects/zim/master/da/>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.4-dev
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
 		Dette plugin giver et panel med bogmærker.
		 %(cmd)s
returnerede en exit-status, der ikke var nul %(code)i %(n_error)i fejl og %(n_warning)i advarsler opstod, check log %A %d. %B %Y %i vedhæftning %i vedhæftninger %i _henvisning hertil %i _henvisninger hertil %i fejl opstod, check log %i fil vil blive slettet %i filer vil blive slettet %i åben komponent %i åbne komponenter %i advarsler opstod, check log <Top> <Ukendt> En desktop-wiki En fil med navnet <b>"%s"</b> findes allerede.
Du kan bruge et andet navn eller overskrive den eksisterende fil. En tabel skal have mindst en kolonne. Handling Tilføj program Tilføj bogmærke Tilføj notesbog Tilføj bogmærke/vis indstillinger Tilføj kolonne Tilføj nye bogmærker i starten af bogmærkelinjen Tilføj række Tilføjer stavekontrol med gtkspell.

Dette er et standardplugin, der leveres med zim.
 Juster Alle filer Alle opgaver Tillad offentlig adgang Brug altid seneste markørplacering ved åbning af side Der opstod en fejl under billedgenereringen.
Ønsker du at gemme kildeteksten alligevel? Annoteret kilde for siden Applikationer Aritmetik Ascii-graf (Ditaa) Vedhæft fil Vedhæft billede først Browser til vedhæftede filer Vedhæftede filer Vedhæftede filer: Forfatter Auto
Foldning Autoindrykning Automatisk gemt version fra zim Vælg automatisk det aktuelle ord, når formattering påføres Konverter automatisk ord med "CamelCase" til links Konverter automatisk filstier til links Interval for automatisk gem, i minutter Gem en version automatisk med faste mellemrum Version for automatisk gem, når bærbaren lukkes Skift tilbage til det originale navn BackLinks Panel med backlinks Backend Backlinks: Bazaar Bogmærker Bogmærkelinje Kantbredde Panel i bunden Gennemse _Punktliste Konfiguration Kan ikke ændre i siden %s Annullér Tag billede af fuld skærm Centreret Byt kolonner Ændringer Tegn Tegn inklusive mellemrum Tilvælg afkrydsningsboks '>' Tilvælg afkrydsningsboks 'V' Tilvælg afkrydsningsboks 'X' Check _stavning Liste med _afkrydsningsbokse Klassisk bakkeikon,
bruger ikke det nye statusikon i Ubuntu Ryd Klon række Kodeblok Kolonne 1 Kommando Kommando modificerer ikke data Kommentar Common include footer Common include header Hele _notesbogen Opsæt programmer Konfigurer plugin Opsæt et program til at åbne "%s"-links Opsæt et program til at åbne filer
af typen "%s" Betragt alle afkrydsningsbokse som opgaver Kopiér Kopiér e-mailadresse Kopiér skabelon Kopiér _som... Kopiér _link Kopiér _placering Kunne ikke finde eksekverbar fil "%s" Kunne ikke finde notesbogen %s Kunne ikke finde skabelonen "%s" Kunne ikke finde fil eller mappe for denne notesbog Kunne ikke indlæse stavekontrol Kunne ikke åbne %s Kunne ikke fortolke udtrykket Kunne ikke indlæse %s Kunne ikke gemme siden %s Opret ny side for hver note Opret mappe? Oprettet Kl_ip Egne værktøjer Egne _værktøjer Tilpas... Dato Dag Standard Standardformat for kopiering af tekst til udklipsholder Standardnotesbog Forsinkelse Slet side Slet siden "%s"? Slet række Nedryk Afhængigheder Beskrivelse Detaljer Diagram Slet note? Uforstyrret redigering Ønsker du at slette alle bogmærker? Ønsker du at etablere siden: %(page)s
til den gemte version version: %(version)s ?

Alle ændringer siden sidst gemte version vil gå tabt! Dokumentets rod Forfaldende Eksport... Rediger %s Rediger egne værktøjer Rediger billede Rediger link Rediger tabel Rediger _kildetekst Redigering Redigerer filen %s Aktivér versionsstyring? Aktivér plugin Aktiveret Ligning Fejl i %(file)s ved linje %(line)i nær "%(snippet)s" Evaluer _matematik Udfold _alle Eksport Eksporter alle sider til en enkelt fil Eksport fuldført Eksporter hver side til separat fil Eksporterer notesbog Fejlet Kunne ikke køre: %s Kunne ikke køre programmet %s Filen eksisterer _Skabeloner... Fil ændret på disk: %s Filen findes allerede Kunne ikke skrive til fil: %s Filtype ikke understøttet: %s Filnavn Filter Find Find _næste Find _foregående Find og erstat Søg efter Marker opgaver til afslutning mandag eller tirsdag før weekenden Mappe Mappen findes allerede og er ikke tom; eksporteres der til denne mappe, kan det overskrive eksisterende filer. Ønsker du at fortsætte? Mappen findes: %s Mappe med skabeloner til vedhæftede filer For avanceret søgning kan man bruge operatorer som
AND, OR og NOT. Se hjælpesiden for yderligere detaljer. For_mat Format Fossil GNU R-plot Hent flere plugins online Hent flere skabeloner online Git Gnuplot Vandrette og lodrette linjer Overskrift _1 Overskrift _2 Overskrift _3 Overskrift _4 Overskrift _5 Højde Skjul journal-rude, hvis den er tom Skjul menulinje i fuldskærmsvisning Fremhæv aktuel linje Forside Vandret _linje Ikon Billeder Importer side Inkluder undersider Indeks Indeksside In-line regnemaskine Indsæt kodeblok Indsæt dato og tidspunkt Indsæt diagram Indsæt Ditaa Indsæt formel Indsæt GNU R-plot Indsæt Gnuplot Indsæt billede Indsæt link Indsæt nodeark Indsæt skærmbillede Indsæt sekvensdiagram Indsæt symbol Indsæt tabel Indsæt tekst fra fil Grænseflade Adgangskode til Interwiki Journal Spring til Spring til side Tastebinding Tastebindinger Labels, der angiver opgaver Sidst ændret Efterlad link til ny side Venstre Venstre sidepanel Linjesortering Linjer Link-kort Link filer under dokumentets rod med fuld filsti Link til Placering Log events med Zeitgeist Logfil Det ser ud til at du har fundet en fejl Sæt som standardprogram Administrer tabelkolonner Sæt dokumentrod til URL Samme _store/små bogstaver Maksimum antal bogmærker Maksimal sidebredde Menulinje Mercurial Ændret Måned Flyt valgt tekst... Flyt tekst til anden side Flyt kolonne frem Flyt kolonne tilbage Flyt tekst til Navn Angiv outputfil for at eksportere til MHTML Angiv outputmappe for at eksportere fuld notesbog Ny fil Ny side Ny side i %s Ny _underside... Ny _vedhæftning Næste Ingen programmer til rådighed Ingen ændringer siden sidste version Ingen afhængigheder Der er ikke angivet nogen dokumentrod til notesbogen Der er ikke noget tilgængeligt plugin til at vise objekter af type: %s Ingen fil med navnet %s Ingen sådan side: %s Der er ikke defineret en sådan wiki: %s Ingen skabeloner installeret Notesbog Notesbøger OK Vis kun aktive opgaver Åbn mappe med vedhæftede _filer Åbn mappe Åbn notesbog Åbn med... Åbn dokumentets rod Åbn mappe med notesbøger Åbn _side Åbn link i celle Åbn hjælp Åbn i nyt vindue Åbn i nyt vindue Åbn ny side Åbn plugin-mappe Åbn med "%s" Valgfri Valgmuligheder for pluginet %s Andet... Outputfil Outputfil findes, angiv "--overwrite" for at gennemtvinge eksport Mappe til output Outputmappen findes og er ikke tom, angiv "--overwrite" for at gennemtvinge eksport Angiv en placering, inden der kan eksporteres Output skal erstatte nuværende valg Overskriv S_tilinje Side Siden "%s" har ingen mappe til vedhæftede filer Sideindeks Navn på side Sideskabelon Siden findes allerede: %s Siden er ikke tilladt: %s Sideafsnit Afsnit Adgangskode Indsæt Stilinje Indtast en kommentar til denne version Bemærk, at link til en ikke-eksisterende side
opretter en ny side automatisk. Vælg navn og mappe til notesbog. Vælg en række, inden der trykkes på knappen. Vælg venligst mere end én linje tekst Vælg en notesbog Plugin Pluginet "%s" kræves for at vise objektet Plugins Port Placering i vinduet _Indstillinger Indstillinger Foregående Udskriv til browser Fremryk _Egenskaber Egenskaber Sender events til Zeitgeist-tjenesten. Kviknote Kviknote... Nylige ændringer Nylige ændringer... Nyligt _ændrede sider Konverter wiki-opmærkning mens der skrives Fjern Fjern alle Fjern kolonne Fjern række Fjerner links Omdøb siden "%s" Gentagne klik på en afkrydsningsboks gennemløber afkrydsningsboksens tilstande Erstat _alle Erstat med Kræver godkendelse Gendan side til gemt version? Rev Højre Højre sidepanel Placering af højremargen Række ned Række op _Gem version... Gem _kopi... Gem kopi Gem version Gem bogmærker Gemt version fra zim Score Screenshot-kommando Søg Søg til_bagevisende links... Søg i dette afsnit Afsnit Afsnit som skal ignoreres Afsnit som skal indekseres Vælg fil Vælg mappe Vælg billede Vælg en version for at se ændringerne imellem den version og den nuværende,
eller vælg flere versioner for at se ændringerne imellem de versioner.
 Vælg format til eksport Vælg outputfil eller -mappe Vælg sider, der skal eksporteres Vælg vindue eller område Markering Sekvensdiagram Server ikke startet Server startet Server stoppet Sæt nyt navn Vælg standardeditor Sæt til denne side Vis linjenumre Vis opgaver som flad liste Vis indholdsfortegnelse som en flydende widget i stedet for i sidepanelet _Vis ændringer Vis særskilt ikon for hver notesbog Vis fuldt navn for siden Vis fuldt sidenavn Vis hjælpe-værktøjslinje Vis på værktøjslinje Vis højremargen Vis opgaveliste i sidepanel Vis cursoren - også i sider, der ikke kan redigeres Vis sidens titeloverskrift i indholdsfortegnelsen Enkelt _side Størrelse Smart Home Der opstod en fejl under kørsel af "%s" Sorter alfabetisk Sort sider efter tags Kildevisning Stavekontrol Start Start _webserver Sy_mbol... Syntaks Systemstandard Tab-bredde Tabel Tabeleditor Indholdsfortegnelse Tags Tags for opgaver, der ikke skal handles på Opgave Opgaveliste Opgaver Skabelon Skabeloner Tekst Tekstfiler Tekst fra _fil... Baggrundsfarve for tekst Forgrundsfarve for tekst Mappen
%s
findes ikke.
Ønsker du at oprette den nu? Mappen "%s" eksisterer ikke endnu.
Ønsker du at oprette den nu? Følgende parametre erstattes i
kommandoen når den eksekveres:
<tt>
<b>%f</b> sidekilden som en midlertidig fil
<b>%d</b> den vedhæftede mappe af den nuværende side
<b>%s</b> den rigtige side sidekildefil (om nogen)
<b>%p</b> sidenavnet
<b>%n</b> notesbogens placering (fil eller mappe)
<b>%D</b> dokumentets rod (om nogen)
<b>%t</b> den markerede tekst eller ord under markør
<b>%T</b> den markerede tekst inklusiv wiki-formatering
</tt>
 Matematik-pluginet kunne ikke evaluere
udtrykket ved markøren. Tabellen skal indeholde mindst en række!
 Der slettes ikke. Der er ingen ændringer i denne notesbog, siden sidste version blev gemt Dette kan betyder, at du ikke har de rette
ordbøger installeret Denne fil findes allerede.
Ønsker du at overskrive den? Denne side har ikke en mappe med vedhæftninger Sidenavnet kan ikke bruges pga. tekniske begrænsninger i lageret Pluginet lader brugeren tage et skærmbillede og indsætte det direkte
i en zim-side.

Dette er et standardplugin, der leveres med zim.
 Pluginet tilføjer en "stilinje" øverst i vinduet.
"Stilinjen" kan vise stien til notesbogen for den nuværende side,
seneste besøgte sider og seneste redigerede sider.
 Pluginet tilføjer en dialog, der viser alle igangværende opgaver fra
denne notesbog. Aktive opgaver kan være åbne afkrydsningsbokse
eller punkter markeret med "TODO" eller "FIXME".

Dette er et standardplugin, der leveres med zim.
 Pluginet tilføjer en dialog til hurtigt at indsætte noget tekst eller udklipsholderens
Indhold i en zim-side.

Dette er et standardplugin, der leveres med zim.
 Pluginet tilføjer et bakkeikon, så man hurtigt kan komme til zim.

Pluginet kræver Gtk+ version 2.10 eller nyere,

Dette er et standardplugin, der leveres med zim.
 Pluginet tilføjer en ekstra widget med en liste over sider
som linker til den aktuelle side.

Dette er et standardplugin, der leveres med zim.
 Pluginet indsætter en ekstra widget, som viser
en indholdsfortegnelse for den aktuelle side.

Dette er et standardplugin, der leveres med zim.
 Dette plugin giver indstillinger, der hjælper med at bruge zim
som editor uden forstyrrende elementer.
 Pluginet tilføjer "Indsæt symbol"-dialogen, som 
tilføjer automatisk formattering af typografiske tegn.

Dette er et standardplugin, der leveres med zim.
 Pluginet tilføjer sideindeks-ruden til hovedvinduet.
 Pluginet tilføjer versionsstyring for notesbøger.

Pluginet understøtter versionssystemerne Bazaar, Git og Mercurial.

Dette er et standardplugin, der leveres med zim.
 Pluginet giver mulighed for at indsætte 'kodeblokke' på siden. De vises
som en indlejrede widgets med syntaksfremhævning, linjenumre osv.
 Denne plugin tillader dig at inkludere aritmetiske beregninger i zim.
Den er baseret på det aritmetiske modul fra
http://pp.com.mx/python/arithmetic.
 Pluginet giver mulighed for hurtigt at vurdere simple
matematiske udtryk i zim.

Dette er et standardplugin, der leveres med zim.
 Pluginet har også egenskaber,
se notesbogens egenskaber-dialog Dette plugin giver en diagrameditor for zim baseret på Ditaa.

Dette er et standardplugin, der leveres med zim.
 Dette plugin tilføjer en GeaphViz-baseret diagram-editor til zim.

Dette er et standardplugin, der leveres med zim.
 Pluginet giver en dialog med en grafisk visning
af notesbogens struktur. Den kan bruges som en slags
"mind map", der viser, hvordan siderne relaterer til hinanden.

Dette er et standardplugin, der leveres med zim.
 Dette plugin giver et sideindeks filtreret efter tags, der er valgt fra en tag-sky.
 Dette plugin tilføjer en plot-editor baseret på GNU R i zim.
 Dette plugin giver adgang til redigering af plots via Gnuplot.
 Dette plugin giver en editor til sekvensdiagrammer for zim baseret på seqdiag.
Det introducerer enkel redigering af sekvensdiagrammer.
 Pluginet fungerer som en måde at komme udenom
manglende printerunderstøttelse i zim. Det eksporterer den
nuværende side til HTML og åbner den i en browser. Såfremt
browseren er sat op til udskrift, får man sine data til printeren i to skridt.

Dette er et standardplugin, der leveres med zim.
 Dette plugin indsætter en LaTeX-baseret formeleditor i zim.

Dette er et standardplugin, der leveres med zim.
 Pluginet giver en node-editor for zim baseret på GNU Lilypond.

Dette er et standardplugin, der leveres med zim.
 Dette plugin viser den aktuelle sides mappe med vedhæftninger som
ikon i nederste panel.
 Dette plugin sorterer markerede linjer i alfabetisk rækkefølge.
Såfremt listen allerede er sorteret sådan, vil rækkefølgen blive omvendt
(A-Å bliver til Å-A).
 Pluginet gør en sektion af notesbogen til en dagbog-journal
med en side pr. dag, uge eller måned.
Tilføjer også en kalenderwidget til at tilgå disse sider.
 Dette betyder almindeligvis, at filen indeholder ugyldige tegn Titel For at fortsætte kan du gemme en kopi af denne side eller forkaste
ændringer. Gemmer du en kopi vil ændringerne også bortfalde, men
kopien vil kunne genindlæses senere. For at oprette en ny notesbog skal der vælges en tom mappe.
Der kan naturligvis også vælges en eksisterende Zim-notesbogsmappe.
 Indholdsfortegnelse I_dag I dag Skift afkrydsningsboks '>' Skift afkrydsningsboks 'V' Skift afkrydsningsboks 'X' Panel i toppen Bakkeikon Gør sidens navn til tag for opgaveliste Type Fravælg afkrydsningsboks Slet indrykning med <BackSpace>
(Hvis deaktiveret kan <Shift><Tab> fortsat anvendes) Ukendt Ukendt billedtype Ukendt objekt Ikke angivet Uden mærker Opdater %i side, der henviser til denne side Opdater %i sider, der henviser til denne side Opdater sidens overskrift Opdaterer links Opdaterer indeks Brug %s til at skifte til sidepanel Anvend egen font Brug en side til hver Brug dato fra journalsider Brug <Enter> til at følge links
(Hvis deaktiveret kan man stadig bruge <Alt><Enter>) Brugernavn Versionsstyring Versionkontrol er ikke aktiveret for denne notesbog.
Ønsker du at slå det til? Versioner Vis _annoteret Vis _log Webserver Uge Ved fejlrapportering bedes
nedenstående oplysninger inkluderet Helt _ord Bredde Wiki-side: %s Med dette plugin kan du indlejre en tabel i wiki-siden. Tabeller vil blive vist som GTK TreeView-widgets.
Ved eksport til forskellige formater, f.eks. HTML/LaTeX, anvendes disses tabelfunktioner.
 Ordtælling Ordtælling... Ord År I går Du er i gang med at redigere en fil i et eksternt program. Du kan lukke denne dialog, når du er færdig Du kan definere egne værktøjer, som vil vises
i Værktøjer-menuen og værktøjslinjen eller kontekstmenuer. Zim Desktop Wiki Zoom _ud _Om _Tilføj _Alle paneler _Aritmetik _Vedhæftede filer... _Tilbage _Gennemse _Fejl _Annuller _Afkrydsningsboks _Underordnet _Ryd formattering _Luk _Sammenfold alle _Indhold K_opiér _Kopiér hertil _Slet _Slet side _Forkast ændringer _Dupliker linje _Rediger _Rediger link _Rediger link eller objekt... _Rediger indstillinger _Rediger... _Kursiv _FAQ _Fil _Find _Find... _Frem _Fuld skærm _Gå _Hjælp _Fremhæv _Historik _Forside _Billede... _Importer side... _Indsæt _Hop _Spring til... _Tastaturbindinger _Link _Link til dato _Link... _Marker _Mere _Flyt _Flyt hertil _Flyt linje ned _Flyt linje op _Ny side her ... _Ny side... _Næste _Ingen _Normal størrelse _Nummereret liste _OK _Åbn _Åbn anden notesbog... _Andet... _Side Side_hierarki _Overordnet _Indsæt _Forhåndsvisning _Foregående _Udskriv _Udskriv til browser _Kviknote... _Afslut _Seneste sider _Gendan _Regulære udtryk _Genindlæs _Fjern _Fjern linje _Slet link _Erstat _Erstat... _Nulstil størrelse _Gendan version _Gem _Gem kopi _Skærmbillede... _Søg _Søg... _Send til... _Sidepaneler _Ved siden af hinanden _Sorter linjer _Overstreg _Fed _Lav skrift _Høj skrift S_kabeloner _Værktøjer _Fortryd _Maskinskrift _Versioner... _Vis _Zoom ind som forfaldsdato til opgaver som startdato til opgaver calendar:week_start:1 brug ikke vandrette linjer ingen linjer skrivebeskyttet sekunder Launchpad Contributions:
  Frederik 'Freso' S. Olesen https://launchpad.net/~freso
  Jeppe Fihl-Pearson https://launchpad.net/~tenzer
  Jhertel https://launchpad.net/~jhertel
  fsando https://launchpad.net/~fsando
  mjjzf https://launchpad.net/~mjjzf
  nanker https://launchpad.net/~nanker
  scootergrisen https://launchpad.net/~scootergrisen lodrette linjer med linjer {count} af {total} 