��    4     �              \#  ,   ]#  .   �#  ?   �#     �#     $     "$  0   >$     o$     �$  	   �$     �$  i   �$  *   %     A%     Q%     ^%     k%  
   �%  -   �%     �%  V   �%     &  	   $&  	   .&     8&  3   L&  Y   �&     �&     �&  
   �&     '     '     ''     :'     F'     S'  	   Z'     d'  $   s'  ?   �'  /   �'  (   (  %   1(     W(  	   m(     w(     �(  
   �(     �(  	   �(     �(     �(     �(     �(     �(  
   �(     �(     )     )     #)     *)     9)  
   A)     L)     h)     {)     �)     �)     �)     �)  	   �)  
   �)     �)     �)      *     *     *     2*  +   C*  3   o*     �*     �*     �*     �*  
   �*     �*     �*     +     *+  3   G+     {+     �+     �+     �+     �+     �+     ,     !,     ),     .,     ;,     I,     V,     [,     _,  0   g,     �,     �,     �,     �,  
   �,     �,     �,     �,     �,      -     -  $   '-  ~   L-     �-     �-  
   �-     �-     �-  
   .  	   .  
   .     !.     ..     6.     G.     _.     m.     u.  5   ~.     �.     �.     �.  !   �.     �.  #   	/     -/     @/     G/     Z/     x/     �/     �/     �/     �/     �/     �/     �/     0  
   0     0      0  	   10  6   ;0     r0  v   y0     �0  *   1  c   -1     �1     �1     �1     �1     �1     �1     �1  
   �1  
   �1  
   �1  
   2  
   2     2     #2     C2  	   Z2     d2     u2     z2     �2     �2     �2  
   �2     �2     �2     �2     �2     �2     3     3     #3     03     <3     I3     [3     s3     �3     �3  	   �3     �3     �3     �3     �3     �3     �3     4     4     4     "4     (4  2   14     d4     l4     u4     �4     �4     �4     �4     �4     �4     5      5  	   (5     25     ;5     A5     W5     o5     |5      �5  *   �5     �5     �5     �5     �5     �5     6     6     *6     H6  *   X6     �6     �6     �6     �6     �6  	   �6     �6     �6     7     7     7     +7     ?7  
   U7  	   `7     j7     }7     �7     �7     �7     �7     �7     �7     �7  9   �7     08  I   >8  !   �8  '   �8  	   �8     �8     �8  0   �8  	   9     %9     39     K9  	   `9     j9  '   p9  V   �9  3   �9  0   #:     T:     n:     u:     }:     �:     �:     �:     �:     �:     �:     �:  
   �:  &   �:  
   ;     ;     ';     6;     H;     `;     �;  
   �;     �;  
   �;     �;     �;     �;     �;     �;     <     <     <     <     4<     =<     D<     U<  	   e<     o<     |<     �<     �<     �<     �<     �<     �<     �<     �<     =     =     (=     6=  �   C=     �=      �=     >     (>  	   @>     J>     [>     n>     }>     �>     �>     �>     �>  2   �>     
?  &   ?     ??     S?     g?     {?     �?  5   �?  &   �?     �?     @  &   @     7@     K@     ^@     j@     x@     ~@  
   �@     �@     �@  	   �@     �@     �@     �@     �@     �@  	   �@     �@  	   �@     A  
   A     A     *A     @A  ?   VA  A   �A  =   �A  K   B  @   bB  6   �B  -   �B  I   C  x   RC  �   �C  L   LD  �   �D  C   (E  k   lE  n   �E  ]   GF     �F  �   %G  7   �G     �G  �   �G  |   �H     I     I     I     "I     6I     JI     ^I  	   gI     qI     vI  D   �I     �I     �I     �I     �I     J  H   
J     SJ     sJ     �J  !   �J     �J     �J  P   �J     *K  U   :K     �K     �K  	   �K  
   �K     �K  N   �K     L     L     $L  
   2L     =L     KL     QL  	   VL  ^   `L  f   �L     &M  	   7M     AM     HM  
   MM     XM     dM     jM     rM     xM  	   �M     �M     �M     �M     �M  	   �M     �M  
   �M     �M     �M     �M     �M  
   �M     
N     "N     3N  	   <N     FN     KN     QN     WN     `N     iN     uN     yN  
   N     �N     �N  	   �N     �N     �N     �N     �N     �N     �N     �N     �N     �N     �N     O  
   	O     O     !O     'O     -O     :O     IO     MO     SO  	   mO     wO     }O     �O     �O     �O  	   �O     �O     �O     �O     �O     �O     �O     �O     P     P     P     $P     0P     <P     MP  
   SP     ^P     mP  
   uP     �P     �P     �P     �P     �P     �P  
   �P     �P  
   �P     �P     �P  	   �P     �P     	Q     Q     Q     .Q     FQ  
   \Q     gQ     xQ     �Q     �Q     �Q  
   �Q     �Q  �  �Q  5   �S  6   �S  <   %T     bT     nT     �T  '   �T     �T     �T     �T     �T  t   	U  (   ~U     �U     �U     �U  $   �U     V  0   V     IV  b   VV     �V  
   �V     �V     �V  9   �V  X   ,W     �W  
   �W  
   �W     �W     �W     �W     �W     �W  	   �W     X     X  !   $X  >   FX  .   �X  '   �X     �X  #   �X  
    Y     +Y     @Y     RY     aY  	   hY     rY  
   ~Y  
   �Y     �Y     �Y     �Y     �Y     �Y     �Y  	   �Y      Z  	   Z     Z     Z     /Z     MZ     kZ     �Z     �Z     �Z     �Z  	   �Z     �Z  $   �Z     �Z     �Z     [     ([  2   B[  6   u[     �[     �[  
   �[     �[     �[     �[  #   \     '\     @\  ;   Z\  "   �\     �\     �\     �\     �\     ]     6]     E]     K]     T]     j]  
   �]     �]     �]     �]  8   �]     �]     �]  
   �]     ^  	   ^      ^     %^     3^     ?^     H^     W^  $   s^  �   �^     _     *_     0_  
   >_     I_     d_     r_     �_     �_  
   �_     �_     �_     �_     �_     �_  5   �_     3`     B`  
   Q`  '   \`     �`  -   �`     �`     �`     �`     �`     a  
    a     +a     Ha     Ya     xa     �a     �a     �a     �a     �a     �a  
   �a  F   �a     &b  }   ,b     �b  "   �b  b   �b     Fc     Nc     Uc  "   \c     c     �c     �c     �c     �c     �c     �c     �c     d  "   d     =d  	   Od     Yd     id     nd     ud     �d     �d     �d     �d     �d     �d     �d     �d     e      e     1e     @e     Oe     `e     ue     �e     �e     �e     �e     �e     �e     �e     �e      f     f     'f     3f     Ff     Uf  
   \f  3   gf     �f     �f     �f     �f     �f     �f     g  $   0g     Ug     pg  	   �g  	   �g     �g     �g     �g     �g     �g     �g  )   �g  5   h     Qh     Xh     `h     mh     ~h     �h     �h  #   �h     �h  ,   �h     i     "i  "   8i     [i  
   ui     �i     �i     �i     �i     �i     �i     �i     �i     �i     	j     j     (j     <j     Jj     ^j  	   mj  "   wj     �j  
   �j  E   �j     �j  Z   k  $   `k  #   �k  
   �k  	   �k     �k  (   �k     �k     �k     l     l     4l     <l  -   Dl  K   rl  #   �l     �l     �l     m     #m     2m     7m     Jm     Ym     gm     om     �m     �m  
   �m  -   �m     �m     �m     �m     �m     n  2   $n     Wn  
   ]n     hn  	   vn     �n     �n     �n     �n  $   �n     �n  
   �n     �n     o     o     "o     *o     9o  
   Ho     So     ao     qo     �o     �o     �o     �o     �o     �o     �o     �o     p  
   p  
   p  �   %p     �p     �p     �p     �p     	q     q     q     3q     Bq     Qq     `q     {q     �q  :   �q     �q  %   �q     r     )r     =r     Vr     kr  0   {r     �r     �r  
   �r  &   �r     �r     s     +s     8s     Hs     Ns  
   _s     js     rs     �s     �s     �s     �s     �s     �s     �s     �s     �s     �s  
   �s     �s     t     t  =   0t  ?   nt  >   �t  N   �t  1   <u  7   nu  #   �u  Q   �u  R   v  �   ov  d   w  �   hw  X   x  �   Zx  �   �x  [   hy  �   �y  �   Nz  7   {     >{  �   E{     �{     p|     �|     �|     �|     �|     �|  
   �|  	   �|     �|     �|  W   }     k}     r}     �}     �}     �}  X   �}     �}     ~     1~  $   C~     h~     �~  \   �~     �~  L     	   S     ]  	   s  	   }     �  W   �     �     �     �  
   �     �     �     !�     %�  [   ,�  k   ��     �     	�     �  	   �     �     -�     9�     B�     O�     U�     ]�     o�     v�     ��     ��     ��     ��     ��       
   Ɂ     ԁ     �     �      ��     �     2�     ?�     K�     P�     U�     [�  	   d�     n�     z�     ��     ��  
   ��     ��  	   ��     ��  	   ��     ʂ     Ђ     ݂     �     �     �     �     �     �  
   �     *�     6�     =�     D�     W�     h�     l�     s�  
   ��     ��     ��  	   ��  	   ��     ��     ̃     Ճ     �     ��     �     �     )�  
   <�     G�     N�     ]�     f�     r�     ��     ��     ��     ��     ��     Ǆ     Є     ݄     �     ��     �     �     �     %�     3�  	   :�     D�  	   K�     U�     c�  	   h�      r�     ��     ��  	   ƅ     Ѕ     �     �  E  ��     @�  
   Q�     \�   		This plugin provides bar for bookmarks.
		 %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i Attachment %i Attachments %i errors occurred, see log %i file will be deleted %i files will be deleted %i warnings occurred, see log <Top> <Unknown> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Add Application Add Bookmark Add Notebook Add bookmark/Show settings Add column Add new bookmarks to the beginning of the bar Add row Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files All Tasks Allow public access Always use last cursor position when opening a page An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Arithmetic Attach File Attach image first Attachment Browser Attachments Attachments: Author Auto
Wrap Auto indenting Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave version on regular intervals Back to Original Name BackLinks BackLinks Pane Backend Backlinks: Bazaar Bookmarks BookmarksBar Border width Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Capture whole screen Center Change columns Changes Characters Characters excluding spaces Check Checkbox '>' Check Checkbox 'V' Check Checkbox 'X' Check _spelling Checkbo_x List Clear Clone row Code Block Command Command does not modify data Comment Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Copy Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Created Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Demote Dependencies Description Details Discard note? Distraction Free Editing Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root Due E_xport... Edit %s Edit Custom Tool Edit Image Edit Link Edit Table Edit _Source Editing Editing file: %s Enable Version Control? Enable plugin Enabled Equation Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Fossil Get more plugins online Get more templates online Git Gnuplot Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide menubar in fullscreen mode Highlight current line Home Page Horizontal _Line Icon Images Import Page Include subpages Index Index page Insert Code Block Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Sequence Diagram Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Last Modified Leave link to new page Left Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Map document root to URL Match _case Maximum number of bookmarks Maximum page width Menubar Mercurial Modified Month Move Selected Text... Move Text to Other Page Move text to Name Need output file to export MHTML Need output folder to export full notebook New File New Page New Page in %s New S_ub Page... New _Attachment Next No Applications Found No changes since last version No dependencies No document root defined for this notebook No such file: %s No such page: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open help Open in New Window Open in New _Window Open new page Open plugins folder Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Name Page Template Page already exists: %s Page not allowed: %s Paragraph Paste Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please select a row, before you push the button. Please specify a notebook Plugin Plugins Port Position in the window Pr_eferences Preferences Prev Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Remove column Remove row Removing Links Rename page "%s" Replace _All Replace with Restore page to saved version? Rev Right Right Side Pane Right margin position Row down Row up S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Screenshot Command Search Search _Backlinks... Search this section Section Section(s) to ignore Section(s) to index Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Sequence Diagram Server not started Server started Server stopped Set New Name Set default text editor Set to Current Page Show Line Numbers Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show full Page Name Show full page name Show helper toolbar Show in the toolbar Show right margin Show the cursor also for pages that can not be edited Show the page title heading in the ToC Single _page Size Some error occurred while running "%s" Sort alphabetically Sort pages by tags Source View Spell Checker Start Start _Web Server Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Task Task List Template Templates Text Text Files Text From _File... Text background color Text foreground color The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The table must consist of at least on row!
 No deletion done. There are no changes in this notebook since the last version that was saved This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This page name cannot be used due to technical limitations of the storage This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin also has properties,
see the notebook properties dialog This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This plugin turns one section of the notebook into a journal
with a page per day, week or month.
Also adds a calendar widget to access these pages.
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox '>' Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Tray Icon Type Un-check Checkbox Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unkown Image type Unkown Object Unspecified Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use a page for each Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Zim Desktop Wiki Zoom _Out _About _Add _All Panes _Arithmetic _Back _Browse _Bugs _Cancel _Checkbox _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Edit _Edit Link _Edit Link or Object... _Edit Properties _Edit... _Emphasis _FAQ _File _Find _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _Move Here _New Page... _Next _None _Normal Size _Numbered List _OK _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In as due date for tasks as start date for tasks calendar:week_start:0 do not use horizontal lines readonly seconds translator-credits vertical lines with lines {count} of {total} Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2020-09-17 05:43+0000
Last-Translator: Allan Nordhøy <epost@anotheragency.no>
Language-Team: Norwegian Bokmål <https://hosted.weblate.org/projects/zim/master/nb_NO/>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.3-dev
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
 		Denne utvidelsen legger til linjer for bokmerker
		 %(cmd)s
returnerte ikke-null avslutningstatus %(code)i %(n_error)i feil og %(n_warning)i advarsler oppstod, se logg %A %d %B %Y %i vedlegg %i vedlegg %i feil oppstod, se logg %i fil vil slettes %i filer vil slettes %i advarsler oppstod, se logg <Topp> <Ukjent> Wiki for Skrivebordet En fil med navnet <b> "%s" </b> finnes allerede.
Du kan bruke et annet navn eller overskrive den eksisterende filen. Tabellen må bestå av minst én kolonne Legg til applikasjon Legg inn bokmerke Legg til notisblokk Legg til bokmerker/vis innstillinger Legg til kolonne Legger bokmerker i begynnelsen av bokmerkelinjen Legg til rad Legger til stave­kontroll ved å bruke gtkspell

Dette er en programtillegg som følger med Zim.
 Juster Alle filer Alle oppgaver Tillat offentlig tilgang Alltid bruk den siste markørposisjon ved åpning av side En feil oppstod under generering av bildet.
Ønsker du å lagre kilde teksten allikevel? Kommentert sidekilde Programmer Aritmetikk Legg ved fil Legg ved bildet først Vedleggsutforsker Vedlegg Vedlegg: Forfatter Tekst
brytning Automatiske innrykk Automatisk lagret versjon fra Zim Velg det gjeldende ordet automatisk når du bruker formatering Gjør "CamelCase" ord om til lenker automatisk Gjør filstier om til lenker automatisk Autolagre ved faste intervaller Tilbake til det opprinnelige navnet Linker hit Panel for linker hit Bakgrunnsløysing Tilbakelenker: Bazaar Bokmerker Bokmerkebar Kantbredde Bunn Panel Bla gjennom Punk_t Liste K_onfigurer Kan ikke redigere siden: %s Avbryt Ta bilde av hele skjermen Midtstilt Endre rader Endringer Tegn Tegn uten mellomrom Huk av avkryssningsboksen '>' Huk av avkryssningsboksen 'V' Huk av avkryssningsboksen 'X' Kjør _stavekontroll Avmerking_s Liste Fjern Klon rad Kodeblokk Kommando Data vil ikke bli endret av kommando Merknad Komplett _notebook Konfigurer programmer Konfigurer programtillegg Konfigurer en applikasjon til å åpne "%s" lenker Konfigurer et program til å åpne filer
av typen "%s" Kopier Kopier e-postadresse Kopier mal Kopier _som... Kopier _Kobling Kopier _lokasjon Kunne ikke finne kjørbar fil: "%s" Fant ikke notatblokk: %s Kunne ikke finne mal "%s" Kan ikke finne filen eller katalogen til denne notatblokken Kunne ikke laste inn stavekontroll Kan ikke åpne: %s Kunne ikke analysere uttrykk Kunne ikke lese: %s Kunne ikke lagre side: %s Lag en ny side for hvert notat Opprett mappe? Laget Klipp_ut tilpassede verktøyer Brukerdefinerte_Verktøy Tilpass... Dato Dag Standard Standardformatet for å kopiere tekst til utklippstavlen Forvalgt notatblokk Forsinkelse Slett Side Slett siden "%s"? Slett rad Senk Avhengigheter Beskrivelse Detaljer Forkast notat? Fri for distraksjoner modus Ønsker du å slette alle bokmerker? Vil du gjenopprette siden: %(page)s
til lagret verjson: %(version)s ?

Alle endringer siden den siste lagrede versjonen vil bli tapt! Dokumentrot Frist E_ksporter... Rediger %s Rediger tilpasset verktøy Rediger bilde Rediger lenke Rediger tabell Rediger _Kilde Redigering Rediger fil: %s Aktivere versjonskontroll? Skri på programtillegg Aktivert Ligning Feil i %(file)s på linje %(line)i nær "%(snippet)s" Evaluer _Matte Ekspander alle Eksportér Eksporter alle sider til en separat fil Eksport utført Eksporter hver enkelt side til separate filer Eksporterer notebook Feilet Kunne ikke kjøre: %s Kan ikke kjøre programmet: %s Filen finnes Fil _Maler Filen er endret på disk: %s Filen eksisterer Filen kan ikke skrives til: %s Fil type er ikke støttet: %s Filnavn Filter Finn Finn ne_ste Finn forr_ige Søk og erstatt Søk etter Flagg oppgaver som skal gjøres på mandag eller tirsdag før ukeslutt Mappe Mappen eksisterer allerede og har innhold. Eksportering til denne mappen kan overskrive eksisterende filer. Vil du fortsette? Mappen eksisterer: %s Mappe med maler for vedleggs filer For avansert søk kan du bruke operatører som
AND, OR og NOT. Se hjelpesiden for mer informasjon. For_mat Format Fossil Hent flere programtillegg på nett Hent flere maler fra internett Git Gnuplot Overskriftstittel _1 Overskriftstittel _2 Overskriftstittel _3 Overskriftstittel _4 Overskriftstittel _5 Høyde Skjul menylinjen i fullskjermmodus Uthev aktiv linje Startside Vannrett _linje Ikon Bilder Importer side Inkluder undersider Indeks Sideregister Sett inn kodeblokk Sett inn dato og klokkeslett Sett inn diagram Sett inn Ditaa Sett inn ligning Sett inn GNU R Plot Sett inn Gnuplot Sett inn bilde Sett inn lenke Sett inn noteark Legg til skjermbilde Sett inn sekvensdiagram Sett inn symbol Sett inn tabell Sett inn tekst fra fil Grensesnitt Interwiki Tastatur Journal Gå til Gå til Side Sist endret Legg ved lenke til ny side Til venstre Venstre Side Panel Linjesortering Linjer Lenke Kart Link filer i dokumentrota med fullstendige filstier Link til Lokalisasjon Logg hendelser med Zeitgeist Loggfil Du har funnet en programfeil Opprett forvalgt applikasjon Rotkartdokument til nettadresse Skill mellom store og små bokstaver Maksimalt antall bokmerker Maksimums side størrelse Menylinje Mercurial Endret Måned Flytt valgt tekst... Flytt tekst til annen side Flytt tekst til Navn Trenger utdatafil for å eksportere MHTML Trenger utdatamappe for å eksportere hele notatboken Ny fil Ny side Ny side i %s Ny _underside... Nytt Vedlegg Neste Ingen programmer funnet Ingen endringer siden siste versjon Ingen avhengigheter Inget dokument definert for denne notisboken Fil finnes ikke: %s Siden finnes ikke: %s Denne wiki`en er ikke definert: %s Ingen maler er installert Notisblokk Notatblokker OK Åpne Vedleggets _Mappe Åpne mappe Åpne Notisblokk Åpne med … Åpne _rotdokument Åpne _Notisblokkmappe Åpne _Side Åpne hjelp Åpne i nytt vindu Åpne i nytt _vindu Åpne ny side Åpne plugin mappen Åpne med "%s" Valgfritt Alternativer for programtillegg %s Annet... Utdata-fil Utdata fil eksisterer, spesifiser "--overwrite" for å tvinge eksport Mappe for utdata Utdata mappen eksisterer eller er ikke tom, spesifiser "--overwrite" for å tvinge eksport Utdata plassering trengs for eksport Utdata bør erstatte gjeldende valg Skriv over S_tilinje Side Siden "%s" har ikke en mappe for vedlegg Navn på side Sidemal Siden finnes allerede: %s Siden tillates ikke: %s Avsnitt Lim inn Vennligst legg til en kommentar til versjonen Merk at lenker til sider som ikke eksisterer
vil opprette siden automatisk. Velg navn og mappe for notisblokken Vennligst velg en rad, Vennligst velg en notatbok Programtillegg Programtillegg Port Plassering i vindu Innstilling_er Innstillinger Forrige Skriv ut til nettleser Hev _Egenskaper Egenskaper Publiserer hendelser til Zeitgeist tjenesten. Raskt notat Raskt notat... Nylige endringer Nylige endringer... Nylige endrede sider Fortløpende omformatering av wiki-markeringstekst Fjern Fjern alle Fjern kolonne Fjern rad Fjerner linker Omdøp side "%s" Erstatt _alle Erstatt med Gjenopprett side til lagret versjon? Versjon Til høyre Høyre Side Panel Høyre margplassering Rad ned Rad opp L_agre Versjon Lagre en _kopi Lagre kopi Lagre Versjon Lagre bokmerker Lagret versjon fra zim Poengsum Skjermbilde Kommando Søk Søk_tilbakelenker Søk i dette avsnittet Seksjon Avsnitt å ignorere Avsnitt å indeksere Velg Fil Velg Mappe Velg bilde Velg en versjon for å se endringer mellom den og nåværende versjon. Eller velg flere versjoner for å se endringer dem imellom.
 Velg eksportformat Velg utdata fil eller mappe Velg sidene for eksport Velg vindu eller område Utvalg Sekvensdiagram Server ikke startet Server startet Server stanset Angi nytt navn Sett standard tekstprogram Sett til nåværende side Vis linjenummer La ToC flyte på siden i stedet for å vises i sidepanelet Vis _endringer Vis et separat ikon for hver notatbok Vis hele sidenavnet VIs hele sidenavnet Vis hjelpeverktøylinjen Vis i verktøylinjen Vis høyre marg Vis også peker for sider som ikke kan redigeres Vis sidetittel i ToC Enkel _side Størrelse En feil skjedde under kjøring av "%s" Sorter alfabetisk Sorter sider etter tagger Kildevisning Stave­kontroll Start Start _webserver Sy_mbol... Syntaks Systemstandard Tabulatorbredde Tabell Tabellredigering Innholdsfortegnelse Tagger Oppgave Oppgaveliste Mal Maler Tekst Tekstfiler Tekst fra _fil... Tekst bakgrunnsfarge Tekst forgrunnsfarge Katalogen
%s
eksisterer ikke.
Ønsker du å opprette den nå? Katalogen "%s" eksisterer ikke. Ønsker du å opprette den nå? Tabellen må bestå av minst en rad!
 Ingen sletting foretatt. Det er ingen endringer i denne notatboken siden forrige versjon som ble lagret Dette betyr kanskje at du mangler installasjon av Denne filen eksisterer allerede.
Vil du overskrive den? Denne siden har ingen vedleggsmappe Dette sidenavnet kan ikke brukes som følge av tekniske begrensninger i lagringen Dette programtillegget lar deg ta et skjermbilde å settte
det inn i en Zim side.
 Dette programtillegget gir en ekstra widget som viser en liste over sider
koblet til den gjeldende siden.

Dette programstilleget følger med Zim.
 Programtillegg som legger inn innstillinger som
lettere lar deg skrive i Zim uten å bli forstyrret
 Dette programtillegget kan du legge inn aritmetiske beregninger i Zim.
Den er basert på den aritmetiske modulen fra
http://pp.com.mx/python/arithmetic
 Dette programtillegget har også egenskaper,
sjekk dialogvinduet for notisbok-egenskaper Dette programtillegget gir deg muligheten til å endre et diagram for Zim basert på Graphviz.

Dette er et kjerne plugin frakt med Zim.
 Dette programtillegget tilfører zim et noteskriving bygget på GNU Lilypond.
Dette er et fast programtillegg som følger med zim.
 Dette programtillegget viser vedleggsmappen til den aktive siden som ikoner i bunnpanelet.
 Dette programtillegget sorterer valgte linjer etter alfabetet.
Hvis listen allerede er sortert, reverseres rekkefølgen.
(A-Å til Å-A)
 Dette programtillegget omgjør en seksjon av notatboken til en journal
med en side per dag, uke eller måned.
Legger også til en kalender-widget for å få tilgang til disse sidene.
 Dette betyr vanligvis at filen inneholder ugyldige tegn Tittel For å fortsette kan du lagre en kopi av siden eller forkaste
alle endringer. Hvis du lagrer en kopi vil endringer også
forkastes, men du kan gjenopprette kopien senere. For å opprette en ny notatbok må du velge en tom mappe.
Du kan selvfølgelig også velge en eksisterende zim-notatbok-mappe.
 Innholdsfortegnelse I _Dag I dag Veksle avkryssningsboks 'v' Veksle avkryssingsboks 'V' Veksle avkryssingsboks 'X' Topp Panel Panelikon Type Tøm avkryssningsboks Fjern innrykk med <BackSpace>
(om det er deaktivert kan du fortsatt bruke <Shift><Tab>) Ukjent Ukjent bildetype Ukjent objekt Ikke oppgitt Utagget Oppdater %i side som lenker til denne siden Oppdater %i sider som lenker til denne siden Oppdater tittel på denne siden Oppdaterer linker Oppdaterer indeks Bruk %s for å bytte til sidepanelet Brukerdefinert Skrifttype Bruk en side til hver Bruk <Enter> for å følge lenker
(Om denne er slått av kan du fortsatt bruke <Alt><Enter>) Versjonskontroll Versjonskontroll er ikke aktivert for denne notatboken.
Vil du aktivere den? Versjoner Vis med _anmerkninger Vis _logg Webtjener Uke Når du rapporterer denne programfeilen inkluder
informasjonen i tekst boksen nedenfor. Hele _ordet Bredde Wiki side: %s Ordtelling Antall ord... Ord År I går Du redigerer filen i en ekstern applikasjon. Du kan lukke denne dialogen når du er ferdig. Du kan endre de brukerdefinerte verktøyinnstillingene som vil vises
i verktøylinjen eller kontekstmenyer. Zim Skrivebords-wiki Zoom Ut _Om _Legg til _Alle Paneler _Aritmetikk Til_bake _Bla gjennom _Feil _Avbryt _Avkryssningsboks _Under _Nullstill formatering _Lukk S_lå sammen alle _Innhold _Kopier _Kopier hit _Slett Slett side Forkast en_dringer _Endre _Endre Kobling _Rediger kobling eller objekt... Rediger _egenskaper _Rediger … _Ettertrykk _OSS _Fil _Finn _Finn... _Fremover _Fullskjerm _Gå til _Hjelp _Markér _Historikk _Hjem _Bilde... _Importer side... Sett _inn _Hopp _Hopp til... _Tastebindinger _Lenke _Link til dato _Kobling _Merke _Mer _Flytt _Flytt hit _Ny Side... _Neste I_ngen _Normal Størrelse _Nummerert Liste _OK _Åpne _Åpne annen notisblokk _Annet ... _Side _Side Hirarki _Forelder _Lime inn _Forhåndsvis _Forrige _Skriv ut til nettleser _Hurtig Notat... _Avslutt _Nylig brukte sider _Gjenopprette _Regulært uttrykk _Oppdatere _Fjern _Fjern kobling _Erstatt _Erstatt... _Nullstill Størrelsen _Gjenopprett versjon _Lagre _Lagre kopi _Skjermbilde... _Søk _Søk... _Send til... _Side Paneler _Side om side _Sorter linjer _Stryk _Sterk _Senket skrift _Hevet skrift M_aler Verk_tøy _Angre _Verbatim _Versjoner... _Vis _Zoom Inn som tidsfristdato for gjøremål som startdato for gjøremål calendar:week_start:1 ikke bruk vannrette linjer Skrivebeskyttet sekunder Launchpad Contributions:
  Allan Nordhøy https://launchpad.net/~comradekingu
  Bjørn Forsman https://launchpad.net/~bjorn-forsman
  Bjørn Olav Samdal https://launchpad.net/~bjornsam
  Børge Johannessen https://launchpad.net/~lmdebruker
  Emil Våge https://launchpad.net/~emil-vaage
  Espen Krømke https://launchpad.net/~espenk
  Jaap Karssenberg https://launchpad.net/~jaap.karssenberg
  Per Knutsen https://launchpad.net/~pmknutsen
  Playmolas https://launchpad.net/~playmolas
  Terje Andre Arnøy https://launchpad.net/~terjeaar
  sunnyolives https://launchpad.net/~sissi90 vertikale linjer med linjer {count} av {total} 