��    �     D              l  .   m     �  0   �     �     �     �  i   	     s     �  V   �  	   �  	   �     �  3     Y   C     �  
   �     �     �     �     �     �  $     ?   (  /   h  (   �  %   �  	   �     �                              "   
   /      :      R      Y      n   
   v      �      �   <   �      �      �      �      !     !     &!     <!     O!     f!  +   w!  3   �!      �!     �!     "     "  
   %"     0"     ?"  3   ["     �"     �"     �"     �"     �"     #     #     #     )#     7#     D#     I#     M#  0   U#     �#     �#     �#     �#     �#     �#     �#     �#     �#     �#  ~   
$     �$  
   �$     �$  
   �$  	   �$     �$     �$     �$     �$     %     %     %     )%     0%     C%     J%     ]%     {%     �%     �%     �%     �%     �%     �%     �%     &  
   	&     &     #&  	   4&  6   >&     u&  v   |&     �&  *   '  c   0'     �'     �'     �'     �'  
   �'  
   �'  
   �'  
   �'  
   �'     �'     �'  	   (     (     (     #(     /(  
   5(     @(     R(     g(     v(     �(     �(     �(     �(     �(     �(     �(     �(     �(  	   )     )     ,)     4)     <)     I)     ^)     l)     �)     �)     �)     �)  2   �)     �)     �)     �)     *     *     /*     H*     a*     m*  	   �*     �*     �*     �*     �*     �*     �*     �*     �*     �*     �*     +     "+     @+     P+     a+     z+     �+  	   �+     �+     �+     �+     �+     �+     �+     �+  
   ,     ,     0,     >,     M,     V,     l,     u,     �,  	   �,     �,     �,  0   �,  	   �,     �,  	   �,  '   �,  V   "-  3   y-     �-     �-     �-     �-     �-     �-     �-     .     
.  
   .  &   !.  
   H.     S.     a.     p.     �.     �.     �.     �.     �.     �.     �.     /     /     '/     8/  	   H/     R/     _/     v/     |/     �/     �/     �/     �/  �   �/     O0      h0     �0     �0  	   �0     �0     �0     �0  2   �0     *1  &   81     _1  5   s1     �1     �1  &   �1     �1     �1     	2     2  
   )2     42     C2     U2     Z2     x2  	   }2     �2  	   �2     �2  
   �2     �2     �2     �2  ?   �2  A   )3  S   k3  K   �3  @   4  6   L4  -   �4  x   �4  �   *5  �   �5  �   x6  �   7  }   �7  L   8  �   R8  �   �8  �   �9  }   :  h   �:  k   �:  �   f;  R   :<  ;   �<  =   �<    =  j   >  n   �>     �>  7   u?     �?  �   �?     O@     S@     Z@     `@     t@     �@  	   �@  '   �@     �@  D   �@     A     A  H   A     gA     �A     �A     �A     �A  P   �A     B  U   ,B     �B     �B  	   �B  
   �B     �B  N   �B     C     C     C  
   $C     /C     =C     CC  	   HC  ^   RC  f   �C     D  	   )D     3D  
   :D     ED     QD     WD     _D     eD     lD     ~D     �D  	   �D     �D  
   �D     �D     �D     �D     �D  
   �D     �D     �D  	   E     E     E     #E     ,E     5E     AE     EE  
   KE     VE     _E  	   eE     oE     E     �E     �E     �E     �E     �E     �E     �E     �E  
   �E     �E     �E     �E     �E      F     F     F  	   /F     9F     ?F     GF     NF  	   WF     aF     sF     �F     �F     �F     �F     �F     �F     �F     �F     �F     �F     �F  
   �F     G     G  
   G     *G     6G     BG     PG     \G     dG  
   lG     wG  
   �G     �G     �G  	   �G     �G     �G     �G     �G     �G     �G     �G  !  �G  /   J     NJ  h   \J  K   �J     K     K  h   'K     �K     �K  R   �K     L     L     L  :   2L  Z   mL     �L  
   �L     �L     �L     M     "M     *M  $   0M  ;   UM  0   �M  *   �M  .   �M     N     .N     HN     PN     WN     gN     nN     �N      �N  	   �N     �N  	   �N     �N     �N     �N  L   O     QO     ZO     _O     yO     �O     �O     �O     �O     �O  +   �O  1   
P  '   <P  !   dP     �P     �P     �P     �P  "   �P  1   �P     Q     5Q     SQ     hQ  '   �Q     �Q     �Q     �Q     �Q     �Q     R     R     R  5   R     JR     \R     bR  !   pR     �R  
   �R     �R     �R      �R     �R  �   �R     uS     �S     �S     �S     �S  
   �S     �S     �S  (   �S  
   T     "T     4T     BT     IT  
   ]T     hT  %   �T     �T     �T  &   �T     �T      U     3U     QU     ^U     eU     kU     |U     �U     �U  A   �U     �U  m   �U     eV  $   vV  p   �V     W     W     W     W     'W     0W     9W     BW     KW     TW  /   \W     �W     �W     �W     �W     �W     �W     �W     �W     �W     �W     X     X     )X     8X     EX     ZX     pX     �X      �X     �X     �X     �X  	   �X     �X     �X     Y  *   )Y     TY     jY     �Y     �Y  <   �Y  
   �Y     �Y  %   �Y     Z      $Z     EZ     \Z     |Z     �Z  	   �Z     �Z     �Z     �Z      �Z     	[     [     ![  
   /[     :[     M[     [[      v[     �[     �[  !   �[     �[     �[     \     \     \  
   %\     0\     ?\     K\     b\     v\     �\     �\     �\     �\     �\  	   �\     �\     �\     ]     ]     ]     $]  
   D]     O]     _]     h]  D   �]  !   �]     �]     �]     �]     ^  
   ^  	    ^     *^     >^  
   K^  	   V^  .   `^     �^     �^     �^     �^     �^  #   �^     _     6_     K_  
   Y_  3   d_  
   �_     �_     �_     �_     �_     �_  $   `     )`     2`     :`     Y`  
   h`     s`  �   `     a  "   #a     Fa     _a     {a     �a     �a     �a  N   �a     b  '   +b     Sb  9   mb     �b     �b  )   �b     �b     �b     c     &c     Ac     Mc     jc     yc     �c     �c     �c     �c     �c     �c     �c     �c     �c     d  6   'd  8   ^d  A   �d  A   �d  ;   e  2   We     �e  �   �e  �   -f  �   g  �   �g  �   h  �   �h  L   %i  �   ri  �   �i  �   �j  s   )k  d   �k  n   l  �   ql  O   ,m  B   |m  E   �m    n  i   o  q   |o  �   �o  9   p     �p  �   �p     tq     �q     �q     �q     �q     �q     �q  .   �q     r  `   #r     �r     �r  �   �r     Vs     os     �s     �s     �s  |   �s     Et  Q   Vt  
   �t     �t     �t     �t     �t  O   �t     >u     Nu     Vu     fu     tu     �u     �u     �u  R   �u  o   �u     ^v     ov     xv     �v     �v     �v  
   �v  	   �v     �v     �v     �v     �v     �v     �v     w  	   w     w     .w     @w     Gw     Ww     ww     �w     �w  	   �w  
   �w     �w     �w     �w     �w     �w  
   �w     �w  
   �w     �w     x     x     &x  	   ;x     Ex     Xx     fx     ox  	   ux     x     �x  
   �x     �x     �x     �x     �x     �x  
   �x     y     
y     y  
   &y  
   1y     <y     Qy     hy     qy  	   �y     �y     �y     �y  	   �y     �y     �y     �y     �y     z     z     *z     3z     @z     Pz     bz     sz  
   �z     �z  
   �z  
   �z  	   �z     �z     �z  	   �z     �z     �z     �z     �z     {     {  �   %{   %(cmd)s
returned non-zero exit status %(code)i %A %d %B %Y %i file will be deleted %i files will be deleted %i open item %i open items <Top> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. Add Application Add Notebook Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 All Files All Tasks Allow public access Always use last cursor position when opening a page An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Arithmetic Attach File Attach image first Attachment Browser Attachments Author Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave version on regular intervals BackLinks BackLinks Pane Backend Bazaar Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Capture whole screen Changes Characters Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Command Command does not modify data Comment Common include footer Common include header Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Could not find notebook: %s Could not find the file or folder for this notebook Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Demote Dependencies Description Details Discard note? Distraction Free Editing Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root E_xport... Edit Custom Tool Edit Image Edit Link Edit _Source Editing Editing file: %s Enable Version Control? Enabled Evaluate _Math Expand _All Export Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Git Gnuplot Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide menubar in fullscreen mode Home Page Icon Images Import Page Index Index page Inline Calculator Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Symbol Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Labels marking tasks Last Modified Leave link to new page Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Map document root to URL Match _case Maximum page width Mercurial Modified Month Move Selected Text... Move Text to Other Page Move text to Name New File New Page New S_ub Page... New _Attachment No Applications Found No changes since last version No dependencies No such file: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open in New _Window Open new page Open with "%s" Optional Options for plugin %s Other... Output file Output folder Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Name Page Template Paragraph Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Plugin Plugins Port Position in the window Pr_eferences Preferences Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Removing Links Rename page "%s" Replace _All Replace with Restore page to saved version? Rev Right Side Pane S_ave Version... Save A _Copy... Save Copy Save Version Saved version from zim Score Search Search _Backlinks... Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Server not started Server started Server stopped Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show in the toolbar Show the cursor also for pages that can not be edited Single _page Size Some error occurred while running "%s" Sort alphabetically Sort pages by tags Spell Checker Start _Web Server Sy_mbol... System Default Table of Contents Tags Tags for non-actionable tasks Task Task List Template Templates Text Text Files Text From _File... Text background color Text foreground color The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The inline calculator plugin was not able
to evaluate the expression at the cursor. There are no changes in this notebook since the last version that was saved This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. ToC To_day Today Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Tray Icon Turn page name into tags for task items Type Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use a custom font Use a page for each Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Zim Desktop Wiki Zoom _Out _About _All Panes _Arithmetic _Back _Browse _Bugs _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Edit _Edit Link _Edit Link or Object... _Edit Properties _Emphasis _FAQ _File _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _Move Here _New Page... _Next _None _Normal Size _Numbered List _Open _Open Another Notebook... _Other... _Page _Parent _Paste _Preview _Previous _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In calendar:week_start:0 readonly seconds translator-credits Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2013-04-14 19:38+0000
Last-Translator: Andrej Znidarsic <andrej.znidarsic@gmail.com>
Language-Team: Slovenian <sl@li.org>
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
X-Generator: Launchpad (build bceb5ef013b87ef7aafe0755545ceb689ca7ac60)
 ukaz %(cmd)s
je vrnil stanje končanja %(code)i %A, %d. %B %Y %i datotek bo izbrisanih %i datoteka bo izbrisana %i datoteki bosta izbrisani %i datoteke bodo izbrisane %i odprtih predmetov %i odprt predmet %i odprta predmeta %i odprti predmeti <Zgoraj> Namizni wiki Datoteka z imenom <b>"%s"</b> že obstaja.
Uporabite lahko drugo ime ali prepišete obstoječo datoteko. Dodaj program Dodajanje beležke Doda podporo črkovanja z uporabo gtkspell.

To je jedrni vstavek, ki je del zim.
 Vse datoteke Vsa opravila Dovoli javni dostop Pri odpiranju strani vedno uporabi zadnji položaj kazalca Med ustvarjanjem slike je prišlo do napake.
Ali vseeno želite shraniti izvorno besedilo? Vir zabeležene strani Aritmetika Priloži datoteko Najprej pripni sliko Brskalnik prilog Priloge Avtor Samodejno shranjena različica z zim Samodejno izberi trenutno besedo ob uveljavitvi oblikovanja Samodejno pretvori besede "CamelCase" v povezave Samodejno pretvori poti datotek v povezave Samodejno shrani različico ob rednih obdobjih Povratne povezave Pladenj povratnih povezav Zaledje Bazaar Spodnji pladenj Brskaj Vrs_tični seznam _Nastavi Ni mogoče spremeniti strani: %s Prekliči Zajemi celoten zaslon Spremembe Znaki Preveri _črkovanje S_eznam spustnih polj Običajna ikona sistemske vrstice
en uporabite nove ikone stanja na Ubuntuju Počisti Ukaz Ukaz ne spremeni podatkov Opomba Skupna noga vključi Skupna glava vključi Celotna _beležka Nastavi programe Nastavi vstavek Nastavite program za odpiranje povezav "%s" Nastavite program za odpiranje datotek
vrste "%s" Obravnavaj vsa izbirna polja kot naloge Kopiraj naslov elektronske pošte Kopiraj predlogo Kopiraj _kot ... _Kopiraj povezavo Kopiraj _mesto Beležke ni bilo mogoče najti: %s Ni mogoče najti datoteke ali mape za to beležko Ni mogoče odpreti: %s Ni mogoče razčleniti izraza Ni mogoče brati: %s Ni mogoče shraniti strani: %s Ustvari novo stran za vsako sporočilce Ali želite ustvariti mapo? I_zreži Orodja po meri _Orodja po meri Prilagodi ... Datum Dan Privzeto Privzeta oblika za kopiranje besedila na odložišče Privzeta beležka Zamik Izbris strani Ali želite izbrisati stran "%s"? Nižja raven Odvisnosti Opis Podrobnosti Ali želite zavreči sporočilo? Urejanje brez motenj Ali želite obnoviti stran: %(page)s
na shranjeno različico: %(version)s?

Vse spremembe od zadnjega shranjevanja bodo izgubljene! Koren dokumenta _Izvozi ... Uredi orodje po meri Urejanje slike Uredi povezavo Uredi _vir Urejanje Urejanje datoteke: %s Ali želite omogočiti nadzor različic? Omogočeno Oceni _matematiko Razširi _vse Izvozi Izvažanje beležke Spodletelo Poganjanje je spodletelo: %s Zaganjanje programa je spodletelo: %s Datoteka že obstaja _Predloge datotek ... Datoteka se je spremenila na disku: %s Datoteka že obstaja V datoteko ni mogoče pisati: %s Vrsta datoteke ni podprta: %s Ime datoteke Filter Najdi Najdi _naslednje Najdi _predhodno Najdi in zamenjaj Besedilo iskanja Označi naloge, ki imajo rok v ponedeljek ali torek pred vikendom Mapa Mapa že obstaja in ima vsebino. Izvoz v to mapo lahko prepiše obstoječe datoteke. Ali želite nadaljevati? Mapa obstaja: %s Mape s predlogami za datoteke prilog Za napredno iskanje lahko uporabite operatorje kot
AND, OR in NOT. Oglejte si stran pomoči za več podrobnosti. O_blika Oblika Git Gnuplot Naslov_1 Naslov_2 Naslov_3 Naslov_4 Naslov_5 Višina Skrij menijsko vrstico v celozaslonskem načinu Domača stran Ikona Slike Uvozi stran Kazalo Kazalo Medvrstično računalo Vstavi datum in čas Vstavi diagram Vstavi Ditaa Vstavi enačbo Vstavi GNU R graf Vstavi Gnuplot Vstavi sliko Vstavljanje povezave Vstavljanje partiture Vstavi zaslonski posnetek Vstavi simbol Vstavljanje besedila iz datoteke Vmesnik Ključna beseda medwiki Dnevnik Skoči na Skoči na stran Oznake za označevanje nalog Zadnja sprememba Pustite povezavo za ustvaritev nove strani Levi stranski pladenj Razvrščevalnik vrstic Vrstice Zemljevid povezav Poveži datoteke pod korenom dokumenta s polno potjo datotek Poveži na Mesto Beleži dogodke s programom Zeitgeist Dnevniška datoteka Videti je,da ste našli hrošča Naredi program privzet Preslikaj koren dokumenta v URL Ujemanje _velikosti črk Največja širina strani Mercurial Spremenjeno Mesec Premakni izbrano besedilo ... Premakni besedilo na drugo stran Premakni besedilo v Ime Nova datoteka Nova stran Nova po_dstran ... Nova _priloga Ni bilo najdenih programov Ni sprememb od zadnje različice Brez odvisnosti Ni takšne datoteke: %s Takšnega wikija ni določena: %s Ni nameščenih predlog Beležka Beležke V redu Odpri _mapo prilog Odpri mapo Odpri beležko Odpri z ... Odpri koren _dokumenta Odpri mapo _beležk Odpri _stran Odpri v novem _oknu Odpri novo stran Odpri s programom "%s" Izbirno Možnosti za vstavek %s Drugo ... Izhodna datoteka Izhodna mapa Prepiši Vrstica _poti Stran Stran "%s" nima mape za priloge Ime strani Predloga strani Odstavek Vnesite opombo za to različico Povezovanje na neobstoječo stran
samodejno ustvari tudi novo stran. Izberite ime in mapo za beležko. Vstavek Vstavki Vrata Položaj v oknu _Možnosti Možnosti Natisni v brskalnik Višja raven _Lastnosti Lastnosti Objavi dogodke v ozadnjemu programu Zeitgeist. Hitro sporočilce HItro sporočilce ... Nedavne spremembe Nedavne spremembe ... Strani nedavno _spremenjeno Sproti preoblikuj označevanje wiki Odstranjevanje povezav Preimenuj stran "%s" Zamenjaj _vse Zamenjaj z Ali želite stran obnoviti na shranjeno različico? Različica Desni stranski pladenj S_hrani različico ... Shrani _kopijo ... Shrani kopijo Shrani različico Shranjena različica iz programa zim Rezultat Iskanje Iskanje _povratnih povezav ... Izbor datoteke Izbor mape Izbor slike Izberite različico za ogled sprememb med tisto različico in trenutnim stanjem.
Izberete lahko več različic za ogled sprememb med njimi.
 Izberite obliko izvoza Izberite izhodno datoteko ali mapo Izberite strani za izvoz Izberite okno ali področje Izbor Strežnik se ni zagnal Strežnik se je zagnal Strežnik se je zaustavil Pokaži gradnik kazala vsebine kot ledbeč gradnik namesto v stranskem pladnju Pokaži _spremembe Pokaži ločeno ikono za vsako beležko Prikaži v orodni vrstici Pokaži kazalko tudi za strani, ki jih ni mogoče urejati Posamezna _stran Velikost Med poganjanjem "%s" je prišlo do napake Razvrsti po abecedi Razvrsti strani po oznakah Črkovalnik Zaženi _spletni strežnik Si_mbol ... Privzete sistemske vrednosti Kazalo vsebine Oznake Oznake za naloge brez dejanj Naloga Seznam nalog Predloga Predloge Besedilo Besedilne datoteke _Besedilo iz datoteke ... Barva ozadja besedila Barva ospredja besedila Mapa
%s
še ne obstaja.
Ali jo želite ustvariti zdaj? Mapa "%s" še ne obstaja.
Ali jo želite ustvariti zdaj? Medvstični vstavek računala ni mogel
oceniti izraza na kazalki. V tej beležki od shranjevanja zadnje različice ni bilo sprememb To lahko pomeni, da nimate nameščenih
ustreznih slovarjev Ta datoteka že obstaja.
Ali jo želite prepisati? Ta stran nima mape prilog Ta vstavek omogoča zajemanje zaslonskega posnetka
in njegovo neposredno vstavljanje v stran zim.

To je jedrni vstavek, ki je del zim.
 Ta vstavek doda pogovorno okno, ki prikazuje vse odprte
naloge v tej beležki. Odprte naloge so lahko odprta
izbira polja ali stvari označene z oznakami "TODO" ali "FIXME".

To je jedrni vstavek, ki je del zim.
 Ta vstavek doda pogovorno okno za hitro spuščanje besedila ali
vsebine odložišča na stran zim.

To je jedrni vstavek, ki je del zim.
 Ta vstavek doda ikono sistemske vrstice za hiter dostop.

Ta vstavek zahteva Gtk+ 2.10 ali noveši.

To je jedrni vstavek, ki je del zim.
 Ta vstavek doda dodaten gradnih, ki prikazuje seznam
strani, ki se povezujejo s trenutno stranjo.

To je jedrni vstavek, ki je del zim.
 Ta vstavek doda dodaten gradnik, ki pokaže preglednico
vsebine trenutne strani.

To je osnovni vstavek, ki pride s programom zim.
 Ta vstavek doda nastavitev, ki pomaga pri uporabi
programa zim brez motenj.
 Ta vstavek doda pogovorno okno 'Vstavi simbol' in omogoča
samodejno oblikovanje tipografskih znakov.

To je jedrni vstavek, ki je del zim.
 Ta vstavek doda nadzor različic za beležke.

Ta vstavek podpira sisteme za nadzor različic Bazaar, Git in Mercurial.

To je jedrni vstavek, ki je del zim.
 Vstavek vam omogoča vstavitev arimetričnih izračunov v zim.
Osnovan je na aritmetričnem modulu iz
http://pp.com.mx/python/arithmetic.
 Ta vstavek vam omogoča hitro oceno enostavnih
matematičnih izrazov v zimu.

To je jedrni vstavek, ki je del zim.
 Ta vstavek zagotavlja urejevalnik diagramov osnovan na Ditaa.

To je jedrni vstavek, ki je del zim.
 Ta vstavek zagotavlja urejevalnik diagramov za zim osnovan na GraphViz.

To je jedrni vstavek, ki je del zim.
 Ta vstavek zagotavlja pogovorno okno z grafično
predstavitvijo strukture povezav beležke.
Uporabiti ga je mogoče kot neke vrste "miselni
vzorec", ki kaže, kako se strani povezujejo.
 Ta vstavek zagotavlja kazalo strani, ki je filtrirano z izbiro oznak v oblaku.
 Ta vstavek zagotavlja urejevalnik grafov za zim osnovan na GNU R.
 Ta vstavek zagotavlja urejevalnik grafov za na zimu osnovan Gnuplot.
 Ta vstavek zagotavlja izogibanje pomanjkanju
podpore za tiskanje v programu zim. Trenutno stran izvozi
v html in jo odpre v brskalniku. V primeru, da brskalnik
podpira tiskanje, boste lahko svoje podatke natisnili
v dveh korakih.

To je jedrni vstavek, ki je del zim.
 Ta vstavek zagotavlja urejevalnik enačb za zim osnovan na latexu.

To je jedrni vstavek, ki je del zim.
 Ta vstavek zagotavlja urejevalnik partitur za zim osnovan na GNU Lilypond.

Je je jedrni vstavek, ki je del zim.
 Ta vstavek razvrsti izbrane vrstice v abecednem vrstnem redu.
V primeru da je seznam že razvrščen, bo bil vrstni red obrnjen.
(A-Ž v Ž-A).
 To običajno pomeni, da datoteka vsebuje neveljavne znake Naslov Za nadaljevanje lahko shranite kopijo te strani ali
zavržete vse spremembe. Tudi v primeru shranjevanja kopije bodo
vse spremembe zavržene, vendar lahko kopijo pozneje obnovite. Kazalo vsebine D_anes Danes Preklopi izbirno polje 'V' Preklopi izbirno polje 'X' Zgornji pladenj Ikona sistemske vrstice Pretvori ime strani v oznake za predmete nalog Vrsta Odstrani zamik ob <Povratni tipki>
(Če je onemogočeno, lahko še vedno uporabite <Shift><Tab>) Neznano Neoznačeno Podosobi %i strani, ki se povezujejo na to stran Podosobi %i stran, ki se povezuje na to stran Podosobi %i strani, ki se povezujeta na to stran Podosobi %i strani, ki povezujejo na to stran Posodobi glavo te strani Posodabljanje povezav Posodabljanje kazala Uporabi pisavo po meri Uporabi stran za vsako Uporabite <Vnosno tipko> za slednje povezavam
(V primeru, da je onemogočeno, lahko še vedno uporabite <Alt><Vnosna tipka>) Nadzor različic Nadzor različic v tej beležki trenutno ni omogočen.
Ali ga želite omogočiti? Različice Ogled _zabeležke Pokaži _dnevnik Spletni strežnik Teden Pri poročanju tega hrošča vključite
podrobnosti iz besedilnega polja spodaj Celotna _beseda Širina Strani Wiki: %s Štetje besed Število besed ... Besede Leto Včeraj Datoteko urejate v zunanjem programu. To pogovorno okno lahko zaprete, ko končate Nastavite lahko orodja po meri, ki se bodo pojavila
v orodnem meniju v v orodni vrstici ali vsebinskih menijih. Namizni Wiki Zim O_ddalji _O Programu _Vsi pladnji _Aritmetika Na_zaj _Prebrskaj _Hrošči _Podrejeni predmet Po_čisti oblikovanje Za_pri _Zloži vse V_sebina _Kopiraj _Kopiraj sem _Izbriši _Izbriši stran _Zavrzi spremembe _Uredi _Uredi povezavo Ur_edi povezavo ali predmet ... _Uredi lastnosti _Poudari _V&O _Datoteka _Najdi ... _Naprej _Celozaslonski način _Pojdi Pomo_č _Poudari _Zgodovina _Domov _Sliko ... _Uvozi stran ... _Vstavi _Skoči na ... _Tipkovne bližnjice _Povezava _Povezava na datum Po_vezavo ... _Označi _Več _Premakni _Premakni sem _Nova stran ... _Naslednji _Brez O_bičajna velikost _Oštevilčen seznam _Odpri _Odpri drugo beležko ... _Drugo ... _Stran N_adrejeni predmet _Prilepi _Predogled _Predhodni _Natisni v brskalnik _Hitro sporočilce ... _Končaj _Nedavne strani _Uveljavi Logični izraz _Znova naloži _Odstrani povezavo _Zamenjaj _Zamenjaj ... _Ponastavi velikost _Obnovi različico _Shrani _Shrani kopijo _Zaslonski posnetek ... _Iskanje _Iskanje ... Pošlji _na ... _Stranski pladnji Stran _ob strani _Razvrsti vrstice _Prečrtaj _Močno Po_dpisano _Nadpisano _Predloge _Orodja _Razveljavi _Verbatim _Različice ... Po_gled _Približaj calendar:week_start:1 le za branje sekunde Launchpad Contributions:
  Andrej Znidarsic https://launchpad.net/~andrej.znidarsic
  Sasa Batistic https://launchpad.net/~sasa-batistic
  Vanja Cvelbar https://launchpad.net/~cvelbar
  c0dehunter https://launchpad.net/~kralj-primoz 