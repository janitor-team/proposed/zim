��    h     \              �&  ,   �&  .   �&  ?   �&     9'     E'     b'     }'  0   �'     �'     �'  	   (     (  i   (  *   �(     �(     �(     �(     �(  
   �(  -   )     /)  V   7)     �)  	   �)     �)  3   �)  Y   �)     @*  
   M*     X*     l*     x*     �*     �*     �*     �*  	   �*  $   �*  ?   �*  /   -+  (   ]+     �+  %   �+  ,   �+     �+  	   ,     ,     %,  
   -,     8,  	   ?,     I,     V,     c,     o,     v,  
   �,     �,     �,     �,     �,     �,     �,  
   �,     �,     -     -     --     @-     P-  <   _-     �-  	   �-  
   �-     �-     �-     �-     �-     �-      .     .  +   (.  3   T.      �.     �.     �.     �.     �.  
   �.     �.     �.     /     0/  3   M/     �/     �/     �/     �/     �/     �/     0     '0     /0     40     A0     O0     \0     a0     e0  0   m0     �0     �0     �0     �0  
   �0     �0     �0     �0     �0     1     1     1  $   51  ~   Z1     �1     �1  
   �1     �1     �1  
   2  	   2  
   $2     /2     <2     D2     U2     m2     {2     �2  5   �2     �2     �2     �2  !   �2     3  #   3     ;3     N3     U3     h3     �3     �3     �3     �3     �3     �3     �3     4     4  
   4     4     .4  	   ?4  6   I4     �4  v   �4     �4  *   5  c   ;5     �5     �5     �5  
   �5     �5     �5     �5     �5  
   �5  
   	6  
   6  
   6  
   *6  
   56     @6     G6     b6     �6  	   �6     �6     �6     �6     �6     �6     �6  
   �6     �6      7     7     '7     67     C7     S7     e7     t7     �7     �7     �7     �7     �7     �7     �7  	   �7     �7     8     8     !8     .8     <8     S8     X8     g8     s8     y8  2   �8     �8     �8     �8     �8     �8     9     9     49     M9     Y9     u9     �9  	   �9     �9     �9     �9     �9     �9     �9     �9     :      :  *   1:     \:     e:     n:     }:     �:     �:     �:     �:     �:  *   �:  2   ;     E;     V;     g;     �;     �;  	   �;     �;     �;     �;     �;     �;     �;     <     <  
   .<     9<  	   P<     Z<     m<     �<     �<     �<     �<     �<     �<     �<  9   �<      =  I   .=  !   x=  '   �=  	   �=     �=     �=  0   �=  
   >  	   >      >     .>     F>     [>  	   h>     r>     x>  '   �>  V   �>  3    ?  0   4?  (   e?     �?     �?  .   �?     �?     �?     �?     @     @     @      @     1@     9@  
   E@  &   P@  
   w@     �@     �@     �@     �@     �@     �@  
   �@     �@  
   	A     A     #A  ?   4A     tA     �A     �A     �A     �A     �A     �A     �A     �A     �A  	   
B     B     !B     0B     GB     MB     `B     gB     |B     �B     �B     �B     �B     �B     �B  �   �B     xC      �C     �C     �C  	   �C     �C      D     D     "D     1D     >D     VD     jD     |D  2   �D     �D  &   �D     �D     E     $E     8E     LE     ^E  5   xE  &   �E     �E     �E     �E  &   �E     F     1F     DF     PF     ^F     dF  
   vF     �F     �F  	   �F     �F     �F     �F     �F     �F     �F  	   �F     �F     �F  	   G     G  
   G     !G     4G     JG  ?   `G  A   �G  S   �G  =   6H  K   tH  @   �H  6   I  -   8I  I   fI  x   �I  �   )J  �   �J  �   �K  �   L  �   �L  }   -M  L   �M  �   �M  9   �N  �   �N  �   dO  �   �O  }   �P  C   �P  h   BQ  k   �Q  �   R  R   �R  ;   >S  =   zS  v   �S    /T  j   CU  n   �U  ]   V     {V  �   �V  7   �W     �W  �   �W  |   jX     �X     �X     �X     �X     Y      Y     4Y  	   =Y  '   GY     oY     tY  D   �Y     �Y     �Y     �Y     �Y     �Y  H   Z     QZ     qZ     �Z  !   �Z     �Z     �Z     �Z  P   �Z     D[  U   T[     �[  	   �[  
   �[     �[  N   �[     \     (\     .\  �   <\  
   �\     ]     ]     ]  	   ]  ^   %]  f   �]     �]  	   �]     ^     ^  
   ^     ^     )^     8^     >^     F^     L^  	   T^     ^^     e^     w^     ~^  	   �^     �^  
   �^     �^     �^     �^     �^     �^  
   �^     �^     _     _  	    _     *_     0_     6_     ?_     H_     T_     X_  
   ^_     i_     r_  	   x_     �_     �_     �_     �_     �_     �_     �_     �_     �_     �_  
   �_     �_     �_     `     `     #`     )`     /`     <`     K`     O`     U`  	   o`     y`     `     �`     �`     �`  	   �`     �`     �`     �`     �`     �`     �`     �`     a     a     a     $a     1a     :a     Fa     Ra     ca  
   ia     ta     �a  
   �a     �a     �a     �a     �a     �a     �a  
   �a     �a  
   �a     �a     b  	   b     b     b     %b     .b     Db     \b  
   rb     }b     �b     �b     �b     �b     �b  
   �b     �b  �  �b  /   �d  4   �d  A   e     Pe     \e     he     ue     �e     �e     �e     �e     �e  g   �e  2   cf     �f     �f     �f  "   �f     �f  '   �f      g  Q   -g     g     �g     �g  A   �g  J   �g     6h  
   ?h     Jh     _h     ph     �h     �h  	   �h     �h     �h     �h  F   �h  4   ,i  1   ai  '   �i  $   �i  /   �i     j  	   %j     /j     ?j  
   Nj     Yj     `j     gj     tj     �j     �j     �j     �j     �j     �j     �j     �j  
   �j  	   �j     	k     k     .k     Dk     Zk     pk     k  B   �k  	   �k     �k  	   �k     �k     �k      l     )l     2l     Cl     Xl  2   kl  >   �l  (   �l     m     m     m     -m     ?m     Mm  0   [m  $   �m      �m  =   �m      n     1n     Gn     en     {n  -   �n     �n     �n     �n     �n     �n     o     o     o     o  2   "o     Uo     jo     oo     }o     �o     �o  
   �o  	   �o     �o     �o     �o     �o  "   �o  �   p     �p     �p  
   �p  
   �p     �p     �p     �p     q     q     )q     1q     Dq     \q     lq  	   rq  9   |q     �q     �q     �q  #   �q     r  -   r     @r     Sr     Yr     or     �r     �r     �r     �r     �r     �r     s     's     .s     3s     Ds     Us     ds  D   ms     �s  `   �s     t  +   /t  n   [t     �t     �t     �t  
   �t  *   �t  +   u     Bu     Fu     Nu     Zu     cu     lu     uu     ~u     �u  $   �u  *   �u     �u     �u      v     v     v     v     ,v     Av     Hv     Wv     jv     }v     �v     �v     �v     �v     �v     �v      w     w     w     7w     Ow     _w     nw  	   �w     �w     �w  	   �w     �w     �w  !   �w     �w     x     x     x     %x  <   2x  	   ox     yx     �x  
   �x     �x     �x     �x  "   �x     y     #y     :y  
   Qy  	   \y     fy     my     sy     �y     �y     �y     �y     �y  ,   �y  @   z     [z     gz     tz     �z     �z     �z     �z  &   �z     �z  -   {  F   9{     �{     �{      �{     �{     �{     �{     �{     �{     |     1|     =|     O|     ^|     s|     �|     �|     �|     �|     �|     �|     �|     }     }     }  
   2}     =}  G   M}     �}  V   �}  '   �}  ,   $~     Q~     W~     e~  0   m~     �~     �~     �~     �~     �~     �~     	            )   &  Z   P  *   �  /   �      �     '�     D�  2   K�     ~�  	   ��     ��     ��  
   ��     ��          Ԁ  	   ܀     �     �     �     �     .�     @�     U�      m�     ��     ��     ��     ��     ��     Ł  ;   ݁     �     &�  &   3�     Z�     `�     q�     ��     ��     ��     ��     ʂ     ق     �     �     �     �     +�     0�     B�     R�     Y�     p�     ��     ��     ��  �   ��     K�  !   _�      ��     ��     ��          ф     �     ��     �     �     7�     P�  #   f�  D   ��     υ  6   �     �     :�     Y�     w�     ��  $   ��  =   ˆ  ,   	�     6�     D�     K�     _�     �     ��     ��     Ň     Ӈ     ه  
   �     ��     �  	   �     �      �  
   1�     <�  (   @�     i�     o�     |�     ��     ��     ��     ��     ��     ��     ψ  +   �  -   �  H   @�  G   ��  A   щ  ,   �  (   @�  (   i�  P   ��  �   �  �   q�  �   B�  �   '�  �   ��     Q�  ~   ю  a   P�  �   ��  @   4�  �   u�  �   �  �   ��     Q�  E   ђ  h   �  k   ��  �   �  `   ֔  H   7�  J   ��  �   ˕    _�  j   �  m   �  f   X�  �   ��  �   B�  <   �     ?�  �   E�  �   �  
   ��  	   ��          ˛     �     ��  
   �  	   �  1   "�     T�     Y�  d   q�     ֜     �     ��     �  	   !�  2   +�     ^�     |�     ��  (   ��     ˝     �  #   �  e   %�     ��  X   ��     �  
   ��  
   �     �  F   �     \�     n�     t�  �   ��     K�     W�     f�     k�     q�  k   y�  c   �     I�  	   Y�     c�     l�     t�     ��     ��     ��     ��     ��     ��  
   ��     ¡     ȡ     ڡ     �     �     ��     �     �     �     )�     <�     L�     U�     e�     ��     ��  
   ��     ��     ��     ��     â     ɢ     ֢     ܢ     �     �     ��  
   ��     �  	   �     !�     )�     5�     =�  
   P�     [�     c�     j�     r�     ��     ��     ��     ��     ʣ     ף     �     �      �     �     
�     �     $�     -�     >�     E�  	   M�     W�     c�     j�     }�     ��     ��  
   ��     ��     Ť     Ѥ     ؤ     �     �  	   ��     �     �     )�     1�     A�     T�     Z�     c�     p�     |�     ��     ��     ��  	   ��     ��     ��     ƥ  	   ̥  	   ֥  	   �  
   �  	   ��  !   ��     !�     =�     S�     b�     s�  	   ��     ��  �   ��     N�     ]�     j�   		This plugin provides bar for bookmarks.
		 %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i Attachment %i Attachments %i _Backlink %i _Backlinks %i errors occurred, see log %i file will be deleted %i files will be deleted %i open item %i open items %i warnings occurred, see log <Unknown> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Add Application Add Bookmark Add Notebook Add bookmark/Show settings Add column Add new bookmarks to the beginning of the bar Add row Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files Allow public access Always use last cursor position when opening a page An error occurred while generating the image.
Do you want to save the source text anyway? Applications Arithmetic Ascii graph (Ditaa) Attach File Attach image first Attachment Browser Attachments Attachments: Author Auto
Wrap Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave interval in minutes Autosave version on regular intervals Autosave version when the notebook is closed Back to Original Name BackLinks BackLinks Pane Backend Backlinks: Bazaar Bookmarks BookmarksBar Border width Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Capture whole screen Center Change columns Changes Characters Characters excluding spaces Check Checkbox '>' Check Checkbox 'V' Check Checkbox 'X' Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Clone row Code Block Column 1 Command Command does not modify data Comment Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Created Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Demote Dependencies Description Details Diagram Discard note? Distraction Free Editing Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root Due E_xport... Edit %s Edit Custom Tool Edit Image Edit Link Edit Table Edit _Source Editing Editing file: %s Enable Version Control? Enable plugin Enabled Equation Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Fossil GNU R Plot Get more plugins online Get more templates online Git Gnuplot Grid lines Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide Journal pane if empty Hide menubar in fullscreen mode Highlight current line Home Page Horizontal _Line Icon Images Import Page Include subpages Index Index page Inline Calculator Insert Code Block Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Sequence Diagram Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Last Modified Leave link to new page Left Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Managing table columns Map document root to URL Match _case Maximum number of bookmarks Maximum page width Menubar Mercurial Modified Month Move Selected Text... Move Text to Other Page Move column ahead Move column backward Move text to Name Need output file to export MHTML Need output folder to export full notebook New File New Page New Page in %s New S_ub Page... New _Attachment Next No Applications Found No changes since last version No dependencies No document root defined for this notebook No plugin available to display objects of type: %s No such file: %s No such page: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Only Show Active Tasks Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open cell content link Open help Open in New Window Open in New _Window Open new page Open plugins folder Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Index Page Name Page Template Page already exists: %s Page not allowed: %s Page section Paragraph Paste Path Bar Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please select a row, before you push the button. Please select more than one line of text Please specify a notebook Plugin Plugin "%s" is required to display this object Plugins Port Position in the window Pr_eferences Preferences Prev Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Remove column Remove row Removing Links Rename page "%s" Repeated clicking a checkbox cycles through the checkbox states Replace _All Replace with Restore page to saved version? Right Right Side Pane Right margin position Row down Row up S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Screenshot Command Search Search _Backlinks... Search this section Section Section(s) to ignore Section(s) to index Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Sequence Diagram Server not started Server started Server stopped Set New Name Set default text editor Set to Current Page Show Line Numbers Show Tasks as Flat List Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show full Page Name Show full page name Show helper toolbar Show in the toolbar Show right margin Show tasklist in sidepane Show the cursor also for pages that can not be edited Show the page title heading in the ToC Single _page Size Smart Home key Some error occurred while running "%s" Sort alphabetically Sort pages by tags Source View Spell Checker Start Start _Web Server Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Tags for non-actionable tasks Task Task List Tasks Template Templates Text Text Files Text From _File... Text background color Text foreground color The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The inline calculator plugin was not able
to evaluate the expression at the cursor. The table must consist of at least on row!
 No deletion done. There are no changes in this notebook since the last version that was saved This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This page name cannot be used due to technical limitations of the storage This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a "path bar" to the top of the window.
This "path bar" can show the notebook path for the current page,
recent visited pages or recent edited pages.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds the page index pane to the main window.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows inserting 'Code Blocks' in the page. These will be
shown as embedded widgets with syntax highlighting, line numbers etc.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin also has properties,
see the notebook properties dialog This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a sequence diagram editor for zim based on seqdiag.
It allows easy editing of sequence diagrams.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This plugin turns one section of the notebook into a journal
with a page per day, week or month.
Also adds a calendar widget to access these pages.
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox '>' Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Tray Icon Turn page name into tags for task items Type Un-check Checkbox Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unkown Image type Unkown Object Unspecified Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use a page for each Use date from journal pages Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s With this plugin you can embed a 'Table' into the wiki page. Tables will be shown as GTK TreeView widgets.
Exporting them to various formats (i.e. HTML/LaTeX) completes the feature set.
 Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Zim Desktop Wiki Zoom _Out _About _Add _All Panes _Arithmetic _Attachment... _Back _Browse _Bugs _Cancel _Checkbox _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Duplicate Line _Edit _Edit Link _Edit Link or Object... _Edit Properties _Edit... _Emphasis _File _Find _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump _Jump To... _Link _Link to date _Link... _Mark _More _Move _Move Here _Move Line Down _Move Line Up _New Page Here... _New Page... _Next _None _Normal Size _Numbered List _OK _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Print _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove _Remove Line _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In as due date for tasks as start date for tasks calendar:week_start:0 do not use horizontal lines no grid lines readonly seconds translator-credits vertical lines with lines {count} of {total} Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2021-02-15 22:50+0000
Last-Translator: Reza Almanda <rezaalmanda27@gmail.com>
Language-Team: Indonesian <https://hosted.weblate.org/projects/zim/master/id/>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Weblate 4.5-dev
 		Plugin ini menyediakan bilah untuk markah.
		 %(cmd)s
mengembalikan status keluar tak-nol %(code)i Terjadi %(n_error)i galat dan %(n_warning)i peringatan, lihat log %A %d %B %Y %i Lampiran %i _Backlink Terjadi %i galat, lihat log Berkas %i akan dihapus %i item terbuka Terjai %i peringatan, lihat log <Tak diketahui> Sebuah wiki destop Berkas dengan nama <b>"%s"</b> sudah ada.
Anda bisa menggunakan nama lain atau menimpa berkas yang ada. Sebuah tabel harus memiliki setidaknya satu kolom. Tambah Aplikasi Tambah Markah Tambah Buku Catatan Tambah markah/Tampilkan pengaturan Tambah kolom Tambah markah baru pada permulaan bilah Tambah baris Tambah dukungan periksa ejaan menggunakan gtkspell.

Ini adalah plugin inti zim.
 Ratakan Semua Berkas Izinkan akses publik Selalu gunakan posisi kursor terakhir saat membuka sebuah halaman Galat terjadi saat membuat gambar.
Anda tetap ingin menyimpan teks sumber? Aplikasi Aritmatika Grafik Ascii (Ditaa) Lampirkan Berkas Lampirkan gambar dahulu Peramban Lampiran Lampiran Lampiran: Penulis Bungkus
Otomatis Versi simpan otomatis dari zim Pilih kata saat ini secara otomatis ketika anda menerapkan pemformatan Ubah kata "CamelCase" menjadi tautan secara otomatis Ubah alamat berkas menjadi tautan secara otomatis Jarak waktu simpan otomatis dalam menit Versi simpan otomatis secara berkala Versi simpan otomatis saat buku catatan ditutup Kembali ke Nama Asli Tautbalik Panel Tautbalik Ujung Belakang Tautbalik: Bazaar Markah Bilah Markah Lebar batas Panel Bawah Ramban Daftar Bule_t K_onfigurasi Tak dapat mengubah halaman: %s Batal Tangkap keseluruhan layar Tengah Ubah kolom Perubahan Karakter Karakter tak termasuk spasi Centang Kotak Cek '>' Centang Kotak Cek 'V' Centang Kotak Cek 'X' Perik_sa ejaan Daftar _Kotak Cek Ikon baki klasi,
tidak menggunakan ikon status gaya baru di Ubuntu Bersihkan Gandakan baris Blok Kode Kolom 1 Perintah Perintah tidak memodifikasi data Komentar Catatan _lengkap Konfigurasi Aplikasi Konfigurasi Plugin Tentukan sebuah aplikasi untuk membuka tautan "%s" Tentukan sebuah aplikasi untuk membuka berkas
dengan tipe "%s" Perlakukan semua kotak cek sebagai tugas Salin Salin Alamat Surel Salin Templat S_alin Sebagai... Sa_lin Tautan Salin _Lokasi Tak dapat menemukan berkas dapat dieksekusi "%s" Tak dapat menemukan buku catatan: %s Tak dapat menemukan templat "%s" Tak dapat menemukan berkas atau folder untuk buku catatan ini Tak dapat memuat pemeriksa ejaan Tak dapat membuka: %s Tak dapat mengurai pernyataan Tak dapat membaca: %s Tak dapat menyimpan halaman: %s Buat sebuah halaman baru untuk setiap catatan Buat folder? Dibuat Po_tong Alat Tersuai Peralatan _Tersuai Atur... Tanggal Hari Standar Format standar untuk menyalin teks dari papan klip Buku catatan standar Jeda Hapus Halaman Hapus halaman "%s"? Hapus baris Turunkan Dependensi Deskripsi Detil Diagram Buang catatan? Penyuntingan Bebas Gangguan Anda ingin menghapus semua markah? Anda ingin mengembalikan halaman: %(page)s
ke versi tersimpan: %(version)s ?

Semua perubahan sejak versi tersimpan terakhir akan hilang ! Alamat Dokumen Jatuh tempo E_kspor... Sunting %s Sunting Alat Tersuai Sunting Gambar Sunting Tautan Sunting Tabel Sunting Kode _Sumber Sunting Sunting berkas: %s Aktifkan Kontrol Versi? Aktifkan plugin Aktif Persamaan Galat pada %(file)s di baris %(line)i dekat "%(snippet)s" Hitung _Matematika Perluas Semu_a Ekspor Ekspor semua halaman ke satu berkas Ekspor selesai Ekspor tiap halaman ke sebuah berkas terpisah Mengekspor catatan Gagal Gagal menjalankan: %s Gagal menjalankan aplikasi: %s Berkas Sudah Ada Berkas _Templat... Berkas berubah di diska: %s Berkas sudah ada Berkas tak dapat ditulis: %s Tipe berkas tidak didukung: %s Nama Berkas Saring Cari Cari Beri_kutnya Cari Sebel_umnya Cari dan Ganti Cari apa Tandai tugas dengan jatuh tempo Senin atau Selasa sebelum minggu ini Folder Folder sudah ada dan memiliki isi, ekspor ke folder ini akan menimpa berkas yang ada. Lanjutkan? Folder sudah ada: %s Folder dengan templat untuk berkas lampiran Untuk pencarian tingkat lanjut, anda bisa menggunakan operator
seperti AND, OR dan NOT. Lihat halaman bantuan. For_mat Format Fossil GNU R Plot Dapatkan lebih banyak plugin secara daring Dapatkan lebih banyak templat secara daring Git Gnuplot Garis petak Tajuk _1 Tajuk _2 Tajuk _3 Tajuk _4 Tajuk _5 Tinggi Sembunyikan panel Jurnal jika kosong Sembunyikan bilah menu di mode layar penuh Sorot baris saat ini Halaman Awal Garis Horisonta_l Ikon Gambar Impor Halaman Termasuk sub-halaman Indeks Halaman indeks Kalkulator Sebaris Masukkan Blok Kode Masukkan Tanggal dan Waktu Masukkan Diagram Masukkan Ditaa Masukkan Persamaan Masukkan GNU R Plot Masukkan Gnuplot Masukkan Gambar Masukkan Tautan Masukkan Nilai Masukkan cuplikan layar Masukkan Diagram Urutan Masukkan Simbol Masukkan Tabel Masukkan Teks dari Berkas Antarmuka Kata Kunci Interwiki Jurnal Lompat ke Lompat ke Halaman Terakhir Diubah Tinggalkan tautan ke halaman baru Kiri Panel Sisi Kiri Urutkan Baris Baris Tautkan Peta Tautkan berkas dibawah lokasi asal dokumen dengan path penuh Tautan ke Lokasi Log even dengan Zeitgeist Berkas log Sepertinya anda menemukan kutu Jadikan aplikasi standar Kelola kolom tabel Petakan lokasi asal dokumen ke URL _Cocokkan huruf Jumlah markah maksimum Lebar halaman maksimum Bilah menu Mercurial Diubah Bulan Pindahkan Teks Terpilih... Pindah Teks ke Halaman Lain Pindah kolom ke depan Pindah kolom ke belakang Pindah teks ke Nama Perlu berkas keluaran untuk mengekspor MHTML Perlu folder keluaran untuk mengekspor buku catatan secara penuh Berkas Baru Halaman Baru Halaman Baru di %s S_ubhalaman baru.. L_ampiran Baru Berikut Tak Ada Aplikasi yang Ditemukan Tak ada perubahan sejak versi terakhir Tak ada dependensi Tak ada alamat dokumen untuk buku catatan ini Tidak ada plugin yang tersedia untuk menampilkan obyek dengan tipe: %s Tak ada berkas: %s Tak ada halaman: %s Tak ada wiki yang ditentukan: %s Tak ada templat terinstal Buku Catatan Buku Catatan OK Hanya Tampilkan Tugas Aktif Buka _Folder Lampiran Buka Folder Buka Buku Catatan Buka Dengan... Buka Alamat _Dokumen Buka Folder Buku Buka _Halaman Buka tautan isi sel Buka bantuan Buka di Jendela Baru Buka di Jendela _Baru Buka halaman baru Buka folder plugin Buka dengan "%s" Opsional Opsi untuk plugin %s Lainnya... Berkas keluaran Berkas keluaran sudah ada, tambahkan "--overwrite" untuk memaksa ekspor Folder keluaran Folder keluaran sudah ada dan tak kosong, tambahkan "--overwrite" untuk memaksa ekspor Lokasi keluaran diperlukan untuk ekspor Keluaran harus menggantikan pilihan saat ini Timpa Bilah _Alamat Halaman Halaman "%s" tak mempunyai folder untuk lampiran Indeks Halaman Nama Halaman Templat Halaman Halaman sudah ada: %s Halaman tak diizinkan: %s Bagian halaman Paragraf Tempel Bilah Alamat SIlakan masukkan komentar untuk versi ini Catat bahwa tautan ke halaman yang tak ada
juga akan membuat halaman baru secara otomatis. Pilih sebuah nama dan folder buku catatan. Pilih sebuah baris sebelum anda menekan tombol. Pilih lebih dari satu baris teks Tentukan sebuah buku catatan Plugin Plugin "%s" dibutuhkan untuk menampilkan obyek ini Plugin Pangkalan Posisi pada jendela Pr_eferensi Pengaturan Sebelum Cetak ke Peramban Naikkan Proper_ti Properti Dorong even ke jurik Zeitgeist. Catatan Cepat Catatan Cepat... Perubahan Terkini Perubahan Terkini... Ha_laman terkini diubah Format ulang markah secara cepat Buang Buang Semua Hapus kolom Hapus baris Hapus Tautan Ganti nama halaman "%s" Mengulang klik pada kotak cek akan mendaur status kotak cek Ganti Semu_a Ganti dengan Kembalikan halaman ke versi tersimpan? Kanan Panel Sisi Kanan Posisi marjin kanan Turunkan baris Naikkan baris Simp_an Versi... Simpan _Duplikatnya... Simpan Salinan Simpan Versi Simpan markah Versi tersimpan dari zim Nilai Perintah Cuplikan Layar Cari Cari Taut_balik.. Cari bagian ini Bagian Bagian untuk diabaikan Bagian untuk diindeks Pilih Berkas Pilih Folder Pilih Gambar Pilih sebuah versi untuk melihat perubahan antara versi itu dengan keadaan
saat ini. Atau pilih beberapa versi untuk melihat perubahan antar versi tersebut.
 Pilih format ekspor Pilih berkas atau folder keluaran Pilih halaman yang akan diekspor Pilih jendela atau area Pilihan Diagram Urutan Server tak dijalankan Server dijalankan Server dihentikan Atur Nama Baru Atur penyunting teks standar Atur ke Halaman Saat Ini Tampilkan Nomor Baris Tampilkan Tugas sebagai Daftar Rata Tampilkan Daftar Isi sebagai widget melayang ketimbang di panel sisi Tampilkan _Perubahan Tampilkan sebuah ikon terpisah untuk tiap buku catatan Tampilkan Nama Halaman lengkap Tampilkan nama halaman lengkap Tampilkan bilah alat pembantu Tampilkan di bilah alat Tampilkan marjin kanan Tampilkan daftar tugas di panel sisi Juga tampilkan kursor pada halaman yang tidak dapat disunting Tampilkan kepala judul halaman di Daftar Isi Satu _halaman Ukuran Tombol Rumah cerdas Ada galat saat menjalankan "%s" Urutkan berdasar alfabet Urutkan halaman berdasar tag Tampilan Sumber Periksa Ejaan Mulai Jalankan Server _Web Si_mbol... Sintaks Standar Sistem Lebar tab Tabel Penyunting Tabel Daftar Isi Tag Tag untuk tugas yang tak dapat dilakukan Tugas Daftar Tugas Tugas Pola Templat Teks Berkas Teks Teks Dari _Berkas... Warna latar belakang teks Warna latar depan teks Folder
%s
tidak ada.
Anda ingin membuatnya? Folder "%s" tidak ada.
Anda ingin membuatnya? Plugin kalkulator sebaris tidak dapat
menghitung pernyataan pada kursor. Tabel harus berisi setidaknya satu baris!
 Tidak dilakukan penghapusan. Tidak ada perubahan di buku catatan sejak versi terakhir disimpan Anda mungkin tidak memiliki
kamus yang tepat Berkas sudah ada.
Anda ingin menimpanya? Halaman ini tak memiliki folder lampiran Nama halaman ini tak bisa digunakan karena batasan teknis dari ruang penyimpanan Plugin ini mengizinkan mengambil sebuah cuplikan layar dan memasukkannya
secara langsung ke sebuah halaman zim.

Ini adalah plugin inti zim.
 Plugin ini menambahkan sebuah "bilah alamat" di bagian atas jendela.
"Bilah alamat" ini bisa menampilkan alamat buku catatan untuk halaman saat ini,
halaman terkini dikunjungi atau halaman terkini disunting.
 Plugin ini menambahkan sebuah dialog yang menampilkan semua tugas
terbuka di buku catatan. Tugas terbuka bisa berupa kotak cek terbuka
atau item yang ditandai dengan tag seperti "TODO" atau "FIXME".

Ini adalah plugin inti zim.
 Plugin ini menambahkan sebuah dialog untuk menambahkan teks atau 
isi papan klip
 secara cepat ke sebuah halaman zim.

Ini adalah plugin inti zim.
 Plugin ini menambahkan sebuah ikon baki untuk akses cepat.

Plugin ini bergantung pada Gtk+ versi 2.10 atau lebih baru.

Ini adalah plugin inti zim.
 Plugin ini menambahkan widget yang menampilkan daftar halaman
yang tertaut pada halaman saat ini.

Ini adalah plugin inti zim.
 Plugin ini menambahkan widget tambahan yang menampilkan sebuah daftar isi dari halaman saat ini.

Ini adalah plugin inti zim.
 Plugin ini menambahkan pengaturan untuk menjadikan zim sebagai sebuah penyunting bebas gangguan.
 Plugin ini menambahkan dialog 'Masukkan Simbol' dan mengizinkan
format otomatis karakter tipografi.

Ini adalah plugin inti zim.
 Plugin ini menambahkan panel indeks halaman pada jendela utama.
 Plugin ini menambahkan kontrol versi untuk buku catatan.

Plugin ini mendukung sistem kontrol versi Bazaar, Git dan Mercurial.

Ini adalah plugin inti zim.
 Plugin ini mengizinkan untuk memasukkan 'Blok Kode' di sebuah halaman, yang akan
ditampilkan sebagai widget tertanam dengan penyorotan sintaks, nomor baris, dan sebagainya.
 Plugin ini memungkinkan anda menyisipkan perhitungan aritmatika di zim.
Berbasis pada modul aritmatika dari
http://pp.com.mx/python/arithmetic.
 Plugin ini mengizinkan anda menghitung suatu pernyataan matematika sederhana secara cepat di zim.

Ini adalah plugin inti zim.
 Plugin ini juga memiliki properti,
lihat dialog properti buku catatan Plugin ini menyediakan sebuah penyunting diagram berbasis Ditaa untuk zim.

Ini adalah plugin inti zim.
 Plugin ini menyediakan sebuah penyunting diagram berbasis GraphViz untuk zim.

Ini adalah plugin inti zim.
 Plugin ini menyediakan sebuah dialog dengan gambaran
grafis dari struktur pentautan buku catatan.
Ini dapat digunakan sebagai semacam "mind map"
yang menunjukkan bagaimana halaman-halaman saling terkait.

Ini adalah plugin inti zim.
 Plugin ini menyediakan sebuah indeks halaman yang disaring dengan pemilihan tag di sebuah awan.
 Plugin ini menyediakan sebuah penyunting plot berbasis GNU R untuk zim.
 Plugin ini menyediakan sebuah penyunting plot berbasis Gnuplot untuk zim.
 Plugin ini menyedian sebuah penyunting diagram urutan berbasis seqdiag untuk zim.
Plugin ini mengizinkan penyuntingan diagram urutan dengan mudah.
 Plugin ini menyediakan sebuah solusi untuk kurangnya dukungan
pencetak di zim. Plugin ini mengekspor halaman saat ini ke html 
dan membuka sebuah peramban. Dukungan pencetak pada peramban 
digunakan untuk mengirimkan data anda ke pencetak dalam dua langkah.

Ini adalah plugin inti zim.
 Plugin ini menyediakan sebuah penyunting persamaan berbasis latex untuk zim.

Ini adalah plugin inti zim.
 Plugin ini menyediakan sebuah penyunting nilai berbasis GNU Lilypond untuk zim.

Ini adalah plugin inti zim.
 Plugin ini menampilkan folder lampiran untuk halaman saat ini
sebagai tampilan ikon pada panel bawah.
 Plugin ini mengurutkan baris terpilih berdasarkan alfabet.
Jika daftar telah diurutkan, maka urutannya akan dibalik
(A-Z ke Z-A).
 Plugin ini mengubah satu bagian buku catatan menjadi sebuah jurnal
dengan satu halaman per hari, minggu, atau bulan.
Juga menambahkan sebuah widget kalender untuk mengakses halaman tersebut.
 Ini biasanya berarti berkas berisi karakter yang tidak valid Judul Untuk melanjutkan, anda bisa menyimpan sebuah salinan dari halaman ini
atau membuang semua perubahan. Jika anda menyimpan sebuah salinan, 
perubahan juga akan dibuang, tapi anda bisa mengembalikan salinannya kemudian. Untuk membuat buku catatan baru, anda perlu memilih folder kosong.
Tentu saja anda juga bisa memilih folder buku catatan zim yang sudah ada.
 Daftar Isi Hari _Ini Hari Ini Jungkit Kotak Cek '>' Jungkit Kotak Cek 'V' Jungkit Kotak Cek 'X' Panel Atas Ikon Baki Jadikan nama halaman sebagai tag untuk item tugas Tipe Hapus Centang Kotak Cek Turunkan indentasi dengan <Backspace>
(Jika dinonaktifkan, anda masih bisa menggunakan <Shift><Tab>) Tak diketahui Tipe Gambar Tak Diketahui Obyek Tak Diketahui Tak ditentukan Tanpa tag Mutakhirkan %i halaman yang tertaut ke halaman ini Mutakhirkan tajuk halaman ini Mutakhirkan Tautan Mutakhirkan indeks Gunakan %s untuk berpindah ke panel sisi Gunakan fonta tersuai Gunakan satu halaman untuk tiap Gunakan tanggal dari halaman jurnal Gunakan <Enter> untuk mengikuti tautan
(Jika dinonaktifkan, anda masih bisa menggunakan <Alt><Enter>) Kontrol Versi Kontrol Versi saat ini tidak diaktifkan untuk buku catatan ini.
Anda ingin mengaktifkan? Versi Lihat _Log Server Web Minggu Ketika melaporkan kutu ini, sertakan
informasi dari kotak teks berikut Keseluruhan _kata Lebar Halaman wiki: %s Dengan plugin ini, anda bisa menyisipkan sebuah 'Tabel' ke dalam halaman wiki. Tabel akan ditampilkan sebagai widget GTK TreeView.
Dilengkapi fitur ekspor ke berbagai format (misalnya HTML/LaTeX).
 Jumlah Kata Jumlah Kata... Kata Tahun Kemarin Anda sedang menyunting sebuah berkas di aplikasi eksternal. Anda bisa menutup dialog ini jika sudah selesai Anda dapat mengatur alat tersuai yang akan muncul
pada menu alat dan pada bilah alat atau menu isi. Wiki Destop Zim Per_kecil Tent_ang T_ambah Semu_a Panel _Aritmatika _Lampiran... _Mundur Ram_ban _Kutu _Batal Kotak _Cek _Anak _Bersihkan Format _Tutup Persempit _Semua Daftar _Isi Sali_n _Salin Ke Sini _Hapus Hapus Halaman _Abaikan Perubahan Gan_dakan Baris _Sunting _Sunting Tautan Sunting Tautan atau Oby_ek... _Sunting Properti _Sunting... Pen_ekanan _Berkas _Cari _Cari... _Maju _Layar penuh Bu_ka _Bantuan _Sorot _Riwayat _Awal _Gambar... _Impor Halaman.. _Sisipkan _Lompat Lompat ke.. _Tautan Tautan ke tangga_l _Tautan... _Tandai _Lebih _Pindah _Pindah Ke Sini _Turunkan Baris Naikkan _Baris Halama_n Baru di Sini... Halama_n Baru.. _Berikut_nya _Tak ada Ukuran _Normal Daftar Ber_nomor _OK _Buka Buka Buku lain.. _Lain... _Halaman Hirarki _Halaman _Induk Tem_pel _Pratilik _Sebelumnya _Cetak Cetak ke _Peramban _Catatan Cepat... _Keluar Halaman Te_rkini _Jadi Lagi Ekspresi _Reguler _Muat Ulang _Buang Hapus Ba_ris _Hapus Tautan Gan_ti _Ganti... Atu_r Ulang Ukuran Kembalikan Ve_rsi _Simpan _Simpan Salinan _Cuplikan Layar... _Cari _Cari... _Kirim Ke... Panel _Sisi Ber_sebelahan Urutkan bari_s _Coret _Tebal _Subskrip _Superskrip _Templat Ala_t _Tak Jadi _Verbatim _Versi... _Tampilkan Per_besar sebagai tanggal jatuh tempo tugas sebagai tanggal mulai tugas calendar:week_start:1 jangan gunakan garis horisontal tanpa garis petak baca-saja detik Launchpad Contributions:
  Hertatijanto Hartono https://launchpad.net/~dvertx
  Triyan W. Nugroho https://launchpad.net/~triyan-wn
  tunggul arif siswoyo https://launchpad.net/~tunggul garis vertikal dengan baris {count} dari {total} 