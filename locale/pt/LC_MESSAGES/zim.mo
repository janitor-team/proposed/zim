��    �     �              �-  ,   �-  .   
.  ?   9.     y.     �.     �.     �.     �.  0   �.  0   #/     T/     o/  5   �/     �/  	   �/     �/  %   �/  i   0  *   r0     �0     �0     �0     �0     �0  
   �0  -   �0     "1  V   *1     �1  	   �1  	   �1     �1  3   �1     �1     �1  Y   2     u2     �2  .   �2  
   �2     �2     �2     �2     3     3     $3     13  	   83     B3  -   Q3  *   3  $   �3  ?   �3  /   4  (   ?4     h4  %   �4  ,   �4     �4  	   �4     �4     5  
   5     5  	   !5     +5     85     E5     L5     X5     _5  
   l5     w5     �5  �   �5  �   6     �6     �6     �6     �6  
   �6     �6     7     7     17     D7     W7     g7     ~7  .   �7  <   �7     �7  	   �7  
   	8     8     8     %8     58     R8     Z8     p8     �8     �8     �8  +   �8  3   �8      !9     B9     G9  	   Z9     d9     r9  
   ~9     �9     �9     �9     �9     �9     :  3   #:     W:     u:     �:     �:     �:     �:     �:     �:     ;     
;     ;     %;     2;     7;     H;     L;  0   T;     �;     �;     �;     �;  
   �;     �;     �;     �;     �;     �;     �;     <     <  )   1<  $   [<  ~   �<     �<     =  
   =     =     $=  
   5=  	   @=  
   J=     U=     b=     j=     {=     �=     �=     �=  5   �=     �=     �=     >  !   
>     ,>  #   =>     a>     t>     {>     �>     �>     �>     �>     �>  #   �>     ?     -?  .   L?     {?     �?     �?     �?     �?     �?  
   �?     �?     �?  	   @  6   @     F@  v   M@     �@  *   �@  c   A     eA     mA     tA  
   {A     �A     �A     �A     �A     �A  
   �A     �A  
   �A  
   �A  
   �A  
   B  
   B  
   B     )B     0B     KB     kB  	   �B     �B     �B     �B     �B     �B     �B  %   �B     �B     �B  #   	C     -C     >C  
   DC     OC     aC     sC     �C     �C     �C     �C     �C     �C     �C     �C     �C     D     %D     3D     @D  	   VD     `D     rD     zD     �D     �D     �D  �   �D     uE     �E     �E     �E     �E     �E     �E     �E     �E     �E     �E  2   �E     -F     5F     >F     XF     aF     |F     �F     �F     �F     �F     �F      G  	   G     G     G     !G     7G     OG     aG     vG     �G     �G      �G  *   �G     �G     �G     H     H     H     0H     @H     EH     [H     yH  *   �H  2   �H     �H     �H     	I     "I     9I     JI  	   SI     ]I     `I     wI     �I     �I     �I     �I     �I  
   �I     �I  	   J     J      J     4J     BJ     VJ     eJ     nJ     �J     �J  9   �J     �J  I   �J  !   +K  '   MK  	   uK     K     �K  w   �K  �   L  0   �L  
   �L  	   �L     �L     �L  &   �L      M     5M     LM  	   YM     cM     lM     rM     �M  '   �M  V   �M  3   N  0   AN  (   rN     �N     �N  .   �N     �N     �N     �N     O  !   O  !   >O     `O     lO     qO     �O     �O  
   �O  &   �O  
   �O     �O     �O     �O     P     P     :P  
   AP     LP     ZP  
   mP     xP     �P     �P     �P  ?   �P     �P     Q     Q     )Q     HQ     LQ     RQ     bQ     xQ     �Q     �Q     �Q  	   �Q     �Q     �Q     �Q     �Q     �Q     �Q     R     R     /R     7R     LR     `R     lR     zR  �   �R     S      0S     QS     lS  	   �S     �S     �S     �S     �S     �S     �S     �S     T     T     .T     KT     ]T  2   uT     �T  &   �T     �T  $   �T     U     0U     EU     YU     mU      �U     �U  !   �U     �U  5   �U  &   &V     MV     ZV     _V     eV  &   tV     �V     �V     �V     �V     �V     �V  
   �V     �V     W  
   'W     2W     9W  	   HW     RW     XW     eW     wW     |W     �W  	   �W     �W     �W  	   �W     �W  
   �W     �W     �W     �W     X  F    X  ?   gX  A   �X  �  �X  S   �Z  =   [     ?[  K   E[  )   �[  @   �[  6   �[  -   3\  F   a\  I   �\  x   �\  �   k]  �   ^  �   �^  �   `_  �   �_  }   o`  L   �`  �   :a  9   �a  �   �a  �   �b  �   3c  }   �c  C   @d  s   �d  �   �d  h   �e  k   7f  �   �f  -   wg  R   �g  ;   �g  =   4h  v   rh    �h  j   �i  n   hj  ]   �j     5k  �   �k  7   Jl     �l  �   �l  |   $m     �m     �m     �m     �m     �m     �m     �m     n     n     #n     ,n     9n     Gn     Kn  
   Tn  9   _n  	   �n  (   �n  '   �n     �n     �n  D   o     Po     Xo     jo     xo     �o  H   �o     �o     �o     p  !   p     6p     Hp     \p     xp  +   �p  P   �p     q     "q     +q  U   ;q     �q     �q  	   �q     �q  
   �q     �q  N   �q     "r     .r     4r  �   Br  
   �r     s     s     s  	   !s  ^   +s  f   �s  �   �s     �t  	   �t     �t     �t  
   �t     �t     �t     �t     �t     �t     �t  	   �t     u     u      u     'u  	   5u     ?u  
   Eu     Pu     Xu  
   eu     pu     �u     �u  
   �u     �u     �u     �u  	   �u     �u     �u     �u     �u     �u     v     v     v  
   v     "v     +v  	   1v     ;v     Kv     Sv     Yv     ev  /   rv     �v     �v     �v     �v     �v     �v  
   �v     �v     �v     �v     w     w     w     .w     4w     Aw     Pw     Tw     Zw  	   tw     ~w     �w     �w     �w     �w  	   �w     �w     �w     �w     �w     �w     �w     x     x     x     +x     3x     ;x     Kx     Xx     ex     rx  
   �x     �x     �x     �x     �x     �x  
   �x     �x     �x  
   �x     �x     y     y      y     ,y     4y  
   <y     Gy  
   Ty     _y     fy  	   ly     vy     �y     �y     �y     �y     �y  
   �y     �y     �y      z     z     z     z     2z  
   Az     Lz  �  _z  9   8|  6   r|  E   �|     �|     }  -   }  -   C}  /   q}  :   �}  L   �}     )~  0   I~  E   z~     �~     �~     �~  %   �~  f     (   z     �     �     �     �  ,   �     �  .   *�     Y�  w   i�     �     �     ��     �  @   &�  #   g�  -   ��  ^   ��     �     3�  0   @�     q�     }�     ��     ��     ��     Ђ     ׂ     ߂     �     �  ?   �  9   \�  )   ��  F   ��  ?   �  <   G�  .   ��  9   ��  C   �     1�     K�     _�     }�     ��     ��  
   ��     ��     ��     Յ     ۅ     �     �     
�  *   �     A�  �   J�  �   ކ     ��     ć     ˇ     ڇ  
   �      �  !   �  !   5�  !   W�  !   y�     ��     ��     ш  >   �  Q   -�     �     ��     ��     ��     ��     ��     ȉ     �     ��     �     �     8�     P�  5   g�  <   ��  4   ڊ     �     �     0�     =�     K�     [�     m�  !   ��  /   ��  '   ׋  1   ��  *   1�  I   \�  6   ��     ݌  (   ��     "�  (   5�  %   ^�     ��     ��  	   ��     ��     ��     ٍ     �     �     ��     �  C   �     Q�     l�     s�     ��     ��     ��     ��          Ύ     ׎     ��     ��     	�  <   (�  (   e�  �   ��     #�  
   5�     @�  	   M�  !   W�     y�     ��     ��     ��     ��     ��     Ր     �     ��  	   �  9   �     K�     `�     o�  2   x�     ��  /   Ñ     �     �     �  &   3�     Z�     p�  #   ��     ��  %   ��     �  -   �  >   2�  )   q�  '   ��  #   Ó     �     ��     ��     �     �     0�     F�  T   W�     ��  v   ��     )�  )   @�  n   j�  	   ٕ     �     �  
   �  !   ��     �     <�     @�     H�     O�     X�     r�     ��     ��     ��     ��     ��     Ȗ  -   ϖ  $   ��     "�     9�     I�     [�     b�     j�     z�     ��  (   ��     ��     ɗ  %   ڗ      �     �     �     0�     F�     _�     s�     ��     ��     ��     ��     ʘ     ٘     �     ��     �     4�     E�  %   T�  	   z�     ��     ��     ��     ��     ��     ̙  �   ݙ     ��     ��     ƚ     ښ  $   �     �     �     7�     K�  	   R�     \�  J   o�     ��     ɛ      כ     ��     �     +�     J�     b�     ��     ��     ��     ʜ  	   ؜  
   �     �     �     �     ,�     C�  !   [�     }�     ��  8   ��  J   ̝     �     *�     8�     E�     X�     m�     y�     ��  (   ��     Ȟ  ?   ڞ  C   �     ^�     v�     ��     ��     ��     ؟     �     ��     ��     �     0�     <�     Q�     ^�     t�     ��  "   ��     Š     Ѡ     �      �     �     -�     <�     E�     b�     k�  V   ~�     ա  e   �  /   L�  4   |�  
   ��     ��     Ϣ  �   ע  �   Z�  -   "�     P�     a�     q�     ��  6   ��     Ѥ     �     	�  
   �     (�     6�     <�     T�  '   b�  h   ��  5   �  8   )�  /   b�  &   ��  	   ��  5   æ  
   ��     �     
�     �  1   -�  1   _�     ��     ��     ��     ��     ç     ѧ  &   ާ     �     �     "�     8�      N�  *   o�     ��     ��     ��     ��     �     �     �      �     :�  H   P�     ��     ��     ��  .   թ     �     �     �     '�     C�     T�     d�     x�     ��     ��     ��     ��     ۪     �  	   �  !   �     /�     H�     Q�     m�     ��     ��     ��  �   ��  #   d�  )   ��  #   ��     ֬  	   �     ��     �     /�     A�     Q�     c�     �  %   ��     ��  /   ܭ  !   �  "   .�  =   Q�     ��  3   ��  +   ۮ  6   �  !   >�  $   `�  !   ��  %   ��     ͯ  0   ��     �  9   7�  )   q�  @   ��  "   ܰ     ��     �     �     �  #   .�     R�  %   j�     ��     ��     ��     ��  
   ױ     �     �     �     �     #�     =�     T�     [�     m�  	   ��  '   ��     ��     ��     ̲     Բ     ۲     �     �     ��     �     (�     G�  F   _�  ,   ��  ;   ӳ    �  S   !�  S   u�     ɶ  E   ζ  +   �  N   @�  *   ��  )   ��  Z   �  Z   ?�  �   ��  �   2�    �  �   �  �   ۻ  �   ��  �   N�  f   �  �   O�  M   ��  �   D�  �   �  �   ��  �   3�  V   ��  �   �  �   ��     ��  �   #�  	  ��  5   ��  h   ��  K   O�  M   ��  �   ��  =  ~�  �   ��  �   ?�  Z   ��  �   %�  �   ��  G   f�     ��  �   ��  �   w�     �     �     �     �  #   0�     T�     t�     ��     ��     ��     ��     ��     �     �     $�  E   4�      z�  =   ��  6   ��     �     �  M   2�     ��     ��     ��     ��     ��  W   ��  $   4�     Y�     s�  +   ��     ��     ��  !   ��  &   
�  H   1�  U   z�     ��     ��     ��  V   �     _�     h�     x�  $   ��     ��     ��  Q   ��     �     "�     *�  �   ;�     �     '�     ?�     H�     L�  \   R�  �   ��  �   4�     ��  	   ��     �  
   �     �     ,�  
   9�     D�  	   \�     f�  	   m�     w�     ��     ��     ��     ��  
   ��     ��     ��  	   ��     ��     �     �     +�     ;�     C�     U�     t�  
   ��  	   ��     ��  	   ��  	   ��     ��     ��     ��     ��     �  	   �     �     "�  
   +�     6�     J�     S�     \�     h�  1   |�  
   ��     ��     ��  	   ��     ��     ��     ��     ��     �     .�     D�  	   U�     _�     t�     |�     ��     ��     ��     ��     ��     ��     ��     ��     �     
�  	   �     %�  	   :�     D�     `�     n�     �     ��     ��     ��     ��     ��     ��     ��     ��      �     �     -�     >�     J�     Y�     l�     �     ��     ��  
   ��     ��     ��     ��     ��     ��  	   �     �  
   �     "�     /�     8�  	   E�     O�     e�     r�     y�     ��  !   ��     ��  	   ��     ��     ��     	�     �     ,�  �  5�     ��     �     �   		This plugin provides bar for bookmarks.
		 %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i Attachment %i Attachments %i BackLink %i BackLinks %i _Backlink %i _Backlinks %i errors occurred, see log %i file will be deleted %i files will be deleted %i file will be trashed %i files will be trashed %i open item %i open items %i warnings occurred, see log (Un-)indenting a list item also changes any sub-items <Top> <Unknown> A desktop wiki A file with that name already exists. A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Action Add Application Add Bookmark Add Notebook Add bookmark/Show settings Add column Add new bookmarks to the beginning of the bar Add row Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files All Tasks Allow public access Always use last cursor position when opening a page Always wrap at character Always wrap at word boundaries An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Are you sure you want to delete the file '%s'? Arithmetic Ascii graph (Ditaa) Attach File Attach image first Attachment Browser Attachments Attachments: Author Auto
Wrap Auto indenting Automatically collapse sections on close page Automatically expand sections on open page Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave interval in minutes Autosave version on regular intervals Autosave version when the notebook is closed Back to Original Name BackLinks BackLinks Pane Backend Backlinks: Bazaar Bookmarks BookmarksBar Border width Bottom Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Cannot write this file. Probably this is due to the lenght
of the file name, please try using a name with less
than 255 characters Cannot write this file. Probably this is due to the lenght
of the file path, please try using a folder structure resulting in less
than 4096 characters Capture whole screen Center Change columns Changes Characters Characters excluding spaces Check Checkbox '<' Check Checkbox '>' Check Checkbox 'V' Check Checkbox 'X' Check _spelling Check and Update Index Checkbo_x List Checking a checkbox also changes any sub-items Classic trayicon,
do not use new style status icon on Ubuntu Clear Clone row Code Block Column 1 Command Command Palette Command does not modify data Comment Common include footer Common include header Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Copy Email Address Copy Line Copy Template Copy _As... Copy _Link Copy _Location Copy _link to this location Copy link to clipboard Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Created Cu_t Custom Tools Custom _Tools Customize... Date Date and Time... Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Demote Dependencies Description Details Diagram Discard note? Display line numbers Distraction Free Editing Do not use system trash for this notebook Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root Due E_xport... Edit %s Edit Custom Tool Edit Image Edit Link Edit Table Edit _Source Editing Editing file: %s Enable Version Control? Enable plugin Enabled Equation Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File exists, do you want to import? File is not writable: %s File name should not be blank. File name should not contain path declaration. File name too long: %s File path too long: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Fossil GNU R Plot Get more plugins online Get more templates online Git Gnuplot Go back Go forward Go to home page Grid lines Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide Journal pane if empty Hide menubar in fullscreen mode Highlight current line Home Page Horizontal _Line Icon Icons Icons & Text Icons & Text horizontal Id Id "%s" not found on the current page Images Import Page Include horizontal lines in the ToC Include subpages Index Index page Inline Calculator Insert Code Block Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Sequence Diagram Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Key Binding Key bindings Key bindings can be changed by clicking on a field with a key combination
in the list and then press the new key binding.
To disable a keybinding, select it in the list and use <tt>&lt;Backspace&gt;</tt>. Labels marking tasks Large Last Modified Leave Fullscreen Leave link to new page Left Left Side Pane Line Sorter Lines Link Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Managing table columns Map document root to URL Match _case Maximum number of bookmarks Maximum page width Menubar Mercurial Modified Month Move Selected Text... Move Text to Other Page Move column ahead Move column backward Move page "%s" to trash? Move text to Name Need output file to export MHTML Need output folder to export full notebook Never wrap lines New File New Page New Page in %s New S_ub Page... New _Attachment Next No Applications Found No changes since last version No dependencies No document root defined for this notebook No plugin available to display objects of type: %s No such file: %s No such page: %s No such wiki defined: %s No templates installed No text selected Notebook Notebooks OK Only Show Active Tasks Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open cell content link Open help Open in New Window Open in New _Window Open new page Open plugins folder Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" and all of it's sub-pages and
attachments will be deleted.

This deletion is permanent and cannot be un-done. Page "%s" and all of it's sub-pages and
attachments will be moved to your system's trash.

To undo later, go to your system's trashcan. Page "%s" does not have a folder for attachments Page Index Page Name Page Template Page already exists: %s Page is read-only and cannot be edited Page not allowed: %s Page not available: %s Page section Paragraph Password Paste Paste As _Verbatim Path Bar Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please select a row, before you push the button. Please select more than one line of text Please specify a notebook Plugin Plugin "%s" is required to display this object Plugins Port Position in the window Pr_eferences Prefer short link names for pages Prefer short names for page links Preferences Prev Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Remove column Remove links to %s Remove row Removing Links Rename file Rename or Move Page Rename page "%s" Repeated clicking a checkbox cycles through the checkbox states Replace _All Replace with Require authentication Restore page to saved version? Rev Right Right Side Pane Right margin position Row down Row up S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Screenshot Command Search Search _Backlinks... Search this section Section Section(s) to ignore Section(s) to index Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Sequence Diagram Server not started Server started Server stopped Set New Name Set ToC fontsize Set default browser Set default text editor Set to Current Page Show BackLink count in title Show Line Numbers Show Tasks as Flat List Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show due date in sidepane Show edit bar along bottom of editor Show full Page Name Show full Page Names Show full page name Show helper toolbar Show in the toolbar Show linkmap button in headerbar Show right margin Show tasklist button in headerbar Show tasklist in sidepane Show the cursor also for pages that can not be edited Show the page title heading in the ToC Single _page Size Small Smart Home key Some error occurred while running "%s" Sort alphabetically Sort pages by tags Source View Spell Checker Start Start _Web Server Su_bscript Su_perscript Support thumbnails for SVG Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Tags for non-actionable tasks Task Task List Tasks Template Templates Text Text Files Text From _File... Text background color Text foreground color Text wrap mode The file "%s" exists but is not a wiki page.
Do you want to import it? The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The following parameters will be substituted
in the command when it is executed:
<tt>
<b>%f</b> the page source as a temporary file
<b>%d</b> the attachment directory of the current page
<b>%s</b> the real page source file (if any)
<b>%p</b> the page name
<b>%n</b> the notebook location (file or folder)
<b>%D</b> the document root (if any)
<b>%t</b> the selected text or word under cursor
<b>%T</b> the selected text including wiki formatting
</tt>
 The inline calculator plugin was not able
to evaluate the expression at the cursor. The table must consist of at least on row!
 No deletion done. Theme There are no changes in this notebook since the last version that was saved There was a problem loading this plugin

 This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This page name cannot be used due to a conflicting file in the storage This page name cannot be used due to technical limitations of the storage This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a "path bar" to the top of the window.
This "path bar" can show the notebook path for the current page,
recent visited pages or recent edited pages.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds the page index pane to the main window.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows inserting 'Code Blocks' in the page. These will be
shown as embedded widgets with syntax highlighting, line numbers etc.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin also has properties,
see the notebook properties dialog This plugin cannot be enabled due to missing dependencies.
Please see the dependencies section below for details.

 This plugin opens a search dialog to allow quickly executing menu entries. The search dialog can be opened by pressing the keyboard shortcut Ctrl+Shift+P which can be customized via Zim's key bindings preferences. This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a macOS menubar for zim. This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a sequence diagram editor for zim based on seqdiag.
It allows easy editing of sequence diagrams.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This plugin turns one section of the notebook into a journal
with a page per day, week or month.
Also adds a calendar widget to access these pages.
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox '<' Toggle Checkbox '>' Toggle Checkbox 'V' Toggle Checkbox 'X' Toggle _Editable Toggle editable Tool Bar Toolbar size Toolbar style Top Top Pane Trash Page Trash failed, do you want to permanently delete instead ? Tray Icon Try wrap at word boundaries or character Turn page name into tags for task items Type Un-check Checkbox Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unkown Image type Unkown Object Unspecified Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use a page for each Use date from journal pages Use font color for dark theme Use horizontal scrollbar (may need restart) Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Use thumbnails Username Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log View debug log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s With this plugin you can embed a 'Table' into the wiki page. Tables will be shown as GTK TreeView widgets.
Exporting them to various formats (i.e. HTML/LaTeX) completes the feature set.
 Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Your system encoding is set to %s, if you want support for special characters
or see errors due to encoding, please ensure to configure your system to use "UTF-8" Zim Desktop Wiki Zoom _Out _About _Add _All Panes _Arithmetic _Attachment... _Back _Browse _Bugs _Cancel _Checkbox _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Delete... _Discard Changes _Duplicate Line _Edit _Edit Link _Edit Link or Object... _Edit Properties _Edit... _Emphasis _FAQ _File _Find _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump _Jump To... _Keybindings _Limit search to the current page and sub-pages _Link _Link to date _Link... _Mark _More _Move _Move Here _Move Line Down _Move Line Up _New Page Here... _New Page... _Next _Next in Index _None _Normal Size _Numbered List _OK _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Previous in Index _Print _Print to Browser _Properties _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove _Remove Heading _Remove Line _Remove Link _Remove List _Rename or Move Page... _Rename... _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In as due date for tasks as start date for tasks calendar:week_start:0 do not use horizontal lines macOS Menubar no grid lines readonly seconds translator-credits vertical lines with lines {count} of {total} Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2021-09-28 18:34+0000
Last-Translator: SC <lalocas@protonmail.com>
Language-Team: Portuguese <https://hosted.weblate.org/projects/zim/master/pt/>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.9-dev
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
 		Esta extensão fornece uma barra para os marcadores.
		 %(cmd)s
retornou um código diferente de zero %(code)i Surgiram %(n_error)i erros e %(n_warning)i avisos, consulte o registo %A, %d de %B de %Y %i Anexo %i Anexos %i _ligação inversa %i _ligações inversas %i _ligação inversa %i _ligações inversas Surgiram %i erros, consulte o ficheiro de erros %i ficheiro será eliminado %i ficheiros serão eliminados %i ficheiro será movido para o lixo %i ficheiros serão movidos para o lixo %i item aberto %i itens abertos Surgiram %i avisos, consulte o ficheiro de erros Remover o recuo de um item da lista também altera todos os sub-itens <Top> <Desconhecido> Uma wiki para o desktop Já existe um ficheiro com esse nome. O ficheiro com o nome <b>"%s"</b> já existe.
Pode usar outro nome ou substituir o ficheiro existente. Uma tabela deve ter ao menos uma coluna. Ação Adicionar aplicação Adicionar marcador Adicionar bloco de notas Adicionar marcador / Mostrar configurações Adicionar coluna Adicionar novos marcadores no início da barra Adicionar linha Adiciona suporte para verificação ortográfica usando gtkspell. 

Esta é uma extensão padrão que acompanha o Zim.
 Alinhar Todos os ficheiros Todas as Tarefas Permitir acesso público Usar sempre a última posição do ponteiro ao abrir uma página Corte sempre as linhas ao carácter Corte sempre as linhas no limite das palavras Ocorreu um erro ao gerar a imagem.
Você deseja guardar o texto de origem de qualquer maneira? Código da página anotado Aplicações Tem a certeza que quer eliminar o ficheiro '%s'? Aritmética Diagramas ASCII (Ditaa) Anexar ficheiro Anexar imagem primeiro Navegador de Anexos Anexos Anexos: Autoria Quebra de
linha automática Indentação automática Colapsar automaticamente as secções quando se fecha a página Expandir automaticamente as secções numa página aberta Versão automaticamente guardada pelo Zim Selecionar automaticamente a palavra atual quando aplicar formatação Converter automaticamente as palavras "CamelCase" em ligações Converter automaticamente caminhos de ficheiro em ligações Intervalo de salvamento automático em minutos Guardar automaticamente a versão em intervalos regulares Guardar versão automaticamente quando o bloco de notas for fechado Voltar para Nome Original Ligações inversas Painel de ligações inversas Back-end Ligações inversas: Bazaar Marcadores Barra de marcadores Espessura do limite Fundo Painel Inferior Procurar Lista com _marcadores C_onfigurar Não foi possível modificar a página: %s Cancelar Não posso salvar este ficheiro. Provavelmente isto é devido ao comprimento
do nome do ficheiro, por favor use um nome com menos
de 255 caracteres Não posso escrever para este ficheiro. Provavelmente isto é devido ao comprimento
do caminho do ficheiro, por favor use uma estrutura de pastas de onde resulte
um comprimento menor do que 4096 caracteres Capturar ecrã completo Centro Trocar colunas Alterações Caracteres Caracteres excluindo os espaços Marcar caixa de seleção com '<' Marcar caixa de seleção com '>' Marcar caixa de seleção com 'V' Marcar caixa de seleção com 'X' Verificar _ortografia Verificar e atualizar o índice Lista de cai_xa de seleção Marcar uma caixa de seleção também altera os seus sub-itens Ícone da bandeja clássico,
não use o novo estilo de ícone de estado no Ubuntu Limpar Clonar linha Bloco de código Coluna 1 Comando Paleta de comandos O comando não altera dados Comentário Incluir rodapé usual Incluir cabeçalho usual _Bloco de notas completo Configurar aplicações Configurar a extensão Configurar uma aplicação para abrir ligações "%s" Configurar uma aplicação para abrir ficheiros
do tipo "%s" Considerar todas as caixas de seleção como tarefas Copiar Copiar endereço de email Copiar linha Copiar modelo Copiar _como... Copiar _ligação Copiar a l_ocalização Copiar _ligação para este local Copiar ligação para a área de transferência Incapaz de encontrar o executável "%s" Não foi possível encontrar o bloco de notas: %s Não foi possível encontrar o modelo "%s" Não foi possível encontrar o ficheiro ou pasta para este bloco de notas Não foi possível carregar o verificador ortográfico Não foi possível abrir %s Não foi possível analisar a expressão Incapaz de ler: %s Não foi possível guardar a página: %s Criar uma nova página para cada nota Criar pasta? Criado Recor_tar Ferramentas personalizadas Ferramentas _personalizadas Personalizar... Data Data e hora... Dia Predefinido Formato predefinido para cópia de texto da área de transferência Bloco de notas predefinido Demora Eliminar página Eliminar página "%s"? Remover a linha Rebaixar Dependências Descrição Detalhes Diagrama Descartar nota? Exibir número de linhas Edição Livre de Distrações Não utilizar o caixote do lixo do sistema para este caderno Você deseja apagar todos os marcadores? Você quer restaurar a página: %(page)s
à versão guardada: %(version)s?

Todas as modificações desde a última versão gravada serão perdidas! Raiz do documento Vencimento E_xportar... Editar %s Editar ferramentas personalizadas Editar imagem Editar ligação Editar a Tabela Editar _fonte Edição A editar ficheiro: %s Ativar Controlo de Versão? Ativar plugin Ativado Equação Erro em %(file)s na linha %(line)i próximo "%(snippet)s" Calcular a Fór_mula Expandir _tudo Exportar Exportar todas as páginas para um único ficheiro Exportação concluída Exportar cada página para um ficheiro separado A exportar o bloco de notas Sem sucesso Falhou a execução: %s Falhou a execução da aplicação: %s O ficheiro já existe Modelos de _ficheiro… O ficheiro %s foi alterado no disco O ficheiro existe O ficheiro já existe. Quer importar? O ficheiro %s não é gravável O nome do ficheiro não pode ficar em branco. O nome do ficheiro não deve conter a declaração do caminho. O nome do ficheiro é demasiado longo: %s Caminho do ficheiro demasiado longo: %s Tipo de ficheiro não suportado: %s Nome do ficheiro Filtro Procurar Localizar _seguinte Localizar _anterior Procurar e substituir Encontrar o quê Sinalizar antes do fim de semana as tarefas devidas na segunda-feira ou terça-feira Pasta A pasta já existe e tem conteúdo, exportando para esta pasta pode gravar sobre ficheiros existentes. Quer continuar? A pasta já existe: %s Pasta com modelos para ficheiros de anexo Para pesquisas avançadas pode usar operadores como
AND, OR e NOT. Veja a página de ajuda para mais detalhes. For_matar Formato Fossil GNU R Plot Obter mais extensões na Internet Ver mais modelos na Internet Git Gnuplot Voltar Avançar Ir para a página inicial Linhas de grade Cabeçalho _1 Cabeçalho _2 Cabeçalho _3 Cabeçalho _4 Cabeçalho _5 Altura Esconder o painel do Diário se estiver vazio Esconder menu no modo ecrã completo Realçar a linha atual Página inicial _Linha horizontal Ícone Ícones Ícones e texto Ícones e texto horizontal ID ID "%s" não encontrado na página atual Imagens Importar página Incluir linhas horizontais no índice Incluir sub-páginas Índice Página de índice Calculadora Integrada Inserir Bloco de Código Inserir data e hora Inserir Diagrama Inserir Ditaa Inserir Equação Inserir Plot do GNU R Inserir Gnuplot Inserir imagem Inserir ligação Inserir Partitura Inserir imagem de ecrã Inserir Diagrama de Sequência Inserir Símbolo Inserir Tabela Inserir texto a partir de um ficheiro Interface Palavra-chave interwiki Diário Ir para Ir para página Tecla de atalho Teclas de atalho As teclas de atalho podem ser alteradas selecionando a ação na lista
e introduzindo o novo atalho.
Para desativar uma tecla de atalho, selecione-a na lista e use a tecla de <tt>&lt;Apagar&gt;</tt>. Rótulos a marcar tarefas Grande Última alteração Sair do ecrã completo Manter ligação para a nova página Esquerdo Painel Lateral Esquerdo Ordenador de Linhas Linhas Ligação Mapa de Ligações Ligar ficheiros sob a raiz do documento com o caminho completo do ficheiro Ligação para Localização Registar eventos com o Zeitgeist Ficheiro de registo Parece que encontrou uma falha Tornar aplicação predefinida Gerir colunas da tabela Mapear documento raiz para URL Capitalização Número máximo de marcadores Largura máxima da página Barra de menu Mercurial Modificado Mês Mover texto selecionado... Mover texto para outra página Mover coluna à frente Mover coluna para trás Mover a página "%s" para o lixo? Mover texto para Nome É necessário um ficheiro de saída para exportar MHTML É necessário uma pasta de saída para exportar o bloco de notas completo Nunca corte linhas Novo ficheiro Nova página Nova página em %s Nova s_ub-página... Novo an_exo Próx. Nenhuma aplicação encontrada Nenhuma mudança desde a última versão Sem dependências Não existe um documento raiz definido para este bloco de notas Não há nenhum plugin disponível para mostrar objetos do tipo: %s Ficheiro %s inexistente Página não existe: %s Nenhum wiki definido: %s Nenhum modelo instalado Nenhum texto selecionado Bloco de notas Blocos de Notas OK Mostrar Somente Tarefas Ativas Abrir pasta de ane_xos Abrir pasta Abrir bloco de notas Abrir com... Abrir _documento raiz Abrir pasta do b_loco de notas Abrir _Página Abrir ligação contida na célula Abrir ajuda Abrir numa Nova Janela Abrir numa nova _janela Abrir nova página Abrir pasta de extensões Abrir com "%s" Opcional Opções para a extensão %s Outro... Ficheiro de saída O ficheiro de saída existe , especifique "--sobrescrever" para forçar a exportação Pasta de destino A pasta de saída existe e não está vazia, especifique "--sobrescrever" para forçar a exportação Local de saída necessário para a exportação O resultado deve substituir o atualmente selecionado Substituir B_arra de Caminhos Página A página "%s" e todas as suas sub-páginas e
anexos serão eliminados.

Esta eliminação é permanente e não pode ser desfeita. A página "%s" e todas as suas sub-páginas e
os anexos serão movidos para o caixote do lixo do sistema operativo.

Para desfazer esta ação mais tarde, vá ao caixote do lixo do sistema operativo. A página "%s" não tem uma pasta para anexos Índice Páginas Nome da página Modelo de página Página já existe: %s A página é de apenas leitura e não pode ser editada Página não permitida: %s Página não disponível: %s secção da página Parágrafo Palavra-passe Colar Colar sem _formatação Barra Caminho Insira um comentário para esta versão Note que criar uma ligação para uma página inexistente
também cria uma nova página automaticamente. Selecionar um nome e uma pasta para o bloco de notas. Por favor, selecione uma linha antes de premir o botão. Por favor seleccione mais de uma linha de texto or favor especifique um bloco de notas Extensão É necessário o plugin "%s" para mostrar este objeto Extensões Porta Posição na janela _Preferências Preferir nomes de ligações curtas para páginas Preferir nomes curtos para ligações de páginas Preferências Ant. Imprimir no navegador Promover Pr_opriedades Propriedades Encaminha os eventos Zeitgeist Daemon. Nota Rápida Nota Rápida... Alterações recentes Mudanças recentes… Páginas _alteradas recentemente Reformatar a marcação wiki em tempo real Remover Remover todos Remover a coluna Remover hiperligações para %s Remover linha Removendo as ligações Alterar nome do ficheiro Renomear ou mover página Renomear página "%s" Clicar repetidamente numa caixa de seleção muda para o estado seguinte Substituir _todos Substituir por Autenticação necessária Restaurar página à última versão guardada? Rev Direito Painel Lateral Direito Posição da margem direita Linha para baixo Linha para cima Gu_ardar versão... Guardar uma _cópia... Guardar cópia Guardar Versão Guardar marcadores Versão guardada pelo Zim Ocorrências Comando de imagem de ecrã Pesquisar Pesquisar ligações _inversas… Pesquisar nesta secção Secção Secção(ões) para ignorar Secção(ões) para indexar Selecionar ficheiro Seleccionar Pasta Selecionar Imagem Seleciona uma versão para ver as mudanças entre essa versão e a condição atual.
Ou seleciona múltiplas versões para ver as mudanças entre essas versões.
 Selecione o formato de exportação Selecione o ficheiro de saída ou a pasta Selecione as páginas para exportar Selecionar janela ou região Seleção Diagrama de Sequência Servidor não inicializado Servidor iniciado Servidor parado Definir Novo Nome Tamanho da fonte do índice Definir o navegador padrão Definir o editor de texto predefinido Definir como Página Atual Mostrar total de ligações inversas no título Apresentar a numeração de Linha Mostrar Tarefas como Lista Simples Mostrar TdC como um widget flutuante e não no painel lateral Mostrar _Modificações Mostrar um ícone separado para cada bloco de notas Mostrar data de conclusão na barra lateral Mostra a barra de edição na parte inferior do editor Exibir o nome completo da página Mostrar nomes das páginas completos Exibir o nome completo da página Mostrar barra de ferramentas auxiliar Mostrar na barra de ferramentas Mostrar o mapa de ligações na barra de título Mostrar a margem direita Mostrar botão da lista de tarefas na barra de cabeçalho Mostrar lista de tarefas na barra lateral Mostrar o cursor também em páginas que não podem ser editadas Mostra o título da página na TdC _Página única Tamanho Pequeno Smart Home key Algum erro ocorreu ao executar "%s" Ordenar alfabeticamente Classificar as páginas por etiquetas Visualizar código Corretor ortográfico Início Iniciar servidor _web S_ubscrito S_obrescrito Suporte de miniaturas para SVG Sí_mbolo… Sintaxe Predefinição do sistema largura da tabulação Tabela Editor de Tabelas Tabela de Conteúdos Etiquetas Etiquetas para tarefas não-acionáveis Tarefa Lista de Tarefas Tarefas Modelo Modelos Texto Ficheiros de texto Texto de _ficheiro... cor de fundo do texto cor de primeiro plano do texto Modo de corte de linhas O ficheiro "%s" existe mas não é uma página wiki.
Quer importá-lo? A pasta
%s
não existe.
Quer criá-la agora? O diretório "%s" ainda não existe.
Deseja criá-lo agora? Os seguintes parâmetros serão substituídos
no comando quando este for executado:
<tt>
<b>%f</b> o código-fonte da página como ficheiro temporário
<b>%d</b> o diretório de anexos da página atual
<b>%s</b> o ficheiro real com o código-fonte da página (se existir)
<b>%p</b> o nome da página
<b>%n</b> a localização no bloco de notas (ficheiro ou diretório)
<b>%D</b> o documento raiz (se existir)
<b>%t</b> o texto ou palavra selecionada sob o cursor
<b>%T</b> o texto selecionado incluindo a formatação wiki
</tt>
 A extensão de calculadora integrada não conseguiu
avaliar a expressão no cursor. A tabela deve ser constituída por pelo menos uma linha!
 Remoção não executada. Tema Não há mudanças neste bloco de notas desde a última versão salva Houve um problema ao carregar este plugin

 Isto pode significar que você não tem os
dicionários apropriados instalados O ficheiro já existe.
Quer substituí-lo? Esta página não tem uma pasta de anexos Este nome de página não pode ser usado devido a um ficheiro em conflito no armazenamento Este nome de página não pode ser usado devido à limitações técnicas do armazenamento Esta extensão permite capturar a imagem do ecrã e a inserir 
diretamente em uma página do Zim. 

Esta é uma extensão padrão que acompanha o Zim.
 Este plugin adiciona uma "barra de caminho" no topo da janela.
Esta "barra de caminho" pode mostrar o caminho no bloco de notas da página corrente,
páginas visitadas recentemente ou páginas editadas recentemente.
 Esta extensão adiciona uma caixa de diálogo que mostra todas as tarefas
em aberto neste bloco de notas. As tarefas em aberto podem ser tanto caixas de
seleção ou itens marcados com etiquetas como "TODO" ou "FIXME".

Esta é uma extensão padrão que acompanha o Zim.
 Esta extensão adiciona uma caixa de diálogo para colocar rapidamente texto
ou conteúdo da área de transferência numa página do Zim. 

Esta é uma extensão padrão que acompanha o Zim.
 Esta extensão adiciona um ícone na área de notificação para acesso rápido.

Esta extensão depende do Gtk+ versão 2.10 ou mais recente. 

Esta é uma extensão padrão que acompanha o Zim.
 Esta extensão adiciona um widget extra que apresenta uma lista de páginas
que contêm ligações para a página atual.

Esta é uma extensão padrão que acompanha o Zim.
 Esta extensão adiciona um widget extra que apresenta
uma tabela de conteúdos para a página atual.

Esta é uma extensão padrão que acompanha o Zim.
 Esta extensão adiciona configurações que ajudam a usar o Zim
como um editor livre de distrações.
 Esta extensão adiciona o diálogo "Inserir Símbolo" e permite
a auto-formatação de caracteres tipográficos. 

Esta é uma extensão padrão que acompanha o Zim.
 Este plugin adiciona o painel com o índice de páginas à janela principal.
 Esta extensão adiciona controlo de versão para os blocos de notas Zim.

Esta extensão suporta Bazaar, Git e sistemas de controlo de versão Mercurial. 

Esta é uma extensão padrão que acompanha o Zim.
 Este plugin pemite inserir 'blocos de código' na página. Estes serão
exibidos como widgets embebidos com realce de sintaxe, número de linha etc.
 Esta extensão permite embutir cálculos aritméticos no Zim.
Baseia-se no módulo de aritmética de
http://pp.com.mx/python/arithmetic
 Esta extensão permite você desenvolver rapidamente simples
equações matemáticas no Zim. 

Esta é uma extensão padrão que acompanha o Zim.
 Esta extensão tem propriedades,
consulte o diálogo de propriedades do bloco de notas Este plugin não pode ser ativado devido a dependências que faltam.
Consulte a secção de dependências abaixo para obter detalhes.

 Este plugin abre um diálogo de pesquisa para permitir a rápida execução de entradas de menu. O diálogo de pesquisa pode ser aberto pressionando o atalho de teclado Ctrl+Shift+P que pode ser personalizado através das preferências de teclas do Zim. Esta extensão fornece um editor de diagrama para o Zim baseado em Ditaa. 

Esta é uma extensão padrão que acompanha o Zim.
 Esta extensão fornece um editor de diagramas para o Zim baseado no GraphViz.

Esta é uma extensão padrão que acompanha o Zim.
 Esta extensão fornece um diálogo com uma representação 
gráfica da estrutura de ligação do bloco de notas.
Ele pode ser usado como uma espécie de "mapa mental" 
que mostra como as páginas se relacionam. 

Esta é uma extensão padrão que acompanha o Zim.
 Esta extensão fornece um menu tipo macOS para o zim. Esta extensão fornece um índice de páginas filtrado por meio de uma seleção de etiquetas em nuvem.
 Esta extensão fornece um editor de gráficos para o Zim baseado no Gnu R.
 Esta extensão fornece um editor de gráficos para o Zim baseado no Gnuplot.
 Esta extensão fornece um editor de diagrama de sequência para o Zim baseado no seqdiag.
Ele permite a fácil edição de diagramas de sequência.
 Esta extensão fornece uma solução para a falta de
suporte para impressão no Zim. Ela exporta a página atual
para HTML e abre um navegador web. Presumindo que o 
navegador tem suporte para impressão, isso enviará seus
dados para a impressora em dois passos. 

Esta é uma extensão padrão que acompanha o Zim.
 Esta extensão fornece um editor de equações para o Zim com base no LaTeX. 

Esta é uma extensão padrão que acompanha o Zim.
 Esta extensão proporciona um editor de partitura para o Zim baseado no GNU Lilypond.

Esta é uma extensão padrão que acompanha o Zim.
 Esta extensão mostra a pasta de anexos da página atual como
ícones no painel inferior.
 Esta extensão ordena alfabeticamente as linhas selecionadas. 
Se a lista já estiver ordenada, a ordem será invertida
(A-Z para Z-A).
 Esta extensão transforma uma secção do bloco de notas num diário
com uma página por dia, semana ou mês.
Também acrescenta um widget de calendário para acessar essas páginas.
 Isto normalmente significa que o ficheiro contém caracteres inválidos Título Para continuar pode guardar uma cópia desta página ou descartar 
as mudanças realizadas. Se guardar uma cópia, as mudanças também 
serão perdidas, mas poderá recuperar a cópia depois. Para criar um novo bloco de notas tem que selecionar uma pasta vazia.
Também pode selecionar uma pasta de bloco de notas Zim existente.
 Tdc Ho_je Hoje Alternar caixa de seleção '<' Alternar caixa de seleção com '>' Alternar caixa de seleção 'V' Alternar caixa de seleção 'X' Alternar _editável Alternar editável Barra de ferramentas Tamanho da barra de ferramentas Estilo da barra de ferramentas Topo Painel Superior Página do lixo O caixote do lixo falhou, quer em vez disso eliminar permanentemente? Ícone na Área de Notificação Tente cortar as linhas nos limites das palavras ou caracteres Converter o nome de páginas em etiquetas para tarefas Tipo Desmarcar caixa de seleção Remover recuo ao usar a tecla <Apagar>
(se desativado pode usar <Shift><Tab>) Desconhecido Tipo de imagem desconhecido Objeto desconhecido Não especificado Sem etiqueta Atualizar %i página ligada a esta página Atualizar %i páginas ligadas a esta página Atualizar o cabeçalho desta página Atualizando as ligações Atualizando o índice Usar %s para alternar para o painel lateral Usar fonte personalizada Use uma página para cada Usar data das páginas do diário Usar a cor da fonte para o tema escuro Usar a barra de deslocamento horizontal (pode ser necessário reiniciar) Usar a tecla <Enter> para seguir as ligações
(se desativado pode usar <Alt><Enter>) Usar miniaturas Nome do utilizador Controlo de versões Controlo de versão encontra-se desativado para este bloco de notas.
Deseja ativá-lo? Versões Ver _Anotação Ver r_egisto Ver r_egisto de depuração de erros Servidor web Semana Ao reportar este erro, por favor inclua
as informações da caixa de texto abaixo Pala_vra completa Largura Página Wiki: %s Com esta extensão você pode inserir uma 'Tabela' em uma página wiki. As tabelas serão mostradas como widgets GTK TreeView.
Exportá-los para vários formatos (ou seja, HTML / LaTeX) completa o conjunto de recursos.
 Contar palavras Contagem de palavras... Palavras Ano Ontem Está a editar um ficheiro numa aplicação externa. Pode fechar esta janela quando terminar Pode configurar ferramentas personalizadas que irão aparecer
no menu Ferramentas e na barra de Ferramentas ou em menus de contexto. A codificação do seu sistema é %s, se deseja suportar caracteres especiais
ou está a ver erros devido à codificação, certifique-se que configura o seu sistema para usar "UTF-8" Wiki Pessoal Zim _Diminuir So_bre _Adicionar _Todos os Painéis _Aritmética _Anexos… _Retroceder uma página _Explorar _Erros _Cancelar _Caixa de seleção _Descer um nível Remo_ver formatação _Fechar _Contrair tudo _Conteúdo _Copiar _Copiar aqui _Eliminar _Eliminar página _Eliminar... _Descartar alterações _Duplicar Linha _Editar _Editar ligação _Editar ligação ou objeto… Editar _propriedades _Editar… _Itálico _Questões frequentes _Ficheiro _Procurar _Localizar... _Avançar uma página Ecrã _completo _Navegar A_juda _Realçar _Histórico _Início _Imagem... _Importar página.. _Inserir _Ir para Ir _para... Atalhos de _teclado _Limitar pesquisa à página atual e sub-páginas _Ligação _Ligação para data _Ligação... _Realçar _Mais _Mover _Mover Aqui Mover Linha para _Baixo Mover Linha para _Cima _Nova página aqui... _Nova página... Pró_ximo Pró_ximo no índice _Nenhum Tamanho _normal Lista _numerada _OK _Abrir Abrir outro _bloco de notas... _Outro(s)... _Página Hierarquia da página _Subir um nível Co_lar _Pré-visualizar A_nterior Anterior _no índice _Imprimir Visualizar no navegador Web _Propriedades _Nota Rápida... _Sair Páginas _recentes Re_fazer Expressão _regular _Recarregar _Remover _Remover cabeçalho _Remover Linha _Remover ligação _Remover lista _Renomear ou mover página... Altera_r nome... Substit_uir S_ubstituir... _Restaurar tamanho _Restaurar Versão _Guardar _Guardar cópia _Capturar ecrã... _Pesquisar _Pesquisar... _Enviar para... Painéis _laterais _Lado a Lado _Ordenar linhas Ra_surado N_egrito S_ubscrito S_obrescrito _Modelos _Ferramentas _Desfazer Texto não _formatado _Versões... _Vista A_mpliar como data limite para tarefas como data de início para tarefas calendar:week_start:1 não usar linhas horizontais Barra de menu macOS sem linhas de grade apenas leitura segundos Launchpad Contributions:
  DarkVenger https://launchpad.net/~darkvenger
  Jaap Karssenberg https://launchpad.net/~jaap.karssenberg
  Jorge Paulo https://launchpad.net/~jorge-paulo
  João Santos https://launchpad.net/~jmcs
  Mark Alan https://launchpad.net/~malan
  NeLaS https://launchpad.net/~organelas
  Pedro Alves https://launchpad.net/~alvespms
  debastosj https://launchpad.net/~debastosj
  mtibbi https://launchpad.net/~marcio-tibirica linhas verticais com linhas de grade {count} de {total} 