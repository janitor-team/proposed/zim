��    �     �              �  ,   �  .   �     )     5  0   Q     �     �  	   �     �  i   �     &     6     C  -   P  V   ~  	   �  	   �     �  3   �  Y   1      �      �   
   �      �      �      �      �      �   $   �   ?   #!  /   c!  (   �!  %   �!     �!  	   �!     "     "     "  	    "     *"     7"     C"     J"  
   W"     b"     z"     �"     �"  
   �"     �"     �"     �"  <   �"     !#     '#     /#     L#     T#     g#     ~#  +   �#  3   �#      �#     $     #$     1$  
   =$     H$     W$     v$  3   �$     �$     �$     �$     %     %     ?%     N%     S%     `%     n%     {%     �%     �%  0   �%     �%     �%     �%     �%     �%     �%     &     &     &     (&  $   A&  ~   f&     �&  
   �&     �&  
   '  	   '     $'     1'     9'     J'     b'  5   j'     �'     �'     �'     �'     �'     �'     �'      (     (     *(     =(     V(     b(     {(     �(     �(     �(  
   �(     �(     �(  	   �(  6   �(     )  v   )     �)  *   �)  c   �)     7*     ?*     F*     ^*     x*     |*  
   �*  
   �*  
   �*  
   �*  
   �*     �*     �*  	   �*     �*     �*     �*     +     +  
   +     &+     8+     M+     \+     i+     y+     �+     �+     �+     �+     �+     �+     �+  	   �+      ,     ,     ,     ",     /,     D,     R,     i,     x,     �,     �,  2   �,     �,     �,     �,     �,     �,     -     .-     G-     S-     f-  	   n-     x-     �-     �-     �-     �-     �-      �-  *   �-     .     .     %.     6.     F.     \.     z.     �.     �.     �.     �.  	   �.     �.     �.     �.     /     /     !/     5/  
   K/     V/     i/     }/     �/     �/     �/     �/     �/  9   �/     0  I   0  !   `0  '   �0  	   �0     �0     �0  0   �0  	   �0     �0  	   1  '   1  V   =1  3   �1     �1     �1     �1     �1     �1     2     2     &2     72     ?2  
   K2  &   V2  
   }2     �2     �2     �2     �2     �2     �2  
   �2     3     3     !3     .3     ;3     Z3     ^3     n3     3  	   �3     �3     �3     �3     �3     �3     �3     �3     �3     4  �   4     �4      �4     �4     �4  	   5     5     /5     >5     M5     Z5     r5  2   �5     �5  &   �5     �5     6  5   6     L6     Y6  &   ^6     �6     �6     �6     �6  
   �6     �6     �6     �6     �6  	   7     7  	   7     7  
   $7     /7     B7     X7  ?   n7  A   �7  K   �7  6   <8  -   s8  �   �8  �   j9  �   �9  �   }:  �   �:  �   �;  k   <  �   �<  R   W=  ;   �=  =   �=    $>  j   8?  ]   �?  7   @     9@  �   ?@  |   �@     XA     \A     cA     iA     }A     �A  	   �A  '   �A     �A  D   �A     B     B  H   'B     pB     �B     �B     �B     �B  P   �B     %C  U   5C     �C     �C  	   �C  
   �C     �C  N   �C     D     D     D  
   -D     8D     FD     LD  	   QD  ^   [D  f   �D     !E  	   2E     <E  
   CE     NE     ZE     `E     hE     nE     uE     �E     �E  	   �E     �E  
   �E     �E     �E     �E     �E  
   �E     �E     F  	   F     !F     &F     ,F     5F     >F     JF     NF  
   TF     _F     hF  	   nF     xF     �F     �F     �F     �F     �F     �F     �F     �F     �F  
   �F     �F     �F     �F     �F     G     G  	   +G     5G     ;G     KG     SG     ZG  	   cG     mG     G     �G     �G     �G     �G     �G     �G     �G     �G     �G     �G     H  
   	H     H     #H  
   +H     6H     BH     NH     \H     hH     pH  
   xH     �H  
   �H     �H     �H  	   �H     �H     �H     �H     �H     �H     �H     �H  �  I      �J      K  &   ;K  (   bK     �K     �K     �K     �K     �K  M   �K     $L     7L     DL  !   TL  Q   vL     �L     �L     �L  -   �L  .   #M     RM     eM     rM     M     �M     �M     �M     �M     �M  *   �M  +   �M  $   +N     PN     lN     N     �N     �N     �N     �N  	   �N     �N     �N     �N  
   �N     �N     	O     O     #O     *O     1O     JO     [O  `   oO     �O     �O     �O     �O     �O     P     %P  )   8P  /   bP  $   �P     �P     �P     �P     �P     �P     
Q     "Q  *   ;Q     fQ     wQ     �Q     �Q     �Q     �Q  
   �Q     �Q     R  	   R     %R     ,R     0R  '   7R     _R     oR     vR     �R     �R     �R     �R     �R     �R     �R     �R  �   �R     �S  
   �S     �S     �S     �S     �S     �S     �S     T  	   !T  E   +T     qT     �T     �T     �T     �T     �T     �T     �T     �T     �T  "   U     3U     CU     ZU     wU  	   �U     �U     �U     �U     �U     �U  6   �U  	   V  i   V     yV  $   �V  Z   �V  
   W     W     W     8W     QW     UW     ]W     nW     W     �W     �W     �W     �W     �W     �W     �W     �W     �W     X  	   X     X     (X     ;X     HX     UX     eX     uX     �X     �X     �X     �X     �X     �X     �X     �X     Y     Y     Y     Y     8Y     EY     aY  	   nY     xY     |Y  0   �Y  	   �Y     �Y     �Y     �Y     �Y     Z     .Z     JZ     ^Z  	   qZ  	   {Z     �Z     �Z     �Z     �Z     �Z     �Z     �Z  '   �Z     "[  	   /[     9[     M[     [[  '   q[     �[     �[     �[     �[  	   �[  	   �[     \     \     ,\     <\     L\     Y\     s\     �\     �\     �\     �\     �\  	   �\     ]  	   !]     +]  ?   8]     x]  K   �]     �]     �]     ^     ^     $^  $   +^     P^     ]^     j^     q^  C   �^  -   �^     �^     _     _     ,_     0_  
   C_     N_     U_     h_  
   o_     z_  )   �_     �_     �_     �_     �_     �_  (   `     1`     8`     E`     R`     j`  	   {`  '   �`     �`     �`     �`     �`     �`     �`     �`     
a     a     %a     ,a     Fa     Sa     ca  w   pa     �a     �a     b     3b     Ib     Vb     ib     |b     �b     �b     �b  '   �b     �b     c      c     9c  9   Lc  
   �c     �c     �c     �c     �c     �c     �c     d     d     d     &d     -d     :d     Gd     Nd     Ud     \d     id     �d     �d  =   �d  ,   �d  <   e     We     we  �   �e  �   ff  �   �f  �   �g  {   *h  v   �h  f   i  �   �i  B   ,j  M   oj  5   �j  *  �j  [   l  7   zl  0   �l     �l  �   �l  d   tm     �m  
   �m     �m     �m     n     0n     =n  '   Mn     un  Q   �n     �n     �n  +   �n     o     3o     @o     Mo     `o  M   so     �o  :   �o     	p     p     !p     2p     Bp  3   Fp     zp     �p     �p     �p     �p     �p     �p     �p  T   �p  X   +q     �q  
   �q  
   �q     �q  
   �q  
   �q  
   �q     �q     �q     �q     r     r  
   1r  
   <r     Gr  
   Xr     cr     tr  
   �r     �r     �r     �r     �r     �r  
   �r     �r  
   s     s  
    s  
   +s     6s  
   Ms     Xs     fs     ts  
   �s     �s     �s  
   �s     �s     �s     �s  
   �s  
   �s     �s     t     t     $t     5t     Ft     `t     }t  
   �t     �t     �t  
   �t  
   �t     �t     �t     �t  
   u     u      u     1u     Eu     Vu  
   gu     ru     �u     �u  
   �u     �u     �u  
   �u     �u     �u     �u     
v  
   v     &v     4v     Bv     Pv  
   ^v  
   iv  
   tv     v     �v  
   �v  
   �v     �v     �v     �v  �  �v   		This plugin provides bar for bookmarks.
		 %(cmd)s
returned non-zero exit status %(code)i %A %d %B %Y %i errors occurred, see log %i file will be deleted %i files will be deleted %i open item %i open items <Top> <Unknown> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. Add Application Add Bookmark Add Notebook Add new bookmarks to the beginning of the bar Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 All Files All Tasks Allow public access Always use last cursor position when opening a page An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Arithmetic Attach File Attach image first Attachment Browser Attachments Author Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave version on regular intervals Back to Original Name BackLinks BackLinks Pane Backend Bazaar Bookmarks BookmarksBar Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Capture whole screen Changes Characters Characters excluding spaces Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Command Command does not modify data Comment Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Could not find executable "%s" Could not find notebook: %s Could not find the file or folder for this notebook Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Demote Dependencies Description Details Discard note? Distraction Free Editing Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root E_xport... Edit Custom Tool Edit Image Edit Link Edit _Source Editing Editing file: %s Enable Version Control? Enabled Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export completed Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Get more plugins online Get more templates online Git Gnuplot Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide menubar in fullscreen mode Home Page Icon Images Import Page Include subpages Index Index page Inline Calculator Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Symbol Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Labels marking tasks Last Modified Leave link to new page Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Map document root to URL Match _case Maximum page width Menubar Mercurial Modified Month Move Selected Text... Move Text to Other Page Move text to Name Need output file to export MHTML Need output folder to export full notebook New File New Page New S_ub Page... New _Attachment No Applications Found No changes since last version No dependencies No such file: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open in New Window Open in New _Window Open new page Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Name Page Template Paragraph Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please specify a notebook Plugin Plugins Port Position in the window Pr_eferences Preferences Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Removing Links Rename page "%s" Replace _All Replace with Restore page to saved version? Rev Right Side Pane S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Search Search _Backlinks... Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Server not started Server started Server stopped Set New Name Set default text editor Set to Current Page Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show full Page Name Show in the toolbar Show the cursor also for pages that can not be edited Single _page Size Some error occurred while running "%s" Sort alphabetically Sort pages by tags Spell Checker Start _Web Server Sy_mbol... System Default Table of Contents Tags Task Task List Template Templates Text Text Files Text From _File... Text background color Text foreground color The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? There are no changes in this notebook since the last version that was saved This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Tray Icon Turn page name into tags for task items Type Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use a custom font Use a page for each Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Zim Desktop Wiki Zoom _Out _About _All Panes _Arithmetic _Back _Browse _Bugs _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Edit _Edit Link _Edit Link or Object... _Edit Properties _Emphasis _FAQ _File _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _Move Here _Next _None _Normal Size _Numbered List _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In calendar:week_start:0 readonly seconds translator-credits Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2017-04-28 22:03+0000
Last-Translator: Jaap Karssenberg <jaap.karssenberg@gmail.com>
Language-Team: Simplified Chinese <zh_CN@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2020-01-01 13:40+0000
X-Generator: Launchpad (build bceb5ef013b87ef7aafe0755545ceb689ca7ac60)
 		這個插件顯示書籤欄
		 %(cmd)s
返回碼非零 %(code)i %A (週幾) %d (日) %B (月) %Y (年) 遇到了 %i 個錯誤，請查看日誌 %i 檔案將被移除 %i 件未完成工作 <Top> <未知的> 桌面維基 此檔名<b>"%s"</b>已存在\n
你可更改成其它檔名或是直接覆蓋 新增應用程式 新增書籤 新增筆記本 新書籤新增到書籤欄最前 藉由 gtkspell 提供拼字檢查功能

此為 zim 內附之核心外掛程式
 全部文件 所有任務 允許公開存取 打開頁面時指標永遠位于上次位置 圖像產生失敗
要儲存原始文字嗎？ 註解頁面來源 應用程式 算術運算 附加檔案 先附加圖像 附件瀏覽器 附件 作者 zim 自動儲存的版本 套用格式時，自動選取目前文字 自動將 "CamelCase" 文字轉換為鏈結 自動將檔案路徑轉換為鏈結 每隔一段間自動儲存 恢復原有名稱 反向連結 反向連結窗格 後端 Bazaar 書籤 書籤欄 底部窗格 瀏覽 項目符號列表 設定(_O) 無法修改頁面： %s 取消 快照整面螢幕 更動 字符 字元數 (不計空格) 拼字檢查(_S) 可復選列表(_x) 使用舊樣式之系統匣圖示
在 Ubuntu 作業系統上不使用新風格之狀態欄圖示 清除 指令 指令不會更動資料 註記 整部筆記本(_n) 設定應用程式 設定外掛程式 設定用于打開“%s”連結的程式 設定用于打開“%s”類型文件的程式 將所有勾選框視為工作項目 複製電子郵址 複製範本 複製為(_A)... 複製鏈結(_L) 複製位置(_L) 無法找到程式 "%s" 找不到筆記本： %s 找不到此筆記本之檔案或文件夾 無法開啟: %s 不能解析表達式 無法讀取: %s 無法儲存頁面： %s 為每則記事建立新頁面 要建立文件夾？ 剪下(_T) 自訂工具 自行定義工具(_T) 自訂... 日期 天 預設 復制到剪貼板文本的預設格式 預設筆記本 延遲 刪除頁面 刪除頁面 "%s"? 降級 依賴套件 說明 細節 放棄筆記? 免打擾編輯模式 您想要刪除所有書籤嗎? 您要將頁面：%(page)s
回復至先前儲存的版本：%(version)s 嗎？

上次儲存版本以來，至今的所有更動都將消失喔！ 文件根目錄 匯出(_X) 編輯自訂工具 編輯圖像 編輯鏈結 編輯原始碼(_S) 編輯 正在編輯的文件:%s 啟用版本控制？ 已啟用 檔案 %(file)s 的第%(line)i 行, 在 "%(snippet)s" 附近有錯誤 執行計算(_M) 展開所有(_A)內容 匯出 匯出已完成 匯出筆記本 失敗 執行錯誤: %s 程式執行錯誤: %s 檔案已存在 文件模板(_T) 硬碟裡的檔案已被修改: %s 已有此檔案 檔案無法寫入: %s 不支援的文件類型: %s 檔案名稱 過濾器 尋找 找下一個(_X) 找上一個(_V) 尋找並替換 尋找 將任務標記為在週末前的週一或週二到期 文件夾 此為已存在並有檔案之文件夾。若匯入此文件夾將覆蓋現存檔案。要這麼做嗎？ 目錄已存在: %s 包含附件文件模板的文件夾 欲進階搜尋，可使用運算元如AND、OR、NOT
細節可參考「幫助」之內容 格式(_M) 格式 連線獲取更多插件 連線獲取更多模板 Git Gnuplot 一級標題(_1) 二級標題(_2) 三級標題(_3) 四級標題(_3) 五級標題(_5) 高度 全螢幕時隱藏選單列 首頁 圖示 圖像 匯入頁面 包括子頁面 索引 索引頁 內置計算機 加入日期時間 加入圖表 插入 ditaa 加入方程式 加入數據圖 插入 Gnuplot 加入圖像 加入鏈結 插入樂譜 加入螢幕快照 加入符號 加入檔案裡的文字 界面 內部維基關鍵詞 日誌 前往 前往頁面 以標籤註記工作項目 最後更新 保留到新頁面的連結 左側窗格 行排序 行 鏈結圖覽 以完整路徑鏈結文件根目錄下之文件 鏈結至 位置 使用 Zeitgeist 記錄事件 系統紀錄檔 好像你發現了一個Bug 設定預設應用程式 對應根目錄文件為URL 符合大小寫(_C) 最大頁面寬度 選單列 Mercurial 修改日期 月 移動已選文字... 移動文本到其它頁面 移動文字到 名稱 指定檔案以匯出MHTML 指定資料夾以匯出整個筆記本 新增檔案 新頁面 建立子頁面(_U) 新附件(_A) 未找到應用程式 最近版本以來，至今尚無更動 無需依賴其他套件 無此檔案：%s 內部鏈結 %s 無效 沒有安裝任何模板 筆記本 筆記本 確定 開啟附件之文件夾(_F) 打開文件夾 開啟筆記本 以...開啟 開啟文件根目錄(_D) 開啟筆記本之文件夾(_N) 打開頁面(_P) 在新視窗開啟 於新視窗中開啟(_W) 保留到新頁面的連結 以 "%s" 開啟 可選的 外掛程式 %s 之選項 其它... 匯出檔案 輸出檔案已存在, 可以使用"--overwrite"以強制匯出 匯出文件夾 輸出資料夾已存在且非空, 可以使用"--overwrite"以強制匯出 指定用於匯出的位置 其輸出會替換當前所選 覆蓋 路徑列(_A) 頁面 頁面 "%s" 並無附件之文件夾 頁面名稱 頁面樣式 段落 請為此版本加註 請留意，當鏈結至不存在之頁面
將自動建立新頁面 請為您的筆記本選擇名字和文件夾 請指定一個筆記本 外掛程式 外掛程式 埠 視窗中的位置 選項(_E) 選項 列印至瀏覽器 提升 屬性(_T) 屬性 事件記錄送往 Zeitgeist 伺服器。 快速記筆記 快速記筆記... 最近更改 最近更改... 最近更改的頁面(_C)... 即時重新格式化wiki之標示語法 削除 移除全部 移除鏈結 重新命名頁面 "%s" 替換全部(_A) 替換為 恢復頁面至原先儲存之版本？ 版本 右側窗格 儲存版本(_A) 另存副本(_C) 儲存副本 儲存版本 儲存書籤 zim 儲存之版本 相符 尋找 搜尋反向鏈結(_B)... 選擇檔案 選擇文件夾 選擇圖像 選擇一個版本，以檢視該版本與目前版本的差異
或選擇多個版本，以檢視各版本間的差異
 選擇匯出格式 選擇匯出檔案或文件夾 選擇欲匯出之頁面 選擇視窗或區域 選取範圍 伺服器未啟動 伺服器已啟動 伺服器已停止 設定新名稱 設定預設文字編輯器 設爲當前頁 浮動顯示目錄（代替側面板） 顯示更動(_C) 一本筆記本一個圖示 顯示完整頁面名稱 顯示於工具列 於「不允許編輯」之頁面上，仍然顯示游標 單頁(_p) 大小 執行"%s"時發生錯誤 依英文字母排序 按標簽排序頁面 拼字檢查 啟動 Web 伺服器(_W) 符號(_m)... 系統預設 目錄 標籤 工作項目 工作清單 模板 範本 文字 文字檔案 從檔案加入文字(_F)... 文字背景顏色 文字的前景顏色 "文件夾\n"
"%s\n"
"不存在。\n"
"您想新增它嗎？" 文件夾"%s"不存在\n
你想新增它嗎? 最近儲存的版本以來，此筆記本至今尚無更動 已有此檔案
是否覆蓋？ 此頁面無附件目錄 此外掛程式會加入對話框，顯示此筆記本所有未完成之工作項目
此類項目包含未勾選之勾選框
或帶有 "TODO" 或 "FIXME" 等標籤之項目

此為 zim 內附之核心外掛程式
 此外掛程式會使用對話框，迅速而方便的
將某些文字或剪貼簿內容置入 zim 頁面

此為 zim 內附之核心外掛程式
 此外掛程式於系統匣增添 zim 圖示以方便存取

此外掛程式需依賴 Gtk+ 之 2.10 或較新之版本

此為 zim 內附之核心外掛程式
 此插件會在視窗中新增一個小部件，
顯示連結到當前頁面的所有頁面列表。

此插件為 zim 自帶的核心插件。
 此外掛程式提供「加入符號」之對話框
方便自動輸入特殊符號

此為 zim 內附之核心外掛程式
 此外掛可在 zim 中嵌入算數計算。
此外掛基于
http://pp.com.mx/python/arithmetic
中的算數模塊。
 此外掛程式以 GraphViz 為基礎，提供圖表編輯器

此為 zim 內附之核心外掛程式
 此外掛程式提供對話框
以圖像顯示筆記本之鏈結結構
以「心像圖覽」方式
呈現頁面之交互關係

此為 zim 內附之核心外掛程式
 此外掛依據標簽雲中的所選標簽，生成頁面索引\n
 此外掛程式以 GNU R 為基礎，提供數據圖表設計語法編輯器
 該插件提供一個基于Gnuplot的繪圖編輯器
 此外掛程式為尚無列印支援的 zim 提供解決之道
程式將匯出目前頁面為 html 並開啟瀏覽器程式
若瀏覽器具備列印支援
則此方法可利用上述兩個步驟(匯出頁面、開啟瀏覽器)
讓您得以列印您的資料

此為 zim 內附之核心外掛程式
 此外掛程式提供 latex 之方程式編輯器


此為 zim 內附之核心外掛程式
 這個插件會在底欄顯示附件資料夾的圖示
 通常指此檔案包含了無效的特殊字元 標題 欲繼續作業，您可儲存本頁面之副本，或捨棄任何更動
若儲存副本，仍會捨棄更動
但可於稍後回復副本 請選擇一個空資料夾來創建新筆記本.
當然你可以選擇已有筆記本的資料夾.
 目錄 今天(_D) 今天 切換成打勾之勾選框'V' 切換成打叉之勾選框'X' 頂部窗格 系統匣圖示 將頁面名稱作為任務項的標簽 文件類型 以 <BackSpace> 鍵去除縮排
(若不勾選，仍可用 <Shift><Tab> 來執行) 未知狀態 沒有標籤 更新連結至此書頁共 %i 頁之頁面 更新此頁面之標題 更新鏈結 更新索引 使用自訂字體 每個一個頁面 以 <Enter> 鍵追蹤鏈結
(若不勾選，仍可用 <Alt><Enter> 來執行) 版本控制 此筆記本尚未啟用版本控制功能
需啟用嗎？ 版本 檢視註解(_A) 觀看日誌(_L) 網頁伺服器 週 提交錯誤時請包含以下文字框中的信息 尋找完整單字(_W) 寬度 維基頁面：%s 字數統計 字數統計... 字 年 昨天 你正使用外部程式編輯檔案，當你完成編輯後再關閉對話視窗。 您可以自訂工具使其顯示於
工具選單、工具列、和滑鼠右鍵選單中 Zim 桌面維基 縮小(_O) 關於(_A) 所有窗格(_A) 算術(_A) 回返(_B) 瀏覽(_B) 錯誤回報(_B) 子頁面(_C) 清除格式(_C) 關閉視窗 折疊所有(_C)內容 內容(_C) 複製(_C) 複製至此(_C) 刪除(_D) 移除頁面(_R) 捨棄更動(_D) 編輯(_E) 編輯鏈結(_E) 編輯鏈結或物件(_E)... (_E)編輯屬性 斜體字(_E) 常見問題(_F) 文件(_F) 尋找(_F)... 向前(_F) 全螢幕(_F) 前往(_G) 幫助(_H) 以高亮度標示(_H) 歷史(_H) 主頁面(_H) 圖像(_I)... 匯入頁面(_I) 插入(_I) 前往(_J)... 快捷鍵(_K) 鏈結(_L) 鏈結日期(_L) 鏈結(_L)... 底線字(_M) 更多(_M) 移動(_M) 移動到此處(_M) 下一個(_N) 無(_N) 正常大小(_N) 順序(_N)列表 開啟檔案或鏈結(_O) 開啟別的筆記本(_O)... 其它(_O)... 頁面(_P) 頁面層級(_P) 父頁面(_P) 貼上(_P) 預覽(_P) 上一個(_P) 列印至瀏覽器(_P) 快速記筆記(_Q) 退出(_Q) 最近頁面(_R) 取消還原(_R) 正規表達式(_R) 重新載入(_R) 移除鏈結(_R) 替換(_R) 取代(_R)... 重設大小(_R) 回復版本(_R) 儲存(_S) 儲存副本(_S) 螢幕快照(_S)... 搜尋(_S) 搜尋(_S)... 傳送至...(_S) 側窗格(_S) 並列比較(_S) 排序(_S) 刪除線(_S) 粗體字(_S) 下標體(_S) 上標體(_S) 範本(_T) 工具(_T) 還原(_U) 去格式字(_V) 所有版本(_V) 檢視(_V) 放大(_Z) calendar:week_start:1 唯讀 秒 Launchpad Contributions:
  DreamerC https://launchpad.net/~dreamerwolf
  Henry Lee https://launchpad.net/~henrylee
  Jaap Karssenberg https://launchpad.net/~jaap.karssenberg
  Takashi Kinami https://launchpad.net/~sloanej
  Thomas Tsai https://launchpad.net/~thomas.tsai
  Tsung-Hao Lee https://launchpad.net/~tsunghao
  YPWang https://launchpad.net/~blue119
  Zihao Wang https://launchpad.net/~wzhd
  cyberik https://launchpad.net/~cyberikee 