��    �     �              �-  ,   �-  .   
.  ?   9.     y.     �.     �.     �.     �.  0   �.  0   #/     T/     o/  5   �/     �/  	   �/     �/  %   �/  i   0  *   r0     �0     �0     �0     �0     �0  
   �0  -   �0     "1  V   *1     �1  	   �1  	   �1     �1  3   �1     �1     �1  Y   2     u2     �2  .   �2  
   �2     �2     �2     �2     3     3     $3     13  	   83     B3  -   Q3  *   3  $   �3  ?   �3  /   4  (   ?4     h4  %   �4  ,   �4     �4  	   �4     �4     5  
   5     5  	   !5     +5     85     E5     L5     X5     _5  
   l5     w5     �5  �   �5  �   6     �6     �6     �6     �6  
   �6     �6     7     7     17     D7     W7     g7     ~7  .   �7  <   �7     �7  	   �7  
   	8     8     8     %8     58     R8     Z8     p8     �8     �8     �8  +   �8  3   �8      !9     B9     G9  	   Z9     d9     r9  
   ~9     �9     �9     �9     �9     �9     :  3   #:     W:     u:     �:     �:     �:     �:     �:     �:     ;     
;     ;     %;     2;     7;     H;     L;  0   T;     �;     �;     �;     �;  
   �;     �;     �;     �;     �;     �;     �;     <     <  )   1<  $   [<  ~   �<     �<     =  
   =     =     $=  
   5=  	   @=  
   J=     U=     b=     j=     {=     �=     �=     �=  5   �=     �=     �=     >  !   
>     ,>  #   =>     a>     t>     {>     �>     �>     �>     �>     �>  #   �>     ?     -?  .   L?     {?     �?     �?     �?     �?     �?  
   �?     �?     �?  	   @  6   @     F@  v   M@     �@  *   �@  c   A     eA     mA     tA  
   {A     �A     �A     �A     �A     �A  
   �A     �A  
   �A  
   �A  
   �A  
   B  
   B  
   B     )B     0B     KB     kB  	   �B     �B     �B     �B     �B     �B     �B  %   �B     �B     �B  #   	C     -C     >C  
   DC     OC     aC     sC     �C     �C     �C     �C     �C     �C     �C     �C     �C     D     %D     3D     @D  	   VD     `D     rD     zD     �D     �D     �D  �   �D     uE     �E     �E     �E     �E     �E     �E     �E     �E     �E     �E  2   �E     -F     5F     >F     XF     aF     |F     �F     �F     �F     �F     �F      G  	   G     G     G     !G     7G     OG     aG     vG     �G     �G      �G  *   �G     �G     �G     H     H     H     0H     @H     EH     [H     yH  *   �H  2   �H     �H     �H     	I     "I     9I     JI  	   SI     ]I     `I     wI     �I     �I     �I     �I     �I  
   �I     �I  	   J     J      J     4J     BJ     VJ     eJ     nJ     �J     �J  9   �J     �J  I   �J  !   +K  '   MK  	   uK     K     �K  w   �K  �   L  0   �L  
   �L  	   �L     �L     �L  &   �L      M     5M     LM  	   YM     cM     lM     rM     �M  '   �M  V   �M  3   N  0   AN  (   rN     �N     �N  .   �N     �N     �N     �N     O  !   O  !   >O     `O     lO     qO     �O     �O  
   �O  &   �O  
   �O     �O     �O     �O     P     P     :P  
   AP     LP     ZP  
   mP     xP     �P     �P     �P  ?   �P     �P     Q     Q     )Q     HQ     LQ     RQ     bQ     xQ     �Q     �Q     �Q  	   �Q     �Q     �Q     �Q     �Q     �Q     �Q     R     R     /R     7R     LR     `R     lR     zR  �   �R     S      0S     QS     lS  	   �S     �S     �S     �S     �S     �S     �S     �S     T     T     .T     KT     ]T  2   uT     �T  &   �T     �T  $   �T     U     0U     EU     YU     mU      �U     �U  !   �U     �U  5   �U  &   &V     MV     ZV     _V     eV  &   tV     �V     �V     �V     �V     �V     �V  
   �V     �V     W  
   'W     2W     9W  	   HW     RW     XW     eW     wW     |W     �W  	   �W     �W     �W  	   �W     �W  
   �W     �W     �W     �W     X  F    X  ?   gX  A   �X  �  �X  S   �Z  =   [     ?[  K   E[  )   �[  @   �[  6   �[  -   3\  F   a\  I   �\  x   �\  �   k]  �   ^  �   �^  �   `_  �   �_  }   o`  L   �`  �   :a  9   �a  �   �a  �   �b  �   3c  }   �c  C   @d  s   �d  �   �d  h   �e  k   7f  �   �f  -   wg  R   �g  ;   �g  =   4h  v   rh    �h  j   �i  n   hj  ]   �j     5k  �   �k  7   Jl     �l  �   �l  |   $m     �m     �m     �m     �m     �m     �m     �m     n     n     #n     ,n     9n     Gn     Kn  
   Tn  9   _n  	   �n  (   �n  '   �n     �n     �n  D   o     Po     Xo     jo     xo     �o  H   �o     �o     �o     p  !   p     6p     Hp     \p     xp  +   �p  P   �p     q     "q     +q  U   ;q     �q     �q  	   �q     �q  
   �q     �q  N   �q     "r     .r     4r  �   Br  
   �r     s     s     s  	   !s  ^   +s  f   �s  �   �s     �t  	   �t     �t     �t  
   �t     �t     �t     �t     �t     �t     �t  	   �t     u     u      u     'u  	   5u     ?u  
   Eu     Pu     Xu  
   eu     pu     �u     �u  
   �u     �u     �u     �u  	   �u     �u     �u     �u     �u     �u     v     v     v  
   v     "v     +v  	   1v     ;v     Kv     Sv     Yv     ev  /   rv     �v     �v     �v     �v     �v     �v  
   �v     �v     �v     �v     w     w     w     .w     4w     Aw     Pw     Tw     Zw  	   tw     ~w     �w     �w     �w     �w  	   �w     �w     �w     �w     �w     �w     �w     x     x     x     +x     3x     ;x     Kx     Xx     ex     rx  
   �x     �x     �x     �x     �x     �x  
   �x     �x     �x  
   �x     �x     y     y      y     ,y     4y  
   <y     Gy  
   Ty     _y     fy  	   ly     vy     �y     �y     �y     �y     �y  
   �y     �y     �y      z     z     z     z     2z  
   Az     Lz  !  _z  D   �|  6   �|  T   �|     R}     ^}  %   z}  %   �}  #   �}  3   �}  /   ~  )   N~  .   x~  9   �~     �~     �~     �~        f   )  9   �     �     �     �     �  8   !�     Z�  6   n�     ��  �   ��  
   ?�     J�     W�  !   f�  L   ��     Ձ  %   �  Y   �     r�     ��  /   ��  
   Ƃ     т     �     ��     �     $�     1�     ?�     G�     ^�  8   t�  8   ��     �  *   ��  8   &�  :   _�  (   ��  !   Ä  ;   �     !�     @�     R�     l�     y�     ��     ��     ��     ��     υ     ԅ  
   ��     �     ��     �     "�  �   *�  �   ��     ��     ��     ��     Ӈ  
   ��      �  "   �  "   /�  "   R�  "   u�     ��  $   ��     ؈  @   �  K   /�     {�     ��  	   ��     ��  
   ��     ��  %   ĉ     �     ��     �     �     4�     P�  :   m�  Z   ��  -   �  	   1�     ;�     Q�     `�     r�     ��     ��  $   ��  $   Ӌ  -   ��  !   &�     H�  6   f�  0   ��     Ό  !   �     �     !�  '   7�     _�     w�  
   ��     ��     ��     ��     č     ˍ     ލ     �  3   �     '�     F�     T�     b�     y�     ��     ��     ��  
   ��     ��     ��      Ҏ  *   �  <   �  3   [�  �   ��     -�  
   @�     K�     \�      m�     ��     ��     ��     Ԑ     �     ��  !   	�     +�     H�     V�  B   _�     ��     ��     ȑ  '   Ց     ��  )   �     =�     U�     \�  0   u�     ��     ��  "   ϒ     �  1   �     :�     Q�  2   n�     ��  +   ��      �  	   �     �     �     &�     <�     N�  	   a�  J   k�     ��  s   ��     0�  .   C�  �   r�  
   ��  	   ��     	�  
   �  #   �     ?�     ]�     a�     i�     w�     ��     ��  
   ��  
   ��  
   ��  
   Ö  
   Ζ  	   ٖ      �  +   �     0�  	   I�     S�     f�     k�     r�      ��     ��  5   ��     �     �  9   �     ;�     P�     V�     b�     z�     ��     ��     Ƙ     ژ     �     
�      �     3�     M�     a�     ~�     ��     ��     Ι     �     �     �     "�     )�     6�     O�    g�     i�     ��     ��     ��  &   ��     ޛ     �     ��     �     �     �  R   ,�     �     ��  /   ��  	   ��  !   ʜ  #   �     �  4   -�  )   b�     ��     ��  	   ȝ  	   ҝ     ܝ     �     �  $   �     4�     M�  #   f�     ��     ��  K   ��  M   �     ?�  	   Y�     c�     k�     {�     ��     ��     ��  4   Ɵ     ��  9   �  T   M�     ��     ��      Π     �     �     '�     5�     E�  "   M�     p�     ��     ��     ��     ֡     �     �     �     =�     P�     h�     ��  '   ��     ��     آ     �     �     �  b   �     }�  p   ��  6   ��  /   4�     d�     q�     y�  q   �  �   �  .   ��  
   ��     ��     ǥ     ӥ  /   �     !�     >�     W�  	   f�     p�     x�     ��     ��  -   ��  R   ئ  .   +�  2   Z�     ��     ��     ħ  H   ҧ     �     *�     /�     I�  D   Y�  9   ��     ب     �     �     �     �     -�  3   <�     p�     ~�     ��     ��     ĩ  4   �     �     (�     @�  +   W�     ��     ��     ��      ��     ݪ  J   �     >�     P�     ]�  +   w�     ��     ��     ��     ȫ     �     �      �     �     -�     ?�     R�     i�     ~�     ��     ��     ��     Ŭ     �     �     �     !�     3�     F�  �   Z�  "   ��  *   �  '   G�     o�     ��     ��     ��     ��     Ю     �  +   ��  *   '�  1   R�     ��  2   ��      ӯ  .   ��  /   #�     S�  A   k�  %   ��  4   Ӱ  %   �  "   .�      Q�     r�     ��  5   ��  "   �  .   �  &   6�  ;   ]�  5   ��     ϲ     �     �     ��  #   ��      �      9�     Z�     m�     ��     ��     ��     ��     ��     ۳  	   �     ��     �  
   *�     5�     K�     T�  "   ]�     ��     ��  	   ��     ��     ��     ��     ��     Ǵ     ݴ     �     �  F   �  9   Z�  -   ��  �  µ  M   ��  N   �     ?�  B   E�  3   ��  4   ��  1   �  2   #�  L   V�  M   ��  �   �  �   ��  �   P�  �   E�  �   �  �   ��  �   f�  _   �  �   t�  A   $�  �   f�  �   H�  �   ��  �   ��  X   :�  �   ��  ,  +�  �   X�  v   ��  �   Z�  2   Q�  \   ��  B   ��  G   $�  �   l�  :  �  t   L�  �   ��  N   P�  �   ��  �   "�  M   ��     �  �   �  �   ��     ��     ��     ��  "   ��  "   ��  "   ��  "   	�  !   ,�  !   N�     p�     |�     ��     ��     ��     ��  L   ��     �  >   �  4   [�     ��     ��  R   ��  
   ��     	�     "�     6�     G�  _   T�  &   ��     ��     ��  )   �  !   1�     S�  +   j�  ,   ��  @   ��  g   �     l�     ��     ��  Z   ��     �     �     )�  #   >�     b�     s�  K   x�     ��     ��     ��  �   ��     ��     ��     ��     ��     �  d   	�  {   n�  �   ��     ��     ��  	   ��     ��      �     �     �     *�     9�     E�     M�     U�     g�     }�  	   ��     ��  	   ��  
   ��     ��  	   ��     ��     ��     ��     �     %�     3�  +   N�     z�     ��     ��     ��     ��  	   ��     ��     ��     ��     ��     �  
   �     �     &�     3�     <�     R�     ^�     f�     w�  0   ��     ��     ��     ��     ��     �  
   �     �     +�     =�     P�     ]�     k�     x�     ��     ��     ��     ��     ��     ��  
   ��     ��     ��     �      �     .�     :�     C�     Z�     f�     ��     ��  
   ��     ��     ��     ��     ��     ��     ��     �     $�     ;�  #   Q�     u�     ��  	   ��     ��     ��     ��     ��     ��  	   ��     �     �     5�     C�     W�     i�     u�     ��     ��  	   ��     ��     ��     ��     ��     ��     ��     ��     �     (�     =�     L�     `�     p�     ��  
   ��     ��     ��  
   ��     ��   		This plugin provides bar for bookmarks.
		 %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i Attachment %i Attachments %i BackLink %i BackLinks %i _Backlink %i _Backlinks %i errors occurred, see log %i file will be deleted %i files will be deleted %i file will be trashed %i files will be trashed %i open item %i open items %i warnings occurred, see log (Un-)indenting a list item also changes any sub-items <Top> <Unknown> A desktop wiki A file with that name already exists. A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Action Add Application Add Bookmark Add Notebook Add bookmark/Show settings Add column Add new bookmarks to the beginning of the bar Add row Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files All Tasks Allow public access Always use last cursor position when opening a page Always wrap at character Always wrap at word boundaries An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Are you sure you want to delete the file '%s'? Arithmetic Ascii graph (Ditaa) Attach File Attach image first Attachment Browser Attachments Attachments: Author Auto
Wrap Auto indenting Automatically collapse sections on close page Automatically expand sections on open page Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave interval in minutes Autosave version on regular intervals Autosave version when the notebook is closed Back to Original Name BackLinks BackLinks Pane Backend Backlinks: Bazaar Bookmarks BookmarksBar Border width Bottom Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Cannot write this file. Probably this is due to the lenght
of the file name, please try using a name with less
than 255 characters Cannot write this file. Probably this is due to the lenght
of the file path, please try using a folder structure resulting in less
than 4096 characters Capture whole screen Center Change columns Changes Characters Characters excluding spaces Check Checkbox '<' Check Checkbox '>' Check Checkbox 'V' Check Checkbox 'X' Check _spelling Check and Update Index Checkbo_x List Checking a checkbox also changes any sub-items Classic trayicon,
do not use new style status icon on Ubuntu Clear Clone row Code Block Column 1 Command Command Palette Command does not modify data Comment Common include footer Common include header Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Copy Email Address Copy Line Copy Template Copy _As... Copy _Link Copy _Location Copy _link to this location Copy link to clipboard Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Created Cu_t Custom Tools Custom _Tools Customize... Date Date and Time... Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Demote Dependencies Description Details Diagram Discard note? Display line numbers Distraction Free Editing Do not use system trash for this notebook Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root Due E_xport... Edit %s Edit Custom Tool Edit Image Edit Link Edit Table Edit _Source Editing Editing file: %s Enable Version Control? Enable plugin Enabled Equation Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File exists, do you want to import? File is not writable: %s File name should not be blank. File name should not contain path declaration. File name too long: %s File path too long: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Fossil GNU R Plot Get more plugins online Get more templates online Git Gnuplot Go back Go forward Go to home page Grid lines Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide Journal pane if empty Hide menubar in fullscreen mode Highlight current line Home Page Horizontal _Line Icon Icons Icons & Text Icons & Text horizontal Id Id "%s" not found on the current page Images Import Page Include horizontal lines in the ToC Include subpages Index Index page Inline Calculator Insert Code Block Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Sequence Diagram Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Key Binding Key bindings Key bindings can be changed by clicking on a field with a key combination
in the list and then press the new key binding.
To disable a keybinding, select it in the list and use <tt>&lt;Backspace&gt;</tt>. Labels marking tasks Large Last Modified Leave Fullscreen Leave link to new page Left Left Side Pane Line Sorter Lines Link Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Managing table columns Map document root to URL Match _case Maximum number of bookmarks Maximum page width Menubar Mercurial Modified Month Move Selected Text... Move Text to Other Page Move column ahead Move column backward Move page "%s" to trash? Move text to Name Need output file to export MHTML Need output folder to export full notebook Never wrap lines New File New Page New Page in %s New S_ub Page... New _Attachment Next No Applications Found No changes since last version No dependencies No document root defined for this notebook No plugin available to display objects of type: %s No such file: %s No such page: %s No such wiki defined: %s No templates installed No text selected Notebook Notebooks OK Only Show Active Tasks Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open cell content link Open help Open in New Window Open in New _Window Open new page Open plugins folder Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" and all of it's sub-pages and
attachments will be deleted.

This deletion is permanent and cannot be un-done. Page "%s" and all of it's sub-pages and
attachments will be moved to your system's trash.

To undo later, go to your system's trashcan. Page "%s" does not have a folder for attachments Page Index Page Name Page Template Page already exists: %s Page is read-only and cannot be edited Page not allowed: %s Page not available: %s Page section Paragraph Password Paste Paste As _Verbatim Path Bar Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please select a row, before you push the button. Please select more than one line of text Please specify a notebook Plugin Plugin "%s" is required to display this object Plugins Port Position in the window Pr_eferences Prefer short link names for pages Prefer short names for page links Preferences Prev Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Remove column Remove links to %s Remove row Removing Links Rename file Rename or Move Page Rename page "%s" Repeated clicking a checkbox cycles through the checkbox states Replace _All Replace with Require authentication Restore page to saved version? Rev Right Right Side Pane Right margin position Row down Row up S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Screenshot Command Search Search _Backlinks... Search this section Section Section(s) to ignore Section(s) to index Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Sequence Diagram Server not started Server started Server stopped Set New Name Set ToC fontsize Set default browser Set default text editor Set to Current Page Show BackLink count in title Show Line Numbers Show Tasks as Flat List Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show due date in sidepane Show edit bar along bottom of editor Show full Page Name Show full Page Names Show full page name Show helper toolbar Show in the toolbar Show linkmap button in headerbar Show right margin Show tasklist button in headerbar Show tasklist in sidepane Show the cursor also for pages that can not be edited Show the page title heading in the ToC Single _page Size Small Smart Home key Some error occurred while running "%s" Sort alphabetically Sort pages by tags Source View Spell Checker Start Start _Web Server Su_bscript Su_perscript Support thumbnails for SVG Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Tags for non-actionable tasks Task Task List Tasks Template Templates Text Text Files Text From _File... Text background color Text foreground color Text wrap mode The file "%s" exists but is not a wiki page.
Do you want to import it? The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The following parameters will be substituted
in the command when it is executed:
<tt>
<b>%f</b> the page source as a temporary file
<b>%d</b> the attachment directory of the current page
<b>%s</b> the real page source file (if any)
<b>%p</b> the page name
<b>%n</b> the notebook location (file or folder)
<b>%D</b> the document root (if any)
<b>%t</b> the selected text or word under cursor
<b>%T</b> the selected text including wiki formatting
</tt>
 The inline calculator plugin was not able
to evaluate the expression at the cursor. The table must consist of at least on row!
 No deletion done. Theme There are no changes in this notebook since the last version that was saved There was a problem loading this plugin

 This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This page name cannot be used due to a conflicting file in the storage This page name cannot be used due to technical limitations of the storage This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a "path bar" to the top of the window.
This "path bar" can show the notebook path for the current page,
recent visited pages or recent edited pages.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds the page index pane to the main window.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows inserting 'Code Blocks' in the page. These will be
shown as embedded widgets with syntax highlighting, line numbers etc.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin also has properties,
see the notebook properties dialog This plugin cannot be enabled due to missing dependencies.
Please see the dependencies section below for details.

 This plugin opens a search dialog to allow quickly executing menu entries. The search dialog can be opened by pressing the keyboard shortcut Ctrl+Shift+P which can be customized via Zim's key bindings preferences. This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a macOS menubar for zim. This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a sequence diagram editor for zim based on seqdiag.
It allows easy editing of sequence diagrams.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This plugin turns one section of the notebook into a journal
with a page per day, week or month.
Also adds a calendar widget to access these pages.
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox '<' Toggle Checkbox '>' Toggle Checkbox 'V' Toggle Checkbox 'X' Toggle _Editable Toggle editable Tool Bar Toolbar size Toolbar style Top Top Pane Trash Page Trash failed, do you want to permanently delete instead ? Tray Icon Try wrap at word boundaries or character Turn page name into tags for task items Type Un-check Checkbox Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unkown Image type Unkown Object Unspecified Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use a page for each Use date from journal pages Use font color for dark theme Use horizontal scrollbar (may need restart) Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Use thumbnails Username Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log View debug log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s With this plugin you can embed a 'Table' into the wiki page. Tables will be shown as GTK TreeView widgets.
Exporting them to various formats (i.e. HTML/LaTeX) completes the feature set.
 Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Your system encoding is set to %s, if you want support for special characters
or see errors due to encoding, please ensure to configure your system to use "UTF-8" Zim Desktop Wiki Zoom _Out _About _Add _All Panes _Arithmetic _Attachment... _Back _Browse _Bugs _Cancel _Checkbox _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Delete... _Discard Changes _Duplicate Line _Edit _Edit Link _Edit Link or Object... _Edit Properties _Edit... _Emphasis _FAQ _File _Find _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump _Jump To... _Keybindings _Limit search to the current page and sub-pages _Link _Link to date _Link... _Mark _More _Move _Move Here _Move Line Down _Move Line Up _New Page Here... _New Page... _Next _Next in Index _None _Normal Size _Numbered List _OK _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Previous in Index _Print _Print to Browser _Properties _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove _Remove Heading _Remove Line _Remove Link _Remove List _Rename or Move Page... _Rename... _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In as due date for tasks as start date for tasks calendar:week_start:0 do not use horizontal lines macOS Menubar no grid lines readonly seconds translator-credits vertical lines with lines {count} of {total} Project-Id-Version: Zim 0.48
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2021-10-04 11:12+0000
Last-Translator: Mukli Krisztián <weblate@rejtettmail.eu.org>
Language-Team: Hungarian <https://hosted.weblate.org/projects/zim/master/hu/>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.9-dev
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
X-Poedit-Bookmarks: 566,-1,-1,-1,-1,-1,-1,-1,-1,-1
 		Ez a kiegészítő megjeleníti a könyvjelzők eszköztárát.
		 %(cmd)s
kilépéskor hibakódot adott vissza: %(code)i %(n_error)i hibát és %(n_warning)i figyelmeztetést észleltünk, lásd a logokban %A %d %B %Y %i melléklet %i melléklet %i visszacsatolás %i visszacsatolás %i visszacsatolás %i visszacsatolás %i hiba történt, lásd a logokban %i fájlt fogok törölni %i fáljt fogok törölni %i fájl fog törlődni %i fájl fog törlődni %i db nyitott tétel %i db nyitott tétel %i figyelmeztetés történt, lásd a logokban A listaelemek behúzása az alelemeket is megváltoztatja <Teteje> <Ismeretlen> Egy asztali wiki Ilyen nevű fájl már létezik. Ilyen névvel már van fájl: <b>"%s"</b>
Használj más nevet, vagy felülírhatod a létező fájlt. A táblázatnak legalább egy oszlopot tartalmaznia kell. Művelet Alkalmazás hozzáadása Könyvjelző hozzáadása Jegyzetfüzet hozzáadása Könyvjelző hozzáadása/Beállítások megjelenítése Oszlop hozzáadása Új könyvjelző hozzáadása az eszköztár elejéhez Sor hozzáadása Ez a kiegészítő helyesírás ellenőrzést tesz lehetővé a gtkspell segítségével.

Ez a kiegészítő szerves része a Zim-nek.
 Igazítás Minden fájl Minden feladat Publikus elérés engedélyezése A kurzor visszaállítása a legutóbbi helyzetébe egy oldal megnyitásakor Mindig karakternél törjön Mindig a szavak határainál törjön Hiba történt a kép készítése közben.
Így is el akarod menteni a forrásszöveget? Állapot lap forrása Alkalmazások Biztos, hogy törölni szeretné a '%s' fájlt? Aritmetika Ascii ábra (Ditaa) Fájl csatolása Előbb csatold a képet Mellékletböngésző Mellékletek Mellékletek: Szerző Automatikus
sortörés Automatikus behúzás Automatikusan zárja össze a szakaszokat a bezár lapon Automatikusan nyissa ki a szakaszokat a megnyitott lapon Automatikusan mentve Formázáskor az aktuális szót formázza A "CamelCase" szavak automatikusan hivatkozások lesznek A fájl elérési utak automatikusan hivatkozások lesznek Automatikus mentés megadott percenként Változások automatikus mentése Automatikus változásmentés a jegyzetfüzet bezárásakor Eredeti név visszaállítása Visszacsatolások Visszacsatolások panelja Kiszolgáló Visszacsatolások: Bazaar Könyvjelzők Könyvjelző eszköztár szegély szélesség Lent Alsó panel Tallózás _Felsorolás _Beállítás "%s" nem módosítható lap Mégsem Ez a fájl nem írható. Lehetséges, hogy ezt a fájlnév
hossza okozza, próbálj meg olyan fájlnevet használni
amely 255 karakternél rövidebb Ez a fájl nem írható. Lehetséges, hogy ezt a fájl elérési
útjának hossza okozza, próbálj meg olyan könyvtárstruktúrát használni
amely 4096 karakternél rövidebb elérési utakat eredményez Az egész képernyő mentése Középre zárt Oszlopok módosítása Változások Karakterek Karaketerek szóközök nélkül Jelölőnégyzet beállítása '<' Jelölőnégyzet beállítása '>' Jelölőnégyzet beállítása 'V' Jelölőnégyzet beállítása 'X' _Helyesírás ellenőrzés Index ellenőrzése és frissítése _Jelölőgombos lista A jelölőnégyzet bejelölése megváltoztatja az alelemeket is Klasszikus értesítési ikon,
nem használ új típusú ikont Ubuntu alatt Törlés Sor másolása Kódblokk Oszlop 1 Utasítás Utasítás kereső A parancs nem módosítja az adatokat Megjegyzés Általános lábléc Általános fejléc Teljes jegyzetfüzet Alkalmazások beállítása Kiegészítők beállítása Alkalmazás beállítása "%s" hivatkozás megnyitásához Olyan alkalmazások beállítása, amik képesek megnyitni
egy "%s'" típusú hivatkozást Minden jelölőnégyzet feladatnak tekintése Másolás Levélcím másolása Sor másolása Sablon másolása Másolás _mint... _Hivatkozás másolása He_ly másolása Hivatkozás másolása erre a helyre Hivatkozás másolása a vágólapra Nem található a "%s" futtatható állomány Nem található jegyzetfüzet: %s Nem található a "%s" sablon Nem található az jegyzetfüzet fájlja vagy mappája Nem lehet betölteni a helyesírás-ellenőrzőt %s nem nyitható meg Nem tudtam elemezni a kifejezést Nem sikerült beolvasni: %s Lap nem menthető: %s Új lap létrehozása minden jegyzethez Létrehozzuk a mappát? Létrehozva _Kivágás Egyedi eszközök Egyéni eszközök Testreszabás... Dátum Dátum és idő... Nap Alapértelmezett Vágólapra másoláskor alapértelmezett formátum Alapértelmezett jegyzetfüzet Késleltetés Lap törlése "%s" lap törölhető? Sor törlése Lejjebb sorol Függőségek Leírás Részletek Diagram Jegyzet eldobása? Sorok számának megjelenítése Szerkesztés a figyelem elvonása nélkül Ne használja a rendszer lomtárát ehhez a jegyzetfüzethez Biztos, hogy minden könyvjelzőt törölni akarsz? Vissza kívánod állítani ezt a lapot: %(page)s
erre a mentett változatra: %(version)s ?

Az utolsó mentés óta történt változások el fognak veszni! Dokumentumgyökér Határidő _Exportálás... %s szerkesztése Egyéni eszközök szerkesztése Kép szerkesztése Hivatkozás szerkesztése Táblázat szerkesztése _Forrás szerkesztése Szerkesztés %s szerkesztése Engedélyezed a verziókezelést? Bővítmény engedélyezése Engedélyezve Egyenlet Hiba a %(file)s -ban, a %(line)i sorban, "%(snippet)s" közelében _Math kiértékelése Mindent _kinyit Exportálás Összes oldal exportálása egy fájlba Az exportálás kész Oldalanként külön fájlba exportálás Jegyzetek exportálása Hibás Indítás sikertelen: %s Nem sikerült elindítani a(z) "%s" alkalmazást A fájl már létezik _Fájl sablonok... Az alábbi fájl megváltozott: %s A fájl már létezik Ez a fájl már létezik, szeretnéd felülírni? Fájl nem írható: %s A fájlnév nem lehet üres. A fájlnév nem tartalmazhat elérési útvonalat. A fájlnév túl hosszú: %s A fájl elérési útvonal túl hosszú: %s Nem támogatott fálj típus: %s Fájlnév Szűrő Keresés Következő _találat Elő_ző találat Keresés és csere Mit keres A hétvége előtt megjelöli a hétfőn vagy kedden esedékes feladatokat Mappa A mappa már létezik, és nem üres. Az exportálás visszavonhatatlanul felülírhat fájlokat! Mégis folytatod? Létező mappa: %s Csatolt sablonfájlokat tartalmazó könyvtár A kibővített keresésben használhatók legyenek olyan operátorok, mint
AND, OR és NOT. Nézd meg a súgót a részletekért. _Formátum Formátum Fossil GNU R Plot További kiegészítők letöltése További sablonok letöltése Git Gnuplot Ugrás vissza Ugrás előre Ugrás a kezdőlapra Vonalak Címsor _1 Címsor _2 Címsor _3 Címsor _4 Címsor _5 Magasság Naptár panel rejtése, ha üres Menü elrejtése teljes képernyős módban Aktuális sor kiemelése Kezdőlap Vízszintes _vonal Ikon Ikonok Ikonok és szöveg Ikonok és szöveg vízszintesen Azonosító "%s" azonosító nem található az aktuális oldalon Képek Lap importálása Vízszintes vonalak megjelenítése a tartalomjegyzékben Aloldalakkal együtt Index Index oldal Beépített kalkulátor Kódblokk beillesztése Dátum és idő beillesztése Diagram beillesztése Ditaa beillesztése Egyenlet beillesztése GNU R Plot beillesztése Gnuplot beillesztése Kép beillesztése Hivatkozás beillesztése Kotta beillesztése Képernyőkép beillesztése Szekvenciadiagram beillesztése Szimbólum beillesztése Táblázat beszúrása Szöveg beillesztése fájlból Felhasználói felület Interwiki kulcsszó Naptár Ugrás Ugrás lapra _Billentyűkombinációk Billentyűkombinációk A billentyűkombinációk megváltoztathatóak, ha rákattintasz a megfelelő műveletre
a listában és lenyomod az új billentyűkombinációt.
A billentyűkombináció törléséhez válasszad ki a listából és nyomd le a <tt>&lt;Backspace&gt;</tt> -t. A megjelölt feladatok címkéi Nagy Módosítva Teljes képernyő elhagyása Hagyja meg az új oldalra hivatkozást Balra zárt Baloldali panel Sorrendező Sorok Hivatkozás Hivatkozástérkép A dokumentumgyökérben található fájlok hivatkozása a teljes elérési úttal Hivatkozás Hely Események naplózása a Zeitgeisten keresztül Log fájl Úgy tűnik találtál egy hibát Legyen alapértelmezett alkalmazás Táblázatoszlopok kezelése A dokumentumgyökér hozzáadása a hivatkozásokhoz Kis- és nagy_betű megkülönböztetése Könyvjelzők maximális száma Legnagyobb oldalszélesség Menüsáv Mercurial Módosítva Hónap Kijelölt szöveg mozgatása... Szöveg átmozgatása másik oldalra Oszlop mozgatása előre Oszlop mozgatása hátra "%s" lap áthelyezése a lomtárba? Szöveg áthelyezése Név Kimeneti fájl megadása szükséges az MHTML-be történő exportáláshoz Kimeneti mappa megadása szükséges a teljes jegyzetfüzet exportálásához Sose törje meg a sorokat Új fájl Új lap Új lap itt: %s Új aloldal... Új _melléklet Következõ Alkalmazások nem találhatóak Nem történt módosítás az utolsó változat óta Nincsenek függőségek Nincs dokumentumgyökér megadva ehhez a jegyzetfüzethez Nincs elérhető bővítmény ennek a típusú objektumnak a megjelenítéséhez: %s Nincs ilyen fájl: %s Nincs ilyen oldal: %s Ilyen wiki nincs definiálva: %s Nincsenek telepített sablonok Nincs szöveg kijelölve Jegyzetfüzet Jegyzetfüzetek Rendben Csak az aktív feladatokat mutatja Mellékletek mappa megnyitása Könyvtár megnyitása Jegyzetfüzet megnyitása Megnyitás mással... _Dokumentum gyökere Jegyzetfüzet mappa megnyitása _Oldal megnyitása Cella tartalmának megnyitása Súgó megnyitása Megnyitás új ablakban Megnyitás új _ablakban Új oldal létrehozása Bővítmények mappájának megnyitása Megnyitás ezzel: "%s" Opcionális %s kiegészítő beállítása Más… Kimeneti fájl A kimeneti fájl már létezik, az erőltetett exportáláshoz add meg az "--overwrite" kapcsolót Kimeneti mappa A kimeneti mappa már létezik és nem üres, az erőltetett exportáláshoz add meg az "--overwrite" kapcsolót Kimeneti hely megadása szükséges az exportáláshoz Az aktuális kiválasztás cseréje a kimenetre Felülírás _Helyek Oldal "%s" oldal, aloldalai és
mellékletei törlésre kerülnek.

Ez a törlés végleges, és nem lehet visszavonni. "%s" oldal, annak összes aloldala és
mellékletei a rendszer lomtárába kerülnek.

A későbbi visszavonáshoz lépjen a rendszer lomtárába. A "%s" lapnak nincs mappája a mellékletekhez Oldalindex Lap neve Oldalsablon Ez az oldal már létezik: %s Az oldal csak olvasható és nem szerkeszthető Oldal nem engedélyezett: %s Oldal nem elérhető: %s Oldal szekció Bekezdés Jelszó Beillesztés Beszúrás, mint _kód Útvonal sáv Írj be egy megjegyzést ehhez a változathoz A hivatkozások által mutatott nem létező lapok
jöjjenek létre automatikusan. Válassz nevet és mappát a jegyzetfüzethez. Válassz ki egy sort, mielőtt megnyomod a gombot. Válassz több szövegsort Adj meg egy jegyzetfüzetet Kiegészítő "%s" bővítmény szükséges ennek az objektumnak a megjelenítéséhez Bővítmények Port Ablakon belüli pozíció _Beállítások Az oldalak rövid neveinek előnyben részesítése hivatkozásokban Rövid nevek előnyben részesítése az oldal linkjeihez Tulajdonságok Elõzõ Megjelenítés a böngészőben Feljebb sorol _Tulajdonságok Tulajdonságok Az eseményeket a Zeitgeist démonnak továbbítja. Gyors jegyzet Gyors jegyzet... Legfrissebb változások Legfrissebb változások... Legutóbb _módosított oldalak Wiki jelölőnyelv átalakítása használat közben Eltávolítás Összes eltávolítása Oszlop eltávolítása Hivatkozás eltávolítása a(z) %s oldalra Sor törlése Hivatkozások törlése Fájl átnevezése Lap átnevezése vagy mozgatása "%s" lap átnevezése Ismételt kattintással lehet a jelölőnégyzet kijelzéseit változtatni Ö_sszes cseréje Mire cserél Hitelesítés szükséges Visszaállítja a lap mentett változatát? Vissza Jobbra zárt Jobboldali panel Jobb oldali margó pozíció Mozgatás le Mozgatás fel Változat _mentése... Más_olat mentése... Másolat mentése Változat mentése Könyvjelzők mentése Automatikusan mentve Kotta Képernyőkép parancsa Keresés _Visszacsatolások keresése... Keresés ebben a szakaszban Szakasz Szakasz amit ki kell hagyni Szakasz amit indexelni kell Válasszon fájlt Válasszon mappát Kép kiválasztása Válaszd ki azt a változatot, amelyet össze akarsz hasonlítani a jelenlegi állapottal!
Vagy válassz ki többet, hogy összehasonlítsd őket egymással!
 Válassz exportálási formátumot Válaszd ki a kimeneti fájlt vagy mappát Válaszd ki az exportálandó fájlokat Ablak vagy terület kijelölés Kijelölés Szekvenciadiagram Szerver nem fut Szerver elindítva Szerver leállítva Új név beállítása Tartalomjegyzék betűméretének megadása Alapértelmezett böngésző beállítása Alapértelmezett szövegszerkesztő beállítása Aktuális lap beállítása Hivatkozások számának megjelenítése a címben Sorok számának megjelenítése Feladatok megjelenítése egyszerű listaként A tartalomjegyzéket lebegő panelként mutatja _Változások mutatása Különböző ikon használata minden megnyitott jegyzetfüzethez Mutassa a határidőt az oldalpanelon Szerkesztősáv megjelenítése a szerkesztő alján Oldal teljes nevének megjelenítése Teljes oldal nevek megjelenítése Teljes oldalnév megjelenítése Eszköztár megjelenítése Megjelenítés az eszköztáron Hivatkozástérkép-gomb megjelenítése a fejlécben Jobb oldali margó megjelenítése Feladatlista gomb megjelenítése a fejlécben Feladatlista mutatása oldalpanelként A kurzor akkor is látszódjon, ha a lap nem szerkeszthető Oldal címének megjelenítése a tartalomjegyzékben Önálló _oldal Méret Kicsi Okos Home gomb Hibák voltak, a "%s" futtatásakor Rendezés abc sorrendben Sorba rendezés címkék szerint Forráskód nézet Helyesírás-ellenőrző Kezdet _Web szerver indítása Alsó index Felső index SVG indexképek támogatása _Szimbólumok... Szintaxis A rendszer alapértelmezése Tabulátor-szélesség Táblázat Táblázatszerkesztő Tartalom Címkék Nem indítható feladatok címkéi Feladat Feladatlista Feladatok Sablon Sablonok Szöveg Szöveg fájlok Szöveg _fájlból... Szöveg háttérszín Szöveg színe Sortörési mód A "%s" fájl már létezik, de nem wiki oldal.
Szeretnéd importálni? A(z) 
%s
könyvtár még nem létezik.
Létrehozzuk most? A "%s" mappa nem létezik.
Létrehozzuk most? A következő paraméterek lesznek hozzáadva
az utasításhoz, amikor az fut:
<tt>
<b>%f</b> az oldal forrása, mint ideiglenes fájl
<b>%d</b> az aktuális oldal mellékleteinek könyvtára
<b>%s</b> az oldal forrása (ha létezik)
<b>%p</b> az oldal neve
<b>%n</b> a jegyzetfüzet helye (fájl vagy könyvtár)
<b>%D</b> a dokumentumgyökér (ha létezik)
<b>%t</b> a kiválasztott szöveg, vagy a kurzor alatti szó
<b>%T</b> a kiválasztott szöveg wiki formázással
</tt>
 A beépített kalkulátor nem tudta
kiértékelni a kifejezést a kurzornál. A táblázatnak legalább egy sort tartalmaznia kell!
 Nem történt törlés. Téma Nincsenek változások az jegyzetfüzetben az utolsó mentés óta Hiba történt a bővítmény betöltése közben

 Talán a megfelelő szótárak 
nincsenek telepítve Ez a fájl már létezik!
Szeretnéd felülírni? Ennek a lapnak nincs könyvtára a mellékletekhez Ez az oldalnév nem használható a tárolóban lévő ütköző fájl miatt Ez a név nem használható az oldalhoz, a tároló technikai korlátai miatt Ez a bővítmény lehetővé teszi, hogy közvetlenül
beillesszünk egy képernyőképet egy Zim oldalba.

Ez a bővítmény szerves része a Zimnek.
 Ez a bővítmény egy útvonal sávot ad az ablak felső részéhez.
Ebben a sávban látható az aktuális lap, a gyakran látogatott oldalak
vagy az utoljára szerkesztett oldalak elérési útja
 Ez a kiegészítő az adatbázisba felvett nyitott feladatolat jeleníti
meg egy dialógus ablakban. A nyitott feladatokat az üres
jelölőnégyzetek és a "TODO", "FIXME" kezdetű sorok jelentik.

Ez a kiegészítő szerves része a Zim-nek.
 Ez a kiegészítő egy párbeszédablakot ad egy szöveg
vagy a vágólap tartalmának gyors beszúrásához.

Ez a kiegészítő szerves része a Zim-nek.
 Ez a kiegészítő egy ikont hoz létre az értesítési területen a gyors eléréshez.

Működéséhez legalább 2.10-es GTK+ szükséges.

Ez a kiegészítő szerves része a Zim-nek.
 Ez a kiegészítő megjelenít egy külön panelt ami azokat
az oldalakat sorolja fel, ahol hivatkozás található az aktuális oldalra.

Ez egy alapvető bővítmény, a Zimmel együtt települt.
 Ez a bővítmény egy extra funkciót biztosít,
ami az aktuális oldal tartalomjegyzékét mutatja.

Ez egy alapvető bővítmény, a Zimmel együtt került telepítésre.
 Ez a bővítmény átváltoztatja a Zim-et
olyan szerkesztővé, ami nem vonja el a figyelmet.
 Ez a kiegészítő szimbólumok kódba való beszúrását,
és a tipográfiai jelek automatikus formázását teszi lehetővé.

Ez a kiegészítő szerves része a Zim-nek.
 Ez a bővítmény hozzáadja az oldalindex panelt a főablakhoz.
 Ez a bővítmény változáskövetést biztosít a jegyzetfüzetekhez.

Támogatja a Bazaar, a Git, a Mercurial és Fossil verziókövető rendszereket.

Ez egy alapvető bővítmény, a Zimmel együtt került telepítésre.
 Ez a kiegészítő kódblokkok beillesztését teszi lehetővé az oldalakba, beágyazott panel
formájában. Lehetőség van kódszínezésre, sorszámok megjelenítésére, stb.
 Ezzel a bővítménnyel aritmetikai számításokat végezhetsz a Zim-ben.
Az alábbi helyen található aritmetikai modulra épül:
http://pp.com.mx/python/arithmetic.
 Ezzel a bővítménnyel gyorsan ki lehet értékelni
egyszerű matematikai kifejezéseket a Zim-ben.

Ez a bővítmény szerves része a Zim-nek.
 A bővítmény további beállításaihoz,
lásd a jegyzetfüzet Tulajdonságok ablakát Ez a bővítmény nem engedélyezhető a hiányzó függőségek miatt.
Kérjük, olvassa el a függőségek részt az alábbiakban a részletekért.

 Ez a bővítmény megnyit egy keresési párbeszédpanelt, amely lehetővé teszi a menübejegyzések gyors végrehajtását. A keresési párbeszédpanel a Ctrl+Shift+P billentyűkombináció megnyomásával nyitható meg, amely a Zim gyorsbillentyűinek beállításain keresztül testre szabható. Ez a bővítmény egy diagram szerkesztő, mely a Ditaa-n alapul.

Ez egy alapvető bővítmény, a Zimmel együtt került telepítésre.
 Ez a kiegészítő a GraphViz segítségével diagramokat ad a kódhoz.

Ez a kiegészítő szerves része a Zim-nek.
 Ez a kiegészítő a linkek szerkezeti struktúrájának
grafikus megjelenítését teszi lehetővé.
Használható egyfajta "elmetérképként",
megjelenítve a bejegyzések közötti kapcsolatokat.

Ez a kiegészítő szerves része a Zim-nek.
 Ez a bővítmény egy macOS menüsort ad a zimhez. Ez a bővítmény lehetővé teszi az oldalak szűrését egy címkefelhő segítségével.
 Ez a kiegészítő a GNU R segítségével rajzokat ad a kódhoz.
 Ez a bővítmény grafikonokat jelenít meg a Gnuplot segítségével.
 Ez a kiegészítő egy, a seqdiagon-on alapuló szekvenciadiagram-szerkesztőt ad hozzá a zim-hez.
Szekvenciadiagramok egyszerű szerkesztését teszi lehetővé.
 Ez a kiegészítő egy megkerülése a Zim-ből hiányzó
nyomtatási lehetőségnek. Ki tudja exportálni az
aktuális lapot egy böngészőbe, ahonnan annak
nyomtatási lehetőségét használva nyomtatható
az anyag. Így a nyomtatás két lépésből megoldható.

Ez a kiegészítő szerves része a Zim-nek.
 Ez a kiegészítő a latex segítségével egyenleteket ad a kódhoz.

Ez a kiegészítő szerves része a Zim-nek.
 Ez a bővítmény egy kottaszerkesztő, ami a GNU Lilypond-on alapul.

Ez egy alapvető bővítmény, a Zimmel együtt került telepítésre.
 Ez a kiegészítő a mellékletek mappát ikonként mutatja
az alsó panelon.
 Ez a bővítmény sorba rendezi a kijelölt sorokat.
Már rendezett sorokon fordított sorrendben teszi ugyanezt
(A-Z, majd Z-A).
 Ez a kiegészítő a jegyzetfüzet egy részét átalakítja naplóvá,
oldalakkal a napokhoz, hetekhez vagy hónapokhoz.
Valamint naptár panelt is ad az oldalakhoz.
 Ennek oka általában az, hogy a fájl nem megengedett karaktereket tartalmaz Cím Ahhoz, hogy folytasd a munkát, vagy elmented a fájlt egy másolatba,
vagy eldobod a változásokat. Ha a másolat készítést választod,
a változások akkor is elvesznek, de a másolatből később visszaállíthatod. Új jegyzetfüzet létrehozásához válassz egy üres mappát.
Természetesen egy már létező jegyzetfüzet-mappát is választhatsz.
 Tartalom _Mai dátumra Ma Jelölőnégyzet beállítása '<' Jelölőnégyzet beállítása '>' Jelölőnégyzet beállítása 'V' Jelölőnégyzet beállítása 'X' Szerkeszthetőség átkapcsolása Szerkeszthetőség átkapcsolása Eszköztár Eszköztár méret Eszköztár stílus Fent Felső panel Lap törlése A lomtárba helyezés meghiúsult, szeretnéd inkább véglegesen törölni? Tálca ikon Próbáljon meg a szavak határainál vagy karakternél törni Az oldal neve legyen címke a feladatlista számára Típus Jelölés törlése Behúzás csökkentése <Backspace>-szel
(Ellenkező esetben marad a <Shift><Tab>) Ismeretlen Ismeretlen képformátum Ismeretlen objektum Meghatározatlan Nincs címke Az erre az lapra mutató %i oldal frissítése Az erre az lapra mutató %i oldalak frissítése Frissítsd az aktuális lap fejlécét Hivatkozás frissítése Index frissítése %s használata az oldalsávra váltáshoz Használj egyéni betűkészletet Darabonként egy oldal Használja a dátumot a naptár oldalakról Betűszín használata sötét téma esetén Vízszintes görgetősáv használata (újraindítást igényel) Használd az <Enter>-t a hivatkozások követéséhez
(Ha letiltod az <Alt><Enter> marad használatban) Indexképek használata Felhasználónév Változáskövetés A változáskezelés nincs bekapcsolva ehhez a jegyzetfüzethez.
Be akarod kapcsolni most? Változatok Mutasd a_z állapotokat _Logok megtekintése Hibakeresési nap_ló megtekintése Web kiszolgáló Hét Ha jelented ezt a hibát, kérlek csatold
a hibajelentéshez az alábbiakat _Egész szó Szélesség Wiki oldal: %s Ez a kiegészítő táblázatok beszúrását teszi lehetővé a wiki oldalakba. A táblázatok GTK TreeView widgetként fognak megjelenni.
A különböző formátumokba (pl. HTML/LaTeX) exportálhatóság egészíti ki a funkciókat.
 Szavak száma Szavak száma... Szavak Év Tegnap Egy fájlt egy külső alkalmazással szerkesztesz. Ezt az ablakot akkor zárd be, amikor kész vagy Szerkesztheted az eszközöket, amik megjelenhetnek
az Eszközök menü alatt, és az eszköztáron, vagy a helyi menüben. A rendszered karakterkódolása %s, ha speciális karaktereket is szeretnél használni
vagy karakterkódolási hibákkal találkozol, akkor bizonyosodj meg róla, hogy a rendszered az "UTF-8" karakterkódolást használja Zim asztali wiki _Kicsinyítés _Névjegy Hozzá_adás _Minden panel _Aritmetika Mellékletek... Ugrás _vissza _Tallózás _Hibák _Mégse _Jelölőnégyzet Egy szinttel _lejjebb F_ormázás törlése Bezárás Mindent össze_zár _Tartalom _Másolás _Másolás ide _Törlés Lap tö_rlése _Törlés... Változások _eldobása Sor duplikálása S_zerkesztés Hivatkozás _szerkesztése _Hivatkozás vagy objektum szerkesztése... _Tulajdonságok szerkesztése _Szerkesztés... _Dőlt _GYIK _Fájl _Keresés _Keresés a lapon... Ugrás _előre Teljes képernyő _Ugrás _Súgó _Kiemelés _Előzmények _Kezdőlapra _Kép... Lap _importálása... _Beszúrás _Ugrás _Ugrás lapra... _Billentyűkombinációk Keresés csak a jelenlegi oldalon és aloldalain _Hivatkozás _Csatolás a dátumhoz _Hivatkozás... _Megjelölt _Több infó M_ozgatás _Áthelyezés ide Sor mozgatása le Sor mozgatása fel Új oldal... Új _oldal... _Következő Indexben a _következőre _Nincs _Normál méret Számozott _lista _OK _Megnyitás Jegyzetfüzet _megnyitása... _Egyéb... _Lap _Oldal hierarchia Egy _szintet feljebb Bei_llesztés _Előnézet _Előző Indexben az _előzőre _Nyomtatás Nyomtatás bön_gészővel Tulajdonságok _Gyors jegyzet... _Kilépés _Utoljára megnyitott Új_ra _Reguláris kifejezés _Frissítés _Eltávolítás Címsor törlése Sor eltávolítása Hivatkozás tö_rlése Hivatkozás törlése Lap átnevezése vagy mozgatása... Átnevezés... _Csere _Csere... Méret _visszaállítása Változat vi_sszaállítása _Mentés _Másolat mentése _Képernyőkép... _Keresés Általáno_s keresés... Küldés le_vélben... _Oldalpanelek _Összehasonlítás _Sorok rendezése Át_húzott _Félkövér _Alsó index _Felső index _Sablonok _Eszközök _Visszavonás _Kód _Változatok... _Nézet _Nagyítás mint határidőt a feladatokhoz mint kezdetet a feladatokhoz naptár:hét_eleje:0 nem használja vízszintes vonalak macOS menüsáv vonalak nélkül csak olvasható másodperc Mukli Krisztián függőleges vonalak vonalakkal {count} a {total}-ből 