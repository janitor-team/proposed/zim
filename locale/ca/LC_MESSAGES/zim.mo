��    k     t              �&  ,   �&  .   �&  ?   )'     i'     u'     �'  0   �'     �'     �'     (  	   (     ((  i   7(  *   �(     �(     �(     �(     �(  
   )  -   )     J)  V   R)     �)  	   �)  	   �)     �)  3   �)  Y   *     e*     {*  
   �*     �*     �*     �*     �*     �*     �*     �*  	   �*     +  $   +  ?   7+  /   w+  (   �+     �+  %   �+  ,   ,     @,  	   V,     `,     o,  
   w,     �,  	   �,     �,     �,     �,     �,     �,  
   �,     �,     �,     �,     -     -     "-  
   *-     5-     Q-     d-     w-     �-     �-  <   �-     �-  	   �-  
   �-     .     
.     .     /.     7.     M.     c.     v.     �.  +   �.  3   �.      �.     /     2/     @/  
   L/     W/     f/     �/     �/  3   �/     �/     0     #0     >0     Q0     i0     �0     �0     �0     �0     �0     �0     �0     �0     �0  0   �0     1      1     &1     21  
   D1     O1     V1     c1     o1     w1     1     �1  $   �1  ~   �1     J2     X2  
   \2     g2     o2  
   �2  	   �2  
   �2     �2     �2     �2     �2     �2     �2     �2  5   �2     33     B3     N3  !   U3     w3  #   �3     �3     �3     �3     �3     �3     4     4     /4     ;4     T4     p4     y4     �4  
   �4     �4     �4  	   �4  6   �4     �4  v   �4     o5  *   �5  c   �5     6     6     6  
   &6     16     I6     c6     g6  
   o6  
   z6  
   �6  
   �6  
   �6  
   �6     �6     �6     �6     �6  	   
7     7     %7     *7     17     =7     N7  
   T7     _7     q7     �7     �7     �7     �7     �7     �7     �7     �7     �7     8     8     58     C8     P8  	   f8     p8     �8     �8     �8     �8     �8     �8     �8     �8     �8     �8     �8  2   9     ;9     C9     L9     f9     o9     �9     �9     �9     �9     �9     �9     :  	   :      :     ):     /:     E:     ]:     o:     �:     �:      �:  *   �:     �:     �:     �:     ;     ;     ;     0;     N;  *   ^;  2   �;     �;     �;     �;     �;     <  	   <     !<     $<     ;<     T<     `<     n<     {<     �<  
   �<     �<  	   �<     �<     �<     �<     =     =     )=     2=     H=     Q=  9   ]=     �=  I   �=  !   �=  '   >  	   9>     C>     L>  0   Q>  
   �>  	   �>     �>     �>     �>     �>  	   �>     �>  '   �>  V   ?  3   q?  0   �?  (   �?     �?     @  .    @     O@     W@     \@     s@     �@     �@     �@     �@     �@  
   �@  &   �@  
   �@     �@     A     A     "A     :A     ZA  
   aA     lA  
   zA     �A     �A     �A     �A     �A     �A     �A     �A     �A     B     B     B     /B  	   ?B     IB     VB     eB     |B     �B     �B     �B     �B     �B     �B     �B     �B     C     C  �   C     �C      �C     �C     D  	   D     $D     5D     HD     WD     fD     sD     �D     �D     �D  2   �D     �D  &   
E     1E     EE     YE     mE     �E     �E  5   �E  &   �E     
F     F     F  &   +F     RF     fF     yF     �F     �F     �F  
   �F     �F     �F  	   �F     �F     �F     �F     �F      G     G  	   #G     -G     3G  	   <G     FG  
   KG     VG     iG     G  ?   �G  A   �G  �  H  S   �I  =   /J  K   mJ  @   �J  6   �J  -   1K  I   _K  x   �K  �   "L  �   �L  �   pM  �   �M  }   N  L   �N  �   JO  9   �O  �   P  �   �P  }   EQ  C   �Q  h   R  k   pR  �   �R  R   �S  ;   T  =   ?T  v   }T    �T  j   V  n   sV  ]   �V     @W  �   �W  7   UX     �X  �   �X  |   /Y     �Y     �Y     �Y     �Y     �Y     �Y     �Y  	   Z  '   Z     4Z     9Z  D   KZ     �Z     �Z     �Z     �Z     �Z  H   �Z     [     6[     E[  !   T[     v[     �[     �[  P   �[     	\  U   \     o\     x\  	   �\  
   �\     �\  N   �\     �\     �\     ]  �   ]  
   �]     �]     �]     �]  	   �]  ^   �]  f   Y^     �^  	   �^     �^     �^  
   �^     �^     �^     _     _     _  	   _     $_     +_     =_     D_  	   R_     \_  
   b_     m_     u_     �_     �_     �_  
   �_     �_     �_     �_  	   �_     �_     �_     �_     `     
`     `     `     #`  
   )`     4`     =`  	   C`     M`     ]`     e`     k`     w`     �`     �`     �`     �`     �`     �`  
   �`     �`     �`     �`     �`     �`     �`     a     a     a     a  	   5a     ?a     Ea     Ua     ]a     da  	   ma     wa     ~a     �a     �a     �a     �a     �a     �a     �a     �a     �a     �a      b     b     b     )b  
   /b     :b     Ib  
   Qb     \b     hb     tb     �b     �b     �b  
   �b     �b  
   �b     �b     �b  	   �b     �b     �b     �b     �b     
c     "c  
   8c     Cc     Tc     bc     kc     sc     �c  
   �c     �c  �  �c  8   �e  6   �e  U   �e     Of     [f  4   pf  :   �f     �f  5    g     6g     <g     Ig  j   _g  .   �g     �g     h     h  #   +h     Oh  +   ^h     �h  x   �h  
   i     i     +i     =i  D   Ui  X   �i     �i     j     j     &j     ;j     Mj     `j     rj     zj     �j     �j     �j  )   �j  ;   �j  B   k  ;   Pk  )   �k  4   �k  <   �k     (l     ?l     Nl     dl     ll     |l  	   �l     �l     �l     �l     �l     �l  
   �l  "   �l  
   
m     m     2m     :m     Km  
   Rm     ]m     xm     �m     �m     �m     �m  J   �m     9n     @n     Ln  	   Yn     cn     in  	   �n  !   �n     �n     �n     �n     �n  1   o  5   =o  '   so     �o     �o     �o     �o     �o  "   �o     p     =p  6   [p  +   �p     �p      �p     �p  )   q  (   6q     _q     xq     ~q     �q     �q     �q     �q     �q     �q  3   �q     r      r     'r     :r     Zr     gr     pr     ~r     �r     �r     �r     �r  "   �r  z   �r     hs  	   ws     �s     �s     �s     �s     �s     �s     �s     �s     �s  %   t     *t  	   ?t     It  6   Rt     �t     �t     �t  *   �t     �t  )   �t     u     /u     6u     Ju     iu     |u     �u     �u     �u     �u     v     v     v     v     /v     Cv     Vv  F   \v     �v  �   �v     4w  )   Ow  y   yw     �w     �w     x     	x  !   x      9x     Zx     ^x     fx     yx     �x     �x     �x     �x     �x  !   �x  /   �x     y     5y     Ey     Yy     _y     gy     zy     �y     �y     �y     �y     �y     �y     �y     z     $z     Az     Rz     fz     zz     �z     �z     �z     �z     �z     {     {     *{     0{     7{     I{     f{      {{     �{     �{     �{     �{     �{  @   �{     (|  	   5|     ?|     ]|     p|     �|     �|  !   �|  $   �|     }     #}     >}  	   M}  	   W}     a}     e}     }     �}     �}     �}     �}  8   �}  B   ~  
   W~     b~     o~     �~  	   �~     �~  &   �~     �~  ,   �~  <        W     n     �  #   �     �     �     �      �     �     �     -�     =�     I�     d�     ��     ��     ��     ��     ΀     �     ��     �     $�     -�     G�     P�  Q   b�     ��  c   ǁ  )   +�  /   U�     ��     ��     ��  2   ��     ߂     �     �     �     5�     L�  	   _�     i�  +   w�  ?   ��  )   �  8   �  .   F�      u�  	   ��  :   ��  
   ۄ     �     �     �     �     �     %�     <�     E�  
   Q�  #   \�     ��     ��     ��     ��     ȅ  %   �     �     �     "�     2�     B�     Z�     t�     ��  /   ��     Ɔ     ˆ     ц     ݆     �  
   ��     �     �     /�     A�     Q�     c�  
   }�     ��     ��     ��     Ƈ     ߇     �     ��     �     '�     ?�  �   V�  #   ܈  -    �  $   .�  $   S�  	   x�     ��     ��     ��     ȉ     ؉  %   �     �     )�  "   C�  1   f�     ��  !   ��     ͊     �     �     !�     ;�  +   L�  7   x�  /   ��     ��     ��     ��  ,   �     <�     S�     q�     ��     ��     ��     ��     ��     ƌ     �     ��     ��     �  	   !�  $   +�     P�     V�     g�  	   o�  
   y�     ��     ��     ��     ��     ƍ  4   Ս  7   
�  �  B�  ^   '�  =   ��  B   Đ  K   �  2   S�  :   ��  S   ��  �   �  �   ��  �   ��  �   Q�  �   �  �   ��  f   ?�  �   ��  G   ]�  �   ��  �   `�  �   �  U   v�  x   ̙  �   E�    ך  k   �  D   R�  A   ��  �   ٜ  M  j�  �   ��  �   F�  m   ̟  �   :�  �   Ǡ  1   v�     ��  �   ��  �   h�      �     �     �     �     %�     9�     M�     ]�  C   |�     ��     ƣ  P   أ  
   )�     4�     O�     b�     t�  V   ��  )   ڤ     �     �  &   5�  &   \�     ��     ��  d   ��     %�  V   9�     ��     ��     ��     Ȧ     զ  N   ݦ     ,�     =�     C�  �   U�     �     �     '�     ,�     0�  \   5�  u   ��     �     �     (�     1�     :�     L�  
   Y�     d�     m�     u�     ��     ��     ��     ��     ��  
   ��     ��     ȩ     թ     ީ     ��     	�     �     "�  !   3�     U�  	   g�     q�  
   }�     ��     ��  	   ��  	   ��     ��     ��     Ī  
   ˪  
   ֪     �  
   �     �  	   �     �  
   �     $�     8�     A�     S�     _�     g�     m�  
   r�     }�     ��     ��  	   ��  	   ��     ƫ     ӫ     �     ��     �     
�     �      �     7�     =�     F�  	   Y�  
   c�     n�     ��     ��     ��     ��     ��  	   ˬ     լ     ެ     �     �     �     �     0�     E�     K�     ^�     s�  	   z�     ��     ��     ��     ��     έ  	   ֭  
   �     �     ��     �     �  	   �     �     +�     7�     ?�  !   ^�     ��     ��     ��     ��     ̮     ۮ  h  �     K�     ]�     t�   		This plugin provides bar for bookmarks.
		 %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i Attachment %i Attachments %i errors occurred, see log %i file will be deleted %i files will be deleted %i open item %i open items %i warnings occurred, see log <Top> <Unknown> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Add Application Add Bookmark Add Notebook Add bookmark/Show settings Add column Add new bookmarks to the beginning of the bar Add row Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files All Tasks Allow public access Always use last cursor position when opening a page An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Arithmetic Ascii graph (Ditaa) Attach File Attach image first Attachment Browser Attachments Attachments: Author Auto
Wrap Auto indenting Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave interval in minutes Autosave version on regular intervals Autosave version when the notebook is closed Back to Original Name BackLinks BackLinks Pane Backend Backlinks: Bazaar Bookmarks BookmarksBar Border width Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Capture whole screen Center Change columns Changes Characters Characters excluding spaces Check Checkbox '>' Check Checkbox 'V' Check Checkbox 'X' Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Clone row Code Block Column 1 Command Command does not modify data Comment Common include footer Common include header Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Created Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Demote Dependencies Description Details Diagram Discard note? Distraction Free Editing Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root Due E_xport... Edit %s Edit Custom Tool Edit Image Edit Link Edit Table Edit _Source Editing Editing file: %s Enable Version Control? Enable plugin Enabled Equation Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Fossil GNU R Plot Get more plugins online Get more templates online Git Gnuplot Grid lines Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide Journal pane if empty Hide menubar in fullscreen mode Highlight current line Home Page Horizontal _Line Icon Images Import Page Include subpages Index Index page Inline Calculator Insert Code Block Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Sequence Diagram Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Labels marking tasks Last Modified Leave link to new page Left Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Managing table columns Map document root to URL Match _case Maximum number of bookmarks Maximum page width Menubar Mercurial Modified Month Move Selected Text... Move Text to Other Page Move column ahead Move column backward Move text to Name Need output file to export MHTML Need output folder to export full notebook New File New Page New S_ub Page... New _Attachment Next No Applications Found No changes since last version No dependencies No document root defined for this notebook No plugin available to display objects of type: %s No such file: %s No such page: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Only Show Active Tasks Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open cell content link Open help Open in New Window Open in New _Window Open new page Open plugins folder Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Index Page Name Page Template Page already exists: %s Page not allowed: %s Page section Paragraph Path Bar Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please select a row, before you push the button. Please select more than one line of text Please specify a notebook Plugin Plugin "%s" is required to display this object Plugins Port Position in the window Pr_eferences Preferences Prev Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Remove column Remove row Removing Links Rename page "%s" Replace _All Replace with Restore page to saved version? Rev Right Right Side Pane Right margin position Row down Row up S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Screenshot Command Search Search _Backlinks... Search this section Section Section(s) to ignore Section(s) to index Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Sequence Diagram Server not started Server started Server stopped Set New Name Set default text editor Set to Current Page Show Line Numbers Show Tasks as Flat List Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show full Page Name Show full page name Show helper toolbar Show in the toolbar Show right margin Show tasklist in sidepane Show the cursor also for pages that can not be edited Show the page title heading in the ToC Single _page Size Smart Home key Some error occurred while running "%s" Sort alphabetically Sort pages by tags Source View Spell Checker Start Start _Web Server Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Tags for non-actionable tasks Task Task List Tasks Template Templates Text Text Files Text From _File... Text background color Text foreground color The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The following parameters will be substituted
in the command when it is executed:
<tt>
<b>%f</b> the page source as a temporary file
<b>%d</b> the attachment directory of the current page
<b>%s</b> the real page source file (if any)
<b>%p</b> the page name
<b>%n</b> the notebook location (file or folder)
<b>%D</b> the document root (if any)
<b>%t</b> the selected text or word under cursor
<b>%T</b> the selected text including wiki formatting
</tt>
 The inline calculator plugin was not able
to evaluate the expression at the cursor. The table must consist of at least on row!
 No deletion done. There are no changes in this notebook since the last version that was saved This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This page name cannot be used due to technical limitations of the storage This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds the page index pane to the main window.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin also has properties,
see the notebook properties dialog This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a sequence diagram editor for zim based on seqdiag.
It allows easy editing of sequence diagrams.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This plugin turns one section of the notebook into a journal
with a page per day, week or month.
Also adds a calendar widget to access these pages.
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox '>' Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Tray Icon Turn page name into tags for task items Type Un-check Checkbox Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unkown Image type Unkown Object Unspecified Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use a page for each Use date from journal pages Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s With this plugin you can embed a 'Table' into the wiki page. Tables will be shown as GTK TreeView widgets.
Exporting them to various formats (i.e. HTML/LaTeX) completes the feature set.
 Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Zim Desktop Wiki Zoom _Out _About _Add _All Panes _Arithmetic _Back _Browse _Bugs _Cancel _Checkbox _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Duplicate Line _Edit _Edit Link _Edit Link or Object... _Edit Properties _Edit... _Emphasis _FAQ _File _Find _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _Move Here _Move Line Down _Move Line Up _New Page... _Next _None _Normal Size _Numbered List _OK _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Print _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove _Remove Line _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In as due date for tasks as start date for tasks calendar:week_start:0 do not use horizontal lines no grid lines readonly seconds translator-credits vertical lines with lines {count} of {total} Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2019-04-02 20:08+0000
Last-Translator: SNavas <snavas@gmail.com>
Language-Team: Catalan <ca@li.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
X-Generator: Launchpad (build bceb5ef013b87ef7aafe0755545ceb689ca7ac60)
 		Aquest connector proporciona una barra de marcadors
		 %(cmd)s
ha retornat un estat diferent de cero %(code)i Han ocorregut %(n_error)i errors i %(n_warning)i alertes, mirau el fitxer de registre %A %d %B %Y %i Adjunt %i Adjunts Han ocorregut %i errors, mirau el fitxer de registre El fitxer %i serà esborrat Els %i fitxers seran esborrats %i ítem obert %i ítems oberts Han ocorregut %i alertes, mirau el fitxer de registre <Rel> <Desconegut> Una wiki d'escriptori Ja existeix un fitxer amb el nom <b>"%s"</b>.
Podeu emprar un altre nom o sobreescriure el fitxer existent Una taula necessita tenir almenys una columna. Afegir aplicació Afegir marcador Crea un quadern Afegir marcador/Veure configuració Afegir columna Afegir nou marcador al principi de la barra Afegir fila Afegeix suport de corrector ortogràfic emprant gtkspell

Aquesta és una extensió bàsica que se subministra amb Zim.
 Alineament Tots els fitxers Totes les tasques Permetre accés públic Emprar sempre la darrera posició del cursor quan s'obri una pàgina S'ha produït un error mentre es generava la imatge.
Voleu desar el text font igualment? Font de la pàgina anotada Aplicacions Aritmètica Gràfic ASCII (Dita) Adjunta un fitxer Adjunta una imatge Navegador adjunts Adjunts Adjunts: Autor Auto
Tallar Sagnat automàtic Versió desada automàticament des de Zim Selecciona automàticament el mot actual en aplicar formats Converteix automàticament en enllaços els mots tipus "CamelCase" Converteix automàticament en enllaços les rutes a fitxers Interval de guardat automàtic, en minuts Desa automàticament la versió a intervals regulars Guarda automàticament una versió quan es tanqui el quadern Tornar al Nom Original Retroenllaços Panell retroenllaços Backend Retroenllaços: Bazaar Marcadors Barra de Marcadors Amplada de la vora Panell abaix Explora Llista de _Vinyetes C_onfigura No es pot modificar la pàgina: %s Cancel·la Captura la pantalla completa Centrat Canviar columnes Canvis Caràcters Caràcters excloent espais Marcar casella com a '>' Marcar casella com a 'V' Marcar casella com a 'X' Verificar l'ortografia Llista de _Controls Icona de notificació clàssica,
no empreu el nou tipus d'icona amb Ubuntu Neteja Clonar fila Bloc de codi Columna 1 Ordre L'ordre no modifica dades Comentari Incloure peu de pàgina principal Incloure capçalera principal Quader_n complet Configurar Aplicacions Configura l'extensió Triar una aplicació per obrir els enllaços "%s" Triar una aplicació per obrir fitxers
del tipus "%s" Considera totes les caixes com a feines Copia l'adreça de correu Copiar plantilla Copiar com _A... Copia l'_enllaç Copia la _ubicació No es pot trobar l'executable "%s" No s'ha trobat el quadern: %s No es troba la plantilla "%s" No s'ha trobat el fitxer o la carpeta d'aquest quadern No es pot carregar el corrector ortogràfic No es pot obrir: %s No es pot analitzar l'expressió No s'ha pogut llegir: %s No ha estat possible desar la pàgina: %s Crea una nova pàgina per cada anotació Voleu crear una carpeta? Creat _Talla Eines personalitzades _Eines personalitzades Personalitza... Data Dia Per defecte Format predeterminat per copiar text al portapapers Quadern per omissió Retard Elimina la pàgina Voleu eliminar la pàgina "%s"? Elimina fila Degradar Dependències Descripció Detalls Diagrama Descartar nota? Edició lliure de distraccions Vols esborrar tots els marcadors ? Voleu tornar a la versió %(version)s
de la pàgina %(page)s ?

Tots els canvis efectuats des que la vau desar es perdran! Document arrel Venciment E_xporta Edita %s Edita eina personalitzada Edita la imatge Edita l'enllaç Editar taula Edita el _codi Edició Editant fitxer: %s Voleu activar el control de versions? Habilita l'extensió Habilitat Equació Error a %(file)s a línia %(line)i aprop "%(snippet)s" Evaluar _Math Expandir _Tot Exporta Exporta totes les pàgines a un sol fitxer Exportació completada Exportar cada pàgina a un fitxer separat Exportant el quadern. Fallit Error executant: %s Error executant aplicació: %s El fitxer existeix Plan_tilles de fitxer Fitxer canviat al disc: %s El fitxer ja existeix Fitxer no es pot escriure: %s Tipus de fitxer no suportat: %s Nom de fitxer Filtre Troba Troba el _següent Troba el _precedent Cerca i reemplaça Troba Marca venciment de tasques a Dilluns o dimars abans del cap de setmana Carpeta La carpeta ja existeix i conté informació; si exporteu a aquesta carpeta us arrisqueu a sobreescriure alguns fitxers. Voleu continuar? la carpeta ja existeix: %s Carpeta amb plantilles de fitxers adjunts Per fer cerques elaborades, podeu emprar operadors com
AND, OR i NOT. Vegeu la pàgina d'ajuda si voleu més informació. For_mat Format Fossil Gràfic GNU R Obtenir més connectors en línia Baixar més plantilles en línia Git Gnuplot Línies de graella Encapçalat _1 Encapçalat _2 Encapçalat _3 Encapçalat _4 Encapçalat _5 Alçada Oculta panell Diari si està buit Oculta barra de menú al mode pantalla completa Ressalta línia actual Pàgina d'inici _Línia horitzontal Icona Imatges Importa la pàgina Incloure subpàgines Índex Pàgina índex Calculadora en línia Inserir bloc de codi Insereix la data i l'hora Insereix un diagrama Insertar Ditaa Insereix una equació Insereix un gràfic de GNU R Insertar Gnuplot Insereix una imatge Insereix un enllaç Insereix partitura Insereix captura de pantalla Inserir diagrama de sequència Insereix símbol Insereix taula Insereix text des d'un fitxer Interfície Paraula clau Interwiki Diari Vés a Vés a la pàgina Etiquetes que marquen feines Darrera modificació Deixar enllaç a la pàgina nova Esquerra Panell esquerra Classificador de línies Línies Mapa d'enllaços Enllaçau els documents del directori arrel amb la ruta completa Enllaça amb Ubicació Registra events amb Zeitgeist Fitxer de registre Sembla que has trobat un error Fes aplicació per defecte Gestió columnes taula Enllaçar pàgina rel amb una URL Distingeix majúscules i minúscules Màxim nombre de marcadors Amplada de pàgina màxima Barra de menú Mercurial Modificat Mes Moure text seleccionat... Moure text a una altra pàgina Moure columna endavant Moure columna enrera Moure text a Nom Es necessari un fitxer de sortida per a exportar a MHTML Es necessari un fitxer de sortida per a exportar el quadern sencer Nou Fitxer Pàgina nova S_ubpàgina nova Nou _Adjunt Següent No s'han trobat aplicacions Sense canvis des de la darrera versió Sense dependències No hi ha document arrel per a aquest quadern No existeix extensió per mostrar els objectes del tipus: %s No hi ha cap fitxer %s No existeix la pàgina: %s No hi ha wiki definida: %s No hi ha cap plantilla instal·lada Quadern Quaderns D'acord Mostrar solament tasques actives Obre la _carpeta d'adjunts Obre la carpeta Obre un quadern Obre amb... Obre l'_arrel del document Obre la carpeta de _quaderns Obrir _Pàgina Obrir enllaç de la cel·la Obre l'ajuda Obre a finestra nova Obre una _finestra nova Obrir pàgina nova Obrir carpeta de connectors Obrir amb "%s" Opcional Opcions de l'extensió %s Altre... Fitxer de sortida El fitxer de sortida existeix, especifica "--overwrite" per forçar l'exportació Carpeta de sortida La carpeta de sortida existeix i no està buida, especifica "--overwrite" per forçar l'exportació Es necessària una ubicació per exportar La sortida reemplaçarà la sel·lecció actual Sobreescriure B_arra d'adreces Pàgina La pàgina "%s" no té cap carpeta per als adjunts Index de la pàgina Nom de la pàgina Plantilla de pàgina La pàgina ja existeix: %s Pàgina no permesa: %s Secció de pàgina Paràgraf Barra de ruta Escriviu un comentari per a aquesta versió Enllaçant un pàgina que no existeix
la creeu automàticament. Trieu un nom i una carpeta per al quadern Per favor, selecciona una fila abans de polsar el botó. Per favor selecciona més d'una línia de text Per favor, especifica un quadern Extensió L'extensió "%s" és necessaria per mostrar aquest objecte Extensions Port Posició a la finestra _Preferències Preferències Previ Imprimeix al navegador Promoure Propie_tats Propietats Envia events al daemon de Zeitgeist Anotació ràpida Anotació ràpida... Canvis recents Canvis recents... Pàgines amb _Canvis recents Reformata el marcat de la wiky al vol Elimina Elimina-ho tot Elimina columna Elimina la fila Eliminant els enllaços Reanomena la pàgina "%s" Substitueix-ho tot Reemplaça per Voleu restaurar la pàgina a la versió desada? Rev. Dreta Panell dret Posició marge dret Baixar fila Pujar fila _Desa la versió... _Desa'n una còpia Desa'n una còpia Desa la versió Guardar marcadors Versió desada des de Zim Puntuació Comanda per capturar pantalla Cerca _Cerca els retroenllaços Cercar a aquesta secció Secció Secció/ns a ignorar Secció/ns a indexar Seleccioneu un fitxer Seleccioneu una carpeta Seleccioneu una imatge Seleccioneu una versió veure els canvis entre aquella i l'actual.
O seleccioneu diverses versions per veure els canvis entre elles.
 Seleccioneu el format d'exportació Seleccioneu el fitxer de sortida o la carpeta Seleccioneu les pàgines a exportar. Selecciona una finestra o una regió Selecció Diagrama de seqüències El servidor no s'ha engegat Servidor engegat Servidor aturat Estableix Nom nou Establir editor de text predeterminat Estableix pàgina actual Mostra números de línia Mostrar Tasques com a llista plana Mostra giny flotant IC enlloc del panell lateral. Mostra els _canvis Mostra una icona per cada quadern Mostra nom complet pàgina Mostrar nom de pàgina complet Veure barra eines d'ajuda Mostra a la barra d'eines Veure marge dret Mostrar llista de tasques al panell lateral Mostra el cursor fins i tot per a pàgines no editables Mostra la capçalera de títol de pàgina al IC _Pàgina única Mida Tecla inici intel·ligent Un error ha ocorregut quant s'executava "%s" Ordena alfabèticament Ordena pàgines per etiquetes Veure codi font Corrector ortogràfic Inici Engega el servidor _web Sí_mbol Sintaxi Valor per defecte del sistema Amplada pestanya Taula Editor de taules Índex de continguts Etiquetes Etiquetes per a tasques sense acció Feina Llista de feines Tasques Plantilla Plantilles Text Fitxers de text Text des d'un _fitxer... Color fons del text Color del text La carpeta
%s
no existeix encara.
La vols crear ara? La carpeta "%s" no existeix encara.
La voleu crear ara? Els següents paràmetres seran substituïts
a la comanda quan sigui executada:
<tt>
<b>%f</b> la pàgina original com a fitxer temporal
<b>%d</b> el directori d'adjunts de la pàgina actual
<b>%s</b> el fitxer de la pàgina original (si n'hi ha)
<b>%p</b> el nom de la pàgina
<b>%n</b> la ruta del quadern (fitxer o carpeta)
<b>%D</b> l'arrel del document (si n'hi ha)
<b>%t</b> el text o paraula seleccionat al cursor
<b>%T</b> el texte seleccionat incloent el formatat wiki
</tt>
 El connector de calculadora en línia no es capaç
d'avaluar l'expressió que hi ha al cursor. La taula ha de contenira almenys una fila !
No s'ha esborrat. No hi ha canvis en aquest quadern des de la darrera versió desada Això podria significar que no teniu els diccionaris
correctes instal·lats Aquest fitxer ja existeix.
El voleu sobreescriure? Aquesta pàgina no té una carpeta per als fitxers adjunts Aquest nom de pàgina no pot ser usat per limitacions tècniques del emmagatzematge Aquest connector permet fer una captura de pantalla i inserir-la directament dins
una pàgina de zim.

Aquest connector forma part del codi principal de zim
 Aquesta extensió afegeix un diàleg que mostra totes les feines
pendents del quadern, sigui caixes sense marcar, sigui llistes
marcades amb les etiquetes "TODO" o "FIXME".

Aquesta és una extensió bàsica que se subministra amb Zim.
 Aquesta extensió afegeix un diàleg per inserir ràpidament
text (o el contingut del portapapers a una pàgina.

Aquesta és una extensió bàsica que se subministra amb Zim.
 Aquesta extensió afegeix una icona a la safata del sistema
que permet l'accés ràpid.

Depèn de Gtk+, versió 2.10 o superior

Aquesta és una extensió bàsica que se subministra amb Zim.
 Aquest connector afegeix un widget extra que mostra la llista de pàgines 
que enllacen a la pàgina actual.

Aquest és un connector del codi principal de zim
 Aquest connector afegeix un giny extra d'índex de
continguts de la pàgina actual

Aquest connector forma part del codi principal de zim.
 Aquest connecttor afegeix configuracions per ajudar a emprar zim
com a editor lliure de distraccions.
 Aquesta extensió afegeix el diàleg 'Insereix un símbol' i permet
el formatat automàtic de caràcters tipogràfics.

Aquesta és una extensió bàsica que se subministra amb Zim.
 Aquesta extensió afegeix l'índex de pàgina a la finestra principal.
 Aquest connector afegeix control de versions pels quaderns.

Suporta els sistemes de control de versions Bazaar, Git i Mercurial.

Aquest connector forma part del codi principal de zim.
 Aquest connector us permet incloure càlculs aritmètics a zim.
Està basat en el mòdul aritmètic de
http://pp.com.mx/python/arithmetic.
 Aquest connector permet evaluar ràpidament expressions
matemàtiques simples a zim.

Aquest és un connector del codi principal de zim.
 Aquesta extensió també té propietats, 
veure el diàleg de propietats del quadern. Aquest plugin proveeix un editor de diagrames a zim basat en Ditaa.

Aquest és un connector del codi principal de zim.
 Aquesta extensió proporciona un editor de diagrames per a Zim basat en GraphViz.

Aquesta és una extensió bàsica que se subministra amb Zim.
 Aquesta extensió afegeix un diàleg amb una representació
gràfica de l'estructura d'enllaços del quadern. Es pot emprar
com una mena de mapa mental que mostra com les pàgines
es relacionen entre elles.

Aquesta és una extensió bàsica que se subministra amb Zim.
 Aquest connector proveeix un índex de pàgina filtrat mitjançant una selecció d'etiquetes en un núvol.
 Aquesta extensió proporciona un editor de gràfics basat en GNU R.
 Aquest connector proveeix un editor plot a zim basat en Gnuplot.
 Aquest connector proveeix un editor de diagrames de seqüències a zim basat en seqdiag.
Permet la fàcil edició de diagrames de seqüències.
 Aquesta extensió ofereix una solució alternativa  la manca
de suport d'impressió en Zim. S'exporta la pàgina actual
a HTML i s'obre en el navegador. Suposant que aquest disposi
de suport d'impressió, això us permetra d'imprimir les
vostres pàgines en dos passos.

Aquesta és una extensió bàsica que se subministra amb Zim.
 Aquesta extensió proporciona un editor d'equacions per a Zim basat en Latex.

Aquesta és una extensió bàsica que se subministra amb Zim.
 Aquest connector proveeix un editor de partitures a zim basat enGNU Lilypond.

Aquest connector forma part del codi principal de zim
 Aquest connector mostra la carpeta d'adjunts de la pàgina actual com
una vista d'icones al panell inferior.
 Aquest connector classifica les línies seleccionades en ordre alfabètic.
Si la llista ja està ordenada, s'inverteix l'ordre
(A-Z o Z-A).
 Aquest connector converteix una secció del quadern en un diari
amb una pàgina per dia, setmana o mes.
També afegeix un giny de calendari per accerdir a aquestes pàgines.
 Vol dir que el fitxer conté caràcters invàlids Títol Per continuar, podeu desar una còpia d'aquesta pàgina o descartar
els canvis. Si deseu una còpia, els canvis també seran descartats,
però podreu restaurar la còpia més endavant. Per crear un nou quadern necessitau seleccionar una carpeta buida.
Per suposat, també podeu seleccionar una carpeta on ja existeixi un quadern de zim
 ÍNDEX _Avui Avui Canvia casella '>' Canvia la marca 'V' Canvia la marca 'X' Panell superior Icona de la safata del sistema Conveteix el nom de pàgina en etiquetes pels ítems de les tasques Tipus Desmarcar casella Elimina el sagnat amb <Retrocés>
(Si està deshabilitat, pot emprar <Maj><Tab>) Desconegut Tipus d'imatge desconeguda Objecte desconegut Sense especificar Sense etiqueta Actualitza %i pàgina que enllaça aquesta Actualitza %i pàgines que enllacen aquesta Actualitza l'encapçalament de la pàgina Actualitzant els enllaços Actualitzant l'índex Empra %s per canviar al panell lateral Empra un tipus de lletra personalitzat Empra una pàgina per a cada Emprar data de pàgina de diari Empra la tecla <Entra> per seguir els enllaços.
(Si està deshabilitada, podeu emprar <Alt><Entra>) Control de versions El control de versions no es troba habilitat per a aquest quadern.
Voleu habilitar-lo? Versions Mostra les _anotacions Visua_litza el registre Servidor web Setmana Quan reportis aquest bug, per favor inclou
la informació de text d'aquí baix Paraula completa Ample Pàgina Wiki : %s Amb aquest connector es poden incrustar 'Taules' a les pàgines.  Seràn mostrades com a ginys GTK TreeView.
També es poden exportar amb varis formats (p.ex. HTML/LaTeX).
 Comptador de mots Compta els mots... Mots Any Ahir Estau editant un fitxer amb una aplicació externa. Podeu tancar aquest diàleg quan acabeu. Podeu configurar eines personalitzades que apareixeran
al menú d'eines i a la barra d'eines, o al menús contextuals Zim, wiki d'escriptori _Redueix _Quant a _Afegeix _Tots els Panells _Aritmètica _Precedent _Navegar _Errors _Cancel·la Capsa _Filla _Elimina el format _Tanca _Plega-ho tot _Contingut _Copia _Copia aquí _Elimina _Suprimeix la pàgina _Descarta els canvis Línia _Duplicada _Edita _Edita l'enllaç _Edita un enllaç o un objecte... _Edita propietats _Edita... _Emfasitzat _PMF (FAQ) _Fitxer _Cerca _Troba... _Següent _Pantalla completa _Vés A_juda _Ressaltat _Historial _Inici _Imatge... _Importa una pàgina _Insereix _Salta _Vés a... _Dreceres de teclat _Enllaç _Enllaça la data _Enllaç... _Marcat _Més _Mou _Mou aquí _Mou línia avall _Mou línia amunt _Nova pàgina... _Següent _Cap ni u Mida _normal Llista _numerada _D'acord _Obre _Obre un altre quadern Un _altre... _Pàgina Jerarquia de _Pàgines _Mare Engan_xa _Previsualització _Anterior _Imprimeix Im_primeix al navegador Nota _ràpida... _Surt Pàgines _recents _Refés Expressió _regular _Refresca _Elimina _Esborra línia _Elimina un enllaç _Reemplaça _Substitueix... _Restaura la mida _Restaura la versió _Desa _Desa'n una còpia _Captura de pantalla _Cerca _Cerca... En_via a... _Panells laterals _Un a costat de l'altre _Classifica línies _Tatxat _Destacat _Subíndex Su_períndex Plan_tilles _Eines _Desfés Text _pur _Versions... _Visualitza A_mplia com a venciment de les tasques com a data d'inici de les tasques calendar:week_start:1 no usar línies horitzontals sense graella de línies només lectura segons Launchpad Contributions:
  David Planella https://launchpad.net/~dpm
  Giorgio Grappa https://launchpad.net/~j-monteagudo
  Jaap Karssenberg https://launchpad.net/~jaap.karssenberg
  SNavas https://launchpad.net/~snavas
  Siegfried Gevatter https://launchpad.net/~rainct
  animarval https://launchpad.net/~animarval
  pataquets https://launchpad.net/~pataquets línies verticals amb graella de línies {count} de {total} 