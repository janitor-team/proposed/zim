��    �     �              �-  ,   �-  .   �-  ?   	.     I.     U.     r.     �.     �.  0   �.  0   �.     $/     ?/  5   ]/     �/  	   �/     �/  %   �/  i   �/  *   B0     m0     t0     �0     �0     �0  
   �0  -   �0     �0  V   �0     Q1  	   W1  	   a1     k1  3   1     �1     �1  Y   �1     E2     [2  
   h2     s2     �2     �2     �2     �2     �2     �2  	   �2     �2  -   �2  *    3  $   K3  ?   p3  /   �3  (   �3     	4  %   &4  ,   L4     y4  	   �4     �4     �4  
   �4     �4  	   �4     �4     �4     �4     �4     �4      5  
   5     5     05  �   75  �   �5     R6     g6     n6     }6  
   �6     �6     �6     �6     �6     �6     �6     7     7  .   .7  <   ]7     �7  	   �7  
   �7     �7     �7     �7     �7     �7     �7     8     '8     :8     Q8  +   b8  3   �8      �8     �8     �8  	   �8     9     9  
   9     *9     99     U9     l9     �9     �9  3   �9     �9     :     ):     D:     W:     o:     �:     �:     �:     �:     �:     �:     �:     �:     �:     �:  0   �:     &;     7;     =;     I;  
   [;     f;     m;     z;     �;     �;     �;     �;     �;  )   �;  $   �;  ~   !<     �<     �<  
   �<     �<     �<  
   �<  	   �<  
   �<     �<     =     =     =     4=     B=     J=  5   S=     �=     �=     �=  !   �=     �=  #   �=     >     >     >     />     M>     Y>     l>     �>  #   �>     �>     �>  .   �>     ?     3?     J?     f?     o?     v?  
   {?     �?     �?  	   �?  6   �?     �?  v   �?     e@  *   w@  c   �@     A     A     A  
   A     'A     ?A     YA     ]A     eA  
   mA     xA  
   �A  
   �A  
   �A  
   �A  
   �A  
   �A     �A     �A     �A     B  	   #B     -B     >B     CB     IB     VB     nB  %   qB     �B     �B  #   �B     �B     �B  
   �B     �B     C     C     )C     8C     EC     UC     gC     vC     �C     �C     �C     �C     �C     �C     �C  	   �C     D     D     D     #D     0D     <D  �   ID     E     +E     1E     ?E     PE     gE     lE     {E     �E     �E     �E  2   �E     �E     �E     �E     �E     F     F     6F     MF     fF     rF     �F     �F  	   �F     �F     �F     �F     �F     �F     G     G     0G     =G      BG  *   cG     �G     �G     �G     �G     �G     �G     �G     �G     �G     H  *   *H  2   UH     �H     �H     �H     �H     �H     �H  	   �H     �H     I     I     1I     =I     KI     XI     lI  
   �I     �I  	   �I     �I     �I     �I     �I     �I     J     J     %J     .J  9   :J     tJ  I   �J  !   �J  '   �J  	   K      K     )K  w   .K  �   �K  0   .L  
   _L  	   jL     tL     �L  &   �L     �L     �L     �L  	   �L     M     M     M     &M  '   /M  V   WM  3   �M  0   �M  (   N     <N     VN  .   ]N     �N     �N     �N     �N  !   �N  !   �N     O     O     O     #O     +O  
   7O  &   BO  
   iO     tO     �O     �O     �O     �O     �O  
   �O     �O  
   �O     P     P     !P     5P  ?   FP     �P     �P     �P     �P     �P     �P     �P     �P     Q     Q     Q     'Q  	   7Q     AQ     NQ     ]Q     tQ     zQ     �Q     �Q     �Q     �Q     �Q     �Q     �Q     �Q     R  �   R     �R      �R     �R     �R  	   S     S     -S     @S     OS     ^S     kS     |S     �S     �S     �S     �S     �S  2   T     6T  &   DT     kT     �T     �T     �T     �T     �T      �T     U  !   U     ?U  5   YU  &   �U     �U     �U     �U     �U  &   �U     V     V     +V     7V     EV     KV  
   ]V     hV     uV  
   �V     �V     �V  	   �V     �V     �V     �V     �V     �V     W  	   W     W     W  	   !W     +W  
   0W     ;W     NW     dW     zW  F   �W  ?   �W  A   X  �  RX  S   Z  =   jZ     �Z  K   �Z  )   �Z  @   $[  6   e[  -   �[  F   �[  I   \  x   [\  �   �\  �   {]  �   D^  �   �^  �   W_  }   �_  L   V`  �   �`  9   -a  �   ga  �   b  �   �b  }   +c  C   �c  s   �c  �   ad  h   7e  k   �e  �   f  -   �f  R   g  ;   ag  =   �g  v   �g    Rh  j   fi  n   �i  ]   @j     �j  �   k  7   �k     �k  �   �k  |   �l     
m     m     m     m     /m     Cm     Wm     km     |m     �m     �m     �m     �m     �m  
   �m  9   �m  	   n  (   n  '   5n     ]n     bn  D   tn     �n     �n     �n     �n     �n  H   �n     ?o     _o     no  !   }o     �o     �o     �o     �o  +   �o  P   +p     |p     �p     �p  U   �p     �p     q  	   q     q  
   ,q     7q  N   <q     �q     �q     �q  �   �q  
   fr     qr     r     �r  	   �r  ^   �r  f   �r  �   Zs     �s  	   t     t     t  
   $t     /t     ;t     Jt     Pt     Xt     ^t  	   ft     pt     wt     �t     �t  	   �t     �t  
   �t     �t     �t  
   �t     �t     �t     �t  
    u     u     #u     4u  	   =u     Gu     Lu     Ru     Xu     au     ju     vu     zu  
   �u     �u     �u  	   �u     �u     �u     �u     �u     �u  /   �u     v     v     v     (v     .v     4v  
   :v     Ev     Uv     cv     uv     �v     �v     �v     �v     �v     �v     �v     �v  	   �v     �v     �v     �v     w     w  	   w     w     2w     9w     Kw     Ww     fw     lw     zw     �w     �w     �w     �w     �w     �w     �w     �w  
   �w     �w     x     x     x     0x  
   6x     Ax     Px  
   Xx     cx     ox     {x     �x     �x     �x  
   �x     �x  
   �x     �x     �x  	   �x     �x     �x     �x     �x     y     )y  
   ?y     Jy     [y     iy     wy     �y     �y     �y  
   �y     �y  �  �y  D   �{  7   �{  T   5|     �|     �|      �|  "   �|      �|  3   }  3   G}  $   {}  #   �}  K   �}     ~     ~     #~  .   4~  �   c~  /   �~     !     (     >     V  .   l     �  /   �     �  �   �  
   ��     ��     ��     ��  A   ؀  #   �     >�  ]   [�     ��     Ӂ     ߁     �     �     �     )�     =�  	   K�     U�     [�     q�  A   ��  ?   Ă      �  K   %�  )   q�     ��  "   ��  2   ܃  B   �     R�     r�  !   ��     ��     ��     ��     ��     ̈́     ߄     �     �     	�     �     %�  $   4�  	   Y�     c�  �   �     x�  	   ��     ��     ��     ��          ۆ     ��     �     ,�     F�     d�     ��  ?   ��  O   Ї      �     '�  
   9�     D�     M�     T�     c�  	   ��     ��     ��     ��     Ո     �  @   �  I   G�  .   ��     ��     ɉ     �     ��      �     �     (�  &   =�  '   d�  /   ��      ��  $   ݊  E   �  0   H�     y�     ��     ��  (   ԋ  (   ��     &�     =�     F�     T�      q�     ��     ��     ��     ��     ��  ?   Ì     �     �     #�     2�     G�     V�     b�     r�     �     ��     ��     ��  "   ��  ;   ۍ  )   �  �   A�     ю  
   �     �     ��  '   �     4�     D�     \�     o�  
   ��     ��     ��     Ï  	   ڏ     �  ?   �  "   +�     N�     _�  -   k�     ��  +   ��     ڐ     ��  "   �  +   &�     R�     a�  0   s�     ��  (   ��     �  #   �  0   ,�     ]�     s�  '   ��  	   ��     ��          ɒ     ג     �  	   ��  U   	�     _�  �   k�     �  ,   =�  �   j�     �     
�     �  
   �  '   #�     K�     i�     m�     u�     }�     ��     ��     ��     ��     ��     ʕ     ڕ     �  %   �  '   �     >�  
   Y�     d�     w�     ~�     ��     ��     ��  '   ��     ݖ     �  *   ��     !�     ;�  
   B�     M�     \�     q�     ��     ��     ��     ��     ڗ     �     ��     �     &�     ?�     Y�     j�     |�     ��     ��     ��     Ș     И     ߘ     �    ��     �     "�     (�     :�     K�     j�     w�     ��     ��     ��     ��  ?   Κ     �     �  '   "�     J�  +   Y�     ��     ��  $   ��     �     ��     �     .�  	   :�  	   D�     N�  !   T�      v�     ��     ��  %   ʜ     �     �  '   �  ;   .�     j�  
   ��  
   ��     ��     ��     ��     ̝     ӝ  *   �     �  :    �  >   [�     ��     ��     ʞ     �     ��  	   �     �     '�     *�     G�     b�     q�     ��     ��     ��     ͟     ܟ     ��     �     �     8�     K�     g�     x�  !   ��     ��     ��  F   ��     �  _   �  .   v�  &   ��     ̡     ۡ     �     ��  �   m�  -   �     C�  
   U�     `�     o�  <   ��     ȣ     �     ��     �     �  	   �     &�  
   ;�  8   F�  p   �  9   �  @   *�  -   k�     ��     ��  >   ��      �     �     �     '�  0   9�  /   j�     ��  	   ��     ��     Ʀ     Ӧ     �  -   ��     (�     6�     G�     Z�     p�  (   ��  	   ��     ��     ͧ     ާ     �     ��     �     ,�  [   B�     ��     ��     ��  9   Ԩ     �     �     %�     <�     X�     m�     ��     ��     ��     ĩ     ֩     �  	   	�     �     %�     +�     I�  	   c�     m�     ��     ��     ��     ê  �   Ӫ     ��     ��  (   ܫ     �     %�     -�     =�     Z�     k�     {�     ��     ��  '   ��     �  *   ��     *�  "   A�  =   d�     ��  0   ��  #   �  #   �  "   1�  #   T�     x�     ��  1   ��     ܮ  (   �  &   �  9   B�  *   |�     ��     ��     ��     ů  1   ݯ     �     &�     @�     R�  	   g�     q�     ��     ��  %   ��  
   ǰ     Ұ     ٰ     �     ��     ��     �     &�  .   +�     Z�     b�     p�     y�     ��     ��     ��     ��     ��  	   ɱ     ӱ  [   �  =   E�  @   ��    Ĳ  S   ƴ  I   �  
   d�  C   o�  ;   ��  P   �  >   @�  .   �  X   ��  `   �  �   h�  �   �    ��  �   �  �   ��  �   ��  �   ��  P   T�    ��  F   ž  �   �  �   ��  �   ��  �   ,�  A   ��  y   �  �   ��  �   N�  �   ��  �   _�  H   1�  e   z�  J   ��  U   +�  g   ��    ��  �   ��  �   ��  �   �  �   ��  !  >�  F   `�     ��  �   ��  �   ��     /�     6�     =�     C�     `�     y�     ��     ��     ��     ��     ��     ��     �     �     +�  N   :�     ��      ��  3   ��     ��     �  U   �  	   s�     }�     ��     ��  	   ��  R   ��     �     /�     K�  -   c�     ��     ��  "   ��  ,   ��  <   �  _   U�     ��     ��     ��  [   ��  	   C�     M�     Z�     n�  	   ��     ��  c   ��     �     �     �  �   %�  
   �     �     �     $�     )�  r   1�  }   ��  �   "�     ��     ��  	   ��     �     �     &�     2�     A�     J�     W�  
   h�     s�     ��     ��     ��     ��     ��  	   ��     ��  	   ��     ��     
�     �     .�     @�     L�     e�     ��     ��     ��     ��     ��     ��  
   ��     ��  	   ��     ��     ��     ��     �     �     �      �  
   6�     A�     J�     V�  <   e�     ��     ��     ��     ��  
   ��     ��     ��     �      �     0�     P�  
   _�  "   j�     ��     ��     ��     ��     ��  "   ��     ��     ��     ��     �  
   %�  	   0�     :�  #   F�     j�     s�     ��     ��     ��     ��     ��     ��     ��  
   �  #   �     4�     C�     [�     y�     ��  	   ��     ��     ��     ��  
   ��     ��     
�     �     %�     >�     L�     [�     j�     |�     ��     ��     ��     ��  
   ��     ��     ��     ��     ��     ��  #   	�     -�     I�     a�     p�     ��     ��     ��     ��    ��     ��  
   ��     ��   		This plugin provides bar for bookmarks.
		 %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i Attachment %i Attachments %i BackLink %i BackLinks %i _Backlink %i _Backlinks %i errors occurred, see log %i file will be deleted %i files will be deleted %i file will be trashed %i files will be trashed %i open item %i open items %i warnings occurred, see log (Un-)indenting a list item also changes any sub-items <Top> <Unknown> A desktop wiki A file with that name already exists. A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Action Add Application Add Bookmark Add Notebook Add bookmark/Show settings Add column Add new bookmarks to the beginning of the bar Add row Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files All Tasks Allow public access Always use last cursor position when opening a page Always wrap at character Always wrap at word boundaries An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Arithmetic Ascii graph (Ditaa) Attach File Attach image first Attachment Browser Attachments Attachments: Author Auto
Wrap Auto indenting Automatically collapse sections on close page Automatically expand sections on open page Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave interval in minutes Autosave version on regular intervals Autosave version when the notebook is closed Back to Original Name BackLinks BackLinks Pane Backend Backlinks: Bazaar Bookmarks BookmarksBar Border width Bottom Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Cannot write this file. Probably this is due to the lenght
of the file name, please try using a name with less
than 255 characters Cannot write this file. Probably this is due to the lenght
of the file path, please try using a folder structure resulting in less
than 4096 characters Capture whole screen Center Change columns Changes Characters Characters excluding spaces Check Checkbox '<' Check Checkbox '>' Check Checkbox 'V' Check Checkbox 'X' Check _spelling Check and Update Index Checkbo_x List Checking a checkbox also changes any sub-items Classic trayicon,
do not use new style status icon on Ubuntu Clear Clone row Code Block Column 1 Command Command Palette Command does not modify data Comment Common include footer Common include header Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Copy Email Address Copy Line Copy Template Copy _As... Copy _Link Copy _Location Copy _link to this location Copy link to clipboard Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Created Cu_t Custom Tools Custom _Tools Customize... Date Date and Time... Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Demote Dependencies Description Details Diagram Discard note? Display line numbers Distraction Free Editing Do not use system trash for this notebook Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root Due E_xport... Edit %s Edit Custom Tool Edit Image Edit Link Edit Table Edit _Source Editing Editing file: %s Enable Version Control? Enable plugin Enabled Equation Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File exists, do you want to import? File is not writable: %s File name should not be blank. File name should not contain path declaration. File name too long: %s File path too long: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Fossil GNU R Plot Get more plugins online Get more templates online Git Gnuplot Go back Go forward Go to home page Grid lines Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide Journal pane if empty Hide menubar in fullscreen mode Highlight current line Home Page Horizontal _Line Icon Icons Icons & Text Icons & Text horizontal Id Id "%s" not found on the current page Images Import Page Include horizontal lines in the ToC Include subpages Index Index page Inline Calculator Insert Code Block Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Sequence Diagram Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Key Binding Key bindings Key bindings can be changed by clicking on a field with a key combination
in the list and then press the new key binding.
To disable a keybinding, select it in the list and use <tt>&lt;Backspace&gt;</tt>. Labels marking tasks Large Last Modified Leave Fullscreen Leave link to new page Left Left Side Pane Line Sorter Lines Link Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Managing table columns Map document root to URL Match _case Maximum number of bookmarks Maximum page width Menubar Mercurial Modified Month Move Selected Text... Move Text to Other Page Move column ahead Move column backward Move page "%s" to trash? Move text to Name Need output file to export MHTML Need output folder to export full notebook Never wrap lines New File New Page New Page in %s New S_ub Page... New _Attachment Next No Applications Found No changes since last version No dependencies No document root defined for this notebook No plugin available to display objects of type: %s No such file: %s No such page: %s No such wiki defined: %s No templates installed No text selected Notebook Notebooks OK Only Show Active Tasks Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open cell content link Open help Open in New Window Open in New _Window Open new page Open plugins folder Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" and all of it's sub-pages and
attachments will be deleted.

This deletion is permanent and cannot be un-done. Page "%s" and all of it's sub-pages and
attachments will be moved to your system's trash.

To undo later, go to your system's trashcan. Page "%s" does not have a folder for attachments Page Index Page Name Page Template Page already exists: %s Page is read-only and cannot be edited Page not allowed: %s Page not available: %s Page section Paragraph Password Paste Paste As _Verbatim Path Bar Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please select a row, before you push the button. Please select more than one line of text Please specify a notebook Plugin Plugin "%s" is required to display this object Plugins Port Position in the window Pr_eferences Prefer short link names for pages Prefer short names for page links Preferences Prev Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Remove column Remove row Removing Links Rename file Rename or Move Page Rename page "%s" Repeated clicking a checkbox cycles through the checkbox states Replace _All Replace with Require authentication Restore page to saved version? Rev Right Right Side Pane Right margin position Row down Row up S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Screenshot Command Search Search _Backlinks... Search this section Section Section(s) to ignore Section(s) to index Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Sequence Diagram Server not started Server started Server stopped Set New Name Set ToC fontsize Set default browser Set default text editor Set to Current Page Show BackLink count in title Show Line Numbers Show Tasks as Flat List Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show due date in sidepane Show full Page Name Show full Page Names Show full page name Show helper toolbar Show in the toolbar Show linkmap button in headerbar Show right margin Show tasklist button in headerbar Show tasklist in sidepane Show the cursor also for pages that can not be edited Show the page title heading in the ToC Single _page Size Small Smart Home key Some error occurred while running "%s" Sort alphabetically Sort pages by tags Source View Spell Checker Start Start _Web Server Su_bscript Su_perscript Support thumbnails for SVG Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Tags for non-actionable tasks Task Task List Tasks Template Templates Text Text Files Text From _File... Text background color Text foreground color Text wrap mode The file "%s" exists but is not a wiki page.
Do you want to import it? The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The following parameters will be substituted
in the command when it is executed:
<tt>
<b>%f</b> the page source as a temporary file
<b>%d</b> the attachment directory of the current page
<b>%s</b> the real page source file (if any)
<b>%p</b> the page name
<b>%n</b> the notebook location (file or folder)
<b>%D</b> the document root (if any)
<b>%t</b> the selected text or word under cursor
<b>%T</b> the selected text including wiki formatting
</tt>
 The inline calculator plugin was not able
to evaluate the expression at the cursor. The table must consist of at least on row!
 No deletion done. Theme There are no changes in this notebook since the last version that was saved There was a problem loading this plugin

 This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This page name cannot be used due to a conflicting file in the storage This page name cannot be used due to technical limitations of the storage This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a "path bar" to the top of the window.
This "path bar" can show the notebook path for the current page,
recent visited pages or recent edited pages.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds the page index pane to the main window.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows inserting 'Code Blocks' in the page. These will be
shown as embedded widgets with syntax highlighting, line numbers etc.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin also has properties,
see the notebook properties dialog This plugin cannot be enabled due to missing dependencies.
Please see the dependencies section below for details.

 This plugin opens a search dialog to allow quickly executing menu entries. The search dialog can be opened by pressing the keyboard shortcut Ctrl+Shift+P which can be customized via Zim's key bindings preferences. This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a macOS menubar for zim. This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a sequence diagram editor for zim based on seqdiag.
It allows easy editing of sequence diagrams.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This plugin turns one section of the notebook into a journal
with a page per day, week or month.
Also adds a calendar widget to access these pages.
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox '<' Toggle Checkbox '>' Toggle Checkbox 'V' Toggle Checkbox 'X' Toggle _Editable Toggle editable Tool Bar Toolbar size Toolbar style Top Top Pane Trash Page Trash failed, do you want to permanently delete instead ? Tray Icon Try wrap at word boundaries or character Turn page name into tags for task items Type Un-check Checkbox Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unkown Image type Unkown Object Unspecified Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use a page for each Use date from journal pages Use font color for dark theme Use horizontal scrollbar (may need restart) Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Use thumbnails Username Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log View debug log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s With this plugin you can embed a 'Table' into the wiki page. Tables will be shown as GTK TreeView widgets.
Exporting them to various formats (i.e. HTML/LaTeX) completes the feature set.
 Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Your system encoding is set to %s, if you want support for special characters
or see errors due to encoding, please ensure to configure your system to use "UTF-8" Zim Desktop Wiki Zoom _Out _About _Add _All Panes _Arithmetic _Attachment... _Back _Browse _Bugs _Cancel _Checkbox _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Delete... _Discard Changes _Duplicate Line _Edit _Edit Link _Edit Link or Object... _Edit Properties _Edit... _Emphasis _FAQ _File _Find _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump _Jump To... _Keybindings _Limit search to the current page and sub-pages _Link _Link to date _Link... _Mark _More _Move _Move Here _Move Line Down _Move Line Up _New Page Here... _New Page... _Next _Next in Index _None _Normal Size _Numbered List _OK _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Previous in Index _Print _Print to Browser _Properties _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove _Remove Heading _Remove Line _Remove Link _Remove List _Rename or Move Page... _Rename... _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In as due date for tasks as start date for tasks calendar:week_start:0 do not use horizontal lines macOS Menubar no grid lines readonly seconds translator-credits vertical lines with lines {count} of {total} Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2021-09-28 18:16+0000
Last-Translator: Andreas Klostermaier <andreas.klostermaier@appendx.de>
Language-Team: German <https://hosted.weblate.org/projects/zim/master/de/>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.9-dev
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
 		Diese Erweiterung stellt eine Lesezeichenleiste zur Verfügung.
		 Aufruf von %(cmd)s fehlgeschlagen.
Fehlercode: %(code)i Es sind %(n_error)i Fehler und %(n_warning)i Warnungen aufgetreten (siehe Protokoll) %A, %d. %B %Y %i Anhang %i Anhänge %i Rückverweis %i Rückverweise %i _Rückverweis %i _Rückverweise %i Fehler aufgetreten, siehe Log %i Datei wird gelöscht %i Dateien werden gelöscht %i Datei wird gelöscht %i Dateien werden gelöscht %i offene Aufgabe %i offene Aufgaben %i Warnungen aufgetreten, siehe Log Das Ein-/Ausrücken von Punkten einer Liste ändert auch dessen Unterpunkte <Oben> <Unbekannt> Ein Desktop Wiki Eine Datei mit diesem Namen existiert bereits. Eine Datei mit dem Namen <b>"%s"</b> existiert bereits,
Sie können einen anderen Namen vorschlagen oder die bestehende Datei überschreiben. Eine Tabelle muss mindestens eine Spalte haben. Aktion Anwendung hinzufügen Lesezeichen hinzufügen Notizbuch hinzufügen Lesezeichen hinzufügen/Einstellungen anzeigen Spalte hinzufügen Neue Lesezeichen am Anfang der Leiste einfügen Zeile hinzufügen Diese Erweiterung stellt eine automatische Rechtschreibprüfung auf der Basis von 'gtkspell' zur Verfügung.

Diese Erweiterung gehört zum Lieferumfang von Zim.
 Ausrichten Alle Dateien Alle Aufgaben Öffentlichen Zugriff erlauben Verwende immer die letzte Cursorposition beim Öffnen einer Seite Exakt nach Anzahl Zeichen umbrechen Nur an Wortgrenzen umbrechen Beim Erstellen der Grafik trat ein Fehler auf.
Möchten Sie den Quelltext trotzdem speichern? Quelltext mit Anmerkungen Anwendungen Arithmetisch Ascii-Diagramm (Ditaa) Datei anhängen Bild zuerst anhängen Anhangs-Verzeichnis Dateianhänge Anhänge: Autor Automatisch
Umbrechen Auto-Einrückung Beim Schliessen einer Seite automatisch die Unterseiten zuklappen Beim Öffnen einer Seite automatisch die Unterseiten aufklappen Automatisch gespeicherte Version Das aktuelle Wort automatisch auswählen, wenn Sie die Formatierung ändern 'KamelSchreibWeise' automatisch verlinken Dateien automatisch verlinken Automatisches Speichern in Minuten Automatisch speichern in regelmäßigen Abständen Version automatisch speichern, wenn das Notizbuch geschlossen wird Ursprünglichen Namen verwenden Rückverweise Fensterbereich für Rückverweise Back-End Rückverweise: Bazaar Lesezeichen Lesezeichenleiste Rahmenbreite Unten Unterer Fensterbereich Durchblättern Aufz_ählung K_onfigurieren Seite %s kann nicht geändert werden Abbrechen Die Datei kann nicht geschrieben werden. Vermutlich ist der
Dateiname zu lang. Beschränken Sie den Namen
auf max. 255 Zeichen. Die Datei kann nicht geschrieben werden. Vermutlich ist der
Dateipfad zu lang. Beschränken Sie die Länge der Ordnerstruktur
auf max. 4096 Zeichen. Ganzen Bildschirm aufnehmen Zentriert Spalten ändern Änderungen Zeichen Zeichen ohne Leerzeichen Als 'zurückgegeben' abhaken Als 'weitergeleitet' abhaken Als 'erledigt' abhaken Als 'abgebrochen' abhaken Recht_schreibung überprüfen Index prüfen & aktualisieren Ankreu_zliste Das Abhaken eines Ankreuzfeldes ändert auch dessen Unterpunkte Klassisches Informations-Symbol,
verwende nicht das Symbol im neuen Ubuntu Stil Leeren Zeile duplizieren Code-Block Spalte 1 Befehl Befehlspalette Befehl verändert keine Daten Kommentar Stets als Fuß eingefügt Stets als Kopf eingefügt Komplettes _Notizbuch Anwendungen konfigurieren Erweiterung einrichten Eine Anwendung zum Öffnen von "%s"-Verknüpfungen konfigurieren Anwendung festlegen, mit der Dateien vom Typ "%s" geöffnet werden sollen Alle Ankreuzfelder als Aufgaben interpretieren Kopieren E-Mail-Adresse kopieren Zeile kopieren Kopiere Vorlage Kopieren _als... _Verknüpfung kopieren Seiten_pfad kopieren _Verknüpfung an diese Stelle kopieren Verknüpfung in Zwischenablage kopieren Konnte die ausführbare Datei "%s" nicht finden Konnte Notizbuch %s nicht finden Konnte die Vorlage "%s" nicht finden Konnte für dieses Notizbuch keine Datei bzw. kein Verzeichnis finden Rechtschreibprüfung konnte nicht geladen werden %s kann nicht geöffnet werden Kann Ausdruck nichtz auswerten Lesen von %s nicht möglich! Seite %s konnte nicht gespeichert werden Erstelle eine neue Seite für jede Notiz Verzeichnis erstellen? Angelegt _Ausschneiden Benutzerdefinierte Werkzeuge Benu_tzerdefinierte Werkzeuge... Anpassen... Datum Datum und Zeit... Tag Standard Standardformat für das Kopieren von Text in die Zwischenablage Standard-Notizbuch Verzögerung Seite löschen Seite "%s" löschen? Zeile löschen Herabstufen Abhängigkeiten Beschreibung Details Diagramm Notiz verwerfen? Zeilenzähler anzeigen Ablenkungsfreier Bearbeitungsmodus Für dieses Notizbuch nicht den System-Papierkorb verwenden Sollen alle Lesezeichen gelöscht werden? Möchten Sie die Seite %(page)s
mit der gespeicherten Version %(version)s überschreiben?

Alle Änderungen seit dieser Version gehen verloren! Wurzelverzeichnis Fällig am E_xportieren... %s bearbeiten Benutzerdefiniertes Werkzeug bearbeiten Bild bearbeiten Verknüpfung bearbeiten Tabelle bearbeiten _Quelltext bearbeiten Bearbeiten Datei bearbeiten: %s Versionskontrolle anschalten? Erweiterung aktivieren Aktiviert Formel Fehler in %(file)s in der Zeile %(line)i nahe bei "%(snippet)s" _Mathematischen Ausdruck auswerten Alle _aufklappen Exportieren Alle Seiten in eine einzige Datei exportieren Export abgeschlossen Jede Seite in eine eigene Datei exportieren Notizbuch wird exportiert Nicht erfüllt Fehler bei der Ausführung von: %s Anwendung konnte nicht gestartet werden: %s Datei exisiert Datei_vorlagen... Die Datei wurde auf der Festplatte geändert: %s Die Datei existiert bereits Datei existiert bereits – importieren? Datei ist nicht schreibbar: %s Der Dateiname darf nicht leer sein. Der Dateiname darf keine Pfad-Angaben enthalten. Dateiname zu lang: %s Dateipfad zu lang: %s Dateiformat wird nicht unterstützt: %s Dateiname Filter Suchen _Weitersuchen _Rückwärts suchen Suchen und Ersetzen Suche was Aufgaben, die am Montag oder Dienstag fällig sind, schon vor dem Wochenende anzeigen Verzeichnis Das Verzeichnis existiert bereits und enthält Dateien. Wenn Sie in dieses Verzeichnis exportieren, überschreiben Sie vielleicht existierende Dateien. Wollen Sie weiter machen? Verzeichnis besteht bereits: %s Speicherort für Vorlagen von Dateianhängen Für die fortgeschrittene Suche können die logischen Verknüpfungen AND, OR und NOT verwendet werden. Mehr Informationen dazu finden Sie in der Hilfe. For_mat Format Fossil GNU R Plot Erweiterungen aus dem Internet beziehen Weitere Vorlagen online holen Git Gnuplot Zurück Weiter Zur Startseite Raster Überschrift _1 Überschrift _2 Überschrift _3 Überschrift _4 Überschrift _5 Höhe Tagebuch-Leiste ausblenden, wenn leer Menüleiste im Vollbildmodus verstecken Aktuelle Zeile hervorheben Startseite Horizontale _Linie Symbol Symbole Symbole & Text Symbole & Text nebeneinander ID ID "%s" auf dieser Seite nicht gefunden Bilder Seite importieren Trennlinien im Inhaltsverzeichnis anzeigen Unterseiten einschließen Seiten Indexseite Taschenrechner Code-Block einfügen Datum und Uhrzeit einfügen Diagramm einfügen Ditaa einfügen Formel einfügen GNU R Diagramm einfügen Füge Gnuplot ein Bild einfügen Verknüpfung einfügen Notensatz einfügen Bildschirmfoto einfügen Sequenzdiagramm einfügen Symbol einfügen Tabelle einfügen Text aus Datei einfügen Erscheinungsbild Interwiki Schlüsselwort Tagebuch Gehe zu Gehe zur Seite _Tastaturkürzel Tastenkürzel Zum Ändern eines Tastaturkürzels, zunächst die entsprechende Aktion in der Liste wählen
und dann die gewünschte Tastenkombination drücken.
Zum Löschen eines Tastaturkürzels, die entsprechende Zeile wählen und dann <tt>&lt;Löschtaste&gt;</tt> drücken. Kennzeichnungen für Aufgaben Groß Zuletzt geändert Vollbild beenden Link zur neuen Seite einfügen Linksbündig Linker Fensterbereich Alphabetische Sortierung Zeilen Verknüpfung Verweis-Struktur... Verlinke Dateien im Dokumentverzeichnis mit vollständigem Pfad Verknüpfen mit Ort Ereignisse mit Zeitgeist protokollieren Protokolldatei Anscheinend haben Sie einen Fehler gefunden Zur Standardanwendung machen Tabellen-Spalten verwalten Dokumentverzeichnis auf URL abbilden Groß-/Kleinschreibung Maximale Anzahl an Lesezeichen Maximale Seitenbreite Menüleiste Mercurial Geändert Monat Ausgewählten Text verschieben... Verschiebe Text auf andere Seite Spalte nach oben bewegen Spalte nach unten bewegen Seite "%s" in Papierkorb verschieben? Verschieben nach Name Export von MHTML benötigt Ausgabedatei Export des gesamten Notizbuchs benötigt Ausgabeverzeichnis Zeilen niemals umbrechen Neue Datei Neue Seite Neue Seite in %s Neue Unter_seite... Neuer _Anhang Weiter Keine Anwendungen gefunden Keine Änderung seit der letzten Sicherung Keine. Für dieses Notizbuch ist kein Wurzelverzeichnis definiert Keine Erweiterung verfügbar, um Objekte vom Typ %s anzuzeigen Datei nicht gefunden: %s Keine solche Seite: %s Kein Wiki definiert: %s Keine Vorlagen installiert Kein Text markiert Notizbuch Notizbücher Ok Nur aktive Aufgaben anzeigen _Anhangverzeichnis öffnen Ordner öffnen Notizbuch öffnen Öffnen mit... _Dokumentverzeichnis öffnen _Notizbuchverzeichnis öffnen Seite _Öffnen Zellen-Inhaltslink öffnen Hilfe öffnen In neuem Fenster öffnen In neuem _Fenster öffnen Neue Seite öffnen Öffne Erweiterungen-Ordner Mit "%s" öffnen Optional Einstellungen für Erweiterung %s Anderes … Ausgabedatei Ausgabedatei existiert, Export kann mit "--overwrite" erzwungen werden Ausgabeverzeichnis Ausgabeverzeichnis existiert und ist nicht leer, Export kann mit "--overwrite" erzwungen werden Für den Export wird ein Ausgabeziel benötigt Ausgabe soll aktuelle Auswahl ersetzen Überschreiben Pf_adleiste Seite Seite "%s" wird inkl. alle Unterseiten 
und Anhänge werden gelöscht.

Diese Löschung kann nicht rückgängig gemacht werden. Seite "%s" wird inklusive aller Unterseiten
und Anhänge in den Papierkorb des Systems verschoben.

Zum Rückgängig machen, Dokumente aus dem System-Papierkorb holen. Seite "%s" hat kein Verzeichnis für Anhänge Seitenverzeichnis Seitenname Seiten Vorlage Seite existiert bereits: %s Seite ist schreibgeschützt und kann nicht bearbeitet werden Seite nicht erlaubt: %s Seite nicht verfügbar: %s Seiten-Abschnitt Absatz Passwort Einfügen _Wörtlich einfügen Pfadleiste Bitte geben Sie eine Beschreibung für diese Fassung ein Bitte beachten Sie, dass der Link auf eine nicht
existierende Seite automatisch eine neue, leere 
Seite erzeugt. Bitte wählen sie einen Namen und Ort für das Notizbuch. Bitte erst eine Zeile markieren, bevor die Taste gedrückt wird. Bitte wählen Sie mehr als eine Textzeile aus Bitte ein Notizbuch angeben Erweiterung Erweiterung "%s" ist erforderlich, um dieses Objekt anzuzeigen Erweiterungen Port Position im Fenster _Einstellungen... Kurze Verknüpfungs-Namen für Seiten bevorzugen Kurz-Namen für Seitenverknüpfungen bevorzugen Einstellungen Vorherige Druck in HTML-Datei Heraufstufen _Notizbuch-Eigenschaften Eigenschaften Zeichnet Ereignisse per Zeitgeist-Dämon auf. Schnell-Notiz Schnell-Notiz... Letzte Änderungen Letzte Änderungen... Zuletzt _geänderte Seiten Gleichzeitig Wiki Markups re-formatieren Entfernen Alle entfernen Spalte entfernen Zeile entfernen Links entfernen Datei umbenennen Seite umbenennen/verschieben Seite "%s" umbenennen Wiederholtes Klicken einer Checkbox schaltet durch die verschiedenen Zustände der Checkbox _Alles ersetzen Ersetzen durch Anmeldung erforderlich Aktuellen Stand mit gespeicherter Version überschreiben? Revision Rechtsbündig Rechter Fensterbereich Position des rechten Randes Zeile runterschieben Zeile hochschieben Eine V_ersion speichern... Eine _Kopie speichern... Kopie speichern Version speichern Lesezeichen speichern Gespeicherte Version aus Zim Bewertung Screenshot Befehl Suche Suche nach _Rückverweisen... Suche in diesem Abschnitt Abschnitt Abschnitte ignorieren Abschnitt(e) indizieren Datei auswählen Verzeichnis auswählen Bild auswählen Wählen Sie eine Version aus, um die Unterschiede zwischen der Version und 
dem aktuellen Stand anzeigen zu lassen. Wenn Sie zwei Versionen auswählen, 
werden die Unterschiede dieser Versionen angezeigt.
 Wählen Sie das Exportformat Wählen Sie den Ausgabeordner Wählen Sie die zu exportierenden Seiten Fenster oder Bereich auswählen Auswahl Sequenzdiagramm Server wurde nicht gestartet Server gestartet Server gestoppt Neuen Namen vergeben Schriftgrösse Vorgabe-Browser festlegen Programm für Textbearbeitung festlegen Auf aktuelle Seite einstellen Anzahl der Rückverweise im Titel anzeigen Zeilennummern anzeigen Aufgaben als flache Liste anzeigen Zeige Inhalt als schwebendes Widget statt in der Seitenleiste Unterschiede anzeigen Ein eigenes Symbol für jedes Notizbuch anzeigen Termin in der Seitenleiste anzeigen Vollständigen Seitennamen anzeigen Vollständige Seitennamen anzeigen Vollständigen Seitennamen anzeigen Hilfe-Leiste anzeigen Erscheint in Werkzeugleiste Verweis-Struktur-Taste in der Kopfleiste anzeigen Rechten Rand anzeigen Aufgabenliste in der Kopfleiste anzeigen Aufgabenliste in Seitenleiste anzeigen Auch bei schreibgeschützten Seiten Schreibmarke anzeigen Seitentitel im Inhaltsverzeichnis anzeigen Einzelne _Seite Größe Klein Intelligente Home-Taste Ein Fehler trat bei der Bearbeitung von "%s" auf. Alphabetisch sortieren Sortiere Seiten nach Tags Quellcode-Ansicht Rechtschreibprüfung Startzeit _Webserver starten... Tiefstellung Hochstellung Vorschaubilder für SVG unterstützen Sy_mbol... Syntax Systemvorgabe Tabulatorbreite Tabelle Tabellenbearbeitung Inhaltsverzeichnis Tags Tags für Aufgaben, die keine Aktion erfordern Aufgabe Aufgabenliste Aufgaben Vorlage Vorlagen Text Textdateien Text aus _Datei... Farbe des Texthintergrunds Textfarbe Textumbruch-Verhalten Die Datei "%s" existiert zwar, ist aber keine Wiki-Seite.
Soll die Datei importiert werden? Der Ordner
%s
existiert nicht.
Soll er jetzt angelegt werden? Der Ordner "%s" existiert nicht.
Möchten Sie ihn jetzt anlegen? Die folgenden Parameter werden bei
Ausführung des Befehls ersetzt:
<tt>
<b>%f</b> der Pfadname der Seite als temporäre Datei
<b>%d</b> das Anhangsverzeichnis der aktuellen Seite
<b>%s</b> der Pfadname der eigentlichen Seite (falls vorhanden)
<b>%p</b> der Seitenname
<b>%n</b> der Speicherort des Notizbuchs (Datei oder Ordner)
<b>%D</b> das Wurzelverzeichnis (falls vorhanden)
<b>%t</b> der ausgewählte Text oder das Wort unter dem Mauszeiger
<b>%T</b> der ausgewählte Text inklusive Wiki-Formatierung
</tt>
 Der Text-Taschenrechner konnte den Ausdruck an der Cursor-Position nicht berechnen. Die Tabelle muss mindestens eine Zeile haben!
 Es wurde nichts gelöscht. Farbschema Seit der letzten Sicherung wurde dieses Notizbuch nicht verändert. Beim Laden dieser Erweiterung ist ein Problem aufgetreten

 Dies könnte bedeuten, dass die richtigen Wörterbücher nicht installiert sind. Diese Datei existiert bereits.
Soll sie überschrieben werden? Diese Seite hat kein Verzeichnis für Anhänge Dieser Seitenname kann aufgrund eines Namenskonflikts im Speicher nicht verwendet werden Dieser Seitenname kann aufgrund technischer Beschränkungen des Speichers nicht verwendet werden Mit dieser Erweiterung kann ein Bildschirmfoto aufgenommen und in die aktive Seite eingesetzt werden.

Diese Erweiterung gehört zum Lieferumfang von Zim.
 Diese Erweiterung fügt über dem Fenster eine "Pfad-Leiste" ein. Diese "Pfad-Leiste" zeigt je nach Einstellung die zuletzt besuchten oder geänderten Seiten an (oder beides), oder aber den internen Pfad der aktuellen Seite innerhalb des Notizbuchs.
 Diese Erweiterung zeigt alle offenen Aufgaben des Notizbuchs. Offene Aufgaben sind entweder unangekreuzte Aufgabenkästchen oder Elemente, die mit Schlüsselworten wie 'FIXME' oder 'TODO' markiert wurden.

Diese Erweiterung gehört zum Lieferumfang von Zim.
 Diese Erweiterung erlaubt es, per Menü oder Kommandozeile einfach Text oder den Inhalt einer Zwischenablage in ein Notizbuch einzufügen. Beachten Sie die Anleitung für weitere Informationen.

Diese Erweiterung gehört zum Lieferumfang von Zim.
 Diese Erweiterung fügt dem System-Infobereich ein Zim-Symbol hinzu, mit dem sich Funktionen und Notizbücher schnell aufrufen lassen.

Für die Nutzung ist Gtk+ ab der Version 2.10 erforderlich.

Diese Erweiterung gehört zum Lieferumfang von Zim.
 Diese Erweiterung ergänzt Zim um einen Fensterbereich, der eine Liste von Seiten anzeigt, die auf die aktuelle Seite verweisen.

Diese Erweiterung gehört zum Lieferumfang von Zim.
 Diese Erweiterung erzeugt – basierend auf den Überschriften – ein Inhaltsverzeichnis für die aktuelle Seite.

Diese Erweiterung gehört zum Lieferumfang von Zim.
 Diese Erweiterung ergänzt Zim um einen ablenkungsfreien Textbearbeitungsmodus.
 Diese Erweiterung ergänzt das Einfügen-Menü um den 'Symbol einfügen'-Dialog.
Damit wird die Eingabe typografischer Sonderzeichen erleichtert. Die Liste verfügbarer Sonderzeichen kann an die eigenen Bedürfnisse angepasst werden.

Diese Erweiterung gehört zum Lieferumfang von Zim.
 Diese Erweiterung fügt dem Hauptfenster ein Seitenverzeichnis hinzu.
 Diese Erweiterung ergänzt eine Versionsverwaltung für das Notizbuch.

Unterstützt werden die Versionsverwaltungen Bazaar (bzr), Mercurial (hg), Git (git) und Fossil (fossil).

Diese Erweiterung gehört zum Lieferumfang von Zim.
 Diese Erweiterung ermöglicht das Einfügen von "Code-Blöcken" inklusive Zeilenzähler und Syntax-Hervorhebung für zahlreiche Programmiersprachen.
 Mit dieser Erweiterung können arithmetische Berechnungen in Zim eingebettet werden.
Es basiert auf dem Modul 'arithmetic' (http://pp.com.mx/python/arithmetic).
 Diese Erweiterung ermöglicht die schnelle Berechnung einfacher mathematischer Ausdrücke direkt im Text.

Diese Erweiterung gehört zum Lieferumfang von Zim.
 Weitere Einstellungen finden sich in den Notizbuch-Einstellungen! Diese Erweiterung kann wegen fehlender Abhängigkeiten nicht aktiviert werden.
Beachten Sie die untenstehenden Details.

 Diese Erweiterung öffnet einen Suchdialog für den Schnellzugriff auf Menübefehle. Die Tastenkombination hierfür ist Strg-Umschalt-P und kann in den Tastenkürzel-Einstellungen angepasst werden. Diese Erweiterung ermöglicht es, Ditaa-basierte Diagramme in Zim einzufügen.

Diese Erweiterung gehört zum Lieferumfang von Zim.
 Diese Erweiterung erlaubt das Erstellen von Diagrammen in Zim mit Hilfe von GraphViz.

Diese Erweiterung gehört zum Lieferumfang von Zim.
 Diese Erweiterung stellt in einem eigenen Fenster die Verknüpfungs-Struktur des Notizbuchs grafisch dar. Die Darstellung fungiert als eine Art "Mind Map".

Diese Erweiterung gehört zum Lieferumfang von Zim.
 Diese Erweiterung stellt eine macOS Menüleiste für Zim zur Verfügung. Diese Erweiterung erzeugt ein Seitenverzeichnis, das mithilfe einer Tag-Wolke gefiltert werden kann.
 Diese Erweiterung stellt einen Editor für GNU R Diagramme in Zim bereit.
 Diese Erweiterung ergänzt Zim um einen Funktionenplotter auf der Basis von Gnuplot.
 Diese Erweiterung stellt einen auf 'seqdiag' basierenden Flußdiagramm-Editor für Zim zur Verfügung.
 Diese Erweiterung stellt eine Umgehungslösung für die fehlende Druckfunktion in Zim bereit. Es exportiert die aktuelle Seite in eine HTML-Datei und öffnet diese im Browser. Von dort kann die Datei dann gedruckt werden.

Diese Erweiterung gehört zum Lieferumfang von Zim.
 Diese Erweiterung stellt einen LaTeX-basierten Gleichungs-Editor zur Verfügung.

Diese Erweiterung gehört zum Lieferumfang von Zim.
 Diese Erweiterung stellt einen Notensatz-Editor auf Basis von GNU Lilypond für Zim bereit.

Diese Erweiterung gehört zum Lieferumfang von Zim.
 Diese Erweiterung erstellt ein Verzeichnis der Anhänge der aktuellen Seite in Form einer Symbolansicht unterhalb der aktuellen Seite.
 Diese Erweiterung sortiert markierte Zeilen in alphabetischer Reihenfolge. Wiederholtes Sortieren ändert die Sortierreihenfolge zwischen auf- und absteigend.
 Diese Erweiterung erstellt auf der obersten Ebene des Notizbuchs einen Journal-Abschnitt für Tagebuch-Einträge (untergliedert in Monat, Woche, Tag). Zusätzlich wird ein Kalender-Fensterbereich erzeugt, um schnell auf die Tagebuch-Einträge eines bestimmten Datums zugreifen zu können.
 Gewöhnlich bedeutet dies, dass die Datei ungültige Zeichen enthält. Titel Sie können an dieser Stelle eine Kopie dieser Seite speichern oder 
die Änderungen verwerfen. Wenn Sie eine Kopie speichern, werden 
die Änderungen auch verworfen, Sie können Sie aber später aus der 
Kopie wieder herstellen. Um ein neues Notizbuch anzulegen, muss ein leerer Ordner ausgewählt werden.
Es kann auch ein bereits vorhandener Zim Notizbuch-Ordner ausgewählt werden.
 Inhalt _Heute Heute Als 'zurückgegeben' abhaken Als 'verschoben' abhaken Als 'erledigt' abhaken Als 'nicht erledigt' abhaken Schreibschutz umschalten Schreibschutz Werkzeugleiste Werkzeugleisten-Grösse Werkzeugleisten-Stil Oben Oberer Fensterbereich Seite löschen Bewegen in den Papierkorb ist fehlgeschlagen, stattdessen endgültig löschen? Benachrichtigungsfeldsymbol Möglichst am Wortende umbrechen Seitenname in Tags für die Aufgabenliste umwandeln Dateityp Ankreuzfeld zurücksetzen <Backspace (Zurück)>-Taste verringert den Einzug
(Alternativ <Hochstell><Backspace>) Unbekannt Unbekanntes Bildformat Unbekanntes Objekt Nicht angegeben Ohne Tags %i Link auf diese Seite werden angepasst %i Links auf diese Seite werden angepasst Seitentitel wird angepasst Anpassen der Verknüpfungen Index wird aktualisiert %s verwenden, um zur Seitenleiste zu wechseln Eigene Schriftart benutzen Verwende jeweils eine Seite Datum aus Journal-Seiten verwenden Schriftfarbe für das dunkle Thema verwenden Horizontalen Rollbalken verwenden (erfordert evtl. Neustart) Mit der <Return>-Taste können Sie Links folgen.
(Wenn deaktiviert, benutzen Sie <Alt><Return>) Vorschaubild verwenden Benutzer Versionskontrolle Die Versionskontrolle ist für dieses Notizbuch nicht aktiviert.
Wollen Sie sie aktivieren? Versionen _Anmerkungen _Protokoll anzeigen _Diagnose-Protokoll anzeigen... Webserver Woche Wenn Sie diesen Fehler melden wollen, senden Sie bitte die Meldungen aus der folgenden Textbox mit. Ganzes _Wort Breite Wiki-Seite: %s Mit dieser Erweiterung können Tabellen in die Wiki-Seite eingebettet werden. Tabellen werden mittels 'GTK TreeView' angezeigt.
Exportmöglichkeiten in verschiedene Formate (z.B. HTML/LaTeX) runden die Funktionalität ab.
 Wortanzahl Textzähler... Worte Jahr Gestern Sie bearbeiten eine Datei in einer externen Anwendung. Schließen Sie den Dialog, wenn dieser Vorgang beendet ist. Sie können benutzerdefinierte Werkzeuge erstellen, die im Werkzeugmenü,
der Werkzeugleiste und in Kontextmenüs erscheinen. Die Textkodierung des Betriebssystems ist auf %s eingestellt. Sollte es Fehlermeldungen
oder fehlerhafte Zeichendarstellungen geben, stellen Sie die Textkodierung bitte auf "UTF-8" um. Zim Desktop-Wiki Ver_kleinern _Über... _Hinzufügen _Alle Fensterbereiche _Arithmetik Dateianhang... _Zurück _Durchsuchen _Fehlermeldungen _Abbrechen _Ankreuzfeld _Untergeordneter Seite _Formatierungen löschen _Schließen Alle zu_klappen _Inhalt _Kopieren Hierher _kopieren _Löschen Seite _löschen _Löschen... Änderungen _verwerfen Zeile duplizieren _Bearbeiten Verknüpfung _bearbeiten Link oder Objekt _bearbeiten... Eigenschaften bearbeiten _Bearbeiten... Kursiv _FAQ _Datei _Finden _Suchen... _Weiter _Vollbild _Gehe zu _Hilfe _Hervorheben _Verlauf _Startseite _Bild... Seite _importieren... _Einfügen _Gehe zu _Gehe zu... _Tastenkürzel Suche auf die aktuelle Seite und Unterseite(n) einschränken _Verknüpfung erstellen Ver_linke datum _Verknüpfung... Marker _Anleitung _Verschieben Hierher _verschieben Zeile nach unten Zeile nach oben _Neue Seite an dieser Stelle... _Neue Seite... _Nächstes _Nächster Seite in der Übersicht _Nichts _Standardgröße _Nummerierte Liste _OK Öffnen Ein weiteres Notizbuch _öffnen... _Weitere... _Seite _Seiten Hierarchie _Übergeordneter Seite _Einfügen _Vorschau _Vorheriges _Vorheriger Seite in der Übersicht _Drucken _Seite im Browser öffnen Eigenschaften _Schnell-Notiz... _Beenden _Zuletzt besuchte Seiten _Wiederherstellen _Regulärer Ausdruck _Aktualisieren _Entfernen Überschrift-Formatierung entfernen Zeile löschen Ve_rknüpfung entfernen Listen-Formatierung entfernen Seite umbenennen/verschieben... Umbenennen... _Ersetzen _Ersetzen... Größe zu_rücksetzen Version wiederherstellen _Speichern _Kopie speichern _Bildschirmfoto... _Suchen Notizbuch durchsuchen... _Versenden... _Seitenleisten _Nebeneinander _Zeilen sortieren _Durchgestrichen _Fett Tiefstellung Hochstellung _Vorlagen... _Werkzeuge _Rückgängig _Vorformatiert _Versionen... _Ansicht Ver_größern als Fälligkeitsdatum für Aufgaben als Startzeit für Aufgaben Kalender:Wochen_Start:0 Nicht benutzen Horizontale Linien macOS Menüleiste keine Gitternetzlinien schreibgeschützt Sekunden Launchpad Contributions:
  Andreas Klostermaier https://launchpad.net/~apx-ankl
  Arnold S https://launchpad.net/~arnold.s
  BBO https://launchpad.net/~bbo
  Benedikt Schindler https://launchpad.net/~benedikt-schindler
  Christoph Fischer https://launchpad.net/~christoph.fischer
  Christoph Zwerschke https://launchpad.net/~cito
  Fabian Affolter https://launchpad.net/~fab-fedoraproject
  Fabian Stanke https://launchpad.net/~fmos
  Ghenrik https://launchpad.net/~ghenrik-deactivatedaccount
  Gunnar Thielebein https://launchpad.net/~lorem-ipsum
  Jaap Karssenberg https://launchpad.net/~jaap.karssenberg
  Johannes Reinhardt https://launchpad.net/~johannes-reinhardt
  Julia https://launchpad.net/~jruettgers
  Klaus Vormweg https://launchpad.net/~klaus-vormweg
  Matthias Mailänder https://launchpad.net/~mailaender
  Murat Güven https://launchpad.net/~muratg
  P S https://launchpad.net/~pascal-sennhauser
  SanskritFritz https://launchpad.net/~sanskritfritz+launchpad
  Tobias Bannert https://launchpad.net/~toba
  hruodlant https://launchpad.net/~hruodlant
  lootsy https://launchpad.net/~lootsy
  mrk https://launchpad.net/~m-r-k
  smu https://launchpad.net/~smu
  sojusnik https://launchpad.net/~sojusnik
  ucn| https://launchpad.net/~ucn
  zapyon https://launchpad.net/~zapyon Senkrechte Linien mit Linien {count} von {total} 