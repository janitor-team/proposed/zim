��    l     |              �&  ,   �&  .   
'  ?   9'     y'     �'     �'     �'  0   �'     
(     %(     C(  	   I(     S(  i   b(  *   �(     �(     �(     )     )     ()  
   C)  -   N)     |)  V   �)     �)  	   �)  	   �)     �)  3   	*  Y   =*     �*     �*  
   �*     �*     �*     �*     �*     +     +  	   +     !+  $   0+  ?   U+  /   �+  (   �+     �+  %   ,  ,   1,     ^,  	   t,     ~,     �,  
   �,     �,  	   �,     �,     �,     �,     �,  
   �,     �,     -     -     -     $-     3-  
   ;-     F-     b-     u-     �-     �-     �-  <   �-     �-  	   �-  
   .     .     .     #.     @.     H.     ^.     t.     �.     �.  +   �.  3   �.      /     0/     5/     H/     V/  
   b/     m/     |/     �/     �/  3   �/     0     &0     90     T0     g0     0     �0     �0     �0     �0     �0     �0     �0     �0     �0  0   �0     %1     61     <1     H1  
   Z1     e1     l1     y1     �1     �1     �1     �1  $   �1  ~   �1     `2     n2  
   r2     }2     �2  
   �2  	   �2  
   �2     �2     �2     �2     �2     �2     3     
3  5   3     I3     X3     d3  !   k3     �3  #   �3     �3     �3     �3     �3     4     4     ,4     E4     Q4     j4     �4     �4     �4  
   �4     �4     �4  	   �4  6   �4     5  v   5     �5  *   �5  c   �5     &6     .6     56  
   <6     G6     _6     y6     }6  
   �6  
   �6  
   �6  
   �6  
   �6  
   �6     �6     �6     �6  	   7     7      7     %7     ,7     87     I7  
   O7     Z7     l7     ~7     �7     �7     �7     �7     �7     �7     �7     �7     8     8     08     >8     K8  	   a8     k8     }8     �8     �8     �8     �8  �   �8     �9     �9     �9     �9     �9     �9     �9     �9  2   �9     :     $:     -:     G:     P:     k:     �:     �:     �:     �:     �:     �:  	   �:     ;     
;     ;     &;     >;     P;     e;     r;      w;  *   �;     �;     �;     �;     �;     �;     �;     <     /<  2   ?<     r<     �<     �<     �<     �<  	   �<     �<     �<     �<     
=     =     $=     1=     E=  
   [=     f=  	   }=     �=     �=     �=     �=     �=     �=     �=     �=     >  9   >     M>  I   [>  !   �>  '   �>  	   �>     �>     ?  0   ?  	   8?     B?     P?     h?     }?  	   �?     �?  '   �?  V   �?  3   @  0   M@     ~@     �@  .   �@     �@     �@     �@     �@     �@     A     A     !A     )A  
   5A  &   @A  
   gA     rA     �A     �A     �A     �A     �A  
   �A     �A  
   �A     B     B  ?   $B     dB     qB     ~B     �B     �B     �B     �B     �B     �B     �B     �B  	   �B     C     C     $C     ;C     AC     TC     [C     pC     �C     �C     �C     �C     �C     �C  �   �C     lD      �D     �D     �D  	   �D     �D     �D     E     E     %E     2E     JE     ^E     pE  2   �E     �E  &   �E     �E     F     F     ,F     @F     RF  5   lF  &   �F     �F     �F     �F  &   �F     G     %G     8G     DG     RG     XG  
   jG     uG     |G  	   �G     �G     �G     �G     �G     �G     �G  	   �G     �G     �G  	   �G     H  
   
H     H     (H     >H  ?   TH  A   �H  �  �H  S   �J  =   �J  K   ,K  @   xK  6   �K  -   �K  I   L  x   hL  �   �L  �   �M  �   /N  �   �N  }   >O  L   �O  �   	P  �   �P  �   ;Q  }   �Q  h   HR  k   �R  �   S  R   �S  ;   DT  =   �T  v   �T    5U  j   IV  n   �V  ]   #W     �W  �   X  7   �X     �X  �   �X  |   pY     �Y     �Y     �Y     �Y     Z     &Z     :Z  	   CZ  '   MZ     uZ     zZ  D   �Z     �Z     �Z     �Z     �Z     [  H   [     W[     w[     �[  !   �[     �[     �[     �[  P   �[     J\  U   Z\     �\     �\  	   �\  
   �\     �\  N   �\     2]     >]     D]  �   R]  
   ^     ^     &^     ,^  	   1^  ^   ;^  f   �^  �   _     �_  	   �_     �_     �_  
   �_     �_     �_     �_     �_     �_     `  	   `     `     `     0`     7`  	   E`     O`  
   U`     ``     h`     u`     �`     �`  
   �`     �`     �`     �`  	   �`     �`     �`     �`     �`     �`     a     a     a  
   a     'a     0a  	   6a     @a     Pa     Xa     ^a     ja     wa     }a     �a     �a     �a     �a  
   �a     �a     �a     �a     �a     �a     �a     �a     b     b     b  	   (b     2b     8b     Hb     Pb     Wb  	   `b     jb     qb     �b     �b     �b     �b     �b     �b     �b     �b     �b     �b     �b     �b     c     c  
   "c     -c     <c  
   Dc     Oc     [c     gc     uc     �c     �c  
   �c     �c  
   �c     �c     �c  	   �c     �c     �c     �c     �c     �c     d  
   +d     6d     Gd     Ud     ^d     fd     yd  
   �d     �d  �  �d  7   �f  4   �f  B   g     Eg     Xg  (   kg  "   �g  6   �g     �g  #   h     2h     8h     Gh  l   _h  '   �h     �h     �h     i     #i  ,   5i     bi  -   si     �i  v   �i     (j     4j     Fj     Wj  =   pj  [   �j     
k     #k     /k     ;k     Jk     ak     uk     |k     �k     �k     �k  &   �k  F   �k  4   0l  3   el  .   �l  8   �l  >   m     @m  	   Zm     dm     xm     �m     �m  
   �m     �m     �m     �m     �m     �m  *   �m     n     &n     ;n     Bn     Tn  
   cn      nn  !   �n  !   �n  !   �n     �n     o  a   (o     �o     �o     �o     �o     �o     �o     �o     �o     �o     p     'p     >p  -   Pp  8   ~p  4   �p     �p     �p     q     q     ,q     9q  &   Oq  *   vq  *   �q  A   �q  8   r     Gr  (   cr     �r  '   �r  +   �r     �r     s  	   s     s     4s     Ps     `s     es     is  ?   qs     �s     �s     �s     �s     �s     �s     t     t     t     't     0t     @t  &   _t  �   �t     u  
   %u     0u  	   =u     Gu     gu     uu     �u     �u     �u     �u     �u     �u  	   �u  	   �u  9   v     Bv     Wv     fv  1   ov     �v  .   �v     �v     �v     w     w     8w     Kw  %   _w     �w  )   �w  "   �w     �w     �w     �w     x     x     ,x  	   Cx  ^   Mx     �x  ~   �x     1y     Gy  q   ey  	   �y     �y     �y  
   �y     �y     z     5z     9z     Az     Qz     _z     mz     {z     �z     �z      �z     �z     �z     �z     �z      {     {     {     .{     6{     I{     _{     x{     �{     �{     �{     �{     �{     �{     �{     �{     |     )|     H|     Y|  %   h|  	   �|     �|     �|  
   �|     �|     �|     �|  �   �|     �}     �}     ~     #~     ,~     D~     X~     _~  E   m~     �~     �~      �~     �~  "   �~          7     T  &   s     �     �     �  	   �     �     �     �     �     8�     O�     g�     x�  1   }�  <   ��     �     ��     �     �     '�     .�  (   K�     t�  :   ��     ��     ف  $   �     �     *�     2�     ;�  !   >�     `�     x�     ��     ��     ��     ��     ͂     ܂     ��     �     �     5�     H�     _�     n�     w�     ��     ��  S   ��     �  c   �  /   {�  (   ��     Ԅ     �     �  +   ��     (�     8�     J�     c�     ~�  
   ��     ��  1   ��  f   ԅ  =   ;�  8   y�      ��     ӆ  3   چ     �     �     �     0�     ?�     M�     R�     h�     q�     �  &   ��     ��     Ƈ     ܇     �      �  *   &�     Q�     Y�     g�     x�     ��     ��  X   ��     �     �  +   '�     S�     W�     _�     v�     ��     ��     ��     Ɖ     ؉     �     ��     �     �     +�     F�     O�     n�     ��     ��     ��     ��     Պ     �  �   ��  #   ��  &   ��  #   Ջ     ��  	   �     �     5�     L�     ^�     m�     �     ��     ��  &   Ռ  =   ��     :�  ,   R�  "   �  !   ��  %   č     �     
�  *    �  @   K�  "   ��     ��     ��     Ǝ  #   Վ     ��      �     4�     G�     `�     h�  	   ~�     ��     ��     ��     ��     ��     ˏ  	   ��  '   �     �     �     *�     2�     9�     A�     G�     Y�     n�     ��  .   ��  ;   Ґ  �  �  P   �  T   ^�  M   ��  L   �  0   N�  -   �  ^   ��  �   �  �   ��  �   w�  �   /�  �   �  �   ��  c   �  �   {�  �   �  �   ՚  �   [�  x   ܛ  {   U�  �   ќ  e   ��  G   ��  I   D�  �   ��  ,   �  y   M�  �   Ǡ  W   K�  �   ��  �   +�  F   ڢ     !�  �   )�  �   ��     ��     ��     ��  #   ��     Ĥ     �     �      �  8   5�     n�     s�  V   ��     �     ��     �     '�     9�  _   H�  $   ��     ͦ     ߦ  *   �     �     7�  $   Q�  S   v�     ʧ  O   ާ     .�     7�     G�     \�     i�  O   p�     ��     Ш     ب  �   �     ©     ҩ     �     �     ��  h   ��  �   f�  �   ��     ��  	   ë     ͫ  
   ԫ     ߫     �  	   ��     	�     �     &�  	   ,�     6�     J�     \�     r�     z�  
   ��     ��     ��     ��     ��     ¬     ڬ     �     �     ��     �  
   )�  	   4�     >�     C�  
   L�     W�     e�     {�     ��     ��  	   ��     ��     ��  
   ��     ��     ֭     ߭     �     �     �     �     �  	   %�     /�     5�     <�     H�     `�     w�  	   ��     ��     ��     ��     ��     ��     Ů     ݮ  	   �     ��     �     �     #�  	   3�  	   =�     G�     c�     z�     ��     ��     ��     ��     ��     ů     ԯ     �     �     ��     �     #�     +�     :�  
   N�     Y�     g�     w�     ��     ��     ��     ��  
   ��     Ű     Ұ     ۰  	   �     �  	   �     �     �  '    �  !   H�     j�     ��     ��     ��     ��     ű  �  α     ��     ��     µ   		This plugin provides bar for bookmarks.
		 %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i Attachment %i Attachments %i _Backlink %i _Backlinks %i errors occurred, see log %i file will be deleted %i files will be deleted %i open item %i open items %i warnings occurred, see log <Top> <Unknown> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Action Add Application Add Bookmark Add Notebook Add bookmark/Show settings Add column Add new bookmarks to the beginning of the bar Add row Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files All Tasks Allow public access Always use last cursor position when opening a page An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Arithmetic Attach File Attach image first Attachment Browser Attachments Attachments: Author Auto
Wrap Auto indenting Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave interval in minutes Autosave version on regular intervals Autosave version when the notebook is closed Back to Original Name BackLinks BackLinks Pane Backend Backlinks: Bazaar Bookmarks BookmarksBar Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Capture whole screen Center Change columns Changes Characters Characters excluding spaces Check Checkbox '>' Check Checkbox 'V' Check Checkbox 'X' Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Clone row Code Block Column 1 Command Command does not modify data Comment Common include footer Common include header Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Created Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Demote Dependencies Description Details Diagram Discard note? Distraction Free Editing Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root Due E_xport... Edit %s Edit Custom Tool Edit Image Edit Link Edit Table Edit _Source Editing Editing file: %s Enable Version Control? Enable plugin Enabled Equation Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Fossil GNU R Plot Get more plugins online Get more templates online Git Gnuplot Grid lines Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide menubar in fullscreen mode Highlight current line Home Page Horizontal _Line Icon Images Import Page Include subpages Index Index page Inline Calculator Insert Code Block Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Sequence Diagram Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Key Binding Key bindings Key bindings can be changed by clicking on a field with a key combination
in the list and then press the new key binding.
To disable a keybinding, select it in the list and use <tt>&lt;Backspace&gt;</tt>. Labels marking tasks Last Modified Leave link to new page Left Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Managing table columns Map document root to URL Match _case Maximum number of bookmarks Maximum page width Menubar Mercurial Modified Month Move Selected Text... Move Text to Other Page Move column ahead Move column backward Move text to Name Need output file to export MHTML Need output folder to export full notebook New File New Page New S_ub Page... New _Attachment Next No Applications Found No changes since last version No dependencies No plugin available to display objects of type: %s No such file: %s No such page: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Only Show Active Tasks Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open cell content link Open help Open in New Window Open in New _Window Open new page Open plugins folder Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Name Page Template Page already exists: %s Page not allowed: %s Page section Paragraph Paste Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please select a row, before you push the button. Please specify a notebook Plugin Plugin "%s" is required to display this object Plugins Port Position in the window Pr_eferences Preferences Prev Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Remove column Remove row Removing Links Rename page "%s" Repeated clicking a checkbox cycles through the checkbox states Replace _All Replace with Restore page to saved version? Rev Right Right Side Pane Right margin position Row down Row up S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Screenshot Command Search Search _Backlinks... Search this section Section Section(s) to ignore Section(s) to index Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Sequence Diagram Server not started Server started Server stopped Set New Name Set default text editor Set to Current Page Show Line Numbers Show Tasks as Flat List Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show full Page Name Show full page name Show helper toolbar Show in the toolbar Show right margin Show tasklist in sidepane Show the cursor also for pages that can not be edited Show the page title heading in the ToC Single _page Size Smart Home key Some error occurred while running "%s" Sort alphabetically Sort pages by tags Source View Spell Checker Start Start _Web Server Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Tags for non-actionable tasks Task Task List Tasks Template Templates Text Text Files Text From _File... Text background color Text foreground color The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The following parameters will be substituted
in the command when it is executed:
<tt>
<b>%f</b> the page source as a temporary file
<b>%d</b> the attachment directory of the current page
<b>%s</b> the real page source file (if any)
<b>%p</b> the page name
<b>%n</b> the notebook location (file or folder)
<b>%D</b> the document root (if any)
<b>%t</b> the selected text or word under cursor
<b>%T</b> the selected text including wiki formatting
</tt>
 The inline calculator plugin was not able
to evaluate the expression at the cursor. The table must consist of at least on row!
 No deletion done. There are no changes in this notebook since the last version that was saved This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This page name cannot be used due to technical limitations of the storage This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a sequence diagram editor for zim based on seqdiag.
It allows easy editing of sequence diagrams.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This plugin turns one section of the notebook into a journal
with a page per day, week or month.
Also adds a calendar widget to access these pages.
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox '>' Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Tray Icon Turn page name into tags for task items Type Un-check Checkbox Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unkown Image type Unkown Object Unspecified Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use a page for each Use date from journal pages Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s With this plugin you can embed a 'Table' into the wiki page. Tables will be shown as GTK TreeView widgets.
Exporting them to various formats (i.e. HTML/LaTeX) completes the feature set.
 Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Your system encoding is set to %s, if you want support for special characters
or see errors due to encoding, please ensure to configure your system to use "UTF-8" Zim Desktop Wiki Zoom _Out _About _Add _All Panes _Arithmetic _Attachment... _Back _Browse _Bugs _Cancel _Checkbox _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Duplicate Line _Edit _Edit Link _Edit Link or Object... _Edit Properties _Edit... _Emphasis _FAQ _File _Find _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _Move Here _Move Line Down _Move Line Up _New Page... _Next _None _Normal Size _Numbered List _OK _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Print _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove _Remove Line _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In as due date for tasks as start date for tasks calendar:week_start:0 do not use horizontal lines no grid lines readonly seconds translator-credits vertical lines with lines {count} of {total} Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2021-04-06 14:26+0000
Last-Translator: Kett Lovahr <vagnerlunes@gmail.com>
Language-Team: Portuguese (Brazil) <https://hosted.weblate.org/projects/zim/master/pt_BR/>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Weblate 4.6-dev
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
 		Este plug-in fornece uma barra para os marcadores.
		 %(cmd)s
retornou status de saída não-zero %(code)i %(n_error)i erros e %(n_warning)i avisos ocorreram, consulte o log %A, %d de %B de %Y %i anexo %i anexos %i _Link de Entrada %i _Links de entrada %i erros ocorreram, consulte o log %i arquivo será deletado %i arquivos serão deletados %i item aberto %i itens abertos %i avisos ocorreram, consulte o log <Top> <Desconhecido> Uma wiki para o desktop O arquivo com o nome <b>"%s"</b> já existe.
Você pode usar outro nome ou sobrescrever o arquivo existente. Uma tabela deve ter ao menos uma coluna Ação Adicionar Aplicativo Adicionar marcador Adicionar Caderno Adicionar marcador / Mostrar configurações Adicionar coluna Adicione novos marcadores no início da barra Adicionar linha Este plugin adiciona verificação ortográfica através do gtkspell.

Este é um plugin interno que acompanha o zim.
 Alinhamento Todos os Arquivos Todas as Tarefas Permitir acesso público Sempre use a última posição do cursor ao abrir uma página Ocorreu um erro ao gerar a imagem.
Você quer salvar o texto de origem de qualquer maneira? Fonte Anotada da Página Aplicativos Aritmética Anexar Arquivo Anexar imagem primeiro Navegador de Anexos Anexos Anexos: Autor(a) Quebra de
linha automática Indentação automática Versão automaticamente salva pelo Zim Automaticamente selecionar a palavra atual quando aplicar formatação Automaticamente tornar palavras "CamelCase" em links Automaticamente tornar caminhos de arquivo em links Intervalo de salvamento automático em minutos Salvar automaticamente a versão em intervalos regulares Versão de salvamento automático quando o notebook é fechado Voltar para Nome Original Backlinks Painel do BackLinks Backend Links de Entrada: Bazaar Marcadores Barra de favoritos Painel Inferior Procurar Lista com _Marcadores C_onfigurar Não foi possível modificar a página: %s Cancelar Capturar toda a tela Centro Gerenciar colunas Modificações Caracteres Caracteres excluindo os espaços Marcar caixa de seleção com '>' Marcar caixa de seleção com 'V' Marcar caixa de seleção com 'X' Verificar _ortografia Lista de Cai_xa de Seleção Ícone na área de notificação clássico,
não usar o novo estilo de ícone de status do Ubuntu Limpar Clonar linha Bloco de código Coluna 1 Comando Comando não modifica dados Comentário Incluir rodapé usual Incluir cabeçalho usual Todo o _Caderno Configurar Aplicativos Configurar Plugin Configure um aplicativo para abrir links "%s" Configure um aplicativo para abrir arquivos
do tipo "%s" Considerar todas as caixas de seleção como tarefas Copiar Copiar endereço de e-mail Copiar Modelo Copiar _como... Copiar _Link Copiar L_ocalização O executável "%s" não foi encontrado Não foi possível encontrar o caderno: %s Não foi possível encontrar o modelo "%s" Não foi possível encontrar o arquivo ou pasta para este caderno Não foi possível carregar a verificação ortográfica Não foi possível abrir %s Não foi possível analisar a expressão Não foi possível ler: %s Não foi possível salvar a página: %s Criar uma nova página para cada anotação Criar pasta? Criado Recor_tar Ferramentas Personalizadas Ferramentas _personalizadas Personalizar... Data Dia Padrão Formato padrão para cópia de texto da área de transferência Caderno padrão Atraso Excluir página Excluir página "%s"? Apagar linha Rebaixar Dependências Descrição Detalhes Diagrama Descartar nota? Edição Livre de Distrações Você quer apagar todos os marcadores? Você quer restaurar a página: %(page)s
à versão salva: %(version)s?

Todas as modificações desde a última versão salva serão perdidas! Documento raiz Vencimento E_xportar... Editar %s Editar Ferramenta Personalizada Editar Imagem Editar Ligação Editar Tabela Editar código fonte Editando Editando arquivo: %s Ativar Controle de Versão? Habilitar plugin Habilitar Equação Erro em %(file)s na linha %(line)i próximo "%(snippet)s" Avaliar _Matemática Expandir _Tudo Exportar Exportar todas as páginas para um único arquivo exportação concluída Exportar cada página para um arquivo separado Exportando caderno Falhou Falha ao executar: %s Erro ao executar o programa: %s Arquivo Já Existe Modelos de _Arquivo Arquivo foi alterado externamente: %s O arquivo existe Não é possível escrever no arquivo: %s Tipo de arquivo não suportado: %s Nome do arquivo Filtrar Procurar Encontrar Pró_ximo Encontrar An_terior Encontrar e Substituir Localizar Sinalizar antes do final de semana as tarefas com prazo final na segunda-feira ou terça-feira Pasta A pasta já existe e possui conteúdo, exportar para esta pasta pode sobrescrever arquivos existentes. Você deseja continuar? Diretório existe: %s Pasta com modelos para anexos Para buscas avançadas você pode usar operadores como
AND, OR e NOT. Veja a página de ajuda para mais detalhes. For_matar Formato Fossil GNU R Plot Obtenha mais plugins on-line Obtenha mais modelos on-line Git Gnuplot Linhas de grade Cabeçalho _1 Cabeçalho _2 Cabeçalho _3 Cabeçalho _4 Cabeçalho _5 Alturar Esconder menu no modo tela cheia Realçar a linha atual Página Inicial _Linha Horizontal Ícone Imagens Importar Página Incluir sub-páginas Índice Página do índice Calculadora Integrada Inserir Bloco de Código Inserir Data e Hora Inserir Diagrama Inserir Ditaa Inserir Equação Inserir Plot do GNU R Inserir Gnuplot Inserir Imagem Inserir Link Inserir Partitura Inserir Captura de Tela Inserir Diagrama de Sequência Inserir Símbolo Inserir Tabela Inserir Texto à Partir de um Arquivo Interface Palavra Chave Interwiki Diário Pular para Pular para Página Atalho do teclado Atalhos do teclado Atalhos de teclado podem ser alterados clicando num campo com uma combinação de teclas
na lista e então pressionando o novo atalho.
Para desativar um atalho, selecione-o na lista e use <tt>&lt;Backspace&gt;</tt>. Rótulos marcando tarefas Última Modificação Manter link para a nova página Esquerdo Painel Lateral Esquerdo Ordenador de Linhas Linhas Mapa de Links Vincular arquivos do documento raiz com o caminho completo do arquivo Ligar à Localização Recordar eventos com o Zeitgeist Arquivo de log Parece que você encontrou um erro Tornar aplicativo padrão Gerenciar colunas de tabelas Mapear documento raiz para URL Diferenciar maiúsculas e _minúsculas Número máximo de marcadores Largura máxima da página Barra de menu Mercurial Modificado Em Mês Mover Texto Selecionado... Mover Texto para Outra Página Mover coluna à frente Mover coluna para trás Mover texto para Nome Necessário arquivo de saída para exportar MHTML Necessário pasta de saída para exportar o caderno completo Novo Arquivo Nova página Nova S_ub Página... Novo An_exo Próx. Nenhum Aplicativo Encontrado Nenhuma mudança desde a última versão Sem dependências Nenhum plugin disponível para mostrar objetos do tipo: %s Arquivo não existe: %s Nenhuma página: %s Nenhuma wiki desse tipo definida: %s Nenhum modelo instalado Caderno Cadernos OK Mostrar apenas as tarefas ativas. Abrir pasta dos _anexos Abrir pasta Abrir Caderno Abrir Com... Abrir _documento raiz Abrir pasta do _Caderno Abrir _Página Abrir link contido na célula Abrir ajuda Abrir em uma nova janela Abrir em nova _janela Abrir nova página Abrir pasta de plugins Abrir com "%s" Opcional Opções para o plug-in %s Outro... Arquivo de saída Arquivo de saída existe , especifique "--sobrescrever" para forçar a exportação Diretório de destino Pasta de saída existe e não está vazio, especifique "--sobrescrever" para forçar a exportação Local de saída necessário para a exportação Saída deve substituir a seleção atual Sobrescrever B_arra de Caminhos Página Página "%s" não tem uma pasta para anexos Nome da página Modelo de Página A página já existe: %s Página não permitida: %s seção da página Parágrafo Colar Por favor digite um comentário para esta versão Lembre se que criar um link para uma página não existente
também cria essa página automaticamente. Selecionar um nome e uma pasta para o caderno de anotações. Por favor selecione uma linha antes de apertar o botão. Por favor especifique um caderno Plugin Plugin "%s" é necessário para mostrar esse objeto Plugins Porta Posição na janela _Preferências Preferências Ant. Imprimir no navegador Promover Pr_opriedades Propriedades Passa os eventos ao serviço Zeitgeist Anotação Rápida Anotação Rápida... Mudanças recentes Mudanças recentes... Páginas _alteradas recentemente Reformatar a marcação wiki em tempo real Remover Remover Todos Remover a coluna Remover a linha Removendo links Renomear página "%s" Cliques repetidos numa caixa de seleção alterna entre os estados da caixa de seleção Substituir _Tudo Substituir por Restaurar página à última versão salva? Rev Direito Painel Lateral Direito Posição da margem direita Linha para baixo Linha para cima S_alvar versão... Salvar _Cópia... Salvar Cópia Salvar Versão Salvar marcadores Versão salva do zim Ocorrência(s) Comando de captura de tela Procurar Pesquisar _Links de Entrada... Pesquisar nesta seção Seção Seção(ões) a ignorar Seção(ões) para indexar Selecione o Arquivo Selecione a Pasta Selecionar Imagem Seleciona uma versão para ver as diferenças entre aquela versão e a atual.
Ou selecione múltiplas versão para ver as mudanças entre elas.
 Selecione o formato de exportação Selecione o arquivo ou pasta de saída Selecione as páginas para exportar Selecionar janela ou área Seleção Diagrama de Sequência Servidor não iniciado Servidor iniciado Servidor parou Definir Novo Nome Definir editor de texto padrão Definir como Página Atual Mostrar número das linhas Mostrar as tarefas como lista simples. Mostrar TdC como um widget flutuante e não no painel lateral Mostrar _Modificações Mostrar um ícone separado para cada caderno Mostrar o nome completo da página Exibir o nome completo da página Mostrar barra de ferramentas auxiliar Mostrar na barra de ferramentas Exibir margem direita Mostrar lista de tarefas no painel lateral Mostrar o cursor também em páginas que não podem ser editadas Mostra o título da página na TdC Página única Tamanho Smart Home key Algum erro ocorreu ao executar "%s" Organizar alfabeticamente Organizar páginas por etiquetas Visualizar código Verificador Ortográfico Início Iniciar _Servidor Web Sí_mbolo Sintaxe Padrão do Sistema Largura da aba Tabela Editor de Tabelas Tabela de Conteúdos Etiquetas Etiquetas para tarefas não-acionáveis Tarefa Lista de Tarefas Tarefas Modelo Modelos Texto Arquivos de texto Texto de _Arquivo... cor de fundo do texto cor de primeiro plano do texto A pasta
%s
não existe.
Deseja criá-la agora? O diretório "%s" ainda não existe.
Deseja criá-lo agora? Os seguintes parâmetros serão substituídos 
no comando quando ele for executado:
<tt>
<b>%f</b> O código fonte da página como um arquivo temporário.
<b>%d</b> O diretório de anexos da página atual.
<b>%s</b> O arquivo de origem da página real (se houver).
<b>%p</b> O nome da página.
<b>%n</b> O local do notebook (arquivo ou pasta).
<b>%D</b> A raiz do documento (se houver).
<b>%t</b> O texto ou palavra selecionado sob o cursor.
<b>%T</b> O texto selecionado, incluindo a formatação wiki.
</tt>
 O plugin de calculadora integrada não conseguiu
avaliar a expressão no cursor. A tabela deve ser constituída por pelo menos uma linha!
 Apagamento não executado. Não há mudanças neste caderno de anotações desde a última versão salva Isso pode significar que você não tem
os dicionários corretos instalados. Este arquivo já existe.
Deseja sobrescrevê-lo? Esta página não possui uma pasta de anexos. Este nome de página não pode ser utilizado devido a limitações técnicas de armazenamento. Este plugin permite fazer uma captura da tela e
inseri-lá diretamente numa página do zim.

Este é um plugin interno que acompanha o zim.
 Esse plugin apresenta um diálogo mostrando todas as tarefas
em aberto no caderno. Tarefas em aberto podem ser caixas
abertas ou items com etiquetas como "TODO" ou "FIXME".

Este é um plugin interno que acompanha o zim.
 Esse plugin adiciona uma caixa de diálogo para colocar rapidamente
texto ou conteúdo da área de transferência numa página do zim.

Este é um plugin interno que acompanha o zim.
 Esse plugin adiciona um ícone na área de notificação para acesso rápido.

Esse plugin depende do Gtk+, versão 2.10 ou superior.

Este é um plugin interno que acompanha o zim.
 Este plugin adiciona um widget extra mostrando uma lista de páginas
que contêm links para a página atual.

Este é um plugin interno que acompanha o zim.
 Este plugin adiciona um widget extra que apresenta uma tabela de conteúdos para a página atual.

Este é um plugin padrão que acompanha o Zim.
 Este plugin adiciona configurações que ajudam a usar o Zim
como um editor livre de distrações.
 Esse plugin adiciona o diálogo "Inserir Símbolo" e permite
a auto-formatação de caractéres tipográficos.

Este é um plugin interno que acompanha o zim.
 Esse plugin adiciona controle de versão aos cadernos.

Esse plugin suporta os sistemas de controle de versão: Bazaar, Git e Mercurial.

Este é um plugin interno que acompanha o zim.
 Este plugin permite embutir cálculos aritméticos no Zim.
Baseia-se no módulo de aritmética de
http://pp.com.mx/python/arithmetic
 Esse plugin permite avaliar rapidamente simples
equações matemáticas no zim.

Este é um plugin interno que acompanha o zim.
 Esse plugin fornece um editor de diagramas para o zim baseado no Ditaa.

Este é um plugin interno que acompanha o zim.
 Esse plugin fornece um editor de diagramas para o zim baseado no GraphViz.

Este é um plugin interno que acompanha o zim.
 Esse plugin apresenta um diálogo gráfico
da estrutura dos vínculos do caderno.
Pode ser usado como um "mind map"
do relacionamento das páginas.

Este é um plugin interno que acompanha o zim.
 Esse plugin fornece um índice de páginas filtrado através da seleção de etiquetas em uma nuvem.
 Esse plugin fornece um editor de plotagem para o zim baseado no GNU R.
 Esse plugin fornece um editor de plotagem para o zim baseado no Gnuplot.
 Este plugin fornece um editor de diagrama de sequência para o Zim baseado no seqdiag. Ele permite a fácil edição de diagramas de sequência.
 Este plugin fornece um método para lidar com a falta
de suporte de impressão no Zim. Ele exporta a página atual
em html e abre-a no navegador. Assumindo um navegador
com suporte à impressão isso fará sua informação
ser impressa em dois passos.

Este é um plugin interno que acompanha o Zim.
 Esse plugin fornece um editor de equações para o zim baseado no latex.

Este é um plugin interno que acompanha o zim.
 Esse plugin proporciona um editor de partitura para o Zim baseado no GNU Lilypond.

Este é um plugin interno que acompanha o Zim.
 Este plugin mostra a pasta de anexos da página atual como
ícones no painel inferior.
 Esse plugin ordena alfabeticamente as linhas selecionadas.
Se a lista já estiver ordenada, a ordem será invertida (de A-Z para Z-A).
 Este plugin transforma uma seção do caderno em um diário
com uma página por dia, semana ou mês.
Também acrescenta um widget de calendário para acessar essas páginas.
 Isso normalmente significa que o arquivo contém caracteres inválidos Título Para continuar você pode salvar uma cópia desta página ou descartar 
as mudanças realizadas. Se você salvar uma cópia, as mudanças também 
serão perdidas mas você poderá restaurar a cópia depois. Para criar um novo notebook que você precisa selecionar uma pasta vazia.
Claro que você também pode selecionar uma pasta de notebook Zim existente.
 TdC Ho_je Hoje Alternar caixa de seleção com '>' Alternar Caixa de Seleção 'V' Alternar Caixa de Seleção 'X' Painel Superior Ícone na Área de Notificação Transformar o nome de páginas em etiquetas para tarefas Tipo Desmarcar caixa de seleção Remover recuo ao usar a tecla <BackSpace>
(Se desativado você pode usar <Shift><Tab>) Desconhecido(a) Tipo de imagem desconhecido Objeto desconhecido Não especificado Sem marcadores Atualizar %i página vinculada à esta página Atualizar %i páginas vinculadas à esta página Atualizar o cabeçalho desta página Atualizando links Atualizando índice Use %s para alternar para o painel lateral Usar fonte personalizada Use uma página para cada Usar a data das páginas do diário. Usar a tecla <Enter> para seguir links
(Se desativado você pode usar <Alt><Enter>) Controle de Versão Controle de versão encontra-se desativado para este caderno.
Deseja ativá-lo? Versões Ver _Anotação Visualizar _Registro Servidor Web Semana Ao reportar um erro, por favor,
inclua a informação na caixa de texto abaixo. Toda a _palavra Largura Página Wiki: %s Com este plugin você pode inserir uma 'tabela' em uma página wiki. As tabelas serão mostradas como widgets GTK TreeView. Exportá-los para vários formatos (ou seja, HTML / LaTeX) completa o conjunto de recursos.
 Contar Palavras Contagem de Palavras... Palavras Ano Ontem Você está editando um arquivo em um aplicativo externo. Você pode fechar esta janela quando terminar. Você pode configurar ferramentas personalizadas que irão aparecer
no menu de ferramentas e na barra de ferramentas ou nos menus de contexto. A codificação do sistema está definida como %s, se quiser suporte para caracteres especiais
ou ver erros por conta da codificação, por favor configure seu sistema para que use "UTF-8" Zim Desktop Wiki _Diminuir _Sobre _Adicionar _Todos os Painéis _Aritmética _Anexo... _Voltar uma página _Navegar _Bugs _Cancelar _Caixa de seleção _Descer um nível Remo_ver Formatação _Fechar _Contrair Tudo _Conteúdo _Copiar _Copiar aqui _Apagar _Excluir Página _Descartar alterações _Duplicar Linha _Editar _Editar Link _Editar Ligação ou Objeto _Propriedades _Editar… _Itálico _FAQ _Arquivo _Localizar _Encontrar... _Avançar uma página Tela _Cheia _Navegar _Ajuda _Destacar _Histórico _Início _Imagem... _Importar Página... _Inserir _Pular Ir _para... _Atalhos do teclado _Link _Link para data _Link... _Realçar _Mais _Mover _Mover Aqui Mover Linha para _Baixo Mover Linha para _Cima _Nova Página... _Próximo _Nenhum Tamanho _Normal Lista _Numerada _OK _Abrir _Abrir Outro Caderno... _Outro(s)... _Páginas _Hierarquia de Página _Subir um nível Co_lar _Visualização _Anterior Im_primir Visualizar no navegador Web _Anotação Rápida... _Sair _Páginas recentes Re_fazer Expressão _regular _Recarregar _Remover _Remover Linha _Remover Link _Substituir S_ubstituir... _Restaurar Tamanho _Restaurar Versão Sal_var S_alvar Cópia _Captura de Tela... _Pesquisar _Pesquisar... Enviar _Para... Painéis _Laterais _Lado à Lado _Ordenar linhas _Tachado N_egrito S_ubscrito S_obrescrito _Modelos _Ferramentas _Desfazer Forma _Literal _Versões _Visualizar A_mpliar como data de vencimento para as tarefas Como data de início das tarefas. calendar:week_start:0 não utilizar linhas horizontais sem linhas de grade somente leitura segundos Launchpad Contributions:
  Alexandre Magno https://launchpad.net/~alexandre-mbm
  André Gondim https://launchpad.net/~andregondim
  André Marcelo Alvarenga https://launchpad.net/~andrealvarenga
  Clodoaldo https://launchpad.net/~clodoaldo-carv
  Frederico Gonçalves Guimarães https://launchpad.net/~fgguimaraes
  Frederico Lopes https://launchpad.net/~frelopes
  Giovanni Abner de Brito Júnior https://launchpad.net/~giovannijunior
  Jaap Karssenberg https://launchpad.net/~jaap.karssenberg
  Lewis https://launchpad.net/~akai-hen
  Marco https://launchpad.net/~marcodefreitas
  Matheus https://launchpad.net/~matheus-rwahl
  NeLaS https://launchpad.net/~organelas
  Otto Robba https://launchpad.net/~otto-ottorobba
  Paulo J. S. Silva https://launchpad.net/~pjssilva
  Thaynã Moretti https://launchpad.net/~izn
  ThiagoSerra https://launchpad.net/~thiagoserra
  Wanderson Santiago dos Reis https://launchpad.net/~wasare
  mtibbi https://launchpad.net/~marcio-tibirica linhas verticais com linhas de grade {count} de {total} 