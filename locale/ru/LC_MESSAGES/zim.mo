��                       (  ,   (  .   :(  ?   i(     �(     �(     �(     �(  0   	)     :)     U)     s)  	   y)     �)  i   �)  *   �)     '*     .*     >*     K*     X*  
   s*  -   ~*     �*  V   �*     +  	   +  	   +     %+  3   9+  Y   m+     �+     �+  
   �+     �+     	,     ,     (,     ;,     G,     T,  	   [,     e,  $   t,  ?   �,  /   �,  (   	-     2-  %   O-  ,   u-     �-  	   �-     �-     �-  
   �-     �-  	   �-     �-     .     .     .     ".  
   /.     :.     R.     Y.     n.     u.     �.  
   �.     �.     �.     �.     �.     �.     �.  <   /     H/  	   N/  
   X/     c/     l/     t/     �/     �/     �/     �/     �/     �/  +    0  3   ,0      `0     �0     �0     �0     �0  
   �0     �0     �0     �0     1  3   %1     Y1     w1     �1     �1     �1     �1     �1     �1     2     2     2     '2     42     92     =2  0   E2     v2     �2     �2     �2  
   �2     �2     �2     �2     �2     �2     �2     �2  $   3  ~   23     �3     �3  
   �3     �3     �3  
   �3  	   �3  
   �3     4     4     4     -4     E4     S4     [4  5   d4     �4     �4     �4  !   �4     �4  #   �4     5     &5     -5     @5     ^5     j5     }5     �5     �5     �5     �5     �5     6     6     6  
   6     %6     46  	   E6  6   O6     �6  v   �6     7  *   7  c   A7     �7     �7     �7  
   �7     �7     �7     �7     �7  
   8  
   8  
   8  
   %8  
   08  
   ;8     F8     M8     h8     �8  	   �8     �8     �8     �8     �8     �8     �8  
   �8     �8     9     9     -9     <9     I9     Y9     k9     z9     �9     �9     �9     �9     �9     �9     �9  	   �9     :     :     :     ':     4:     @:     M:     b:     p:     �:     �:     �:     �:     �:  2   �:     �:     �:     �:     ;     ;     8;     Q;     h;     �;     �;     �;     �;  	   �;     �;     �;     �;     �;     <     <     2<     ?<      D<  *   e<     �<     �<     �<     �<     �<     �<     �<     �<     �<     =  *   ,=  2   W=     �=     �=     �=     �=     �=  	   �=     �=     �=     	>     ">     .>     <>     I>     ]>  
   s>     ~>  	   �>     �>     �>     �>     �>     �>     �>      ?     ?     ?  9   +?     e?  I   s?  !   �?  '   �?  	   @     @     @  0   @  
   P@  	   [@     e@     s@     �@     �@  	   �@     �@     �@     �@  '   �@  V   �@  3   NA  0   �A  (   �A     �A     �A  .   �A     ,B     4B     9B     PB     ]B     iB     nB     B     �B  
   �B  &   �B  
   �B     �B     �B     �B     �B     C     7C  
   >C     IC  
   WC     bC     qC  ?   �C     �C     �C     �C     �C     D     D     D     ,D     BD     KD     RD     cD  	   sD     }D     �D     �D     �D     �D     �D     �D     �D     �D     E     E     *E     6E     DE  �   QE     �E      �E     F     6F  	   NF     XF     iF     |F     �F     �F     �F     �F     �F     �F  2   �F     0G  &   >G     eG     yG     �G     �G     �G     �G  5   �G  &   H     >H     KH     PH  &   _H     �H     �H     �H     �H     �H     �H  
   �H     �H     �H  	    I     
I     I     I     /I     4I     RI  	   WI     aI     gI  	   pI     zI  
   I     �I     �I     �I  ?   �I  A   	J  �  KJ  S   L  =   cL     �L  K   �L  @   �L  6   4M  -   kM  I   �M  x   �M  �   \N  �   O  �   �O  �   QP  �   �P  }   `Q  L   �Q  �   +R  9   �R  �   �R  �   �S  �   $T  }   �T  C   1U  h   uU  k   �U  �   JV  -   W  R   LW  ;   �W  =   �W  v   X    �X  j   �Y  n   Z  ]   ~Z     �Z  �   \[  7   �[     )\  �   /\  |   �\     H]     L]     S]     Y]     m]     �]     �]  	   �]  '   �]     �]     �]  D   �]     ,^     4^     F^     T^     `^  H   i^     �^     �^     �^  !   �^     _     $_     8_  P   T_     �_     �_  U   �_     `     `  	   -`  
   7`     B`  N   G`     �`     �`     �`  �   �`  
   qa     |a     �a     �a  	   �a  ^   �a  f   �a     eb  	   vb     �b     �b  
   �b     �b     �b     �b     �b     �b     �b  	   �b     �b     �b     �b     �b  	   c     c  
   c     !c     )c     6c     Gc     Wc  
   ]c     hc     �c     �c  	   �c     �c     �c     �c     �c     �c     �c     �c     �c  
   �c     �c     �c  	   �c     d     d     d     d     +d     8d     >d     Ld     Ud     [d     ad  
   gd     rd     �d     �d     �d     �d     �d     �d     �d     �d     �d     �d  	   �d     e     e     e     #e     *e  	   3e     =e     De     Ve     ee     ke     ye     e     �e     �e     �e     �e     �e     �e     �e     �e     �e  
   �e      f     f  
   f     "f     .f     :f     Hf     Tf     \f  
   df     of  
   |f     �f     �f  	   �f     �f     �f     �f     �f     �f     �f  
   �f     	g     g     (g     1g     9g     Lg  
   [g     fg  '  yg  [   �i  L   �i  |   Jj     �j  ;   �j  e   k  E   vk  u   �k  `   2l  U   �l  
   �l     �l     m  �   m  \   �m     5n  %   Fn  !   ln     �n  E   �n     �n  I   o     ^o  �   |o      p     9p     Kp  2   _p     �p  �   q  B   �q     �q     r     r     7r  '   Wr  !   r     �r     �r  
   �r     �r     �r  >   �r  ~   <s     �s  N   ;t  ?   �t  >   �t  c   	u  .   mu     �u  *   �u  ,   �u     v     1v     8v     Iv     gv     �v  
   �v  '   �v     �v  9   �v     w  &   *w     Qw     cw     �w     �w  /   �w     �w  6   �w  6   #x  (   Zx  )   �x  �   �x     Zy  #   ky     �y     �y     �y  1   �y     �y  .   
z  2   9z     lz  '   �z     �z  U   �z  ^   ){  G   �{     �{  A   �{  !   '|     I|  "   i|      �|  C   �|  1   �|  0   #}  \   T}  L   �}  &   �}  9   %~  *   _~  ;   �~  Q   �~          3     @  %   R  &   x     �     �     �     �  r   �  &   R�     y�  !   ��  '   ��     Ԁ     ��     �     �     )�     @�      S�  6   t�  9   ��  �   �  .   ҂     �     �     *�  D   H�  1   ��  '   ��     �  7   �     ?�  +   \�  /   ��     ��     ք     �  J   �  "   ?�     b�     �  T   ��     �  Y   �  .   ]�     ��     ��  4   ��     �     �  *   '�     R�  ,   p�  3   ��  3   ч  7   �     =�     O�  
   \�     g�      ��     ��     ƈ  k   ؈  
   D�  �   O�  #   "�  I   F�  �   ��     _�     m�     z�     ��  >   ��  >   �     &�     *�     2�     H�     ^�     t�     ��     ��     ��  G   Ì  @   �  4   L�  !   ��  (   ��     ̍     ٍ     ��  4   �     C�     X�  +   x�  "   ��  '   ǎ     �     �     $�  #   @�     d�  '   {�     ��  *   ��  (   �  J   �     `�     ~�  +   ��     ʐ  *   ݐ     �     �  "   &�     I�     g�     ��  %   ��  >   ȑ     �  &   "�  !   I�  
   k�     v�  B   ��     ђ     �  B    �     C�  *   P�  =   {�  6   ��  ^   �  "   O�  >   r�  6   ��     �  	   �     ��  
   
�  9   �  D   O�  2   ��  0   Ǖ  $   ��     �  E   $�  i   j�  5   Ԗ     
�  *   �  !   I�  )   k�     ��  	   ��  (   ��  8   �     �  [   >�  j   ��     �  '   #�  ,   K�  *   x�     ��     ��     Ù  ?   ƙ  1   �     8�     R�  #   p�  ?   ��  +   Ԛ       �  E   !�     g�  %   ��  &   ��  *   қ  -   ��  '   +�     S�  &   n�     ��     ��  �   ��     U�  �   q�  Y   &�  B   ��     Þ     ܞ     ��  D   �  %   M�     s�     ��  /   ��  +   ٟ     �  
   #�     .�     ;�     L�  D   b�  �   ��  <   f�  b   ��  W   �  B   ^�     ��  \   ��     �     �     !�     @�     T�  	   g�     q�     ��     ��     ��  ;   ģ      �      �  #   ?�  &   c�  4   ��  F   ��     �     �     +�     I�     e�  2   ��  s   ��     *�     C�  /   Y�  S   ��     ݦ     �  (   	�  *   2�     ]�     s�  #   ��  !   ��  .   ѧ      �  #    �  #   D�     h�  (   ��  
   ��     ��  '   Ш     ��  '   �      -�     N�     f�  %   ��    ��  5   ��  <   �  9   )�  :   c�     ��  9   ��      �     �  !   (�  &   J�  P   q�  8   ¬  ,   ��  K   (�  s   t�  $   �  [   �  5   i�  5   ��  G   ծ  ?   �  ,   ]�  M   ��  q   د  f   J�     ��     ̰     ٰ  @   ��  ,   .�  9   [�  #   ��  %   ��     ߱  '   ��     �     -�  ?   @�     ��     ��     ��     Ͳ  
   �  5   ��     #�     0�     H�     U�     b�  
   q�     |�     ��     ��     ˳  Q   �  Y   3�    ��  z   ��  �   &�     ��  i   ø  r   -�  @   ��  F   �  o   (�  �   ��  i  i�  {  Ӽ    O�    V�    k�  �   ��  �   s�    �  y   $�  ;  ��  g  ��  �   B�  �   "�  �   �  �   ��  �   Z�  �  �  X   ��  �   �  {   ��  �   �    ��  �  ��  �   j�  �   +�  �   ��    {�  0  ��  n   ��     0�  O  C�  �   ��     ��     ��     ��  &   ��  &   ��  &   �     5�  5   Q�  V   ��     ��  +   ��  �   �     ��  4   ��  #   ��     �     �  �   7�  4   2�  !   g�  )   ��  Z   ��  ,   �  ?   ;�  D   {�  �   ��     ]�     }�  d   ��      �  *   �  $   8�     ]�     q�  �   ~�  &   �     E�     R�  I  p�     ��     ��     ��     ��  
   ��  �   �  �   ��     ��     ��     ��     ��     ��     ��     �     �     +�     7�     E�     S�     n�  .   ��     ��     ��     ��     ��     �     *�      :�  &   [�  &   ��     ��  (   ��  ?   ��  ,    �     M�     l�  /   z�  	   ��     ��     ��     ��  &   ��     �     �     $�  %   >�  "   d�     ��  !   ��     ��     ��     ��     ��     �     )�     E�     V�     p�     x�      ��  /   ��  1   ��  "   �  #   6�     Z�     n�  !   v�  &   ��     ��     ��  .   ��     �     �      %�     F�     _�  0   q�     ��     ��     ��  !   ��     �  "   �     7�  (   G�     p�     ��     ��     ��     ��     ��     ��  &   �     7�     K�     j�     ��     ��     ��     ��     ��  #   ��     �     �     -�     G�     c�     s�     ��     ��     ��     ��     ��  4   ��  4   �     K�     a�  '   �     ��     ��     ��  �  ��  #   ��     
�     �   		This plugin provides bar for bookmarks.
		 %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i Attachment %i Attachments %i _Backlink %i _Backlinks %i errors occurred, see log %i file will be deleted %i files will be deleted %i open item %i open items %i warnings occurred, see log <Top> <Unknown> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Action Add Application Add Bookmark Add Notebook Add bookmark/Show settings Add column Add new bookmarks to the beginning of the bar Add row Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files All Tasks Allow public access Always use last cursor position when opening a page An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Arithmetic Ascii graph (Ditaa) Attach File Attach image first Attachment Browser Attachments Attachments: Author Auto
Wrap Auto indenting Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave interval in minutes Autosave version on regular intervals Autosave version when the notebook is closed Back to Original Name BackLinks BackLinks Pane Backend Backlinks: Bazaar Bookmarks BookmarksBar Border width Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Capture whole screen Center Change columns Changes Characters Characters excluding spaces Check Checkbox '>' Check Checkbox 'V' Check Checkbox 'X' Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Clone row Code Block Column 1 Command Command does not modify data Comment Common include footer Common include header Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Created Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Demote Dependencies Description Details Diagram Discard note? Distraction Free Editing Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root Due E_xport... Edit %s Edit Custom Tool Edit Image Edit Link Edit Table Edit _Source Editing Editing file: %s Enable Version Control? Enable plugin Enabled Equation Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File name too long: %s File path too long: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Fossil GNU R Plot Get more plugins online Get more templates online Git Gnuplot Grid lines Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide Journal pane if empty Hide menubar in fullscreen mode Highlight current line Home Page Horizontal _Line Icon Images Import Page Include subpages Index Index page Inline Calculator Insert Code Block Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Sequence Diagram Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Key Binding Key bindings Labels marking tasks Last Modified Leave link to new page Left Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Managing table columns Map document root to URL Match _case Maximum number of bookmarks Maximum page width Menubar Mercurial Modified Month Move Selected Text... Move Text to Other Page Move column ahead Move column backward Move text to Name Need output file to export MHTML Need output folder to export full notebook Never wrap lines New File New Page New Page in %s New S_ub Page... New _Attachment Next No Applications Found No changes since last version No dependencies No document root defined for this notebook No plugin available to display objects of type: %s No such file: %s No such page: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Only Show Active Tasks Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open cell content link Open help Open in New Window Open in New _Window Open new page Open plugins folder Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Index Page Name Page Template Page already exists: %s Page not allowed: %s Page section Paragraph Password Paste Path Bar Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please select a row, before you push the button. Please select more than one line of text Please specify a notebook Plugin Plugin "%s" is required to display this object Plugins Port Position in the window Pr_eferences Preferences Prev Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Remove column Remove row Removing Links Rename page "%s" Repeated clicking a checkbox cycles through the checkbox states Replace _All Replace with Require authentication Restore page to saved version? Rev Right Right Side Pane Right margin position Row down Row up S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Screenshot Command Search Search _Backlinks... Search this section Section Section(s) to ignore Section(s) to index Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Sequence Diagram Server not started Server started Server stopped Set New Name Set default text editor Set to Current Page Show Line Numbers Show Tasks as Flat List Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show full Page Name Show full page name Show helper toolbar Show in the toolbar Show right margin Show tasklist in sidepane Show the cursor also for pages that can not be edited Show the page title heading in the ToC Single _page Size Smart Home key Some error occurred while running "%s" Sort alphabetically Sort pages by tags Source View Spell Checker Start Start _Web Server Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Tags for non-actionable tasks Task Task List Tasks Template Templates Text Text Files Text From _File... Text background color Text foreground color The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The following parameters will be substituted
in the command when it is executed:
<tt>
<b>%f</b> the page source as a temporary file
<b>%d</b> the attachment directory of the current page
<b>%s</b> the real page source file (if any)
<b>%p</b> the page name
<b>%n</b> the notebook location (file or folder)
<b>%D</b> the document root (if any)
<b>%t</b> the selected text or word under cursor
<b>%T</b> the selected text including wiki formatting
</tt>
 The inline calculator plugin was not able
to evaluate the expression at the cursor. The table must consist of at least on row!
 No deletion done. Theme There are no changes in this notebook since the last version that was saved This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This page name cannot be used due to technical limitations of the storage This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a "path bar" to the top of the window.
This "path bar" can show the notebook path for the current page,
recent visited pages or recent edited pages.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds the page index pane to the main window.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows inserting 'Code Blocks' in the page. These will be
shown as embedded widgets with syntax highlighting, line numbers etc.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin also has properties,
see the notebook properties dialog This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a macOS menubar for zim. This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a sequence diagram editor for zim based on seqdiag.
It allows easy editing of sequence diagrams.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This plugin turns one section of the notebook into a journal
with a page per day, week or month.
Also adds a calendar widget to access these pages.
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox '>' Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Tray Icon Turn page name into tags for task items Type Un-check Checkbox Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unkown Image type Unkown Object Unspecified Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use a page for each Use date from journal pages Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Username Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s With this plugin you can embed a 'Table' into the wiki page. Tables will be shown as GTK TreeView widgets.
Exporting them to various formats (i.e. HTML/LaTeX) completes the feature set.
 Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Zim Desktop Wiki Zoom _Out _About _Add _All Panes _Arithmetic _Attachment... _Back _Browse _Bugs _Cancel _Checkbox _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Duplicate Line _Edit _Edit Link _Edit Link or Object... _Edit Properties _Edit... _Emphasis _FAQ _File _Find _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _Move Here _Move Line Down _Move Line Up _New Page Here... _New Page... _Next _None _Normal Size _Numbered List _OK _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Print _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove _Remove Line _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In as due date for tasks as start date for tasks calendar:week_start:0 do not use horizontal lines no grid lines readonly seconds translator-credits vertical lines with lines {count} of {total} Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2021-03-28 01:29+0000
Last-Translator: Nikolay Korotkiy <sikmir@gmail.com>
Language-Team: Russian <https://hosted.weblate.org/projects/zim/master/ru/>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Weblate 4.6-dev
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
 		Этот плагин предоставляет панель для закладок.
		 %(cmd)s
вернула ненулевой статус выхода %(code)i Произошло %(n_error)i ошибок и %(n_warning)i предупреждений, смотрите протокол %A, %d %b %Y %i вложение %i вложения %i вложения %i _Обратная ссылка %i _Обратные ссылки %i _Обратных ссылок Произошло %i ошибок, смотрите протокол %i файлов будет удалено %i файл будет удалён %i файла будет удалено %i открытых задач %i открытая задача %i открытых задачи Произошло %i предупреждений, смотрите протокол <Верх> <Неизв.> Настольная wiki Файл с именем <b>«%s»</b> уже существует.
Вы можете указать другое имя или перезаписать существующий файл. Таблица должна иметь по крайней мере один столбец. Действие Добавить приложение Добавить закладку Добавить блокнот Добавить закладку/Показать настройки Добавить столбец Добавить новые закладки в начало панели Добавить строку Добавляет проверку правописания с помощью gtkspell.

Это базовый модуль, поставляющийся с Zim.
 Выравнивание Все файлы Все задачи Разрешить публичный доступ Всегда использовать последнюю позицию курсора при открытии страницы Во время генерации изображения возникла ошибка.
Всё равно сохранить исходный текст? Комментированный исходник страницы Приложения Арифметика ASCII-графика (Ditaa) Создать вложение Вставить изображение Менеджер вложений Вложения Вложения: Автор Авто
Перенос Авто отступ Автоматически сохраненная версия Автоматически выделять текущее слово при применении форматирования Автоматически делать ссылками слова, написанные в ПрыгающемРегистре Автоматически делать ссылками пути файлов Интервал автосохранения в минутах Регулярное автосохранение версий Автоматически сохранять версию при закрытии блокнота Вернуть оригинальное имя Обратные ссылки Панель обратных ссылок Система контроля версий Обратные ссылки: Bazaar Закладки Панель закладок Ширина границы Нижняя панель Обзор Маркированный список Настроить Не удалось изменить страницу: %s Отмена Захватить весь экран По центру Изменить столбцы Изменения Символов Символы, исключая пробелы Отметить '>' Установить переключатель в 'V' Установить переключатель в 'X' Проверять _орфографию Список переключателей Классический значок в области уведомлений
(не использовать новый стиль значков статуса в Ubuntu) Очистить Дублировать строку Блок кода Столбец 1 Команда Команда не изменяет данные Комментарий Общий включаемый "подвал" Общий включаемый заголовок Блокнот _целиком Настройка приложений Настроить модуль Настройка приложения для открывания ссылок "%s" Настройка приложения для открывания файлов
типа "%s" Считать задачами все строки с флажками Копировать Копировать адрес электронной почты Копировать шаблон _Копировать как... Копировать _ссылку Копировать _адрес Не удалось найти исполняемый файл "%s" Не удалось найти блокнот: %s Не удалось найти шаблон "%s" Не удалось найти файл или папку для этого блокнота Не удалось загрузить проверку орфографии Не удалось открыть: %s Не удалось разобрать выражение Не удалось прочитать: %s Не удалось сохранить страницу: %s Создавать новую страницу для каждой заметки Создать папку? Создан _Вырезать Внешние инструменты В_нешние инструменты Настроить... Дата День По умолчанию Применять форматирование по умолчанию для копируемого текста Блокнот по умолчанию Задержка Удаление страницы Удалить страницу «%s»? Удалить строку Понизить Зависимости Описание Подробности Диаграмма Сбросить заметку? Редактирование не отвлекаясь Вы хотите удалить все закладки? Вы хотите восстановить страницу: %(page)s
до сохраненной версии: %(version)s?

Все изменения с последней сохранённой версии будут утеряны! Корневая папка документа Завершить к _Экспорт… Редактировать %s Редактирование внешнего инструмента Редактировать изображение Редактировать ссылку Редактор таблицы Редактировать _исходный текст Редактирование Редактирование файла: %s Включить контроль версий? Включить модуль Включен Формула Ошибка в %(file)s в строке %(line)i около "%(snippet)s" Посчитать _формулу Развернуть _всё Экспорт Экспортировать все страницы в один общий файл Экспорт завершён Экспортировать каждую страницу в отдельный файл Экспорт записей блокнота Ошибка Ошибка запуска %s Ошибка запуска приложения: %s Файл существует Файл _Шаблоны... Файл %s на диске изменён Файл существует Файл %s защищён от записи Имя файла слишком длинное: %s Слишком длинное имя файла: %s Тип файла не поддерживается: %s Имя файла Фильтр Найти Найти _следующее Найти _предыдущее Найти и заменить Найти что Пометить задачи на понедельни или вторник перед выходными Папка Папка уже существует и содержит данные. Экспорт в неё может привести к перезаписи существующих файлов. Продолжить? Папка существует: %s Каталог с шаблонами для файлов-вложений Для расширенного поиска можно использовать операторы И, ИЛИ и НЕ.
Более подробная информация на странице помощи. _Формат Формат Окаменелость Графика GNU R Получить больше плагинов на сайте Получить больше шаблонов на сайте Git Gnuplot Линии сетки Заголовок _1 Заголовок _2 Заголовок _3 Заголовок _4 Заголовок _5 Высота Скрывать панель журнала если он пустой Скрыть меню в полноэкранном режиме Подсвечивать текущую строку Домашняя страница Горизонтальная _линия Значок Изображения Импорт страницы Включить вложенные страницы Содержание Главная страница Встроенный калькулятор Вставить блок кода Вставить дату и время Вставка диаграмм Вставка ditaa Вставка формул Вставка графика GNU R Вставка Gnuplot Вставить изображение Вставить ссылку Вставка нотного текста Вставка снимка экрана Вставить диаграмму последовательностей Вставка символа Вставить таблицу Вставить текст из файла Интерфейс Название для интервики Дневник Переход Перейти к странице Горячие клавиши Горячие клавиши Метки для задач Последнее изменение Оставить ссылку на новую страницу По левому краю Левая боковая панель Сортировщик строк Строк Карта ссылок Использовать полный путь для файлов Ссылка на Местоположение Вести журнал событий с помощью Zeitgeist Журнал Похоже, вы нашли ошибку Сделать приложением по умолчанию Управление столбцами таблицы Преобразовать путь к корневой папке документов в URL _Учитывать регистр Максимальное количество закладок Максимальная ширина страницы Меню Mercurial Изменён Месяц Переместить выделенный текст… Переместить текст на другую страницу Переместить столбец вперёд Переместить столбец назад Переместить текст в Имя Нужен выходной файл для экспорта в MHTML Нужна выходная папка для экспорта полной записной книжки Никогда не переносить строки Новый файл Создать новую страницу Новая страница в %s Создать _подстраницу… Новое _вложение След. Приложений не найдено Нет изменений с прошлой версии Нет зависимостей Для этого блокнота неопределена корневой каталог Отсутствует модуль для отображения объектов этого типа: %s Файл %s не найден Страница %s не найдена Неизвестная wiki-ссылка: %s Шаблоны не установлены Блокнот Блокноты OK Показывать только активные задачи Открыть папку с вложениями Открыть папку Открыть блокнот Открыть с помощью… Открыть корневую папку документов Открыть папку _блокнота Открыть _Страницу Открыть ссылку, содержащуюся в ячейке Открыть справку Открыть в новом окне Открыть в _новом окне Открыть новую страницу Открыть папку с модулями Открыть с помощью «%s» Дополнительно Параметры модуля «%s» Другой... Выходной файл Выходной файл существует, укажите "--overwrite", чтобы выполнить экспорт принудительно Выходная папка Выходная директория существует и не пуста, укажите "--overwrite", чтобы выполнить экспорт принудительно Выходное расположение, требующееся для экспорта Вывод должен заменить текущий выбор Перезаписать Панель _адреса Страница У страницы «%s» нет папки для вложений Содержание страницы Имя страницы Шаблон страницы Страница %s уже существует Страница %s не разрешена Раздел страницы Абзац Пароль Вставить Панель пути Добавьте комментарий для этой версии Имейте ввиду, что ссылка на несуществующую страницу
приведёт к автоматическому созданию этой страницы. Укажите имя и папку для блокнота. Пожалуйста, выберите строку перед нажатием на кнопку. Выберите пожалуйста более одной строчки текста Пожалуйста, укажите записную книжку Модуль Требуется модуль "%s" для отображения этого объекта Модули Порт Положение в окне _Параметры Параметры Пред. Печать в браузер Повысить _Свойства Свойства Передаёт события процессу zeitgeist. Быстрые заметки Быстрая заметка… Недавние изменения Недавние изменения... Недавно изменённые страницы Переформатировать wiki-разметку на лету Удалить Удалить все Удалить столбец Удалить строку Удаление ссылок Переименовать страницу «%s» Повторение нажатий вызывает цикличную смену статуса чек-бокса Заменить _всё Заменить на Требуется аутентификация Восстановить страницу до сохранённой версии? Ревизия По правому краю Правая боковая панель Позиция правой границы Строку вниз Строку вверх Сохранить _версию… Сохранить коп_ию… Сохранить копию страницы Сохранить версию Сохранить закладки Сохранённая версия Нотный текст Команда для скриншота Поиск Найти _ссылки... Искать в этом разделе Раздел Игнорировать разделы Искать в разделах Выбрать файл Выбрать папку Выбрать изображение Выберите версию, чтобы увидеть изменения между этой версией и
текущим состоянием. При выборе нескольких версий будут показаны различия между ними.
 Выберите формат для экспорта Выберите выходной файл или папку Выберите страницы для экспорта Выбрать окно или область экрана Выделение Диаграмма последовательностей Сервер не запущен Сервер запущен Сервер остановлен Установить новое имя Установить текстовой редактор по умолчанию Использовать текущую страницу Показывать номера строк Отображать задачи в виде простого списка Показать оглавление в плавающем виджете (а не в боковой панели) Показать _изменения Показывать отдельный значок для каждого блокнота Показать полное имя страницы Показать полное имя страницы Показать помощник панели инструментов Показывать на панели инструментов Показать правую границу Показывать список задач на боковой панели Показывать курсор на страницах, защищённых от редактирования Показать заголовок страницы как заголовок в оглавлении Одна _страница Размер Ключ Smart Home При выполнении «%s» возникла ошибка Сортировать по алфавиту Сортировать страницы по меткам Просмотр исходника Проверка орфографии Начинается Запустить _веб-сервер Си_мвол… Синтаксис Настройка по умолчанию (системная) Ширина табуляции Таблица Редактор таблиц Оглавление Метки Теги для невыполняемых задач Задача Список задач Задачи Шаблон Шаблоны Текст Текстовые файлы Текст из _файла… Цвет фона Цвет текста Каталог 
%s
не существует.
Хотите его создать? Каталог "%s" ещё не существует.
Хотите его создать? Следующие параметры будут заменяться
в команде, во время её исполнения:
<tt>
<b>%f</b> исходный код страницы как временный файл
<b>%d</b> прикреплённый к текущей странице каталог
<b>%s</b> настоящий исходный файл страницы (если определён)
<b>%p</b> имя страницы
<b>%n</b> местохахождение блокнота (файл или каталог)
<b>%D</b> корневой каталог документа (если определён)
<b>%t</b> выделенный текст или слово под курсором
<b>%T</b> выделенный текст включая wiki-разметку
</tt>
 Встроенный калькулятор не смог
рассчитать выражение под курсором. Таблица должна состоять по крайней мере из строки!
Никакого удаления не сделано. Тема Со времени сохранения последней версии изменений не было Это может означать, что соответствующий
словарь не установлен Этот файл уже существует.
Заменить? У этой страницы нет папки для вложений Данное имя страницы недопустимо из-за ограничений хранилища Этот модуль позволяет сделать снимок экрана и вставить его
в страницу Zim.

Это базовый модуль, поставляющийся с Zim.
 Плагин добавляет "панель пути" над окном текущей страницы.
Эта "панель пути" может показывать путь к текущей странице, список недавно посещённых страниц или список недавно редактированных страниц.
 Этот модуль добавляет диалог, показывающий список открытых задач в определенном блокноте.
Открытые задачи помечаются флажками или метками, например TODO или FIXME.

Это - базовый плагин, поставляется вместе с zim.
 Этот модуль добавляет диалог для быстрой вставки текста
или содержания буфера обмена на страницу в Zim.

Это базовый модуль, поставляющийся с Zim.
 Этот модуль добавляет значок Zim в область уведомлений.

Для работы этого модуля требуется версия Gtk+ не ниже 2.10.

Это базовый модуль, поставляющийся с Zim.
 Этот модуль добавляет дополнительный виджет, который показывает список страниц,
ссылающихся на текущую страницу.

Это -- базовый модуль; поставляется с zim.
 Этот модуль добавляет дополнительный виджет, показывающий
оглавление текущей страницы.

Это - базовый модуль; поставляется с zim.
 Этот модуль добавляет установки, которые позволяют 
редактировать в zim, не отвлекаясь.
 Этот модуль добавляет диалог «Вставка символа» и
позволяет автоформатирование типографских знаков.

Это базовый модуль, поставляющийся вместе с Zim.
 Этот плагин добавляет панель содержания страницы в основное окно
 Этот модуль обеспечивает контроль версий для блокнотов.

Этот модуль поддерживает следующие системы контроля версий: Bazaar, Git и Mercurial.

Это - базовый модуль; поставляется с zim.
 Этот плагин позволяет вставить "блок кода" в текущее место на странице.
При этом он предлагает выбрать язык программирования для выделения синтаксиса , а также нумерует линии текста в "блоке кода".
 Этот модуль позволяет вставлять арифметические вычисления в zim. 
Он основан на арифметическом модуле 
http://pp.com.mx/python/arithmetic.
 Этот модуль позволяет быстро рассчитать
простые математические выражения в Zim.

Это базовый модуль, поставляющийся вместе с Zim.
 Этот плагин имеет дополнительные свойства,
смотри диалог свойств блокнота Этот плагин добавляет редактор диаграмм, основанный на Ditaa. 

Это - базовый плагин, поставляется вместе с zim.
 Этот модуль добавляет редактор диаграмм для Zim на основе GraphViz.

Это базовый модуль, поставляющийся с Zim.
 Этот модуль отображает диалоговое окно с графическим
представлением структуры ссылок блокнота. Он может
быть использован в качестве «карты», показывающей,
как связаны страницы.

Это базовый модуль, поставляющийся с Zim.
 Этот плагин предоставляет панель меню macOS для zim. Этот модуль позволяет фильтровать содержание по выбранным меткам в облаке.
 Этот модуль, основанный на GNU R, служит для редактирования графиков.
 Этот модуль предоставляет редактор графиков для Zim, основанный на Gnuplot.
 Этот плагин предоставляет редактор диаграммы последовательностей для zim на основе seqdiag.
Это позволяет легко редактировать диаграммы последовательностей.
 Этот модуль предоставляет отсутствующую в Zim
поддержку печати. Он экспортирует текущую страницу
в HTML и открывает ее в браузере. Если браузер имеет 
поддержку печати, вы сможете произвести печать оттуда.

Это базовый модуль, поставляющийся с Zim.
 Этот плагин добавляет редактор формул для Zim на основе LaTeX.

Это - базовый плагин, поставляется вместе с zim.
 Этот модуль добавляет нотный редактор, основанный на GNU Lilypond. 
Это - базовый модуль; поставляется с zim.
 Этот плагин показывает папку вложений текущей страницы
значком на нижней панели.
 Этот модуль сортирует выделенные строки в алфавитном
порядке. Если строки уже отсортированы, порядок
сортировки будет изменен на обратный (А-Я на Я-А).
 Этот плагин перенаправляет один раздел записной книжки в журнал
со страницей в день, неделю или месяц.
Также добавляет виджет календаря для доступа к этим страницам.
 Это обычно означает, что файл содержит некорректные символы Заголовок Для продолжения вы можете сохранить копию этой страницы 
или отклонить все изменения. Если вы сохраните копию, изменения
также не будут внесены, но они могут быть восстановлены позже. Для создания нового блокнота Вам необходимо выбрать пустой каталог.
Конечно, Вы можете также выбрать существующий каталог блокнота zim.
 Оглавление _Сегодня Сегодня Пометить как >-флажок Пометить как V-флажок Пометить как X-флажок Верхняя панель Значок в области уведомлений Преобразовывать имя страницы в метки для задач Тип Сбросить переключатель Удаление отступов по <BackSpace>
(если отключено, можно использовать <Shift><Tab>) нет данных Неизвестный тип изображения Неизвестный объект Неопределённый Непомеченные Обновить %i страницу, ссылающуюся на эту страницу Обновить %i страницы, ссылающихся на эту Обновить %i страниц, ссылающихся на эту страницу Обновить заголовок страницы Обновление ссылок Обновление содержания Используйте %s для переключения на боковую панель Использовать свой шрифт Странице календаря соответствует: Использовать дату из страниц журнала Использовать <Enter> для перехода по ссылкам
(если отключено, можно использовать <Alt><Enter>) Имя пользователя Контроль версий Контроль версий для этого блокнота отключен.
Включить? Версии Просмотреть _аннотации Просмотреть _журнал Веб-сервер Неделя При отправке отчета об ошибке, пожалуйста,
включите информацию из текстового поля ниже Только слово целиком Ширина Вики-страница: %s С помощью этого плагина вы можете вставлять таблицу в вики-страницу.
Таблицы будут показаны как виджеты GTK TreeView.
Экспорт их в различные форматы (т. е. HTML/LaTeX) дополняет набор функций.
 Статистика Статистика… Слов Год Вчера Вы редактируете файл во внешнем приложении. Закройте этот диалог по завершении редактирования Вы можете настроить внешние инструменты, которые затем 
будут отображаться на панели инструментов или в контекстном меню. Zim Desktop Wiki У_меньшить _О программе _Добавить _Все панели _Арифметика Вложение… На_зад _Обзор О_шибки _Отмена Переключатель Уровнем _ниже _Очистить форматирование _Закрыть _Свернуть всё Со_держание _Копировать _Копировать сюда _Удалить _Удалить страницу От_клонить изменения _Дублировать строчку _Правка _Редактировать ссылку _Редактировать ссылку или объект… _Редактировать свойства _Редактировать... _Курсив _Часто задаваемые вопросы _Файл _Найти _Найти… В_перед Полноэкранный _режим Пере_ход _Справка П_одсвечивать Посещенные страницы _Домашняя страница _Изображение... _Импорт страницы… _Вставка _Переход Пере_йти к… _Горячие клавиши _Связать _Ссылка на дату _Ссылка… _Подчеркнутый _Ещё _Переместить _Переместить сюда П_ередвинуть строчку вниз _Передвинуть строчку вверх Создать страницу… _Создать страницу… _Следующее _Нет Нормальный размер _Нумерованный список _OK _Открыть _Открыть другой блокнот… _Другой… _Страница _Иерархия страниц Уровнем _выше Вст_авить _Предварительный просмотр _Предыдущее _Печать _Печать в браузер _Быстрая заметка… В_ыход _Недавние страницы Ве_рнуть _Регулярное выражение _Обновить _Убрать _Удалить строчку Убрать _ссылку _Заменить _Заменить… _Сбросить размер _Восстановить версию Со_хранить _Сохранить копию С_нимок экрана… _Поиск _Найти... _Отправить… _Боковые панели _Рядом Сортировать строки Зач_еркнутый _Жирный Нижний индекс Верхний индекс _Шаблоны _Инструменты _Отменить Стенограмма _Версии… _Вид _Увеличить как требуемая дата для задач как начальная дата для задач calendar:week_start:1 не использовать горизонтальные линии без линий сетки только чтение сек. Launchpad Contributions:
  Andrew Kuzminov https://launchpad.net/~ilobster
  Antonio https://launchpad.net/~ghoniq
  Arsa Chernikov https://launchpad.net/~arsa-chernikov
  Artem Anufrij https://launchpad.net/~artem-anufrij
  DIG https://launchpad.net/~dig
  Dmitry Ostasevich https://launchpad.net/~ostasevich
  Dominus Alexander Z. https://launchpad.net/~dominusalex
  Eugene Krivobokov https://launchpad.net/~eugene-krivobokov
  Eugene Mikhantiev https://launchpad.net/~mehanik
  Eugene Morozov https://launchpad.net/~cactus-mouse
  Eugene Roskin https://launchpad.net/~lowrider
  Eugene Schava https://launchpad.net/~eschava
  Jaap Karssenberg https://launchpad.net/~jaap.karssenberg
  ManDrive https://launchpad.net/~roman-romul
  Nick https://launchpad.net/~nick222-yandex
  Nikolay A. Fetisov https://launchpad.net/~naf-altlinux
  Oleg https://launchpad.net/~oleg-devyatilov
  Sergey Shlyapugin https://launchpad.net/~inbalboa
  Sergey Vlasov https://launchpad.net/~sigprof
  Vitaliy Starostin https://launchpad.net/~vvs
  Vitaly https://launchpad.net/~jauthu
  Vladimir Sharshov https://launchpad.net/~vsharshov
  aks-id https://launchpad.net/~aks-id
  anton https://launchpad.net/~faq-ru
  gest https://launchpad.net/~drug-detstva
  vantu5z https://launchpad.net/~vantu5z вертикальные линии с линиями {count} из {total} 