��    r     �              <'  ,   ='  .   j'  ?   �'     �'     �'     (  0   (     O(     j(     �(  	   �(     �(  i   �(  *   )     <)     L)     Y)     f)  
   �)  -   �)     �)  V   �)     *  	   *  	   )*     3*  3   G*  Y   {*     �*     �*  
   �*     +     +     #+     6+     I+     U+     b+  	   i+     s+  $   �+  ?   �+  /   �+  (   ,     @,  %   ],  ,   �,     �,  	   �,     �,     �,  
   �,     �,  	   �,     -     -     -     )-     0-  
   =-     H-     `-     g-     |-     �-     �-  
   �-     �-     �-     �-     �-     �-     
.  <   .     V.  	   \.  
   f.     q.     z.     �.     �.     �.     �.     �.     �.     �.  +   /  3   :/      n/     �/     �/     �/     �/  
   �/     �/     �/     �/     0  3   30     g0     �0     �0     �0     �0     �0     �0     1     1     1     '1     51     B1     G1     K1  0   S1     �1     �1     �1     �1  
   �1     �1     �1     �1     �1     �1     �1     2  $   2  ~   @2     �2     �2  
   �2     �2     �2  
   �2  	    3  
   
3     3     "3     *3     ;3     S3     a3     i3  5   r3     �3     �3     �3  !   �3     �3  #   �3     !4     44     ;4     N4     l4     x4     �4     �4     �4     �4     �4     �4     �4  
   �4     5     5  	   %5  6   /5     f5  v   m5     �5  *   �5  c   !6     �6     �6     �6  
   �6     �6     �6     �6     �6  
   �6  
   �6  
   �6  
   7  
   7  
   7     &7     -7     H7     h7  	   7     �7     �7     �7     �7     �7     �7  
   �7     �7     �7     �7     8     8     )8     98     K8     Z8     g8     s8     �8     �8     �8     �8     �8  	   �8     �8     �8     �8     9     9     )9     79     N9     S9     b9     n9     t9  2   }9     �9     �9     �9     �9     �9     �9     :     /:     H:     T:     p:     �:  	   �:     �:     �:     �:     �:     �:     �:     �:     ;      ;  *   ,;     W;     `;     i;     x;     �;     �;     �;     �;     �;  *   �;  2   <     @<     Q<     b<     {<     �<  	   �<     �<     �<     �<     �<     �<     �<     �<     =  
   )=     4=  	   K=     U=     h=     |=     �=     �=     �=     �=     �=     �=  9   �=     >  I   )>  !   s>  '   �>  	   �>     �>     �>  0   �>  
   ?  	   ?     ?     )?     A?     V?  	   c?     m?     s?  '   |?  V   �?  3   �?  0   /@  (   `@     �@     �@  .   �@     �@     �@     �@     �@     
A     A     A     ,A     4A  
   @A  &   KA  
   rA     }A     �A     �A     �A     �A     �A  
   �A     �A  
   B     B     B  ?   /B     oB     |B     �B     �B     �B     �B     �B     �B     �B     �B     �B  	   	C     C      C     /C     FC     LC     _C     fC     {C     �C     �C     �C     �C     �C     �C  �   �C     wD      �D     �D     �D  	   �D     �D     �D     E     !E     0E     =E     UE     iE     {E  2   �E     �E  &   �E     �E     F     #F     7F     KF     ]F  5   wF  &   �F     �F     �F     �F  &   �F     G     0G     CG     OG     ]G     cG  
   uG     �G     �G  	   �G     �G     �G     �G     �G     �G     �G  	   �G     �G     �G  	   H     H  
   H      H     3H     IH  ?   _H  A   �H  �  �H  S   �J  =   �J  K   7K  @   �K  6   �K  -   �K  I   )L  x   sL  �   �L  �   �M  �   \N  �   �N  �   oO  }   �O  L   nP  �   �P  9   EQ  �   Q  �   'R  �   �R  }   CS  C   �S  h   T  k   nT  �   �T  R   �U  ;   V  =   =V  v   {V    �V  j   X  n   qX  ]   �X     >Y  �   �Y  7   SZ     �Z  �   �Z  |   -[     �[     �[     �[     �[     �[     �[     �[  	    \  '   
\     2\     7\  D   I\     �\     �\     �\     �\     �\  H   �\     ]     4]     C]  !   R]     t]     �]     �]  P   �]     ^  U   ^     m^     v^  	   �^  
   �^     �^  N   �^     �^     �^     _  �   _  
   �_     �_     �_     �_  	   �_  ^   �_  f   W`     �`  	   �`     �`     �`  
   �`     �`     �`     a     
a     a  	   a     "a     )a     ;a     Ba  	   Pa     Za  
   `a     ka     sa     �a     �a     �a  
   �a     �a     �a     �a  	   �a     �a     �a     �a     �a     b     b     b     !b  
   'b     2b     ;b  	   Ab     Kb     [b     cb     ib     ub     �b     �b     �b     �b     �b     �b  
   �b     �b     �b     �b     �b     �b     �b     c     c     !c     %c     +c  	   Ec     Oc     Uc     ec     mc     tc  	   }c     �c     �c     �c     �c     �c     �c     �c     �c     �c     �c     �c     d     d     d     (d     9d  
   ?d     Jd     Yd  
   ad     ld     xd     �d     �d     �d     �d  
   �d     �d  
   �d     �d     �d  	   �d     �d     �d     �d     e     e     2e  
   He     Se     de     re     {e     �e     �e  
   �e     �e  �  �e  9   �g  B   �g  M   h     ah     mh  &   �h  4   �h  %   �h  ,   i     4i     9i     Gi  u   ^i  *   �i     �i     j     )j  )   >j     hj  2   yj     �j  �   �j     Hk     Pk     ]k     pk  H   �k  b   �k     6l     Ol  
   \l     gl     }l     �l     �l     �l  	   �l     �l     �l     �l  %   �l  O   m  f   hm  <   �m  #   n  ,   0n  ?   ]n     �n     �n     �n     �n     �n     �n  
   o     o     o     ,o     ?o     Go  
   Wo  $   bo     �o     �o     �o     �o  	   �o  	   �o     �o     �o     �o     p     *p     Ap  q   ap     �p  
   �p     �p  	   �p     q     
q     )q     2q     Iq     ^q     tq     �q  5   �q  5   �q  6   r     Cr     Ir     `r     nr     }r     �r  %   �r  &   �r  #   �r  @   s  +   Ts     �s  $   �s     �s  !   �s  *   �s     !t     5t     <t     Dt     ]t     wt     �t     �t     �t  7   �t     �t     �t     �t     u      u     -u  
   Cu     Nu     Zu  	   cu     mu     }u     �u  �   �u  !   Iv     kv     tv     �v  %   �v     �v     �v     �v     �v     �v     w      "w     Cw     Qw  	   Xw  8   bw     �w     �w     �w  '   �w     �w  '   x     9x     Rx     _x  '   vx     �x     �x  (   �x     �x     y     !y  	   Ay     Ky     Ry     Xy     jy     |y     �y  J   �y     �y  �   �y     vz  *   �z  �   �z     ?{     H{     P{  
   W{     b{     �{     �{     �{     �{  	   �{  	   �{  	   �{  	   �{  	   �{     �{  $   �{  6   |     O|     i|     y|     �|     �|     �|     �|     �|     �|     �|     �|     �|     }     &}     6}     J}     b}     t}     �}     �}     �}     �}     �}     �}     ~     #~     /~     G~     N~     T~  &   d~     �~  (   �~  
   �~     �~     �~     �~     �~  T     	   i     s     |     �  (   �      �     �  >   �  #   M�     q�     ��     ��  	   ��  
   ǀ     Ҁ     ׀  !   ��     �     2�     J�     ]�  5   b�  E   ��  
   ށ     �     ��     	�     �     ,�     2�  %   O�     u�  R   ��  E   ۂ     !�     6�     S�     l�     ��     ��     ��     ��          ڃ     �     ��  *   �  !   0�     R�     _�  
   y�     ��     ��     ��     ̈́     �  	   ��     �     �      �  ]   5�     ��  p   ��  G   �  -   e�     ��     ��     ��  3   ��     ��     ��     �     "�     <�     V�  	   e�     o�     w�  (   ��  b   ��  :   �  0   Q�  #   ��     ��  
   ��  <   ̈  
   	�     �     �     3�  
   ?�     J�     O�     a�     t�  
   ��  #   ��     ��          ؉     �     ��  %   �     A�     I�     W�     g�  #   t�     ��  i   ��     �     -�  -   =�     k�     o�     x�     ��  
   ��  
   ��     ��     Ӌ     �     �     �     �  
   +�     6�     M�     S�     k�     ��     ��     ��     ��     Ɍ     ܌  �   �  $   ��  /   č     �     �  	   /�     9�     O�     b�     q�     ��  #   ��     ��     ֎  &   �  D   �     X�  0   j�  $   ��  !   ��     �  "   ��     "�  5   8�  6   n�  5   ��     ې  
   �     ��  6   �     H�     _�     {�     ��     ��     ��     Ñ     ϑ     ؑ     �     �     �     "�  	   +�  &   5�  	   \�     f�  	   w�     ��     ��     ��     ��     ��     ��     Ғ  *   �  5   �  �  S�  n   C�  E   ��  ]   ��  <   V�  2   ��  2   Ɩ  c   ��  �   ]�  �   �  :  �  �   �  �   ۚ  �   ��  �   E�  _   Ҝ  �   2�  f   �  �   V�  �   �  �   џ  �   Y�  V   ߠ  �   6�  �   ¡  (  Q�  �   z�  Y   �  [   ]�  �   ��  G  H�  �   ��  �   �  y   ��  �   *�  �   ��  B   f�     ��  �   ��  �   ��     -�     6�     <�  +   A�     m�     ��     ��     ��  ;   ȫ     �     	�  x   �     ��     ��     ��     Ҭ     �  q   ��  #   f�  '   ��  "   ��  '   խ     ��     �  #   2�  ~   V�     ծ  U   �     >�     G�     _�  
   t�  	   �  \   ��     �  	   ��     ��  �   �     �     �     �     �     �  o   �  �   ��     �     $�     :�  	   H�     R�     c�  	   o�     y�     ��     ��     ��     ��     ��     в     ز  
   �     �  
   ��     �     �     �     0�  	   >�     H�  #   _�     ��     ��     ��     ��     ��     ǳ  	   γ     س     �     �     ��  
   ��     �     �     $�     1�  
   D�     O�  	   T�     ^�     w�     ��     ��     ��     ��     ��     Ŵ     Ѵ     �     ��     �     "�     .�     7�     K�     \�     `�     f�  	   ��     ��     ��     ��     ��  
   ʵ     յ     �     �     ��     �     �     )�     1�  	   G�     Q�     Z�     h�     ~�     ��     ��     ��     Ƕ     ζ     ۶     �  	   �     ��     �     �     &�     4�  
   =�     H�     P�     W�  
   `�     k�  	   t�     ~�     ��     ��  %   ��  "   Է     ��  	   �     �     )�     <�     I�  /  Q�     ��     ��     ��   		This plugin provides bar for bookmarks.
		 %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i Attachment %i Attachments %i errors occurred, see log %i file will be deleted %i files will be deleted %i open item %i open items %i warnings occurred, see log <Top> <Unknown> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Add Application Add Bookmark Add Notebook Add bookmark/Show settings Add column Add new bookmarks to the beginning of the bar Add row Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files All Tasks Allow public access Always use last cursor position when opening a page An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Arithmetic Ascii graph (Ditaa) Attach File Attach image first Attachment Browser Attachments Attachments: Author Auto
Wrap Auto indenting Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave interval in minutes Autosave version on regular intervals Autosave version when the notebook is closed Back to Original Name BackLinks BackLinks Pane Backend Backlinks: Bazaar Bookmarks BookmarksBar Border width Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Capture whole screen Center Change columns Changes Characters Characters excluding spaces Check Checkbox '>' Check Checkbox 'V' Check Checkbox 'X' Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Clone row Code Block Column 1 Command Command does not modify data Comment Common include footer Common include header Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Created Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Demote Dependencies Description Details Diagram Discard note? Distraction Free Editing Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root Due E_xport... Edit %s Edit Custom Tool Edit Image Edit Link Edit Table Edit _Source Editing Editing file: %s Enable Version Control? Enable plugin Enabled Equation Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Fossil GNU R Plot Get more plugins online Get more templates online Git Gnuplot Grid lines Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide Journal pane if empty Hide menubar in fullscreen mode Highlight current line Home Page Horizontal _Line Icon Images Import Page Include subpages Index Index page Inline Calculator Insert Code Block Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Sequence Diagram Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Labels marking tasks Last Modified Leave link to new page Left Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Managing table columns Map document root to URL Match _case Maximum number of bookmarks Maximum page width Menubar Mercurial Modified Month Move Selected Text... Move Text to Other Page Move column ahead Move column backward Move text to Name Need output file to export MHTML Need output folder to export full notebook New File New Page New Page in %s New S_ub Page... New _Attachment Next No Applications Found No changes since last version No dependencies No document root defined for this notebook No plugin available to display objects of type: %s No such file: %s No such page: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Only Show Active Tasks Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open cell content link Open help Open in New Window Open in New _Window Open new page Open plugins folder Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Index Page Name Page Template Page already exists: %s Page not allowed: %s Page section Paragraph Paste Path Bar Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please select a row, before you push the button. Please select more than one line of text Please specify a notebook Plugin Plugin "%s" is required to display this object Plugins Port Position in the window Pr_eferences Preferences Prev Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Remove column Remove row Removing Links Rename page "%s" Repeated clicking a checkbox cycles through the checkbox states Replace _All Replace with Restore page to saved version? Rev Right Right Side Pane Right margin position Row down Row up S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Screenshot Command Search Search _Backlinks... Search this section Section Section(s) to ignore Section(s) to index Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Sequence Diagram Server not started Server started Server stopped Set New Name Set default text editor Set to Current Page Show Line Numbers Show Tasks as Flat List Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show full Page Name Show full page name Show helper toolbar Show in the toolbar Show right margin Show tasklist in sidepane Show the cursor also for pages that can not be edited Show the page title heading in the ToC Single _page Size Smart Home key Some error occurred while running "%s" Sort alphabetically Sort pages by tags Source View Spell Checker Start Start _Web Server Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Tags for non-actionable tasks Task Task List Tasks Template Templates Text Text Files Text From _File... Text background color Text foreground color The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The following parameters will be substituted
in the command when it is executed:
<tt>
<b>%f</b> the page source as a temporary file
<b>%d</b> the attachment directory of the current page
<b>%s</b> the real page source file (if any)
<b>%p</b> the page name
<b>%n</b> the notebook location (file or folder)
<b>%D</b> the document root (if any)
<b>%t</b> the selected text or word under cursor
<b>%T</b> the selected text including wiki formatting
</tt>
 The inline calculator plugin was not able
to evaluate the expression at the cursor. The table must consist of at least on row!
 No deletion done. There are no changes in this notebook since the last version that was saved This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This page name cannot be used due to technical limitations of the storage This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a "path bar" to the top of the window.
This "path bar" can show the notebook path for the current page,
recent visited pages or recent edited pages.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds the page index pane to the main window.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows inserting 'Code Blocks' in the page. These will be
shown as embedded widgets with syntax highlighting, line numbers etc.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin also has properties,
see the notebook properties dialog This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a sequence diagram editor for zim based on seqdiag.
It allows easy editing of sequence diagrams.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This plugin turns one section of the notebook into a journal
with a page per day, week or month.
Also adds a calendar widget to access these pages.
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox '>' Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Tray Icon Turn page name into tags for task items Type Un-check Checkbox Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unkown Image type Unkown Object Unspecified Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use a page for each Use date from journal pages Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s With this plugin you can embed a 'Table' into the wiki page. Tables will be shown as GTK TreeView widgets.
Exporting them to various formats (i.e. HTML/LaTeX) completes the feature set.
 Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Zim Desktop Wiki Zoom _Out _About _Add _All Panes _Arithmetic _Back _Browse _Bugs _Cancel _Checkbox _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Duplicate Line _Edit _Edit Link _Edit Link or Object... _Edit Properties _Edit... _Emphasis _FAQ _File _Find _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _Move Here _Move Line Down _Move Line Up _New Page Here... _New Page... _Next _None _Normal Size _Numbered List _OK _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Print _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove _Remove Line _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In as due date for tasks as start date for tasks calendar:week_start:0 do not use horizontal lines no grid lines readonly seconds translator-credits vertical lines with lines {count} of {total} Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2019-05-17 16:59+0000
Last-Translator: Marco Cevoli <Unknown>
Language-Team: Italian <it@li.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
X-Generator: Launchpad (build bceb5ef013b87ef7aafe0755545ceb689ca7ac60)
 		Questa estensione aggiunge una barra dei segnalibri.
		 %(cmd)s
ha restituito uno stato di uscita diverso da zero %(code)i Si sono verificati %(n_error)i errori and %(n_warning)i avvisi; vedi registro %A %d %B %Y %i allegato %i allegati Si sono verificati %i errori; vedi log Verrà eliminato %i file Verranno eliminati %i  file %i elemento aperto %i elementi aperti Si sono verificati %i avvertimenti; vedi log <Su> <Sconosciuto> Un wiki per il desktop Esiste già un file con il nome <b>"%s"</b>.
È possibile utilizzare un altro nome o sovrascrivere il file esistente. Una tabella deve avere almeno una colonna. Aggiungi applicazione Aggiungi segnalibro Aggiungi blocco note Aggiungi segnalibro / Mostra impostazioni Aggiungi colonna Aggiungi i nuovi segnalibri all'inizio della barra Aggiungi riga Questa estensione aggiunge la funzionalità di controllo ortografico mediante gtkspell.

È un'estensione di base distribuita insieme a Zim.
 Allinea Tutti i file Tutte le attività Permetti accesso pubblico Utilizza sempre l'ultima posizione del cursore quando si apre una pagina Si è verificato un errore durante la creazione dell'immagine.
Salvare comunque il testo sorgente? Sorgente pagina annotato Applicazioni Aritmetica Grafico Ascii (Ditaa) Allega file Prima allega un'immagine Sfoglia allegati Allegati Allegati: Autore A capo
automatico Rientri automatici Versione salvata in automatico da Zim Seleziona automaticamente la parola corrente quando si applica la formattazione Converti automaticamente le parole composte con le iniziali maiuscole ("CamelCase") in un collegamento Converti automaticamente i percorsi dei file in collegamenti Intervallo di salvataggio in minuti Salva automaticamente ad intervalli regolari Salva automaticamente la versione alla chiusura del blocco note Ripristina nome originale Link entranti Pannello link entranti Backend Collegamenti inversi: Bazaar Segnalibri BookmarksBar Spessore del bordo Pannello inferiore Esplora Elenco pun_tato C_onfigura Impossibile modificare la pagina: %s Annulla Cattura l'intero schermo Centro Cambia colonne Modifiche Caratteri Caratteri senza spazi Seleziona casella '>' Seleziona casella 'V' Seleziona casella 'X' Controllo _ortografico Elenco con casella di controllo Stile classico per l'icona nell'area di notifica;
non utilizzare il nuovo stile per lo stato dell'icona in Ubuntu Cancella Clona riga Blocco di codice Colonna 1 Comando Il comando non modifica i dati Commento Includi piè di pagina Includi intestazione Blocco _note completo Configura applicazioni Configura estensione Scegli un'applicazione per aprire i collegamenti "%s" Scegli un'applicazione per aprire i file
di tipo "%s" Considera tutte le caselle di controllo come attività Copia Copia indirizzo e-mail Copia Modello Copi_a come... Copia _collegamento Copia _posizione Impossibile trovare l'eseguibile "%s" Impossibile trovare il blocco note: %s Impossibile trovare il modello "%s" Impossibile trovare il file o la cartella di questo blocco note. Impossibile caricare correttore ortografico Impossibile aprire: %s Impossibile analizzare l'espressione Impossibile leggere: %s Impossibile salvare la pagina: %s Crea una nuova pagina per ogni annotazione Creare la cartella? Creato _Taglia Strumenti personalizzati Strumen_ti personalizzati Personalizza... Data Giorno Predefinito Formato predefinito per la copia di testo negli appunti Blocco note predefinito Ritardo Elimina pagina Eliminare la pagina "%s"? Elimina riga Diminuisci di livello Dipendenze Descrizione Dettagli Diagramma Elimina la nota Redazione senza distrazioni Eliminare tutti i segnalibri? Ripristinare la pagina: %(page)s
alla versione salvata: %(version)s ?

Verranno perse tutte le modifiche effettuate dall'ultima versione salvata. Cartella principale dei documenti Scadenza _Esporta... Modifica %s Modifica gli strumenti personalizzati Modifica immagine Modifica collegamento Modifica tabella Modifica _sorgente Formattazione Modifica del file: %s Abilitare il controllo versione? Attiva plugin Attivo Equazione Errore in %(file)s, riga %(line)i vicino a "%(snippet)s" Valuta espressione _matematica Espandi _tutto Esporta Esporta tutte le pagine in un solo file Esportazione completata Esporta ogni pagina in un file separato Esportazione blocco note Non riuscito Impossibile avviare %s Impossibile eseguire l'applicazione: %s File già esistente _Modelli di file... Il file %s è stato modificato sul disco Il file esiste già il file %s non è scrivibile Tipo di file non supportato: %s Nome file Filtro Trova Trova s_uccessivo Trova _precedente Cerca e sostituisci Cerca qualcosa Segnala attività in scadenza lunedì o martedì prima del fine settimana. Cartella La cartella esiste già e non è vuota; esportando in questa cartella i file contenuti potrebbero essere sovrascritti. Continuare? Cartella già esistente: %s Cartella con i modelli per i file allegati Si possono eseguire ricerche avanzate con gli operatori
AND, OR e NOT. Consulta la guida in linea (Aiuto) per ulteriori dettagli. For_mato Formato Fossil Plot GNU R Ottieni altre estensioni online Ottieni altri modelli online Git Gnuplot Bordi griglia Titolo _1 Titolo _2 Titolo _3 Titolo _4 Titolo _5 Altezza Nascondi il riquadro Diario se vuoto Nascondi la barra dei menu in modalità schermo intero Evidenzia la riga attuale Pagina iniziale _Linea orizzontale Icona Immagini Importa pagina Includi sottopagine Indice Indice Calcolatrice in linea Inserisci blocco di codice Inserisci data e ora Inserisci diagramma Inserisci Ditaa Inserisci equazione Inserisci grafico GNU R Inserisci Gnuplot Inserisci immagine Inserisci collegamento Inserisci Partitura Inserisci screenshot Inserisci diagramma di sequenza Inserisci simbolo Inserisci tabella Inserisci testo da file Interfaccia Parola chiave interwiki Diario Vai a Vai alla pagina Le etichette identificano le attività Ultima Modifica Lascia il collegamento alla nuova pagina A sinistra Pannello laterale sinistro Ordina linee Righe Mappa dei collegamenti Collega i file alla cartella principale dei documenti indicando il percorso completo Collega a Percorso Registra eventi con Zeitgeist File di registro Apparentemente è stato rilevato un bug. Rendi l'applicazione predefinita Gestione colonne tabella Fai corrispondere la cartella principale dei documenti all'URL Corrispondenza _maiuscole/minuscole Numero massimo di segnalibri Larghezza massima della pagina Barra dei menù Mercurial Modificato Mese Sposta il testo selezionato... Sposta il testo nell'altra pagina Sposta colonna a sinistra Sposta colonna a destra Sposta il testo in Nome È necessario un file di output per esportare l'MHTML È necessario un file di output per esportare il blocco note completo Nuovo file Nuova pagina Nuova pagina in %s Nuova s_ottopagina Nuovo _Allegato Pross Nessuna applicazione trovata Nessuna modifica dall'ultima versione Nessuna dipendenza Non è stata definita la cartella principale per i documenti di questo blocco note Non sono disponibili plugin per visualizzare gli oggetti del tipo: %s File non trovato: %s Questa pagina non esiste: %s Nessun wiki definito: %s Nessun modello installato Blocco note Blocchi note OK Mostra solo le attività attive Apri cartella _allegati Apri cartella Apri blocco note Apri con... Apri la cartella principale dei _documenti Apri la cartella del _blocco note Apri _pagina Apri link contenuto cella Apri aiuto Apri in una nuova finestra Apri in una nuova _finestra Apri nuova pagina Apri la cartella dei plugin Apri con "%s" Opzionale Opzioni estensione %s Altro... File di destinazione Il file di output esiste già; specificare "--overwrite" per eseguire comunque l'esportazione Cartella di destinazione La cartella di output esiste già e non è vuota; specificare "--overwrite" per eseguire comunque l'esportazione È necessario indicare l'ubicazione dell'output prima dell'esportazione L'output deve sostituire la selezione attuale Sovrascrivi B_arra di navigazione Pagina La pagina "%s" non ha una cartella per gli allegati Indice pagina Nome della pagina Modello di pagina La pagina esiste già: %s Pagina non consentita: %s Sezione pagina Paragrafo Incolla Barra del percorso Inserire un commento per questa versione Verrà creata una nuova pagina, perché si sta inserendo
un collegamento a una pagina inesistente. Selezionare un nome e una cartella per questo blocco note. Scegliere una riga prima di premere il pulsante. Seleziona più di una riga di testo Specificare un blocco note Estensione Per visualizzare questo oggetto è necessario il plugin "%s" Estensioni Porta Posizione nella finestra Pr_eferenze Preferenze Prec Stampa su browser Aumenta di livello Proprie_tà Proprietà Invia eventi al demone di Zeitgeist Annotazione veloce Annotazione veloce... Modifiche recenti Modifiche Recenti Pagine modificate recentemente Riformatta al volo il markup del wiki Rimuovi Rimuovi tutto Elimina colonna Elimina riga Rimozione dei collegamenti in corso Rinomina la pagina "%s" Cliccando ripetutamente su una casella di controllo si passano in sequenza gli stati della casella stessa Sostituisci _tutto Sostituisci con Ripristinare la pagina alla versione salvata? Rev A destra Pannello laterale destro Posizione margine destro Riga sotto Riga sopra S_alva versione... Salva una _copia... Salva copia Salva versione Salva segnalibri Versione salvata da Zim Occorrenze Comando per screenshot Cerca Cerca _link entranti... Cerca in questa sezione Sezione Sezione/i da ignorare Sezione/i da indicizzare Seleziona file Seleziona cartella Seleziona immagine Seleziona una versione per visualizzare i cambiamenti tra la versione scelta e quella
corrente. Oppure seleziona versioni multiple per visualizzare i cambiamenti tra di esse.
 Seleziona il formato di esportazione Seleziona il file o la cartella di destinazione Seleziona pagina da esportare Seleziona finestra o regione Selezione Diagramma di sequenza Server non avviato Server avviato Server arrestato Imposta nuovo nome Imposta editor di testo predefinito Imposta alla pagina corrente Mostra numeri di riga Mostra le attività come elenco piatto Mostra il sommario come widget mobile anziché nel pannello laterale Mostra _modifiche Mostra un'icona separata per ciascun blocco note Mostra il nome completo della pagina Mostra nome completo della pagina Mostra barra strumenti aiuto Mostra nella barra degli strumenti Mostra margine destro Mostra l'elenco delle attività nel pannello laterale Mostra il cursore anche per le pagine non modificabili Mostra l'intestazione del titolo della pagina nel ToC _Pagina singola Dimensioni Pulsante home intelligente Si è verificato un errore mentre veniva eseguito "%s" Ordina alfabeticamente Ordina pagine per etichette Visualizzazione sorgente Controllo ortografico Inizio Avvia server _Web Si_mbolo... Sintassi Predefinito di sistema Larghezza della tabulazione Tabella Editor tabelle Sommario Etichette Etichetta per attività non eseguibili Attività Elenco attività Attività Modello Modelli Testo File di testo Testo da _file... Colore di sfondo del testo Colore di primo piano del testo La cartella
%s
non esiste.
Crearla adesso? La cartella "%s" non esiste ancora.
Vuoi crearla ora? I seguenti parametri saranno sostituiti
all'esecuzione del comando:
<tt>
<b>%f</b> la pagina sorgente come file temporaneo
<b>%d</b> la cartella degli allegati della pagina corrente
<b>%s</b> l'eventuale file sorgente della pagina reale
<b>%p</b> il nome della pagina
<b>%n</b> l'ubicazione del blocco note (file o cartella)
<b>%D</b> l'eventuale radice del documento
<b>%t</b> il testo selezionato o la parola sotto il cursore
<b>%T</b> il testo selezionato inclusa la formattazione wiki
</tt>
 L'estensione Calcolatrice in linea non è stata in grado
di valutare l'espressione alla posizione del cursore. La tabella deve avere almeno una riga.
 Non è stato eliminato nulla. Non sono state apportate modifiche a questo blocco note rispetto all'ultima versione salvata. Probabilmente non è stato installato
il dizionario corretto Questo file esiste già.
Sovrascriverlo veramente? Questa pagina non ha una cartella per gli allegati Questo nome di pagina non può essere usato a causa dei limiti tecnici del sistema di archiviazione Questa estensione crea un'istantanea dello schermo e la inserisce
in una pagina di Zim.

È un'estensione di base distribuita insieme a Zim.
 Questo componente aggiuntivo aggiunge una "barra percorso" nella parte superiore della finestra.
La "barra percorso" può mostrare il percorso del blocco note per la pagina corrente,
le pagine visitate per ultime o quelle modificate per ultime.
 Questa estensione aggiunge una finestra di dialogo che visualizza tutte le attività in sospeso
presenti nel blocco note. Le attività in sospeso possono essere caselle di controllo
o elementi marcati con le etichette "TODO" (da fare) o "FIXME" (da risolvere).

È un'estensione di base distribuita insieme a Zim.
 Questa estensione aggiunge una finestra di dialogo per copiare rapidamente del testo 
o il contenuto degli appunti in una pagina di Zim.

È un'estensione di base distribuita insieme a Zim.
 Questa estensione aggiunge un'icona all'area di notifica per un accesso rapido.

Questa estensione necessita di Gtk+, versione 2.10 o più recente.

È un'estensione di base distribuita insieme a Zim.
 Questa estensione aggiunge un nuovo widget che mostra 
l'elenco delle pagine collegate a quella corrente.

È un'estensione di base distribuita insieme a Zim.
 Questa estensione aggiunge un widget che visualizza il sommario
della pagina corrente.

È un'estensione di base distribuita insieme a Zim.
 Questa estensione aggiunge alcune impostazioni per usare Zim
come un editor senza distrazioni.
 Questa estensione aggiunge la finestra di dialogo 'Inserisci simbolo' e permette
la formattazione automatica di caratteri tipografici.

È un'estensione di base distribuita insieme a Zim.
 Questo componente aggiuntivo aggiunge un riquadro con l'indice della pagina alla finestra principale.
 Questa estensione aggiunge il controllo versione ai blocchi note.


L'estensione supporta i sistemi di controllo versione Bazaar, Git e Mercurial.

È un'estensione di base distribuita insieme a Zim.
 Questo componente aggiuntivo consente di inserire 'blocchi di codice' nella pagina, che
saranno mostrati come widget incorporati, con sintassi in evidenza, numeri di riga, ecc.
 Questo plugin permette di includere espressioni aritmetiche in zim.
Si basa sul modulo aritmetico 
http://pp.com.mx/python/arithmetic.
 Questa estensione permette di valutare semplici
espressioni matematiche in Zim.

È un'estensione di base distribuita insieme a Zim.
 Il plugin ha anche delle proprietà;
vedi la finestra delle proprietà del blocco note Questa estensione mette a disposizione di Zim un editor di diagrammi basato su Ditaa.

È un'estensione di base distribuita insieme a Zim.
 Questa estensione mette a disposizione di Zim un editor di diagrammi basato su GraphViz.

È un'estensione di base distribuita insieme a Zim.
 Questa estensione mette a disposizione una finestra di dialogo con una
rappresentazione grafica della struttura dei collegamenti del
blocco note. Può essere utilizzata come una specie di "mappa mentale"
che mostra le relazioni fra le pagine.

È un'estensione di base distribuita insieme a Zim.
 Questa estensione mette a disposizione l'indice delle pagine filtrato attraverso la selezione
di una o più etichette da una tag cloud.
 Questa estensione mette a disposizione di Zim un editor di grafici plot basato su GNU R.
 Questa estensione mette a disposizione di Zim un editor di grafici plot basato su Gnuplot.
 Questa estensione aggiunge a Zim un editor di diagrammi di sequenza basato su seqdiag.
Consente la modifica facile dei diagrammi di sequenza.
 Questa estensione fornisce una soluzione alternativa alla mancanza del
supporto alla stampa in Zim. Esporta la pagina corrente
in formato HTML e la apre in un browser. Supponendo che il browser
consenta la funzionalità di stampa, si potrà
stampare la pagina con due clic.

È un'estensione di base distribuita insieme a Zim.
 Questa estensione mette a disposizione di Zim un editor di equazioni basato su LaTeX.

È un'estensione di base distribuita insieme a Zim.
 Questa estensione mette a disposizione un editor di partiture per zim basato su GNU Lilypond.

È un'estensione di base distribuita insieme a Zim.
 Questa estensione mostra la cartella con gli allegati della pagina corrente sotto forma
di icona nel riquadro inferiore.
 Questa estensione ordina alfabeticamente le righe selezionate.
Se l'elenco è già ordinato, l'ordine viene invertito
(da A-Z a Z-A).
 Questa estensione trasforma una sezione del blocco note in
un diario con una pagina per ogni giorno, settimana o
mese.
Inoltre aggiunge un calendario per accedere a queste pagine.
 Ciò di solito significa che il file contiene caratteri non validi Titolo Per continuare è possibile salvare una copia di questa pagina o scartare
tutte le modifiche. Se se ne salva una copia, le modifiche saranno comunque
scartate, ma sarà possibile ripristinare la copia in seguito. Per creare un nuovo blocco note è necessario selezionare una cartella vuota.
Naturalmente è anche possibile selezionare la cartella di un blocco note zim esistente.
 Sommario _Oggi Oggi Attiva o disattiva casella di controllo '>' Attiva la casella 'V' Attiva la casella 'X' Pannello superiore Icona nell'area di notifica Converti il nome della pagina in etichette per le attività Tipo Deseleziona casella Premi il tasto <Backspace> per ridurre l'indentazione
(se disabilitato puoi ancora usare la combinazione <Maiusc><Tab>). Sconosciuto Tipo d'immagine sconosciuto Oggetto sconosciuto Non specificato Nessuna etichetta Aggiorna %i collegamento che punta alla pagina corrente Aggiorna %i collegamenti che puntano alla pagina corrente Aggiorna il titolo di questa pagina Aggiornamento dei collegamenti in corso Aggiornamento dell'indice in corso Usa %s per passare al pannello laterale Usa carattere personalizzato Usa una pagina per ogni Usa la data delle pagine del diario Premi il tasto <Invio> per visitare i collegamenti
(se disabilitato puoi comunque usare la combinazione di tasti <Alt><Invio>) Controllo versione Al momento il controllo versione non è abilitato per questo blocco note.
Abilitarlo? Versioni Visualizza _annotazioni Visualizza _registro Server Web Settimana Quando si segnala questo bug, includere
i dati contenuti nella casella di testo sottostante. Intera _parola Larghezza Pagina wiki: %s Con questa estensione puoi inserire una 'tabella' in una pagina wiki. Le tabelle saranno mostrate come widget GTK TreeView.
La funzionalità consente anche l'esportazione in vari formati (ad es. HTML o LaTeX).
 Conteggio parole Conteggio parole... Parole Anno Ieri Si sta modificando un file con un'applicazione esterna. Al termine si può chiudere questa finestra di dialogo. È possibile configurare gli strumenti personalizzati che appariranno
nel menù strumenti e nella barra strumenti o nel menù contestuale. Zim Desktop Wiki _Riduci ingrandimento _Informazioni _Aggiungi Tutti i pannelli _Aritmetica _Indietro _Sfoglia _Bug _Annulla _Casella di controllo _Livello inferiore _Elimina formattazione _Chiudi _Collassa tutto _Contenuti _Copia _Copia qui _Elimina _Elimina pagina Scarta _modifiche _Duplica riga _Modifica _Modifica collegamento _Modifica oggetto o collegamento... _Modifica Proprietà _Modifica... _Corsivo Domande _frequenti _File _Trova _Trova... _Avanti Schermo _intero _Vai A_iuto _Evidenzia _Cronologia _Pagina iniziale _Immagine... _Importa pagina... _Inserisci _Vai _Vai a... _Scorciatoie da tastiera _Collegamento _Collega alla data _Collegamento... _Evidenziato A_ltro _Sposta _Sposta qui _Sposta riga in basso _Sposta riga in alto _Nuova pagina qui... _Nuova pagina... _Successivo _Nessuno Dimensioni _normali Elenco _numerato _OK _Apri _Apri un altro blocco note... _Altro... _Pagina _Gerarchia della pagina _Livello superiore _Incolla _Anteprima _Precedente _Stampa _Stampa su browser Annotazione _veloce... _Esci Pagine _recenti _Ripeti Espressione _regolare _Ricarica _Rimuovi _Elimina riga _Rimuovi collegamento S_ostituisci _Sostituisci... _Ripristina dimensioni _Ripristina versione _Salva _Salva copia _Screenshot... _Cerca _Cerca... _Invia a... Riquadri _Laterali _Affiancate _Ordina linee _Barrato _Grassetto _Pedice _Apice _Modelli S_trumenti _Annulla _Verbatim _Versioni... _Visualizza _Aumenta ingrandimento come data di scadenza delle attività come data d'inizio delle attività calendar:week_start:1 non usare bordi orizzontali bordi non visibili Sola lettura secondi Launchpad Contributions:
  Alessandro Sarretta https://launchpad.net/~alessandro-sarretta
  Calogero Bonasia https://launchpad.net/~0disse0
  Davide Truffa https://launchpad.net/~catoblepa
  Giuseppe Carrera https://launchpad.net/~expertia
  Jaap Karssenberg https://launchpad.net/~jaap.karssenberg
  Jacopo Moronato https://launchpad.net/~jmoronat
  Marco Cevoli https://launchpad.net/~marco-cevoli
  Mario Calabrese https://launchpad.net/~mario-calabrese
  Martino Barbon https://launchpad.net/~martins999
  Matteo Ferrabone https://launchpad.net/~desmoteo-gmail
  Nicola Gramola https://launchpad.net/~nicola-gramola
  Nicola Jelmorini https://launchpad.net/~jelmorini
  Nicola Moretto https://launchpad.net/~nicola88
  Nicola Piovesan https://launchpad.net/~piovesannicola
  igi https://launchpad.net/~igor-cali bordi verticali bordi visibili {count} di {total} 