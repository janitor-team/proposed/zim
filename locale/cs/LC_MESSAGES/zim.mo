��    r     �              <'  ,   ='  .   j'  ?   �'     �'     �'     (  0   (     O(     j(     �(  	   �(     �(  i   �(  *   )     <)     L)     Y)     f)  
   �)  -   �)     �)  V   �)     *  	   *  	   )*     3*  3   G*  Y   {*     �*     �*  
   �*     +     +     #+     6+     I+     U+     b+  	   i+     s+  $   �+  ?   �+  /   �+  (   ,     @,  %   ],  ,   �,     �,  	   �,     �,     �,  
   �,     �,  	   �,     -     -     -     )-     0-  
   =-     H-     `-     g-     |-     �-     �-  
   �-     �-     �-     �-     �-     �-     
.  <   .     V.  	   \.  
   f.     q.     z.     �.     �.     �.     �.     �.     �.     �.  +   /  3   :/      n/     �/     �/     �/     �/  
   �/     �/     �/     �/     0  3   30     g0     �0     �0     �0     �0     �0     �0     1     1     1     '1     51     B1     G1     K1  0   S1     �1     �1     �1     �1  
   �1     �1     �1     �1     �1     �1     �1     2  $   2  ~   @2     �2     �2  
   �2     �2     �2  
   �2  	    3  
   
3     3     "3     *3     ;3     S3     a3     i3  5   r3     �3     �3     �3  !   �3     �3  #   �3     !4     44     ;4     N4     l4     x4     �4     �4     �4     �4     �4     �4     �4  
   �4     5     5  	   %5  6   /5     f5  v   m5     �5  *   �5  c   !6     �6     �6     �6  
   �6     �6     �6     �6     �6  
   �6  
   �6  
   �6  
   7  
   7  
   7     &7     -7     H7     h7  	   7     �7     �7     �7     �7     �7     �7  
   �7     �7     �7     �7     8     8     )8     98     K8     Z8     g8     s8     �8     �8     �8     �8     �8  	   �8     �8     �8     �8     9     9     )9     79     N9     S9     b9     n9     t9  2   }9     �9     �9     �9     �9     �9     �9     :     /:     H:     T:     p:     �:  	   �:     �:     �:     �:     �:     �:     �:     �:     ;      ;  *   ,;     W;     `;     i;     x;     �;     �;     �;     �;     �;  *   �;  2   <     @<     Q<     b<     {<     �<  	   �<     �<     �<     �<     �<     �<     �<     �<     =  
   )=     4=  	   K=     U=     h=     |=     �=     �=     �=     �=     �=     �=  9   �=     >  I   )>  !   s>  '   �>  	   �>     �>     �>  0   �>  
   ?  	   ?     ?     )?     A?     V?  	   c?     m?     s?  '   |?  V   �?  3   �?  0   /@  (   `@     �@     �@  .   �@     �@     �@     �@     �@     
A     A     A     ,A     4A  
   @A  &   KA  
   rA     }A     �A     �A     �A     �A     �A  
   �A     �A  
   B     B     B  ?   /B     oB     |B     �B     �B     �B     �B     �B     �B     �B     �B     �B  	   	C     C      C     /C     FC     LC     _C     fC     {C     �C     �C     �C     �C     �C     �C  �   �C     wD      �D     �D     �D  	   �D     �D     �D     E     !E     0E     =E     UE     iE     {E  2   �E     �E  &   �E     �E     F     #F     7F     KF     ]F  5   wF  &   �F     �F     �F     �F  &   �F     G     0G     CG     OG     ]G     cG  
   uG     �G     �G  	   �G     �G     �G     �G     �G     �G     �G  	   �G     �G     �G  	   H     H  
   H      H     3H     IH  ?   _H  A   �H  �  �H  S   �J  =   �J  K   7K  @   �K  6   �K  -   �K  I   )L  x   sL  �   �L  �   �M  �   \N  �   �N  �   oO  }   �O  L   nP  �   �P  9   EQ  �   Q  �   'R  �   �R  }   CS  C   �S  h   T  k   nT  �   �T  R   �U  ;   V  =   =V  v   {V    �V  j   X  n   qX  ]   �X     >Y  �   �Y  7   SZ     �Z  �   �Z  |   -[     �[     �[     �[     �[     �[     �[     �[  	    \  '   
\     2\     7\  D   I\     �\     �\     �\     �\     �\  H   �\     ]     4]     C]  !   R]     t]     �]     �]  P   �]     ^  U   ^     m^     v^  	   �^  
   �^     �^  N   �^     �^     �^     _  �   _  
   �_     �_     �_     �_  	   �_  ^   �_  f   W`     �`  	   �`     �`     �`  
   �`     �`     �`     a     
a     a  	   a     "a     )a     ;a     Ba  	   Pa     Za  
   `a     ka     sa     �a     �a     �a  
   �a     �a     �a     �a  	   �a     �a     �a     �a     �a     b     b     b     !b  
   'b     2b     ;b  	   Ab     Kb     [b     cb     ib     ub     �b     �b     �b     �b     �b     �b  
   �b     �b     �b     �b     �b     �b     �b     c     c     !c     %c     +c  	   Ec     Oc     Uc     ec     mc     tc  	   }c     �c     �c     �c     �c     �c     �c     �c     �c     �c     �c     �c     d     d     d     (d     9d  
   ?d     Jd     Yd  
   ad     ld     xd     �d     �d     �d     �d  
   �d     �d  
   �d     �d     �d  	   �d     �d     �d     �d     e     e     2e  
   He     Se     de     re     {e     �e     �e  
   �e     �e  �  �e  +   �g  /   �g  A   h     Wh  %   eh  $   �h  J   �h  F   �h  )   Bi     li     ui     �i  V   �i  ,   �i     j     'j     9j  %   Hj     nj  .   ~j     �j  _   �j     k     )k     :k     Jk  D   fk  W   �k     l     l  	   (l     2l     El     Vl     ql  	   �l  
   �l     �l     �l     �l  !   �l  7   �l  0   0m  4   am      �m  6   �m  1   �m      n     ;n     Kn     dn     ln     }n  	   �n     �n     �n  
   �n  
   �n     �n  	   �n     �n     �n     o  	   o     'o     7o     >o     Do     Uo     oo     �o     �o     �o  U   �o     %p     -p     @p  	   Wp     ap     jp  
   �p     �p     �p     �p     �p     �p  -   �p  3   !q  *   Uq  
   �q     �q     �q     �q     �q     �q     �q     r     )r  1   Fr  "   xr     �r     �r     �r     �r  .   �r     #s  
   6s     As     Js     ]s     qs     �s     �s  	   �s  6   �s     �s     �s     �s     �s     t     t     .t     :t     @t     Lt     Tt     it  !   {t  �   �t      u     1u     9u  
   Hu     Su     mu     ~u     �u     �u     �u     �u     �u     �u     �u     �u  B   v     Jv     av  
   pv  &   {v     �v  ,   �v     �v     �v     w  "    w     Cw     Sw     fw     �w  $   �w  !   �w     �w     �w     �w     �w     x     "x     4x  F   Bx     �x  i   �x     �x     y  i   0y     �y     �y     �y     �y     �y     �y     �y     z     
z  	   z  	   !z  	   +z  	   5z  	   ?z     Iz  (   Qz  1   zz     �z     �z     �z     �z     �z     �z     {  
   ({     3{     F{     ^{     }{     �{     �{     �{     �{     �{     �{     �{     |     |     3|     O|     ^|     n|  	   �|     �|     �|  
   �|     �|     �|     �|     }     !}     '}     3}     F}     O}  B   \}     �}     �}  #   �}     �}  &   �}     ~     +~  !   F~     h~  $   ~~     �~     �~  	   �~     �~     �~     �~  !        *     D     \     o  6   v  A   �     �     �     �     �     2�     C�     R�  "   d�     ��  6   ��  E   π     �     *�  *   A�  "   l�     ��     ��     ��     ��      ��     �     �     �  '   �     A�     `�     t�     ��     ��     ��     Ղ     �     �  
    �     +�  
   K�     V�  N   h�     ��  c   ʃ  1   .�  *   `�     ��     ��     ��  '   ��     ؄     �     ��     
�     $�     ?�     N�     W�     _�  (   t�  i   ��  ,   �      4�  .   U�     ��     ��  *   ��     ֆ     ݆     �     ��  
   �     �     �     2�     C�  
   O�  1   Z�     ��     ��     ��     ć     ؇  %   ��  	   �     %�     4�     F�     X�     i�  A   ��     Ĉ     ӈ  $   �     
�     �     �     "�     9�     I�     Y�     k�     z�     ��     ��     ��     ��     ŉ     �     �     �     �     �     )�     9�     G�     V�  �   g�     ��  (   �     ;�     V�     n�     v�     ��     ��     ��     ��  "   ҋ     ��     �     /�  <   O�     ��  -   ��     ˌ     �     	�     !�     A�  )   W�  =   ��  ,   ��     �     ��     �     �     9�  !   L�     n�     }�  	   ��     ��     ��     ��     Î     �     ��     ��     �     �  !   %�     G�     M�     \�     c�     l�     u�     z�     ��     ��     ��  (   ��  :   �  �   �  H   �  D   O�  B   ��  H   ג  /    �  $   P�  R   u�  �   ȓ  �   J�  �   ��  �   ޕ  �   ��  }   �  I   ��  O   ϗ  �   �  @   ��  �   �  �   r�  �   �  {   ��  N   �  ^   _�  `   ��  �   �  O   Ĝ  B   �  :   W�  |   ��  �   �  Z   ��  �   Q�  `   ӟ  j   4�  �   ��  8   C�     |�  �   ��  �   !�     ��     ��     ��     ��     ע     ��     �     #�  (   ?�     h�     l�  P   ��  	   ݣ     �     ��  	   �     �  �   '�     ��     Ӥ     �  +   �     :�     S�  $   q�  b   ��     ��  N   �     W�     ]�     t�     ��     ��  J   ��     �     ��     ��  �   �     ӧ     ߧ     �     �     ��  W   ��  {   V�     Ҩ  	   �     ��     ��     �  
   �     �     %�     1�     8�     A�     N�     `�  	   x�     ��     ��     ��     ��     ��     ��     ˩     ۩     �     ��     �     "�     6�     B�     K�     \�     d�  
   l�     w�     �  	   ��     ��     ��  	   ��     Ǫ     Ϊ     ۪     �  	   ��     �     �     &�     -�     =�  	   D�     N�     U�     a�     q�     ��     ��     ��     ɫ  	   ҫ     ܫ     �     �  
   
�     �     -�  	   9�     C�     X�     j�     s�     |�  	   ��     ��     ��  	   ì     ͬ     �     ��     �  
   �     #�     3�  	   D�     N�     [�     o�     ~�     ��     ��     ��  
   ��     ��     ˭     ޭ     �     �     �     �     (�  	   6�  
   @�     K�  
   T�  	   _�  	   i�  
   s�     ~�  #   ��     ��     ׮     �     ��     �     �  �  #�     "�     7�     G�   		This plugin provides bar for bookmarks.
		 %(cmd)s
returned non-zero exit status %(code)i %(n_error)i errors and %(n_warning)i warnings occurred, see log %A %d %B %Y %i Attachment %i Attachments %i errors occurred, see log %i file will be deleted %i files will be deleted %i open item %i open items %i warnings occurred, see log <Top> <Unknown> A desktop wiki A file with the name <b>"%s"</b> already exists.
You can use another name or overwrite the existing file. A table needs to have at least one column. Add Application Add Bookmark Add Notebook Add bookmark/Show settings Add column Add new bookmarks to the beginning of the bar Add row Adds spell checking support using gtkspell.

This is a core plugin shipping with zim.
 Align All Files All Tasks Allow public access Always use last cursor position when opening a page An error occurred while generating the image.
Do you want to save the source text anyway? Annotated Page Source Applications Arithmetic Ascii graph (Ditaa) Attach File Attach image first Attachment Browser Attachments Attachments: Author Auto
Wrap Auto indenting Automatically saved version from zim Automatically select the current word when you apply formatting Automatically turn "CamelCase" words into links Automatically turn file paths into links Autosave interval in minutes Autosave version on regular intervals Autosave version when the notebook is closed Back to Original Name BackLinks BackLinks Pane Backend Backlinks: Bazaar Bookmarks BookmarksBar Border width Bottom Pane Browse Bulle_t List C_onfigure Can not modify page: %s Cancel Capture whole screen Center Change columns Changes Characters Characters excluding spaces Check Checkbox '>' Check Checkbox 'V' Check Checkbox 'X' Check _spelling Checkbo_x List Classic trayicon,
do not use new style status icon on Ubuntu Clear Clone row Code Block Column 1 Command Command does not modify data Comment Common include footer Common include header Complete _notebook Configure Applications Configure Plugin Configure an application to open "%s" links Configure an application to open files
of type "%s" Consider all checkboxes as tasks Copy Copy Email Address Copy Template Copy _As... Copy _Link Copy _Location Could not find executable "%s" Could not find notebook: %s Could not find template "%s" Could not find the file or folder for this notebook Could not load spell checking Could not open: %s Could not parse expression Could not read: %s Could not save page: %s Create a new page for each note Create folder? Created Cu_t Custom Tools Custom _Tools Customize... Date Day Default Default format for copying text to the clipboard Default notebook Delay Delete Page Delete page "%s"? Delete row Demote Dependencies Description Details Diagram Discard note? Distraction Free Editing Do you want to delete all bookmarks? Do you want to restore page: %(page)s
to saved version: %(version)s ?

All changes since the last saved version will be lost ! Document Root Due E_xport... Edit %s Edit Custom Tool Edit Image Edit Link Edit Table Edit _Source Editing Editing file: %s Enable Version Control? Enable plugin Enabled Equation Error in %(file)s at line %(line)i near "%(snippet)s" Evaluate _Math Expand _All Export Export all pages to a single file Export completed Export each page to a separate file Exporting notebook Failed Failed running: %s Failed to run application: %s File Exists File _Templates... File changed on disk: %s File exists File is not writable: %s File type not supported: %s Filename Filter Find Find Ne_xt Find Pre_vious Find and Replace Find what Flag tasks due on Monday or Tuesday before the weekend Folder Folder already exists and has content, exporting to this folder may overwrite existing files. Do you want to continue? Folder exists: %s Folder with templates for attachment files For advanced search you can use operators like
AND, OR and NOT. See the help page for more details. For_mat Format Fossil GNU R Plot Get more plugins online Get more templates online Git Gnuplot Grid lines Heading _1 Heading _2 Heading _3 Heading _4 Heading _5 Height Hide Journal pane if empty Hide menubar in fullscreen mode Highlight current line Home Page Horizontal _Line Icon Images Import Page Include subpages Index Index page Inline Calculator Insert Code Block Insert Date and Time Insert Diagram Insert Ditaa Insert Equation Insert GNU R Plot Insert Gnuplot Insert Image Insert Link Insert Score Insert Screenshot Insert Sequence Diagram Insert Symbol Insert Table Insert Text From File Interface Interwiki Keyword Journal Jump to Jump to Page Labels marking tasks Last Modified Leave link to new page Left Left Side Pane Line Sorter Lines Link Map Link files under document root with full file path Link to Location Log events with Zeitgeist Log file Looks like you found a bug Make default application Managing table columns Map document root to URL Match _case Maximum number of bookmarks Maximum page width Menubar Mercurial Modified Month Move Selected Text... Move Text to Other Page Move column ahead Move column backward Move text to Name Need output file to export MHTML Need output folder to export full notebook New File New Page New Page in %s New S_ub Page... New _Attachment Next No Applications Found No changes since last version No dependencies No document root defined for this notebook No plugin available to display objects of type: %s No such file: %s No such page: %s No such wiki defined: %s No templates installed Notebook Notebooks OK Only Show Active Tasks Open Attachments _Folder Open Folder Open Notebook Open With... Open _Document Root Open _Notebook Folder Open _Page Open cell content link Open help Open in New Window Open in New _Window Open new page Open plugins folder Open with "%s" Optional Options for plugin %s Other... Output file Output file exists, specify "--overwrite" to force export Output folder Output folder exists and not empty, specify "--overwrite" to force export Output location needed for export Output should replace current selection Overwrite P_athbar Page Page "%s" does not have a folder for attachments Page Index Page Name Page Template Page already exists: %s Page not allowed: %s Page section Paragraph Paste Path Bar Please enter a comment for this version Please note that linking to a non-existing page
also creates a new page automatically. Please select a name and a folder for the notebook. Please select a row, before you push the button. Please select more than one line of text Please specify a notebook Plugin Plugin "%s" is required to display this object Plugins Port Position in the window Pr_eferences Preferences Prev Print to Browser Promote Proper_ties Properties Pushes events to the Zeitgeist daemon. Quick Note Quick Note... Recent Changes Recent Changes... Recently _Changed pages Reformat wiki markup on the fly Remove Remove All Remove column Remove row Removing Links Rename page "%s" Repeated clicking a checkbox cycles through the checkbox states Replace _All Replace with Restore page to saved version? Rev Right Right Side Pane Right margin position Row down Row up S_ave Version... Save A _Copy... Save Copy Save Version Save bookmarks Saved version from zim Score Screenshot Command Search Search _Backlinks... Search this section Section Section(s) to ignore Section(s) to index Select File Select Folder Select Image Select a version to see changes between that version and the current
state. Or select multiple versions to see changes between those versions.
 Select the export format Select the output file or folder Select the pages to export Select window or region Selection Sequence Diagram Server not started Server started Server stopped Set New Name Set default text editor Set to Current Page Show Line Numbers Show Tasks as Flat List Show ToC as floating widget instead of in sidepane Show _Changes Show a separate icon for each notebook Show full Page Name Show full page name Show helper toolbar Show in the toolbar Show right margin Show tasklist in sidepane Show the cursor also for pages that can not be edited Show the page title heading in the ToC Single _page Size Smart Home key Some error occurred while running "%s" Sort alphabetically Sort pages by tags Source View Spell Checker Start Start _Web Server Sy_mbol... Syntax System Default Tab width Table Table Editor Table of Contents Tags Tags for non-actionable tasks Task Task List Tasks Template Templates Text Text Files Text From _File... Text background color Text foreground color The folder
%s
does not yet exist.
Do you want to create it now? The folder "%s" does not yet exist.
Do you want to create it now? The following parameters will be substituted
in the command when it is executed:
<tt>
<b>%f</b> the page source as a temporary file
<b>%d</b> the attachment directory of the current page
<b>%s</b> the real page source file (if any)
<b>%p</b> the page name
<b>%n</b> the notebook location (file or folder)
<b>%D</b> the document root (if any)
<b>%t</b> the selected text or word under cursor
<b>%T</b> the selected text including wiki formatting
</tt>
 The inline calculator plugin was not able
to evaluate the expression at the cursor. The table must consist of at least on row!
 No deletion done. There are no changes in this notebook since the last version that was saved This could mean you don't have the proper
dictionaries installed This file already exists.
Do you want to overwrite it? This page does not have an attachments folder This page name cannot be used due to technical limitations of the storage This plugin  allows taking a screenshot and directly insert it
in a zim page.

This is a core plugin shipping with zim.
 This plugin adds a "path bar" to the top of the window.
This "path bar" can show the notebook path for the current page,
recent visited pages or recent edited pages.
 This plugin adds a dialog showing all open tasks in
this notebook. Open tasks can be either open checkboxes
or items marked with tags like "TODO" or "FIXME".

This is a core plugin shipping with zim.
 This plugin adds a dialog to quickly drop some text or clipboard
content into a zim page.

This is a core plugin shipping with zim.
 This plugin adds a tray icon for quick access.

This plugin depends on Gtk+ version 2.10 or newer.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a list of pages
linking to the current page.

This is a core plugin shipping with zim.
 This plugin adds an extra widget showing a table of
contents for the current page.

This is a core plugin shipping with zim.
 This plugin adds settings that help using zim
as a distraction free editor.
 This plugin adds the 'Insert Symbol' dialog and allows
auto-formatting typographic characters.

This is a core plugin shipping with zim.
 This plugin adds the page index pane to the main window.
 This plugin adds version control for notebooks.

This plugin supports the Bazaar, Git and Mercurial version control systems.

This is a core plugin shipping with zim.
 This plugin allows inserting 'Code Blocks' in the page. These will be
shown as embedded widgets with syntax highlighting, line numbers etc.
 This plugin allows you to embed arithmetic calculations in zim.
It is based on the arithmetic module from
http://pp.com.mx/python/arithmetic.
 This plugin allows you to quickly evaluate simple
mathematical expressions in zim.

This is a core plugin shipping with zim.
 This plugin also has properties,
see the notebook properties dialog This plugin provides a diagram editor for zim based on Ditaa.

This is a core plugin shipping with zim.
 This plugin provides a diagram editor for zim based on GraphViz.

This is a core plugin shipping with zim.
 This plugin provides a dialog with a graphical
representation of the linking structure of the
notebook. It can be used as a kind of "mind map"
showing how pages relate.

This is a core plugin shipping with zim.
 This plugin provides a page index filtered by means of selecting tags in a cloud.
 This plugin provides a plot editor for zim based on GNU R.
 This plugin provides a plot editor for zim based on Gnuplot.
 This plugin provides a sequence diagram editor for zim based on seqdiag.
It allows easy editing of sequence diagrams.
 This plugin provides a workaround for the lack of
printing support in zim. It exports the current page
to html and opens a browser. Assuming the browser
does have printing support this will get your
data to the printer in two steps.

This is a core plugin shipping with zim.
 This plugin provides an equation editor for zim based on latex.

This is a core plugin shipping with zim.
 This plugin provides an score editor for zim based on GNU Lilypond.

This is a core plugin shipping with zim.
 This plugin shows the attachments folder of the current page as an
icon view at bottom pane.
 This plugin sorts selected lines in alphabetical order.
If the list is already sorted the order will be reversed
(A-Z to Z-A).
 This plugin turns one section of the notebook into a journal
with a page per day, week or month.
Also adds a calendar widget to access these pages.
 This usually means the file contains invalid characters Title To continue you can save a copy of this page or discard
any changes. If you save a copy changes will be also
discarded, but you can restore the copy later. To create a new notebook you need to select an empty folder.
Of course you can also select an existing zim notebook folder.
 ToC To_day Today Toggle Checkbox '>' Toggle Checkbox 'V' Toggle Checkbox 'X' Top Pane Tray Icon Turn page name into tags for task items Type Un-check Checkbox Unindent on <BackSpace>
(If disabled you can still use <Shift><Tab>) Unknown Unkown Image type Unkown Object Unspecified Untagged Update %i page linking to this page Update %i pages linking to this page Update the heading of this page Updating Links Updating index Use %s to switch to the side pane Use a custom font Use a page for each Use date from journal pages Use the <Enter> key to follow links
(If disabled you can still use <Alt><Enter>) Version Control Version control is currently not enabled for this notebook.
Do you want to enable it? Versions View _Annotated View _Log Web Server Week When reporting this bug please include
the information from the text box below Whole _word Width Wiki page: %s With this plugin you can embed a 'Table' into the wiki page. Tables will be shown as GTK TreeView widgets.
Exporting them to various formats (i.e. HTML/LaTeX) completes the feature set.
 Word Count Word Count... Words Year Yesterday You are editing a file in an external application. You can close this dialog when you are done You can configure custom tools that will appear
in the tool menu and in the tool bar or context menus. Zim Desktop Wiki Zoom _Out _About _Add _All Panes _Arithmetic _Back _Browse _Bugs _Cancel _Checkbox _Child _Clear Formatting _Close _Collapse All _Contents _Copy _Copy Here _Delete _Delete Page _Discard Changes _Duplicate Line _Edit _Edit Link _Edit Link or Object... _Edit Properties _Edit... _Emphasis _FAQ _File _Find _Find... _Forward _Fullscreen _Go _Help _Highlight _History _Home _Image... _Import Page... _Insert _Jump _Jump To... _Keybindings _Link _Link to date _Link... _Mark _More _Move _Move Here _Move Line Down _Move Line Up _New Page Here... _New Page... _Next _None _Normal Size _Numbered List _OK _Open _Open Another Notebook... _Other... _Page _Page Hierarchy _Parent _Paste _Preview _Previous _Print _Print to Browser _Quick Note... _Quit _Recent pages _Redo _Regular expression _Reload _Remove _Remove Line _Remove Link _Replace _Replace... _Reset Size _Restore Version _Save _Save Copy _Screenshot... _Search _Search... _Send To... _Side Panes _Side by Side _Sort lines _Strike _Strong _Subscript _Superscript _Templates _Tools _Undo _Verbatim _Versions... _View _Zoom In as due date for tasks as start date for tasks calendar:week_start:0 do not use horizontal lines no grid lines readonly seconds translator-credits vertical lines with lines {count} of {total} Project-Id-Version: zim
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-07 21:09+0200
PO-Revision-Date: 2020-09-24 20:40+0000
Last-Translator: vlastimilott <admin@wp-admin.cz>
Language-Team: Czech <https://hosted.weblate.org/projects/zim/master/cs/>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Weblate 4.3-dev
X-Launchpad-Export-Date: 2020-01-01 13:41+0000
 		Plugin zobrazuje panel se záložkami.
		 Příkaz %(cmd)s
vrátil chybový kód %(code)i Nastalo %(n_error)i chyb a %(n_warning)i varování, více v logu %A, %d. %B %Y %i příloha %i přílohy %i příloh počet objevených chyb: %i, viz log %i soubor bude smazán %i soubory budou smazány %i souborů bude smazáno %i otevřená položka %i otevřené položky %i otevřených položek počet objevených varování %i, viz log <Vrchol> <Neznámý> Desktopová wiki Soubor jménem <b>"%s"</b> už existuje.
Vyberte jiné jméno, nebo soubor přepište. Tabulka musí obsahovat aspoň jeden sloupec Přidat aplikaci Přidat záložku Přidat sešit Přidat záložku/Zobrazit nastavení Přidat sloupec Přidávat nové záložky na začátek panelu Přidat řádek Modul zajišťuje kontrolu překlepů pomocí GtkSpell.

Základní modul dodávaný se Zimem.
 Zarovnání Všechny soubory Všechny úkoly Povolit veřejný přístup Při otevření stránky umístit kurzor na poslední známou pozici Při vytváření obrázku došlo k chybě.
Přejete si zdrojový text přesto uložit? Komentovaný zdroj stránky Aplikace Výpočty Ascii graf (Ditaa) Připojit soubor Nejprve připojit obrázek Prohlížeč příloh Přílohy Přílohy: Autor Automatické
zalamování Automatické odsazování Verze uložená automaticky zimem Při formátování automaticky vybrat aktuální slovo Automaticky vytvářet odkazy ze SloženýchSlov Automaticky vytvářet odkazy z adresářových cest Interval ukládání v minutách Automaticky ukládat verze v pravidelných intervalech Automaticky ukládat verzi při zavření sešitu Zpět k původnímu názvu Zpětné odkazy Panel zpětných odkazů Systém Zpětné odkazy: Bazaar Záložky Panel záložek Šířka okraje Panel dole Procházet Odráž_kový seznam _Nastavit Nelze upravit stránku %s Zrušit Sejmout celou obrazovku Na střed Změnit sloupce Změny Znaky Znaků bez mezer Zatrhnout přepínač '>' Zatrhnout přepínač 'V' Zatrhnout přepínač 'X' _Kontrola překlepů Zašk_rtávací seznam Klasická ikona do systémového panelu,
ikona v novém pojetí se nehodí pro Ubuntu Vymazat Duplikovat řádek Blok zdrojového kódu Sloupec 1 Příkaz Příkaz nezmění data Komentář Společné zápatí Společné záhlaví Kompletní sešit Nastavit aplikace Nastavit zásuvný modul Nastavit aplikaci pro otevírání odkazů %s Nastavit aplikaci pro otevírání souborů
typu %s Považovat všechny přepínače za úlohy Kopírovat Kopírovat e-mailovou adresu Kopírovat šablonu Kopírovat _jako... Kopírovat _odkaz Kopírovat _umístění Program "%s" nenalezen. Nelze najít sešit %s Nelze načíst šablonu "%s" Nelze najít soubor nebo složku pro tento sešit Nelze načíst kontrolu překlepů Nelze otevřít %s Výraz je nesrozumitelný Nelze načíst %s Stránku %s nelze uložit Vytvořit pro každou poznámku novou stránku Vytvořit složku? Vytvořeno _Vyjmout Vlastní nástroje _Vlastní nástroje Přizpůsobit... Datum Den Výchozí Výchozí formát pro kopírování textu do schránky Výchozí sešit Prodleva Smazat stránku Smazat stránku %s? Odstranit řádek O úroveň níž Závislosti Popis Podrobnosti Diagram Odstranit poznámku? Nerušivé psaní Chcete smazat všechny záložky? Přejete si obnovit stránku %(page)s
na uloženou verzi %(version)s?
Všechny změny od poslední uložené verze budou ztraceny! Kořen dokumentu Termín E_xportovat... Upravit %s Upravit vlastní nástroj Upravit obrázek Upravit odkaz Upravit tabulku _Upravit zdrojový kód Úpravy Úpravy souboru %s Povolit správu verzí? Povolit plugin Povoleno Rovnice Chyba v souboru %(file)s na řádku %(line)i blízko "%(snippet)s" Vyhodnotit _matematiku _Rozbalit vše Exportovat Exportovat stránky do jednoho souboru Export dokončen Exportovat stránky do oddělených souborů Probíhá export sešitu Nevyřešeny Nepodařilo se spustit %s Nepodařilo se spustit aplikaci %s Soubor existuje Šablony _souborů Soubor %s se na disku změnil Soubor existuje Soubor %s je chráněn proti zápisu Typ souboru není podporován: %s Jméno souboru Filtr Najít Najít _následující Najít _předchozí Najít a zaměnit Najít výraz Úkoly s uzávěrkou v pondělí nebo úterý označit před víkendem Složka Složka již existuje a obsahuje soubory - hrozí, že při exportu budou přepsány. Chcete pokračovat? Adresář %s existuje Složka se šablonami příloh Pro pokročilé vyhledávání můžete použít operátory
AND, OR a NOT. Více informace v nápovědě. _Formát Formát Fossil Nákres GNU R Získejte více modulů online Získejte více šablon online Git Gnuplot Ohraničení Nadpis _1 Nadpis _2 Nadpis _3 Nadpis _4 Nadpis _5 Výška Skrýt panel deníku, pokud je prázdný V režimu celé obrazovky skrýt hlavní nabídku Zvýraznit aktuální řádek Domovská stránka _Vodorovná čára Ikona Obrázky Importovat stránku Zahrnout podstránky Rejstřík Stránka s indexem Zabudovaná kalkulačka Vložit blok zdrojového kódu Vložit datum a čas Vložit diagram Vložit Ditaa Vložit rovnici Vložit graf GNU R Vložit Gnuplot Vložit obrázek Vložit odkaz Vložit notový zápis Vložit snímek obrazovky Vložit sekvenční diagram Vložit symbol Vložit tabulku Vložit text ze souboru Rozhraní klíčové slovo interwiki Deník Skočit na Skočit na stránku Značky pro označení úloh Naposledy upraveno Vložit odkaz na novou stránku Vlevo Panel vlevo Setřídit řádky Řádků Mapa odkazů Odkazovat soubory v kořenové složce dokumentu absolutní cestou Odkaz na Umístění Ukládat záznamy pomocí Zeitgeist Soubor se záznamem Pravděpodobně jste narazili na chybu Nastavit jako výchozí Správa sloupců v tabulce Převést kořen dokumentu na URL Rozlišovat _velikost Nejvyšší možný počet záložek Maximální šířka strany Hlavní nabídka Mercurial Změněn Měsíc _Přesunout vybraný text... Přesunout text na jinou stránku Posunout sloupec dopředu Posunout sloupec dozadu Přesunout text na Název K exportu do MHTML je potřeba zadat výstupní soubor Pro export celého sešitu je potřeba zadat výstupní adresář Nový soubor Nová stránka Nová stránka v %s Nová _podstránka Nová _příloha Následující Žádné aplikace Žádné změny od poslední verze Bez závislostí Pro tento sešit není nastavený kořenový adresář Není k dispozici žádný plugin, který by zobrazil objekty typu %s Soubor %s neexistuje Stránka %s neexistuje Takový odkaz na wiki není definován: %s Nenainstalovány žádné šablony Sešit Sešity OK Zobrazovat jen aktivní úkoly Otevřít složku s _přílohami Otevřít složku Otevřít sešit Otevřít pomocí... Otevřít _kořenovou složku dokumentu Otevřít složku se _sešitem Otevřít _stránku Otevřít odkaz v buňce Otevřít nápovědu Otevřít v novém okně Otevřít v novém _okně Otevřít novou stránku Otevřít adresář pluginů Otevřít s „%s“ Volitelné Možnosti zásuvného modulu %s Další... Výstupní soubor Výstupní soubor už existuje, použijte parametr "--overwrite" k přepsání Výstupní složka Výstupní adresář už existuje a není prázdný, použijte parametr "--overwrite" k přepsání Pro výstup z exportu je nutné zadat umístění Výstupem se přepíše aktuální výběr Přepsat Ukazatel struktury Stránka Stránka %s nemá složku pro přílohy Seznam stránek Název stránky Šablona stránky Stránka %s již existuje Stránka %s není povolena Sekce stránky Odstavec Vložit Stránková navigace Vložte prosím k této verzi komentář Pamatujte na to, že pokud odkážete na stránku, která dosud neexistuje, 
bude automaticky vytvořena. Vyberte prosím název a složku pro sešit. Vyberte prosím nejprve řádek. Vyberte prosím více než jeden řádek textu Vyberte prosím sešit Zásuvný modul K zobrazení objektu je nutný plugin "%s" Moduly Port Pozice uvnitř okna _Nastavení Nastavení Předchozí Zobrazit v prohlížeči O úroveň výš V_lastnosti Vlastnosti Zaznamenává události pomocí démonu Zeitgeist Rychlá poznámka Rychlá poznámka... Nedávné změny Nedávné změny... Nedávno změněné stránky Přeformátovat wiki značky za běhu Odstranit Odstranit vše Odstranit sloupec Odstranit řádek Odstranit odkazy Přejmenovat stránku %s Opakované klikání na přepínač postupně aktivuje jeho stavy Nahradit _vše Nahradit výrazem Obnovit stránku na uloženou verzi? Rev Vpravo Panel vpravo Pozice pravého okraje O řádek níž O řádek výš Uložit v_erzi... Uložit _kopii Uložit kopii Uložit verzi Uložit záložky Verze uložená zimem Skóre Příkaz pro snímek obrazovky Hledat Hledat _zpětné odkazy Prohledat sekci Sekce Ignorovat sekce Indexovat sekce Vybrat soubor Vybrat složku Vyberte obrázek Vyberte verzi, abyste mohli srovnat rozdíly mezi touto verzí a aktuální verzí.
Nebo vyberte více verzí a uvidíte rozdíly mezi nimi.
 Vyberte exportovaný formát Vyberte výstupní soubor nebo adresář Vyberte stránky k exportu Vybrat okno nebo oblast Výběr Sekvenční diagram Server neběží Server spuštěn Server zastaven Nastavit nový název Nastavit výchozí textový editor Nastavit na aktuální stránku Zobrazit čísla řádků Nezobrazovat nadřazené úkoly Obsah stránky jako plovoucí prvek místo v bočním panelu Zobrazit _změny Pro každý sešit zobrazit samostatnou ikonu Zobrazit celý název stránky Zobrazit celý název stránky Zobrazit pomocný panel Zobrazit v nástrojové liště Zobrazit pravý okraj Zobrazit seznam úkolů v bočním panelu Zobrazit kurzor také na stránkách, které nelze upravovat. V souhrnném obsahu zobrazit název stránky Jednu _stránku Velikost Chytrá klávesa Home Při běhu %s došlo k chybě Seřadit abecedně Seřadit stránky podle štítků Zobrazit zdroj Kontrola překlepů Začátek Spustit _webový server Sy_mbol Syntaxe Výchozí nastavení systému Šířka tabulátoru Tabulka Editor tabulek Obsah stránky Štítky Štítky pro nesplnitelné úkoly Úkol Seznam úkolů Úkoly Šablona Šablony Text Textové soubory Text ze _souboru Barva pozadí textu Barva textu Složka
%s
neexistuje. Má se vytvořit? Složka "%s" zatím neexistuje.
Chcete ji nyní vytvořit? Následující parametry budou v příkazu 
po spuštění nahrazeny:
<tt>
<b>%f</b> zdroj stránky jako dočasný soubor
<b>%d</b> adresář s přílohami současné stránky
<b>%s</b> skutečný zdrojový soubor stránky (pokud existuje)
<b>%p</b> název stránky
<b>%n</b> umístění sešitu (soubor nebo adresář)
<b>%D</b> kořenový adresář dokumentu (pokud existuje)
<b>%t</b> vybraný text nebo slovo pod kurzorem
<b>%T</b> vybraný text včetně wiki formátování
</tt>
 Zabudovaná kalkulačka nedokázala
vyhodnotit výraz na pozici kurzoru. Tabulka musí obsahovat aspoň jeden řádek. 
 Nic nebylo smazáno. Od poslední uložené verze nevznikly v sešitě žádné změny. To může znamenat, že nemáte nainstalované
odpovídající slovníky Tento soubor už existuje.
Chcete jej přepsat? Stránka nemá složku s přílohami Název stránky nelze použít kvůli technickému omezení datového úložiště Tento modul umožní vytvořit snímek obrazovky
a vložit jej přímo do stránky.

Jde o základní modul dodávaný se Zimem.
 Plugin přidává do horní části okna pás s názvy stránek.
Může obsahovat cestu k aktuální stránce v sešitu,
naposledy navštívené nebo upravené stránky.
 Modul přidává dialogové okno zobrazující všechny 
otevřené úkoly v sešitu. Otevřené úkoly jsou buď
nezatržené přepínače nebo položky označené
slovy jako TODO nebo FIXME.

Základní modul dodávaný se zimem.
 Tento modul přidává dialog, pomocí něhož můžete do stránky 
rychle přidat nějaký text nebo obsah schránky.

Je to základní modul dodávaný se Zimem.
 Modul zajišťuje ikonu v systémové oblasti.
Závisí na Gtk+ verze 2.10 nebo novější.
Základní modul dodávaný se zimem.
 Modul zobrazuje widget se seznamem stránek,
které odkazují na aktuální stránku.

Základní modul dodávaný se Zimem.
 Modul zobrazuje osnovu na aktuální stránce osnovu
z nadpisů (obsah).
 Tento modul přidává nastavení, která ze Zimu
vytvoří nerušivý editor.
 Modul přidává dialog 'Vložit symbol' a umožňuje
automaticky formátovat typografické znaky.

Základní modul dodávaný se Zimem.
 Plugin přidává do hlavního okna panel se seznamem stránek.
 Modul přidává sešitům možnost správy verzí.
Podporuje systémy Bazaar, Git a Mercurial.
Základní modul dodávaný se Zimem.
 Tento plugin umožní vkládat bloky kódu do stránky. Zobrazí se
jako vložené rámečky se zvýrazněnou syntaxí, číslováním řádek apod.
 Tento modul umožňuje zahrnout do zimu aritmetické výpočty.
Je založen na výpočetním modulu z
 http://pp.com.mx/python/arithmetic.
 Tento modul vám umožní rychle vyhodnotit
jednoduché matematické výrazy.

Jde o základní modul dodávaný se Zimem.
 Tento plugin má také další vlastnosti,
které najdete v nastavení sešitu Modul poskytuje editor diagramů založených na Ditaa.
Základní modul dodávaný se Zimem.
 Modul poskytuje editor diagramů založený na GraphViz.

Základní modul dodávaný se Zimem.
 Modul zobrazuje okno se strukturou sešitu. Můžete ho použít jako 
myšlenkovou mapu - zobrazuje vztahy mezi stránkami.

Základní modul dodávaný se Zimem.
 Modul poskytuje seznam stránek filtrovaný na základě vybraných štítků.
 Tento modul nabízí editor diagramů, schémat a grafů v GNU R.
 Modul poskytuje editor pro grafy  založené na Gnuplotu.
 Tento modul poskytuje editor sekvenčních diagramů založený na seqdiag.
Díky němu je snadné tyto diagramy upravovat.
 Tento modul poskytuje dočasné řešení pro tisk,
jehož podpora v Zimu chybí. Exportuje stránku 
do HTML a spustí webový prohlížeč, v němž ji 
pak lze snadno vytisknout.

Tohle je základní modul dodávaný se Zimem.
 Modul poskytuje editor rovnic založený na LaTeX.

Základní modul dodávaný se Zimem.
 Modul poskytuje editor pro vytváření notových zápisů 
(založený na GNU Lilypond).

Základní modul dodávaný se Zimem.
 Tento plugin zobrazuje složku s přílohami současné stránky jako ikonu 
na dolním panelu.
 Modul seřadí označené řádky podle abecedy.
Pokud je seznam už setříděný, pořadí se obrátí.
 Tento modul upraví jednu sekci v sešitu na deník
s jednou stránkou na den, týden nebo měsíc.
Také přidává kalendář pro přístup k těmto stránkám.
 Obvykle to znamená, že soubor obsahuje neplatné znaky Nadpis Můžete uložit kopii stránky, nebo všechny změny zahodit.
Pokud uložíte kopii, změny budou sice také zahozeny, 
ale kopii můžete později obnovit. K vytvoření nového sešitu musíte vybrat prázdnou složku.
Samozřejmě můžete vybrat také složku existujícího sešitu.
 Obsah _Dnešek Dnes Změnit stav přepínače '>' Zatrhnout přepínač kladně Zatrhnout přepínač záporně Panel nahoře Ikona v systémové oblasti Názvy stránek jako štítky v úkolech Typ Zrušit zatržení přepínače Smazat odsazení při stisku Backspace
(pokud to nefunguje, použijte Shift-Tab) Neznámé Neznámý typ obrázku Neznámý objekt Neurčeno Beze štítku Aktualizovat %i stránku, která sem odkazuje Aktualizovat %i stránky, které sem odkazují Aktualizovat %i stránek, které sem odkazují Aktualizovat záhlaví stránky Probíhá aktualizace odkazů Probíhá aktualizace indexu Nastavit %s pro přepnutí bočního panelu Použít vlastní písmo Použít pro každý stránku Použít datum ze stránek v deníku Pomocí klávesy Enter můžete přejít na cíl odkazu
(pokud to nefunguje, použijte Alt-Enter). Správa verzí Správa verzí není pro tento sešit  aktuálně povolena.
Chcete ji povolit? Verze Zobrazit _komentované Zobrazit _záznam Webový server Týden Pokud budete hlásit chybu, přiložte,
prosím, níže uvedené informace _Celá slova Šířka Wiki stránka: %s Pomocí tohoto pluginu můžete do stránky vložit tabulku. Tabulky se zobrazují jako GTK prvky TreeView.
Mezi jejich možnosti patří také export do různých formátů (např. HTML/LaTeX).
 Počet slov Počet slov... Slov Rok Včera Upravujete soubor v externí aplikaci. Až budete hotovi, můžete tohle okno zavřít. Můžete si nastavit vlastní programy, zobrazí se v nabídce Nástroje,
 v nástrojové liště a v kontextové nabídce. Zim Desktop Wiki Z_menšit _O programu Př_idat Všechny _panely _Výpočty _Zpět _Procházet _Chyby _Zrušit Přepí_nač O úroveň _níž _Vymazat formátování _Zavřít S_balit vše _Obsah _Kopírovat _Kopírovat sem _Smazat _Smazat stránku _Zahodit změny D_uplikovat řádek Ú_pravy _Upravit odkaz _Upravit odkaz nebo objekt _Upravit vlastnosti _Upravit… _Kurziva Časté _otázky _Soubor _Najít N_ajít... _Vpřed _Celoobrazovkový režim _Přejít _Nápověda _Zvýraznění _Historie _Domů _Obrázek... _Importovat stránku _Vložit _Přejít Skočit na... _Klávesové zkratky _Odkaz _Odkaz na datum _Odkaz O_značit _Více _Přesunout _Přesunout sem Posunout řádek _dolů Posunout řádek na_horu _Nová stránka zde... _Nová stránka _Další Žá_dný _Normální velikost Čís_lovaný seznam _OK _Otevřít _Otevřít jiný sešit _Další... _Stránku _Hierarchie stránek O úroveň _výš V_ložit _Náhled _Předchozí _Tisknout _Zobrazit v prohlížeči _Rychlá poznámka... U_končit _Nedávné stránky Zrušit v_rácení _Regulární výraz Z_novu načíst _Odstranit _Smazat řádek Odst_ranit odkaz _Nahradit Nah_radit... _Původní velikost O_bnovit verzi _Uložit Uložit _kopii _Snímek obrazovky... _Hledat _Hledat... Odesla_t... _Postranní panely _Jedna ku jedné _Setřídit řádky Proškr_tnout _Tučně _Dolní index _Horní index Ša_blony Nás_troje V_rátit Stro_jopis _Verze... _Zobrazit Z_většit jako koncové datum pro úkoly jako počáteční datum pro úkoly calendar:week_start:1 nepoužívat vodorovné ohraničení bez ohraničení jen ke čtení sekund Launchpad Contributions:
  1.John@seznam.cz https://launchpad.net/~neozvuck
  Jakub Kozisek https://launchpad.net/~kozisek-j
  Konki https://launchpad.net/~pavel-konkol
  Pastorek https://launchpad.net/~tillf
  Radek Tříška https://launchpad.net/~radek-fastlinux
  Tomas Sara https://launchpad.net/~tomas-sara
  Vlastimil Ott https://launchpad.net/~vlastimil
  Vlastimil Ott https://launchpad.net/~vlastimil-e-ott
  Vlastimil Ott https://launchpad.net/~vlastimil-ott
  aloisam https://launchpad.net/~a-musil svislé ohraničení s ohraničením {count} z {total} 